
alter table "public"."tree" add column "genus" text
 null;

alter table "public"."tree" add column "species" text
 null;

alter table "public"."tree" add column "vernacular_name" text
 null;

alter table "public"."tree" add column "height" text
 null;

alter table "public"."tree" add column "diameter" integer
 null;

alter table "public"."tree" add column "plantation_date" text
 null;

alter table "public"."tree" add column "watering" boolean
 null;

alter table "public"."tree" add column "watering_notes" text
 null;

alter table "public"."tree" add column "is_tree_of_interest" boolean
 null;

alter table "public"."tree" add column "notes" text
 null;
