CREATE EXTENSION IF NOT EXISTS pgcrypto;
alter table "public"."organization" add column "bucket" uuid
 null unique default gen_random_uuid();
