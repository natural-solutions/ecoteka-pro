DROP TABLE IF EXISTS "public"."green_area" CASCADE;

CREATE TABLE "public"."vegetated_area" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "coords" geometry NOT NULL, "address" text, "creation_date" timestamptz NOT NULL DEFAULT now(), "edition_date" timestamptz,  "type" text NOT NULL, "surface" numeric NOT NULL, "urban_site_id" uuid, "landscape_type" text,  "has_differentiated_mowing" boolean not null default 'false', "is_accessible" boolean not null default 'false', "inconvenience_risk" text, "shrubs_data" jsonb, "herbaceous_data" jsonb, "note" text, PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."vegetated_area" IS E'List of all vegetated areas';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "public"."vegetated_area"
  add constraint "vegetated_area_urban_site_id_fkey"
  foreign key ("urban_site_id")
  references "public"."urban_site"
  ("id") on update restrict on delete restrict;


