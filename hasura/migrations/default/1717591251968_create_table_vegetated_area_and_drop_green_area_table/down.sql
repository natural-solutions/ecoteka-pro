-- Drop the foreign key constraint
ALTER TABLE "public"."vegetated_area"
  DROP CONSTRAINT IF EXISTS "vegetated_area_urban_site_id_fkey";

-- Drop the primary key constraint
ALTER TABLE "public"."vegetated_area"
  DROP CONSTRAINT IF EXISTS "vegetated_area_pkey";

-- Drop the table
DROP TABLE IF EXISTS "public"."vegetated_area";

-- Drop the pgcrypto extension if it was created only for this table
DROP EXTENSION IF EXISTS pgcrypto;
