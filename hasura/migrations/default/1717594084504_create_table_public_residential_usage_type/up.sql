CREATE TABLE "public"."residential_usage_type" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "slug" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."residential_usage_type" IS E'List of all residential usage types';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
