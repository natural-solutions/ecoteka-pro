
DROP INDEX IF EXISTS "public"."location_status_color_idx";

DROP INDEX IF EXISTS "public"."intervention_realization_date_idx";

DROP INDEX IF EXISTS "public"."intervention_scheduled_date_idx";

DROP INDEX IF EXISTS "public"."diagnosis_tree_condition_idx";

DROP INDEX IF EXISTS "public"."tree_plantation_date_idx";

DROP INDEX IF EXISTS "public"."tree_vernacular_name_idx";

DROP INDEX IF EXISTS "public"."tree_scientific_name_idx";

DROP INDEX IF EXISTS "public"."intervention_type_slug_idx";

DROP INDEX IF EXISTS "public"."location_status_id_idx";

DROP INDEX IF EXISTS "public"."location_status_name_idx";

DROP INDEX IF EXISTS "public"."location_id_status_idx";

DROP INDEX IF EXISTS "public"."location_address_idx";
