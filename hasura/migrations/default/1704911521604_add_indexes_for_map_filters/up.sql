
CREATE  INDEX "location_address_idx" on
  "public"."location" using btree ("address");

CREATE  INDEX "location_id_status_idx" on
  "public"."location" using btree ("id_status");

CREATE  INDEX "location_status_name_idx" on
  "public"."location_status" using btree ("status");

CREATE  INDEX "location_status_id_idx" on
  "public"."location_status" using btree ("id");

CREATE  INDEX "intervention_type_slug_idx" on
  "public"."intervention_type" using btree ("slug");

CREATE  INDEX "tree_scientific_name_idx" on
  "public"."tree" using btree ("scientific_name");

CREATE  INDEX "tree_vernacular_name_idx" on
  "public"."tree" using btree ("vernacular_name");

CREATE  INDEX "tree_plantation_date_idx" on
  "public"."tree" using btree ("plantation_date");

CREATE  INDEX "diagnosis_tree_condition_idx" on
  "public"."diagnosis" using btree ("tree_condition");

CREATE  INDEX "intervention_scheduled_date_idx" on
  "public"."intervention" using btree ("scheduled_date");

CREATE  INDEX "intervention_realization_date_idx" on
  "public"."intervention" using btree ("realization_date");

CREATE  INDEX "location_status_color_idx" on
  "public"."location_status" using btree ("color");
