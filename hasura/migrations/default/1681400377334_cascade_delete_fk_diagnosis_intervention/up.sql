
alter table "public"."intervention" drop constraint "intervention_tree_id_fkey",
  add constraint "intervention_tree_id_fkey"
  foreign key ("tree_id")
  references "public"."tree"
  ("id") on update restrict on delete cascade;

alter table "public"."diagnosis" drop constraint "diagnosis_tree_id_fkey",
  add constraint "diagnosis_tree_id_fkey"
  foreign key ("tree_id")
  references "public"."tree"
  ("id") on update restrict on delete cascade;

alter table "public"."intervention" drop constraint "intervention_location_id_fkey",
  add constraint "intervention_location_id_fkey"
  foreign key ("location_id")
  references "public"."location"
  ("id") on update restrict on delete cascade;
