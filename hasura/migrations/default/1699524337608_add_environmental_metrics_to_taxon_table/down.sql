
alter table "public"."taxon" drop column "carbon";

alter table "public"."taxon" drop column "icu";

alter table "public"."taxon" drop column "air";

alter table "public"."taxon" drop column "biodiversity";

alter table "public"."taxon" drop column "allergenic_score";

alter table "public"."taxon" drop column "vulnerability";

alter table "public"."taxon" drop column "growth_category";