
alter table "public"."taxon" add column "carbon" numeric
 null;

alter table "public"."taxon" add column "icu" numeric
 null;

alter table "public"."taxon" add column "air" numeric
 null;

alter table "public"."taxon" add column "biodiversity" numeric
 null;

alter table "public"."taxon" add column "allergenic_score" numeric
 null;

alter table "public"."taxon" add column "vulnerability" numeric
 null;

alter table "public"."taxon" add column "growth_category" text
 null;
