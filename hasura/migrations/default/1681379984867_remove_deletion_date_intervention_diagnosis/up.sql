
alter table "public"."intervention" drop column "deletion_date" cascade;

alter table "public"."diagnosis" drop column "deletion_date" cascade;
