
alter table "public"."diagnosis" alter column "deletion_date" drop not null;
alter table "public"."diagnosis" add column "deletion_date" timestamptz;

alter table "public"."intervention" alter column "deletion_date" drop not null;
alter table "public"."intervention" add column "deletion_date" timestamptz;
