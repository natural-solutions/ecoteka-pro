CREATE TABLE "public"."worksite_intervention" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "worksite_id" uuid NOT NULL, "intervention_id" uuid NOT NULL, "deletion_date" timestamptz, "edition_date" timestamptz, "creation_date" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."worksite_intervention" IS E'Join table between worksite and intervention';
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_edition_date"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."edition_date" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_worksite_intervention_edition_date"
BEFORE UPDATE ON "public"."worksite_intervention"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_edition_date"();
COMMENT ON TRIGGER "set_public_worksite_intervention_edition_date" ON "public"."worksite_intervention" 
IS 'trigger to set value of column "edition_date" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "public"."worksite_intervention"
  add constraint "worksite_intervention_worksite_id_fkey"
  foreign key ("worksite_id")
  references "public"."worksite"
  ("id") on update restrict on delete restrict;

alter table "public"."worksite_intervention"
  add constraint "worksite_intervention_intervention_id_fkey"
  foreign key ("intervention_id")
  references "public"."intervention"
  ("id") on update restrict on delete restrict;