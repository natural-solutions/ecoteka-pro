UPDATE "public"."intervention" i
SET worksite_id = (
    SELECT wi.worksite_id 
    FROM worksite_intervention wi
    WHERE wi.intervention_id = i.id
)
WHERE EXISTS (
    SELECT 1 
    FROM worksite_intervention wi
    WHERE wi.intervention_id = i.id
);
DROP TABLE "public"."worksite_intervention";


