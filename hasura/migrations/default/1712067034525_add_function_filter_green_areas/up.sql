CREATE OR REPLACE FUNCTION filter_green_areas(
    z integer,
    x integer,
    y integer,
    query_params json
) 
RETURNS bytea 
LANGUAGE plpgsql 
IMMUTABLE STRICT PARALLEL SAFE 
AS $$
DECLARE
    _green_area_active boolean := (query_params->>'green_area_active')::boolean;
    mvt bytea;
BEGIN
    SELECT INTO mvt ST_AsMVT(tile, 'filter_green_areas', 4096, 'geom')
    FROM (
        SELECT 
            ST_AsMVTGeom(
                ST_Transform(
                    ST_CurveToLine(ST_GeomFromGeoJSON(ST_AsGeoJSON(coords))),
                    3857
                ),
                ST_TileEnvelope(z, x, y),
                4096,
                64,
                true
            ) AS geom,  
            green_area.id AS green_area_id,
            green_area.name AS green_area_name,
            green_area.description as green_area_description
        FROM 
            green_area
        WHERE 
            ST_Intersects(coords, ST_Transform(ST_TileEnvelope(z, x, y), 4326))
            AND (
                _green_area_active IS TRUE
            )
    ) AS tile
    WHERE 
        geom IS NOT NULL;  
    RETURN mvt;
END;
$$;
