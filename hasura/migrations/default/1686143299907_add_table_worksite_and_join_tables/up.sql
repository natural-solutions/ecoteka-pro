
CREATE TABLE "public"."worksite" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "creation_date" timestamptz NOT NULL DEFAULT now(), "edition_date" timestamptz, "deletion_date" timestamptz, "name" text NOT NULL, "description" text, PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."worksite" IS E'Table for managing worksite group';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_edition_date"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."edition_date" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_worksite_edition_date"
BEFORE UPDATE ON "public"."worksite"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_edition_date"();
COMMENT ON TRIGGER "set_public_worksite_edition_date" ON "public"."worksite" 
IS 'trigger to set value of column "edition_date" to current timestamp on row update';

CREATE TABLE "public"."worksite_intervention" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "worksite_id" uuid NOT NULL, "intervention_id" uuid NOT NULL, "deletion_date" timestamptz, "edition_date" timestamptz, "creation_date" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."worksite_intervention" IS E'Join table between worksite and intervention';
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_edition_date"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."edition_date" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_worksite_intervention_edition_date"
BEFORE UPDATE ON "public"."worksite_intervention"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_edition_date"();
COMMENT ON TRIGGER "set_public_worksite_intervention_edition_date" ON "public"."worksite_intervention" 
IS 'trigger to set value of column "edition_date" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "public"."worksite_intervention"
  add constraint "worksite_intervention_worksite_id_fkey"
  foreign key ("worksite_id")
  references "public"."worksite"
  ("id") on update restrict on delete restrict;

alter table "public"."worksite_intervention"
  add constraint "worksite_intervention_intervention_id_fkey"
  foreign key ("intervention_id")
  references "public"."intervention"
  ("id") on update restrict on delete restrict;

CREATE TABLE "public"."worksite_location" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "worksite_id" uuid NOT NULL, "location_id" uuid NOT NULL, "deletion_date" timestamptz, "edition_date" timestamptz, "creation_date" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") , FOREIGN KEY ("worksite_id") REFERENCES "public"."worksite"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("location_id") REFERENCES "public"."location"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("id"));COMMENT ON TABLE "public"."worksite_location" IS E'Join table between worksite and location';
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_edition_date"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."edition_date" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_worksite_location_edition_date"
BEFORE UPDATE ON "public"."worksite_location"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_edition_date"();
COMMENT ON TRIGGER "set_public_worksite_location_edition_date" ON "public"."worksite_location" 
IS 'trigger to set value of column "edition_date" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
