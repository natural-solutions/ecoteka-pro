
DROP TABLE "public"."worksite_location";

alter table "public"."worksite_intervention" drop constraint "worksite_intervention_intervention_id_fkey";

alter table "public"."worksite_intervention" drop constraint "worksite_intervention_worksite_id_fkey";

DROP TABLE "public"."worksite_intervention";

DROP TABLE "public"."worksite";
