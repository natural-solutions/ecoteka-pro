
alter table "public"."intervention" alter column "realization_date" set not null;

alter table "public"."intervention" rename column "scheduled_date" to "provisional_date";

alter table "public"."intervention" rename column "realization_date" to "intervention_date";
