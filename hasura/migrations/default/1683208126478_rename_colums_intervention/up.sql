
alter table "public"."intervention" rename column "intervention_date" to "realization_date";

alter table "public"."intervention" rename column "provisional_date" to "scheduled_date";

alter table "public"."intervention" alter column "realization_date" drop not null;
