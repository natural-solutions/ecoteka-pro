CREATE VIEW tree_scientific_names_count AS
SELECT scientific_name, vernacular_name, taxon_id, COUNT(*) 
FROM public.tree
WHERE 
scientific_name is not null
and
vernacular_name is not null
GROUP BY scientific_name, vernacular_name, taxon_id;
