
alter table "public"."intervention" add column "worksite_id" uuid
 null;

alter table "public"."intervention"
  add constraint "intervention_worksite_id_fkey"
  foreign key ("worksite_id")
  references "public"."worksite"
  ("id") on update restrict on delete cascade;
