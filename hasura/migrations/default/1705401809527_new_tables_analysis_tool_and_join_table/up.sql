
CREATE TABLE "public"."analysis_tool" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "label" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."analysis_tool" IS E'List of all of possible analysis tools of a diagnosis';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "public"."diagnosis_analysis_tool" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "diagnosis_id" uuid NOT NULL, "analysis_tool_id" uuid NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("diagnosis_id") REFERENCES "public"."diagnosis"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("analysis_tool_id") REFERENCES "public"."analysis_tool"("id") ON UPDATE restrict ON DELETE restrict);COMMENT ON TABLE "public"."diagnosis_analysis_tool" IS E'Join table between diagnosis and analysis_tool';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
