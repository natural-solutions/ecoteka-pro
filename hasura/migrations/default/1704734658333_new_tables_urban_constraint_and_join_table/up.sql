
CREATE TABLE "public"."urban_constraint" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "slug" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."urban_constraint" IS E'List of all of possible urban constraints of a location';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "public"."location_urban_constraint" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "location_id" uuid NOT NULL, "urban_constraint_id" uuid NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("location_id") REFERENCES "public"."location"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("urban_constraint_id") REFERENCES "public"."urban_constraint"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("id"));COMMENT ON TABLE "public"."location_urban_constraint" IS E'Join table between location and urban_constraint';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
