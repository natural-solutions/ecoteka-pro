
CREATE TABLE "public"."import" ("id" uuid NOT NULL, "task_id" text, "creation_date" timestamptz NOT NULL DEFAULT now(), "source_file_path" text NOT NULL, "source_crs" text, "data_entry_user_id" text NOT NULL, "edition_date" timestamptz DEFAULT now(), PRIMARY KEY ("id") , FOREIGN KEY ("data_entry_user_id") REFERENCES "public"."user_entity"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("id"), UNIQUE ("task_id"));COMMENT ON TABLE "public"."import" IS E'List of all imports';
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_edition_date"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."edition_date" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_import_edition_date"
BEFORE UPDATE ON "public"."import"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_edition_date"();
COMMENT ON TRIGGER "set_public_import_edition_date" ON "public"."import" 
IS 'trigger to set value of column "edition_date" to current timestamp on row update';

alter table "public"."import" alter column "id" set default gen_random_uuid();
