CREATE TABLE "public"."green_area" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" text NOT NULL, "coords" geometry NOT NULL, "description" text, "creation_date" timestamptz NOT NULL DEFAULT now(), "edition_date" timestamptz, PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."green_area" IS E'List of all green areas';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
