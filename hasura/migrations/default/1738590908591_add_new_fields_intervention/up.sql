
alter table "public"."intervention" add column "request_origin" text
 null;

alter table "public"."intervention" add column "is_parking_banned" boolean
 null default 'false';