CREATE OR REPLACE FUNCTION set_organization_default_to_false()
RETURNS TRIGGER AS $$
BEGIN
  UPDATE "public"."organization"
  SET "default" = false
  WHERE id <> NEW.id;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER set_organization_default_to_false_trigger
AFTER INSERT OR UPDATE ON "public"."organization"
FOR EACH ROW
WHEN (NEW.default = true)
EXECUTE FUNCTION set_organization_default_to_false();
