
alter table "public"."worksite" add column "realization_report" text
 null;

alter table "public"."worksite" add column "realization_user_id" text
 null;

alter table "public"."worksite"
  add constraint "worksite_realization_user_id_fkey"
  foreign key ("realization_user_id")
  references "public"."user_entity"
  ("id") on update restrict on delete restrict;
