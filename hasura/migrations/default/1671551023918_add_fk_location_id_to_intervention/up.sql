
alter table "public"."intervention" add column "location_id" uuid
 null;

alter table "public"."intervention"
  add constraint "intervention_location_id_fkey"
  foreign key ("location_id")
  references "public"."location"
  ("id") on update restrict on delete restrict;
