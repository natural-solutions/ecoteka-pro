DROP VIEW tree_scientific_names_count;

CREATE OR REPLACE VIEW tree_scientific_names_count AS
SELECT scientific_name, COUNT(*) 
FROM public.tree
WHERE 
scientific_name is not null
and
scientific_name <> ''
GROUP BY scientific_name;
