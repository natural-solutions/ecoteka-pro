-- Restore 'diagnosis' family first
INSERT INTO family_intervention_type (slug) VALUES ('diagnosis');

-- Restore old intervention types with their families
INSERT INTO intervention_type (slug, location_types, family_intervention_type_id) 
SELECT 'carpenters', '["tree"]', id 
FROM family_intervention_type 
WHERE slug = 'maintenance';

INSERT INTO intervention_type (slug, location_types, family_intervention_type_id) 
SELECT 'follow_up_planting', '["tree"]', id 
FROM family_intervention_type 
WHERE slug = 'planting';

-- Move diagnosis types back to diagnosis family
UPDATE intervention_type it
SET family_intervention_type_id = (
    SELECT fit.id 
    FROM family_intervention_type fit 
    WHERE fit.slug = 'diagnosis'
)
WHERE it.slug IN ('visual_diagnosis', 'detailed_diagnosis');

-- Restore old pruning names with correct family association
UPDATE intervention_type it 
SET slug = 'pruning',
    family_intervention_type_id = (SELECT id FROM family_intervention_type WHERE slug = 'pruning')
WHERE it.slug = 'maintenance_pruning';

UPDATE intervention_type it
SET slug = 'trim',
    family_intervention_type_id = (SELECT id FROM family_intervention_type WHERE slug = 'pruning')
WHERE it.slug = 'size_pruning';

UPDATE intervention_type it
SET slug = 'dead_wood',
    family_intervention_type_id = (SELECT id FROM family_intervention_type WHERE slug = 'pruning')
WHERE it.slug = 'sanitary_pruning';

-- Remove new pruning types
DELETE FROM intervention_type WHERE slug IN ('specific_pruning', 'pruning_other');

-- Restore old maintenance names with correct family association
UPDATE intervention_type it
SET slug = 'treatment',
    family_intervention_type_id = (SELECT id FROM family_intervention_type WHERE slug = 'maintenance')
WHERE it.slug = 'treatment_application';

-- Remove new intervention types from planting family
DELETE FROM intervention_type WHERE slug IN (
    'marking_staking_dict',
    'pit_preparation',
    'stake_replacement',
    'stake_removal',
    'watering_basin',
    'probe_installation',
    'probe_removal',
    'probe_maintenance',
    'probe_reading',
    'planting_tree_other'
);

-- Remove new maintenance types
DELETE FROM intervention_type WHERE slug IN (
    'treatment_control',
    'tree_base_weeding',
    'tree_base_mulching',
    'tree_base_groundcover',
    'trunk_protection_installation',
    'trunk_protection_removal',
    'trunk_protection_control',
    'biodiversity_accessories',
    'maintenance_other'
);

-- Remove new felling types (trees)
DELETE FROM intervention_type WHERE slug IN (
    'dismantling',
    'bracing_installation',
    'bracing_removal',
    'bracing_replacement',
    'bracing_control',
    'felling_tree_other'
);

-- Remove new felling types (stumps)
DELETE FROM intervention_type WHERE slug IN (
    'trimming',
    'devitalization',
    'felling_stump_other'
);