-- add cascade delete on intervention_type_id
alter table "public"."intervention" drop constraint "intervention_intervention_type_id_fkey",
  add constraint "intervention_intervention_type_id_fkey"
  foreign key ("intervention_type_id")
  references "public"."intervention_type"
  ("id") on update restrict on delete cascade;

-- Étape 1 : Associer les types 'visual_diagnosis' et 'detailed_diagnosis' à la famille 'maintenance'
UPDATE intervention_type it
SET family_intervention_type_id = (
    SELECT fit.id 
    FROM family_intervention_type fit 
    WHERE fit.slug = 'maintenance'
)
WHERE it.slug IN ('visual_diagnosis', 'detailed_diagnosis');

-- Étape 2 : Supprimer la famille 'diagnosis'
DELETE FROM family_intervention_type
WHERE slug = 'diagnosis';

-- Supprimer les types d'intervention 'carpenters' et 'follow_up_planting'
DELETE FROM intervention_type
WHERE slug IN ('carpenters', 'follow_up_planting');

-- Famille planting
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id) 
SELECT 'marking_staking_dict', '["empty_location"]', id FROM family_intervention_type WHERE slug = 'planting';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'pit_preparation', '["empty_location"]', id FROM family_intervention_type WHERE slug = 'planting';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'stake_replacement', '["tree"]', id FROM family_intervention_type WHERE slug = 'planting';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'stake_removal', '["tree"]', id FROM family_intervention_type WHERE slug = 'planting';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'watering_basin', '["tree"]', id FROM family_intervention_type WHERE slug = 'planting';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'probe_installation', '["tree"]', id FROM family_intervention_type WHERE slug = 'planting';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'probe_removal', '["tree"]', id FROM family_intervention_type WHERE slug = 'planting';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'probe_maintenance', '["tree"]', id FROM family_intervention_type WHERE slug = 'planting';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'probe_reading', '["tree"]', id FROM family_intervention_type WHERE slug = 'planting';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'planting_tree_other', '["tree"]', id FROM family_intervention_type WHERE slug = 'planting';

-- Famille pruning
UPDATE public.intervention_type it 
SET slug = 'maintenance_pruning',
    family_intervention_type_id = (SELECT id FROM family_intervention_type WHERE slug = 'pruning')
WHERE it.slug = 'pruning';

UPDATE public.intervention_type it
SET slug = 'size_pruning',
    family_intervention_type_id = (SELECT id FROM family_intervention_type WHERE slug = 'pruning')
WHERE it.slug = 'trim';

UPDATE public.intervention_type it
SET slug = 'sanitary_pruning',
    family_intervention_type_id = (SELECT id FROM family_intervention_type WHERE slug = 'pruning')
WHERE it.slug = 'dead_wood';

INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'specific_pruning', '["tree"]', id FROM family_intervention_type WHERE slug = 'pruning';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'pruning_other', '["tree"]', id FROM family_intervention_type WHERE slug = 'pruning';

-- Famille maintenance
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'treatment_control', '["tree"]', id FROM family_intervention_type WHERE slug = 'maintenance';

UPDATE public.intervention_type it
SET slug = 'treatment_application',
    family_intervention_type_id = (SELECT id FROM family_intervention_type WHERE slug = 'maintenance')
WHERE it.slug = 'treatment';

INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'tree_base_weeding', '["tree"]', id FROM family_intervention_type WHERE slug = 'maintenance';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'tree_base_mulching', '["tree"]', id FROM family_intervention_type WHERE slug = 'maintenance';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'tree_base_groundcover', '["tree"]', id FROM family_intervention_type WHERE slug = 'maintenance';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'trunk_protection_installation', '["tree"]', id FROM family_intervention_type WHERE slug = 'maintenance';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'trunk_protection_removal', '["tree"]', id FROM family_intervention_type WHERE slug = 'maintenance';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'trunk_protection_control', '["tree"]', id FROM family_intervention_type WHERE slug = 'maintenance';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'biodiversity_accessories', '["tree"]', id FROM family_intervention_type WHERE slug = 'maintenance';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'maintenance_other', '["tree"]', id FROM family_intervention_type WHERE slug = 'maintenance';

-- Famille felling (arbres)
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'dismantling', '["tree"]', id FROM family_intervention_type WHERE slug = 'felling';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'bracing_installation', '["tree"]', id FROM family_intervention_type WHERE slug = 'felling';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'bracing_removal', '["tree"]', id FROM family_intervention_type WHERE slug = 'felling';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'bracing_replacement', '["tree"]', id FROM family_intervention_type WHERE slug = 'felling';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'bracing_control', '["tree"]', id FROM family_intervention_type WHERE slug = 'felling';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'felling_tree_other', '["tree"]', id FROM family_intervention_type WHERE slug = 'felling';

-- Famille felling (souches)
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'trimming', '["stump"]', id FROM family_intervention_type WHERE slug = 'felling';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'devitalization', '["stump"]', id FROM family_intervention_type WHERE slug = 'felling';
INSERT INTO public.intervention_type (slug, location_types, family_intervention_type_id)
SELECT 'felling_stump_other', '["stump"]', id FROM family_intervention_type WHERE slug = 'felling';
