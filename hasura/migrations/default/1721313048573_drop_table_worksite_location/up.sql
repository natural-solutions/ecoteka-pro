BEGIN;

-- Drop the trigger associated with the worksite_location table
DROP TRIGGER IF EXISTS "set_public_worksite_location_edition_date" ON "public"."worksite_location";

-- Drop the table
DROP TABLE IF EXISTS "public"."worksite_location";

COMMIT;
