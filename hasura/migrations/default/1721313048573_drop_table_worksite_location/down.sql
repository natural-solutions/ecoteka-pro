-- Down migration: Recreate the table
BEGIN;

-- Create the table again
CREATE TABLE "public"."worksite_location" (
    "id" uuid NOT NULL DEFAULT gen_random_uuid(),
    "worksite_id" uuid NOT NULL,
    "location_id" uuid NOT NULL,
    "deletion_date" timestamptz,
    "edition_date" timestamptz,
    "creation_date" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("id"),
    FOREIGN KEY ("worksite_id") REFERENCES "public"."worksite"("id") ON UPDATE restrict ON DELETE restrict,
    FOREIGN KEY ("location_id") REFERENCES "public"."location"("id") ON UPDATE restrict ON DELETE restrict,
    UNIQUE ("id")
);

-- Add the comment on the table
COMMENT ON TABLE "public"."worksite_location" IS E'Join table between worksite and location';

-- Create the function for the trigger
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_edition_date"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."edition_date" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;

-- Create the trigger
CREATE TRIGGER "set_public_worksite_location_edition_date"
BEFORE UPDATE ON "public"."worksite_location"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_edition_date"();

-- Add comment on the trigger
COMMENT ON TRIGGER "set_public_worksite_location_edition_date" ON "public"."worksite_location" 
IS 'trigger to set value of column "edition_date" to current timestamp on row update';

-- Reapply the altered constraints
ALTER TABLE "public"."worksite_location" 
DROP CONSTRAINT IF EXISTS "worksite_location_location_id_fkey",
ADD CONSTRAINT "worksite_location_location_id_fkey"
FOREIGN KEY ("location_id")
REFERENCES "public"."location"("id")
ON UPDATE restrict ON DELETE cascade;

ALTER TABLE "public"."worksite_location" 
DROP CONSTRAINT IF EXISTS "worksite_location_worksite_id_fkey",
ADD CONSTRAINT "worksite_location_worksite_id_fkey"
FOREIGN KEY ("worksite_id")
REFERENCES "public"."worksite"("id")
ON UPDATE restrict ON DELETE cascade;

COMMIT;
