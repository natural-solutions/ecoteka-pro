
alter table "public"."diagnosis_analysis_tool" drop constraint "diagnosis_analysis_tool_diagnosis_id_fkey",
  add constraint "diagnosis_analysis_tool_diagnosis_id_fkey"
  foreign key ("diagnosis_id")
  references "public"."diagnosis"
  ("id") on update restrict on delete cascade;

alter table "public"."diagnosis_pathogen" drop constraint "diagnosis_pathogen_diagnosis_id_fkey",
  add constraint "diagnosis_pathogen_diagnosis_id_fkey"
  foreign key ("diagnosis_id")
  references "public"."diagnosis"
  ("id") on update restrict on delete cascade;
