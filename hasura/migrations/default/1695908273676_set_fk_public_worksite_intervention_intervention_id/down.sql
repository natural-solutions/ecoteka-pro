alter table "public"."worksite_intervention" drop constraint "worksite_intervention_intervention_id_fkey",
  add constraint "worksite_intervention_intervention_id_fkey"
  foreign key ("intervention_id")
  references "public"."intervention"
  ("id") on update restrict on delete cascade;
