
alter table "public"."location" drop column "urban_site_type" cascade;
alter table "public"."location" add column "urban_site_id" uuid;

CREATE TABLE "public"."urban_site" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "label" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."urban_site" IS E'List of all of possible urban site type of a location';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "public"."location"
  add constraint "location_urban_site_id_fkey"
  foreign key ("urban_site_id")
  references "public"."urban_site"
  ("id") on update restrict on delete restrict;
