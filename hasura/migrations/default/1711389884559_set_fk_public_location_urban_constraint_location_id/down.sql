alter table "public"."location_urban_constraint" drop constraint "location_urban_constraint_location_id_fkey",
  add constraint "location_urban_constraint_location_id_fkey"
  foreign key ("location_id")
  references "public"."location"
  ("id") on update restrict on delete restrict;
