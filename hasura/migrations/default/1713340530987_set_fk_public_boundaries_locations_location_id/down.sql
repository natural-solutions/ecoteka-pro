alter table "public"."boundaries_locations" drop constraint "boundaries_locations_location_id_fkey",
  add constraint "boundaries_locations_location_id_fkey"
  foreign key ("location_id")
  references "public"."location"
  ("id") on update restrict on delete restrict;
