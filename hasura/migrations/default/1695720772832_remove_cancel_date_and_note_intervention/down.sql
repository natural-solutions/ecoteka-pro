
alter table "public"."intervention" alter column "cancel_date" drop not null;
alter table "public"."intervention" add column "cancel_date" timestamptz;

alter table "public"."intervention" alter column "cancel_note" drop not null;
alter table "public"."intervention" add column "cancel_note" text;
