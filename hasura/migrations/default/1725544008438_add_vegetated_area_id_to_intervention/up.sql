
alter table "public"."intervention" add column "vegetated_area_id" uuid
 null;

alter table "public"."intervention"
  add constraint "intervention_vegetated_area_id_fkey"
  foreign key ("vegetated_area_id")
  references "public"."vegetated_area"
  ("id") on update restrict on delete cascade;
