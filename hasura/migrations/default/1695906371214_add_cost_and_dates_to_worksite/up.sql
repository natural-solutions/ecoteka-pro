
alter table "public"."worksite" add column "cost" numeric
 null;

alter table "public"."worksite" add column "scheduled_date" date
 null;

alter table "public"."worksite" add column "realization_date" date
 null;
