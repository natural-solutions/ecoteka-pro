
-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."worksite" add column "realization_date" date
--  null;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."worksite" add column "scheduled_date" date
--  null;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."worksite" add column "cost" numeric
--  null;

alter table "public"."worksite" drop column "cost" numeric
 null;

alter table "public"."worksite" drop column "scheduled_date" date
 null;

alter table "public"."worksite" drop column "realization_date" date
 null;
