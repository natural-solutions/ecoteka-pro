CREATE OR REPLACE FUNCTION filter_polygons(
    z integer,
    x integer,
    y integer,
    query_params json
) 
RETURNS bytea 
LANGUAGE plpgsql 
IMMUTABLE STRICT PARALLEL SAFE 
AS $$
DECLARE
    _geographic_boundaries_active boolean := (query_params->>'geographic_boundaries_active')::boolean;
    _station_boundaries_active boolean := (query_params->>'station_boundaries_active')::boolean;
    _vegetated_areas_active boolean := (query_params->>'vegetated_areas_active')::boolean;
    _vegetated_areas_ids uuid[] := string_to_array(query_params->>'vegetated_areas_ids', ',')::uuid[];
    mvt bytea;
    boundary_mvt bytea;
    vegetated_area_mvt bytea;
BEGIN
    -- Create MVT for boundaries
    IF _geographic_boundaries_active OR _station_boundaries_active THEN
        SELECT INTO boundary_mvt ST_AsMVT(tile, 'boundaries', 4096, 'geom')
        FROM (
            SELECT 
                ST_AsMVTGeom(
                    ST_Transform(
                        ST_CurveToLine(ST_GeomFromGeoJSON(ST_AsGeoJSON(coords))),
                        3857
                    ),
                    ST_TileEnvelope(z, x, y),
                    4096,
                    64,
                    true
                ) AS geom,  
                boundary.id AS boundary_id,
                boundary.name AS boundary_name,
                boundary.type as boundary_type,
                NULL as surface,
                NULL::json AS shrubs_data,
                NULL::json AS herbaceous_data,
                NULL::json AS tree_count,
                NULL::json AS afforestation_trees_data
            FROM 
                boundary
            WHERE 
                ST_Intersects(coords, ST_Transform(ST_TileEnvelope(z, x, y), 4326))
                AND (
                    (_geographic_boundaries_active IS TRUE AND boundary.type = 'geographic')
                    OR
                    (_station_boundaries_active IS TRUE AND boundary.type = 'station')
                )
        ) AS tile;
    END IF;
    
    -- Create MVT for vegetated areas
    IF _vegetated_areas_active THEN
        SELECT INTO vegetated_area_mvt ST_AsMVT(tile, 'vegetated_areas', 4096, 'geom')
        FROM (
            SELECT 
                ST_AsMVTGeom(
                    ST_Transform(
                        ST_CurveToLine(ST_GeomFromGeoJSON(ST_AsGeoJSON(veg.coords))),
                        3857
                    ),
                    ST_TileEnvelope(z, x, y),
                    4096,
                    64,
                    true
                ) AS geom,  
                veg.id AS vegetated_area_id,
                veg.type AS vegetated_area_type,
                ST_AsGeoJSON(veg.coords) AS vegetated_area_coords,
                veg.address as vegetated_area_address,
                veg.surface as vegetated_area_surface,
                veg.shrubs_data::json as vegetated_area_shrubs_data,
                veg.herbaceous_data::json as vegetated_area_herbaceous_data,
                 (
                    SELECT COUNT(*)
                    FROM vegetated_areas_locations val
                    JOIN location loc ON val.location_id = loc.id
                    JOIN location_status ls ON loc.id_status = ls.id
                    WHERE val.vegetated_area_id = veg.id AND ls.status = 'alive'
                ) AS vegetated_area_tree_count,
                veg.afforestation_trees_data::json as vegetated_area_afforestation_trees_data
            FROM 
                vegetated_area veg
            WHERE 
                ST_Intersects(veg.coords, ST_Transform(ST_TileEnvelope(z, x, y), 4326))
                AND (
                     (_vegetated_areas_ids IS NULL)
                    OR veg.id = ANY (_vegetated_areas_ids)
                )
        ) AS tile;
    END IF;
    
    -- Combine MVT layers
    mvt := COALESCE(boundary_mvt, '') || COALESCE(vegetated_area_mvt, '');
    
    RETURN mvt;
END;
$$;

-- Drop the old function
DROP FUNCTION IF EXISTS filter_boundaries(z integer, x integer, y integer, query_params json);
