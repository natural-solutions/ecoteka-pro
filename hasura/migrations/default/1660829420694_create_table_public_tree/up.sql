CREATE TABLE "public"."tree" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "location_id" uuid, PRIMARY KEY ("id") , FOREIGN KEY ("location_id") REFERENCES "public"."location"("id") ON UPDATE cascade ON DELETE cascade);
CREATE EXTENSION IF NOT EXISTS pgcrypto;
