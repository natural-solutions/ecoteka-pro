
CREATE TABLE "public"."foot_type" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "slug" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."foot_type" IS E'List of all of possible foot types of a location';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "public"."location_foot_type" ("location_id" uuid NOT NULL, "foot_type_id" uuid NOT NULL, "id" uuid NOT NULL DEFAULT gen_random_uuid(), PRIMARY KEY ("id") , FOREIGN KEY ("foot_type_id") REFERENCES "public"."foot_type"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("location_id") REFERENCES "public"."location"("id") ON UPDATE restrict ON DELETE restrict, UNIQUE ("id"));COMMENT ON TABLE "public"."location_foot_type" IS E'Join table between location and foot_type';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
