CREATE TABLE "public"."vegetated_areas_locations" (
    "id" uuid NOT NULL DEFAULT gen_random_uuid(),
    "vegetated_area_id" uuid NOT NULL,
    "location_id" uuid NOT NULL,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("vegetated_area_id") REFERENCES "public"."vegetated_area" ("id") ON UPDATE RESTRICT ON DELETE CASCADE,
    FOREIGN KEY ("location_id") REFERENCES "public"."location" ("id") ON UPDATE RESTRICT ON DELETE CASCADE,
    UNIQUE ("id")
);

COMMENT ON TABLE "public"."vegetated_areas_locations" IS 'Relation table between VegetatedAreas and Locations';

CREATE EXTENSION IF NOT EXISTS pgcrypto;
