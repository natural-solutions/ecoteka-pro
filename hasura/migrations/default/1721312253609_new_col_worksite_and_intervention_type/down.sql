
comment on column "public"."worksite"."location_status" is E'Table for managing worksite group';
alter table "public"."worksite" alter column "location_status" drop not null;
alter table "public"."worksite" add column "location_status" text;

ALTER TABLE "public"."intervention_type" DROP COLUMN "object_type";