CREATE OR replace FUNCTION filter_locations(
    z integer,
    x integer,
    y integer,
    query_params json
  ) RETURNS bytea LANGUAGE plpgsql IMMUTABLE STRICT PARALLEL SAFE as $$
DECLARE _scnames text [] := string_to_array(query_params->>'scientific_names', ',');
_vernacular_names text [] := string_to_array(query_params->>'vernacular_names', ',');
_plantation_date_start text := query_params->>'plantation_date_start';
_plantation_date_end text := query_params->>'plantation_date_end';
_tree_condition text [] := string_to_array(query_params->>'tree_condition', ',');
_tree_is_dangerous boolean := (query_params->>'tree_is_dangerous')::boolean;
_recommendation text [] := string_to_array(query_params->>'recommendation', ',');
_address text [] := string_to_array(query_params->>'address', ',');
_location_status text [] := string_to_array(query_params->>'location_status', ',');
_inventory_source jsonb := to_jsonb(string_to_array(query_params->>'inventory_source', ','));
_administrative_boundary text [] := string_to_array(query_params->>'administrative_boundary', ',');
_intervention_partner_id text [] := string_to_array(
  query_params->>'intervention_partner_id',
  ','
);
_intervention_type_id text [] := string_to_array(query_params->>'intervention_type_id', ',');
_scheduled_date_start text := query_params->>'scheduled_date_start';
_realized_date_end text := query_params->>'realized_date_end';
_scheduled_date_end text := query_params->>'scheduled_date_end';
_realized_date_start text := query_params->>'realized_date_start';
_intervention_status text := query_params->>'intervention_status';
_is_parking_banned boolean := (query_params->>'is_parking_banned')::boolean;
_has_urbasense boolean := (query_params->>'has_urbasense')::boolean;
_has_worksite boolean := (query_params->>'has_worksite')::boolean;
_location_id text := query_params->>'location_id';
_location_ids text[] := string_to_array(query_params->>'location_ids', ',')::uuid[];
mvt bytea;
begin
SELECT INTO mvt ST_AsMVT(tile, 'filter_locations', 4096, 'coords')
FROM (
    SELECT ST_AsMVTGeom(
        ST_Transform(
          ST_CurveToLine(location.coords),
          3857
        ),
        ST_TileEnvelope(z, x, y),
        4096,
        64,
        true
      ) AS coords,
      location.id as location_id,
      location.address as location_address,
      location_status.status as status_name,
      location_status.color as status_color,
      location.inventory_source as inventory_source,
      ST_AsGeoJSON(location.coords) AS location_coords,
      tree.id as tree_id,
      tree.height as tree_height,
      tree.scientific_name as tree_scientific_name,
      tree.vernacular_name as tree_vernacular_name,
      tree.serial_number as tree_serial_number,
      tree.plantation_date as plantation_date,
      diagnosis.tree_condition as tree_condition,
      diagnosis.tree_is_dangerous as tree_is_dangerous,
      diagnosis.recommendation as recommendation,
      intervention.scheduled_date as scheduled_intervention,
      intervention.realization_date as realized_intervention,
      intervention.intervention_partner_id as intervention_partner_id,
      intervention.intervention_type_id as intervention_type_id,
      bl.boundary_id as boundary_location_id
    FROM location
      LEFT JOIN tree ON location.id = tree.location_id
      LEFT JOIN diagnosis ON tree.id = diagnosis.tree_id
      LEFT JOIN intervention ON tree.id = intervention.tree_id
      LEFT JOIN intervention_partner ON intervention.intervention_partner_id = intervention_partner.id
      LEFT JOIN intervention_type ON intervention.intervention_type_id = intervention_type.id
      LEFT JOIN worksite on worksite.id = intervention.worksite_id
      LEFT JOIN boundaries_locations bl on bl.location_id = location.id
      LEFT JOIN boundary b on b.id = bl.boundary_id
      RIGHT JOIN location_status ON location.id_status = location_status.id
    WHERE location.coords && ST_Transform(
        ST_TileEnvelope(z, x, y),
        4326
      )
      AND (
        (_location_id IS NULL)
        OR location.id = _location_id::uuid
      )
      AND (
        (_location_ids IS NULL)
        OR location.id = ANY (_location_ids::uuid [])
      )
      AND (
        (
          _has_urbasense IS FALSE
          OR _has_urbasense IS NULL
        )
        OR (
          _has_urbasense IS TRUE
          AND (
            tree.urbasense_subject_id IS NOT NULL
            AND tree.urbasense_subject_id != ''
          )
        )
      )
      AND (
        (
          _has_worksite IS FALSE
          OR _has_worksite IS NULL
        )
        OR (
          _has_worksite IS TRUE
          AND (
          worksite.id IS NOT NULL
          )
          AND (
            worksite.realization_date is NULL
          )
        )
      )
      AND (
        array_length(_scnames, 1) IS NULL
        OR tree.scientific_name ilike any (_scnames)
      )
      AND (
        array_length(_tree_condition, 1) IS NULL
        OR diagnosis.tree_condition ilike any (_tree_condition)
      )
      AND (
      (
      _tree_is_dangerous IS FALSE OR _tree_is_dangerous IS NULL
      )
      OR (
        _tree_is_dangerous IS TRUE
        AND (
        diagnosis.tree_is_dangerous IS TRUE
        )
      )
      )
      AND (
        array_length(_recommendation, 1) IS NULL
        OR diagnosis.recommendation ilike any (_recommendation)
      )
      AND (
        array_length(_vernacular_names, 1) IS NULL
        OR tree.vernacular_name ilike any (_vernacular_names)
      )
      AND (
        array_length(_address, 1) IS NULL
        OR location.address ilike any (_address)
      )
      AND (
        array_length(_location_status, 1) IS NULL
        OR location.id_status::text ilike any (_location_status)
      )
      AND (
        _inventory_source IS NULL
        OR _inventory_source = '[]'::jsonb
        OR (
            _inventory_source IS NOT NULL
            AND location.inventory_source IS NOT NULL
            AND location.inventory_source @> _inventory_source
            )
      )
      AND (
        array_length(_intervention_partner_id, 1) IS NULL
        OR intervention.intervention_partner_id = ANY (
          _intervention_partner_id::uuid []
        )
      )
      AND (
        array_length(_administrative_boundary, 1) IS NULL
        OR bl.boundary_id = ANY (
          _administrative_boundary::uuid []
        )
      )
      AND (
        array_length(_intervention_type_id, 1) IS NULL
        OR intervention.intervention_type_id = ANY (_intervention_type_id::uuid [])
      )
      AND (
        (
          _plantation_date_start IS NULL
          AND _plantation_date_end IS NULL
        )
        OR (
          tree.plantation_date::text = _plantation_date_start::text
          AND _plantation_date_end IS NULL
        )
        OR (
          _plantation_date_start IS NOT NULL
          AND _plantation_date_end IS NOT NULL
          AND tree.plantation_date::text BETWEEN _plantation_date_start::text AND _plantation_date_end::text
        )
      )
      AND (
        (
          _scheduled_date_start IS NULL
          AND _scheduled_date_end IS NULL
        )
        OR (
          intervention.scheduled_date::date = _scheduled_date_start::date
          AND _scheduled_date_end IS NULL
        )
        OR (
          _scheduled_date_start IS NOT NULL
          AND _scheduled_date_end IS NOT NULL
          AND intervention.scheduled_date::date BETWEEN _scheduled_date_start::date AND _scheduled_date_end::date
        )
      )
      AND (
        (
          _realized_date_start IS NULL
          AND _realized_date_end IS NULL
        )
        OR (
          intervention.realization_date::date = _realized_date_start::date
          AND _realized_date_end IS NULL
        )
        OR (
          _realized_date_start IS NOT NULL
          AND _realized_date_end IS NOT NULL
          AND intervention.realization_date::date BETWEEN _realized_date_start::date AND _realized_date_end::date
        )
      )
      AND (
      (
      _is_parking_banned IS FALSE OR _is_parking_banned IS NULL
      )
      OR (
        _is_parking_banned IS TRUE
        AND intervention.is_parking_banned IS TRUE
      )
      )
      AND (
        (_intervention_status IS NULL)
        OR (
          _intervention_status = 'realized'
          AND intervention.realization_date IS NOT NULL
        )
        OR (
          _intervention_status = 'late'
          AND intervention.scheduled_date < CURRENT_DATE
          and intervention.realization_date IS NULL
        )
        OR (
          _intervention_status = 'toCome'
          AND intervention.scheduled_date > CURRENT_DATE
          and intervention.realization_date IS NULL
        )
      )
  ) as tile
WHERE tile.coords IS NOT NULL;
RETURN mvt;
end;
$$;
