INSERT INTO public.family_intervention_type (slug, color) VALUES ('planting', '#61D878');
INSERT INTO public.family_intervention_type (slug, color) VALUES ('pruning', '#EDE408');
INSERT INTO public.family_intervention_type (slug, color) VALUES ('felling', '#FF4C51');
INSERT INTO public.family_intervention_type (slug, color) VALUES ('diagnosis', '#9155FD');
INSERT INTO public.family_intervention_type (slug, color) VALUES ('maintenance', '#16B1FF');

UPDATE intervention_type it
SET family_intervention_type_id = fit.id
FROM family_intervention_type fit
WHERE it.slug = fit.slug
  OR (
    it.slug IN ('watering', 'treatment', 'follow_up_planting')
    AND fit.slug = 'maintenance'
  )
  OR (
    it.slug IN ('visual_diagnosis', 'detailed_diagnosis')
    AND fit.slug = 'diagnosis'
  )
  OR (
    it.slug IN ('felling', 'grubbing')
    AND fit.slug = 'felling'
  )
  OR (
    it.slug IN ('dead_wood', 'trim', 'carpenters', 'pruning')
    AND fit.slug = 'pruning'
  )
  OR (
    it.slug = 'planting'
    AND fit.slug = 'planting'
  );
COMMIT;
END;