
alter table "public"."urban_site" rename column "label" to "slug";

alter table "public"."pathogen" rename column "label" to "slug";

alter table "public"."analysis_tool" rename column "label" to "slug";
