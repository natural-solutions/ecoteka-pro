
alter table "public"."worksite" add column "scheduled_start_date" date
 null;

alter table "public"."worksite" add column "scheduled_end_date" date
 null;
UPDATE "public"."worksite"
SET "scheduled_start_date" = "scheduled_date",
    "scheduled_end_date" = "scheduled_date";

ALTER TABLE "public"."worksite" DROP COLUMN "scheduled_date";
