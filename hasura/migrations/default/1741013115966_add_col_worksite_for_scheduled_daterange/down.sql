ALTER TABLE "public"."worksite" ADD COLUMN "scheduled_date" DATE NULL;

UPDATE "public"."worksite"
SET "scheduled_date" = "scheduled_start_date";

ALTER TABLE "public"."worksite" DROP COLUMN "scheduled_start_date";
ALTER TABLE "public"."worksite" DROP COLUMN "scheduled_end_date";
