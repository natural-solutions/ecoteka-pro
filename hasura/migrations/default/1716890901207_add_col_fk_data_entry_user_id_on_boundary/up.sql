
alter table "public"."boundary" add column "data_entry_user_id" text
 null;

alter table "public"."boundary"
  add constraint "boundary_data_entry_user_id_fkey"
  foreign key ("data_entry_user_id")
  references "public"."user_entity"
  ("id") on update restrict on delete restrict;
