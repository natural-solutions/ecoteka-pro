
alter table "public"."vegetated_area" add column "linear_meters" numeric
 null;

alter table "public"."vegetated_area" add column "hedge_type" Text
 null;

alter table "public"."vegetated_area" add column "afforestation_trees_data" jsonb
 null;
