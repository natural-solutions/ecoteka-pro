alter table "public"."location"
  add constraint "location_id_status_fkey"
  foreign key ("id_status")
  references "public"."location_status"
  ("id") on update cascade on delete cascade;
