
alter table "public"."intervention_partner" add column "user_id" varchar
 null unique;

alter table "public"."intervention_partner" add constraint "intervention_partner_name_key" unique ("name");
