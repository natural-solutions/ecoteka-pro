CREATE TABLE
    "public"."location_status" (
        "id" uuid NOT NULL DEFAULT gen_random_uuid (),
        "status" text NOT NULL,
        PRIMARY KEY ("id"),
        UNIQUE ("status"),
        "color" text null default '[6, 182, 174, 200]'
    );

CREATE EXTENSION IF NOT EXISTS pgcrypto;

INSERT INTO
    "public"."location_status" ("id", "status", "color")
VALUES
    (
        'bf6fcfe3-fbe9-4467-98be-a935efcf67a3',
        'alive',
        '[47, 163, 124, 220]'
    );

INSERT INTO
    "public"."location_status" ("id", "status", "color")
VALUES
    (
        'd223091c-4931-47a0-8ebc-44c99a1663ed',
        'stump',
        '[150, 97, 51, 200]'
    );

INSERT INTO
    "public"."location_status" ("id", "status", "color")
VALUES
    (
        'b2fbee37-c6a6-4431-9c70-f361ade578b2',
        'empty',
        '[243, 237, 232, 200]'
    );