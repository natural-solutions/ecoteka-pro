alter table "public"."boundaries_locations" drop constraint "boundaries_locations_boundary_id_fkey",
  add constraint "boundaries_locations_boundary_id_fkey"
  foreign key ("boundary_id")
  references "public"."boundary"
  ("id") on update restrict on delete restrict;
