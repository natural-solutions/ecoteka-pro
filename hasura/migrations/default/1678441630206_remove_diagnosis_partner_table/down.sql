
-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- DROP table "public"."diagnosis_partner";

alter table "public"."diagnosis" alter column "diagnosis_partner_id" drop not null;
alter table "public"."diagnosis" add column "diagnosis_partner_id" uuid;

alter table "public"."diagnosis"
  add constraint "diagnosis_diagnosis_partner_id_fkey"
  foreign key ("diagnosis_partner_id")
  references "public"."diagnosis_partner"
  ("id") on update restrict on delete restrict;
