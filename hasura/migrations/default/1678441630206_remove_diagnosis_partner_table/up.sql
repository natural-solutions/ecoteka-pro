
alter table "public"."diagnosis" drop constraint "diagnosis_diagnosis_partner_id_fkey";

alter table "public"."diagnosis" drop column "diagnosis_partner_id" cascade;

DROP table "public"."diagnosis_partner";
