-- Down Migration

-- Revert the change made in the up migration to alter the data type of the "height" column.
-- Change the data type of the "height" column back to TEXT.

ALTER TABLE "public"."tree"
ALTER COLUMN "height" TYPE text;
