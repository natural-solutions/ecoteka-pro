CREATE TABLE "public"."family_intervention_type" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "slug" text NOT NULL, "color" text NULL, PRIMARY KEY ("id") , UNIQUE ("slug"), UNIQUE ("id"));
