
CREATE TABLE "public"."pathogen" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "label" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));COMMENT ON TABLE "public"."pathogen" IS E'List of all of possible pathogens of a diagnosis';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "public"."diagnosis_pathogen" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "diagnosis_id" uuid NOT NULL, "pathogen_id" uuid NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("diagnosis_id") REFERENCES "public"."diagnosis"("id") ON UPDATE restrict ON DELETE restrict, FOREIGN KEY ("pathogen_id") REFERENCES "public"."pathogen"("id") ON UPDATE restrict ON DELETE restrict);COMMENT ON TABLE "public"."diagnosis_pathogen" IS E'Join table between diagnosis and pathogen';
CREATE EXTENSION IF NOT EXISTS pgcrypto;
