alter table "public"."worksite_location" drop constraint "worksite_location_worksite_id_fkey",
  add constraint "worksite_location_worksite_id_fkey"
  foreign key ("worksite_id")
  references "public"."worksite"
  ("id") on update restrict on delete restrict;
