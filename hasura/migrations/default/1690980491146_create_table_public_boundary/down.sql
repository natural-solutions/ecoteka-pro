
alter table "public"."boundary" drop constraint "boundary_geographic_boundary_id_fkey";

alter table "public"."boundary" drop constraint "boundary_administrative_boundary_id_fkey";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."boundary" add column "geographic_boundary_id" uuid
--  null;

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "public"."boundary" add column "administrative_boundary_id" uuid
--  null;

DROP TABLE "public"."boundary";
