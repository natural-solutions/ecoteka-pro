
CREATE TABLE "public"."boundary" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), "name" text NOT NULL, "type" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("id"));
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_boundary_updated_at"
BEFORE UPDATE ON "public"."boundary"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_boundary_updated_at" ON "public"."boundary"
IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "public"."boundary" add column "administrative_boundary_id" uuid
 null;

alter table "public"."boundary" add column "geographic_boundary_id" uuid
 null;

alter table "public"."boundary"
  add constraint "boundary_administrative_boundary_id_fkey"
  foreign key ("administrative_boundary_id")
  references "public"."boundary"
  ("id") on update restrict on delete restrict;

alter table "public"."boundary"
  add constraint "boundary_geographic_boundary_id_fkey"
  foreign key ("geographic_boundary_id")
  references "public"."boundary"
  ("id") on update restrict on delete restrict;
