
alter table "public"."tree" add column "infos" jsonb
 null;

alter table "public"."tree" add column "diameter" numeric
 null;

alter table "public"."tree" add column "diameter_is_estimated" boolean
 null;

alter table "public"."tree" add column "stage" text
 null;

alter table "public"."tree" add column "crown_diameter" numeric
 null;

alter table "public"."tree" add column "crown_diameter_is_estimated" boolean
 null;

alter table "public"."tree" add column "height_is_estimated" boolean
 null;
