
alter table "public"."location" add column "landscape_type" text
 null;

alter table "public"."location" add column "urban_site_type" text
 null;

alter table "public"."location" add column "sidewalk_type" text
 null;

alter table "public"."location" add column "plantation_type" text
 null;

alter table "public"."location" add column "is_protected" boolean
 null;
