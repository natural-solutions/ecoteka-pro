alter table "public"."worksite_intervention" drop constraint "worksite_intervention_worksite_id_fkey",
  add constraint "worksite_intervention_worksite_id_fkey"
  foreign key ("worksite_id")
  references "public"."worksite"
  ("id") on update restrict on delete restrict;
