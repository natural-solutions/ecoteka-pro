# 1. Install dependencies only when needed
FROM node:lts-alpine AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat 

WORKDIR /app
COPY package.json ./
RUN yarn install --frozen-lockfile

# 2. Rebuild the source code only when needed
FROM node:lts-alpine AS development
RUN apk add --no-cache curl
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .
# This will do the trick, use the corresponding env file for each environment.
ENV NEXT_TELEMETRY_DISABLED=1
RUN yarn build

EXPOSE 3000

CMD ["yarn", "dev"]

# 3. Production image, copy all the files and run next
FROM development AS production

CMD ["yarn", "start"]