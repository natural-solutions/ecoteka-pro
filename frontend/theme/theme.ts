import { PaletteMode } from "@mui/material";
import { TypeBackground } from "@mui/material/styles";
import { ThemeOptions } from "@mui/material/styles/createTheme";

declare module "@mui/material/styles" {
  interface EcotekaBackground extends TypeBackground {
    dark: string;
    shade: string;
  }
  interface Palette {
    toolbar: string;
    background: TypeBackground;
  }

  interface PaletteOptions {
    toolbar?: string;
  }
  interface Theme {
    palette: Palette;
  }
  // allow configuration using `createTheme`
  interface ThemeOptions {}
}

// A custom theme for this app
export const getDesignTokens = (mode: PaletteMode): ThemeOptions => ({
  palette: {
    mode,
    ...(mode === "light"
      ? {
          background: {
            default: "#fbfdfe",
            paper: "#fff",
            // dark: "#384145",
            // shade: "#2D363A",
          },
          text: {
            primary: "#434A4A",
            secondary: "#7E8989",
            // hint: "#131313",
            disabled: "#ABB5B5",
          },
          primary: {
            light: "#D5DEDB",
            main: "#2FA37C",
            dark: "#1D7554",
            contrastText: "#fff",
          },
          secondary: {
            light: "#EFEBEE",
            main: "#B07462",
            dark: "#6E3931",
            contrastText: "#fff",
          },
          toolbar: "#fbfdfe",
        }
      : {
          background: {
            default: "#384145",
            paper: "#424242",
            // dark: "#384145",
            // shade: "#2D363A",
          },
          text: {
            primary: "#ffffff",
            secondary: "#ABB5B5",
            // hint: "#131313",
            disabled: "#7E8989",
          },
          primary: {
            light: "#E2F4EF",
            main: "#8BD3BB",
            dark: "#41B28E",
            contrastText: "#fff",
          },
          secondary: {
            light: "#FFE7D7",
            main: "#DEAC9D",
            dark: "#C48D7C",
            contrastText: "#434A4A",
          },
          toolbar: "#384145",
        }),
  },
  typography: {
    fontFamily: "Poppins, Arial, sans-serif",
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 700,
    h1: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 900,
      fontSize: "3rem",
      lineHeight: 1.167,
      letterSpacing: "0px",
    },
    h2: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 900,
      fontSize: "2.5rem",
      lineHeight: 1.2,
      letterSpacing: "0em",
    },
    h3: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 700,
      fontSize: "2rem",
      lineHeight: 1.167,
      letterSpacing: "0em",
    },
    h4: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 700,
      fontSize: "2.125rem",
      lineHeight: 1.235,
      letterSpacing: "0em",
    },
    h5: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 600,
      fontSize: "1.5rem",
      lineHeight: 1.334,
      letterSpacing: "0em",
      color: "#3A3541DE",
    },
    h6: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 500,
      fontSize: "1.125rem",
      lineHeight: 1.6,
      letterSpacing: "0em",
    },
    subtitle1: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 600,
      fontSize: "1rem",
      lineHeight: 1.75,
      letterSpacing: "0em",
    },
    subtitle2: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 500,
      fontSize: "0.875rem",
      lineHeight: 1.57,
      letterSpacing: "0em",
    },
    body1: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 400,
      fontSize: "0.875rem",
      lineHeight: 1.5,
      letterSpacing: "0.00938em",
    },
    body2: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 400,
      fontSize: "0.75rem",
      lineHeight: 1.43,
      letterSpacing: "0.01071em",
    },
    button: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 500,
      fontSize: "0.875rem",
      lineHeight: 1.75,
      letterSpacing: "0.02857em",
    },
    caption: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 400,
      fontSize: "0.75rem",
      lineHeight: 1.66,
      letterSpacing: "0.025em",
    },
    overline: {
      fontFamily: "Poppins, Arial, sans-serif",
      fontWeight: 400,
      fontSize: "0.75rem",
      lineHeight: 2.66,
      letterSpacing: "0.08333em",
    },
  },
  components: {
    MuiLink: {
      styleOverrides: {
        root: {
          my: 2,
          display: "flex",
          fontFamily: "Poppins, Arial, sans-serif",
          alignItems: "center",
          gap: "8px",
          textTransform: "capitalize",
          fontSize: "16px",
          px: "32px",
          margin: "0",
          textDecoration: "none",
          border: "solid 2px transparent",
          borderBottom: `2px solid #2FA37C`,
          color: "#434A4A",
          fontWeight: 400,
          "&:hover": {
            color: "#2FA37C",
            fontWeight: "500",
            borderBottomColor: "#2FA37C",
          },
        },
      },
    },
    MuiButton: {
      defaultProps: {
        disableElevation: true,
      },
      variants: [
        {
          props: { variant: "text", color: "primary" },
          style: {
            fontFamily: "Poppins, Arial, sans-serif",
            backgroundColor: "#e5eff0",
            color: "#2fa37c",
            "&:hover": {
              background: "#b1dbcf",
            },
          },
        },
        {
          props: { variant: "contained", color: "inherit" },
          style: {
            color: "#434A4A",
            backgroundColor: "#fff",
            display: "flex",
            fontFamily: "Poppins, Arial, sans-serif",
            alignItems: "center",
            textTransform: "uppercase",
            borderRadius: "6px",
            fontWeight: "500",
            fontSize: "15px",
          },
        },
        {
          props: { variant: "contained", color: "primary" },
          style: {
            fontFamily: "Poppins, Arial, sans-serif",
            backgroundColor: "#2FA37C",
            color: "#fff",
          },
        },
        {
          props: { variant: "text", color: "error" },
          style: {
            backgroundColor: "#ffebef",
            fontFamily: "Poppins, Arial, sans-serif",
            color: "#F44336",
            "&:hover": {
              background: "#fcc7d2",
            },
          },
        },
        {
          props: { variant: "text", color: "inherit" },
          style: {
            color: "#434A4A",
            display: "flex",
            fontFamily: "Poppins, Arial, sans-serif",
            alignItems: "center",
            textTransform: "capitalize",
            fontWeight: "500",
            fontSize: "15px",
          },
        },
        {
          props: { variant: "contained", color: "inherit" },
          style: {
            fontFamily: "Poppins, Arial, sans-serif",
            backgroundColor: "#EFEBEE",
            color: "#B07462",
            textTransform: "none",
            borderRadius: 50,
          },
        },
      ],
      styleOverrides: {
        root: ({ ownerState }) => ({
          borderRadius: 50,
          textTransform: "none",
          ...(ownerState.variant === "contained" &&
            ownerState.color === "secondary" && {
              // backgroundColor: "#202020",
              // color: "#fff",
              boxShadow: "none",
            }),
        }),
      },
    },
    MuiCard: {
      styleOverrides: {
        root: {
          boxShadow:
            "0px 6px 16px -4px rgba(58, 53, 65, 0.10), 0px 2px 12px -4px rgba(58, 53, 65, 0.08), 0px 2px 12px -4px rgba(58, 53, 65, 0.10);",
        },
      },
    },
    MuiFilledInput: {
      styleOverrides: {
        root: {
          backgroundColor: "#FAFAFA",
          "&.Mui-disabled": {
            backgroundColor: "#FAFAFA",
            ".MuiInputBase-input": {
              WebkitTextFillColor: "#434A4A",
            },
          },
          "&.Mui-disabled:before": {
            border: "none",
          },
        },
      },
    },
  },
});
