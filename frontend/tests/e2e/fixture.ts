import { expect, Page } from "@playwright/test";

export const login = async (page: Page, login: string, password: string) => {
  // Go to http://mutatec.localdomain:8888/
  await page.goto("http://ecoteka.localdomain:8888/");
  // Click input[name="username"]
  await page.locator('input[name="username"]').click();
  // Fill input[name="username"]
  await page.locator('input[name="username"]').fill(login);
  // Press Tab
  await page.locator('input[name="username"]').press("Tab");
  // Fill input[name="password"]
  await page.locator('input[name="password"]').fill(password);
  // Press Enter
  await page.locator('input[name="password"]').press("Enter");
  await expect(page).toHaveURL("http://mutatec.localdomain:8888/");
};
