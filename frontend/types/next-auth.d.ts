import NextAuth from "next-auth";

declare module "next-auth" {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session {
    id: string;
    idToken: string;
    user: {
      /** The user's id. */
      id?: string;
      name?: string | null;
      email?: string | null;
      preferred_username?: string | null;
    };
    roles?: string[];
    accessToken?: string;
    error?: any;
  }

  interface DefaultSession {
    id: string;
    idToken: string;
    user: {
      /** The user's id. */
      id?: string;
      name?: string | null;
      email?: string | null;
    };
    roles?: string[];
    accessToken?: string;
    error?: any;
  }

  interface Account {
    provider: string;
    type: string;
    id: string;
    accessToken: string;
    accessTokenExpires?: any;
    refreshToken: string;
    idToken: string;
    access_token: string;
    expires_in: number;
    refresh_expires_in: number;
    refresh_token: string;
    token_type: string;
    id_token: string;
    "not-before-policy": number;
    session_state: string;
    scope: string;
  }

  interface Profile {
    exp: number;
    iat: number;
    id?: string;
    auth_time: number;
    jti: string;
    iss: string;
    aud: string;
    sub: string;
    typ: string;
    azp: string;
    session_state: string;
    at_hash: string;
    sid: string;
    email_verified: boolean;
    name: string;
    preferred_username: string;
    given_name: string;
    family_name: string;
    email: string;
    "x-hasura-user-id": string;
    "x-hasura-default-role": string;
    "x-hasura-allowed-roles": string[];
  }
}

declare module "next-auth/jwt" {
  /** Returned by the `jwt` callback and `getToken`, when using JWT sessions */
  interface JWT {
    idToken?: string;
    user?: Session["user"];
    accessToken?: string;
    accessTokenExpires: number;
    refreshToken?: string;
    roles?: string[];
    error?: any;
  }
}
