interface Point {
  type: "Point";
  crs: {
    type: "name";
    properties: {
      name: string;
    };
  };
  coordinates: [number, number];
}

export interface CarbonStorageData {
  carbon_range: string;
  tree_count: number;
}

export interface TaxonomyNode {
  children?: TaxonomyNode[];
  color: string;
  count?: number; // You mentioned count in the console log, but it's not present in the provided data
  name: string;
  percentage: number;
  size: number;
}

export type TaxonomyData = TaxonomyNode[];

export interface EcosystemData {
  air_quality_score: { air_score: number }[];
  carbon_storage_score: {
    data: CarbonStorageData[];
    metadata: {
      total_tree_count: number;
      tree_count: number;
    };
  };
  biodiversity_score: { biodiversity_score: number }[];
  icu_score: { icu_score: number }[];
}

export interface OrganizationData {
  id: string;
  name: string;
  population: number;
  surface: number;
  default: boolean;
  image: string;
  bucket: string;
  domain: string;
  coords: Point;
  __typename: string;
}

export interface LateIntervention {
  family_name: string;
  family_color: string;
  intervention_type: string;
  intervention_id: string;
  scheduled_date: string;
  tree_id: string;
  intervention_type_id: string;
}

export interface Boundary {
  id: string;
  name: string;
  type: string;
  __typename: string;
}

export interface BoundariesData {
  boundary: Boundary[];
}

export interface Diagnosis {
  id: string;
  tree_is_dangerous: boolean;
  __typename: "diagnosis";
}

export interface HealthScore {
  population: number;
  num_trees_no_felling_date: number;
}

export interface HealthData {
  health_score: HealthScore;
  non_allergenic_score: number;
}

export interface ResilienceData {
  vulnerability_score: number;
}

export interface NotableTree {
  id: string;
  __typename: string;
}

export interface EcosystemMetrics {
  air_quality_score: Array<{
    air_score: number;
  }>;
  carbon_storage_score: {
    data: Array<{
      carbon_range: string;
      tree_count: number;
    }>;
    metadata: {
      tree_count: number;
      total_tree_count: number;
    };
  };
  biodiversity_score: Array<{
    biodiversity_score: number;
  }>;
  icu_score: Array<{
    icu_score: number;
  }>;
}

export type QueryParams = {
  id?: number | null;
  provider: string;
  type: string;
  ids: number[];
};

export type MetricMessage = {
  badge?: string | null;
  nom?: string | null;
  tooltip?: string | null;
  unite?: string | null;
  valeur?: number | null;
  dates?: string[];
  abbrege?: string | null;
};


export type DendroMessage = {
  croissanceMoyMois: MetricMessage;
  amcMoyJour: MetricMessage;
  stressThermMoyJour: MetricMessage;
};

export type MeteoMessage = {
  tempAirMax: MetricMessage;
  humAirMoy: MetricMessage;
  vitVentMax: MetricMessage;
  utci: MetricMessage;
  listeParams?: string[];
  nbSujets: MetricMessage;
};

export type TensioMessage = {
  nbSujets: {
    valeur: number;
  };
  arrosageDernier: MetricMessage;
  arrosageProchain: MetricMessage;
  rurMax: {
    valeur: number;
    nom: string;
    tooltip: string;
  };
  rurMin: {
    valeur: number;
    nom: string;
    tooltip: string;
  };
};

export type SectionError = {
  error: boolean;
};

export type Section<T> = SectionError & {
  msg: T;
};

export type UrbasenseMetricsByGroupResponse = {
  dendro: Section<DendroMessage>;
  meteo: Section<MeteoMessage>;
  tensio: Section<TensioMessage>;
};

export type GetUrbasenseMetricsByGroupQueryResult = {
  urbasenseGetMetricsByGroup: UrbasenseMetricsByGroupResponse;
};
