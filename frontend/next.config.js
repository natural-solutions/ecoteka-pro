module.exports = {
  i18n: {
    locales: ["en", "fr", "es"],
    defaultLocale: "fr",
  },
  reactStrictMode: true,
  swcMinify: true,
  publicRuntimeConfig: {
    NEXTAUTH_URL: process.env.NEXTAUTH_URL,
    GRAPHQL_API_URL: process.env.GRAPHQL_API_URL,
    KEYCLOAK_CLIENT_ID: process.env.KEYCLOAK_CLIENT_ID,
    KEYCLOAK_ISSUER: process.env.KEYCLOAK_ISSUER,
    NEXT_PUBLIC_FRONT_URL: process.env.NEXT_PUBLIC_FRONT_URL,
    NEXT_PUBLIC_TILESERVICE_URL: process.env.NEXT_PUBLIC_TILESERVICE_URL,
    URBASENSE_DASHBOARD_ACTIVE: process.env.URBASENSE_DASHBOARD_ACTIVE,
    ALLOW_ALPHA_MODE: process.env.ALLOW_ALPHA_MODE,
    DEFAULT_STORAGE_BUCKET_NAME: process.env.MINIO_BUCKET_NAME,
  },
  serverRuntimeConfig: {
    KEYCLOAK_CLIENT_SECRET: process.env.KEYCLOAK_CLIENT_SECRET,
    DEFAULT_STORAGE_BUCKET_NAME: process.env.MINIO_BUCKET_NAME,
    DEFAULT_STORAGE_ACCESS_KEY: process.env.MINIO_ROOT_USER,
    DEFAULT_STORAGE_ACCESS_KEY_SECRET: process.env.MINIO_ROOT_PASSWORD,
  },
  images: {
    domains: ["ecoteka.localdomain"],
  },
};
