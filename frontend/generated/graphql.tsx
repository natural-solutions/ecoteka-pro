import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  bigint: any;
  date: any;
  geography: any;
  geometry: any;
  jsonb: any;
  numeric: number;
  timestamptz: string;
  uuid: any;
};

/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['Boolean']>;
  _gt?: InputMaybe<Scalars['Boolean']>;
  _gte?: InputMaybe<Scalars['Boolean']>;
  _in?: InputMaybe<Array<Scalars['Boolean']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['Boolean']>;
  _lte?: InputMaybe<Scalars['Boolean']>;
  _neq?: InputMaybe<Scalars['Boolean']>;
  _nin?: InputMaybe<Array<Scalars['Boolean']>>;
};

export type ComboOutput = {
  __typename?: 'ComboOutput';
  data?: Maybe<Array<Maybe<Data>>>;
  metadata?: Maybe<Array<Maybe<Metadata>>>;
};

export type Data = {
  __typename?: 'Data';
  S_1?: Maybe<Scalars['Int']>;
  S_2?: Maybe<Scalars['Int']>;
  S_3?: Maybe<Scalars['Int']>;
  S_t?: Maybe<Scalars['Int']>;
  date_mes?: Maybe<Scalars['String']>;
  pp_mmjour?: Maybe<Scalars['String']>;
  tair_max?: Maybe<Scalars['String']>;
};

export type GroupOutput = {
  __typename?: 'GroupOutput';
  id_grp: Scalars['Int'];
  nb_suj?: Maybe<Scalars['Int']>;
  nom?: Maybe<Scalars['String']>;
  type_arro?: Maybe<Scalars['String']>;
};

/** Boolean expression to compare columns of type "Int". All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['Int']>;
  _gt?: InputMaybe<Scalars['Int']>;
  _gte?: InputMaybe<Scalars['Int']>;
  _in?: InputMaybe<Array<Scalars['Int']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['Int']>;
  _lte?: InputMaybe<Scalars['Int']>;
  _neq?: InputMaybe<Scalars['Int']>;
  _nin?: InputMaybe<Array<Scalars['Int']>>;
};

export type MainOutput = {
  __typename?: 'MainOutput';
  meteo?: Maybe<Array<Maybe<SubjectMeteo>>>;
  temperature?: Maybe<Array<Maybe<SubjectTemperature>>>;
  tensio?: Maybe<Array<Maybe<SubjectTensio>>>;
};

export type Metadata = {
  __typename?: 'Metadata';
  alias?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  unite?: Maybe<Scalars['String']>;
};

export type SiteInput = {
  site_id: Scalars['Int'];
};

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['String']>;
  _gt?: InputMaybe<Scalars['String']>;
  _gte?: InputMaybe<Scalars['String']>;
  /** does the column match the given case-insensitive pattern */
  _ilike?: InputMaybe<Scalars['String']>;
  _in?: InputMaybe<Array<Scalars['String']>>;
  /** does the column match the given POSIX regular expression, case insensitive */
  _iregex?: InputMaybe<Scalars['String']>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  /** does the column match the given pattern */
  _like?: InputMaybe<Scalars['String']>;
  _lt?: InputMaybe<Scalars['String']>;
  _lte?: InputMaybe<Scalars['String']>;
  _neq?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given case-insensitive pattern */
  _nilike?: InputMaybe<Scalars['String']>;
  _nin?: InputMaybe<Array<Scalars['String']>>;
  /** does the column NOT match the given POSIX regular expression, case insensitive */
  _niregex?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given pattern */
  _nlike?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given POSIX regular expression, case sensitive */
  _nregex?: InputMaybe<Scalars['String']>;
  /** does the column NOT match the given SQL regular expression */
  _nsimilar?: InputMaybe<Scalars['String']>;
  /** does the column match the given POSIX regular expression, case sensitive */
  _regex?: InputMaybe<Scalars['String']>;
  /** does the column match the given SQL regular expression */
  _similar?: InputMaybe<Scalars['String']>;
};

export type SubjectMeteo = {
  __typename?: 'SubjectMeteo';
  date_mes?: Maybe<Scalars['String']>;
  pp_mmjour?: Maybe<Scalars['String']>;
  tair_max?: Maybe<Scalars['String']>;
};

export type SubjectTemperature = {
  __typename?: 'SubjectTemperature';
  Date?: Maybe<Scalars['String']>;
  series?: Maybe<Scalars['String']>;
  series_name?: Maybe<Scalars['String']>;
  unite?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Int']>;
};

export type SubjectTensio = {
  __typename?: 'SubjectTensio';
  Date?: Maybe<Scalars['String']>;
  series?: Maybe<Scalars['String']>;
  series_name?: Maybe<Scalars['String']>;
  unite?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Int']>;
};

export type TaxonHit = {
  __typename?: 'TaxonHit';
  accepted?: Maybe<Scalars['String']>;
  accepted_key?: Maybe<Scalars['Int']>;
  authorship?: Maybe<Scalars['String']>;
  basionym?: Maybe<Scalars['String']>;
  basionym_key?: Maybe<Scalars['Int']>;
  canonical_name?: Maybe<Scalars['String']>;
  class?: Maybe<Scalars['String']>;
  class_key?: Maybe<Scalars['Int']>;
  constituent_key?: Maybe<Scalars['String']>;
  dataset_key?: Maybe<Scalars['String']>;
  family?: Maybe<Scalars['String']>;
  family_key?: Maybe<Scalars['Int']>;
  genus?: Maybe<Scalars['String']>;
  genus_key?: Maybe<Scalars['Int']>;
  id: Scalars['Int'];
  key?: Maybe<Scalars['Int']>;
  kingdom?: Maybe<Scalars['String']>;
  kingdom_key?: Maybe<Scalars['Int']>;
  name_key?: Maybe<Scalars['Int']>;
  name_type?: Maybe<Scalars['String']>;
  nub_key?: Maybe<Scalars['Int']>;
  num_descendants?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['String']>;
  order_key?: Maybe<Scalars['Int']>;
  origin?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['String']>;
  parent_key?: Maybe<Scalars['Int']>;
  phylum?: Maybe<Scalars['String']>;
  phylum_key?: Maybe<Scalars['Int']>;
  published_in?: Maybe<Scalars['String']>;
  rank?: Maybe<Scalars['String']>;
  scientific_name?: Maybe<Scalars['String']>;
  source_taxon_key?: Maybe<Scalars['Int']>;
  species?: Maybe<Scalars['String']>;
  species_key?: Maybe<Scalars['Int']>;
  synonym?: Maybe<Scalars['String']>;
  taxon_id?: Maybe<Scalars['String']>;
  taxonomic_status?: Maybe<Scalars['String']>;
  vernacular_name?: Maybe<Scalars['String']>;
};

/** List of all of possible analysis tools of a diagnosis */
export type Analysis_Tool = {
  __typename?: 'analysis_tool';
  id: Scalars['uuid'];
  slug: Scalars['String'];
};

/** aggregated selection of "analysis_tool" */
export type Analysis_Tool_Aggregate = {
  __typename?: 'analysis_tool_aggregate';
  aggregate?: Maybe<Analysis_Tool_Aggregate_Fields>;
  nodes: Array<Analysis_Tool>;
};

/** aggregate fields of "analysis_tool" */
export type Analysis_Tool_Aggregate_Fields = {
  __typename?: 'analysis_tool_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Analysis_Tool_Max_Fields>;
  min?: Maybe<Analysis_Tool_Min_Fields>;
};


/** aggregate fields of "analysis_tool" */
export type Analysis_Tool_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Analysis_Tool_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "analysis_tool". All fields are combined with a logical 'AND'. */
export type Analysis_Tool_Bool_Exp = {
  _and?: InputMaybe<Array<Analysis_Tool_Bool_Exp>>;
  _not?: InputMaybe<Analysis_Tool_Bool_Exp>;
  _or?: InputMaybe<Array<Analysis_Tool_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "analysis_tool" */
export enum Analysis_Tool_Constraint {
  /** unique or primary key constraint on columns "id" */
  AnalysisToolPkey = 'analysis_tool_pkey'
}

/** input type for inserting data into table "analysis_tool" */
export type Analysis_Tool_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Analysis_Tool_Max_Fields = {
  __typename?: 'analysis_tool_max_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Analysis_Tool_Min_Fields = {
  __typename?: 'analysis_tool_min_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "analysis_tool" */
export type Analysis_Tool_Mutation_Response = {
  __typename?: 'analysis_tool_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Analysis_Tool>;
};

/** input type for inserting object relation for remote table "analysis_tool" */
export type Analysis_Tool_Obj_Rel_Insert_Input = {
  data: Analysis_Tool_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Analysis_Tool_On_Conflict>;
};

/** on_conflict condition type for table "analysis_tool" */
export type Analysis_Tool_On_Conflict = {
  constraint: Analysis_Tool_Constraint;
  update_columns?: Array<Analysis_Tool_Update_Column>;
  where?: InputMaybe<Analysis_Tool_Bool_Exp>;
};

/** Ordering options when selecting data from "analysis_tool". */
export type Analysis_Tool_Order_By = {
  id?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: analysis_tool */
export type Analysis_Tool_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "analysis_tool" */
export enum Analysis_Tool_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "analysis_tool" */
export type Analysis_Tool_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "analysis_tool" */
export type Analysis_Tool_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Analysis_Tool_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Analysis_Tool_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** update columns of table "analysis_tool" */
export enum Analysis_Tool_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

export type Analysis_Tool_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Analysis_Tool_Set_Input>;
  /** filter the rows which have to be updated */
  where: Analysis_Tool_Bool_Exp;
};

/** Boolean expression to compare columns of type "bigint". All fields are combined with logical 'AND'. */
export type Bigint_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['bigint']>;
  _gt?: InputMaybe<Scalars['bigint']>;
  _gte?: InputMaybe<Scalars['bigint']>;
  _in?: InputMaybe<Array<Scalars['bigint']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['bigint']>;
  _lte?: InputMaybe<Scalars['bigint']>;
  _neq?: InputMaybe<Scalars['bigint']>;
  _nin?: InputMaybe<Array<Scalars['bigint']>>;
};

/** columns and relationships of "boundaries_locations" */
export type Boundaries_Locations = {
  __typename?: 'boundaries_locations';
  /** An object relationship */
  boundary: Boundary;
  boundary_id: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An object relationship */
  location: Location;
  location_id: Scalars['uuid'];
};

/** aggregated selection of "boundaries_locations" */
export type Boundaries_Locations_Aggregate = {
  __typename?: 'boundaries_locations_aggregate';
  aggregate?: Maybe<Boundaries_Locations_Aggregate_Fields>;
  nodes: Array<Boundaries_Locations>;
};

export type Boundaries_Locations_Aggregate_Bool_Exp = {
  count?: InputMaybe<Boundaries_Locations_Aggregate_Bool_Exp_Count>;
};

export type Boundaries_Locations_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Boundaries_Locations_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Boundaries_Locations_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "boundaries_locations" */
export type Boundaries_Locations_Aggregate_Fields = {
  __typename?: 'boundaries_locations_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Boundaries_Locations_Max_Fields>;
  min?: Maybe<Boundaries_Locations_Min_Fields>;
};


/** aggregate fields of "boundaries_locations" */
export type Boundaries_Locations_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Boundaries_Locations_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "boundaries_locations" */
export type Boundaries_Locations_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Boundaries_Locations_Max_Order_By>;
  min?: InputMaybe<Boundaries_Locations_Min_Order_By>;
};

/** input type for inserting array relation for remote table "boundaries_locations" */
export type Boundaries_Locations_Arr_Rel_Insert_Input = {
  data: Array<Boundaries_Locations_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Boundaries_Locations_On_Conflict>;
};

/** Boolean expression to filter rows from the table "boundaries_locations". All fields are combined with a logical 'AND'. */
export type Boundaries_Locations_Bool_Exp = {
  _and?: InputMaybe<Array<Boundaries_Locations_Bool_Exp>>;
  _not?: InputMaybe<Boundaries_Locations_Bool_Exp>;
  _or?: InputMaybe<Array<Boundaries_Locations_Bool_Exp>>;
  boundary?: InputMaybe<Boundary_Bool_Exp>;
  boundary_id?: InputMaybe<Uuid_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  location?: InputMaybe<Location_Bool_Exp>;
  location_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "boundaries_locations" */
export enum Boundaries_Locations_Constraint {
  /** unique or primary key constraint on columns "id" */
  BoundariesLocationsPkey = 'boundaries_locations_pkey'
}

/** input type for inserting data into table "boundaries_locations" */
export type Boundaries_Locations_Insert_Input = {
  boundary?: InputMaybe<Boundary_Obj_Rel_Insert_Input>;
  boundary_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  location?: InputMaybe<Location_Obj_Rel_Insert_Input>;
  location_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Boundaries_Locations_Max_Fields = {
  __typename?: 'boundaries_locations_max_fields';
  boundary_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  location_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "boundaries_locations" */
export type Boundaries_Locations_Max_Order_By = {
  boundary_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Boundaries_Locations_Min_Fields = {
  __typename?: 'boundaries_locations_min_fields';
  boundary_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  location_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "boundaries_locations" */
export type Boundaries_Locations_Min_Order_By = {
  boundary_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "boundaries_locations" */
export type Boundaries_Locations_Mutation_Response = {
  __typename?: 'boundaries_locations_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Boundaries_Locations>;
};

/** on_conflict condition type for table "boundaries_locations" */
export type Boundaries_Locations_On_Conflict = {
  constraint: Boundaries_Locations_Constraint;
  update_columns?: Array<Boundaries_Locations_Update_Column>;
  where?: InputMaybe<Boundaries_Locations_Bool_Exp>;
};

/** Ordering options when selecting data from "boundaries_locations". */
export type Boundaries_Locations_Order_By = {
  boundary?: InputMaybe<Boundary_Order_By>;
  boundary_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  location?: InputMaybe<Location_Order_By>;
  location_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: boundaries_locations */
export type Boundaries_Locations_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "boundaries_locations" */
export enum Boundaries_Locations_Select_Column {
  /** column name */
  BoundaryId = 'boundary_id',
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id'
}

/** input type for updating data in table "boundaries_locations" */
export type Boundaries_Locations_Set_Input = {
  boundary_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  location_id?: InputMaybe<Scalars['uuid']>;
};

/** Streaming cursor of the table "boundaries_locations" */
export type Boundaries_Locations_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Boundaries_Locations_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Boundaries_Locations_Stream_Cursor_Value_Input = {
  boundary_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  location_id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "boundaries_locations" */
export enum Boundaries_Locations_Update_Column {
  /** column name */
  BoundaryId = 'boundary_id',
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id'
}

export type Boundaries_Locations_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Boundaries_Locations_Set_Input>;
  /** filter the rows which have to be updated */
  where: Boundaries_Locations_Bool_Exp;
};

/** columns and relationships of "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas = {
  __typename?: 'boundaries_vegetated_areas';
  /** An object relationship */
  boundary: Boundary;
  boundary_id: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An object relationship */
  vegetated_area: Vegetated_Area;
  vegetated_area_id: Scalars['uuid'];
};

/** aggregated selection of "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Aggregate = {
  __typename?: 'boundaries_vegetated_areas_aggregate';
  aggregate?: Maybe<Boundaries_Vegetated_Areas_Aggregate_Fields>;
  nodes: Array<Boundaries_Vegetated_Areas>;
};

export type Boundaries_Vegetated_Areas_Aggregate_Bool_Exp = {
  count?: InputMaybe<Boundaries_Vegetated_Areas_Aggregate_Bool_Exp_Count>;
};

export type Boundaries_Vegetated_Areas_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Boundaries_Vegetated_Areas_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Aggregate_Fields = {
  __typename?: 'boundaries_vegetated_areas_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Boundaries_Vegetated_Areas_Max_Fields>;
  min?: Maybe<Boundaries_Vegetated_Areas_Min_Fields>;
};


/** aggregate fields of "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Boundaries_Vegetated_Areas_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Boundaries_Vegetated_Areas_Max_Order_By>;
  min?: InputMaybe<Boundaries_Vegetated_Areas_Min_Order_By>;
};

/** input type for inserting array relation for remote table "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Arr_Rel_Insert_Input = {
  data: Array<Boundaries_Vegetated_Areas_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Boundaries_Vegetated_Areas_On_Conflict>;
};

/** Boolean expression to filter rows from the table "boundaries_vegetated_areas". All fields are combined with a logical 'AND'. */
export type Boundaries_Vegetated_Areas_Bool_Exp = {
  _and?: InputMaybe<Array<Boundaries_Vegetated_Areas_Bool_Exp>>;
  _not?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
  _or?: InputMaybe<Array<Boundaries_Vegetated_Areas_Bool_Exp>>;
  boundary?: InputMaybe<Boundary_Bool_Exp>;
  boundary_id?: InputMaybe<Uuid_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  vegetated_area?: InputMaybe<Vegetated_Area_Bool_Exp>;
  vegetated_area_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "boundaries_vegetated_areas" */
export enum Boundaries_Vegetated_Areas_Constraint {
  /** unique or primary key constraint on columns "id" */
  BoundariesVegetatedAreasPkey = 'boundaries_vegetated_areas_pkey'
}

/** input type for inserting data into table "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Insert_Input = {
  boundary?: InputMaybe<Boundary_Obj_Rel_Insert_Input>;
  boundary_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  vegetated_area?: InputMaybe<Vegetated_Area_Obj_Rel_Insert_Input>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Boundaries_Vegetated_Areas_Max_Fields = {
  __typename?: 'boundaries_vegetated_areas_max_fields';
  boundary_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Max_Order_By = {
  boundary_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Boundaries_Vegetated_Areas_Min_Fields = {
  __typename?: 'boundaries_vegetated_areas_min_fields';
  boundary_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Min_Order_By = {
  boundary_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Mutation_Response = {
  __typename?: 'boundaries_vegetated_areas_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Boundaries_Vegetated_Areas>;
};

/** on_conflict condition type for table "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_On_Conflict = {
  constraint: Boundaries_Vegetated_Areas_Constraint;
  update_columns?: Array<Boundaries_Vegetated_Areas_Update_Column>;
  where?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
};

/** Ordering options when selecting data from "boundaries_vegetated_areas". */
export type Boundaries_Vegetated_Areas_Order_By = {
  boundary?: InputMaybe<Boundary_Order_By>;
  boundary_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  vegetated_area?: InputMaybe<Vegetated_Area_Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: boundaries_vegetated_areas */
export type Boundaries_Vegetated_Areas_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "boundaries_vegetated_areas" */
export enum Boundaries_Vegetated_Areas_Select_Column {
  /** column name */
  BoundaryId = 'boundary_id',
  /** column name */
  Id = 'id',
  /** column name */
  VegetatedAreaId = 'vegetated_area_id'
}

/** input type for updating data in table "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Set_Input = {
  boundary_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** Streaming cursor of the table "boundaries_vegetated_areas" */
export type Boundaries_Vegetated_Areas_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Boundaries_Vegetated_Areas_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Boundaries_Vegetated_Areas_Stream_Cursor_Value_Input = {
  boundary_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "boundaries_vegetated_areas" */
export enum Boundaries_Vegetated_Areas_Update_Column {
  /** column name */
  BoundaryId = 'boundary_id',
  /** column name */
  Id = 'id',
  /** column name */
  VegetatedAreaId = 'vegetated_area_id'
}

export type Boundaries_Vegetated_Areas_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Boundaries_Vegetated_Areas_Set_Input>;
  /** filter the rows which have to be updated */
  where: Boundaries_Vegetated_Areas_Bool_Exp;
};

/** columns and relationships of "boundary" */
export type Boundary = {
  __typename?: 'boundary';
  administrative_boundary_id?: Maybe<Scalars['uuid']>;
  coords?: Maybe<Scalars['geometry']>;
  created_at: Scalars['timestamptz'];
  data_entry_user_id?: Maybe<Scalars['String']>;
  geographic_boundary_id?: Maybe<Scalars['uuid']>;
  id: Scalars['uuid'];
  name: Scalars['String'];
  type: Scalars['String'];
  updated_at: Scalars['timestamptz'];
};

/** aggregated selection of "boundary" */
export type Boundary_Aggregate = {
  __typename?: 'boundary_aggregate';
  aggregate?: Maybe<Boundary_Aggregate_Fields>;
  nodes: Array<Boundary>;
};

/** aggregate fields of "boundary" */
export type Boundary_Aggregate_Fields = {
  __typename?: 'boundary_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Boundary_Max_Fields>;
  min?: Maybe<Boundary_Min_Fields>;
};


/** aggregate fields of "boundary" */
export type Boundary_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Boundary_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "boundary". All fields are combined with a logical 'AND'. */
export type Boundary_Bool_Exp = {
  _and?: InputMaybe<Array<Boundary_Bool_Exp>>;
  _not?: InputMaybe<Boundary_Bool_Exp>;
  _or?: InputMaybe<Array<Boundary_Bool_Exp>>;
  administrative_boundary_id?: InputMaybe<Uuid_Comparison_Exp>;
  coords?: InputMaybe<Geometry_Comparison_Exp>;
  created_at?: InputMaybe<Timestamptz_Comparison_Exp>;
  data_entry_user_id?: InputMaybe<String_Comparison_Exp>;
  geographic_boundary_id?: InputMaybe<Uuid_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  name?: InputMaybe<String_Comparison_Exp>;
  type?: InputMaybe<String_Comparison_Exp>;
  updated_at?: InputMaybe<Timestamptz_Comparison_Exp>;
};

/** unique or primary key constraints on table "boundary" */
export enum Boundary_Constraint {
  /** unique or primary key constraint on columns "id" */
  BoundaryPkey = 'boundary_pkey'
}

/** input type for inserting data into table "boundary" */
export type Boundary_Insert_Input = {
  administrative_boundary_id?: InputMaybe<Scalars['uuid']>;
  coords?: InputMaybe<Scalars['geometry']>;
  created_at?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  geographic_boundary_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  name?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<Scalars['String']>;
  updated_at?: InputMaybe<Scalars['timestamptz']>;
};

/** aggregate max on columns */
export type Boundary_Max_Fields = {
  __typename?: 'boundary_max_fields';
  administrative_boundary_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  geographic_boundary_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** aggregate min on columns */
export type Boundary_Min_Fields = {
  __typename?: 'boundary_min_fields';
  administrative_boundary_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamptz']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  geographic_boundary_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamptz']>;
};

/** response of any mutation on the table "boundary" */
export type Boundary_Mutation_Response = {
  __typename?: 'boundary_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Boundary>;
};

/** input type for inserting object relation for remote table "boundary" */
export type Boundary_Obj_Rel_Insert_Input = {
  data: Boundary_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Boundary_On_Conflict>;
};

/** on_conflict condition type for table "boundary" */
export type Boundary_On_Conflict = {
  constraint: Boundary_Constraint;
  update_columns?: Array<Boundary_Update_Column>;
  where?: InputMaybe<Boundary_Bool_Exp>;
};

/** Ordering options when selecting data from "boundary". */
export type Boundary_Order_By = {
  administrative_boundary_id?: InputMaybe<Order_By>;
  coords?: InputMaybe<Order_By>;
  created_at?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  geographic_boundary_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  name?: InputMaybe<Order_By>;
  type?: InputMaybe<Order_By>;
  updated_at?: InputMaybe<Order_By>;
};

/** primary key columns input for table: boundary */
export type Boundary_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "boundary" */
export enum Boundary_Select_Column {
  /** column name */
  AdministrativeBoundaryId = 'administrative_boundary_id',
  /** column name */
  Coords = 'coords',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  GeographicBoundaryId = 'geographic_boundary_id',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "boundary" */
export type Boundary_Set_Input = {
  administrative_boundary_id?: InputMaybe<Scalars['uuid']>;
  coords?: InputMaybe<Scalars['geometry']>;
  created_at?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  geographic_boundary_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  name?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<Scalars['String']>;
  updated_at?: InputMaybe<Scalars['timestamptz']>;
};

/** Streaming cursor of the table "boundary" */
export type Boundary_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Boundary_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Boundary_Stream_Cursor_Value_Input = {
  administrative_boundary_id?: InputMaybe<Scalars['uuid']>;
  coords?: InputMaybe<Scalars['geometry']>;
  created_at?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  geographic_boundary_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  name?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<Scalars['String']>;
  updated_at?: InputMaybe<Scalars['timestamptz']>;
};

/** update columns of table "boundary" */
export enum Boundary_Update_Column {
  /** column name */
  AdministrativeBoundaryId = 'administrative_boundary_id',
  /** column name */
  Coords = 'coords',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  GeographicBoundaryId = 'geographic_boundary_id',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Boundary_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Boundary_Set_Input>;
  /** filter the rows which have to be updated */
  where: Boundary_Bool_Exp;
};

/** ordering argument of a cursor */
export enum Cursor_Ordering {
  /** ascending ordering of the cursor */
  Asc = 'ASC',
  /** descending ordering of the cursor */
  Desc = 'DESC'
}

/** Boolean expression to compare columns of type "date". All fields are combined with logical 'AND'. */
export type Date_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['date']>;
  _gt?: InputMaybe<Scalars['date']>;
  _gte?: InputMaybe<Scalars['date']>;
  _in?: InputMaybe<Array<Scalars['date']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['date']>;
  _lte?: InputMaybe<Scalars['date']>;
  _neq?: InputMaybe<Scalars['date']>;
  _nin?: InputMaybe<Array<Scalars['date']>>;
};

/** columns and relationships of "diagnosis" */
export type Diagnosis = {
  __typename?: 'diagnosis';
  analysis_results?: Maybe<Scalars['String']>;
  carpenters_condition?: Maybe<Scalars['String']>;
  collar_condition?: Maybe<Scalars['String']>;
  creation_date: Scalars['timestamptz'];
  crown_condition?: Maybe<Scalars['String']>;
  data_entry_user_id: Scalars['String'];
  /** An array relationship */
  diagnosis_analysis_tools: Array<Diagnosis_Analysis_Tool>;
  /** An aggregate relationship */
  diagnosis_analysis_tools_aggregate: Diagnosis_Analysis_Tool_Aggregate;
  diagnosis_date: Scalars['date'];
  /** An array relationship */
  diagnosis_pathogens: Array<Diagnosis_Pathogen>;
  /** An aggregate relationship */
  diagnosis_pathogens_aggregate: Diagnosis_Pathogen_Aggregate;
  /** An object relationship */
  diagnosis_type: Diagnosis_Type;
  diagnosis_type_id: Scalars['uuid'];
  edition_date?: Maybe<Scalars['timestamptz']>;
  id: Scalars['uuid'];
  note?: Maybe<Scalars['String']>;
  organ?: Maybe<Scalars['String']>;
  recommendation?: Maybe<Scalars['String']>;
  /** An object relationship */
  tree: Tree;
  tree_condition: Scalars['String'];
  tree_id: Scalars['uuid'];
  tree_is_dangerous?: Maybe<Scalars['Boolean']>;
  tree_vigor?: Maybe<Scalars['String']>;
  trunk_condition?: Maybe<Scalars['String']>;
  /** An object relationship */
  user_entity: User_Entity;
};


/** columns and relationships of "diagnosis" */
export type DiagnosisDiagnosis_Analysis_ToolsArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Analysis_Tool_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Analysis_Tool_Order_By>>;
  where?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
};


/** columns and relationships of "diagnosis" */
export type DiagnosisDiagnosis_Analysis_Tools_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Analysis_Tool_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Analysis_Tool_Order_By>>;
  where?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
};


/** columns and relationships of "diagnosis" */
export type DiagnosisDiagnosis_PathogensArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Pathogen_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Pathogen_Order_By>>;
  where?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
};


/** columns and relationships of "diagnosis" */
export type DiagnosisDiagnosis_Pathogens_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Pathogen_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Pathogen_Order_By>>;
  where?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
};

/** aggregated selection of "diagnosis" */
export type Diagnosis_Aggregate = {
  __typename?: 'diagnosis_aggregate';
  aggregate?: Maybe<Diagnosis_Aggregate_Fields>;
  nodes: Array<Diagnosis>;
};

export type Diagnosis_Aggregate_Bool_Exp = {
  bool_and?: InputMaybe<Diagnosis_Aggregate_Bool_Exp_Bool_And>;
  bool_or?: InputMaybe<Diagnosis_Aggregate_Bool_Exp_Bool_Or>;
  count?: InputMaybe<Diagnosis_Aggregate_Bool_Exp_Count>;
};

export type Diagnosis_Aggregate_Bool_Exp_Bool_And = {
  arguments: Diagnosis_Select_Column_Diagnosis_Aggregate_Bool_Exp_Bool_And_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Diagnosis_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Diagnosis_Aggregate_Bool_Exp_Bool_Or = {
  arguments: Diagnosis_Select_Column_Diagnosis_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Diagnosis_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Diagnosis_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Diagnosis_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Diagnosis_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "diagnosis" */
export type Diagnosis_Aggregate_Fields = {
  __typename?: 'diagnosis_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Diagnosis_Max_Fields>;
  min?: Maybe<Diagnosis_Min_Fields>;
};


/** aggregate fields of "diagnosis" */
export type Diagnosis_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Diagnosis_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "diagnosis" */
export type Diagnosis_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Diagnosis_Max_Order_By>;
  min?: InputMaybe<Diagnosis_Min_Order_By>;
};

/** Join table between diagnosis and analysis_tool */
export type Diagnosis_Analysis_Tool = {
  __typename?: 'diagnosis_analysis_tool';
  /** An object relationship */
  analysis_tool: Analysis_Tool;
  analysis_tool_id: Scalars['uuid'];
  diagnosis_id: Scalars['uuid'];
  id: Scalars['uuid'];
};

/** aggregated selection of "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Aggregate = {
  __typename?: 'diagnosis_analysis_tool_aggregate';
  aggregate?: Maybe<Diagnosis_Analysis_Tool_Aggregate_Fields>;
  nodes: Array<Diagnosis_Analysis_Tool>;
};

export type Diagnosis_Analysis_Tool_Aggregate_Bool_Exp = {
  count?: InputMaybe<Diagnosis_Analysis_Tool_Aggregate_Bool_Exp_Count>;
};

export type Diagnosis_Analysis_Tool_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Diagnosis_Analysis_Tool_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Aggregate_Fields = {
  __typename?: 'diagnosis_analysis_tool_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Diagnosis_Analysis_Tool_Max_Fields>;
  min?: Maybe<Diagnosis_Analysis_Tool_Min_Fields>;
};


/** aggregate fields of "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Diagnosis_Analysis_Tool_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Diagnosis_Analysis_Tool_Max_Order_By>;
  min?: InputMaybe<Diagnosis_Analysis_Tool_Min_Order_By>;
};

/** input type for inserting array relation for remote table "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Arr_Rel_Insert_Input = {
  data: Array<Diagnosis_Analysis_Tool_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Diagnosis_Analysis_Tool_On_Conflict>;
};

/** Boolean expression to filter rows from the table "diagnosis_analysis_tool". All fields are combined with a logical 'AND'. */
export type Diagnosis_Analysis_Tool_Bool_Exp = {
  _and?: InputMaybe<Array<Diagnosis_Analysis_Tool_Bool_Exp>>;
  _not?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
  _or?: InputMaybe<Array<Diagnosis_Analysis_Tool_Bool_Exp>>;
  analysis_tool?: InputMaybe<Analysis_Tool_Bool_Exp>;
  analysis_tool_id?: InputMaybe<Uuid_Comparison_Exp>;
  diagnosis_id?: InputMaybe<Uuid_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "diagnosis_analysis_tool" */
export enum Diagnosis_Analysis_Tool_Constraint {
  /** unique or primary key constraint on columns "id" */
  DiagnosisAnalysisToolPkey = 'diagnosis_analysis_tool_pkey'
}

/** input type for inserting data into table "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Insert_Input = {
  analysis_tool?: InputMaybe<Analysis_Tool_Obj_Rel_Insert_Input>;
  analysis_tool_id?: InputMaybe<Scalars['uuid']>;
  diagnosis_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Diagnosis_Analysis_Tool_Max_Fields = {
  __typename?: 'diagnosis_analysis_tool_max_fields';
  analysis_tool_id?: Maybe<Scalars['uuid']>;
  diagnosis_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Max_Order_By = {
  analysis_tool_id?: InputMaybe<Order_By>;
  diagnosis_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Diagnosis_Analysis_Tool_Min_Fields = {
  __typename?: 'diagnosis_analysis_tool_min_fields';
  analysis_tool_id?: Maybe<Scalars['uuid']>;
  diagnosis_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Min_Order_By = {
  analysis_tool_id?: InputMaybe<Order_By>;
  diagnosis_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Mutation_Response = {
  __typename?: 'diagnosis_analysis_tool_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Diagnosis_Analysis_Tool>;
};

/** on_conflict condition type for table "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_On_Conflict = {
  constraint: Diagnosis_Analysis_Tool_Constraint;
  update_columns?: Array<Diagnosis_Analysis_Tool_Update_Column>;
  where?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
};

/** Ordering options when selecting data from "diagnosis_analysis_tool". */
export type Diagnosis_Analysis_Tool_Order_By = {
  analysis_tool?: InputMaybe<Analysis_Tool_Order_By>;
  analysis_tool_id?: InputMaybe<Order_By>;
  diagnosis_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: diagnosis_analysis_tool */
export type Diagnosis_Analysis_Tool_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "diagnosis_analysis_tool" */
export enum Diagnosis_Analysis_Tool_Select_Column {
  /** column name */
  AnalysisToolId = 'analysis_tool_id',
  /** column name */
  DiagnosisId = 'diagnosis_id',
  /** column name */
  Id = 'id'
}

/** input type for updating data in table "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Set_Input = {
  analysis_tool_id?: InputMaybe<Scalars['uuid']>;
  diagnosis_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
};

/** Streaming cursor of the table "diagnosis_analysis_tool" */
export type Diagnosis_Analysis_Tool_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Diagnosis_Analysis_Tool_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Diagnosis_Analysis_Tool_Stream_Cursor_Value_Input = {
  analysis_tool_id?: InputMaybe<Scalars['uuid']>;
  diagnosis_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "diagnosis_analysis_tool" */
export enum Diagnosis_Analysis_Tool_Update_Column {
  /** column name */
  AnalysisToolId = 'analysis_tool_id',
  /** column name */
  DiagnosisId = 'diagnosis_id',
  /** column name */
  Id = 'id'
}

export type Diagnosis_Analysis_Tool_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Diagnosis_Analysis_Tool_Set_Input>;
  /** filter the rows which have to be updated */
  where: Diagnosis_Analysis_Tool_Bool_Exp;
};

/** input type for inserting array relation for remote table "diagnosis" */
export type Diagnosis_Arr_Rel_Insert_Input = {
  data: Array<Diagnosis_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Diagnosis_On_Conflict>;
};

/** Boolean expression to filter rows from the table "diagnosis". All fields are combined with a logical 'AND'. */
export type Diagnosis_Bool_Exp = {
  _and?: InputMaybe<Array<Diagnosis_Bool_Exp>>;
  _not?: InputMaybe<Diagnosis_Bool_Exp>;
  _or?: InputMaybe<Array<Diagnosis_Bool_Exp>>;
  analysis_results?: InputMaybe<String_Comparison_Exp>;
  carpenters_condition?: InputMaybe<String_Comparison_Exp>;
  collar_condition?: InputMaybe<String_Comparison_Exp>;
  creation_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  crown_condition?: InputMaybe<String_Comparison_Exp>;
  data_entry_user_id?: InputMaybe<String_Comparison_Exp>;
  diagnosis_analysis_tools?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
  diagnosis_analysis_tools_aggregate?: InputMaybe<Diagnosis_Analysis_Tool_Aggregate_Bool_Exp>;
  diagnosis_date?: InputMaybe<Date_Comparison_Exp>;
  diagnosis_pathogens?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
  diagnosis_pathogens_aggregate?: InputMaybe<Diagnosis_Pathogen_Aggregate_Bool_Exp>;
  diagnosis_type?: InputMaybe<Diagnosis_Type_Bool_Exp>;
  diagnosis_type_id?: InputMaybe<Uuid_Comparison_Exp>;
  edition_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  note?: InputMaybe<String_Comparison_Exp>;
  organ?: InputMaybe<String_Comparison_Exp>;
  recommendation?: InputMaybe<String_Comparison_Exp>;
  tree?: InputMaybe<Tree_Bool_Exp>;
  tree_condition?: InputMaybe<String_Comparison_Exp>;
  tree_id?: InputMaybe<Uuid_Comparison_Exp>;
  tree_is_dangerous?: InputMaybe<Boolean_Comparison_Exp>;
  tree_vigor?: InputMaybe<String_Comparison_Exp>;
  trunk_condition?: InputMaybe<String_Comparison_Exp>;
  user_entity?: InputMaybe<User_Entity_Bool_Exp>;
};

/** unique or primary key constraints on table "diagnosis" */
export enum Diagnosis_Constraint {
  /** unique or primary key constraint on columns "id" */
  DiagnosisPkey = 'diagnosis_pkey'
}

/** input type for inserting data into table "diagnosis" */
export type Diagnosis_Insert_Input = {
  analysis_results?: InputMaybe<Scalars['String']>;
  carpenters_condition?: InputMaybe<Scalars['String']>;
  collar_condition?: InputMaybe<Scalars['String']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  crown_condition?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  diagnosis_analysis_tools?: InputMaybe<Diagnosis_Analysis_Tool_Arr_Rel_Insert_Input>;
  diagnosis_date?: InputMaybe<Scalars['date']>;
  diagnosis_pathogens?: InputMaybe<Diagnosis_Pathogen_Arr_Rel_Insert_Input>;
  diagnosis_type?: InputMaybe<Diagnosis_Type_Obj_Rel_Insert_Input>;
  diagnosis_type_id?: InputMaybe<Scalars['uuid']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  note?: InputMaybe<Scalars['String']>;
  organ?: InputMaybe<Scalars['String']>;
  recommendation?: InputMaybe<Scalars['String']>;
  tree?: InputMaybe<Tree_Obj_Rel_Insert_Input>;
  tree_condition?: InputMaybe<Scalars['String']>;
  tree_id?: InputMaybe<Scalars['uuid']>;
  tree_is_dangerous?: InputMaybe<Scalars['Boolean']>;
  tree_vigor?: InputMaybe<Scalars['String']>;
  trunk_condition?: InputMaybe<Scalars['String']>;
  user_entity?: InputMaybe<User_Entity_Obj_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type Diagnosis_Max_Fields = {
  __typename?: 'diagnosis_max_fields';
  analysis_results?: Maybe<Scalars['String']>;
  carpenters_condition?: Maybe<Scalars['String']>;
  collar_condition?: Maybe<Scalars['String']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  crown_condition?: Maybe<Scalars['String']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  diagnosis_date?: Maybe<Scalars['date']>;
  diagnosis_type_id?: Maybe<Scalars['uuid']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  note?: Maybe<Scalars['String']>;
  organ?: Maybe<Scalars['String']>;
  recommendation?: Maybe<Scalars['String']>;
  tree_condition?: Maybe<Scalars['String']>;
  tree_id?: Maybe<Scalars['uuid']>;
  tree_vigor?: Maybe<Scalars['String']>;
  trunk_condition?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "diagnosis" */
export type Diagnosis_Max_Order_By = {
  analysis_results?: InputMaybe<Order_By>;
  carpenters_condition?: InputMaybe<Order_By>;
  collar_condition?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  crown_condition?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  diagnosis_date?: InputMaybe<Order_By>;
  diagnosis_type_id?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  organ?: InputMaybe<Order_By>;
  recommendation?: InputMaybe<Order_By>;
  tree_condition?: InputMaybe<Order_By>;
  tree_id?: InputMaybe<Order_By>;
  tree_vigor?: InputMaybe<Order_By>;
  trunk_condition?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Diagnosis_Min_Fields = {
  __typename?: 'diagnosis_min_fields';
  analysis_results?: Maybe<Scalars['String']>;
  carpenters_condition?: Maybe<Scalars['String']>;
  collar_condition?: Maybe<Scalars['String']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  crown_condition?: Maybe<Scalars['String']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  diagnosis_date?: Maybe<Scalars['date']>;
  diagnosis_type_id?: Maybe<Scalars['uuid']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  note?: Maybe<Scalars['String']>;
  organ?: Maybe<Scalars['String']>;
  recommendation?: Maybe<Scalars['String']>;
  tree_condition?: Maybe<Scalars['String']>;
  tree_id?: Maybe<Scalars['uuid']>;
  tree_vigor?: Maybe<Scalars['String']>;
  trunk_condition?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "diagnosis" */
export type Diagnosis_Min_Order_By = {
  analysis_results?: InputMaybe<Order_By>;
  carpenters_condition?: InputMaybe<Order_By>;
  collar_condition?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  crown_condition?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  diagnosis_date?: InputMaybe<Order_By>;
  diagnosis_type_id?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  organ?: InputMaybe<Order_By>;
  recommendation?: InputMaybe<Order_By>;
  tree_condition?: InputMaybe<Order_By>;
  tree_id?: InputMaybe<Order_By>;
  tree_vigor?: InputMaybe<Order_By>;
  trunk_condition?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "diagnosis" */
export type Diagnosis_Mutation_Response = {
  __typename?: 'diagnosis_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Diagnosis>;
};

/** on_conflict condition type for table "diagnosis" */
export type Diagnosis_On_Conflict = {
  constraint: Diagnosis_Constraint;
  update_columns?: Array<Diagnosis_Update_Column>;
  where?: InputMaybe<Diagnosis_Bool_Exp>;
};

/** Ordering options when selecting data from "diagnosis". */
export type Diagnosis_Order_By = {
  analysis_results?: InputMaybe<Order_By>;
  carpenters_condition?: InputMaybe<Order_By>;
  collar_condition?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  crown_condition?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  diagnosis_analysis_tools_aggregate?: InputMaybe<Diagnosis_Analysis_Tool_Aggregate_Order_By>;
  diagnosis_date?: InputMaybe<Order_By>;
  diagnosis_pathogens_aggregate?: InputMaybe<Diagnosis_Pathogen_Aggregate_Order_By>;
  diagnosis_type?: InputMaybe<Diagnosis_Type_Order_By>;
  diagnosis_type_id?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  organ?: InputMaybe<Order_By>;
  recommendation?: InputMaybe<Order_By>;
  tree?: InputMaybe<Tree_Order_By>;
  tree_condition?: InputMaybe<Order_By>;
  tree_id?: InputMaybe<Order_By>;
  tree_is_dangerous?: InputMaybe<Order_By>;
  tree_vigor?: InputMaybe<Order_By>;
  trunk_condition?: InputMaybe<Order_By>;
  user_entity?: InputMaybe<User_Entity_Order_By>;
};

/** Join table between diagnosis and pathogen */
export type Diagnosis_Pathogen = {
  __typename?: 'diagnosis_pathogen';
  diagnosis_id: Scalars['uuid'];
  id: Scalars['uuid'];
  /** An object relationship */
  pathogen: Pathogen;
  pathogen_id: Scalars['uuid'];
};

/** aggregated selection of "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Aggregate = {
  __typename?: 'diagnosis_pathogen_aggregate';
  aggregate?: Maybe<Diagnosis_Pathogen_Aggregate_Fields>;
  nodes: Array<Diagnosis_Pathogen>;
};

export type Diagnosis_Pathogen_Aggregate_Bool_Exp = {
  count?: InputMaybe<Diagnosis_Pathogen_Aggregate_Bool_Exp_Count>;
};

export type Diagnosis_Pathogen_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Diagnosis_Pathogen_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Aggregate_Fields = {
  __typename?: 'diagnosis_pathogen_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Diagnosis_Pathogen_Max_Fields>;
  min?: Maybe<Diagnosis_Pathogen_Min_Fields>;
};


/** aggregate fields of "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Diagnosis_Pathogen_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Diagnosis_Pathogen_Max_Order_By>;
  min?: InputMaybe<Diagnosis_Pathogen_Min_Order_By>;
};

/** input type for inserting array relation for remote table "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Arr_Rel_Insert_Input = {
  data: Array<Diagnosis_Pathogen_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Diagnosis_Pathogen_On_Conflict>;
};

/** Boolean expression to filter rows from the table "diagnosis_pathogen". All fields are combined with a logical 'AND'. */
export type Diagnosis_Pathogen_Bool_Exp = {
  _and?: InputMaybe<Array<Diagnosis_Pathogen_Bool_Exp>>;
  _not?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
  _or?: InputMaybe<Array<Diagnosis_Pathogen_Bool_Exp>>;
  diagnosis_id?: InputMaybe<Uuid_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  pathogen?: InputMaybe<Pathogen_Bool_Exp>;
  pathogen_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "diagnosis_pathogen" */
export enum Diagnosis_Pathogen_Constraint {
  /** unique or primary key constraint on columns "id" */
  DiagnosisPathogenPkey = 'diagnosis_pathogen_pkey'
}

/** input type for inserting data into table "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Insert_Input = {
  diagnosis_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  pathogen?: InputMaybe<Pathogen_Obj_Rel_Insert_Input>;
  pathogen_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Diagnosis_Pathogen_Max_Fields = {
  __typename?: 'diagnosis_pathogen_max_fields';
  diagnosis_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  pathogen_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Max_Order_By = {
  diagnosis_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  pathogen_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Diagnosis_Pathogen_Min_Fields = {
  __typename?: 'diagnosis_pathogen_min_fields';
  diagnosis_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  pathogen_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Min_Order_By = {
  diagnosis_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  pathogen_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Mutation_Response = {
  __typename?: 'diagnosis_pathogen_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Diagnosis_Pathogen>;
};

/** on_conflict condition type for table "diagnosis_pathogen" */
export type Diagnosis_Pathogen_On_Conflict = {
  constraint: Diagnosis_Pathogen_Constraint;
  update_columns?: Array<Diagnosis_Pathogen_Update_Column>;
  where?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
};

/** Ordering options when selecting data from "diagnosis_pathogen". */
export type Diagnosis_Pathogen_Order_By = {
  diagnosis_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  pathogen?: InputMaybe<Pathogen_Order_By>;
  pathogen_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: diagnosis_pathogen */
export type Diagnosis_Pathogen_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "diagnosis_pathogen" */
export enum Diagnosis_Pathogen_Select_Column {
  /** column name */
  DiagnosisId = 'diagnosis_id',
  /** column name */
  Id = 'id',
  /** column name */
  PathogenId = 'pathogen_id'
}

/** input type for updating data in table "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Set_Input = {
  diagnosis_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  pathogen_id?: InputMaybe<Scalars['uuid']>;
};

/** Streaming cursor of the table "diagnosis_pathogen" */
export type Diagnosis_Pathogen_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Diagnosis_Pathogen_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Diagnosis_Pathogen_Stream_Cursor_Value_Input = {
  diagnosis_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  pathogen_id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "diagnosis_pathogen" */
export enum Diagnosis_Pathogen_Update_Column {
  /** column name */
  DiagnosisId = 'diagnosis_id',
  /** column name */
  Id = 'id',
  /** column name */
  PathogenId = 'pathogen_id'
}

export type Diagnosis_Pathogen_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Diagnosis_Pathogen_Set_Input>;
  /** filter the rows which have to be updated */
  where: Diagnosis_Pathogen_Bool_Exp;
};

/** primary key columns input for table: diagnosis */
export type Diagnosis_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "diagnosis" */
export enum Diagnosis_Select_Column {
  /** column name */
  AnalysisResults = 'analysis_results',
  /** column name */
  CarpentersCondition = 'carpenters_condition',
  /** column name */
  CollarCondition = 'collar_condition',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  CrownCondition = 'crown_condition',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  DiagnosisDate = 'diagnosis_date',
  /** column name */
  DiagnosisTypeId = 'diagnosis_type_id',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  Id = 'id',
  /** column name */
  Note = 'note',
  /** column name */
  Organ = 'organ',
  /** column name */
  Recommendation = 'recommendation',
  /** column name */
  TreeCondition = 'tree_condition',
  /** column name */
  TreeId = 'tree_id',
  /** column name */
  TreeIsDangerous = 'tree_is_dangerous',
  /** column name */
  TreeVigor = 'tree_vigor',
  /** column name */
  TrunkCondition = 'trunk_condition'
}

/** select "diagnosis_aggregate_bool_exp_bool_and_arguments_columns" columns of table "diagnosis" */
export enum Diagnosis_Select_Column_Diagnosis_Aggregate_Bool_Exp_Bool_And_Arguments_Columns {
  /** column name */
  TreeIsDangerous = 'tree_is_dangerous'
}

/** select "diagnosis_aggregate_bool_exp_bool_or_arguments_columns" columns of table "diagnosis" */
export enum Diagnosis_Select_Column_Diagnosis_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns {
  /** column name */
  TreeIsDangerous = 'tree_is_dangerous'
}

/** input type for updating data in table "diagnosis" */
export type Diagnosis_Set_Input = {
  analysis_results?: InputMaybe<Scalars['String']>;
  carpenters_condition?: InputMaybe<Scalars['String']>;
  collar_condition?: InputMaybe<Scalars['String']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  crown_condition?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  diagnosis_date?: InputMaybe<Scalars['date']>;
  diagnosis_type_id?: InputMaybe<Scalars['uuid']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  note?: InputMaybe<Scalars['String']>;
  organ?: InputMaybe<Scalars['String']>;
  recommendation?: InputMaybe<Scalars['String']>;
  tree_condition?: InputMaybe<Scalars['String']>;
  tree_id?: InputMaybe<Scalars['uuid']>;
  tree_is_dangerous?: InputMaybe<Scalars['Boolean']>;
  tree_vigor?: InputMaybe<Scalars['String']>;
  trunk_condition?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "diagnosis" */
export type Diagnosis_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Diagnosis_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Diagnosis_Stream_Cursor_Value_Input = {
  analysis_results?: InputMaybe<Scalars['String']>;
  carpenters_condition?: InputMaybe<Scalars['String']>;
  collar_condition?: InputMaybe<Scalars['String']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  crown_condition?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  diagnosis_date?: InputMaybe<Scalars['date']>;
  diagnosis_type_id?: InputMaybe<Scalars['uuid']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  note?: InputMaybe<Scalars['String']>;
  organ?: InputMaybe<Scalars['String']>;
  recommendation?: InputMaybe<Scalars['String']>;
  tree_condition?: InputMaybe<Scalars['String']>;
  tree_id?: InputMaybe<Scalars['uuid']>;
  tree_is_dangerous?: InputMaybe<Scalars['Boolean']>;
  tree_vigor?: InputMaybe<Scalars['String']>;
  trunk_condition?: InputMaybe<Scalars['String']>;
};

/** columns and relationships of "diagnosis_type" */
export type Diagnosis_Type = {
  __typename?: 'diagnosis_type';
  id: Scalars['uuid'];
  slug: Scalars['String'];
};

/** aggregated selection of "diagnosis_type" */
export type Diagnosis_Type_Aggregate = {
  __typename?: 'diagnosis_type_aggregate';
  aggregate?: Maybe<Diagnosis_Type_Aggregate_Fields>;
  nodes: Array<Diagnosis_Type>;
};

/** aggregate fields of "diagnosis_type" */
export type Diagnosis_Type_Aggregate_Fields = {
  __typename?: 'diagnosis_type_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Diagnosis_Type_Max_Fields>;
  min?: Maybe<Diagnosis_Type_Min_Fields>;
};


/** aggregate fields of "diagnosis_type" */
export type Diagnosis_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Diagnosis_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "diagnosis_type". All fields are combined with a logical 'AND'. */
export type Diagnosis_Type_Bool_Exp = {
  _and?: InputMaybe<Array<Diagnosis_Type_Bool_Exp>>;
  _not?: InputMaybe<Diagnosis_Type_Bool_Exp>;
  _or?: InputMaybe<Array<Diagnosis_Type_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "diagnosis_type" */
export enum Diagnosis_Type_Constraint {
  /** unique or primary key constraint on columns "id" */
  DiagnosisTypePkey = 'diagnosis_type_pkey'
}

/** input type for inserting data into table "diagnosis_type" */
export type Diagnosis_Type_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Diagnosis_Type_Max_Fields = {
  __typename?: 'diagnosis_type_max_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Diagnosis_Type_Min_Fields = {
  __typename?: 'diagnosis_type_min_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "diagnosis_type" */
export type Diagnosis_Type_Mutation_Response = {
  __typename?: 'diagnosis_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Diagnosis_Type>;
};

/** input type for inserting object relation for remote table "diagnosis_type" */
export type Diagnosis_Type_Obj_Rel_Insert_Input = {
  data: Diagnosis_Type_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Diagnosis_Type_On_Conflict>;
};

/** on_conflict condition type for table "diagnosis_type" */
export type Diagnosis_Type_On_Conflict = {
  constraint: Diagnosis_Type_Constraint;
  update_columns?: Array<Diagnosis_Type_Update_Column>;
  where?: InputMaybe<Diagnosis_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "diagnosis_type". */
export type Diagnosis_Type_Order_By = {
  id?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: diagnosis_type */
export type Diagnosis_Type_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "diagnosis_type" */
export enum Diagnosis_Type_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "diagnosis_type" */
export type Diagnosis_Type_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "diagnosis_type" */
export type Diagnosis_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Diagnosis_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Diagnosis_Type_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** update columns of table "diagnosis_type" */
export enum Diagnosis_Type_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

export type Diagnosis_Type_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Diagnosis_Type_Set_Input>;
  /** filter the rows which have to be updated */
  where: Diagnosis_Type_Bool_Exp;
};

/** update columns of table "diagnosis" */
export enum Diagnosis_Update_Column {
  /** column name */
  AnalysisResults = 'analysis_results',
  /** column name */
  CarpentersCondition = 'carpenters_condition',
  /** column name */
  CollarCondition = 'collar_condition',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  CrownCondition = 'crown_condition',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  DiagnosisDate = 'diagnosis_date',
  /** column name */
  DiagnosisTypeId = 'diagnosis_type_id',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  Id = 'id',
  /** column name */
  Note = 'note',
  /** column name */
  Organ = 'organ',
  /** column name */
  Recommendation = 'recommendation',
  /** column name */
  TreeCondition = 'tree_condition',
  /** column name */
  TreeId = 'tree_id',
  /** column name */
  TreeIsDangerous = 'tree_is_dangerous',
  /** column name */
  TreeVigor = 'tree_vigor',
  /** column name */
  TrunkCondition = 'trunk_condition'
}

export type Diagnosis_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Diagnosis_Set_Input>;
  /** filter the rows which have to be updated */
  where: Diagnosis_Bool_Exp;
};

/** columns and relationships of "family_intervention_type" */
export type Family_Intervention_Type = {
  __typename?: 'family_intervention_type';
  color?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  /** An array relationship */
  intervention_types: Array<Intervention_Type>;
  /** An aggregate relationship */
  intervention_types_aggregate: Intervention_Type_Aggregate;
  slug: Scalars['String'];
};


/** columns and relationships of "family_intervention_type" */
export type Family_Intervention_TypeIntervention_TypesArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Type_Order_By>>;
  where?: InputMaybe<Intervention_Type_Bool_Exp>;
};


/** columns and relationships of "family_intervention_type" */
export type Family_Intervention_TypeIntervention_Types_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Type_Order_By>>;
  where?: InputMaybe<Intervention_Type_Bool_Exp>;
};

/** aggregated selection of "family_intervention_type" */
export type Family_Intervention_Type_Aggregate = {
  __typename?: 'family_intervention_type_aggregate';
  aggregate?: Maybe<Family_Intervention_Type_Aggregate_Fields>;
  nodes: Array<Family_Intervention_Type>;
};

/** aggregate fields of "family_intervention_type" */
export type Family_Intervention_Type_Aggregate_Fields = {
  __typename?: 'family_intervention_type_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Family_Intervention_Type_Max_Fields>;
  min?: Maybe<Family_Intervention_Type_Min_Fields>;
};


/** aggregate fields of "family_intervention_type" */
export type Family_Intervention_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Family_Intervention_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "family_intervention_type". All fields are combined with a logical 'AND'. */
export type Family_Intervention_Type_Bool_Exp = {
  _and?: InputMaybe<Array<Family_Intervention_Type_Bool_Exp>>;
  _not?: InputMaybe<Family_Intervention_Type_Bool_Exp>;
  _or?: InputMaybe<Array<Family_Intervention_Type_Bool_Exp>>;
  color?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  intervention_types?: InputMaybe<Intervention_Type_Bool_Exp>;
  intervention_types_aggregate?: InputMaybe<Intervention_Type_Aggregate_Bool_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "family_intervention_type" */
export enum Family_Intervention_Type_Constraint {
  /** unique or primary key constraint on columns "id" */
  FamilyInterventionTypePkey = 'family_intervention_type_pkey',
  /** unique or primary key constraint on columns "slug" */
  FamilyInterventionTypeSlugKey = 'family_intervention_type_slug_key'
}

/** input type for inserting data into table "family_intervention_type" */
export type Family_Intervention_Type_Insert_Input = {
  color?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  intervention_types?: InputMaybe<Intervention_Type_Arr_Rel_Insert_Input>;
  slug?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Family_Intervention_Type_Max_Fields = {
  __typename?: 'family_intervention_type_max_fields';
  color?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Family_Intervention_Type_Min_Fields = {
  __typename?: 'family_intervention_type_min_fields';
  color?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "family_intervention_type" */
export type Family_Intervention_Type_Mutation_Response = {
  __typename?: 'family_intervention_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Family_Intervention_Type>;
};

/** input type for inserting object relation for remote table "family_intervention_type" */
export type Family_Intervention_Type_Obj_Rel_Insert_Input = {
  data: Family_Intervention_Type_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Family_Intervention_Type_On_Conflict>;
};

/** on_conflict condition type for table "family_intervention_type" */
export type Family_Intervention_Type_On_Conflict = {
  constraint: Family_Intervention_Type_Constraint;
  update_columns?: Array<Family_Intervention_Type_Update_Column>;
  where?: InputMaybe<Family_Intervention_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "family_intervention_type". */
export type Family_Intervention_Type_Order_By = {
  color?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  intervention_types_aggregate?: InputMaybe<Intervention_Type_Aggregate_Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: family_intervention_type */
export type Family_Intervention_Type_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "family_intervention_type" */
export enum Family_Intervention_Type_Select_Column {
  /** column name */
  Color = 'color',
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "family_intervention_type" */
export type Family_Intervention_Type_Set_Input = {
  color?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "family_intervention_type" */
export type Family_Intervention_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Family_Intervention_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Family_Intervention_Type_Stream_Cursor_Value_Input = {
  color?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** update columns of table "family_intervention_type" */
export enum Family_Intervention_Type_Update_Column {
  /** column name */
  Color = 'color',
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

export type Family_Intervention_Type_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Family_Intervention_Type_Set_Input>;
  /** filter the rows which have to be updated */
  where: Family_Intervention_Type_Bool_Exp;
};

/** List of all of possible foot types of a location */
export type Foot_Type = {
  __typename?: 'foot_type';
  id: Scalars['uuid'];
  slug: Scalars['String'];
};

/** aggregated selection of "foot_type" */
export type Foot_Type_Aggregate = {
  __typename?: 'foot_type_aggregate';
  aggregate?: Maybe<Foot_Type_Aggregate_Fields>;
  nodes: Array<Foot_Type>;
};

/** aggregate fields of "foot_type" */
export type Foot_Type_Aggregate_Fields = {
  __typename?: 'foot_type_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Foot_Type_Max_Fields>;
  min?: Maybe<Foot_Type_Min_Fields>;
};


/** aggregate fields of "foot_type" */
export type Foot_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Foot_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "foot_type". All fields are combined with a logical 'AND'. */
export type Foot_Type_Bool_Exp = {
  _and?: InputMaybe<Array<Foot_Type_Bool_Exp>>;
  _not?: InputMaybe<Foot_Type_Bool_Exp>;
  _or?: InputMaybe<Array<Foot_Type_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "foot_type" */
export enum Foot_Type_Constraint {
  /** unique or primary key constraint on columns "id" */
  FootTypePkey = 'foot_type_pkey'
}

/** input type for inserting data into table "foot_type" */
export type Foot_Type_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Foot_Type_Max_Fields = {
  __typename?: 'foot_type_max_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Foot_Type_Min_Fields = {
  __typename?: 'foot_type_min_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "foot_type" */
export type Foot_Type_Mutation_Response = {
  __typename?: 'foot_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Foot_Type>;
};

/** on_conflict condition type for table "foot_type" */
export type Foot_Type_On_Conflict = {
  constraint: Foot_Type_Constraint;
  update_columns?: Array<Foot_Type_Update_Column>;
  where?: InputMaybe<Foot_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "foot_type". */
export type Foot_Type_Order_By = {
  id?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: foot_type */
export type Foot_Type_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "foot_type" */
export enum Foot_Type_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "foot_type" */
export type Foot_Type_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "foot_type" */
export type Foot_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Foot_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Foot_Type_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** update columns of table "foot_type" */
export enum Foot_Type_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

export type Foot_Type_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Foot_Type_Set_Input>;
  /** filter the rows which have to be updated */
  where: Foot_Type_Bool_Exp;
};

export type Geography_Cast_Exp = {
  geometry?: InputMaybe<Geometry_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "geography". All fields are combined with logical 'AND'. */
export type Geography_Comparison_Exp = {
  _cast?: InputMaybe<Geography_Cast_Exp>;
  _eq?: InputMaybe<Scalars['geography']>;
  _gt?: InputMaybe<Scalars['geography']>;
  _gte?: InputMaybe<Scalars['geography']>;
  _in?: InputMaybe<Array<Scalars['geography']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['geography']>;
  _lte?: InputMaybe<Scalars['geography']>;
  _neq?: InputMaybe<Scalars['geography']>;
  _nin?: InputMaybe<Array<Scalars['geography']>>;
  /** is the column within a given distance from the given geography value */
  _st_d_within?: InputMaybe<St_D_Within_Geography_Input>;
  /** does the column spatially intersect the given geography value */
  _st_intersects?: InputMaybe<Scalars['geography']>;
};

export type Geometry_Cast_Exp = {
  geography?: InputMaybe<Geography_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "geometry". All fields are combined with logical 'AND'. */
export type Geometry_Comparison_Exp = {
  _cast?: InputMaybe<Geometry_Cast_Exp>;
  _eq?: InputMaybe<Scalars['geometry']>;
  _gt?: InputMaybe<Scalars['geometry']>;
  _gte?: InputMaybe<Scalars['geometry']>;
  _in?: InputMaybe<Array<Scalars['geometry']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['geometry']>;
  _lte?: InputMaybe<Scalars['geometry']>;
  _neq?: InputMaybe<Scalars['geometry']>;
  _nin?: InputMaybe<Array<Scalars['geometry']>>;
  /** is the column within a given 3D distance from the given geometry value */
  _st_3d_d_within?: InputMaybe<St_D_Within_Input>;
  /** does the column spatially intersect the given geometry value in 3D */
  _st_3d_intersects?: InputMaybe<Scalars['geometry']>;
  /** does the column contain the given geometry value */
  _st_contains?: InputMaybe<Scalars['geometry']>;
  /** does the column cross the given geometry value */
  _st_crosses?: InputMaybe<Scalars['geometry']>;
  /** is the column within a given distance from the given geometry value */
  _st_d_within?: InputMaybe<St_D_Within_Input>;
  /** is the column equal to given geometry value (directionality is ignored) */
  _st_equals?: InputMaybe<Scalars['geometry']>;
  /** does the column spatially intersect the given geometry value */
  _st_intersects?: InputMaybe<Scalars['geometry']>;
  /** does the column 'spatially overlap' (intersect but not completely contain) the given geometry value */
  _st_overlaps?: InputMaybe<Scalars['geometry']>;
  /** does the column have atleast one point in common with the given geometry value */
  _st_touches?: InputMaybe<Scalars['geometry']>;
  /** is the column contained in the given geometry value */
  _st_within?: InputMaybe<Scalars['geometry']>;
};

/** List of all imports */
export type Import = {
  __typename?: 'import';
  creation_date: Scalars['timestamptz'];
  data_entry_user_id: Scalars['String'];
  edition_date?: Maybe<Scalars['timestamptz']>;
  id: Scalars['uuid'];
  organization_id: Scalars['uuid'];
  processing_code: Scalars['Int'];
  source_crs?: Maybe<Scalars['String']>;
  source_file_path: Scalars['String'];
  task_id?: Maybe<Scalars['String']>;
  /** An object relationship */
  user_entity: User_Entity;
};

/** aggregated selection of "import" */
export type Import_Aggregate = {
  __typename?: 'import_aggregate';
  aggregate?: Maybe<Import_Aggregate_Fields>;
  nodes: Array<Import>;
};

/** aggregate fields of "import" */
export type Import_Aggregate_Fields = {
  __typename?: 'import_aggregate_fields';
  avg?: Maybe<Import_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Import_Max_Fields>;
  min?: Maybe<Import_Min_Fields>;
  stddev?: Maybe<Import_Stddev_Fields>;
  stddev_pop?: Maybe<Import_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Import_Stddev_Samp_Fields>;
  sum?: Maybe<Import_Sum_Fields>;
  var_pop?: Maybe<Import_Var_Pop_Fields>;
  var_samp?: Maybe<Import_Var_Samp_Fields>;
  variance?: Maybe<Import_Variance_Fields>;
};


/** aggregate fields of "import" */
export type Import_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Import_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type Import_Avg_Fields = {
  __typename?: 'import_avg_fields';
  processing_code?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "import". All fields are combined with a logical 'AND'. */
export type Import_Bool_Exp = {
  _and?: InputMaybe<Array<Import_Bool_Exp>>;
  _not?: InputMaybe<Import_Bool_Exp>;
  _or?: InputMaybe<Array<Import_Bool_Exp>>;
  creation_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  data_entry_user_id?: InputMaybe<String_Comparison_Exp>;
  edition_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  organization_id?: InputMaybe<Uuid_Comparison_Exp>;
  processing_code?: InputMaybe<Int_Comparison_Exp>;
  source_crs?: InputMaybe<String_Comparison_Exp>;
  source_file_path?: InputMaybe<String_Comparison_Exp>;
  task_id?: InputMaybe<String_Comparison_Exp>;
  user_entity?: InputMaybe<User_Entity_Bool_Exp>;
};

/** unique or primary key constraints on table "import" */
export enum Import_Constraint {
  /** unique or primary key constraint on columns "id" */
  ImportPkey = 'import_pkey'
}

/** input type for incrementing numeric columns in table "import" */
export type Import_Inc_Input = {
  processing_code?: InputMaybe<Scalars['Int']>;
};

/** input type for inserting data into table "import" */
export type Import_Insert_Input = {
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  organization_id?: InputMaybe<Scalars['uuid']>;
  processing_code?: InputMaybe<Scalars['Int']>;
  source_crs?: InputMaybe<Scalars['String']>;
  source_file_path?: InputMaybe<Scalars['String']>;
  task_id?: InputMaybe<Scalars['String']>;
  user_entity?: InputMaybe<User_Entity_Obj_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type Import_Max_Fields = {
  __typename?: 'import_max_fields';
  creation_date?: Maybe<Scalars['timestamptz']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  organization_id?: Maybe<Scalars['uuid']>;
  processing_code?: Maybe<Scalars['Int']>;
  source_crs?: Maybe<Scalars['String']>;
  source_file_path?: Maybe<Scalars['String']>;
  task_id?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Import_Min_Fields = {
  __typename?: 'import_min_fields';
  creation_date?: Maybe<Scalars['timestamptz']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  organization_id?: Maybe<Scalars['uuid']>;
  processing_code?: Maybe<Scalars['Int']>;
  source_crs?: Maybe<Scalars['String']>;
  source_file_path?: Maybe<Scalars['String']>;
  task_id?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "import" */
export type Import_Mutation_Response = {
  __typename?: 'import_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Import>;
};

/** on_conflict condition type for table "import" */
export type Import_On_Conflict = {
  constraint: Import_Constraint;
  update_columns?: Array<Import_Update_Column>;
  where?: InputMaybe<Import_Bool_Exp>;
};

/** Ordering options when selecting data from "import". */
export type Import_Order_By = {
  creation_date?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  organization_id?: InputMaybe<Order_By>;
  processing_code?: InputMaybe<Order_By>;
  source_crs?: InputMaybe<Order_By>;
  source_file_path?: InputMaybe<Order_By>;
  task_id?: InputMaybe<Order_By>;
  user_entity?: InputMaybe<User_Entity_Order_By>;
};

/** primary key columns input for table: import */
export type Import_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "import" */
export enum Import_Select_Column {
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  Id = 'id',
  /** column name */
  OrganizationId = 'organization_id',
  /** column name */
  ProcessingCode = 'processing_code',
  /** column name */
  SourceCrs = 'source_crs',
  /** column name */
  SourceFilePath = 'source_file_path',
  /** column name */
  TaskId = 'task_id'
}

/** input type for updating data in table "import" */
export type Import_Set_Input = {
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  organization_id?: InputMaybe<Scalars['uuid']>;
  processing_code?: InputMaybe<Scalars['Int']>;
  source_crs?: InputMaybe<Scalars['String']>;
  source_file_path?: InputMaybe<Scalars['String']>;
  task_id?: InputMaybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type Import_Stddev_Fields = {
  __typename?: 'import_stddev_fields';
  processing_code?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Import_Stddev_Pop_Fields = {
  __typename?: 'import_stddev_pop_fields';
  processing_code?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Import_Stddev_Samp_Fields = {
  __typename?: 'import_stddev_samp_fields';
  processing_code?: Maybe<Scalars['Float']>;
};

/** Streaming cursor of the table "import" */
export type Import_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Import_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Import_Stream_Cursor_Value_Input = {
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  organization_id?: InputMaybe<Scalars['uuid']>;
  processing_code?: InputMaybe<Scalars['Int']>;
  source_crs?: InputMaybe<Scalars['String']>;
  source_file_path?: InputMaybe<Scalars['String']>;
  task_id?: InputMaybe<Scalars['String']>;
};

/** aggregate sum on columns */
export type Import_Sum_Fields = {
  __typename?: 'import_sum_fields';
  processing_code?: Maybe<Scalars['Int']>;
};

/** update columns of table "import" */
export enum Import_Update_Column {
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  Id = 'id',
  /** column name */
  OrganizationId = 'organization_id',
  /** column name */
  ProcessingCode = 'processing_code',
  /** column name */
  SourceCrs = 'source_crs',
  /** column name */
  SourceFilePath = 'source_file_path',
  /** column name */
  TaskId = 'task_id'
}

export type Import_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Import_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Import_Set_Input>;
  /** filter the rows which have to be updated */
  where: Import_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Import_Var_Pop_Fields = {
  __typename?: 'import_var_pop_fields';
  processing_code?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Import_Var_Samp_Fields = {
  __typename?: 'import_var_samp_fields';
  processing_code?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Import_Variance_Fields = {
  __typename?: 'import_variance_fields';
  processing_code?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "intervention" */
export type Intervention = {
  __typename?: 'intervention';
  cost?: Maybe<Scalars['numeric']>;
  creation_date: Scalars['timestamptz'];
  data_entry_user_id: Scalars['String'];
  edition_date?: Maybe<Scalars['timestamptz']>;
  id: Scalars['uuid'];
  /** An object relationship */
  intervention_partner?: Maybe<Intervention_Partner>;
  intervention_partner_id?: Maybe<Scalars['uuid']>;
  /** An object relationship */
  intervention_type: Intervention_Type;
  intervention_type_id: Scalars['uuid'];
  is_parking_banned?: Maybe<Scalars['Boolean']>;
  /** An object relationship */
  location?: Maybe<Location>;
  location_id?: Maybe<Scalars['uuid']>;
  note?: Maybe<Scalars['String']>;
  realization_date?: Maybe<Scalars['date']>;
  request_origin?: Maybe<Scalars['String']>;
  scheduled_date: Scalars['date'];
  /** An object relationship */
  tree?: Maybe<Tree>;
  tree_id?: Maybe<Scalars['uuid']>;
  /** An object relationship */
  user_entity: User_Entity;
  validation_note?: Maybe<Scalars['String']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
  /** An object relationship */
  worksite?: Maybe<Worksite>;
  worksite_id?: Maybe<Scalars['uuid']>;
};

/** aggregated selection of "intervention" */
export type Intervention_Aggregate = {
  __typename?: 'intervention_aggregate';
  aggregate?: Maybe<Intervention_Aggregate_Fields>;
  nodes: Array<Intervention>;
};

export type Intervention_Aggregate_Bool_Exp = {
  bool_and?: InputMaybe<Intervention_Aggregate_Bool_Exp_Bool_And>;
  bool_or?: InputMaybe<Intervention_Aggregate_Bool_Exp_Bool_Or>;
  count?: InputMaybe<Intervention_Aggregate_Bool_Exp_Count>;
};

export type Intervention_Aggregate_Bool_Exp_Bool_And = {
  arguments: Intervention_Select_Column_Intervention_Aggregate_Bool_Exp_Bool_And_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Intervention_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Intervention_Aggregate_Bool_Exp_Bool_Or = {
  arguments: Intervention_Select_Column_Intervention_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Intervention_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Intervention_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Intervention_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Intervention_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "intervention" */
export type Intervention_Aggregate_Fields = {
  __typename?: 'intervention_aggregate_fields';
  avg?: Maybe<Intervention_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Intervention_Max_Fields>;
  min?: Maybe<Intervention_Min_Fields>;
  stddev?: Maybe<Intervention_Stddev_Fields>;
  stddev_pop?: Maybe<Intervention_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Intervention_Stddev_Samp_Fields>;
  sum?: Maybe<Intervention_Sum_Fields>;
  var_pop?: Maybe<Intervention_Var_Pop_Fields>;
  var_samp?: Maybe<Intervention_Var_Samp_Fields>;
  variance?: Maybe<Intervention_Variance_Fields>;
};


/** aggregate fields of "intervention" */
export type Intervention_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Intervention_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "intervention" */
export type Intervention_Aggregate_Order_By = {
  avg?: InputMaybe<Intervention_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Intervention_Max_Order_By>;
  min?: InputMaybe<Intervention_Min_Order_By>;
  stddev?: InputMaybe<Intervention_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Intervention_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Intervention_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Intervention_Sum_Order_By>;
  var_pop?: InputMaybe<Intervention_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Intervention_Var_Samp_Order_By>;
  variance?: InputMaybe<Intervention_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "intervention" */
export type Intervention_Arr_Rel_Insert_Input = {
  data: Array<Intervention_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Intervention_On_Conflict>;
};

/** aggregate avg on columns */
export type Intervention_Avg_Fields = {
  __typename?: 'intervention_avg_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "intervention" */
export type Intervention_Avg_Order_By = {
  cost?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "intervention". All fields are combined with a logical 'AND'. */
export type Intervention_Bool_Exp = {
  _and?: InputMaybe<Array<Intervention_Bool_Exp>>;
  _not?: InputMaybe<Intervention_Bool_Exp>;
  _or?: InputMaybe<Array<Intervention_Bool_Exp>>;
  cost?: InputMaybe<Numeric_Comparison_Exp>;
  creation_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  data_entry_user_id?: InputMaybe<String_Comparison_Exp>;
  edition_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  intervention_partner?: InputMaybe<Intervention_Partner_Bool_Exp>;
  intervention_partner_id?: InputMaybe<Uuid_Comparison_Exp>;
  intervention_type?: InputMaybe<Intervention_Type_Bool_Exp>;
  intervention_type_id?: InputMaybe<Uuid_Comparison_Exp>;
  is_parking_banned?: InputMaybe<Boolean_Comparison_Exp>;
  location?: InputMaybe<Location_Bool_Exp>;
  location_id?: InputMaybe<Uuid_Comparison_Exp>;
  note?: InputMaybe<String_Comparison_Exp>;
  realization_date?: InputMaybe<Date_Comparison_Exp>;
  request_origin?: InputMaybe<String_Comparison_Exp>;
  scheduled_date?: InputMaybe<Date_Comparison_Exp>;
  tree?: InputMaybe<Tree_Bool_Exp>;
  tree_id?: InputMaybe<Uuid_Comparison_Exp>;
  user_entity?: InputMaybe<User_Entity_Bool_Exp>;
  validation_note?: InputMaybe<String_Comparison_Exp>;
  vegetated_area_id?: InputMaybe<Uuid_Comparison_Exp>;
  worksite?: InputMaybe<Worksite_Bool_Exp>;
  worksite_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "intervention" */
export enum Intervention_Constraint {
  /** unique or primary key constraint on columns "id" */
  InterventionPkey = 'intervention_pkey'
}

/** input type for incrementing numeric columns in table "intervention" */
export type Intervention_Inc_Input = {
  cost?: InputMaybe<Scalars['numeric']>;
};

/** input type for inserting data into table "intervention" */
export type Intervention_Insert_Input = {
  cost?: InputMaybe<Scalars['numeric']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  intervention_partner?: InputMaybe<Intervention_Partner_Obj_Rel_Insert_Input>;
  intervention_partner_id?: InputMaybe<Scalars['uuid']>;
  intervention_type?: InputMaybe<Intervention_Type_Obj_Rel_Insert_Input>;
  intervention_type_id?: InputMaybe<Scalars['uuid']>;
  is_parking_banned?: InputMaybe<Scalars['Boolean']>;
  location?: InputMaybe<Location_Obj_Rel_Insert_Input>;
  location_id?: InputMaybe<Scalars['uuid']>;
  note?: InputMaybe<Scalars['String']>;
  realization_date?: InputMaybe<Scalars['date']>;
  request_origin?: InputMaybe<Scalars['String']>;
  scheduled_date?: InputMaybe<Scalars['date']>;
  tree?: InputMaybe<Tree_Obj_Rel_Insert_Input>;
  tree_id?: InputMaybe<Scalars['uuid']>;
  user_entity?: InputMaybe<User_Entity_Obj_Rel_Insert_Input>;
  validation_note?: InputMaybe<Scalars['String']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
  worksite?: InputMaybe<Worksite_Obj_Rel_Insert_Input>;
  worksite_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Intervention_Max_Fields = {
  __typename?: 'intervention_max_fields';
  cost?: Maybe<Scalars['numeric']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  intervention_partner_id?: Maybe<Scalars['uuid']>;
  intervention_type_id?: Maybe<Scalars['uuid']>;
  location_id?: Maybe<Scalars['uuid']>;
  note?: Maybe<Scalars['String']>;
  realization_date?: Maybe<Scalars['date']>;
  request_origin?: Maybe<Scalars['String']>;
  scheduled_date?: Maybe<Scalars['date']>;
  tree_id?: Maybe<Scalars['uuid']>;
  validation_note?: Maybe<Scalars['String']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
  worksite_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "intervention" */
export type Intervention_Max_Order_By = {
  cost?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  intervention_partner_id?: InputMaybe<Order_By>;
  intervention_type_id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  realization_date?: InputMaybe<Order_By>;
  request_origin?: InputMaybe<Order_By>;
  scheduled_date?: InputMaybe<Order_By>;
  tree_id?: InputMaybe<Order_By>;
  validation_note?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
  worksite_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Intervention_Min_Fields = {
  __typename?: 'intervention_min_fields';
  cost?: Maybe<Scalars['numeric']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  intervention_partner_id?: Maybe<Scalars['uuid']>;
  intervention_type_id?: Maybe<Scalars['uuid']>;
  location_id?: Maybe<Scalars['uuid']>;
  note?: Maybe<Scalars['String']>;
  realization_date?: Maybe<Scalars['date']>;
  request_origin?: Maybe<Scalars['String']>;
  scheduled_date?: Maybe<Scalars['date']>;
  tree_id?: Maybe<Scalars['uuid']>;
  validation_note?: Maybe<Scalars['String']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
  worksite_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "intervention" */
export type Intervention_Min_Order_By = {
  cost?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  intervention_partner_id?: InputMaybe<Order_By>;
  intervention_type_id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  realization_date?: InputMaybe<Order_By>;
  request_origin?: InputMaybe<Order_By>;
  scheduled_date?: InputMaybe<Order_By>;
  tree_id?: InputMaybe<Order_By>;
  validation_note?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
  worksite_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "intervention" */
export type Intervention_Mutation_Response = {
  __typename?: 'intervention_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Intervention>;
};

/** on_conflict condition type for table "intervention" */
export type Intervention_On_Conflict = {
  constraint: Intervention_Constraint;
  update_columns?: Array<Intervention_Update_Column>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};

/** Ordering options when selecting data from "intervention". */
export type Intervention_Order_By = {
  cost?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  intervention_partner?: InputMaybe<Intervention_Partner_Order_By>;
  intervention_partner_id?: InputMaybe<Order_By>;
  intervention_type?: InputMaybe<Intervention_Type_Order_By>;
  intervention_type_id?: InputMaybe<Order_By>;
  is_parking_banned?: InputMaybe<Order_By>;
  location?: InputMaybe<Location_Order_By>;
  location_id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  realization_date?: InputMaybe<Order_By>;
  request_origin?: InputMaybe<Order_By>;
  scheduled_date?: InputMaybe<Order_By>;
  tree?: InputMaybe<Tree_Order_By>;
  tree_id?: InputMaybe<Order_By>;
  user_entity?: InputMaybe<User_Entity_Order_By>;
  validation_note?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
  worksite?: InputMaybe<Worksite_Order_By>;
  worksite_id?: InputMaybe<Order_By>;
};

/** columns and relationships of "intervention_partner" */
export type Intervention_Partner = {
  __typename?: 'intervention_partner';
  id: Scalars['uuid'];
  name: Scalars['String'];
  user_id?: Maybe<Scalars['String']>;
};

/** aggregated selection of "intervention_partner" */
export type Intervention_Partner_Aggregate = {
  __typename?: 'intervention_partner_aggregate';
  aggregate?: Maybe<Intervention_Partner_Aggregate_Fields>;
  nodes: Array<Intervention_Partner>;
};

/** aggregate fields of "intervention_partner" */
export type Intervention_Partner_Aggregate_Fields = {
  __typename?: 'intervention_partner_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Intervention_Partner_Max_Fields>;
  min?: Maybe<Intervention_Partner_Min_Fields>;
};


/** aggregate fields of "intervention_partner" */
export type Intervention_Partner_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Intervention_Partner_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "intervention_partner". All fields are combined with a logical 'AND'. */
export type Intervention_Partner_Bool_Exp = {
  _and?: InputMaybe<Array<Intervention_Partner_Bool_Exp>>;
  _not?: InputMaybe<Intervention_Partner_Bool_Exp>;
  _or?: InputMaybe<Array<Intervention_Partner_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  name?: InputMaybe<String_Comparison_Exp>;
  user_id?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "intervention_partner" */
export enum Intervention_Partner_Constraint {
  /** unique or primary key constraint on columns "name" */
  InterventionPartnerNameKey = 'intervention_partner_name_key',
  /** unique or primary key constraint on columns "id" */
  InterventionPartnerPkey = 'intervention_partner_pkey',
  /** unique or primary key constraint on columns "user_id" */
  InterventionPartnerUserIdKey = 'intervention_partner_user_id_key'
}

/** input type for inserting data into table "intervention_partner" */
export type Intervention_Partner_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  name?: InputMaybe<Scalars['String']>;
  user_id?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Intervention_Partner_Max_Fields = {
  __typename?: 'intervention_partner_max_fields';
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  user_id?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Intervention_Partner_Min_Fields = {
  __typename?: 'intervention_partner_min_fields';
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  user_id?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "intervention_partner" */
export type Intervention_Partner_Mutation_Response = {
  __typename?: 'intervention_partner_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Intervention_Partner>;
};

/** input type for inserting object relation for remote table "intervention_partner" */
export type Intervention_Partner_Obj_Rel_Insert_Input = {
  data: Intervention_Partner_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Intervention_Partner_On_Conflict>;
};

/** on_conflict condition type for table "intervention_partner" */
export type Intervention_Partner_On_Conflict = {
  constraint: Intervention_Partner_Constraint;
  update_columns?: Array<Intervention_Partner_Update_Column>;
  where?: InputMaybe<Intervention_Partner_Bool_Exp>;
};

/** Ordering options when selecting data from "intervention_partner". */
export type Intervention_Partner_Order_By = {
  id?: InputMaybe<Order_By>;
  name?: InputMaybe<Order_By>;
  user_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: intervention_partner */
export type Intervention_Partner_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "intervention_partner" */
export enum Intervention_Partner_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "intervention_partner" */
export type Intervention_Partner_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  name?: InputMaybe<Scalars['String']>;
  user_id?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "intervention_partner" */
export type Intervention_Partner_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Intervention_Partner_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Intervention_Partner_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  name?: InputMaybe<Scalars['String']>;
  user_id?: InputMaybe<Scalars['String']>;
};

/** update columns of table "intervention_partner" */
export enum Intervention_Partner_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  UserId = 'user_id'
}

export type Intervention_Partner_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Intervention_Partner_Set_Input>;
  /** filter the rows which have to be updated */
  where: Intervention_Partner_Bool_Exp;
};

/** primary key columns input for table: intervention */
export type Intervention_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "intervention" */
export enum Intervention_Select_Column {
  /** column name */
  Cost = 'cost',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  Id = 'id',
  /** column name */
  InterventionPartnerId = 'intervention_partner_id',
  /** column name */
  InterventionTypeId = 'intervention_type_id',
  /** column name */
  IsParkingBanned = 'is_parking_banned',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Note = 'note',
  /** column name */
  RealizationDate = 'realization_date',
  /** column name */
  RequestOrigin = 'request_origin',
  /** column name */
  ScheduledDate = 'scheduled_date',
  /** column name */
  TreeId = 'tree_id',
  /** column name */
  ValidationNote = 'validation_note',
  /** column name */
  VegetatedAreaId = 'vegetated_area_id',
  /** column name */
  WorksiteId = 'worksite_id'
}

/** select "intervention_aggregate_bool_exp_bool_and_arguments_columns" columns of table "intervention" */
export enum Intervention_Select_Column_Intervention_Aggregate_Bool_Exp_Bool_And_Arguments_Columns {
  /** column name */
  IsParkingBanned = 'is_parking_banned'
}

/** select "intervention_aggregate_bool_exp_bool_or_arguments_columns" columns of table "intervention" */
export enum Intervention_Select_Column_Intervention_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns {
  /** column name */
  IsParkingBanned = 'is_parking_banned'
}

/** input type for updating data in table "intervention" */
export type Intervention_Set_Input = {
  cost?: InputMaybe<Scalars['numeric']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  intervention_partner_id?: InputMaybe<Scalars['uuid']>;
  intervention_type_id?: InputMaybe<Scalars['uuid']>;
  is_parking_banned?: InputMaybe<Scalars['Boolean']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  note?: InputMaybe<Scalars['String']>;
  realization_date?: InputMaybe<Scalars['date']>;
  request_origin?: InputMaybe<Scalars['String']>;
  scheduled_date?: InputMaybe<Scalars['date']>;
  tree_id?: InputMaybe<Scalars['uuid']>;
  validation_note?: InputMaybe<Scalars['String']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
  worksite_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type Intervention_Stddev_Fields = {
  __typename?: 'intervention_stddev_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "intervention" */
export type Intervention_Stddev_Order_By = {
  cost?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Intervention_Stddev_Pop_Fields = {
  __typename?: 'intervention_stddev_pop_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "intervention" */
export type Intervention_Stddev_Pop_Order_By = {
  cost?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Intervention_Stddev_Samp_Fields = {
  __typename?: 'intervention_stddev_samp_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "intervention" */
export type Intervention_Stddev_Samp_Order_By = {
  cost?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "intervention" */
export type Intervention_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Intervention_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Intervention_Stream_Cursor_Value_Input = {
  cost?: InputMaybe<Scalars['numeric']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  intervention_partner_id?: InputMaybe<Scalars['uuid']>;
  intervention_type_id?: InputMaybe<Scalars['uuid']>;
  is_parking_banned?: InputMaybe<Scalars['Boolean']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  note?: InputMaybe<Scalars['String']>;
  realization_date?: InputMaybe<Scalars['date']>;
  request_origin?: InputMaybe<Scalars['String']>;
  scheduled_date?: InputMaybe<Scalars['date']>;
  tree_id?: InputMaybe<Scalars['uuid']>;
  validation_note?: InputMaybe<Scalars['String']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
  worksite_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate sum on columns */
export type Intervention_Sum_Fields = {
  __typename?: 'intervention_sum_fields';
  cost?: Maybe<Scalars['numeric']>;
};

/** order by sum() on columns of table "intervention" */
export type Intervention_Sum_Order_By = {
  cost?: InputMaybe<Order_By>;
};

/** columns and relationships of "intervention_type" */
export type Intervention_Type = {
  __typename?: 'intervention_type';
  /** An object relationship */
  family_intervention_type?: Maybe<Family_Intervention_Type>;
  family_intervention_type_id?: Maybe<Scalars['uuid']>;
  id: Scalars['uuid'];
  location_types?: Maybe<Scalars['jsonb']>;
  object_type?: Maybe<Scalars['jsonb']>;
  slug: Scalars['String'];
};


/** columns and relationships of "intervention_type" */
export type Intervention_TypeLocation_TypesArgs = {
  path?: InputMaybe<Scalars['String']>;
};


/** columns and relationships of "intervention_type" */
export type Intervention_TypeObject_TypeArgs = {
  path?: InputMaybe<Scalars['String']>;
};

/** aggregated selection of "intervention_type" */
export type Intervention_Type_Aggregate = {
  __typename?: 'intervention_type_aggregate';
  aggregate?: Maybe<Intervention_Type_Aggregate_Fields>;
  nodes: Array<Intervention_Type>;
};

export type Intervention_Type_Aggregate_Bool_Exp = {
  count?: InputMaybe<Intervention_Type_Aggregate_Bool_Exp_Count>;
};

export type Intervention_Type_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Intervention_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Intervention_Type_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "intervention_type" */
export type Intervention_Type_Aggregate_Fields = {
  __typename?: 'intervention_type_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Intervention_Type_Max_Fields>;
  min?: Maybe<Intervention_Type_Min_Fields>;
};


/** aggregate fields of "intervention_type" */
export type Intervention_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Intervention_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "intervention_type" */
export type Intervention_Type_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Intervention_Type_Max_Order_By>;
  min?: InputMaybe<Intervention_Type_Min_Order_By>;
};

/** append existing jsonb value of filtered columns with new jsonb value */
export type Intervention_Type_Append_Input = {
  location_types?: InputMaybe<Scalars['jsonb']>;
  object_type?: InputMaybe<Scalars['jsonb']>;
};

/** input type for inserting array relation for remote table "intervention_type" */
export type Intervention_Type_Arr_Rel_Insert_Input = {
  data: Array<Intervention_Type_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Intervention_Type_On_Conflict>;
};

/** Boolean expression to filter rows from the table "intervention_type". All fields are combined with a logical 'AND'. */
export type Intervention_Type_Bool_Exp = {
  _and?: InputMaybe<Array<Intervention_Type_Bool_Exp>>;
  _not?: InputMaybe<Intervention_Type_Bool_Exp>;
  _or?: InputMaybe<Array<Intervention_Type_Bool_Exp>>;
  family_intervention_type?: InputMaybe<Family_Intervention_Type_Bool_Exp>;
  family_intervention_type_id?: InputMaybe<Uuid_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  location_types?: InputMaybe<Jsonb_Comparison_Exp>;
  object_type?: InputMaybe<Jsonb_Comparison_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "intervention_type" */
export enum Intervention_Type_Constraint {
  /** unique or primary key constraint on columns "id" */
  InterventionTypePkey = 'intervention_type_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Intervention_Type_Delete_At_Path_Input = {
  location_types?: InputMaybe<Array<Scalars['String']>>;
  object_type?: InputMaybe<Array<Scalars['String']>>;
};

/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
export type Intervention_Type_Delete_Elem_Input = {
  location_types?: InputMaybe<Scalars['Int']>;
  object_type?: InputMaybe<Scalars['Int']>;
};

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Intervention_Type_Delete_Key_Input = {
  location_types?: InputMaybe<Scalars['String']>;
  object_type?: InputMaybe<Scalars['String']>;
};

/** input type for inserting data into table "intervention_type" */
export type Intervention_Type_Insert_Input = {
  family_intervention_type?: InputMaybe<Family_Intervention_Type_Obj_Rel_Insert_Input>;
  family_intervention_type_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  location_types?: InputMaybe<Scalars['jsonb']>;
  object_type?: InputMaybe<Scalars['jsonb']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Intervention_Type_Max_Fields = {
  __typename?: 'intervention_type_max_fields';
  family_intervention_type_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "intervention_type" */
export type Intervention_Type_Max_Order_By = {
  family_intervention_type_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Intervention_Type_Min_Fields = {
  __typename?: 'intervention_type_min_fields';
  family_intervention_type_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "intervention_type" */
export type Intervention_Type_Min_Order_By = {
  family_intervention_type_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "intervention_type" */
export type Intervention_Type_Mutation_Response = {
  __typename?: 'intervention_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Intervention_Type>;
};

/** input type for inserting object relation for remote table "intervention_type" */
export type Intervention_Type_Obj_Rel_Insert_Input = {
  data: Intervention_Type_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Intervention_Type_On_Conflict>;
};

/** on_conflict condition type for table "intervention_type" */
export type Intervention_Type_On_Conflict = {
  constraint: Intervention_Type_Constraint;
  update_columns?: Array<Intervention_Type_Update_Column>;
  where?: InputMaybe<Intervention_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "intervention_type". */
export type Intervention_Type_Order_By = {
  family_intervention_type?: InputMaybe<Family_Intervention_Type_Order_By>;
  family_intervention_type_id?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  location_types?: InputMaybe<Order_By>;
  object_type?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: intervention_type */
export type Intervention_Type_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Intervention_Type_Prepend_Input = {
  location_types?: InputMaybe<Scalars['jsonb']>;
  object_type?: InputMaybe<Scalars['jsonb']>;
};

/** select columns of table "intervention_type" */
export enum Intervention_Type_Select_Column {
  /** column name */
  FamilyInterventionTypeId = 'family_intervention_type_id',
  /** column name */
  Id = 'id',
  /** column name */
  LocationTypes = 'location_types',
  /** column name */
  ObjectType = 'object_type',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "intervention_type" */
export type Intervention_Type_Set_Input = {
  family_intervention_type_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  location_types?: InputMaybe<Scalars['jsonb']>;
  object_type?: InputMaybe<Scalars['jsonb']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "intervention_type" */
export type Intervention_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Intervention_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Intervention_Type_Stream_Cursor_Value_Input = {
  family_intervention_type_id?: InputMaybe<Scalars['uuid']>;
  id?: InputMaybe<Scalars['uuid']>;
  location_types?: InputMaybe<Scalars['jsonb']>;
  object_type?: InputMaybe<Scalars['jsonb']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** update columns of table "intervention_type" */
export enum Intervention_Type_Update_Column {
  /** column name */
  FamilyInterventionTypeId = 'family_intervention_type_id',
  /** column name */
  Id = 'id',
  /** column name */
  LocationTypes = 'location_types',
  /** column name */
  ObjectType = 'object_type',
  /** column name */
  Slug = 'slug'
}

export type Intervention_Type_Updates = {
  /** append existing jsonb value of filtered columns with new jsonb value */
  _append?: InputMaybe<Intervention_Type_Append_Input>;
  /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
  _delete_at_path?: InputMaybe<Intervention_Type_Delete_At_Path_Input>;
  /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
  _delete_elem?: InputMaybe<Intervention_Type_Delete_Elem_Input>;
  /** delete key/value pair or string element. key/value pairs are matched based on their key value */
  _delete_key?: InputMaybe<Intervention_Type_Delete_Key_Input>;
  /** prepend existing jsonb value of filtered columns with new jsonb value */
  _prepend?: InputMaybe<Intervention_Type_Prepend_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Intervention_Type_Set_Input>;
  /** filter the rows which have to be updated */
  where: Intervention_Type_Bool_Exp;
};

/** update columns of table "intervention" */
export enum Intervention_Update_Column {
  /** column name */
  Cost = 'cost',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  Id = 'id',
  /** column name */
  InterventionPartnerId = 'intervention_partner_id',
  /** column name */
  InterventionTypeId = 'intervention_type_id',
  /** column name */
  IsParkingBanned = 'is_parking_banned',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Note = 'note',
  /** column name */
  RealizationDate = 'realization_date',
  /** column name */
  RequestOrigin = 'request_origin',
  /** column name */
  ScheduledDate = 'scheduled_date',
  /** column name */
  TreeId = 'tree_id',
  /** column name */
  ValidationNote = 'validation_note',
  /** column name */
  VegetatedAreaId = 'vegetated_area_id',
  /** column name */
  WorksiteId = 'worksite_id'
}

export type Intervention_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Intervention_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Intervention_Set_Input>;
  /** filter the rows which have to be updated */
  where: Intervention_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Intervention_Var_Pop_Fields = {
  __typename?: 'intervention_var_pop_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "intervention" */
export type Intervention_Var_Pop_Order_By = {
  cost?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Intervention_Var_Samp_Fields = {
  __typename?: 'intervention_var_samp_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "intervention" */
export type Intervention_Var_Samp_Order_By = {
  cost?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Intervention_Variance_Fields = {
  __typename?: 'intervention_variance_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "intervention" */
export type Intervention_Variance_Order_By = {
  cost?: InputMaybe<Order_By>;
};

export type Jsonb_Cast_Exp = {
  String?: InputMaybe<String_Comparison_Exp>;
};

/** Boolean expression to compare columns of type "jsonb". All fields are combined with logical 'AND'. */
export type Jsonb_Comparison_Exp = {
  _cast?: InputMaybe<Jsonb_Cast_Exp>;
  /** is the column contained in the given json value */
  _contained_in?: InputMaybe<Scalars['jsonb']>;
  /** does the column contain the given json value at the top level */
  _contains?: InputMaybe<Scalars['jsonb']>;
  _eq?: InputMaybe<Scalars['jsonb']>;
  _gt?: InputMaybe<Scalars['jsonb']>;
  _gte?: InputMaybe<Scalars['jsonb']>;
  /** does the string exist as a top-level key in the column */
  _has_key?: InputMaybe<Scalars['String']>;
  /** do all of these strings exist as top-level keys in the column */
  _has_keys_all?: InputMaybe<Array<Scalars['String']>>;
  /** do any of these strings exist as top-level keys in the column */
  _has_keys_any?: InputMaybe<Array<Scalars['String']>>;
  _in?: InputMaybe<Array<Scalars['jsonb']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['jsonb']>;
  _lte?: InputMaybe<Scalars['jsonb']>;
  _neq?: InputMaybe<Scalars['jsonb']>;
  _nin?: InputMaybe<Array<Scalars['jsonb']>>;
};

/** columns and relationships of "location" */
export type Location = {
  __typename?: 'location';
  address?: Maybe<Scalars['String']>;
  coords: Scalars['geometry'];
  creation_date: Scalars['timestamptz'];
  data_entry_user_id: Scalars['String'];
  deletion_date?: Maybe<Scalars['timestamptz']>;
  foot_type?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  id_status: Scalars['uuid'];
  /** An array relationship */
  interventions: Array<Intervention>;
  /** An aggregate relationship */
  interventions_aggregate: Intervention_Aggregate;
  inventory_source?: Maybe<Scalars['jsonb']>;
  is_protected?: Maybe<Scalars['Boolean']>;
  landscape_type?: Maybe<Scalars['String']>;
  /** An array relationship */
  location_boundaries: Array<Boundaries_Locations>;
  /** An aggregate relationship */
  location_boundaries_aggregate: Boundaries_Locations_Aggregate;
  /** An object relationship */
  location_status: Location_Status;
  /** An array relationship */
  location_urban_constraints: Array<Location_Urban_Constraint>;
  /** An aggregate relationship */
  location_urban_constraints_aggregate: Location_Urban_Constraint_Aggregate;
  /** An array relationship */
  location_vegetated_area: Array<Vegetated_Areas_Locations>;
  /** An aggregate relationship */
  location_vegetated_area_aggregate: Vegetated_Areas_Locations_Aggregate;
  note?: Maybe<Scalars['String']>;
  organization_id: Scalars['uuid'];
  plantation_type?: Maybe<Scalars['String']>;
  sidewalk_type?: Maybe<Scalars['String']>;
  /** An array relationship */
  tree: Array<Tree>;
  /** An aggregate relationship */
  tree_aggregate: Tree_Aggregate;
  /** An object relationship */
  urban_site?: Maybe<Urban_Site>;
  urban_site_id?: Maybe<Scalars['uuid']>;
};


/** columns and relationships of "location" */
export type LocationInterventionsArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Order_By>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};


/** columns and relationships of "location" */
export type LocationInterventions_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Order_By>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};


/** columns and relationships of "location" */
export type LocationInventory_SourceArgs = {
  path?: InputMaybe<Scalars['String']>;
};


/** columns and relationships of "location" */
export type LocationLocation_BoundariesArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Locations_Order_By>>;
  where?: InputMaybe<Boundaries_Locations_Bool_Exp>;
};


/** columns and relationships of "location" */
export type LocationLocation_Boundaries_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Locations_Order_By>>;
  where?: InputMaybe<Boundaries_Locations_Bool_Exp>;
};


/** columns and relationships of "location" */
export type LocationLocation_Urban_ConstraintsArgs = {
  distinct_on?: InputMaybe<Array<Location_Urban_Constraint_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Urban_Constraint_Order_By>>;
  where?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
};


/** columns and relationships of "location" */
export type LocationLocation_Urban_Constraints_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Location_Urban_Constraint_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Urban_Constraint_Order_By>>;
  where?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
};


/** columns and relationships of "location" */
export type LocationLocation_Vegetated_AreaArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Locations_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
};


/** columns and relationships of "location" */
export type LocationLocation_Vegetated_Area_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Locations_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
};


/** columns and relationships of "location" */
export type LocationTreeArgs = {
  distinct_on?: InputMaybe<Array<Tree_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Tree_Order_By>>;
  where?: InputMaybe<Tree_Bool_Exp>;
};


/** columns and relationships of "location" */
export type LocationTree_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Tree_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Tree_Order_By>>;
  where?: InputMaybe<Tree_Bool_Exp>;
};

/** aggregated selection of "location" */
export type Location_Aggregate = {
  __typename?: 'location_aggregate';
  aggregate?: Maybe<Location_Aggregate_Fields>;
  nodes: Array<Location>;
};

export type Location_Aggregate_Bool_Exp = {
  bool_and?: InputMaybe<Location_Aggregate_Bool_Exp_Bool_And>;
  bool_or?: InputMaybe<Location_Aggregate_Bool_Exp_Bool_Or>;
  count?: InputMaybe<Location_Aggregate_Bool_Exp_Count>;
};

export type Location_Aggregate_Bool_Exp_Bool_And = {
  arguments: Location_Select_Column_Location_Aggregate_Bool_Exp_Bool_And_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Location_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Location_Aggregate_Bool_Exp_Bool_Or = {
  arguments: Location_Select_Column_Location_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Location_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Location_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Location_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Location_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "location" */
export type Location_Aggregate_Fields = {
  __typename?: 'location_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Location_Max_Fields>;
  min?: Maybe<Location_Min_Fields>;
};


/** aggregate fields of "location" */
export type Location_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Location_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "location" */
export type Location_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Location_Max_Order_By>;
  min?: InputMaybe<Location_Min_Order_By>;
};

/** append existing jsonb value of filtered columns with new jsonb value */
export type Location_Append_Input = {
  inventory_source?: InputMaybe<Scalars['jsonb']>;
};

/** input type for inserting array relation for remote table "location" */
export type Location_Arr_Rel_Insert_Input = {
  data: Array<Location_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Location_On_Conflict>;
};

/** Boolean expression to filter rows from the table "location". All fields are combined with a logical 'AND'. */
export type Location_Bool_Exp = {
  _and?: InputMaybe<Array<Location_Bool_Exp>>;
  _not?: InputMaybe<Location_Bool_Exp>;
  _or?: InputMaybe<Array<Location_Bool_Exp>>;
  address?: InputMaybe<String_Comparison_Exp>;
  coords?: InputMaybe<Geometry_Comparison_Exp>;
  creation_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  data_entry_user_id?: InputMaybe<String_Comparison_Exp>;
  deletion_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  foot_type?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  id_status?: InputMaybe<Uuid_Comparison_Exp>;
  interventions?: InputMaybe<Intervention_Bool_Exp>;
  interventions_aggregate?: InputMaybe<Intervention_Aggregate_Bool_Exp>;
  inventory_source?: InputMaybe<Jsonb_Comparison_Exp>;
  is_protected?: InputMaybe<Boolean_Comparison_Exp>;
  landscape_type?: InputMaybe<String_Comparison_Exp>;
  location_boundaries?: InputMaybe<Boundaries_Locations_Bool_Exp>;
  location_boundaries_aggregate?: InputMaybe<Boundaries_Locations_Aggregate_Bool_Exp>;
  location_status?: InputMaybe<Location_Status_Bool_Exp>;
  location_urban_constraints?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
  location_urban_constraints_aggregate?: InputMaybe<Location_Urban_Constraint_Aggregate_Bool_Exp>;
  location_vegetated_area?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
  location_vegetated_area_aggregate?: InputMaybe<Vegetated_Areas_Locations_Aggregate_Bool_Exp>;
  note?: InputMaybe<String_Comparison_Exp>;
  organization_id?: InputMaybe<Uuid_Comparison_Exp>;
  plantation_type?: InputMaybe<String_Comparison_Exp>;
  sidewalk_type?: InputMaybe<String_Comparison_Exp>;
  tree?: InputMaybe<Tree_Bool_Exp>;
  tree_aggregate?: InputMaybe<Tree_Aggregate_Bool_Exp>;
  urban_site?: InputMaybe<Urban_Site_Bool_Exp>;
  urban_site_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "location" */
export enum Location_Constraint {
  /** unique or primary key constraint on columns "coords" */
  LocationCoordsKey = 'location_coords_key',
  /** unique or primary key constraint on columns "id" */
  LocationPkey = 'location_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Location_Delete_At_Path_Input = {
  inventory_source?: InputMaybe<Array<Scalars['String']>>;
};

/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
export type Location_Delete_Elem_Input = {
  inventory_source?: InputMaybe<Scalars['Int']>;
};

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Location_Delete_Key_Input = {
  inventory_source?: InputMaybe<Scalars['String']>;
};

/** input type for inserting data into table "location" */
export type Location_Insert_Input = {
  address?: InputMaybe<Scalars['String']>;
  coords?: InputMaybe<Scalars['geometry']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  deletion_date?: InputMaybe<Scalars['timestamptz']>;
  foot_type?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  id_status?: InputMaybe<Scalars['uuid']>;
  interventions?: InputMaybe<Intervention_Arr_Rel_Insert_Input>;
  inventory_source?: InputMaybe<Scalars['jsonb']>;
  is_protected?: InputMaybe<Scalars['Boolean']>;
  landscape_type?: InputMaybe<Scalars['String']>;
  location_boundaries?: InputMaybe<Boundaries_Locations_Arr_Rel_Insert_Input>;
  location_status?: InputMaybe<Location_Status_Obj_Rel_Insert_Input>;
  location_urban_constraints?: InputMaybe<Location_Urban_Constraint_Arr_Rel_Insert_Input>;
  location_vegetated_area?: InputMaybe<Vegetated_Areas_Locations_Arr_Rel_Insert_Input>;
  note?: InputMaybe<Scalars['String']>;
  organization_id?: InputMaybe<Scalars['uuid']>;
  plantation_type?: InputMaybe<Scalars['String']>;
  sidewalk_type?: InputMaybe<Scalars['String']>;
  tree?: InputMaybe<Tree_Arr_Rel_Insert_Input>;
  urban_site?: InputMaybe<Urban_Site_Obj_Rel_Insert_Input>;
  urban_site_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Location_Max_Fields = {
  __typename?: 'location_max_fields';
  address?: Maybe<Scalars['String']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  deletion_date?: Maybe<Scalars['timestamptz']>;
  foot_type?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  id_status?: Maybe<Scalars['uuid']>;
  landscape_type?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  organization_id?: Maybe<Scalars['uuid']>;
  plantation_type?: Maybe<Scalars['String']>;
  sidewalk_type?: Maybe<Scalars['String']>;
  urban_site_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "location" */
export type Location_Max_Order_By = {
  address?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  deletion_date?: InputMaybe<Order_By>;
  foot_type?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  id_status?: InputMaybe<Order_By>;
  landscape_type?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  organization_id?: InputMaybe<Order_By>;
  plantation_type?: InputMaybe<Order_By>;
  sidewalk_type?: InputMaybe<Order_By>;
  urban_site_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Location_Min_Fields = {
  __typename?: 'location_min_fields';
  address?: Maybe<Scalars['String']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  deletion_date?: Maybe<Scalars['timestamptz']>;
  foot_type?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  id_status?: Maybe<Scalars['uuid']>;
  landscape_type?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  organization_id?: Maybe<Scalars['uuid']>;
  plantation_type?: Maybe<Scalars['String']>;
  sidewalk_type?: Maybe<Scalars['String']>;
  urban_site_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "location" */
export type Location_Min_Order_By = {
  address?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  deletion_date?: InputMaybe<Order_By>;
  foot_type?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  id_status?: InputMaybe<Order_By>;
  landscape_type?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  organization_id?: InputMaybe<Order_By>;
  plantation_type?: InputMaybe<Order_By>;
  sidewalk_type?: InputMaybe<Order_By>;
  urban_site_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "location" */
export type Location_Mutation_Response = {
  __typename?: 'location_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Location>;
};

/** input type for inserting object relation for remote table "location" */
export type Location_Obj_Rel_Insert_Input = {
  data: Location_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Location_On_Conflict>;
};

/** on_conflict condition type for table "location" */
export type Location_On_Conflict = {
  constraint: Location_Constraint;
  update_columns?: Array<Location_Update_Column>;
  where?: InputMaybe<Location_Bool_Exp>;
};

/** Ordering options when selecting data from "location". */
export type Location_Order_By = {
  address?: InputMaybe<Order_By>;
  coords?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  deletion_date?: InputMaybe<Order_By>;
  foot_type?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  id_status?: InputMaybe<Order_By>;
  interventions_aggregate?: InputMaybe<Intervention_Aggregate_Order_By>;
  inventory_source?: InputMaybe<Order_By>;
  is_protected?: InputMaybe<Order_By>;
  landscape_type?: InputMaybe<Order_By>;
  location_boundaries_aggregate?: InputMaybe<Boundaries_Locations_Aggregate_Order_By>;
  location_status?: InputMaybe<Location_Status_Order_By>;
  location_urban_constraints_aggregate?: InputMaybe<Location_Urban_Constraint_Aggregate_Order_By>;
  location_vegetated_area_aggregate?: InputMaybe<Vegetated_Areas_Locations_Aggregate_Order_By>;
  note?: InputMaybe<Order_By>;
  organization_id?: InputMaybe<Order_By>;
  plantation_type?: InputMaybe<Order_By>;
  sidewalk_type?: InputMaybe<Order_By>;
  tree_aggregate?: InputMaybe<Tree_Aggregate_Order_By>;
  urban_site?: InputMaybe<Urban_Site_Order_By>;
  urban_site_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: location */
export type Location_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Location_Prepend_Input = {
  inventory_source?: InputMaybe<Scalars['jsonb']>;
};

/** select columns of table "location" */
export enum Location_Select_Column {
  /** column name */
  Address = 'address',
  /** column name */
  Coords = 'coords',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  DeletionDate = 'deletion_date',
  /** column name */
  FootType = 'foot_type',
  /** column name */
  Id = 'id',
  /** column name */
  IdStatus = 'id_status',
  /** column name */
  InventorySource = 'inventory_source',
  /** column name */
  IsProtected = 'is_protected',
  /** column name */
  LandscapeType = 'landscape_type',
  /** column name */
  Note = 'note',
  /** column name */
  OrganizationId = 'organization_id',
  /** column name */
  PlantationType = 'plantation_type',
  /** column name */
  SidewalkType = 'sidewalk_type',
  /** column name */
  UrbanSiteId = 'urban_site_id'
}

/** select "location_aggregate_bool_exp_bool_and_arguments_columns" columns of table "location" */
export enum Location_Select_Column_Location_Aggregate_Bool_Exp_Bool_And_Arguments_Columns {
  /** column name */
  IsProtected = 'is_protected'
}

/** select "location_aggregate_bool_exp_bool_or_arguments_columns" columns of table "location" */
export enum Location_Select_Column_Location_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns {
  /** column name */
  IsProtected = 'is_protected'
}

/** input type for updating data in table "location" */
export type Location_Set_Input = {
  address?: InputMaybe<Scalars['String']>;
  coords?: InputMaybe<Scalars['geometry']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  deletion_date?: InputMaybe<Scalars['timestamptz']>;
  foot_type?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  id_status?: InputMaybe<Scalars['uuid']>;
  inventory_source?: InputMaybe<Scalars['jsonb']>;
  is_protected?: InputMaybe<Scalars['Boolean']>;
  landscape_type?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  organization_id?: InputMaybe<Scalars['uuid']>;
  plantation_type?: InputMaybe<Scalars['String']>;
  sidewalk_type?: InputMaybe<Scalars['String']>;
  urban_site_id?: InputMaybe<Scalars['uuid']>;
};

/** columns and relationships of "location_status" */
export type Location_Status = {
  __typename?: 'location_status';
  color?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  /** An array relationship */
  locations: Array<Location>;
  /** An aggregate relationship */
  locations_aggregate: Location_Aggregate;
  status: Scalars['String'];
};


/** columns and relationships of "location_status" */
export type Location_StatusLocationsArgs = {
  distinct_on?: InputMaybe<Array<Location_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Order_By>>;
  where?: InputMaybe<Location_Bool_Exp>;
};


/** columns and relationships of "location_status" */
export type Location_StatusLocations_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Location_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Order_By>>;
  where?: InputMaybe<Location_Bool_Exp>;
};

/** aggregated selection of "location_status" */
export type Location_Status_Aggregate = {
  __typename?: 'location_status_aggregate';
  aggregate?: Maybe<Location_Status_Aggregate_Fields>;
  nodes: Array<Location_Status>;
};

/** aggregate fields of "location_status" */
export type Location_Status_Aggregate_Fields = {
  __typename?: 'location_status_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Location_Status_Max_Fields>;
  min?: Maybe<Location_Status_Min_Fields>;
};


/** aggregate fields of "location_status" */
export type Location_Status_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Location_Status_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "location_status". All fields are combined with a logical 'AND'. */
export type Location_Status_Bool_Exp = {
  _and?: InputMaybe<Array<Location_Status_Bool_Exp>>;
  _not?: InputMaybe<Location_Status_Bool_Exp>;
  _or?: InputMaybe<Array<Location_Status_Bool_Exp>>;
  color?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  locations?: InputMaybe<Location_Bool_Exp>;
  locations_aggregate?: InputMaybe<Location_Aggregate_Bool_Exp>;
  status?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "location_status" */
export enum Location_Status_Constraint {
  /** unique or primary key constraint on columns "id" */
  LocationStatusPkey = 'location_status_pkey',
  /** unique or primary key constraint on columns "status" */
  LocationStatusStatusKey = 'location_status_status_key'
}

/** input type for inserting data into table "location_status" */
export type Location_Status_Insert_Input = {
  color?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  locations?: InputMaybe<Location_Arr_Rel_Insert_Input>;
  status?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Location_Status_Max_Fields = {
  __typename?: 'location_status_max_fields';
  color?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  status?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Location_Status_Min_Fields = {
  __typename?: 'location_status_min_fields';
  color?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  status?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "location_status" */
export type Location_Status_Mutation_Response = {
  __typename?: 'location_status_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Location_Status>;
};

/** input type for inserting object relation for remote table "location_status" */
export type Location_Status_Obj_Rel_Insert_Input = {
  data: Location_Status_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Location_Status_On_Conflict>;
};

/** on_conflict condition type for table "location_status" */
export type Location_Status_On_Conflict = {
  constraint: Location_Status_Constraint;
  update_columns?: Array<Location_Status_Update_Column>;
  where?: InputMaybe<Location_Status_Bool_Exp>;
};

/** Ordering options when selecting data from "location_status". */
export type Location_Status_Order_By = {
  color?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  locations_aggregate?: InputMaybe<Location_Aggregate_Order_By>;
  status?: InputMaybe<Order_By>;
};

/** primary key columns input for table: location_status */
export type Location_Status_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "location_status" */
export enum Location_Status_Select_Column {
  /** column name */
  Color = 'color',
  /** column name */
  Id = 'id',
  /** column name */
  Status = 'status'
}

/** input type for updating data in table "location_status" */
export type Location_Status_Set_Input = {
  color?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  status?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "location_status" */
export type Location_Status_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Location_Status_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Location_Status_Stream_Cursor_Value_Input = {
  color?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  status?: InputMaybe<Scalars['String']>;
};

/** update columns of table "location_status" */
export enum Location_Status_Update_Column {
  /** column name */
  Color = 'color',
  /** column name */
  Id = 'id',
  /** column name */
  Status = 'status'
}

export type Location_Status_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Location_Status_Set_Input>;
  /** filter the rows which have to be updated */
  where: Location_Status_Bool_Exp;
};

/** Streaming cursor of the table "location" */
export type Location_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Location_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Location_Stream_Cursor_Value_Input = {
  address?: InputMaybe<Scalars['String']>;
  coords?: InputMaybe<Scalars['geometry']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  deletion_date?: InputMaybe<Scalars['timestamptz']>;
  foot_type?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  id_status?: InputMaybe<Scalars['uuid']>;
  inventory_source?: InputMaybe<Scalars['jsonb']>;
  is_protected?: InputMaybe<Scalars['Boolean']>;
  landscape_type?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  organization_id?: InputMaybe<Scalars['uuid']>;
  plantation_type?: InputMaybe<Scalars['String']>;
  sidewalk_type?: InputMaybe<Scalars['String']>;
  urban_site_id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "location" */
export enum Location_Update_Column {
  /** column name */
  Address = 'address',
  /** column name */
  Coords = 'coords',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  DeletionDate = 'deletion_date',
  /** column name */
  FootType = 'foot_type',
  /** column name */
  Id = 'id',
  /** column name */
  IdStatus = 'id_status',
  /** column name */
  InventorySource = 'inventory_source',
  /** column name */
  IsProtected = 'is_protected',
  /** column name */
  LandscapeType = 'landscape_type',
  /** column name */
  Note = 'note',
  /** column name */
  OrganizationId = 'organization_id',
  /** column name */
  PlantationType = 'plantation_type',
  /** column name */
  SidewalkType = 'sidewalk_type',
  /** column name */
  UrbanSiteId = 'urban_site_id'
}

export type Location_Updates = {
  /** append existing jsonb value of filtered columns with new jsonb value */
  _append?: InputMaybe<Location_Append_Input>;
  /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
  _delete_at_path?: InputMaybe<Location_Delete_At_Path_Input>;
  /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
  _delete_elem?: InputMaybe<Location_Delete_Elem_Input>;
  /** delete key/value pair or string element. key/value pairs are matched based on their key value */
  _delete_key?: InputMaybe<Location_Delete_Key_Input>;
  /** prepend existing jsonb value of filtered columns with new jsonb value */
  _prepend?: InputMaybe<Location_Prepend_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Location_Set_Input>;
  /** filter the rows which have to be updated */
  where: Location_Bool_Exp;
};

/** Join table between location and urban_constraint */
export type Location_Urban_Constraint = {
  __typename?: 'location_urban_constraint';
  id: Scalars['uuid'];
  location_id: Scalars['uuid'];
  /** An object relationship */
  urban_constraint: Urban_Constraint;
  urban_constraint_id: Scalars['uuid'];
};

/** aggregated selection of "location_urban_constraint" */
export type Location_Urban_Constraint_Aggregate = {
  __typename?: 'location_urban_constraint_aggregate';
  aggregate?: Maybe<Location_Urban_Constraint_Aggregate_Fields>;
  nodes: Array<Location_Urban_Constraint>;
};

export type Location_Urban_Constraint_Aggregate_Bool_Exp = {
  count?: InputMaybe<Location_Urban_Constraint_Aggregate_Bool_Exp_Count>;
};

export type Location_Urban_Constraint_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Location_Urban_Constraint_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "location_urban_constraint" */
export type Location_Urban_Constraint_Aggregate_Fields = {
  __typename?: 'location_urban_constraint_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Location_Urban_Constraint_Max_Fields>;
  min?: Maybe<Location_Urban_Constraint_Min_Fields>;
};


/** aggregate fields of "location_urban_constraint" */
export type Location_Urban_Constraint_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Location_Urban_Constraint_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "location_urban_constraint" */
export type Location_Urban_Constraint_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Location_Urban_Constraint_Max_Order_By>;
  min?: InputMaybe<Location_Urban_Constraint_Min_Order_By>;
};

/** input type for inserting array relation for remote table "location_urban_constraint" */
export type Location_Urban_Constraint_Arr_Rel_Insert_Input = {
  data: Array<Location_Urban_Constraint_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Location_Urban_Constraint_On_Conflict>;
};

/** Boolean expression to filter rows from the table "location_urban_constraint". All fields are combined with a logical 'AND'. */
export type Location_Urban_Constraint_Bool_Exp = {
  _and?: InputMaybe<Array<Location_Urban_Constraint_Bool_Exp>>;
  _not?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
  _or?: InputMaybe<Array<Location_Urban_Constraint_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  location_id?: InputMaybe<Uuid_Comparison_Exp>;
  urban_constraint?: InputMaybe<Urban_Constraint_Bool_Exp>;
  urban_constraint_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "location_urban_constraint" */
export enum Location_Urban_Constraint_Constraint {
  /** unique or primary key constraint on columns "id" */
  LocationUrbanConstraintPkey = 'location_urban_constraint_pkey'
}

/** input type for inserting data into table "location_urban_constraint" */
export type Location_Urban_Constraint_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  urban_constraint?: InputMaybe<Urban_Constraint_Obj_Rel_Insert_Input>;
  urban_constraint_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Location_Urban_Constraint_Max_Fields = {
  __typename?: 'location_urban_constraint_max_fields';
  id?: Maybe<Scalars['uuid']>;
  location_id?: Maybe<Scalars['uuid']>;
  urban_constraint_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "location_urban_constraint" */
export type Location_Urban_Constraint_Max_Order_By = {
  id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
  urban_constraint_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Location_Urban_Constraint_Min_Fields = {
  __typename?: 'location_urban_constraint_min_fields';
  id?: Maybe<Scalars['uuid']>;
  location_id?: Maybe<Scalars['uuid']>;
  urban_constraint_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "location_urban_constraint" */
export type Location_Urban_Constraint_Min_Order_By = {
  id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
  urban_constraint_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "location_urban_constraint" */
export type Location_Urban_Constraint_Mutation_Response = {
  __typename?: 'location_urban_constraint_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Location_Urban_Constraint>;
};

/** on_conflict condition type for table "location_urban_constraint" */
export type Location_Urban_Constraint_On_Conflict = {
  constraint: Location_Urban_Constraint_Constraint;
  update_columns?: Array<Location_Urban_Constraint_Update_Column>;
  where?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
};

/** Ordering options when selecting data from "location_urban_constraint". */
export type Location_Urban_Constraint_Order_By = {
  id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
  urban_constraint?: InputMaybe<Urban_Constraint_Order_By>;
  urban_constraint_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: location_urban_constraint */
export type Location_Urban_Constraint_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "location_urban_constraint" */
export enum Location_Urban_Constraint_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  UrbanConstraintId = 'urban_constraint_id'
}

/** input type for updating data in table "location_urban_constraint" */
export type Location_Urban_Constraint_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  urban_constraint_id?: InputMaybe<Scalars['uuid']>;
};

/** Streaming cursor of the table "location_urban_constraint" */
export type Location_Urban_Constraint_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Location_Urban_Constraint_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Location_Urban_Constraint_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  urban_constraint_id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "location_urban_constraint" */
export enum Location_Urban_Constraint_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  UrbanConstraintId = 'urban_constraint_id'
}

export type Location_Urban_Constraint_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Location_Urban_Constraint_Set_Input>;
  /** filter the rows which have to be updated */
  where: Location_Urban_Constraint_Bool_Exp;
};

/** mutation root */
export type Mutation_Root = {
  __typename?: 'mutation_root';
  /** delete data from the table: "analysis_tool" */
  delete_analysis_tool?: Maybe<Analysis_Tool_Mutation_Response>;
  /** delete single row from the table: "analysis_tool" */
  delete_analysis_tool_by_pk?: Maybe<Analysis_Tool>;
  /** delete data from the table: "boundaries_locations" */
  delete_boundaries_locations?: Maybe<Boundaries_Locations_Mutation_Response>;
  /** delete single row from the table: "boundaries_locations" */
  delete_boundaries_locations_by_pk?: Maybe<Boundaries_Locations>;
  /** delete data from the table: "boundaries_vegetated_areas" */
  delete_boundaries_vegetated_areas?: Maybe<Boundaries_Vegetated_Areas_Mutation_Response>;
  /** delete single row from the table: "boundaries_vegetated_areas" */
  delete_boundaries_vegetated_areas_by_pk?: Maybe<Boundaries_Vegetated_Areas>;
  /** delete data from the table: "boundary" */
  delete_boundary?: Maybe<Boundary_Mutation_Response>;
  /** delete single row from the table: "boundary" */
  delete_boundary_by_pk?: Maybe<Boundary>;
  /** delete data from the table: "diagnosis" */
  delete_diagnosis?: Maybe<Diagnosis_Mutation_Response>;
  /** delete data from the table: "diagnosis_analysis_tool" */
  delete_diagnosis_analysis_tool?: Maybe<Diagnosis_Analysis_Tool_Mutation_Response>;
  /** delete single row from the table: "diagnosis_analysis_tool" */
  delete_diagnosis_analysis_tool_by_pk?: Maybe<Diagnosis_Analysis_Tool>;
  /** delete single row from the table: "diagnosis" */
  delete_diagnosis_by_pk?: Maybe<Diagnosis>;
  /** delete data from the table: "diagnosis_pathogen" */
  delete_diagnosis_pathogen?: Maybe<Diagnosis_Pathogen_Mutation_Response>;
  /** delete single row from the table: "diagnosis_pathogen" */
  delete_diagnosis_pathogen_by_pk?: Maybe<Diagnosis_Pathogen>;
  /** delete data from the table: "diagnosis_type" */
  delete_diagnosis_type?: Maybe<Diagnosis_Type_Mutation_Response>;
  /** delete single row from the table: "diagnosis_type" */
  delete_diagnosis_type_by_pk?: Maybe<Diagnosis_Type>;
  /** delete data from the table: "family_intervention_type" */
  delete_family_intervention_type?: Maybe<Family_Intervention_Type_Mutation_Response>;
  /** delete single row from the table: "family_intervention_type" */
  delete_family_intervention_type_by_pk?: Maybe<Family_Intervention_Type>;
  /** delete data from the table: "foot_type" */
  delete_foot_type?: Maybe<Foot_Type_Mutation_Response>;
  /** delete single row from the table: "foot_type" */
  delete_foot_type_by_pk?: Maybe<Foot_Type>;
  /** delete data from the table: "import" */
  delete_import?: Maybe<Import_Mutation_Response>;
  /** delete single row from the table: "import" */
  delete_import_by_pk?: Maybe<Import>;
  /** delete data from the table: "intervention" */
  delete_intervention?: Maybe<Intervention_Mutation_Response>;
  /** delete single row from the table: "intervention" */
  delete_intervention_by_pk?: Maybe<Intervention>;
  /** delete data from the table: "intervention_partner" */
  delete_intervention_partner?: Maybe<Intervention_Partner_Mutation_Response>;
  /** delete single row from the table: "intervention_partner" */
  delete_intervention_partner_by_pk?: Maybe<Intervention_Partner>;
  /** delete data from the table: "intervention_type" */
  delete_intervention_type?: Maybe<Intervention_Type_Mutation_Response>;
  /** delete single row from the table: "intervention_type" */
  delete_intervention_type_by_pk?: Maybe<Intervention_Type>;
  /** delete data from the table: "location" */
  delete_location?: Maybe<Location_Mutation_Response>;
  /** delete single row from the table: "location" */
  delete_location_by_pk?: Maybe<Location>;
  /** delete data from the table: "location_status" */
  delete_location_status?: Maybe<Location_Status_Mutation_Response>;
  /** delete single row from the table: "location_status" */
  delete_location_status_by_pk?: Maybe<Location_Status>;
  /** delete data from the table: "location_urban_constraint" */
  delete_location_urban_constraint?: Maybe<Location_Urban_Constraint_Mutation_Response>;
  /** delete single row from the table: "location_urban_constraint" */
  delete_location_urban_constraint_by_pk?: Maybe<Location_Urban_Constraint>;
  /** delete data from the table: "organization" */
  delete_organization?: Maybe<Organization_Mutation_Response>;
  /** delete single row from the table: "organization" */
  delete_organization_by_pk?: Maybe<Organization>;
  /** delete data from the table: "pathogen" */
  delete_pathogen?: Maybe<Pathogen_Mutation_Response>;
  /** delete single row from the table: "pathogen" */
  delete_pathogen_by_pk?: Maybe<Pathogen>;
  /** delete data from the table: "residential_usage_type" */
  delete_residential_usage_type?: Maybe<Residential_Usage_Type_Mutation_Response>;
  /** delete single row from the table: "residential_usage_type" */
  delete_residential_usage_type_by_pk?: Maybe<Residential_Usage_Type>;
  /** delete data from the table: "taxon" */
  delete_taxon?: Maybe<Taxon_Mutation_Response>;
  /** delete single row from the table: "taxon" */
  delete_taxon_by_pk?: Maybe<Taxon>;
  /** delete data from the table: "tree" */
  delete_tree?: Maybe<Tree_Mutation_Response>;
  /** delete single row from the table: "tree" */
  delete_tree_by_pk?: Maybe<Tree>;
  /** delete data from the table: "urban_constraint" */
  delete_urban_constraint?: Maybe<Urban_Constraint_Mutation_Response>;
  /** delete single row from the table: "urban_constraint" */
  delete_urban_constraint_by_pk?: Maybe<Urban_Constraint>;
  /** delete data from the table: "urban_site" */
  delete_urban_site?: Maybe<Urban_Site_Mutation_Response>;
  /** delete single row from the table: "urban_site" */
  delete_urban_site_by_pk?: Maybe<Urban_Site>;
  /** delete data from the table: "user_entity" */
  delete_user_entity?: Maybe<User_Entity_Mutation_Response>;
  /** delete single row from the table: "user_entity" */
  delete_user_entity_by_pk?: Maybe<User_Entity>;
  /** delete data from the table: "vegetated_area" */
  delete_vegetated_area?: Maybe<Vegetated_Area_Mutation_Response>;
  /** delete single row from the table: "vegetated_area" */
  delete_vegetated_area_by_pk?: Maybe<Vegetated_Area>;
  /** delete data from the table: "vegetated_areas_locations" */
  delete_vegetated_areas_locations?: Maybe<Vegetated_Areas_Locations_Mutation_Response>;
  /** delete single row from the table: "vegetated_areas_locations" */
  delete_vegetated_areas_locations_by_pk?: Maybe<Vegetated_Areas_Locations>;
  /** delete data from the table: "vegetated_areas_residential_usage_types" */
  delete_vegetated_areas_residential_usage_types?: Maybe<Vegetated_Areas_Residential_Usage_Types_Mutation_Response>;
  /** delete single row from the table: "vegetated_areas_residential_usage_types" */
  delete_vegetated_areas_residential_usage_types_by_pk?: Maybe<Vegetated_Areas_Residential_Usage_Types>;
  /** delete data from the table: "vegetated_areas_urban_constraints" */
  delete_vegetated_areas_urban_constraints?: Maybe<Vegetated_Areas_Urban_Constraints_Mutation_Response>;
  /** delete single row from the table: "vegetated_areas_urban_constraints" */
  delete_vegetated_areas_urban_constraints_by_pk?: Maybe<Vegetated_Areas_Urban_Constraints>;
  /** delete data from the table: "worksite" */
  delete_worksite?: Maybe<Worksite_Mutation_Response>;
  /** delete single row from the table: "worksite" */
  delete_worksite_by_pk?: Maybe<Worksite>;
  /** insert data into the table: "analysis_tool" */
  insert_analysis_tool?: Maybe<Analysis_Tool_Mutation_Response>;
  /** insert a single row into the table: "analysis_tool" */
  insert_analysis_tool_one?: Maybe<Analysis_Tool>;
  /** insert data into the table: "boundaries_locations" */
  insert_boundaries_locations?: Maybe<Boundaries_Locations_Mutation_Response>;
  /** insert a single row into the table: "boundaries_locations" */
  insert_boundaries_locations_one?: Maybe<Boundaries_Locations>;
  /** insert data into the table: "boundaries_vegetated_areas" */
  insert_boundaries_vegetated_areas?: Maybe<Boundaries_Vegetated_Areas_Mutation_Response>;
  /** insert a single row into the table: "boundaries_vegetated_areas" */
  insert_boundaries_vegetated_areas_one?: Maybe<Boundaries_Vegetated_Areas>;
  /** insert data into the table: "boundary" */
  insert_boundary?: Maybe<Boundary_Mutation_Response>;
  /** insert a single row into the table: "boundary" */
  insert_boundary_one?: Maybe<Boundary>;
  /** insert data into the table: "diagnosis" */
  insert_diagnosis?: Maybe<Diagnosis_Mutation_Response>;
  /** insert data into the table: "diagnosis_analysis_tool" */
  insert_diagnosis_analysis_tool?: Maybe<Diagnosis_Analysis_Tool_Mutation_Response>;
  /** insert a single row into the table: "diagnosis_analysis_tool" */
  insert_diagnosis_analysis_tool_one?: Maybe<Diagnosis_Analysis_Tool>;
  /** insert a single row into the table: "diagnosis" */
  insert_diagnosis_one?: Maybe<Diagnosis>;
  /** insert data into the table: "diagnosis_pathogen" */
  insert_diagnosis_pathogen?: Maybe<Diagnosis_Pathogen_Mutation_Response>;
  /** insert a single row into the table: "diagnosis_pathogen" */
  insert_diagnosis_pathogen_one?: Maybe<Diagnosis_Pathogen>;
  /** insert data into the table: "diagnosis_type" */
  insert_diagnosis_type?: Maybe<Diagnosis_Type_Mutation_Response>;
  /** insert a single row into the table: "diagnosis_type" */
  insert_diagnosis_type_one?: Maybe<Diagnosis_Type>;
  /** insert data into the table: "family_intervention_type" */
  insert_family_intervention_type?: Maybe<Family_Intervention_Type_Mutation_Response>;
  /** insert a single row into the table: "family_intervention_type" */
  insert_family_intervention_type_one?: Maybe<Family_Intervention_Type>;
  /** insert data into the table: "foot_type" */
  insert_foot_type?: Maybe<Foot_Type_Mutation_Response>;
  /** insert a single row into the table: "foot_type" */
  insert_foot_type_one?: Maybe<Foot_Type>;
  /** insert data into the table: "import" */
  insert_import?: Maybe<Import_Mutation_Response>;
  /** insert a single row into the table: "import" */
  insert_import_one?: Maybe<Import>;
  /** insert data into the table: "intervention" */
  insert_intervention?: Maybe<Intervention_Mutation_Response>;
  /** insert a single row into the table: "intervention" */
  insert_intervention_one?: Maybe<Intervention>;
  /** insert data into the table: "intervention_partner" */
  insert_intervention_partner?: Maybe<Intervention_Partner_Mutation_Response>;
  /** insert a single row into the table: "intervention_partner" */
  insert_intervention_partner_one?: Maybe<Intervention_Partner>;
  /** insert data into the table: "intervention_type" */
  insert_intervention_type?: Maybe<Intervention_Type_Mutation_Response>;
  /** insert a single row into the table: "intervention_type" */
  insert_intervention_type_one?: Maybe<Intervention_Type>;
  /** insert data into the table: "location" */
  insert_location?: Maybe<Location_Mutation_Response>;
  /** insert a single row into the table: "location" */
  insert_location_one?: Maybe<Location>;
  /** insert data into the table: "location_status" */
  insert_location_status?: Maybe<Location_Status_Mutation_Response>;
  /** insert a single row into the table: "location_status" */
  insert_location_status_one?: Maybe<Location_Status>;
  /** insert data into the table: "location_urban_constraint" */
  insert_location_urban_constraint?: Maybe<Location_Urban_Constraint_Mutation_Response>;
  /** insert a single row into the table: "location_urban_constraint" */
  insert_location_urban_constraint_one?: Maybe<Location_Urban_Constraint>;
  /** insert data into the table: "organization" */
  insert_organization?: Maybe<Organization_Mutation_Response>;
  /** insert a single row into the table: "organization" */
  insert_organization_one?: Maybe<Organization>;
  /** insert data into the table: "pathogen" */
  insert_pathogen?: Maybe<Pathogen_Mutation_Response>;
  /** insert a single row into the table: "pathogen" */
  insert_pathogen_one?: Maybe<Pathogen>;
  /** insert data into the table: "residential_usage_type" */
  insert_residential_usage_type?: Maybe<Residential_Usage_Type_Mutation_Response>;
  /** insert a single row into the table: "residential_usage_type" */
  insert_residential_usage_type_one?: Maybe<Residential_Usage_Type>;
  /** insert data into the table: "taxon" */
  insert_taxon?: Maybe<Taxon_Mutation_Response>;
  /** insert a single row into the table: "taxon" */
  insert_taxon_one?: Maybe<Taxon>;
  /** insert data into the table: "tree" */
  insert_tree?: Maybe<Tree_Mutation_Response>;
  /** insert a single row into the table: "tree" */
  insert_tree_one?: Maybe<Tree>;
  /** insert data into the table: "urban_constraint" */
  insert_urban_constraint?: Maybe<Urban_Constraint_Mutation_Response>;
  /** insert a single row into the table: "urban_constraint" */
  insert_urban_constraint_one?: Maybe<Urban_Constraint>;
  /** insert data into the table: "urban_site" */
  insert_urban_site?: Maybe<Urban_Site_Mutation_Response>;
  /** insert a single row into the table: "urban_site" */
  insert_urban_site_one?: Maybe<Urban_Site>;
  /** insert data into the table: "user_entity" */
  insert_user_entity?: Maybe<User_Entity_Mutation_Response>;
  /** insert a single row into the table: "user_entity" */
  insert_user_entity_one?: Maybe<User_Entity>;
  /** insert data into the table: "vegetated_area" */
  insert_vegetated_area?: Maybe<Vegetated_Area_Mutation_Response>;
  /** insert a single row into the table: "vegetated_area" */
  insert_vegetated_area_one?: Maybe<Vegetated_Area>;
  /** insert data into the table: "vegetated_areas_locations" */
  insert_vegetated_areas_locations?: Maybe<Vegetated_Areas_Locations_Mutation_Response>;
  /** insert a single row into the table: "vegetated_areas_locations" */
  insert_vegetated_areas_locations_one?: Maybe<Vegetated_Areas_Locations>;
  /** insert data into the table: "vegetated_areas_residential_usage_types" */
  insert_vegetated_areas_residential_usage_types?: Maybe<Vegetated_Areas_Residential_Usage_Types_Mutation_Response>;
  /** insert a single row into the table: "vegetated_areas_residential_usage_types" */
  insert_vegetated_areas_residential_usage_types_one?: Maybe<Vegetated_Areas_Residential_Usage_Types>;
  /** insert data into the table: "vegetated_areas_urban_constraints" */
  insert_vegetated_areas_urban_constraints?: Maybe<Vegetated_Areas_Urban_Constraints_Mutation_Response>;
  /** insert a single row into the table: "vegetated_areas_urban_constraints" */
  insert_vegetated_areas_urban_constraints_one?: Maybe<Vegetated_Areas_Urban_Constraints>;
  /** insert data into the table: "worksite" */
  insert_worksite?: Maybe<Worksite_Mutation_Response>;
  /** insert a single row into the table: "worksite" */
  insert_worksite_one?: Maybe<Worksite>;
  /** update data of the table: "analysis_tool" */
  update_analysis_tool?: Maybe<Analysis_Tool_Mutation_Response>;
  /** update single row of the table: "analysis_tool" */
  update_analysis_tool_by_pk?: Maybe<Analysis_Tool>;
  /** update multiples rows of table: "analysis_tool" */
  update_analysis_tool_many?: Maybe<Array<Maybe<Analysis_Tool_Mutation_Response>>>;
  /** update data of the table: "boundaries_locations" */
  update_boundaries_locations?: Maybe<Boundaries_Locations_Mutation_Response>;
  /** update single row of the table: "boundaries_locations" */
  update_boundaries_locations_by_pk?: Maybe<Boundaries_Locations>;
  /** update multiples rows of table: "boundaries_locations" */
  update_boundaries_locations_many?: Maybe<Array<Maybe<Boundaries_Locations_Mutation_Response>>>;
  /** update data of the table: "boundaries_vegetated_areas" */
  update_boundaries_vegetated_areas?: Maybe<Boundaries_Vegetated_Areas_Mutation_Response>;
  /** update single row of the table: "boundaries_vegetated_areas" */
  update_boundaries_vegetated_areas_by_pk?: Maybe<Boundaries_Vegetated_Areas>;
  /** update multiples rows of table: "boundaries_vegetated_areas" */
  update_boundaries_vegetated_areas_many?: Maybe<Array<Maybe<Boundaries_Vegetated_Areas_Mutation_Response>>>;
  /** update data of the table: "boundary" */
  update_boundary?: Maybe<Boundary_Mutation_Response>;
  /** update single row of the table: "boundary" */
  update_boundary_by_pk?: Maybe<Boundary>;
  /** update multiples rows of table: "boundary" */
  update_boundary_many?: Maybe<Array<Maybe<Boundary_Mutation_Response>>>;
  /** update data of the table: "diagnosis" */
  update_diagnosis?: Maybe<Diagnosis_Mutation_Response>;
  /** update data of the table: "diagnosis_analysis_tool" */
  update_diagnosis_analysis_tool?: Maybe<Diagnosis_Analysis_Tool_Mutation_Response>;
  /** update single row of the table: "diagnosis_analysis_tool" */
  update_diagnosis_analysis_tool_by_pk?: Maybe<Diagnosis_Analysis_Tool>;
  /** update multiples rows of table: "diagnosis_analysis_tool" */
  update_diagnosis_analysis_tool_many?: Maybe<Array<Maybe<Diagnosis_Analysis_Tool_Mutation_Response>>>;
  /** update single row of the table: "diagnosis" */
  update_diagnosis_by_pk?: Maybe<Diagnosis>;
  /** update multiples rows of table: "diagnosis" */
  update_diagnosis_many?: Maybe<Array<Maybe<Diagnosis_Mutation_Response>>>;
  /** update data of the table: "diagnosis_pathogen" */
  update_diagnosis_pathogen?: Maybe<Diagnosis_Pathogen_Mutation_Response>;
  /** update single row of the table: "diagnosis_pathogen" */
  update_diagnosis_pathogen_by_pk?: Maybe<Diagnosis_Pathogen>;
  /** update multiples rows of table: "diagnosis_pathogen" */
  update_diagnosis_pathogen_many?: Maybe<Array<Maybe<Diagnosis_Pathogen_Mutation_Response>>>;
  /** update data of the table: "diagnosis_type" */
  update_diagnosis_type?: Maybe<Diagnosis_Type_Mutation_Response>;
  /** update single row of the table: "diagnosis_type" */
  update_diagnosis_type_by_pk?: Maybe<Diagnosis_Type>;
  /** update multiples rows of table: "diagnosis_type" */
  update_diagnosis_type_many?: Maybe<Array<Maybe<Diagnosis_Type_Mutation_Response>>>;
  /** update data of the table: "family_intervention_type" */
  update_family_intervention_type?: Maybe<Family_Intervention_Type_Mutation_Response>;
  /** update single row of the table: "family_intervention_type" */
  update_family_intervention_type_by_pk?: Maybe<Family_Intervention_Type>;
  /** update multiples rows of table: "family_intervention_type" */
  update_family_intervention_type_many?: Maybe<Array<Maybe<Family_Intervention_Type_Mutation_Response>>>;
  /** update data of the table: "foot_type" */
  update_foot_type?: Maybe<Foot_Type_Mutation_Response>;
  /** update single row of the table: "foot_type" */
  update_foot_type_by_pk?: Maybe<Foot_Type>;
  /** update multiples rows of table: "foot_type" */
  update_foot_type_many?: Maybe<Array<Maybe<Foot_Type_Mutation_Response>>>;
  /** update data of the table: "import" */
  update_import?: Maybe<Import_Mutation_Response>;
  /** update single row of the table: "import" */
  update_import_by_pk?: Maybe<Import>;
  /** update multiples rows of table: "import" */
  update_import_many?: Maybe<Array<Maybe<Import_Mutation_Response>>>;
  /** update data of the table: "intervention" */
  update_intervention?: Maybe<Intervention_Mutation_Response>;
  /** update single row of the table: "intervention" */
  update_intervention_by_pk?: Maybe<Intervention>;
  /** update multiples rows of table: "intervention" */
  update_intervention_many?: Maybe<Array<Maybe<Intervention_Mutation_Response>>>;
  /** update data of the table: "intervention_partner" */
  update_intervention_partner?: Maybe<Intervention_Partner_Mutation_Response>;
  /** update single row of the table: "intervention_partner" */
  update_intervention_partner_by_pk?: Maybe<Intervention_Partner>;
  /** update multiples rows of table: "intervention_partner" */
  update_intervention_partner_many?: Maybe<Array<Maybe<Intervention_Partner_Mutation_Response>>>;
  /** update data of the table: "intervention_type" */
  update_intervention_type?: Maybe<Intervention_Type_Mutation_Response>;
  /** update single row of the table: "intervention_type" */
  update_intervention_type_by_pk?: Maybe<Intervention_Type>;
  /** update multiples rows of table: "intervention_type" */
  update_intervention_type_many?: Maybe<Array<Maybe<Intervention_Type_Mutation_Response>>>;
  /** update data of the table: "location" */
  update_location?: Maybe<Location_Mutation_Response>;
  /** update single row of the table: "location" */
  update_location_by_pk?: Maybe<Location>;
  /** update multiples rows of table: "location" */
  update_location_many?: Maybe<Array<Maybe<Location_Mutation_Response>>>;
  /** update data of the table: "location_status" */
  update_location_status?: Maybe<Location_Status_Mutation_Response>;
  /** update single row of the table: "location_status" */
  update_location_status_by_pk?: Maybe<Location_Status>;
  /** update multiples rows of table: "location_status" */
  update_location_status_many?: Maybe<Array<Maybe<Location_Status_Mutation_Response>>>;
  /** update data of the table: "location_urban_constraint" */
  update_location_urban_constraint?: Maybe<Location_Urban_Constraint_Mutation_Response>;
  /** update single row of the table: "location_urban_constraint" */
  update_location_urban_constraint_by_pk?: Maybe<Location_Urban_Constraint>;
  /** update multiples rows of table: "location_urban_constraint" */
  update_location_urban_constraint_many?: Maybe<Array<Maybe<Location_Urban_Constraint_Mutation_Response>>>;
  /** update data of the table: "organization" */
  update_organization?: Maybe<Organization_Mutation_Response>;
  /** update single row of the table: "organization" */
  update_organization_by_pk?: Maybe<Organization>;
  /** update multiples rows of table: "organization" */
  update_organization_many?: Maybe<Array<Maybe<Organization_Mutation_Response>>>;
  /** update data of the table: "pathogen" */
  update_pathogen?: Maybe<Pathogen_Mutation_Response>;
  /** update single row of the table: "pathogen" */
  update_pathogen_by_pk?: Maybe<Pathogen>;
  /** update multiples rows of table: "pathogen" */
  update_pathogen_many?: Maybe<Array<Maybe<Pathogen_Mutation_Response>>>;
  /** update data of the table: "residential_usage_type" */
  update_residential_usage_type?: Maybe<Residential_Usage_Type_Mutation_Response>;
  /** update single row of the table: "residential_usage_type" */
  update_residential_usage_type_by_pk?: Maybe<Residential_Usage_Type>;
  /** update multiples rows of table: "residential_usage_type" */
  update_residential_usage_type_many?: Maybe<Array<Maybe<Residential_Usage_Type_Mutation_Response>>>;
  /** update data of the table: "taxon" */
  update_taxon?: Maybe<Taxon_Mutation_Response>;
  /** update single row of the table: "taxon" */
  update_taxon_by_pk?: Maybe<Taxon>;
  /** update multiples rows of table: "taxon" */
  update_taxon_many?: Maybe<Array<Maybe<Taxon_Mutation_Response>>>;
  /** update data of the table: "tree" */
  update_tree?: Maybe<Tree_Mutation_Response>;
  /** update single row of the table: "tree" */
  update_tree_by_pk?: Maybe<Tree>;
  /** update multiples rows of table: "tree" */
  update_tree_many?: Maybe<Array<Maybe<Tree_Mutation_Response>>>;
  /** update data of the table: "urban_constraint" */
  update_urban_constraint?: Maybe<Urban_Constraint_Mutation_Response>;
  /** update single row of the table: "urban_constraint" */
  update_urban_constraint_by_pk?: Maybe<Urban_Constraint>;
  /** update multiples rows of table: "urban_constraint" */
  update_urban_constraint_many?: Maybe<Array<Maybe<Urban_Constraint_Mutation_Response>>>;
  /** update data of the table: "urban_site" */
  update_urban_site?: Maybe<Urban_Site_Mutation_Response>;
  /** update single row of the table: "urban_site" */
  update_urban_site_by_pk?: Maybe<Urban_Site>;
  /** update multiples rows of table: "urban_site" */
  update_urban_site_many?: Maybe<Array<Maybe<Urban_Site_Mutation_Response>>>;
  /** update data of the table: "user_entity" */
  update_user_entity?: Maybe<User_Entity_Mutation_Response>;
  /** update single row of the table: "user_entity" */
  update_user_entity_by_pk?: Maybe<User_Entity>;
  /** update multiples rows of table: "user_entity" */
  update_user_entity_many?: Maybe<Array<Maybe<User_Entity_Mutation_Response>>>;
  /** update data of the table: "vegetated_area" */
  update_vegetated_area?: Maybe<Vegetated_Area_Mutation_Response>;
  /** update single row of the table: "vegetated_area" */
  update_vegetated_area_by_pk?: Maybe<Vegetated_Area>;
  /** update multiples rows of table: "vegetated_area" */
  update_vegetated_area_many?: Maybe<Array<Maybe<Vegetated_Area_Mutation_Response>>>;
  /** update data of the table: "vegetated_areas_locations" */
  update_vegetated_areas_locations?: Maybe<Vegetated_Areas_Locations_Mutation_Response>;
  /** update single row of the table: "vegetated_areas_locations" */
  update_vegetated_areas_locations_by_pk?: Maybe<Vegetated_Areas_Locations>;
  /** update multiples rows of table: "vegetated_areas_locations" */
  update_vegetated_areas_locations_many?: Maybe<Array<Maybe<Vegetated_Areas_Locations_Mutation_Response>>>;
  /** update data of the table: "vegetated_areas_residential_usage_types" */
  update_vegetated_areas_residential_usage_types?: Maybe<Vegetated_Areas_Residential_Usage_Types_Mutation_Response>;
  /** update single row of the table: "vegetated_areas_residential_usage_types" */
  update_vegetated_areas_residential_usage_types_by_pk?: Maybe<Vegetated_Areas_Residential_Usage_Types>;
  /** update multiples rows of table: "vegetated_areas_residential_usage_types" */
  update_vegetated_areas_residential_usage_types_many?: Maybe<Array<Maybe<Vegetated_Areas_Residential_Usage_Types_Mutation_Response>>>;
  /** update data of the table: "vegetated_areas_urban_constraints" */
  update_vegetated_areas_urban_constraints?: Maybe<Vegetated_Areas_Urban_Constraints_Mutation_Response>;
  /** update single row of the table: "vegetated_areas_urban_constraints" */
  update_vegetated_areas_urban_constraints_by_pk?: Maybe<Vegetated_Areas_Urban_Constraints>;
  /** update multiples rows of table: "vegetated_areas_urban_constraints" */
  update_vegetated_areas_urban_constraints_many?: Maybe<Array<Maybe<Vegetated_Areas_Urban_Constraints_Mutation_Response>>>;
  /** update data of the table: "worksite" */
  update_worksite?: Maybe<Worksite_Mutation_Response>;
  /** update single row of the table: "worksite" */
  update_worksite_by_pk?: Maybe<Worksite>;
  /** update multiples rows of table: "worksite" */
  update_worksite_many?: Maybe<Array<Maybe<Worksite_Mutation_Response>>>;
};


/** mutation root */
export type Mutation_RootDelete_Analysis_ToolArgs = {
  where: Analysis_Tool_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Analysis_Tool_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Boundaries_LocationsArgs = {
  where: Boundaries_Locations_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Boundaries_Locations_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Boundaries_Vegetated_AreasArgs = {
  where: Boundaries_Vegetated_Areas_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Boundaries_Vegetated_Areas_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_BoundaryArgs = {
  where: Boundary_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Boundary_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_DiagnosisArgs = {
  where: Diagnosis_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Diagnosis_Analysis_ToolArgs = {
  where: Diagnosis_Analysis_Tool_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Diagnosis_Analysis_Tool_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Diagnosis_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Diagnosis_PathogenArgs = {
  where: Diagnosis_Pathogen_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Diagnosis_Pathogen_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Diagnosis_TypeArgs = {
  where: Diagnosis_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Diagnosis_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Family_Intervention_TypeArgs = {
  where: Family_Intervention_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Family_Intervention_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Foot_TypeArgs = {
  where: Foot_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Foot_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_ImportArgs = {
  where: Import_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Import_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_InterventionArgs = {
  where: Intervention_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Intervention_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Intervention_PartnerArgs = {
  where: Intervention_Partner_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Intervention_Partner_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Intervention_TypeArgs = {
  where: Intervention_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Intervention_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_LocationArgs = {
  where: Location_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Location_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Location_StatusArgs = {
  where: Location_Status_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Location_Status_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Location_Urban_ConstraintArgs = {
  where: Location_Urban_Constraint_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Location_Urban_Constraint_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_OrganizationArgs = {
  where: Organization_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Organization_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_PathogenArgs = {
  where: Pathogen_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Pathogen_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Residential_Usage_TypeArgs = {
  where: Residential_Usage_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Residential_Usage_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_TaxonArgs = {
  where: Taxon_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Taxon_By_PkArgs = {
  id: Scalars['Int'];
};


/** mutation root */
export type Mutation_RootDelete_TreeArgs = {
  where: Tree_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Tree_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Urban_ConstraintArgs = {
  where: Urban_Constraint_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Urban_Constraint_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Urban_SiteArgs = {
  where: Urban_Site_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Urban_Site_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_User_EntityArgs = {
  where: User_Entity_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_User_Entity_By_PkArgs = {
  id: Scalars['String'];
};


/** mutation root */
export type Mutation_RootDelete_Vegetated_AreaArgs = {
  where: Vegetated_Area_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Vegetated_Area_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Vegetated_Areas_LocationsArgs = {
  where: Vegetated_Areas_Locations_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Vegetated_Areas_Locations_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Vegetated_Areas_Residential_Usage_TypesArgs = {
  where: Vegetated_Areas_Residential_Usage_Types_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Vegetated_Areas_Residential_Usage_Types_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Vegetated_Areas_Urban_ConstraintsArgs = {
  where: Vegetated_Areas_Urban_Constraints_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Vegetated_Areas_Urban_Constraints_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_WorksiteArgs = {
  where: Worksite_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Worksite_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootInsert_Analysis_ToolArgs = {
  objects: Array<Analysis_Tool_Insert_Input>;
  on_conflict?: InputMaybe<Analysis_Tool_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Analysis_Tool_OneArgs = {
  object: Analysis_Tool_Insert_Input;
  on_conflict?: InputMaybe<Analysis_Tool_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Boundaries_LocationsArgs = {
  objects: Array<Boundaries_Locations_Insert_Input>;
  on_conflict?: InputMaybe<Boundaries_Locations_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Boundaries_Locations_OneArgs = {
  object: Boundaries_Locations_Insert_Input;
  on_conflict?: InputMaybe<Boundaries_Locations_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Boundaries_Vegetated_AreasArgs = {
  objects: Array<Boundaries_Vegetated_Areas_Insert_Input>;
  on_conflict?: InputMaybe<Boundaries_Vegetated_Areas_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Boundaries_Vegetated_Areas_OneArgs = {
  object: Boundaries_Vegetated_Areas_Insert_Input;
  on_conflict?: InputMaybe<Boundaries_Vegetated_Areas_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_BoundaryArgs = {
  objects: Array<Boundary_Insert_Input>;
  on_conflict?: InputMaybe<Boundary_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Boundary_OneArgs = {
  object: Boundary_Insert_Input;
  on_conflict?: InputMaybe<Boundary_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_DiagnosisArgs = {
  objects: Array<Diagnosis_Insert_Input>;
  on_conflict?: InputMaybe<Diagnosis_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Diagnosis_Analysis_ToolArgs = {
  objects: Array<Diagnosis_Analysis_Tool_Insert_Input>;
  on_conflict?: InputMaybe<Diagnosis_Analysis_Tool_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Diagnosis_Analysis_Tool_OneArgs = {
  object: Diagnosis_Analysis_Tool_Insert_Input;
  on_conflict?: InputMaybe<Diagnosis_Analysis_Tool_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Diagnosis_OneArgs = {
  object: Diagnosis_Insert_Input;
  on_conflict?: InputMaybe<Diagnosis_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Diagnosis_PathogenArgs = {
  objects: Array<Diagnosis_Pathogen_Insert_Input>;
  on_conflict?: InputMaybe<Diagnosis_Pathogen_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Diagnosis_Pathogen_OneArgs = {
  object: Diagnosis_Pathogen_Insert_Input;
  on_conflict?: InputMaybe<Diagnosis_Pathogen_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Diagnosis_TypeArgs = {
  objects: Array<Diagnosis_Type_Insert_Input>;
  on_conflict?: InputMaybe<Diagnosis_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Diagnosis_Type_OneArgs = {
  object: Diagnosis_Type_Insert_Input;
  on_conflict?: InputMaybe<Diagnosis_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Family_Intervention_TypeArgs = {
  objects: Array<Family_Intervention_Type_Insert_Input>;
  on_conflict?: InputMaybe<Family_Intervention_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Family_Intervention_Type_OneArgs = {
  object: Family_Intervention_Type_Insert_Input;
  on_conflict?: InputMaybe<Family_Intervention_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Foot_TypeArgs = {
  objects: Array<Foot_Type_Insert_Input>;
  on_conflict?: InputMaybe<Foot_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Foot_Type_OneArgs = {
  object: Foot_Type_Insert_Input;
  on_conflict?: InputMaybe<Foot_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ImportArgs = {
  objects: Array<Import_Insert_Input>;
  on_conflict?: InputMaybe<Import_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Import_OneArgs = {
  object: Import_Insert_Input;
  on_conflict?: InputMaybe<Import_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_InterventionArgs = {
  objects: Array<Intervention_Insert_Input>;
  on_conflict?: InputMaybe<Intervention_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Intervention_OneArgs = {
  object: Intervention_Insert_Input;
  on_conflict?: InputMaybe<Intervention_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Intervention_PartnerArgs = {
  objects: Array<Intervention_Partner_Insert_Input>;
  on_conflict?: InputMaybe<Intervention_Partner_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Intervention_Partner_OneArgs = {
  object: Intervention_Partner_Insert_Input;
  on_conflict?: InputMaybe<Intervention_Partner_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Intervention_TypeArgs = {
  objects: Array<Intervention_Type_Insert_Input>;
  on_conflict?: InputMaybe<Intervention_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Intervention_Type_OneArgs = {
  object: Intervention_Type_Insert_Input;
  on_conflict?: InputMaybe<Intervention_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_LocationArgs = {
  objects: Array<Location_Insert_Input>;
  on_conflict?: InputMaybe<Location_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Location_OneArgs = {
  object: Location_Insert_Input;
  on_conflict?: InputMaybe<Location_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Location_StatusArgs = {
  objects: Array<Location_Status_Insert_Input>;
  on_conflict?: InputMaybe<Location_Status_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Location_Status_OneArgs = {
  object: Location_Status_Insert_Input;
  on_conflict?: InputMaybe<Location_Status_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Location_Urban_ConstraintArgs = {
  objects: Array<Location_Urban_Constraint_Insert_Input>;
  on_conflict?: InputMaybe<Location_Urban_Constraint_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Location_Urban_Constraint_OneArgs = {
  object: Location_Urban_Constraint_Insert_Input;
  on_conflict?: InputMaybe<Location_Urban_Constraint_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_OrganizationArgs = {
  objects: Array<Organization_Insert_Input>;
  on_conflict?: InputMaybe<Organization_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Organization_OneArgs = {
  object: Organization_Insert_Input;
  on_conflict?: InputMaybe<Organization_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_PathogenArgs = {
  objects: Array<Pathogen_Insert_Input>;
  on_conflict?: InputMaybe<Pathogen_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Pathogen_OneArgs = {
  object: Pathogen_Insert_Input;
  on_conflict?: InputMaybe<Pathogen_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Residential_Usage_TypeArgs = {
  objects: Array<Residential_Usage_Type_Insert_Input>;
  on_conflict?: InputMaybe<Residential_Usage_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Residential_Usage_Type_OneArgs = {
  object: Residential_Usage_Type_Insert_Input;
  on_conflict?: InputMaybe<Residential_Usage_Type_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_TaxonArgs = {
  objects: Array<Taxon_Insert_Input>;
  on_conflict?: InputMaybe<Taxon_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Taxon_OneArgs = {
  object: Taxon_Insert_Input;
  on_conflict?: InputMaybe<Taxon_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_TreeArgs = {
  objects: Array<Tree_Insert_Input>;
  on_conflict?: InputMaybe<Tree_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Tree_OneArgs = {
  object: Tree_Insert_Input;
  on_conflict?: InputMaybe<Tree_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Urban_ConstraintArgs = {
  objects: Array<Urban_Constraint_Insert_Input>;
  on_conflict?: InputMaybe<Urban_Constraint_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Urban_Constraint_OneArgs = {
  object: Urban_Constraint_Insert_Input;
  on_conflict?: InputMaybe<Urban_Constraint_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Urban_SiteArgs = {
  objects: Array<Urban_Site_Insert_Input>;
  on_conflict?: InputMaybe<Urban_Site_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Urban_Site_OneArgs = {
  object: Urban_Site_Insert_Input;
  on_conflict?: InputMaybe<Urban_Site_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_EntityArgs = {
  objects: Array<User_Entity_Insert_Input>;
  on_conflict?: InputMaybe<User_Entity_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_User_Entity_OneArgs = {
  object: User_Entity_Insert_Input;
  on_conflict?: InputMaybe<User_Entity_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Vegetated_AreaArgs = {
  objects: Array<Vegetated_Area_Insert_Input>;
  on_conflict?: InputMaybe<Vegetated_Area_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Vegetated_Area_OneArgs = {
  object: Vegetated_Area_Insert_Input;
  on_conflict?: InputMaybe<Vegetated_Area_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Vegetated_Areas_LocationsArgs = {
  objects: Array<Vegetated_Areas_Locations_Insert_Input>;
  on_conflict?: InputMaybe<Vegetated_Areas_Locations_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Vegetated_Areas_Locations_OneArgs = {
  object: Vegetated_Areas_Locations_Insert_Input;
  on_conflict?: InputMaybe<Vegetated_Areas_Locations_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Vegetated_Areas_Residential_Usage_TypesArgs = {
  objects: Array<Vegetated_Areas_Residential_Usage_Types_Insert_Input>;
  on_conflict?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Vegetated_Areas_Residential_Usage_Types_OneArgs = {
  object: Vegetated_Areas_Residential_Usage_Types_Insert_Input;
  on_conflict?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Vegetated_Areas_Urban_ConstraintsArgs = {
  objects: Array<Vegetated_Areas_Urban_Constraints_Insert_Input>;
  on_conflict?: InputMaybe<Vegetated_Areas_Urban_Constraints_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Vegetated_Areas_Urban_Constraints_OneArgs = {
  object: Vegetated_Areas_Urban_Constraints_Insert_Input;
  on_conflict?: InputMaybe<Vegetated_Areas_Urban_Constraints_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_WorksiteArgs = {
  objects: Array<Worksite_Insert_Input>;
  on_conflict?: InputMaybe<Worksite_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Worksite_OneArgs = {
  object: Worksite_Insert_Input;
  on_conflict?: InputMaybe<Worksite_On_Conflict>;
};


/** mutation root */
export type Mutation_RootUpdate_Analysis_ToolArgs = {
  _set?: InputMaybe<Analysis_Tool_Set_Input>;
  where: Analysis_Tool_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Analysis_Tool_By_PkArgs = {
  _set?: InputMaybe<Analysis_Tool_Set_Input>;
  pk_columns: Analysis_Tool_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Analysis_Tool_ManyArgs = {
  updates: Array<Analysis_Tool_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Boundaries_LocationsArgs = {
  _set?: InputMaybe<Boundaries_Locations_Set_Input>;
  where: Boundaries_Locations_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Boundaries_Locations_By_PkArgs = {
  _set?: InputMaybe<Boundaries_Locations_Set_Input>;
  pk_columns: Boundaries_Locations_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Boundaries_Locations_ManyArgs = {
  updates: Array<Boundaries_Locations_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Boundaries_Vegetated_AreasArgs = {
  _set?: InputMaybe<Boundaries_Vegetated_Areas_Set_Input>;
  where: Boundaries_Vegetated_Areas_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Boundaries_Vegetated_Areas_By_PkArgs = {
  _set?: InputMaybe<Boundaries_Vegetated_Areas_Set_Input>;
  pk_columns: Boundaries_Vegetated_Areas_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Boundaries_Vegetated_Areas_ManyArgs = {
  updates: Array<Boundaries_Vegetated_Areas_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_BoundaryArgs = {
  _set?: InputMaybe<Boundary_Set_Input>;
  where: Boundary_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Boundary_By_PkArgs = {
  _set?: InputMaybe<Boundary_Set_Input>;
  pk_columns: Boundary_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Boundary_ManyArgs = {
  updates: Array<Boundary_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_DiagnosisArgs = {
  _set?: InputMaybe<Diagnosis_Set_Input>;
  where: Diagnosis_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_Analysis_ToolArgs = {
  _set?: InputMaybe<Diagnosis_Analysis_Tool_Set_Input>;
  where: Diagnosis_Analysis_Tool_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_Analysis_Tool_By_PkArgs = {
  _set?: InputMaybe<Diagnosis_Analysis_Tool_Set_Input>;
  pk_columns: Diagnosis_Analysis_Tool_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_Analysis_Tool_ManyArgs = {
  updates: Array<Diagnosis_Analysis_Tool_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_By_PkArgs = {
  _set?: InputMaybe<Diagnosis_Set_Input>;
  pk_columns: Diagnosis_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_ManyArgs = {
  updates: Array<Diagnosis_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_PathogenArgs = {
  _set?: InputMaybe<Diagnosis_Pathogen_Set_Input>;
  where: Diagnosis_Pathogen_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_Pathogen_By_PkArgs = {
  _set?: InputMaybe<Diagnosis_Pathogen_Set_Input>;
  pk_columns: Diagnosis_Pathogen_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_Pathogen_ManyArgs = {
  updates: Array<Diagnosis_Pathogen_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_TypeArgs = {
  _set?: InputMaybe<Diagnosis_Type_Set_Input>;
  where: Diagnosis_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_Type_By_PkArgs = {
  _set?: InputMaybe<Diagnosis_Type_Set_Input>;
  pk_columns: Diagnosis_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Diagnosis_Type_ManyArgs = {
  updates: Array<Diagnosis_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Family_Intervention_TypeArgs = {
  _set?: InputMaybe<Family_Intervention_Type_Set_Input>;
  where: Family_Intervention_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Family_Intervention_Type_By_PkArgs = {
  _set?: InputMaybe<Family_Intervention_Type_Set_Input>;
  pk_columns: Family_Intervention_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Family_Intervention_Type_ManyArgs = {
  updates: Array<Family_Intervention_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Foot_TypeArgs = {
  _set?: InputMaybe<Foot_Type_Set_Input>;
  where: Foot_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Foot_Type_By_PkArgs = {
  _set?: InputMaybe<Foot_Type_Set_Input>;
  pk_columns: Foot_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Foot_Type_ManyArgs = {
  updates: Array<Foot_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_ImportArgs = {
  _inc?: InputMaybe<Import_Inc_Input>;
  _set?: InputMaybe<Import_Set_Input>;
  where: Import_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Import_By_PkArgs = {
  _inc?: InputMaybe<Import_Inc_Input>;
  _set?: InputMaybe<Import_Set_Input>;
  pk_columns: Import_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Import_ManyArgs = {
  updates: Array<Import_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_InterventionArgs = {
  _inc?: InputMaybe<Intervention_Inc_Input>;
  _set?: InputMaybe<Intervention_Set_Input>;
  where: Intervention_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Intervention_By_PkArgs = {
  _inc?: InputMaybe<Intervention_Inc_Input>;
  _set?: InputMaybe<Intervention_Set_Input>;
  pk_columns: Intervention_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Intervention_ManyArgs = {
  updates: Array<Intervention_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Intervention_PartnerArgs = {
  _set?: InputMaybe<Intervention_Partner_Set_Input>;
  where: Intervention_Partner_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Intervention_Partner_By_PkArgs = {
  _set?: InputMaybe<Intervention_Partner_Set_Input>;
  pk_columns: Intervention_Partner_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Intervention_Partner_ManyArgs = {
  updates: Array<Intervention_Partner_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Intervention_TypeArgs = {
  _append?: InputMaybe<Intervention_Type_Append_Input>;
  _delete_at_path?: InputMaybe<Intervention_Type_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Intervention_Type_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Intervention_Type_Delete_Key_Input>;
  _prepend?: InputMaybe<Intervention_Type_Prepend_Input>;
  _set?: InputMaybe<Intervention_Type_Set_Input>;
  where: Intervention_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Intervention_Type_By_PkArgs = {
  _append?: InputMaybe<Intervention_Type_Append_Input>;
  _delete_at_path?: InputMaybe<Intervention_Type_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Intervention_Type_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Intervention_Type_Delete_Key_Input>;
  _prepend?: InputMaybe<Intervention_Type_Prepend_Input>;
  _set?: InputMaybe<Intervention_Type_Set_Input>;
  pk_columns: Intervention_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Intervention_Type_ManyArgs = {
  updates: Array<Intervention_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_LocationArgs = {
  _append?: InputMaybe<Location_Append_Input>;
  _delete_at_path?: InputMaybe<Location_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Location_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Location_Delete_Key_Input>;
  _prepend?: InputMaybe<Location_Prepend_Input>;
  _set?: InputMaybe<Location_Set_Input>;
  where: Location_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Location_By_PkArgs = {
  _append?: InputMaybe<Location_Append_Input>;
  _delete_at_path?: InputMaybe<Location_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Location_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Location_Delete_Key_Input>;
  _prepend?: InputMaybe<Location_Prepend_Input>;
  _set?: InputMaybe<Location_Set_Input>;
  pk_columns: Location_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Location_ManyArgs = {
  updates: Array<Location_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Location_StatusArgs = {
  _set?: InputMaybe<Location_Status_Set_Input>;
  where: Location_Status_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Location_Status_By_PkArgs = {
  _set?: InputMaybe<Location_Status_Set_Input>;
  pk_columns: Location_Status_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Location_Status_ManyArgs = {
  updates: Array<Location_Status_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Location_Urban_ConstraintArgs = {
  _set?: InputMaybe<Location_Urban_Constraint_Set_Input>;
  where: Location_Urban_Constraint_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Location_Urban_Constraint_By_PkArgs = {
  _set?: InputMaybe<Location_Urban_Constraint_Set_Input>;
  pk_columns: Location_Urban_Constraint_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Location_Urban_Constraint_ManyArgs = {
  updates: Array<Location_Urban_Constraint_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_OrganizationArgs = {
  _inc?: InputMaybe<Organization_Inc_Input>;
  _set?: InputMaybe<Organization_Set_Input>;
  where: Organization_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Organization_By_PkArgs = {
  _inc?: InputMaybe<Organization_Inc_Input>;
  _set?: InputMaybe<Organization_Set_Input>;
  pk_columns: Organization_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Organization_ManyArgs = {
  updates: Array<Organization_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_PathogenArgs = {
  _set?: InputMaybe<Pathogen_Set_Input>;
  where: Pathogen_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Pathogen_By_PkArgs = {
  _set?: InputMaybe<Pathogen_Set_Input>;
  pk_columns: Pathogen_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Pathogen_ManyArgs = {
  updates: Array<Pathogen_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Residential_Usage_TypeArgs = {
  _set?: InputMaybe<Residential_Usage_Type_Set_Input>;
  where: Residential_Usage_Type_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Residential_Usage_Type_By_PkArgs = {
  _set?: InputMaybe<Residential_Usage_Type_Set_Input>;
  pk_columns: Residential_Usage_Type_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Residential_Usage_Type_ManyArgs = {
  updates: Array<Residential_Usage_Type_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_TaxonArgs = {
  _inc?: InputMaybe<Taxon_Inc_Input>;
  _set?: InputMaybe<Taxon_Set_Input>;
  where: Taxon_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Taxon_By_PkArgs = {
  _inc?: InputMaybe<Taxon_Inc_Input>;
  _set?: InputMaybe<Taxon_Set_Input>;
  pk_columns: Taxon_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Taxon_ManyArgs = {
  updates: Array<Taxon_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_TreeArgs = {
  _append?: InputMaybe<Tree_Append_Input>;
  _delete_at_path?: InputMaybe<Tree_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Tree_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Tree_Delete_Key_Input>;
  _inc?: InputMaybe<Tree_Inc_Input>;
  _prepend?: InputMaybe<Tree_Prepend_Input>;
  _set?: InputMaybe<Tree_Set_Input>;
  where: Tree_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Tree_By_PkArgs = {
  _append?: InputMaybe<Tree_Append_Input>;
  _delete_at_path?: InputMaybe<Tree_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Tree_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Tree_Delete_Key_Input>;
  _inc?: InputMaybe<Tree_Inc_Input>;
  _prepend?: InputMaybe<Tree_Prepend_Input>;
  _set?: InputMaybe<Tree_Set_Input>;
  pk_columns: Tree_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Tree_ManyArgs = {
  updates: Array<Tree_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Urban_ConstraintArgs = {
  _set?: InputMaybe<Urban_Constraint_Set_Input>;
  where: Urban_Constraint_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Urban_Constraint_By_PkArgs = {
  _set?: InputMaybe<Urban_Constraint_Set_Input>;
  pk_columns: Urban_Constraint_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Urban_Constraint_ManyArgs = {
  updates: Array<Urban_Constraint_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Urban_SiteArgs = {
  _set?: InputMaybe<Urban_Site_Set_Input>;
  where: Urban_Site_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Urban_Site_By_PkArgs = {
  _set?: InputMaybe<Urban_Site_Set_Input>;
  pk_columns: Urban_Site_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Urban_Site_ManyArgs = {
  updates: Array<Urban_Site_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_User_EntityArgs = {
  _inc?: InputMaybe<User_Entity_Inc_Input>;
  _set?: InputMaybe<User_Entity_Set_Input>;
  where: User_Entity_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_User_Entity_By_PkArgs = {
  _inc?: InputMaybe<User_Entity_Inc_Input>;
  _set?: InputMaybe<User_Entity_Set_Input>;
  pk_columns: User_Entity_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_User_Entity_ManyArgs = {
  updates: Array<User_Entity_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_AreaArgs = {
  _append?: InputMaybe<Vegetated_Area_Append_Input>;
  _delete_at_path?: InputMaybe<Vegetated_Area_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Vegetated_Area_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Vegetated_Area_Delete_Key_Input>;
  _inc?: InputMaybe<Vegetated_Area_Inc_Input>;
  _prepend?: InputMaybe<Vegetated_Area_Prepend_Input>;
  _set?: InputMaybe<Vegetated_Area_Set_Input>;
  where: Vegetated_Area_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Area_By_PkArgs = {
  _append?: InputMaybe<Vegetated_Area_Append_Input>;
  _delete_at_path?: InputMaybe<Vegetated_Area_Delete_At_Path_Input>;
  _delete_elem?: InputMaybe<Vegetated_Area_Delete_Elem_Input>;
  _delete_key?: InputMaybe<Vegetated_Area_Delete_Key_Input>;
  _inc?: InputMaybe<Vegetated_Area_Inc_Input>;
  _prepend?: InputMaybe<Vegetated_Area_Prepend_Input>;
  _set?: InputMaybe<Vegetated_Area_Set_Input>;
  pk_columns: Vegetated_Area_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Area_ManyArgs = {
  updates: Array<Vegetated_Area_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Areas_LocationsArgs = {
  _set?: InputMaybe<Vegetated_Areas_Locations_Set_Input>;
  where: Vegetated_Areas_Locations_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Areas_Locations_By_PkArgs = {
  _set?: InputMaybe<Vegetated_Areas_Locations_Set_Input>;
  pk_columns: Vegetated_Areas_Locations_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Areas_Locations_ManyArgs = {
  updates: Array<Vegetated_Areas_Locations_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Areas_Residential_Usage_TypesArgs = {
  _set?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Set_Input>;
  where: Vegetated_Areas_Residential_Usage_Types_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Areas_Residential_Usage_Types_By_PkArgs = {
  _set?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Set_Input>;
  pk_columns: Vegetated_Areas_Residential_Usage_Types_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Areas_Residential_Usage_Types_ManyArgs = {
  updates: Array<Vegetated_Areas_Residential_Usage_Types_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Areas_Urban_ConstraintsArgs = {
  _set?: InputMaybe<Vegetated_Areas_Urban_Constraints_Set_Input>;
  where: Vegetated_Areas_Urban_Constraints_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Areas_Urban_Constraints_By_PkArgs = {
  _set?: InputMaybe<Vegetated_Areas_Urban_Constraints_Set_Input>;
  pk_columns: Vegetated_Areas_Urban_Constraints_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Vegetated_Areas_Urban_Constraints_ManyArgs = {
  updates: Array<Vegetated_Areas_Urban_Constraints_Updates>;
};


/** mutation root */
export type Mutation_RootUpdate_WorksiteArgs = {
  _inc?: InputMaybe<Worksite_Inc_Input>;
  _set?: InputMaybe<Worksite_Set_Input>;
  where: Worksite_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Worksite_By_PkArgs = {
  _inc?: InputMaybe<Worksite_Inc_Input>;
  _set?: InputMaybe<Worksite_Set_Input>;
  pk_columns: Worksite_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Worksite_ManyArgs = {
  updates: Array<Worksite_Updates>;
};

/** Boolean expression to compare columns of type "numeric". All fields are combined with logical 'AND'. */
export type Numeric_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['numeric']>;
  _gt?: InputMaybe<Scalars['numeric']>;
  _gte?: InputMaybe<Scalars['numeric']>;
  _in?: InputMaybe<Array<Scalars['numeric']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['numeric']>;
  _lte?: InputMaybe<Scalars['numeric']>;
  _neq?: InputMaybe<Scalars['numeric']>;
  _nin?: InputMaybe<Array<Scalars['numeric']>>;
};

/** column ordering options */
export enum Order_By {
  /** in ascending order, nulls last */
  Asc = 'asc',
  /** in ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in descending order, nulls first */
  Desc = 'desc',
  /** in descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** columns and relationships of "organization" */
export type Organization = {
  __typename?: 'organization';
  bucket?: Maybe<Scalars['uuid']>;
  coords?: Maybe<Scalars['geometry']>;
  default: Scalars['Boolean'];
  domain?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  image: Scalars['String'];
  name: Scalars['String'];
  population: Scalars['numeric'];
  surface: Scalars['numeric'];
};

/** aggregated selection of "organization" */
export type Organization_Aggregate = {
  __typename?: 'organization_aggregate';
  aggregate?: Maybe<Organization_Aggregate_Fields>;
  nodes: Array<Organization>;
};

/** aggregate fields of "organization" */
export type Organization_Aggregate_Fields = {
  __typename?: 'organization_aggregate_fields';
  avg?: Maybe<Organization_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Organization_Max_Fields>;
  min?: Maybe<Organization_Min_Fields>;
  stddev?: Maybe<Organization_Stddev_Fields>;
  stddev_pop?: Maybe<Organization_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Organization_Stddev_Samp_Fields>;
  sum?: Maybe<Organization_Sum_Fields>;
  var_pop?: Maybe<Organization_Var_Pop_Fields>;
  var_samp?: Maybe<Organization_Var_Samp_Fields>;
  variance?: Maybe<Organization_Variance_Fields>;
};


/** aggregate fields of "organization" */
export type Organization_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Organization_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type Organization_Avg_Fields = {
  __typename?: 'organization_avg_fields';
  population?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "organization". All fields are combined with a logical 'AND'. */
export type Organization_Bool_Exp = {
  _and?: InputMaybe<Array<Organization_Bool_Exp>>;
  _not?: InputMaybe<Organization_Bool_Exp>;
  _or?: InputMaybe<Array<Organization_Bool_Exp>>;
  bucket?: InputMaybe<Uuid_Comparison_Exp>;
  coords?: InputMaybe<Geometry_Comparison_Exp>;
  default?: InputMaybe<Boolean_Comparison_Exp>;
  domain?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  image?: InputMaybe<String_Comparison_Exp>;
  name?: InputMaybe<String_Comparison_Exp>;
  population?: InputMaybe<Numeric_Comparison_Exp>;
  surface?: InputMaybe<Numeric_Comparison_Exp>;
};

/** unique or primary key constraints on table "organization" */
export enum Organization_Constraint {
  /** unique or primary key constraint on columns "bucket" */
  OrganizationBucketKey = 'organization_bucket_key',
  /** unique or primary key constraint on columns "domain" */
  OrganizationDomainKey = 'organization_domain_key',
  /** unique or primary key constraint on columns "id" */
  OrganizationPkey = 'organization_pkey'
}

/** input type for incrementing numeric columns in table "organization" */
export type Organization_Inc_Input = {
  population?: InputMaybe<Scalars['numeric']>;
  surface?: InputMaybe<Scalars['numeric']>;
};

/** input type for inserting data into table "organization" */
export type Organization_Insert_Input = {
  bucket?: InputMaybe<Scalars['uuid']>;
  coords?: InputMaybe<Scalars['geometry']>;
  default?: InputMaybe<Scalars['Boolean']>;
  domain?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  image?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  population?: InputMaybe<Scalars['numeric']>;
  surface?: InputMaybe<Scalars['numeric']>;
};

/** aggregate max on columns */
export type Organization_Max_Fields = {
  __typename?: 'organization_max_fields';
  bucket?: Maybe<Scalars['uuid']>;
  domain?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  population?: Maybe<Scalars['numeric']>;
  surface?: Maybe<Scalars['numeric']>;
};

/** aggregate min on columns */
export type Organization_Min_Fields = {
  __typename?: 'organization_min_fields';
  bucket?: Maybe<Scalars['uuid']>;
  domain?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  population?: Maybe<Scalars['numeric']>;
  surface?: Maybe<Scalars['numeric']>;
};

/** response of any mutation on the table "organization" */
export type Organization_Mutation_Response = {
  __typename?: 'organization_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Organization>;
};

/** on_conflict condition type for table "organization" */
export type Organization_On_Conflict = {
  constraint: Organization_Constraint;
  update_columns?: Array<Organization_Update_Column>;
  where?: InputMaybe<Organization_Bool_Exp>;
};

/** Ordering options when selecting data from "organization". */
export type Organization_Order_By = {
  bucket?: InputMaybe<Order_By>;
  coords?: InputMaybe<Order_By>;
  default?: InputMaybe<Order_By>;
  domain?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  image?: InputMaybe<Order_By>;
  name?: InputMaybe<Order_By>;
  population?: InputMaybe<Order_By>;
  surface?: InputMaybe<Order_By>;
};

/** primary key columns input for table: organization */
export type Organization_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "organization" */
export enum Organization_Select_Column {
  /** column name */
  Bucket = 'bucket',
  /** column name */
  Coords = 'coords',
  /** column name */
  Default = 'default',
  /** column name */
  Domain = 'domain',
  /** column name */
  Id = 'id',
  /** column name */
  Image = 'image',
  /** column name */
  Name = 'name',
  /** column name */
  Population = 'population',
  /** column name */
  Surface = 'surface'
}

/** input type for updating data in table "organization" */
export type Organization_Set_Input = {
  bucket?: InputMaybe<Scalars['uuid']>;
  coords?: InputMaybe<Scalars['geometry']>;
  default?: InputMaybe<Scalars['Boolean']>;
  domain?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  image?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  population?: InputMaybe<Scalars['numeric']>;
  surface?: InputMaybe<Scalars['numeric']>;
};

/** aggregate stddev on columns */
export type Organization_Stddev_Fields = {
  __typename?: 'organization_stddev_fields';
  population?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Organization_Stddev_Pop_Fields = {
  __typename?: 'organization_stddev_pop_fields';
  population?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Organization_Stddev_Samp_Fields = {
  __typename?: 'organization_stddev_samp_fields';
  population?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** Streaming cursor of the table "organization" */
export type Organization_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Organization_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Organization_Stream_Cursor_Value_Input = {
  bucket?: InputMaybe<Scalars['uuid']>;
  coords?: InputMaybe<Scalars['geometry']>;
  default?: InputMaybe<Scalars['Boolean']>;
  domain?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['uuid']>;
  image?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  population?: InputMaybe<Scalars['numeric']>;
  surface?: InputMaybe<Scalars['numeric']>;
};

/** aggregate sum on columns */
export type Organization_Sum_Fields = {
  __typename?: 'organization_sum_fields';
  population?: Maybe<Scalars['numeric']>;
  surface?: Maybe<Scalars['numeric']>;
};

/** update columns of table "organization" */
export enum Organization_Update_Column {
  /** column name */
  Bucket = 'bucket',
  /** column name */
  Coords = 'coords',
  /** column name */
  Default = 'default',
  /** column name */
  Domain = 'domain',
  /** column name */
  Id = 'id',
  /** column name */
  Image = 'image',
  /** column name */
  Name = 'name',
  /** column name */
  Population = 'population',
  /** column name */
  Surface = 'surface'
}

export type Organization_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Organization_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Organization_Set_Input>;
  /** filter the rows which have to be updated */
  where: Organization_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Organization_Var_Pop_Fields = {
  __typename?: 'organization_var_pop_fields';
  population?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Organization_Var_Samp_Fields = {
  __typename?: 'organization_var_samp_fields';
  population?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Organization_Variance_Fields = {
  __typename?: 'organization_variance_fields';
  population?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** List of all of possible pathogens of a diagnosis */
export type Pathogen = {
  __typename?: 'pathogen';
  id: Scalars['uuid'];
  slug: Scalars['String'];
};

/** aggregated selection of "pathogen" */
export type Pathogen_Aggregate = {
  __typename?: 'pathogen_aggregate';
  aggregate?: Maybe<Pathogen_Aggregate_Fields>;
  nodes: Array<Pathogen>;
};

/** aggregate fields of "pathogen" */
export type Pathogen_Aggregate_Fields = {
  __typename?: 'pathogen_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Pathogen_Max_Fields>;
  min?: Maybe<Pathogen_Min_Fields>;
};


/** aggregate fields of "pathogen" */
export type Pathogen_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Pathogen_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "pathogen". All fields are combined with a logical 'AND'. */
export type Pathogen_Bool_Exp = {
  _and?: InputMaybe<Array<Pathogen_Bool_Exp>>;
  _not?: InputMaybe<Pathogen_Bool_Exp>;
  _or?: InputMaybe<Array<Pathogen_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "pathogen" */
export enum Pathogen_Constraint {
  /** unique or primary key constraint on columns "id" */
  PathogenPkey = 'pathogen_pkey'
}

/** input type for inserting data into table "pathogen" */
export type Pathogen_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Pathogen_Max_Fields = {
  __typename?: 'pathogen_max_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Pathogen_Min_Fields = {
  __typename?: 'pathogen_min_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "pathogen" */
export type Pathogen_Mutation_Response = {
  __typename?: 'pathogen_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Pathogen>;
};

/** input type for inserting object relation for remote table "pathogen" */
export type Pathogen_Obj_Rel_Insert_Input = {
  data: Pathogen_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Pathogen_On_Conflict>;
};

/** on_conflict condition type for table "pathogen" */
export type Pathogen_On_Conflict = {
  constraint: Pathogen_Constraint;
  update_columns?: Array<Pathogen_Update_Column>;
  where?: InputMaybe<Pathogen_Bool_Exp>;
};

/** Ordering options when selecting data from "pathogen". */
export type Pathogen_Order_By = {
  id?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: pathogen */
export type Pathogen_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "pathogen" */
export enum Pathogen_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "pathogen" */
export type Pathogen_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "pathogen" */
export type Pathogen_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Pathogen_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Pathogen_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** update columns of table "pathogen" */
export enum Pathogen_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

export type Pathogen_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Pathogen_Set_Input>;
  /** filter the rows which have to be updated */
  where: Pathogen_Bool_Exp;
};

export type Query_Root = {
  __typename?: 'query_root';
  /** fetch data from the table: "analysis_tool" */
  analysis_tool: Array<Analysis_Tool>;
  /** fetch aggregated fields from the table: "analysis_tool" */
  analysis_tool_aggregate: Analysis_Tool_Aggregate;
  /** fetch data from the table: "analysis_tool" using primary key columns */
  analysis_tool_by_pk?: Maybe<Analysis_Tool>;
  /** fetch data from the table: "boundaries_locations" */
  boundaries_locations: Array<Boundaries_Locations>;
  /** fetch aggregated fields from the table: "boundaries_locations" */
  boundaries_locations_aggregate: Boundaries_Locations_Aggregate;
  /** fetch data from the table: "boundaries_locations" using primary key columns */
  boundaries_locations_by_pk?: Maybe<Boundaries_Locations>;
  /** An array relationship */
  boundaries_vegetated_areas: Array<Boundaries_Vegetated_Areas>;
  /** An aggregate relationship */
  boundaries_vegetated_areas_aggregate: Boundaries_Vegetated_Areas_Aggregate;
  /** fetch data from the table: "boundaries_vegetated_areas" using primary key columns */
  boundaries_vegetated_areas_by_pk?: Maybe<Boundaries_Vegetated_Areas>;
  /** fetch data from the table: "boundary" */
  boundary: Array<Boundary>;
  /** fetch aggregated fields from the table: "boundary" */
  boundary_aggregate: Boundary_Aggregate;
  /** fetch data from the table: "boundary" using primary key columns */
  boundary_by_pk?: Maybe<Boundary>;
  /** fetch data from the table: "diagnosis" */
  diagnosis: Array<Diagnosis>;
  /** fetch aggregated fields from the table: "diagnosis" */
  diagnosis_aggregate: Diagnosis_Aggregate;
  /** fetch data from the table: "diagnosis_analysis_tool" */
  diagnosis_analysis_tool: Array<Diagnosis_Analysis_Tool>;
  /** fetch aggregated fields from the table: "diagnosis_analysis_tool" */
  diagnosis_analysis_tool_aggregate: Diagnosis_Analysis_Tool_Aggregate;
  /** fetch data from the table: "diagnosis_analysis_tool" using primary key columns */
  diagnosis_analysis_tool_by_pk?: Maybe<Diagnosis_Analysis_Tool>;
  /** fetch data from the table: "diagnosis" using primary key columns */
  diagnosis_by_pk?: Maybe<Diagnosis>;
  /** fetch data from the table: "diagnosis_pathogen" */
  diagnosis_pathogen: Array<Diagnosis_Pathogen>;
  /** fetch aggregated fields from the table: "diagnosis_pathogen" */
  diagnosis_pathogen_aggregate: Diagnosis_Pathogen_Aggregate;
  /** fetch data from the table: "diagnosis_pathogen" using primary key columns */
  diagnosis_pathogen_by_pk?: Maybe<Diagnosis_Pathogen>;
  /** fetch data from the table: "diagnosis_type" */
  diagnosis_type: Array<Diagnosis_Type>;
  /** fetch aggregated fields from the table: "diagnosis_type" */
  diagnosis_type_aggregate: Diagnosis_Type_Aggregate;
  /** fetch data from the table: "diagnosis_type" using primary key columns */
  diagnosis_type_by_pk?: Maybe<Diagnosis_Type>;
  /** fetch data from the table: "family_intervention_type" */
  family_intervention_type: Array<Family_Intervention_Type>;
  /** fetch aggregated fields from the table: "family_intervention_type" */
  family_intervention_type_aggregate: Family_Intervention_Type_Aggregate;
  /** fetch data from the table: "family_intervention_type" using primary key columns */
  family_intervention_type_by_pk?: Maybe<Family_Intervention_Type>;
  /** fetch data from the table: "foot_type" */
  foot_type: Array<Foot_Type>;
  /** fetch aggregated fields from the table: "foot_type" */
  foot_type_aggregate: Foot_Type_Aggregate;
  /** fetch data from the table: "foot_type" using primary key columns */
  foot_type_by_pk?: Maybe<Foot_Type>;
  /** fetch data from the table: "import" */
  import: Array<Import>;
  /** fetch aggregated fields from the table: "import" */
  import_aggregate: Import_Aggregate;
  /** fetch data from the table: "import" using primary key columns */
  import_by_pk?: Maybe<Import>;
  /** fetch data from the table: "intervention" */
  intervention: Array<Intervention>;
  /** fetch aggregated fields from the table: "intervention" */
  intervention_aggregate: Intervention_Aggregate;
  /** fetch data from the table: "intervention" using primary key columns */
  intervention_by_pk?: Maybe<Intervention>;
  /** fetch data from the table: "intervention_partner" */
  intervention_partner: Array<Intervention_Partner>;
  /** fetch aggregated fields from the table: "intervention_partner" */
  intervention_partner_aggregate: Intervention_Partner_Aggregate;
  /** fetch data from the table: "intervention_partner" using primary key columns */
  intervention_partner_by_pk?: Maybe<Intervention_Partner>;
  /** fetch data from the table: "intervention_type" */
  intervention_type: Array<Intervention_Type>;
  /** fetch aggregated fields from the table: "intervention_type" */
  intervention_type_aggregate: Intervention_Type_Aggregate;
  /** fetch data from the table: "intervention_type" using primary key columns */
  intervention_type_by_pk?: Maybe<Intervention_Type>;
  /** fetch data from the table: "location" */
  location: Array<Location>;
  /** fetch aggregated fields from the table: "location" */
  location_aggregate: Location_Aggregate;
  /** fetch data from the table: "location" using primary key columns */
  location_by_pk?: Maybe<Location>;
  /** fetch data from the table: "location_status" */
  location_status: Array<Location_Status>;
  /** fetch aggregated fields from the table: "location_status" */
  location_status_aggregate: Location_Status_Aggregate;
  /** fetch data from the table: "location_status" using primary key columns */
  location_status_by_pk?: Maybe<Location_Status>;
  /** fetch data from the table: "location_urban_constraint" */
  location_urban_constraint: Array<Location_Urban_Constraint>;
  /** fetch aggregated fields from the table: "location_urban_constraint" */
  location_urban_constraint_aggregate: Location_Urban_Constraint_Aggregate;
  /** fetch data from the table: "location_urban_constraint" using primary key columns */
  location_urban_constraint_by_pk?: Maybe<Location_Urban_Constraint>;
  /** fetch data from the table: "organization" */
  organization: Array<Organization>;
  /** fetch aggregated fields from the table: "organization" */
  organization_aggregate: Organization_Aggregate;
  /** fetch data from the table: "organization" using primary key columns */
  organization_by_pk?: Maybe<Organization>;
  /** fetch data from the table: "pathogen" */
  pathogen: Array<Pathogen>;
  /** fetch aggregated fields from the table: "pathogen" */
  pathogen_aggregate: Pathogen_Aggregate;
  /** fetch data from the table: "pathogen" using primary key columns */
  pathogen_by_pk?: Maybe<Pathogen>;
  /** fetch data from the table: "residential_usage_type" */
  residential_usage_type: Array<Residential_Usage_Type>;
  /** fetch aggregated fields from the table: "residential_usage_type" */
  residential_usage_type_aggregate: Residential_Usage_Type_Aggregate;
  /** fetch data from the table: "residential_usage_type" using primary key columns */
  residential_usage_type_by_pk?: Maybe<Residential_Usage_Type>;
  taxaSearch?: Maybe<SearchTaxaOutput>;
  /** fetch data from the table: "taxon" */
  taxon: Array<Taxon>;
  /** fetch aggregated fields from the table: "taxon" */
  taxon_aggregate: Taxon_Aggregate;
  /** fetch data from the table: "taxon" using primary key columns */
  taxon_by_pk?: Maybe<Taxon>;
  /** An array relationship */
  tree: Array<Tree>;
  /** An aggregate relationship */
  tree_aggregate: Tree_Aggregate;
  /** fetch data from the table: "tree" using primary key columns */
  tree_by_pk?: Maybe<Tree>;
  /** fetch data from the table: "tree_scientific_names_count" */
  tree_scientific_names_count: Array<Tree_Scientific_Names_Count>;
  /** fetch aggregated fields from the table: "tree_scientific_names_count" */
  tree_scientific_names_count_aggregate: Tree_Scientific_Names_Count_Aggregate;
  /** fetch data from the table: "urban_constraint" */
  urban_constraint: Array<Urban_Constraint>;
  /** fetch aggregated fields from the table: "urban_constraint" */
  urban_constraint_aggregate: Urban_Constraint_Aggregate;
  /** fetch data from the table: "urban_constraint" using primary key columns */
  urban_constraint_by_pk?: Maybe<Urban_Constraint>;
  /** fetch data from the table: "urban_site" */
  urban_site: Array<Urban_Site>;
  /** fetch aggregated fields from the table: "urban_site" */
  urban_site_aggregate: Urban_Site_Aggregate;
  /** fetch data from the table: "urban_site" using primary key columns */
  urban_site_by_pk?: Maybe<Urban_Site>;
  /** urbasenseGetGroups */
  urbasenseGetGroups?: Maybe<Array<Maybe<GroupOutput>>>;
  /** urbasenseGetSubjectMonitoring */
  urbasenseGetSubjectMonitoring?: Maybe<MainOutput>;
  /** urbasenseGetSubjectMonitoringCombo */
  urbasenseGetSubjectMonitoringCombo?: Maybe<ComboOutput>;
  /** fetch data from the table: "user_entity" */
  user_entity: Array<User_Entity>;
  /** fetch aggregated fields from the table: "user_entity" */
  user_entity_aggregate: User_Entity_Aggregate;
  /** fetch data from the table: "user_entity" using primary key columns */
  user_entity_by_pk?: Maybe<User_Entity>;
  /** fetch data from the table: "vegetated_area" */
  vegetated_area: Array<Vegetated_Area>;
  /** fetch aggregated fields from the table: "vegetated_area" */
  vegetated_area_aggregate: Vegetated_Area_Aggregate;
  /** fetch data from the table: "vegetated_area" using primary key columns */
  vegetated_area_by_pk?: Maybe<Vegetated_Area>;
  /** fetch data from the table: "vegetated_areas_locations" */
  vegetated_areas_locations: Array<Vegetated_Areas_Locations>;
  /** fetch aggregated fields from the table: "vegetated_areas_locations" */
  vegetated_areas_locations_aggregate: Vegetated_Areas_Locations_Aggregate;
  /** fetch data from the table: "vegetated_areas_locations" using primary key columns */
  vegetated_areas_locations_by_pk?: Maybe<Vegetated_Areas_Locations>;
  /** fetch data from the table: "vegetated_areas_residential_usage_types" */
  vegetated_areas_residential_usage_types: Array<Vegetated_Areas_Residential_Usage_Types>;
  /** fetch aggregated fields from the table: "vegetated_areas_residential_usage_types" */
  vegetated_areas_residential_usage_types_aggregate: Vegetated_Areas_Residential_Usage_Types_Aggregate;
  /** fetch data from the table: "vegetated_areas_residential_usage_types" using primary key columns */
  vegetated_areas_residential_usage_types_by_pk?: Maybe<Vegetated_Areas_Residential_Usage_Types>;
  /** fetch data from the table: "vegetated_areas_urban_constraints" */
  vegetated_areas_urban_constraints: Array<Vegetated_Areas_Urban_Constraints>;
  /** fetch aggregated fields from the table: "vegetated_areas_urban_constraints" */
  vegetated_areas_urban_constraints_aggregate: Vegetated_Areas_Urban_Constraints_Aggregate;
  /** fetch data from the table: "vegetated_areas_urban_constraints" using primary key columns */
  vegetated_areas_urban_constraints_by_pk?: Maybe<Vegetated_Areas_Urban_Constraints>;
  /** fetch data from the table: "worksite" */
  worksite: Array<Worksite>;
  /** fetch aggregated fields from the table: "worksite" */
  worksite_aggregate: Worksite_Aggregate;
  /** fetch data from the table: "worksite" using primary key columns */
  worksite_by_pk?: Maybe<Worksite>;
};


export type Query_RootAnalysis_ToolArgs = {
  distinct_on?: InputMaybe<Array<Analysis_Tool_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Analysis_Tool_Order_By>>;
  where?: InputMaybe<Analysis_Tool_Bool_Exp>;
};


export type Query_RootAnalysis_Tool_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Analysis_Tool_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Analysis_Tool_Order_By>>;
  where?: InputMaybe<Analysis_Tool_Bool_Exp>;
};


export type Query_RootAnalysis_Tool_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootBoundaries_LocationsArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Locations_Order_By>>;
  where?: InputMaybe<Boundaries_Locations_Bool_Exp>;
};


export type Query_RootBoundaries_Locations_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Locations_Order_By>>;
  where?: InputMaybe<Boundaries_Locations_Bool_Exp>;
};


export type Query_RootBoundaries_Locations_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootBoundaries_Vegetated_AreasArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Vegetated_Areas_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Vegetated_Areas_Order_By>>;
  where?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
};


export type Query_RootBoundaries_Vegetated_Areas_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Vegetated_Areas_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Vegetated_Areas_Order_By>>;
  where?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
};


export type Query_RootBoundaries_Vegetated_Areas_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootBoundaryArgs = {
  distinct_on?: InputMaybe<Array<Boundary_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundary_Order_By>>;
  where?: InputMaybe<Boundary_Bool_Exp>;
};


export type Query_RootBoundary_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Boundary_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundary_Order_By>>;
  where?: InputMaybe<Boundary_Bool_Exp>;
};


export type Query_RootBoundary_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootDiagnosisArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Order_By>>;
  where?: InputMaybe<Diagnosis_Bool_Exp>;
};


export type Query_RootDiagnosis_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Order_By>>;
  where?: InputMaybe<Diagnosis_Bool_Exp>;
};


export type Query_RootDiagnosis_Analysis_ToolArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Analysis_Tool_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Analysis_Tool_Order_By>>;
  where?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
};


export type Query_RootDiagnosis_Analysis_Tool_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Analysis_Tool_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Analysis_Tool_Order_By>>;
  where?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
};


export type Query_RootDiagnosis_Analysis_Tool_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootDiagnosis_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootDiagnosis_PathogenArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Pathogen_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Pathogen_Order_By>>;
  where?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
};


export type Query_RootDiagnosis_Pathogen_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Pathogen_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Pathogen_Order_By>>;
  where?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
};


export type Query_RootDiagnosis_Pathogen_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootDiagnosis_TypeArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Type_Order_By>>;
  where?: InputMaybe<Diagnosis_Type_Bool_Exp>;
};


export type Query_RootDiagnosis_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Type_Order_By>>;
  where?: InputMaybe<Diagnosis_Type_Bool_Exp>;
};


export type Query_RootDiagnosis_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootFamily_Intervention_TypeArgs = {
  distinct_on?: InputMaybe<Array<Family_Intervention_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Family_Intervention_Type_Order_By>>;
  where?: InputMaybe<Family_Intervention_Type_Bool_Exp>;
};


export type Query_RootFamily_Intervention_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Family_Intervention_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Family_Intervention_Type_Order_By>>;
  where?: InputMaybe<Family_Intervention_Type_Bool_Exp>;
};


export type Query_RootFamily_Intervention_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootFoot_TypeArgs = {
  distinct_on?: InputMaybe<Array<Foot_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Foot_Type_Order_By>>;
  where?: InputMaybe<Foot_Type_Bool_Exp>;
};


export type Query_RootFoot_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Foot_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Foot_Type_Order_By>>;
  where?: InputMaybe<Foot_Type_Bool_Exp>;
};


export type Query_RootFoot_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootImportArgs = {
  distinct_on?: InputMaybe<Array<Import_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Import_Order_By>>;
  where?: InputMaybe<Import_Bool_Exp>;
};


export type Query_RootImport_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Import_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Import_Order_By>>;
  where?: InputMaybe<Import_Bool_Exp>;
};


export type Query_RootImport_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootInterventionArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Order_By>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};


export type Query_RootIntervention_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Order_By>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};


export type Query_RootIntervention_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootIntervention_PartnerArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Partner_Order_By>>;
  where?: InputMaybe<Intervention_Partner_Bool_Exp>;
};


export type Query_RootIntervention_Partner_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Partner_Order_By>>;
  where?: InputMaybe<Intervention_Partner_Bool_Exp>;
};


export type Query_RootIntervention_Partner_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootIntervention_TypeArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Type_Order_By>>;
  where?: InputMaybe<Intervention_Type_Bool_Exp>;
};


export type Query_RootIntervention_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Type_Order_By>>;
  where?: InputMaybe<Intervention_Type_Bool_Exp>;
};


export type Query_RootIntervention_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootLocationArgs = {
  distinct_on?: InputMaybe<Array<Location_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Order_By>>;
  where?: InputMaybe<Location_Bool_Exp>;
};


export type Query_RootLocation_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Location_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Order_By>>;
  where?: InputMaybe<Location_Bool_Exp>;
};


export type Query_RootLocation_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootLocation_StatusArgs = {
  distinct_on?: InputMaybe<Array<Location_Status_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Status_Order_By>>;
  where?: InputMaybe<Location_Status_Bool_Exp>;
};


export type Query_RootLocation_Status_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Location_Status_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Status_Order_By>>;
  where?: InputMaybe<Location_Status_Bool_Exp>;
};


export type Query_RootLocation_Status_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootLocation_Urban_ConstraintArgs = {
  distinct_on?: InputMaybe<Array<Location_Urban_Constraint_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Urban_Constraint_Order_By>>;
  where?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
};


export type Query_RootLocation_Urban_Constraint_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Location_Urban_Constraint_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Urban_Constraint_Order_By>>;
  where?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
};


export type Query_RootLocation_Urban_Constraint_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootOrganizationArgs = {
  distinct_on?: InputMaybe<Array<Organization_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Organization_Order_By>>;
  where?: InputMaybe<Organization_Bool_Exp>;
};


export type Query_RootOrganization_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Organization_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Organization_Order_By>>;
  where?: InputMaybe<Organization_Bool_Exp>;
};


export type Query_RootOrganization_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootPathogenArgs = {
  distinct_on?: InputMaybe<Array<Pathogen_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Pathogen_Order_By>>;
  where?: InputMaybe<Pathogen_Bool_Exp>;
};


export type Query_RootPathogen_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Pathogen_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Pathogen_Order_By>>;
  where?: InputMaybe<Pathogen_Bool_Exp>;
};


export type Query_RootPathogen_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootResidential_Usage_TypeArgs = {
  distinct_on?: InputMaybe<Array<Residential_Usage_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Residential_Usage_Type_Order_By>>;
  where?: InputMaybe<Residential_Usage_Type_Bool_Exp>;
};


export type Query_RootResidential_Usage_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Residential_Usage_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Residential_Usage_Type_Order_By>>;
  where?: InputMaybe<Residential_Usage_Type_Bool_Exp>;
};


export type Query_RootResidential_Usage_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootTaxaSearchArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  query?: InputMaybe<Scalars['String']>;
};


export type Query_RootTaxonArgs = {
  distinct_on?: InputMaybe<Array<Taxon_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Taxon_Order_By>>;
  where?: InputMaybe<Taxon_Bool_Exp>;
};


export type Query_RootTaxon_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Taxon_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Taxon_Order_By>>;
  where?: InputMaybe<Taxon_Bool_Exp>;
};


export type Query_RootTaxon_By_PkArgs = {
  id: Scalars['Int'];
};


export type Query_RootTreeArgs = {
  distinct_on?: InputMaybe<Array<Tree_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Tree_Order_By>>;
  where?: InputMaybe<Tree_Bool_Exp>;
};


export type Query_RootTree_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Tree_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Tree_Order_By>>;
  where?: InputMaybe<Tree_Bool_Exp>;
};


export type Query_RootTree_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootTree_Scientific_Names_CountArgs = {
  distinct_on?: InputMaybe<Array<Tree_Scientific_Names_Count_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Tree_Scientific_Names_Count_Order_By>>;
  where?: InputMaybe<Tree_Scientific_Names_Count_Bool_Exp>;
};


export type Query_RootTree_Scientific_Names_Count_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Tree_Scientific_Names_Count_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Tree_Scientific_Names_Count_Order_By>>;
  where?: InputMaybe<Tree_Scientific_Names_Count_Bool_Exp>;
};


export type Query_RootUrban_ConstraintArgs = {
  distinct_on?: InputMaybe<Array<Urban_Constraint_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Urban_Constraint_Order_By>>;
  where?: InputMaybe<Urban_Constraint_Bool_Exp>;
};


export type Query_RootUrban_Constraint_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Urban_Constraint_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Urban_Constraint_Order_By>>;
  where?: InputMaybe<Urban_Constraint_Bool_Exp>;
};


export type Query_RootUrban_Constraint_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootUrban_SiteArgs = {
  distinct_on?: InputMaybe<Array<Urban_Site_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Urban_Site_Order_By>>;
  where?: InputMaybe<Urban_Site_Bool_Exp>;
};


export type Query_RootUrban_Site_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Urban_Site_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Urban_Site_Order_By>>;
  where?: InputMaybe<Urban_Site_Bool_Exp>;
};


export type Query_RootUrban_Site_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootUrbasenseGetGroupsArgs = {
  SiteParams: SiteInput;
};


export type Query_RootUrbasenseGetSubjectMonitoringArgs = {
  subjectId: Scalars['Int'];
};


export type Query_RootUrbasenseGetSubjectMonitoringComboArgs = {
  subjectId: Scalars['Int'];
};


export type Query_RootUser_EntityArgs = {
  distinct_on?: InputMaybe<Array<User_Entity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Entity_Order_By>>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};


export type Query_RootUser_Entity_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Entity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Entity_Order_By>>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};


export type Query_RootUser_Entity_By_PkArgs = {
  id: Scalars['String'];
};


export type Query_RootVegetated_AreaArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Area_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Area_Order_By>>;
  where?: InputMaybe<Vegetated_Area_Bool_Exp>;
};


export type Query_RootVegetated_Area_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Area_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Area_Order_By>>;
  where?: InputMaybe<Vegetated_Area_Bool_Exp>;
};


export type Query_RootVegetated_Area_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootVegetated_Areas_LocationsArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Locations_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
};


export type Query_RootVegetated_Areas_Locations_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Locations_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
};


export type Query_RootVegetated_Areas_Locations_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootVegetated_Areas_Residential_Usage_TypesArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
};


export type Query_RootVegetated_Areas_Residential_Usage_Types_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
};


export type Query_RootVegetated_Areas_Residential_Usage_Types_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootVegetated_Areas_Urban_ConstraintsArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
};


export type Query_RootVegetated_Areas_Urban_Constraints_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
};


export type Query_RootVegetated_Areas_Urban_Constraints_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Query_RootWorksiteArgs = {
  distinct_on?: InputMaybe<Array<Worksite_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Worksite_Order_By>>;
  where?: InputMaybe<Worksite_Bool_Exp>;
};


export type Query_RootWorksite_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Worksite_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Worksite_Order_By>>;
  where?: InputMaybe<Worksite_Bool_Exp>;
};


export type Query_RootWorksite_By_PkArgs = {
  id: Scalars['uuid'];
};

/** List of all residential usage types */
export type Residential_Usage_Type = {
  __typename?: 'residential_usage_type';
  id: Scalars['uuid'];
  slug: Scalars['String'];
};

/** aggregated selection of "residential_usage_type" */
export type Residential_Usage_Type_Aggregate = {
  __typename?: 'residential_usage_type_aggregate';
  aggregate?: Maybe<Residential_Usage_Type_Aggregate_Fields>;
  nodes: Array<Residential_Usage_Type>;
};

/** aggregate fields of "residential_usage_type" */
export type Residential_Usage_Type_Aggregate_Fields = {
  __typename?: 'residential_usage_type_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Residential_Usage_Type_Max_Fields>;
  min?: Maybe<Residential_Usage_Type_Min_Fields>;
};


/** aggregate fields of "residential_usage_type" */
export type Residential_Usage_Type_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Residential_Usage_Type_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "residential_usage_type". All fields are combined with a logical 'AND'. */
export type Residential_Usage_Type_Bool_Exp = {
  _and?: InputMaybe<Array<Residential_Usage_Type_Bool_Exp>>;
  _not?: InputMaybe<Residential_Usage_Type_Bool_Exp>;
  _or?: InputMaybe<Array<Residential_Usage_Type_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "residential_usage_type" */
export enum Residential_Usage_Type_Constraint {
  /** unique or primary key constraint on columns "id" */
  ResidentialUsageTypePkey = 'residential_usage_type_pkey'
}

/** input type for inserting data into table "residential_usage_type" */
export type Residential_Usage_Type_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Residential_Usage_Type_Max_Fields = {
  __typename?: 'residential_usage_type_max_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Residential_Usage_Type_Min_Fields = {
  __typename?: 'residential_usage_type_min_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "residential_usage_type" */
export type Residential_Usage_Type_Mutation_Response = {
  __typename?: 'residential_usage_type_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Residential_Usage_Type>;
};

/** input type for inserting object relation for remote table "residential_usage_type" */
export type Residential_Usage_Type_Obj_Rel_Insert_Input = {
  data: Residential_Usage_Type_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Residential_Usage_Type_On_Conflict>;
};

/** on_conflict condition type for table "residential_usage_type" */
export type Residential_Usage_Type_On_Conflict = {
  constraint: Residential_Usage_Type_Constraint;
  update_columns?: Array<Residential_Usage_Type_Update_Column>;
  where?: InputMaybe<Residential_Usage_Type_Bool_Exp>;
};

/** Ordering options when selecting data from "residential_usage_type". */
export type Residential_Usage_Type_Order_By = {
  id?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: residential_usage_type */
export type Residential_Usage_Type_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "residential_usage_type" */
export enum Residential_Usage_Type_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "residential_usage_type" */
export type Residential_Usage_Type_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "residential_usage_type" */
export type Residential_Usage_Type_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Residential_Usage_Type_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Residential_Usage_Type_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** update columns of table "residential_usage_type" */
export enum Residential_Usage_Type_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

export type Residential_Usage_Type_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Residential_Usage_Type_Set_Input>;
  /** filter the rows which have to be updated */
  where: Residential_Usage_Type_Bool_Exp;
};

export type SearchTaxaOutput = {
  __typename?: 'searchTaxaOutput';
  hits?: Maybe<Array<Maybe<TaxonHit>>>;
};

export type St_D_Within_Geography_Input = {
  distance: Scalars['Float'];
  from: Scalars['geography'];
  use_spheroid?: InputMaybe<Scalars['Boolean']>;
};

export type St_D_Within_Input = {
  distance: Scalars['Float'];
  from: Scalars['geometry'];
};

export type Subscription_Root = {
  __typename?: 'subscription_root';
  /** fetch data from the table: "analysis_tool" */
  analysis_tool: Array<Analysis_Tool>;
  /** fetch aggregated fields from the table: "analysis_tool" */
  analysis_tool_aggregate: Analysis_Tool_Aggregate;
  /** fetch data from the table: "analysis_tool" using primary key columns */
  analysis_tool_by_pk?: Maybe<Analysis_Tool>;
  /** fetch data from the table in a streaming manner: "analysis_tool" */
  analysis_tool_stream: Array<Analysis_Tool>;
  /** fetch data from the table: "boundaries_locations" */
  boundaries_locations: Array<Boundaries_Locations>;
  /** fetch aggregated fields from the table: "boundaries_locations" */
  boundaries_locations_aggregate: Boundaries_Locations_Aggregate;
  /** fetch data from the table: "boundaries_locations" using primary key columns */
  boundaries_locations_by_pk?: Maybe<Boundaries_Locations>;
  /** fetch data from the table in a streaming manner: "boundaries_locations" */
  boundaries_locations_stream: Array<Boundaries_Locations>;
  /** An array relationship */
  boundaries_vegetated_areas: Array<Boundaries_Vegetated_Areas>;
  /** An aggregate relationship */
  boundaries_vegetated_areas_aggregate: Boundaries_Vegetated_Areas_Aggregate;
  /** fetch data from the table: "boundaries_vegetated_areas" using primary key columns */
  boundaries_vegetated_areas_by_pk?: Maybe<Boundaries_Vegetated_Areas>;
  /** fetch data from the table in a streaming manner: "boundaries_vegetated_areas" */
  boundaries_vegetated_areas_stream: Array<Boundaries_Vegetated_Areas>;
  /** fetch data from the table: "boundary" */
  boundary: Array<Boundary>;
  /** fetch aggregated fields from the table: "boundary" */
  boundary_aggregate: Boundary_Aggregate;
  /** fetch data from the table: "boundary" using primary key columns */
  boundary_by_pk?: Maybe<Boundary>;
  /** fetch data from the table in a streaming manner: "boundary" */
  boundary_stream: Array<Boundary>;
  /** fetch data from the table: "diagnosis" */
  diagnosis: Array<Diagnosis>;
  /** fetch aggregated fields from the table: "diagnosis" */
  diagnosis_aggregate: Diagnosis_Aggregate;
  /** fetch data from the table: "diagnosis_analysis_tool" */
  diagnosis_analysis_tool: Array<Diagnosis_Analysis_Tool>;
  /** fetch aggregated fields from the table: "diagnosis_analysis_tool" */
  diagnosis_analysis_tool_aggregate: Diagnosis_Analysis_Tool_Aggregate;
  /** fetch data from the table: "diagnosis_analysis_tool" using primary key columns */
  diagnosis_analysis_tool_by_pk?: Maybe<Diagnosis_Analysis_Tool>;
  /** fetch data from the table in a streaming manner: "diagnosis_analysis_tool" */
  diagnosis_analysis_tool_stream: Array<Diagnosis_Analysis_Tool>;
  /** fetch data from the table: "diagnosis" using primary key columns */
  diagnosis_by_pk?: Maybe<Diagnosis>;
  /** fetch data from the table: "diagnosis_pathogen" */
  diagnosis_pathogen: Array<Diagnosis_Pathogen>;
  /** fetch aggregated fields from the table: "diagnosis_pathogen" */
  diagnosis_pathogen_aggregate: Diagnosis_Pathogen_Aggregate;
  /** fetch data from the table: "diagnosis_pathogen" using primary key columns */
  diagnosis_pathogen_by_pk?: Maybe<Diagnosis_Pathogen>;
  /** fetch data from the table in a streaming manner: "diagnosis_pathogen" */
  diagnosis_pathogen_stream: Array<Diagnosis_Pathogen>;
  /** fetch data from the table in a streaming manner: "diagnosis" */
  diagnosis_stream: Array<Diagnosis>;
  /** fetch data from the table: "diagnosis_type" */
  diagnosis_type: Array<Diagnosis_Type>;
  /** fetch aggregated fields from the table: "diagnosis_type" */
  diagnosis_type_aggregate: Diagnosis_Type_Aggregate;
  /** fetch data from the table: "diagnosis_type" using primary key columns */
  diagnosis_type_by_pk?: Maybe<Diagnosis_Type>;
  /** fetch data from the table in a streaming manner: "diagnosis_type" */
  diagnosis_type_stream: Array<Diagnosis_Type>;
  /** fetch data from the table: "family_intervention_type" */
  family_intervention_type: Array<Family_Intervention_Type>;
  /** fetch aggregated fields from the table: "family_intervention_type" */
  family_intervention_type_aggregate: Family_Intervention_Type_Aggregate;
  /** fetch data from the table: "family_intervention_type" using primary key columns */
  family_intervention_type_by_pk?: Maybe<Family_Intervention_Type>;
  /** fetch data from the table in a streaming manner: "family_intervention_type" */
  family_intervention_type_stream: Array<Family_Intervention_Type>;
  /** fetch data from the table: "foot_type" */
  foot_type: Array<Foot_Type>;
  /** fetch aggregated fields from the table: "foot_type" */
  foot_type_aggregate: Foot_Type_Aggregate;
  /** fetch data from the table: "foot_type" using primary key columns */
  foot_type_by_pk?: Maybe<Foot_Type>;
  /** fetch data from the table in a streaming manner: "foot_type" */
  foot_type_stream: Array<Foot_Type>;
  /** fetch data from the table: "import" */
  import: Array<Import>;
  /** fetch aggregated fields from the table: "import" */
  import_aggregate: Import_Aggregate;
  /** fetch data from the table: "import" using primary key columns */
  import_by_pk?: Maybe<Import>;
  /** fetch data from the table in a streaming manner: "import" */
  import_stream: Array<Import>;
  /** fetch data from the table: "intervention" */
  intervention: Array<Intervention>;
  /** fetch aggregated fields from the table: "intervention" */
  intervention_aggregate: Intervention_Aggregate;
  /** fetch data from the table: "intervention" using primary key columns */
  intervention_by_pk?: Maybe<Intervention>;
  /** fetch data from the table: "intervention_partner" */
  intervention_partner: Array<Intervention_Partner>;
  /** fetch aggregated fields from the table: "intervention_partner" */
  intervention_partner_aggregate: Intervention_Partner_Aggregate;
  /** fetch data from the table: "intervention_partner" using primary key columns */
  intervention_partner_by_pk?: Maybe<Intervention_Partner>;
  /** fetch data from the table in a streaming manner: "intervention_partner" */
  intervention_partner_stream: Array<Intervention_Partner>;
  /** fetch data from the table in a streaming manner: "intervention" */
  intervention_stream: Array<Intervention>;
  /** fetch data from the table: "intervention_type" */
  intervention_type: Array<Intervention_Type>;
  /** fetch aggregated fields from the table: "intervention_type" */
  intervention_type_aggregate: Intervention_Type_Aggregate;
  /** fetch data from the table: "intervention_type" using primary key columns */
  intervention_type_by_pk?: Maybe<Intervention_Type>;
  /** fetch data from the table in a streaming manner: "intervention_type" */
  intervention_type_stream: Array<Intervention_Type>;
  /** fetch data from the table: "location" */
  location: Array<Location>;
  /** fetch aggregated fields from the table: "location" */
  location_aggregate: Location_Aggregate;
  /** fetch data from the table: "location" using primary key columns */
  location_by_pk?: Maybe<Location>;
  /** fetch data from the table: "location_status" */
  location_status: Array<Location_Status>;
  /** fetch aggregated fields from the table: "location_status" */
  location_status_aggregate: Location_Status_Aggregate;
  /** fetch data from the table: "location_status" using primary key columns */
  location_status_by_pk?: Maybe<Location_Status>;
  /** fetch data from the table in a streaming manner: "location_status" */
  location_status_stream: Array<Location_Status>;
  /** fetch data from the table in a streaming manner: "location" */
  location_stream: Array<Location>;
  /** fetch data from the table: "location_urban_constraint" */
  location_urban_constraint: Array<Location_Urban_Constraint>;
  /** fetch aggregated fields from the table: "location_urban_constraint" */
  location_urban_constraint_aggregate: Location_Urban_Constraint_Aggregate;
  /** fetch data from the table: "location_urban_constraint" using primary key columns */
  location_urban_constraint_by_pk?: Maybe<Location_Urban_Constraint>;
  /** fetch data from the table in a streaming manner: "location_urban_constraint" */
  location_urban_constraint_stream: Array<Location_Urban_Constraint>;
  /** fetch data from the table: "organization" */
  organization: Array<Organization>;
  /** fetch aggregated fields from the table: "organization" */
  organization_aggregate: Organization_Aggregate;
  /** fetch data from the table: "organization" using primary key columns */
  organization_by_pk?: Maybe<Organization>;
  /** fetch data from the table in a streaming manner: "organization" */
  organization_stream: Array<Organization>;
  /** fetch data from the table: "pathogen" */
  pathogen: Array<Pathogen>;
  /** fetch aggregated fields from the table: "pathogen" */
  pathogen_aggregate: Pathogen_Aggregate;
  /** fetch data from the table: "pathogen" using primary key columns */
  pathogen_by_pk?: Maybe<Pathogen>;
  /** fetch data from the table in a streaming manner: "pathogen" */
  pathogen_stream: Array<Pathogen>;
  /** fetch data from the table: "residential_usage_type" */
  residential_usage_type: Array<Residential_Usage_Type>;
  /** fetch aggregated fields from the table: "residential_usage_type" */
  residential_usage_type_aggregate: Residential_Usage_Type_Aggregate;
  /** fetch data from the table: "residential_usage_type" using primary key columns */
  residential_usage_type_by_pk?: Maybe<Residential_Usage_Type>;
  /** fetch data from the table in a streaming manner: "residential_usage_type" */
  residential_usage_type_stream: Array<Residential_Usage_Type>;
  /** fetch data from the table: "taxon" */
  taxon: Array<Taxon>;
  /** fetch aggregated fields from the table: "taxon" */
  taxon_aggregate: Taxon_Aggregate;
  /** fetch data from the table: "taxon" using primary key columns */
  taxon_by_pk?: Maybe<Taxon>;
  /** fetch data from the table in a streaming manner: "taxon" */
  taxon_stream: Array<Taxon>;
  /** An array relationship */
  tree: Array<Tree>;
  /** An aggregate relationship */
  tree_aggregate: Tree_Aggregate;
  /** fetch data from the table: "tree" using primary key columns */
  tree_by_pk?: Maybe<Tree>;
  /** fetch data from the table: "tree_scientific_names_count" */
  tree_scientific_names_count: Array<Tree_Scientific_Names_Count>;
  /** fetch aggregated fields from the table: "tree_scientific_names_count" */
  tree_scientific_names_count_aggregate: Tree_Scientific_Names_Count_Aggregate;
  /** fetch data from the table in a streaming manner: "tree_scientific_names_count" */
  tree_scientific_names_count_stream: Array<Tree_Scientific_Names_Count>;
  /** fetch data from the table in a streaming manner: "tree" */
  tree_stream: Array<Tree>;
  /** fetch data from the table: "urban_constraint" */
  urban_constraint: Array<Urban_Constraint>;
  /** fetch aggregated fields from the table: "urban_constraint" */
  urban_constraint_aggregate: Urban_Constraint_Aggregate;
  /** fetch data from the table: "urban_constraint" using primary key columns */
  urban_constraint_by_pk?: Maybe<Urban_Constraint>;
  /** fetch data from the table in a streaming manner: "urban_constraint" */
  urban_constraint_stream: Array<Urban_Constraint>;
  /** fetch data from the table: "urban_site" */
  urban_site: Array<Urban_Site>;
  /** fetch aggregated fields from the table: "urban_site" */
  urban_site_aggregate: Urban_Site_Aggregate;
  /** fetch data from the table: "urban_site" using primary key columns */
  urban_site_by_pk?: Maybe<Urban_Site>;
  /** fetch data from the table in a streaming manner: "urban_site" */
  urban_site_stream: Array<Urban_Site>;
  /** fetch data from the table: "user_entity" */
  user_entity: Array<User_Entity>;
  /** fetch aggregated fields from the table: "user_entity" */
  user_entity_aggregate: User_Entity_Aggregate;
  /** fetch data from the table: "user_entity" using primary key columns */
  user_entity_by_pk?: Maybe<User_Entity>;
  /** fetch data from the table in a streaming manner: "user_entity" */
  user_entity_stream: Array<User_Entity>;
  /** fetch data from the table: "vegetated_area" */
  vegetated_area: Array<Vegetated_Area>;
  /** fetch aggregated fields from the table: "vegetated_area" */
  vegetated_area_aggregate: Vegetated_Area_Aggregate;
  /** fetch data from the table: "vegetated_area" using primary key columns */
  vegetated_area_by_pk?: Maybe<Vegetated_Area>;
  /** fetch data from the table in a streaming manner: "vegetated_area" */
  vegetated_area_stream: Array<Vegetated_Area>;
  /** fetch data from the table: "vegetated_areas_locations" */
  vegetated_areas_locations: Array<Vegetated_Areas_Locations>;
  /** fetch aggregated fields from the table: "vegetated_areas_locations" */
  vegetated_areas_locations_aggregate: Vegetated_Areas_Locations_Aggregate;
  /** fetch data from the table: "vegetated_areas_locations" using primary key columns */
  vegetated_areas_locations_by_pk?: Maybe<Vegetated_Areas_Locations>;
  /** fetch data from the table in a streaming manner: "vegetated_areas_locations" */
  vegetated_areas_locations_stream: Array<Vegetated_Areas_Locations>;
  /** fetch data from the table: "vegetated_areas_residential_usage_types" */
  vegetated_areas_residential_usage_types: Array<Vegetated_Areas_Residential_Usage_Types>;
  /** fetch aggregated fields from the table: "vegetated_areas_residential_usage_types" */
  vegetated_areas_residential_usage_types_aggregate: Vegetated_Areas_Residential_Usage_Types_Aggregate;
  /** fetch data from the table: "vegetated_areas_residential_usage_types" using primary key columns */
  vegetated_areas_residential_usage_types_by_pk?: Maybe<Vegetated_Areas_Residential_Usage_Types>;
  /** fetch data from the table in a streaming manner: "vegetated_areas_residential_usage_types" */
  vegetated_areas_residential_usage_types_stream: Array<Vegetated_Areas_Residential_Usage_Types>;
  /** fetch data from the table: "vegetated_areas_urban_constraints" */
  vegetated_areas_urban_constraints: Array<Vegetated_Areas_Urban_Constraints>;
  /** fetch aggregated fields from the table: "vegetated_areas_urban_constraints" */
  vegetated_areas_urban_constraints_aggregate: Vegetated_Areas_Urban_Constraints_Aggregate;
  /** fetch data from the table: "vegetated_areas_urban_constraints" using primary key columns */
  vegetated_areas_urban_constraints_by_pk?: Maybe<Vegetated_Areas_Urban_Constraints>;
  /** fetch data from the table in a streaming manner: "vegetated_areas_urban_constraints" */
  vegetated_areas_urban_constraints_stream: Array<Vegetated_Areas_Urban_Constraints>;
  /** fetch data from the table: "worksite" */
  worksite: Array<Worksite>;
  /** fetch aggregated fields from the table: "worksite" */
  worksite_aggregate: Worksite_Aggregate;
  /** fetch data from the table: "worksite" using primary key columns */
  worksite_by_pk?: Maybe<Worksite>;
  /** fetch data from the table in a streaming manner: "worksite" */
  worksite_stream: Array<Worksite>;
};


export type Subscription_RootAnalysis_ToolArgs = {
  distinct_on?: InputMaybe<Array<Analysis_Tool_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Analysis_Tool_Order_By>>;
  where?: InputMaybe<Analysis_Tool_Bool_Exp>;
};


export type Subscription_RootAnalysis_Tool_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Analysis_Tool_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Analysis_Tool_Order_By>>;
  where?: InputMaybe<Analysis_Tool_Bool_Exp>;
};


export type Subscription_RootAnalysis_Tool_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootAnalysis_Tool_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Analysis_Tool_Stream_Cursor_Input>>;
  where?: InputMaybe<Analysis_Tool_Bool_Exp>;
};


export type Subscription_RootBoundaries_LocationsArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Locations_Order_By>>;
  where?: InputMaybe<Boundaries_Locations_Bool_Exp>;
};


export type Subscription_RootBoundaries_Locations_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Locations_Order_By>>;
  where?: InputMaybe<Boundaries_Locations_Bool_Exp>;
};


export type Subscription_RootBoundaries_Locations_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootBoundaries_Locations_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Boundaries_Locations_Stream_Cursor_Input>>;
  where?: InputMaybe<Boundaries_Locations_Bool_Exp>;
};


export type Subscription_RootBoundaries_Vegetated_AreasArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Vegetated_Areas_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Vegetated_Areas_Order_By>>;
  where?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
};


export type Subscription_RootBoundaries_Vegetated_Areas_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Vegetated_Areas_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Vegetated_Areas_Order_By>>;
  where?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
};


export type Subscription_RootBoundaries_Vegetated_Areas_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootBoundaries_Vegetated_Areas_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Boundaries_Vegetated_Areas_Stream_Cursor_Input>>;
  where?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
};


export type Subscription_RootBoundaryArgs = {
  distinct_on?: InputMaybe<Array<Boundary_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundary_Order_By>>;
  where?: InputMaybe<Boundary_Bool_Exp>;
};


export type Subscription_RootBoundary_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Boundary_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundary_Order_By>>;
  where?: InputMaybe<Boundary_Bool_Exp>;
};


export type Subscription_RootBoundary_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootBoundary_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Boundary_Stream_Cursor_Input>>;
  where?: InputMaybe<Boundary_Bool_Exp>;
};


export type Subscription_RootDiagnosisArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Order_By>>;
  where?: InputMaybe<Diagnosis_Bool_Exp>;
};


export type Subscription_RootDiagnosis_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Order_By>>;
  where?: InputMaybe<Diagnosis_Bool_Exp>;
};


export type Subscription_RootDiagnosis_Analysis_ToolArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Analysis_Tool_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Analysis_Tool_Order_By>>;
  where?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
};


export type Subscription_RootDiagnosis_Analysis_Tool_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Analysis_Tool_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Analysis_Tool_Order_By>>;
  where?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
};


export type Subscription_RootDiagnosis_Analysis_Tool_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootDiagnosis_Analysis_Tool_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Diagnosis_Analysis_Tool_Stream_Cursor_Input>>;
  where?: InputMaybe<Diagnosis_Analysis_Tool_Bool_Exp>;
};


export type Subscription_RootDiagnosis_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootDiagnosis_PathogenArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Pathogen_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Pathogen_Order_By>>;
  where?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
};


export type Subscription_RootDiagnosis_Pathogen_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Pathogen_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Pathogen_Order_By>>;
  where?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
};


export type Subscription_RootDiagnosis_Pathogen_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootDiagnosis_Pathogen_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Diagnosis_Pathogen_Stream_Cursor_Input>>;
  where?: InputMaybe<Diagnosis_Pathogen_Bool_Exp>;
};


export type Subscription_RootDiagnosis_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Diagnosis_Stream_Cursor_Input>>;
  where?: InputMaybe<Diagnosis_Bool_Exp>;
};


export type Subscription_RootDiagnosis_TypeArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Type_Order_By>>;
  where?: InputMaybe<Diagnosis_Type_Bool_Exp>;
};


export type Subscription_RootDiagnosis_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Type_Order_By>>;
  where?: InputMaybe<Diagnosis_Type_Bool_Exp>;
};


export type Subscription_RootDiagnosis_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootDiagnosis_Type_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Diagnosis_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<Diagnosis_Type_Bool_Exp>;
};


export type Subscription_RootFamily_Intervention_TypeArgs = {
  distinct_on?: InputMaybe<Array<Family_Intervention_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Family_Intervention_Type_Order_By>>;
  where?: InputMaybe<Family_Intervention_Type_Bool_Exp>;
};


export type Subscription_RootFamily_Intervention_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Family_Intervention_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Family_Intervention_Type_Order_By>>;
  where?: InputMaybe<Family_Intervention_Type_Bool_Exp>;
};


export type Subscription_RootFamily_Intervention_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootFamily_Intervention_Type_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Family_Intervention_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<Family_Intervention_Type_Bool_Exp>;
};


export type Subscription_RootFoot_TypeArgs = {
  distinct_on?: InputMaybe<Array<Foot_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Foot_Type_Order_By>>;
  where?: InputMaybe<Foot_Type_Bool_Exp>;
};


export type Subscription_RootFoot_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Foot_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Foot_Type_Order_By>>;
  where?: InputMaybe<Foot_Type_Bool_Exp>;
};


export type Subscription_RootFoot_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootFoot_Type_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Foot_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<Foot_Type_Bool_Exp>;
};


export type Subscription_RootImportArgs = {
  distinct_on?: InputMaybe<Array<Import_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Import_Order_By>>;
  where?: InputMaybe<Import_Bool_Exp>;
};


export type Subscription_RootImport_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Import_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Import_Order_By>>;
  where?: InputMaybe<Import_Bool_Exp>;
};


export type Subscription_RootImport_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootImport_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Import_Stream_Cursor_Input>>;
  where?: InputMaybe<Import_Bool_Exp>;
};


export type Subscription_RootInterventionArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Order_By>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};


export type Subscription_RootIntervention_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Order_By>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};


export type Subscription_RootIntervention_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootIntervention_PartnerArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Partner_Order_By>>;
  where?: InputMaybe<Intervention_Partner_Bool_Exp>;
};


export type Subscription_RootIntervention_Partner_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Partner_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Partner_Order_By>>;
  where?: InputMaybe<Intervention_Partner_Bool_Exp>;
};


export type Subscription_RootIntervention_Partner_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootIntervention_Partner_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Intervention_Partner_Stream_Cursor_Input>>;
  where?: InputMaybe<Intervention_Partner_Bool_Exp>;
};


export type Subscription_RootIntervention_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Intervention_Stream_Cursor_Input>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};


export type Subscription_RootIntervention_TypeArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Type_Order_By>>;
  where?: InputMaybe<Intervention_Type_Bool_Exp>;
};


export type Subscription_RootIntervention_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Type_Order_By>>;
  where?: InputMaybe<Intervention_Type_Bool_Exp>;
};


export type Subscription_RootIntervention_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootIntervention_Type_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Intervention_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<Intervention_Type_Bool_Exp>;
};


export type Subscription_RootLocationArgs = {
  distinct_on?: InputMaybe<Array<Location_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Order_By>>;
  where?: InputMaybe<Location_Bool_Exp>;
};


export type Subscription_RootLocation_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Location_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Order_By>>;
  where?: InputMaybe<Location_Bool_Exp>;
};


export type Subscription_RootLocation_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootLocation_StatusArgs = {
  distinct_on?: InputMaybe<Array<Location_Status_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Status_Order_By>>;
  where?: InputMaybe<Location_Status_Bool_Exp>;
};


export type Subscription_RootLocation_Status_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Location_Status_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Status_Order_By>>;
  where?: InputMaybe<Location_Status_Bool_Exp>;
};


export type Subscription_RootLocation_Status_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootLocation_Status_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Location_Status_Stream_Cursor_Input>>;
  where?: InputMaybe<Location_Status_Bool_Exp>;
};


export type Subscription_RootLocation_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Location_Stream_Cursor_Input>>;
  where?: InputMaybe<Location_Bool_Exp>;
};


export type Subscription_RootLocation_Urban_ConstraintArgs = {
  distinct_on?: InputMaybe<Array<Location_Urban_Constraint_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Urban_Constraint_Order_By>>;
  where?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
};


export type Subscription_RootLocation_Urban_Constraint_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Location_Urban_Constraint_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Location_Urban_Constraint_Order_By>>;
  where?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
};


export type Subscription_RootLocation_Urban_Constraint_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootLocation_Urban_Constraint_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Location_Urban_Constraint_Stream_Cursor_Input>>;
  where?: InputMaybe<Location_Urban_Constraint_Bool_Exp>;
};


export type Subscription_RootOrganizationArgs = {
  distinct_on?: InputMaybe<Array<Organization_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Organization_Order_By>>;
  where?: InputMaybe<Organization_Bool_Exp>;
};


export type Subscription_RootOrganization_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Organization_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Organization_Order_By>>;
  where?: InputMaybe<Organization_Bool_Exp>;
};


export type Subscription_RootOrganization_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootOrganization_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Organization_Stream_Cursor_Input>>;
  where?: InputMaybe<Organization_Bool_Exp>;
};


export type Subscription_RootPathogenArgs = {
  distinct_on?: InputMaybe<Array<Pathogen_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Pathogen_Order_By>>;
  where?: InputMaybe<Pathogen_Bool_Exp>;
};


export type Subscription_RootPathogen_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Pathogen_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Pathogen_Order_By>>;
  where?: InputMaybe<Pathogen_Bool_Exp>;
};


export type Subscription_RootPathogen_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootPathogen_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Pathogen_Stream_Cursor_Input>>;
  where?: InputMaybe<Pathogen_Bool_Exp>;
};


export type Subscription_RootResidential_Usage_TypeArgs = {
  distinct_on?: InputMaybe<Array<Residential_Usage_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Residential_Usage_Type_Order_By>>;
  where?: InputMaybe<Residential_Usage_Type_Bool_Exp>;
};


export type Subscription_RootResidential_Usage_Type_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Residential_Usage_Type_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Residential_Usage_Type_Order_By>>;
  where?: InputMaybe<Residential_Usage_Type_Bool_Exp>;
};


export type Subscription_RootResidential_Usage_Type_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootResidential_Usage_Type_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Residential_Usage_Type_Stream_Cursor_Input>>;
  where?: InputMaybe<Residential_Usage_Type_Bool_Exp>;
};


export type Subscription_RootTaxonArgs = {
  distinct_on?: InputMaybe<Array<Taxon_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Taxon_Order_By>>;
  where?: InputMaybe<Taxon_Bool_Exp>;
};


export type Subscription_RootTaxon_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Taxon_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Taxon_Order_By>>;
  where?: InputMaybe<Taxon_Bool_Exp>;
};


export type Subscription_RootTaxon_By_PkArgs = {
  id: Scalars['Int'];
};


export type Subscription_RootTaxon_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Taxon_Stream_Cursor_Input>>;
  where?: InputMaybe<Taxon_Bool_Exp>;
};


export type Subscription_RootTreeArgs = {
  distinct_on?: InputMaybe<Array<Tree_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Tree_Order_By>>;
  where?: InputMaybe<Tree_Bool_Exp>;
};


export type Subscription_RootTree_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Tree_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Tree_Order_By>>;
  where?: InputMaybe<Tree_Bool_Exp>;
};


export type Subscription_RootTree_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootTree_Scientific_Names_CountArgs = {
  distinct_on?: InputMaybe<Array<Tree_Scientific_Names_Count_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Tree_Scientific_Names_Count_Order_By>>;
  where?: InputMaybe<Tree_Scientific_Names_Count_Bool_Exp>;
};


export type Subscription_RootTree_Scientific_Names_Count_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Tree_Scientific_Names_Count_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Tree_Scientific_Names_Count_Order_By>>;
  where?: InputMaybe<Tree_Scientific_Names_Count_Bool_Exp>;
};


export type Subscription_RootTree_Scientific_Names_Count_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Tree_Scientific_Names_Count_Stream_Cursor_Input>>;
  where?: InputMaybe<Tree_Scientific_Names_Count_Bool_Exp>;
};


export type Subscription_RootTree_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Tree_Stream_Cursor_Input>>;
  where?: InputMaybe<Tree_Bool_Exp>;
};


export type Subscription_RootUrban_ConstraintArgs = {
  distinct_on?: InputMaybe<Array<Urban_Constraint_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Urban_Constraint_Order_By>>;
  where?: InputMaybe<Urban_Constraint_Bool_Exp>;
};


export type Subscription_RootUrban_Constraint_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Urban_Constraint_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Urban_Constraint_Order_By>>;
  where?: InputMaybe<Urban_Constraint_Bool_Exp>;
};


export type Subscription_RootUrban_Constraint_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootUrban_Constraint_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Urban_Constraint_Stream_Cursor_Input>>;
  where?: InputMaybe<Urban_Constraint_Bool_Exp>;
};


export type Subscription_RootUrban_SiteArgs = {
  distinct_on?: InputMaybe<Array<Urban_Site_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Urban_Site_Order_By>>;
  where?: InputMaybe<Urban_Site_Bool_Exp>;
};


export type Subscription_RootUrban_Site_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Urban_Site_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Urban_Site_Order_By>>;
  where?: InputMaybe<Urban_Site_Bool_Exp>;
};


export type Subscription_RootUrban_Site_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootUrban_Site_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Urban_Site_Stream_Cursor_Input>>;
  where?: InputMaybe<Urban_Site_Bool_Exp>;
};


export type Subscription_RootUser_EntityArgs = {
  distinct_on?: InputMaybe<Array<User_Entity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Entity_Order_By>>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};


export type Subscription_RootUser_Entity_AggregateArgs = {
  distinct_on?: InputMaybe<Array<User_Entity_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<User_Entity_Order_By>>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};


export type Subscription_RootUser_Entity_By_PkArgs = {
  id: Scalars['String'];
};


export type Subscription_RootUser_Entity_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<User_Entity_Stream_Cursor_Input>>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};


export type Subscription_RootVegetated_AreaArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Area_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Area_Order_By>>;
  where?: InputMaybe<Vegetated_Area_Bool_Exp>;
};


export type Subscription_RootVegetated_Area_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Area_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Area_Order_By>>;
  where?: InputMaybe<Vegetated_Area_Bool_Exp>;
};


export type Subscription_RootVegetated_Area_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootVegetated_Area_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Vegetated_Area_Stream_Cursor_Input>>;
  where?: InputMaybe<Vegetated_Area_Bool_Exp>;
};


export type Subscription_RootVegetated_Areas_LocationsArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Locations_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
};


export type Subscription_RootVegetated_Areas_Locations_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Locations_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
};


export type Subscription_RootVegetated_Areas_Locations_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootVegetated_Areas_Locations_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Vegetated_Areas_Locations_Stream_Cursor_Input>>;
  where?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
};


export type Subscription_RootVegetated_Areas_Residential_Usage_TypesArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
};


export type Subscription_RootVegetated_Areas_Residential_Usage_Types_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
};


export type Subscription_RootVegetated_Areas_Residential_Usage_Types_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootVegetated_Areas_Residential_Usage_Types_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Vegetated_Areas_Residential_Usage_Types_Stream_Cursor_Input>>;
  where?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
};


export type Subscription_RootVegetated_Areas_Urban_ConstraintsArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
};


export type Subscription_RootVegetated_Areas_Urban_Constraints_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
};


export type Subscription_RootVegetated_Areas_Urban_Constraints_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootVegetated_Areas_Urban_Constraints_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Vegetated_Areas_Urban_Constraints_Stream_Cursor_Input>>;
  where?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
};


export type Subscription_RootWorksiteArgs = {
  distinct_on?: InputMaybe<Array<Worksite_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Worksite_Order_By>>;
  where?: InputMaybe<Worksite_Bool_Exp>;
};


export type Subscription_RootWorksite_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Worksite_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Worksite_Order_By>>;
  where?: InputMaybe<Worksite_Bool_Exp>;
};


export type Subscription_RootWorksite_By_PkArgs = {
  id: Scalars['uuid'];
};


export type Subscription_RootWorksite_StreamArgs = {
  batch_size: Scalars['Int'];
  cursor: Array<InputMaybe<Worksite_Stream_Cursor_Input>>;
  where?: InputMaybe<Worksite_Bool_Exp>;
};

/** Tree taxonomic reference data */
export type Taxon = {
  __typename?: 'taxon';
  accepted?: Maybe<Scalars['String']>;
  accepted_key?: Maybe<Scalars['Int']>;
  air?: Maybe<Scalars['numeric']>;
  allergenic_score?: Maybe<Scalars['numeric']>;
  authorship?: Maybe<Scalars['String']>;
  basionym?: Maybe<Scalars['String']>;
  basionym_key?: Maybe<Scalars['Int']>;
  biodiversity?: Maybe<Scalars['numeric']>;
  canonical_name?: Maybe<Scalars['String']>;
  carbon?: Maybe<Scalars['numeric']>;
  class?: Maybe<Scalars['String']>;
  class_key?: Maybe<Scalars['Int']>;
  constituent_key?: Maybe<Scalars['String']>;
  dataset_key?: Maybe<Scalars['String']>;
  family?: Maybe<Scalars['String']>;
  family_key?: Maybe<Scalars['Int']>;
  genus?: Maybe<Scalars['String']>;
  genus_key?: Maybe<Scalars['Int']>;
  growth_category?: Maybe<Scalars['String']>;
  icu?: Maybe<Scalars['numeric']>;
  id: Scalars['Int'];
  key?: Maybe<Scalars['Int']>;
  kingdom?: Maybe<Scalars['String']>;
  kingdom_key?: Maybe<Scalars['Int']>;
  name_key?: Maybe<Scalars['Int']>;
  name_type?: Maybe<Scalars['String']>;
  nub_key?: Maybe<Scalars['Int']>;
  num_descendants?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['String']>;
  order_key?: Maybe<Scalars['Int']>;
  origin?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['String']>;
  parent_key?: Maybe<Scalars['Int']>;
  phylum?: Maybe<Scalars['String']>;
  phylum_key?: Maybe<Scalars['Int']>;
  published_in?: Maybe<Scalars['String']>;
  rank?: Maybe<Scalars['String']>;
  scientific_name?: Maybe<Scalars['String']>;
  source_taxon_key?: Maybe<Scalars['Int']>;
  species?: Maybe<Scalars['String']>;
  species_key?: Maybe<Scalars['Int']>;
  synonym?: Maybe<Scalars['String']>;
  taxon_id?: Maybe<Scalars['String']>;
  taxonomic_status?: Maybe<Scalars['String']>;
  vernacular_name?: Maybe<Scalars['String']>;
  vulnerability?: Maybe<Scalars['numeric']>;
};

/** aggregated selection of "taxon" */
export type Taxon_Aggregate = {
  __typename?: 'taxon_aggregate';
  aggregate?: Maybe<Taxon_Aggregate_Fields>;
  nodes: Array<Taxon>;
};

/** aggregate fields of "taxon" */
export type Taxon_Aggregate_Fields = {
  __typename?: 'taxon_aggregate_fields';
  avg?: Maybe<Taxon_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Taxon_Max_Fields>;
  min?: Maybe<Taxon_Min_Fields>;
  stddev?: Maybe<Taxon_Stddev_Fields>;
  stddev_pop?: Maybe<Taxon_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Taxon_Stddev_Samp_Fields>;
  sum?: Maybe<Taxon_Sum_Fields>;
  var_pop?: Maybe<Taxon_Var_Pop_Fields>;
  var_samp?: Maybe<Taxon_Var_Samp_Fields>;
  variance?: Maybe<Taxon_Variance_Fields>;
};


/** aggregate fields of "taxon" */
export type Taxon_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Taxon_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type Taxon_Avg_Fields = {
  __typename?: 'taxon_avg_fields';
  accepted_key?: Maybe<Scalars['Float']>;
  air?: Maybe<Scalars['Float']>;
  allergenic_score?: Maybe<Scalars['Float']>;
  basionym_key?: Maybe<Scalars['Float']>;
  biodiversity?: Maybe<Scalars['Float']>;
  carbon?: Maybe<Scalars['Float']>;
  class_key?: Maybe<Scalars['Float']>;
  family_key?: Maybe<Scalars['Float']>;
  genus_key?: Maybe<Scalars['Float']>;
  icu?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  key?: Maybe<Scalars['Float']>;
  kingdom_key?: Maybe<Scalars['Float']>;
  name_key?: Maybe<Scalars['Float']>;
  nub_key?: Maybe<Scalars['Float']>;
  num_descendants?: Maybe<Scalars['Float']>;
  order_key?: Maybe<Scalars['Float']>;
  parent_key?: Maybe<Scalars['Float']>;
  phylum_key?: Maybe<Scalars['Float']>;
  source_taxon_key?: Maybe<Scalars['Float']>;
  species_key?: Maybe<Scalars['Float']>;
  vulnerability?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "taxon". All fields are combined with a logical 'AND'. */
export type Taxon_Bool_Exp = {
  _and?: InputMaybe<Array<Taxon_Bool_Exp>>;
  _not?: InputMaybe<Taxon_Bool_Exp>;
  _or?: InputMaybe<Array<Taxon_Bool_Exp>>;
  accepted?: InputMaybe<String_Comparison_Exp>;
  accepted_key?: InputMaybe<Int_Comparison_Exp>;
  air?: InputMaybe<Numeric_Comparison_Exp>;
  allergenic_score?: InputMaybe<Numeric_Comparison_Exp>;
  authorship?: InputMaybe<String_Comparison_Exp>;
  basionym?: InputMaybe<String_Comparison_Exp>;
  basionym_key?: InputMaybe<Int_Comparison_Exp>;
  biodiversity?: InputMaybe<Numeric_Comparison_Exp>;
  canonical_name?: InputMaybe<String_Comparison_Exp>;
  carbon?: InputMaybe<Numeric_Comparison_Exp>;
  class?: InputMaybe<String_Comparison_Exp>;
  class_key?: InputMaybe<Int_Comparison_Exp>;
  constituent_key?: InputMaybe<String_Comparison_Exp>;
  dataset_key?: InputMaybe<String_Comparison_Exp>;
  family?: InputMaybe<String_Comparison_Exp>;
  family_key?: InputMaybe<Int_Comparison_Exp>;
  genus?: InputMaybe<String_Comparison_Exp>;
  genus_key?: InputMaybe<Int_Comparison_Exp>;
  growth_category?: InputMaybe<String_Comparison_Exp>;
  icu?: InputMaybe<Numeric_Comparison_Exp>;
  id?: InputMaybe<Int_Comparison_Exp>;
  key?: InputMaybe<Int_Comparison_Exp>;
  kingdom?: InputMaybe<String_Comparison_Exp>;
  kingdom_key?: InputMaybe<Int_Comparison_Exp>;
  name_key?: InputMaybe<Int_Comparison_Exp>;
  name_type?: InputMaybe<String_Comparison_Exp>;
  nub_key?: InputMaybe<Int_Comparison_Exp>;
  num_descendants?: InputMaybe<Int_Comparison_Exp>;
  order?: InputMaybe<String_Comparison_Exp>;
  order_key?: InputMaybe<Int_Comparison_Exp>;
  origin?: InputMaybe<String_Comparison_Exp>;
  parent?: InputMaybe<String_Comparison_Exp>;
  parent_key?: InputMaybe<Int_Comparison_Exp>;
  phylum?: InputMaybe<String_Comparison_Exp>;
  phylum_key?: InputMaybe<Int_Comparison_Exp>;
  published_in?: InputMaybe<String_Comparison_Exp>;
  rank?: InputMaybe<String_Comparison_Exp>;
  scientific_name?: InputMaybe<String_Comparison_Exp>;
  source_taxon_key?: InputMaybe<Int_Comparison_Exp>;
  species?: InputMaybe<String_Comparison_Exp>;
  species_key?: InputMaybe<Int_Comparison_Exp>;
  synonym?: InputMaybe<String_Comparison_Exp>;
  taxon_id?: InputMaybe<String_Comparison_Exp>;
  taxonomic_status?: InputMaybe<String_Comparison_Exp>;
  vernacular_name?: InputMaybe<String_Comparison_Exp>;
  vulnerability?: InputMaybe<Numeric_Comparison_Exp>;
};

/** unique or primary key constraints on table "taxon" */
export enum Taxon_Constraint {
  /** unique or primary key constraint on columns "id" */
  TaxonPkey = 'taxon_pkey'
}

/** input type for incrementing numeric columns in table "taxon" */
export type Taxon_Inc_Input = {
  accepted_key?: InputMaybe<Scalars['Int']>;
  air?: InputMaybe<Scalars['numeric']>;
  allergenic_score?: InputMaybe<Scalars['numeric']>;
  basionym_key?: InputMaybe<Scalars['Int']>;
  biodiversity?: InputMaybe<Scalars['numeric']>;
  carbon?: InputMaybe<Scalars['numeric']>;
  class_key?: InputMaybe<Scalars['Int']>;
  family_key?: InputMaybe<Scalars['Int']>;
  genus_key?: InputMaybe<Scalars['Int']>;
  icu?: InputMaybe<Scalars['numeric']>;
  id?: InputMaybe<Scalars['Int']>;
  key?: InputMaybe<Scalars['Int']>;
  kingdom_key?: InputMaybe<Scalars['Int']>;
  name_key?: InputMaybe<Scalars['Int']>;
  nub_key?: InputMaybe<Scalars['Int']>;
  num_descendants?: InputMaybe<Scalars['Int']>;
  order_key?: InputMaybe<Scalars['Int']>;
  parent_key?: InputMaybe<Scalars['Int']>;
  phylum_key?: InputMaybe<Scalars['Int']>;
  source_taxon_key?: InputMaybe<Scalars['Int']>;
  species_key?: InputMaybe<Scalars['Int']>;
  vulnerability?: InputMaybe<Scalars['numeric']>;
};

/** input type for inserting data into table "taxon" */
export type Taxon_Insert_Input = {
  accepted?: InputMaybe<Scalars['String']>;
  accepted_key?: InputMaybe<Scalars['Int']>;
  air?: InputMaybe<Scalars['numeric']>;
  allergenic_score?: InputMaybe<Scalars['numeric']>;
  authorship?: InputMaybe<Scalars['String']>;
  basionym?: InputMaybe<Scalars['String']>;
  basionym_key?: InputMaybe<Scalars['Int']>;
  biodiversity?: InputMaybe<Scalars['numeric']>;
  canonical_name?: InputMaybe<Scalars['String']>;
  carbon?: InputMaybe<Scalars['numeric']>;
  class?: InputMaybe<Scalars['String']>;
  class_key?: InputMaybe<Scalars['Int']>;
  constituent_key?: InputMaybe<Scalars['String']>;
  dataset_key?: InputMaybe<Scalars['String']>;
  family?: InputMaybe<Scalars['String']>;
  family_key?: InputMaybe<Scalars['Int']>;
  genus?: InputMaybe<Scalars['String']>;
  genus_key?: InputMaybe<Scalars['Int']>;
  growth_category?: InputMaybe<Scalars['String']>;
  icu?: InputMaybe<Scalars['numeric']>;
  id?: InputMaybe<Scalars['Int']>;
  key?: InputMaybe<Scalars['Int']>;
  kingdom?: InputMaybe<Scalars['String']>;
  kingdom_key?: InputMaybe<Scalars['Int']>;
  name_key?: InputMaybe<Scalars['Int']>;
  name_type?: InputMaybe<Scalars['String']>;
  nub_key?: InputMaybe<Scalars['Int']>;
  num_descendants?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Scalars['String']>;
  order_key?: InputMaybe<Scalars['Int']>;
  origin?: InputMaybe<Scalars['String']>;
  parent?: InputMaybe<Scalars['String']>;
  parent_key?: InputMaybe<Scalars['Int']>;
  phylum?: InputMaybe<Scalars['String']>;
  phylum_key?: InputMaybe<Scalars['Int']>;
  published_in?: InputMaybe<Scalars['String']>;
  rank?: InputMaybe<Scalars['String']>;
  scientific_name?: InputMaybe<Scalars['String']>;
  source_taxon_key?: InputMaybe<Scalars['Int']>;
  species?: InputMaybe<Scalars['String']>;
  species_key?: InputMaybe<Scalars['Int']>;
  synonym?: InputMaybe<Scalars['String']>;
  taxon_id?: InputMaybe<Scalars['String']>;
  taxonomic_status?: InputMaybe<Scalars['String']>;
  vernacular_name?: InputMaybe<Scalars['String']>;
  vulnerability?: InputMaybe<Scalars['numeric']>;
};

/** aggregate max on columns */
export type Taxon_Max_Fields = {
  __typename?: 'taxon_max_fields';
  accepted?: Maybe<Scalars['String']>;
  accepted_key?: Maybe<Scalars['Int']>;
  air?: Maybe<Scalars['numeric']>;
  allergenic_score?: Maybe<Scalars['numeric']>;
  authorship?: Maybe<Scalars['String']>;
  basionym?: Maybe<Scalars['String']>;
  basionym_key?: Maybe<Scalars['Int']>;
  biodiversity?: Maybe<Scalars['numeric']>;
  canonical_name?: Maybe<Scalars['String']>;
  carbon?: Maybe<Scalars['numeric']>;
  class?: Maybe<Scalars['String']>;
  class_key?: Maybe<Scalars['Int']>;
  constituent_key?: Maybe<Scalars['String']>;
  dataset_key?: Maybe<Scalars['String']>;
  family?: Maybe<Scalars['String']>;
  family_key?: Maybe<Scalars['Int']>;
  genus?: Maybe<Scalars['String']>;
  genus_key?: Maybe<Scalars['Int']>;
  growth_category?: Maybe<Scalars['String']>;
  icu?: Maybe<Scalars['numeric']>;
  id?: Maybe<Scalars['Int']>;
  key?: Maybe<Scalars['Int']>;
  kingdom?: Maybe<Scalars['String']>;
  kingdom_key?: Maybe<Scalars['Int']>;
  name_key?: Maybe<Scalars['Int']>;
  name_type?: Maybe<Scalars['String']>;
  nub_key?: Maybe<Scalars['Int']>;
  num_descendants?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['String']>;
  order_key?: Maybe<Scalars['Int']>;
  origin?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['String']>;
  parent_key?: Maybe<Scalars['Int']>;
  phylum?: Maybe<Scalars['String']>;
  phylum_key?: Maybe<Scalars['Int']>;
  published_in?: Maybe<Scalars['String']>;
  rank?: Maybe<Scalars['String']>;
  scientific_name?: Maybe<Scalars['String']>;
  source_taxon_key?: Maybe<Scalars['Int']>;
  species?: Maybe<Scalars['String']>;
  species_key?: Maybe<Scalars['Int']>;
  synonym?: Maybe<Scalars['String']>;
  taxon_id?: Maybe<Scalars['String']>;
  taxonomic_status?: Maybe<Scalars['String']>;
  vernacular_name?: Maybe<Scalars['String']>;
  vulnerability?: Maybe<Scalars['numeric']>;
};

/** aggregate min on columns */
export type Taxon_Min_Fields = {
  __typename?: 'taxon_min_fields';
  accepted?: Maybe<Scalars['String']>;
  accepted_key?: Maybe<Scalars['Int']>;
  air?: Maybe<Scalars['numeric']>;
  allergenic_score?: Maybe<Scalars['numeric']>;
  authorship?: Maybe<Scalars['String']>;
  basionym?: Maybe<Scalars['String']>;
  basionym_key?: Maybe<Scalars['Int']>;
  biodiversity?: Maybe<Scalars['numeric']>;
  canonical_name?: Maybe<Scalars['String']>;
  carbon?: Maybe<Scalars['numeric']>;
  class?: Maybe<Scalars['String']>;
  class_key?: Maybe<Scalars['Int']>;
  constituent_key?: Maybe<Scalars['String']>;
  dataset_key?: Maybe<Scalars['String']>;
  family?: Maybe<Scalars['String']>;
  family_key?: Maybe<Scalars['Int']>;
  genus?: Maybe<Scalars['String']>;
  genus_key?: Maybe<Scalars['Int']>;
  growth_category?: Maybe<Scalars['String']>;
  icu?: Maybe<Scalars['numeric']>;
  id?: Maybe<Scalars['Int']>;
  key?: Maybe<Scalars['Int']>;
  kingdom?: Maybe<Scalars['String']>;
  kingdom_key?: Maybe<Scalars['Int']>;
  name_key?: Maybe<Scalars['Int']>;
  name_type?: Maybe<Scalars['String']>;
  nub_key?: Maybe<Scalars['Int']>;
  num_descendants?: Maybe<Scalars['Int']>;
  order?: Maybe<Scalars['String']>;
  order_key?: Maybe<Scalars['Int']>;
  origin?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['String']>;
  parent_key?: Maybe<Scalars['Int']>;
  phylum?: Maybe<Scalars['String']>;
  phylum_key?: Maybe<Scalars['Int']>;
  published_in?: Maybe<Scalars['String']>;
  rank?: Maybe<Scalars['String']>;
  scientific_name?: Maybe<Scalars['String']>;
  source_taxon_key?: Maybe<Scalars['Int']>;
  species?: Maybe<Scalars['String']>;
  species_key?: Maybe<Scalars['Int']>;
  synonym?: Maybe<Scalars['String']>;
  taxon_id?: Maybe<Scalars['String']>;
  taxonomic_status?: Maybe<Scalars['String']>;
  vernacular_name?: Maybe<Scalars['String']>;
  vulnerability?: Maybe<Scalars['numeric']>;
};

/** response of any mutation on the table "taxon" */
export type Taxon_Mutation_Response = {
  __typename?: 'taxon_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Taxon>;
};

/** input type for inserting object relation for remote table "taxon" */
export type Taxon_Obj_Rel_Insert_Input = {
  data: Taxon_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Taxon_On_Conflict>;
};

/** on_conflict condition type for table "taxon" */
export type Taxon_On_Conflict = {
  constraint: Taxon_Constraint;
  update_columns?: Array<Taxon_Update_Column>;
  where?: InputMaybe<Taxon_Bool_Exp>;
};

/** Ordering options when selecting data from "taxon". */
export type Taxon_Order_By = {
  accepted?: InputMaybe<Order_By>;
  accepted_key?: InputMaybe<Order_By>;
  air?: InputMaybe<Order_By>;
  allergenic_score?: InputMaybe<Order_By>;
  authorship?: InputMaybe<Order_By>;
  basionym?: InputMaybe<Order_By>;
  basionym_key?: InputMaybe<Order_By>;
  biodiversity?: InputMaybe<Order_By>;
  canonical_name?: InputMaybe<Order_By>;
  carbon?: InputMaybe<Order_By>;
  class?: InputMaybe<Order_By>;
  class_key?: InputMaybe<Order_By>;
  constituent_key?: InputMaybe<Order_By>;
  dataset_key?: InputMaybe<Order_By>;
  family?: InputMaybe<Order_By>;
  family_key?: InputMaybe<Order_By>;
  genus?: InputMaybe<Order_By>;
  genus_key?: InputMaybe<Order_By>;
  growth_category?: InputMaybe<Order_By>;
  icu?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  key?: InputMaybe<Order_By>;
  kingdom?: InputMaybe<Order_By>;
  kingdom_key?: InputMaybe<Order_By>;
  name_key?: InputMaybe<Order_By>;
  name_type?: InputMaybe<Order_By>;
  nub_key?: InputMaybe<Order_By>;
  num_descendants?: InputMaybe<Order_By>;
  order?: InputMaybe<Order_By>;
  order_key?: InputMaybe<Order_By>;
  origin?: InputMaybe<Order_By>;
  parent?: InputMaybe<Order_By>;
  parent_key?: InputMaybe<Order_By>;
  phylum?: InputMaybe<Order_By>;
  phylum_key?: InputMaybe<Order_By>;
  published_in?: InputMaybe<Order_By>;
  rank?: InputMaybe<Order_By>;
  scientific_name?: InputMaybe<Order_By>;
  source_taxon_key?: InputMaybe<Order_By>;
  species?: InputMaybe<Order_By>;
  species_key?: InputMaybe<Order_By>;
  synonym?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
  taxonomic_status?: InputMaybe<Order_By>;
  vernacular_name?: InputMaybe<Order_By>;
  vulnerability?: InputMaybe<Order_By>;
};

/** primary key columns input for table: taxon */
export type Taxon_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "taxon" */
export enum Taxon_Select_Column {
  /** column name */
  Accepted = 'accepted',
  /** column name */
  AcceptedKey = 'accepted_key',
  /** column name */
  Air = 'air',
  /** column name */
  AllergenicScore = 'allergenic_score',
  /** column name */
  Authorship = 'authorship',
  /** column name */
  Basionym = 'basionym',
  /** column name */
  BasionymKey = 'basionym_key',
  /** column name */
  Biodiversity = 'biodiversity',
  /** column name */
  CanonicalName = 'canonical_name',
  /** column name */
  Carbon = 'carbon',
  /** column name */
  Class = 'class',
  /** column name */
  ClassKey = 'class_key',
  /** column name */
  ConstituentKey = 'constituent_key',
  /** column name */
  DatasetKey = 'dataset_key',
  /** column name */
  Family = 'family',
  /** column name */
  FamilyKey = 'family_key',
  /** column name */
  Genus = 'genus',
  /** column name */
  GenusKey = 'genus_key',
  /** column name */
  GrowthCategory = 'growth_category',
  /** column name */
  Icu = 'icu',
  /** column name */
  Id = 'id',
  /** column name */
  Key = 'key',
  /** column name */
  Kingdom = 'kingdom',
  /** column name */
  KingdomKey = 'kingdom_key',
  /** column name */
  NameKey = 'name_key',
  /** column name */
  NameType = 'name_type',
  /** column name */
  NubKey = 'nub_key',
  /** column name */
  NumDescendants = 'num_descendants',
  /** column name */
  Order = 'order',
  /** column name */
  OrderKey = 'order_key',
  /** column name */
  Origin = 'origin',
  /** column name */
  Parent = 'parent',
  /** column name */
  ParentKey = 'parent_key',
  /** column name */
  Phylum = 'phylum',
  /** column name */
  PhylumKey = 'phylum_key',
  /** column name */
  PublishedIn = 'published_in',
  /** column name */
  Rank = 'rank',
  /** column name */
  ScientificName = 'scientific_name',
  /** column name */
  SourceTaxonKey = 'source_taxon_key',
  /** column name */
  Species = 'species',
  /** column name */
  SpeciesKey = 'species_key',
  /** column name */
  Synonym = 'synonym',
  /** column name */
  TaxonId = 'taxon_id',
  /** column name */
  TaxonomicStatus = 'taxonomic_status',
  /** column name */
  VernacularName = 'vernacular_name',
  /** column name */
  Vulnerability = 'vulnerability'
}

/** input type for updating data in table "taxon" */
export type Taxon_Set_Input = {
  accepted?: InputMaybe<Scalars['String']>;
  accepted_key?: InputMaybe<Scalars['Int']>;
  air?: InputMaybe<Scalars['numeric']>;
  allergenic_score?: InputMaybe<Scalars['numeric']>;
  authorship?: InputMaybe<Scalars['String']>;
  basionym?: InputMaybe<Scalars['String']>;
  basionym_key?: InputMaybe<Scalars['Int']>;
  biodiversity?: InputMaybe<Scalars['numeric']>;
  canonical_name?: InputMaybe<Scalars['String']>;
  carbon?: InputMaybe<Scalars['numeric']>;
  class?: InputMaybe<Scalars['String']>;
  class_key?: InputMaybe<Scalars['Int']>;
  constituent_key?: InputMaybe<Scalars['String']>;
  dataset_key?: InputMaybe<Scalars['String']>;
  family?: InputMaybe<Scalars['String']>;
  family_key?: InputMaybe<Scalars['Int']>;
  genus?: InputMaybe<Scalars['String']>;
  genus_key?: InputMaybe<Scalars['Int']>;
  growth_category?: InputMaybe<Scalars['String']>;
  icu?: InputMaybe<Scalars['numeric']>;
  id?: InputMaybe<Scalars['Int']>;
  key?: InputMaybe<Scalars['Int']>;
  kingdom?: InputMaybe<Scalars['String']>;
  kingdom_key?: InputMaybe<Scalars['Int']>;
  name_key?: InputMaybe<Scalars['Int']>;
  name_type?: InputMaybe<Scalars['String']>;
  nub_key?: InputMaybe<Scalars['Int']>;
  num_descendants?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Scalars['String']>;
  order_key?: InputMaybe<Scalars['Int']>;
  origin?: InputMaybe<Scalars['String']>;
  parent?: InputMaybe<Scalars['String']>;
  parent_key?: InputMaybe<Scalars['Int']>;
  phylum?: InputMaybe<Scalars['String']>;
  phylum_key?: InputMaybe<Scalars['Int']>;
  published_in?: InputMaybe<Scalars['String']>;
  rank?: InputMaybe<Scalars['String']>;
  scientific_name?: InputMaybe<Scalars['String']>;
  source_taxon_key?: InputMaybe<Scalars['Int']>;
  species?: InputMaybe<Scalars['String']>;
  species_key?: InputMaybe<Scalars['Int']>;
  synonym?: InputMaybe<Scalars['String']>;
  taxon_id?: InputMaybe<Scalars['String']>;
  taxonomic_status?: InputMaybe<Scalars['String']>;
  vernacular_name?: InputMaybe<Scalars['String']>;
  vulnerability?: InputMaybe<Scalars['numeric']>;
};

/** aggregate stddev on columns */
export type Taxon_Stddev_Fields = {
  __typename?: 'taxon_stddev_fields';
  accepted_key?: Maybe<Scalars['Float']>;
  air?: Maybe<Scalars['Float']>;
  allergenic_score?: Maybe<Scalars['Float']>;
  basionym_key?: Maybe<Scalars['Float']>;
  biodiversity?: Maybe<Scalars['Float']>;
  carbon?: Maybe<Scalars['Float']>;
  class_key?: Maybe<Scalars['Float']>;
  family_key?: Maybe<Scalars['Float']>;
  genus_key?: Maybe<Scalars['Float']>;
  icu?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  key?: Maybe<Scalars['Float']>;
  kingdom_key?: Maybe<Scalars['Float']>;
  name_key?: Maybe<Scalars['Float']>;
  nub_key?: Maybe<Scalars['Float']>;
  num_descendants?: Maybe<Scalars['Float']>;
  order_key?: Maybe<Scalars['Float']>;
  parent_key?: Maybe<Scalars['Float']>;
  phylum_key?: Maybe<Scalars['Float']>;
  source_taxon_key?: Maybe<Scalars['Float']>;
  species_key?: Maybe<Scalars['Float']>;
  vulnerability?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Taxon_Stddev_Pop_Fields = {
  __typename?: 'taxon_stddev_pop_fields';
  accepted_key?: Maybe<Scalars['Float']>;
  air?: Maybe<Scalars['Float']>;
  allergenic_score?: Maybe<Scalars['Float']>;
  basionym_key?: Maybe<Scalars['Float']>;
  biodiversity?: Maybe<Scalars['Float']>;
  carbon?: Maybe<Scalars['Float']>;
  class_key?: Maybe<Scalars['Float']>;
  family_key?: Maybe<Scalars['Float']>;
  genus_key?: Maybe<Scalars['Float']>;
  icu?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  key?: Maybe<Scalars['Float']>;
  kingdom_key?: Maybe<Scalars['Float']>;
  name_key?: Maybe<Scalars['Float']>;
  nub_key?: Maybe<Scalars['Float']>;
  num_descendants?: Maybe<Scalars['Float']>;
  order_key?: Maybe<Scalars['Float']>;
  parent_key?: Maybe<Scalars['Float']>;
  phylum_key?: Maybe<Scalars['Float']>;
  source_taxon_key?: Maybe<Scalars['Float']>;
  species_key?: Maybe<Scalars['Float']>;
  vulnerability?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Taxon_Stddev_Samp_Fields = {
  __typename?: 'taxon_stddev_samp_fields';
  accepted_key?: Maybe<Scalars['Float']>;
  air?: Maybe<Scalars['Float']>;
  allergenic_score?: Maybe<Scalars['Float']>;
  basionym_key?: Maybe<Scalars['Float']>;
  biodiversity?: Maybe<Scalars['Float']>;
  carbon?: Maybe<Scalars['Float']>;
  class_key?: Maybe<Scalars['Float']>;
  family_key?: Maybe<Scalars['Float']>;
  genus_key?: Maybe<Scalars['Float']>;
  icu?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  key?: Maybe<Scalars['Float']>;
  kingdom_key?: Maybe<Scalars['Float']>;
  name_key?: Maybe<Scalars['Float']>;
  nub_key?: Maybe<Scalars['Float']>;
  num_descendants?: Maybe<Scalars['Float']>;
  order_key?: Maybe<Scalars['Float']>;
  parent_key?: Maybe<Scalars['Float']>;
  phylum_key?: Maybe<Scalars['Float']>;
  source_taxon_key?: Maybe<Scalars['Float']>;
  species_key?: Maybe<Scalars['Float']>;
  vulnerability?: Maybe<Scalars['Float']>;
};

/** Streaming cursor of the table "taxon" */
export type Taxon_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Taxon_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Taxon_Stream_Cursor_Value_Input = {
  accepted?: InputMaybe<Scalars['String']>;
  accepted_key?: InputMaybe<Scalars['Int']>;
  air?: InputMaybe<Scalars['numeric']>;
  allergenic_score?: InputMaybe<Scalars['numeric']>;
  authorship?: InputMaybe<Scalars['String']>;
  basionym?: InputMaybe<Scalars['String']>;
  basionym_key?: InputMaybe<Scalars['Int']>;
  biodiversity?: InputMaybe<Scalars['numeric']>;
  canonical_name?: InputMaybe<Scalars['String']>;
  carbon?: InputMaybe<Scalars['numeric']>;
  class?: InputMaybe<Scalars['String']>;
  class_key?: InputMaybe<Scalars['Int']>;
  constituent_key?: InputMaybe<Scalars['String']>;
  dataset_key?: InputMaybe<Scalars['String']>;
  family?: InputMaybe<Scalars['String']>;
  family_key?: InputMaybe<Scalars['Int']>;
  genus?: InputMaybe<Scalars['String']>;
  genus_key?: InputMaybe<Scalars['Int']>;
  growth_category?: InputMaybe<Scalars['String']>;
  icu?: InputMaybe<Scalars['numeric']>;
  id?: InputMaybe<Scalars['Int']>;
  key?: InputMaybe<Scalars['Int']>;
  kingdom?: InputMaybe<Scalars['String']>;
  kingdom_key?: InputMaybe<Scalars['Int']>;
  name_key?: InputMaybe<Scalars['Int']>;
  name_type?: InputMaybe<Scalars['String']>;
  nub_key?: InputMaybe<Scalars['Int']>;
  num_descendants?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Scalars['String']>;
  order_key?: InputMaybe<Scalars['Int']>;
  origin?: InputMaybe<Scalars['String']>;
  parent?: InputMaybe<Scalars['String']>;
  parent_key?: InputMaybe<Scalars['Int']>;
  phylum?: InputMaybe<Scalars['String']>;
  phylum_key?: InputMaybe<Scalars['Int']>;
  published_in?: InputMaybe<Scalars['String']>;
  rank?: InputMaybe<Scalars['String']>;
  scientific_name?: InputMaybe<Scalars['String']>;
  source_taxon_key?: InputMaybe<Scalars['Int']>;
  species?: InputMaybe<Scalars['String']>;
  species_key?: InputMaybe<Scalars['Int']>;
  synonym?: InputMaybe<Scalars['String']>;
  taxon_id?: InputMaybe<Scalars['String']>;
  taxonomic_status?: InputMaybe<Scalars['String']>;
  vernacular_name?: InputMaybe<Scalars['String']>;
  vulnerability?: InputMaybe<Scalars['numeric']>;
};

/** aggregate sum on columns */
export type Taxon_Sum_Fields = {
  __typename?: 'taxon_sum_fields';
  accepted_key?: Maybe<Scalars['Int']>;
  air?: Maybe<Scalars['numeric']>;
  allergenic_score?: Maybe<Scalars['numeric']>;
  basionym_key?: Maybe<Scalars['Int']>;
  biodiversity?: Maybe<Scalars['numeric']>;
  carbon?: Maybe<Scalars['numeric']>;
  class_key?: Maybe<Scalars['Int']>;
  family_key?: Maybe<Scalars['Int']>;
  genus_key?: Maybe<Scalars['Int']>;
  icu?: Maybe<Scalars['numeric']>;
  id?: Maybe<Scalars['Int']>;
  key?: Maybe<Scalars['Int']>;
  kingdom_key?: Maybe<Scalars['Int']>;
  name_key?: Maybe<Scalars['Int']>;
  nub_key?: Maybe<Scalars['Int']>;
  num_descendants?: Maybe<Scalars['Int']>;
  order_key?: Maybe<Scalars['Int']>;
  parent_key?: Maybe<Scalars['Int']>;
  phylum_key?: Maybe<Scalars['Int']>;
  source_taxon_key?: Maybe<Scalars['Int']>;
  species_key?: Maybe<Scalars['Int']>;
  vulnerability?: Maybe<Scalars['numeric']>;
};

/** update columns of table "taxon" */
export enum Taxon_Update_Column {
  /** column name */
  Accepted = 'accepted',
  /** column name */
  AcceptedKey = 'accepted_key',
  /** column name */
  Air = 'air',
  /** column name */
  AllergenicScore = 'allergenic_score',
  /** column name */
  Authorship = 'authorship',
  /** column name */
  Basionym = 'basionym',
  /** column name */
  BasionymKey = 'basionym_key',
  /** column name */
  Biodiversity = 'biodiversity',
  /** column name */
  CanonicalName = 'canonical_name',
  /** column name */
  Carbon = 'carbon',
  /** column name */
  Class = 'class',
  /** column name */
  ClassKey = 'class_key',
  /** column name */
  ConstituentKey = 'constituent_key',
  /** column name */
  DatasetKey = 'dataset_key',
  /** column name */
  Family = 'family',
  /** column name */
  FamilyKey = 'family_key',
  /** column name */
  Genus = 'genus',
  /** column name */
  GenusKey = 'genus_key',
  /** column name */
  GrowthCategory = 'growth_category',
  /** column name */
  Icu = 'icu',
  /** column name */
  Id = 'id',
  /** column name */
  Key = 'key',
  /** column name */
  Kingdom = 'kingdom',
  /** column name */
  KingdomKey = 'kingdom_key',
  /** column name */
  NameKey = 'name_key',
  /** column name */
  NameType = 'name_type',
  /** column name */
  NubKey = 'nub_key',
  /** column name */
  NumDescendants = 'num_descendants',
  /** column name */
  Order = 'order',
  /** column name */
  OrderKey = 'order_key',
  /** column name */
  Origin = 'origin',
  /** column name */
  Parent = 'parent',
  /** column name */
  ParentKey = 'parent_key',
  /** column name */
  Phylum = 'phylum',
  /** column name */
  PhylumKey = 'phylum_key',
  /** column name */
  PublishedIn = 'published_in',
  /** column name */
  Rank = 'rank',
  /** column name */
  ScientificName = 'scientific_name',
  /** column name */
  SourceTaxonKey = 'source_taxon_key',
  /** column name */
  Species = 'species',
  /** column name */
  SpeciesKey = 'species_key',
  /** column name */
  Synonym = 'synonym',
  /** column name */
  TaxonId = 'taxon_id',
  /** column name */
  TaxonomicStatus = 'taxonomic_status',
  /** column name */
  VernacularName = 'vernacular_name',
  /** column name */
  Vulnerability = 'vulnerability'
}

export type Taxon_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Taxon_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Taxon_Set_Input>;
  /** filter the rows which have to be updated */
  where: Taxon_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Taxon_Var_Pop_Fields = {
  __typename?: 'taxon_var_pop_fields';
  accepted_key?: Maybe<Scalars['Float']>;
  air?: Maybe<Scalars['Float']>;
  allergenic_score?: Maybe<Scalars['Float']>;
  basionym_key?: Maybe<Scalars['Float']>;
  biodiversity?: Maybe<Scalars['Float']>;
  carbon?: Maybe<Scalars['Float']>;
  class_key?: Maybe<Scalars['Float']>;
  family_key?: Maybe<Scalars['Float']>;
  genus_key?: Maybe<Scalars['Float']>;
  icu?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  key?: Maybe<Scalars['Float']>;
  kingdom_key?: Maybe<Scalars['Float']>;
  name_key?: Maybe<Scalars['Float']>;
  nub_key?: Maybe<Scalars['Float']>;
  num_descendants?: Maybe<Scalars['Float']>;
  order_key?: Maybe<Scalars['Float']>;
  parent_key?: Maybe<Scalars['Float']>;
  phylum_key?: Maybe<Scalars['Float']>;
  source_taxon_key?: Maybe<Scalars['Float']>;
  species_key?: Maybe<Scalars['Float']>;
  vulnerability?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Taxon_Var_Samp_Fields = {
  __typename?: 'taxon_var_samp_fields';
  accepted_key?: Maybe<Scalars['Float']>;
  air?: Maybe<Scalars['Float']>;
  allergenic_score?: Maybe<Scalars['Float']>;
  basionym_key?: Maybe<Scalars['Float']>;
  biodiversity?: Maybe<Scalars['Float']>;
  carbon?: Maybe<Scalars['Float']>;
  class_key?: Maybe<Scalars['Float']>;
  family_key?: Maybe<Scalars['Float']>;
  genus_key?: Maybe<Scalars['Float']>;
  icu?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  key?: Maybe<Scalars['Float']>;
  kingdom_key?: Maybe<Scalars['Float']>;
  name_key?: Maybe<Scalars['Float']>;
  nub_key?: Maybe<Scalars['Float']>;
  num_descendants?: Maybe<Scalars['Float']>;
  order_key?: Maybe<Scalars['Float']>;
  parent_key?: Maybe<Scalars['Float']>;
  phylum_key?: Maybe<Scalars['Float']>;
  source_taxon_key?: Maybe<Scalars['Float']>;
  species_key?: Maybe<Scalars['Float']>;
  vulnerability?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Taxon_Variance_Fields = {
  __typename?: 'taxon_variance_fields';
  accepted_key?: Maybe<Scalars['Float']>;
  air?: Maybe<Scalars['Float']>;
  allergenic_score?: Maybe<Scalars['Float']>;
  basionym_key?: Maybe<Scalars['Float']>;
  biodiversity?: Maybe<Scalars['Float']>;
  carbon?: Maybe<Scalars['Float']>;
  class_key?: Maybe<Scalars['Float']>;
  family_key?: Maybe<Scalars['Float']>;
  genus_key?: Maybe<Scalars['Float']>;
  icu?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  key?: Maybe<Scalars['Float']>;
  kingdom_key?: Maybe<Scalars['Float']>;
  name_key?: Maybe<Scalars['Float']>;
  nub_key?: Maybe<Scalars['Float']>;
  num_descendants?: Maybe<Scalars['Float']>;
  order_key?: Maybe<Scalars['Float']>;
  parent_key?: Maybe<Scalars['Float']>;
  phylum_key?: Maybe<Scalars['Float']>;
  source_taxon_key?: Maybe<Scalars['Float']>;
  species_key?: Maybe<Scalars['Float']>;
  vulnerability?: Maybe<Scalars['Float']>;
};

/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['timestamptz']>;
  _gt?: InputMaybe<Scalars['timestamptz']>;
  _gte?: InputMaybe<Scalars['timestamptz']>;
  _in?: InputMaybe<Array<Scalars['timestamptz']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['timestamptz']>;
  _lte?: InputMaybe<Scalars['timestamptz']>;
  _neq?: InputMaybe<Scalars['timestamptz']>;
  _nin?: InputMaybe<Array<Scalars['timestamptz']>>;
};

/** columns and relationships of "tree" */
export type Tree = {
  __typename?: 'tree';
  circumference?: Maybe<Scalars['numeric']>;
  creation_date: Scalars['timestamptz'];
  crown_diameter?: Maybe<Scalars['numeric']>;
  crown_diameter_is_estimated?: Maybe<Scalars['Boolean']>;
  data_entry_user_id: Scalars['String'];
  deletion_date?: Maybe<Scalars['timestamptz']>;
  /** An array relationship */
  diagnoses: Array<Diagnosis>;
  /** An aggregate relationship */
  diagnoses_aggregate: Diagnosis_Aggregate;
  diameter?: Maybe<Scalars['numeric']>;
  diameter_is_estimated?: Maybe<Scalars['Boolean']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  estimated_date?: Maybe<Scalars['Boolean']>;
  felling_date?: Maybe<Scalars['date']>;
  habit?: Maybe<Scalars['String']>;
  height?: Maybe<Scalars['numeric']>;
  height_is_estimated?: Maybe<Scalars['Boolean']>;
  id: Scalars['uuid'];
  infos?: Maybe<Scalars['jsonb']>;
  /** An array relationship */
  interventions: Array<Intervention>;
  /** An aggregate relationship */
  interventions_aggregate: Intervention_Aggregate;
  is_tree_of_interest?: Maybe<Scalars['Boolean']>;
  /** An object relationship */
  location: Location;
  location_id: Scalars['uuid'];
  note?: Maybe<Scalars['String']>;
  plantation_date?: Maybe<Scalars['String']>;
  scientific_name?: Maybe<Scalars['String']>;
  serial_number?: Maybe<Scalars['String']>;
  stage?: Maybe<Scalars['String']>;
  /** An object relationship */
  taxon?: Maybe<Taxon>;
  taxon_id?: Maybe<Scalars['Int']>;
  urbasense_subject_id?: Maybe<Scalars['String']>;
  variety?: Maybe<Scalars['String']>;
  vernacular_name?: Maybe<Scalars['String']>;
  watering?: Maybe<Scalars['Boolean']>;
};


/** columns and relationships of "tree" */
export type TreeDiagnosesArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Order_By>>;
  where?: InputMaybe<Diagnosis_Bool_Exp>;
};


/** columns and relationships of "tree" */
export type TreeDiagnoses_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Diagnosis_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Diagnosis_Order_By>>;
  where?: InputMaybe<Diagnosis_Bool_Exp>;
};


/** columns and relationships of "tree" */
export type TreeInfosArgs = {
  path?: InputMaybe<Scalars['String']>;
};


/** columns and relationships of "tree" */
export type TreeInterventionsArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Order_By>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};


/** columns and relationships of "tree" */
export type TreeInterventions_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Order_By>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};

/** aggregated selection of "tree" */
export type Tree_Aggregate = {
  __typename?: 'tree_aggregate';
  aggregate?: Maybe<Tree_Aggregate_Fields>;
  nodes: Array<Tree>;
};

export type Tree_Aggregate_Bool_Exp = {
  bool_and?: InputMaybe<Tree_Aggregate_Bool_Exp_Bool_And>;
  bool_or?: InputMaybe<Tree_Aggregate_Bool_Exp_Bool_Or>;
  count?: InputMaybe<Tree_Aggregate_Bool_Exp_Count>;
};

export type Tree_Aggregate_Bool_Exp_Bool_And = {
  arguments: Tree_Select_Column_Tree_Aggregate_Bool_Exp_Bool_And_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Tree_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Tree_Aggregate_Bool_Exp_Bool_Or = {
  arguments: Tree_Select_Column_Tree_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Tree_Bool_Exp>;
  predicate: Boolean_Comparison_Exp;
};

export type Tree_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Tree_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Tree_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "tree" */
export type Tree_Aggregate_Fields = {
  __typename?: 'tree_aggregate_fields';
  avg?: Maybe<Tree_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Tree_Max_Fields>;
  min?: Maybe<Tree_Min_Fields>;
  stddev?: Maybe<Tree_Stddev_Fields>;
  stddev_pop?: Maybe<Tree_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Tree_Stddev_Samp_Fields>;
  sum?: Maybe<Tree_Sum_Fields>;
  var_pop?: Maybe<Tree_Var_Pop_Fields>;
  var_samp?: Maybe<Tree_Var_Samp_Fields>;
  variance?: Maybe<Tree_Variance_Fields>;
};


/** aggregate fields of "tree" */
export type Tree_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Tree_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "tree" */
export type Tree_Aggregate_Order_By = {
  avg?: InputMaybe<Tree_Avg_Order_By>;
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Tree_Max_Order_By>;
  min?: InputMaybe<Tree_Min_Order_By>;
  stddev?: InputMaybe<Tree_Stddev_Order_By>;
  stddev_pop?: InputMaybe<Tree_Stddev_Pop_Order_By>;
  stddev_samp?: InputMaybe<Tree_Stddev_Samp_Order_By>;
  sum?: InputMaybe<Tree_Sum_Order_By>;
  var_pop?: InputMaybe<Tree_Var_Pop_Order_By>;
  var_samp?: InputMaybe<Tree_Var_Samp_Order_By>;
  variance?: InputMaybe<Tree_Variance_Order_By>;
};

/** append existing jsonb value of filtered columns with new jsonb value */
export type Tree_Append_Input = {
  infos?: InputMaybe<Scalars['jsonb']>;
};

/** input type for inserting array relation for remote table "tree" */
export type Tree_Arr_Rel_Insert_Input = {
  data: Array<Tree_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Tree_On_Conflict>;
};

/** aggregate avg on columns */
export type Tree_Avg_Fields = {
  __typename?: 'tree_avg_fields';
  circumference?: Maybe<Scalars['Float']>;
  crown_diameter?: Maybe<Scalars['Float']>;
  diameter?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  taxon_id?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "tree" */
export type Tree_Avg_Order_By = {
  circumference?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  diameter?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
};

/** Boolean expression to filter rows from the table "tree". All fields are combined with a logical 'AND'. */
export type Tree_Bool_Exp = {
  _and?: InputMaybe<Array<Tree_Bool_Exp>>;
  _not?: InputMaybe<Tree_Bool_Exp>;
  _or?: InputMaybe<Array<Tree_Bool_Exp>>;
  circumference?: InputMaybe<Numeric_Comparison_Exp>;
  creation_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  crown_diameter?: InputMaybe<Numeric_Comparison_Exp>;
  crown_diameter_is_estimated?: InputMaybe<Boolean_Comparison_Exp>;
  data_entry_user_id?: InputMaybe<String_Comparison_Exp>;
  deletion_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  diagnoses?: InputMaybe<Diagnosis_Bool_Exp>;
  diagnoses_aggregate?: InputMaybe<Diagnosis_Aggregate_Bool_Exp>;
  diameter?: InputMaybe<Numeric_Comparison_Exp>;
  diameter_is_estimated?: InputMaybe<Boolean_Comparison_Exp>;
  edition_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  estimated_date?: InputMaybe<Boolean_Comparison_Exp>;
  felling_date?: InputMaybe<Date_Comparison_Exp>;
  habit?: InputMaybe<String_Comparison_Exp>;
  height?: InputMaybe<Numeric_Comparison_Exp>;
  height_is_estimated?: InputMaybe<Boolean_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  infos?: InputMaybe<Jsonb_Comparison_Exp>;
  interventions?: InputMaybe<Intervention_Bool_Exp>;
  interventions_aggregate?: InputMaybe<Intervention_Aggregate_Bool_Exp>;
  is_tree_of_interest?: InputMaybe<Boolean_Comparison_Exp>;
  location?: InputMaybe<Location_Bool_Exp>;
  location_id?: InputMaybe<Uuid_Comparison_Exp>;
  note?: InputMaybe<String_Comparison_Exp>;
  plantation_date?: InputMaybe<String_Comparison_Exp>;
  scientific_name?: InputMaybe<String_Comparison_Exp>;
  serial_number?: InputMaybe<String_Comparison_Exp>;
  stage?: InputMaybe<String_Comparison_Exp>;
  taxon?: InputMaybe<Taxon_Bool_Exp>;
  taxon_id?: InputMaybe<Int_Comparison_Exp>;
  urbasense_subject_id?: InputMaybe<String_Comparison_Exp>;
  variety?: InputMaybe<String_Comparison_Exp>;
  vernacular_name?: InputMaybe<String_Comparison_Exp>;
  watering?: InputMaybe<Boolean_Comparison_Exp>;
};

/** unique or primary key constraints on table "tree" */
export enum Tree_Constraint {
  /** unique or primary key constraint on columns "id" */
  TreePkey = 'tree_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Tree_Delete_At_Path_Input = {
  infos?: InputMaybe<Array<Scalars['String']>>;
};

/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
export type Tree_Delete_Elem_Input = {
  infos?: InputMaybe<Scalars['Int']>;
};

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Tree_Delete_Key_Input = {
  infos?: InputMaybe<Scalars['String']>;
};

/** input type for incrementing numeric columns in table "tree" */
export type Tree_Inc_Input = {
  circumference?: InputMaybe<Scalars['numeric']>;
  crown_diameter?: InputMaybe<Scalars['numeric']>;
  diameter?: InputMaybe<Scalars['numeric']>;
  height?: InputMaybe<Scalars['numeric']>;
  taxon_id?: InputMaybe<Scalars['Int']>;
};

/** input type for inserting data into table "tree" */
export type Tree_Insert_Input = {
  circumference?: InputMaybe<Scalars['numeric']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  crown_diameter?: InputMaybe<Scalars['numeric']>;
  crown_diameter_is_estimated?: InputMaybe<Scalars['Boolean']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  deletion_date?: InputMaybe<Scalars['timestamptz']>;
  diagnoses?: InputMaybe<Diagnosis_Arr_Rel_Insert_Input>;
  diameter?: InputMaybe<Scalars['numeric']>;
  diameter_is_estimated?: InputMaybe<Scalars['Boolean']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  estimated_date?: InputMaybe<Scalars['Boolean']>;
  felling_date?: InputMaybe<Scalars['date']>;
  habit?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['numeric']>;
  height_is_estimated?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['uuid']>;
  infos?: InputMaybe<Scalars['jsonb']>;
  interventions?: InputMaybe<Intervention_Arr_Rel_Insert_Input>;
  is_tree_of_interest?: InputMaybe<Scalars['Boolean']>;
  location?: InputMaybe<Location_Obj_Rel_Insert_Input>;
  location_id?: InputMaybe<Scalars['uuid']>;
  note?: InputMaybe<Scalars['String']>;
  plantation_date?: InputMaybe<Scalars['String']>;
  scientific_name?: InputMaybe<Scalars['String']>;
  serial_number?: InputMaybe<Scalars['String']>;
  stage?: InputMaybe<Scalars['String']>;
  taxon?: InputMaybe<Taxon_Obj_Rel_Insert_Input>;
  taxon_id?: InputMaybe<Scalars['Int']>;
  urbasense_subject_id?: InputMaybe<Scalars['String']>;
  variety?: InputMaybe<Scalars['String']>;
  vernacular_name?: InputMaybe<Scalars['String']>;
  watering?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate max on columns */
export type Tree_Max_Fields = {
  __typename?: 'tree_max_fields';
  circumference?: Maybe<Scalars['numeric']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  crown_diameter?: Maybe<Scalars['numeric']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  deletion_date?: Maybe<Scalars['timestamptz']>;
  diameter?: Maybe<Scalars['numeric']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  felling_date?: Maybe<Scalars['date']>;
  habit?: Maybe<Scalars['String']>;
  height?: Maybe<Scalars['numeric']>;
  id?: Maybe<Scalars['uuid']>;
  location_id?: Maybe<Scalars['uuid']>;
  note?: Maybe<Scalars['String']>;
  plantation_date?: Maybe<Scalars['String']>;
  scientific_name?: Maybe<Scalars['String']>;
  serial_number?: Maybe<Scalars['String']>;
  stage?: Maybe<Scalars['String']>;
  taxon_id?: Maybe<Scalars['Int']>;
  urbasense_subject_id?: Maybe<Scalars['String']>;
  variety?: Maybe<Scalars['String']>;
  vernacular_name?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "tree" */
export type Tree_Max_Order_By = {
  circumference?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  deletion_date?: InputMaybe<Order_By>;
  diameter?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  felling_date?: InputMaybe<Order_By>;
  habit?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  plantation_date?: InputMaybe<Order_By>;
  scientific_name?: InputMaybe<Order_By>;
  serial_number?: InputMaybe<Order_By>;
  stage?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
  urbasense_subject_id?: InputMaybe<Order_By>;
  variety?: InputMaybe<Order_By>;
  vernacular_name?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Tree_Min_Fields = {
  __typename?: 'tree_min_fields';
  circumference?: Maybe<Scalars['numeric']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  crown_diameter?: Maybe<Scalars['numeric']>;
  data_entry_user_id?: Maybe<Scalars['String']>;
  deletion_date?: Maybe<Scalars['timestamptz']>;
  diameter?: Maybe<Scalars['numeric']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  felling_date?: Maybe<Scalars['date']>;
  habit?: Maybe<Scalars['String']>;
  height?: Maybe<Scalars['numeric']>;
  id?: Maybe<Scalars['uuid']>;
  location_id?: Maybe<Scalars['uuid']>;
  note?: Maybe<Scalars['String']>;
  plantation_date?: Maybe<Scalars['String']>;
  scientific_name?: Maybe<Scalars['String']>;
  serial_number?: Maybe<Scalars['String']>;
  stage?: Maybe<Scalars['String']>;
  taxon_id?: Maybe<Scalars['Int']>;
  urbasense_subject_id?: Maybe<Scalars['String']>;
  variety?: Maybe<Scalars['String']>;
  vernacular_name?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "tree" */
export type Tree_Min_Order_By = {
  circumference?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  deletion_date?: InputMaybe<Order_By>;
  diameter?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  felling_date?: InputMaybe<Order_By>;
  habit?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  plantation_date?: InputMaybe<Order_By>;
  scientific_name?: InputMaybe<Order_By>;
  serial_number?: InputMaybe<Order_By>;
  stage?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
  urbasense_subject_id?: InputMaybe<Order_By>;
  variety?: InputMaybe<Order_By>;
  vernacular_name?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "tree" */
export type Tree_Mutation_Response = {
  __typename?: 'tree_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Tree>;
};

/** input type for inserting object relation for remote table "tree" */
export type Tree_Obj_Rel_Insert_Input = {
  data: Tree_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Tree_On_Conflict>;
};

/** on_conflict condition type for table "tree" */
export type Tree_On_Conflict = {
  constraint: Tree_Constraint;
  update_columns?: Array<Tree_Update_Column>;
  where?: InputMaybe<Tree_Bool_Exp>;
};

/** Ordering options when selecting data from "tree". */
export type Tree_Order_By = {
  circumference?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  crown_diameter_is_estimated?: InputMaybe<Order_By>;
  data_entry_user_id?: InputMaybe<Order_By>;
  deletion_date?: InputMaybe<Order_By>;
  diagnoses_aggregate?: InputMaybe<Diagnosis_Aggregate_Order_By>;
  diameter?: InputMaybe<Order_By>;
  diameter_is_estimated?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  estimated_date?: InputMaybe<Order_By>;
  felling_date?: InputMaybe<Order_By>;
  habit?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  height_is_estimated?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  infos?: InputMaybe<Order_By>;
  interventions_aggregate?: InputMaybe<Intervention_Aggregate_Order_By>;
  is_tree_of_interest?: InputMaybe<Order_By>;
  location?: InputMaybe<Location_Order_By>;
  location_id?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  plantation_date?: InputMaybe<Order_By>;
  scientific_name?: InputMaybe<Order_By>;
  serial_number?: InputMaybe<Order_By>;
  stage?: InputMaybe<Order_By>;
  taxon?: InputMaybe<Taxon_Order_By>;
  taxon_id?: InputMaybe<Order_By>;
  urbasense_subject_id?: InputMaybe<Order_By>;
  variety?: InputMaybe<Order_By>;
  vernacular_name?: InputMaybe<Order_By>;
  watering?: InputMaybe<Order_By>;
};

/** primary key columns input for table: tree */
export type Tree_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Tree_Prepend_Input = {
  infos?: InputMaybe<Scalars['jsonb']>;
};

/** columns and relationships of "tree_scientific_names_count" */
export type Tree_Scientific_Names_Count = {
  __typename?: 'tree_scientific_names_count';
  count?: Maybe<Scalars['bigint']>;
  scientific_name?: Maybe<Scalars['String']>;
  /** An object relationship */
  tree?: Maybe<Tree>;
};

/** aggregated selection of "tree_scientific_names_count" */
export type Tree_Scientific_Names_Count_Aggregate = {
  __typename?: 'tree_scientific_names_count_aggregate';
  aggregate?: Maybe<Tree_Scientific_Names_Count_Aggregate_Fields>;
  nodes: Array<Tree_Scientific_Names_Count>;
};

/** aggregate fields of "tree_scientific_names_count" */
export type Tree_Scientific_Names_Count_Aggregate_Fields = {
  __typename?: 'tree_scientific_names_count_aggregate_fields';
  avg?: Maybe<Tree_Scientific_Names_Count_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Tree_Scientific_Names_Count_Max_Fields>;
  min?: Maybe<Tree_Scientific_Names_Count_Min_Fields>;
  stddev?: Maybe<Tree_Scientific_Names_Count_Stddev_Fields>;
  stddev_pop?: Maybe<Tree_Scientific_Names_Count_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Tree_Scientific_Names_Count_Stddev_Samp_Fields>;
  sum?: Maybe<Tree_Scientific_Names_Count_Sum_Fields>;
  var_pop?: Maybe<Tree_Scientific_Names_Count_Var_Pop_Fields>;
  var_samp?: Maybe<Tree_Scientific_Names_Count_Var_Samp_Fields>;
  variance?: Maybe<Tree_Scientific_Names_Count_Variance_Fields>;
};


/** aggregate fields of "tree_scientific_names_count" */
export type Tree_Scientific_Names_Count_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Tree_Scientific_Names_Count_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type Tree_Scientific_Names_Count_Avg_Fields = {
  __typename?: 'tree_scientific_names_count_avg_fields';
  count?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "tree_scientific_names_count". All fields are combined with a logical 'AND'. */
export type Tree_Scientific_Names_Count_Bool_Exp = {
  _and?: InputMaybe<Array<Tree_Scientific_Names_Count_Bool_Exp>>;
  _not?: InputMaybe<Tree_Scientific_Names_Count_Bool_Exp>;
  _or?: InputMaybe<Array<Tree_Scientific_Names_Count_Bool_Exp>>;
  count?: InputMaybe<Bigint_Comparison_Exp>;
  scientific_name?: InputMaybe<String_Comparison_Exp>;
  tree?: InputMaybe<Tree_Bool_Exp>;
};

/** aggregate max on columns */
export type Tree_Scientific_Names_Count_Max_Fields = {
  __typename?: 'tree_scientific_names_count_max_fields';
  count?: Maybe<Scalars['bigint']>;
  scientific_name?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Tree_Scientific_Names_Count_Min_Fields = {
  __typename?: 'tree_scientific_names_count_min_fields';
  count?: Maybe<Scalars['bigint']>;
  scientific_name?: Maybe<Scalars['String']>;
};

/** Ordering options when selecting data from "tree_scientific_names_count". */
export type Tree_Scientific_Names_Count_Order_By = {
  count?: InputMaybe<Order_By>;
  scientific_name?: InputMaybe<Order_By>;
  tree?: InputMaybe<Tree_Order_By>;
};

/** select columns of table "tree_scientific_names_count" */
export enum Tree_Scientific_Names_Count_Select_Column {
  /** column name */
  Count = 'count',
  /** column name */
  ScientificName = 'scientific_name'
}

/** aggregate stddev on columns */
export type Tree_Scientific_Names_Count_Stddev_Fields = {
  __typename?: 'tree_scientific_names_count_stddev_fields';
  count?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Tree_Scientific_Names_Count_Stddev_Pop_Fields = {
  __typename?: 'tree_scientific_names_count_stddev_pop_fields';
  count?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Tree_Scientific_Names_Count_Stddev_Samp_Fields = {
  __typename?: 'tree_scientific_names_count_stddev_samp_fields';
  count?: Maybe<Scalars['Float']>;
};

/** Streaming cursor of the table "tree_scientific_names_count" */
export type Tree_Scientific_Names_Count_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Tree_Scientific_Names_Count_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Tree_Scientific_Names_Count_Stream_Cursor_Value_Input = {
  count?: InputMaybe<Scalars['bigint']>;
  scientific_name?: InputMaybe<Scalars['String']>;
};

/** aggregate sum on columns */
export type Tree_Scientific_Names_Count_Sum_Fields = {
  __typename?: 'tree_scientific_names_count_sum_fields';
  count?: Maybe<Scalars['bigint']>;
};

/** aggregate var_pop on columns */
export type Tree_Scientific_Names_Count_Var_Pop_Fields = {
  __typename?: 'tree_scientific_names_count_var_pop_fields';
  count?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Tree_Scientific_Names_Count_Var_Samp_Fields = {
  __typename?: 'tree_scientific_names_count_var_samp_fields';
  count?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Tree_Scientific_Names_Count_Variance_Fields = {
  __typename?: 'tree_scientific_names_count_variance_fields';
  count?: Maybe<Scalars['Float']>;
};

/** select columns of table "tree" */
export enum Tree_Select_Column {
  /** column name */
  Circumference = 'circumference',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  CrownDiameter = 'crown_diameter',
  /** column name */
  CrownDiameterIsEstimated = 'crown_diameter_is_estimated',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  DeletionDate = 'deletion_date',
  /** column name */
  Diameter = 'diameter',
  /** column name */
  DiameterIsEstimated = 'diameter_is_estimated',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  EstimatedDate = 'estimated_date',
  /** column name */
  FellingDate = 'felling_date',
  /** column name */
  Habit = 'habit',
  /** column name */
  Height = 'height',
  /** column name */
  HeightIsEstimated = 'height_is_estimated',
  /** column name */
  Id = 'id',
  /** column name */
  Infos = 'infos',
  /** column name */
  IsTreeOfInterest = 'is_tree_of_interest',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Note = 'note',
  /** column name */
  PlantationDate = 'plantation_date',
  /** column name */
  ScientificName = 'scientific_name',
  /** column name */
  SerialNumber = 'serial_number',
  /** column name */
  Stage = 'stage',
  /** column name */
  TaxonId = 'taxon_id',
  /** column name */
  UrbasenseSubjectId = 'urbasense_subject_id',
  /** column name */
  Variety = 'variety',
  /** column name */
  VernacularName = 'vernacular_name',
  /** column name */
  Watering = 'watering'
}

/** select "tree_aggregate_bool_exp_bool_and_arguments_columns" columns of table "tree" */
export enum Tree_Select_Column_Tree_Aggregate_Bool_Exp_Bool_And_Arguments_Columns {
  /** column name */
  CrownDiameterIsEstimated = 'crown_diameter_is_estimated',
  /** column name */
  DiameterIsEstimated = 'diameter_is_estimated',
  /** column name */
  EstimatedDate = 'estimated_date',
  /** column name */
  HeightIsEstimated = 'height_is_estimated',
  /** column name */
  IsTreeOfInterest = 'is_tree_of_interest',
  /** column name */
  Watering = 'watering'
}

/** select "tree_aggregate_bool_exp_bool_or_arguments_columns" columns of table "tree" */
export enum Tree_Select_Column_Tree_Aggregate_Bool_Exp_Bool_Or_Arguments_Columns {
  /** column name */
  CrownDiameterIsEstimated = 'crown_diameter_is_estimated',
  /** column name */
  DiameterIsEstimated = 'diameter_is_estimated',
  /** column name */
  EstimatedDate = 'estimated_date',
  /** column name */
  HeightIsEstimated = 'height_is_estimated',
  /** column name */
  IsTreeOfInterest = 'is_tree_of_interest',
  /** column name */
  Watering = 'watering'
}

/** input type for updating data in table "tree" */
export type Tree_Set_Input = {
  circumference?: InputMaybe<Scalars['numeric']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  crown_diameter?: InputMaybe<Scalars['numeric']>;
  crown_diameter_is_estimated?: InputMaybe<Scalars['Boolean']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  deletion_date?: InputMaybe<Scalars['timestamptz']>;
  diameter?: InputMaybe<Scalars['numeric']>;
  diameter_is_estimated?: InputMaybe<Scalars['Boolean']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  estimated_date?: InputMaybe<Scalars['Boolean']>;
  felling_date?: InputMaybe<Scalars['date']>;
  habit?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['numeric']>;
  height_is_estimated?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['uuid']>;
  infos?: InputMaybe<Scalars['jsonb']>;
  is_tree_of_interest?: InputMaybe<Scalars['Boolean']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  note?: InputMaybe<Scalars['String']>;
  plantation_date?: InputMaybe<Scalars['String']>;
  scientific_name?: InputMaybe<Scalars['String']>;
  serial_number?: InputMaybe<Scalars['String']>;
  stage?: InputMaybe<Scalars['String']>;
  taxon_id?: InputMaybe<Scalars['Int']>;
  urbasense_subject_id?: InputMaybe<Scalars['String']>;
  variety?: InputMaybe<Scalars['String']>;
  vernacular_name?: InputMaybe<Scalars['String']>;
  watering?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate stddev on columns */
export type Tree_Stddev_Fields = {
  __typename?: 'tree_stddev_fields';
  circumference?: Maybe<Scalars['Float']>;
  crown_diameter?: Maybe<Scalars['Float']>;
  diameter?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  taxon_id?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "tree" */
export type Tree_Stddev_Order_By = {
  circumference?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  diameter?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Tree_Stddev_Pop_Fields = {
  __typename?: 'tree_stddev_pop_fields';
  circumference?: Maybe<Scalars['Float']>;
  crown_diameter?: Maybe<Scalars['Float']>;
  diameter?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  taxon_id?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "tree" */
export type Tree_Stddev_Pop_Order_By = {
  circumference?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  diameter?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Tree_Stddev_Samp_Fields = {
  __typename?: 'tree_stddev_samp_fields';
  circumference?: Maybe<Scalars['Float']>;
  crown_diameter?: Maybe<Scalars['Float']>;
  diameter?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  taxon_id?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "tree" */
export type Tree_Stddev_Samp_Order_By = {
  circumference?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  diameter?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
};

/** Streaming cursor of the table "tree" */
export type Tree_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Tree_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Tree_Stream_Cursor_Value_Input = {
  circumference?: InputMaybe<Scalars['numeric']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  crown_diameter?: InputMaybe<Scalars['numeric']>;
  crown_diameter_is_estimated?: InputMaybe<Scalars['Boolean']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  deletion_date?: InputMaybe<Scalars['timestamptz']>;
  diameter?: InputMaybe<Scalars['numeric']>;
  diameter_is_estimated?: InputMaybe<Scalars['Boolean']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  estimated_date?: InputMaybe<Scalars['Boolean']>;
  felling_date?: InputMaybe<Scalars['date']>;
  habit?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['numeric']>;
  height_is_estimated?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['uuid']>;
  infos?: InputMaybe<Scalars['jsonb']>;
  is_tree_of_interest?: InputMaybe<Scalars['Boolean']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  note?: InputMaybe<Scalars['String']>;
  plantation_date?: InputMaybe<Scalars['String']>;
  scientific_name?: InputMaybe<Scalars['String']>;
  serial_number?: InputMaybe<Scalars['String']>;
  stage?: InputMaybe<Scalars['String']>;
  taxon_id?: InputMaybe<Scalars['Int']>;
  urbasense_subject_id?: InputMaybe<Scalars['String']>;
  variety?: InputMaybe<Scalars['String']>;
  vernacular_name?: InputMaybe<Scalars['String']>;
  watering?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate sum on columns */
export type Tree_Sum_Fields = {
  __typename?: 'tree_sum_fields';
  circumference?: Maybe<Scalars['numeric']>;
  crown_diameter?: Maybe<Scalars['numeric']>;
  diameter?: Maybe<Scalars['numeric']>;
  height?: Maybe<Scalars['numeric']>;
  taxon_id?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "tree" */
export type Tree_Sum_Order_By = {
  circumference?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  diameter?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
};

/** update columns of table "tree" */
export enum Tree_Update_Column {
  /** column name */
  Circumference = 'circumference',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  CrownDiameter = 'crown_diameter',
  /** column name */
  CrownDiameterIsEstimated = 'crown_diameter_is_estimated',
  /** column name */
  DataEntryUserId = 'data_entry_user_id',
  /** column name */
  DeletionDate = 'deletion_date',
  /** column name */
  Diameter = 'diameter',
  /** column name */
  DiameterIsEstimated = 'diameter_is_estimated',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  EstimatedDate = 'estimated_date',
  /** column name */
  FellingDate = 'felling_date',
  /** column name */
  Habit = 'habit',
  /** column name */
  Height = 'height',
  /** column name */
  HeightIsEstimated = 'height_is_estimated',
  /** column name */
  Id = 'id',
  /** column name */
  Infos = 'infos',
  /** column name */
  IsTreeOfInterest = 'is_tree_of_interest',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Note = 'note',
  /** column name */
  PlantationDate = 'plantation_date',
  /** column name */
  ScientificName = 'scientific_name',
  /** column name */
  SerialNumber = 'serial_number',
  /** column name */
  Stage = 'stage',
  /** column name */
  TaxonId = 'taxon_id',
  /** column name */
  UrbasenseSubjectId = 'urbasense_subject_id',
  /** column name */
  Variety = 'variety',
  /** column name */
  VernacularName = 'vernacular_name',
  /** column name */
  Watering = 'watering'
}

export type Tree_Updates = {
  /** append existing jsonb value of filtered columns with new jsonb value */
  _append?: InputMaybe<Tree_Append_Input>;
  /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
  _delete_at_path?: InputMaybe<Tree_Delete_At_Path_Input>;
  /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
  _delete_elem?: InputMaybe<Tree_Delete_Elem_Input>;
  /** delete key/value pair or string element. key/value pairs are matched based on their key value */
  _delete_key?: InputMaybe<Tree_Delete_Key_Input>;
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Tree_Inc_Input>;
  /** prepend existing jsonb value of filtered columns with new jsonb value */
  _prepend?: InputMaybe<Tree_Prepend_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Tree_Set_Input>;
  /** filter the rows which have to be updated */
  where: Tree_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Tree_Var_Pop_Fields = {
  __typename?: 'tree_var_pop_fields';
  circumference?: Maybe<Scalars['Float']>;
  crown_diameter?: Maybe<Scalars['Float']>;
  diameter?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  taxon_id?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "tree" */
export type Tree_Var_Pop_Order_By = {
  circumference?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  diameter?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Tree_Var_Samp_Fields = {
  __typename?: 'tree_var_samp_fields';
  circumference?: Maybe<Scalars['Float']>;
  crown_diameter?: Maybe<Scalars['Float']>;
  diameter?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  taxon_id?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "tree" */
export type Tree_Var_Samp_Order_By = {
  circumference?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  diameter?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
};

/** aggregate variance on columns */
export type Tree_Variance_Fields = {
  __typename?: 'tree_variance_fields';
  circumference?: Maybe<Scalars['Float']>;
  crown_diameter?: Maybe<Scalars['Float']>;
  diameter?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  taxon_id?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "tree" */
export type Tree_Variance_Order_By = {
  circumference?: InputMaybe<Order_By>;
  crown_diameter?: InputMaybe<Order_By>;
  diameter?: InputMaybe<Order_By>;
  height?: InputMaybe<Order_By>;
  taxon_id?: InputMaybe<Order_By>;
};

/** List of all of possible urban constraints of a location */
export type Urban_Constraint = {
  __typename?: 'urban_constraint';
  id: Scalars['uuid'];
  slug: Scalars['String'];
};

/** aggregated selection of "urban_constraint" */
export type Urban_Constraint_Aggregate = {
  __typename?: 'urban_constraint_aggregate';
  aggregate?: Maybe<Urban_Constraint_Aggregate_Fields>;
  nodes: Array<Urban_Constraint>;
};

/** aggregate fields of "urban_constraint" */
export type Urban_Constraint_Aggregate_Fields = {
  __typename?: 'urban_constraint_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Urban_Constraint_Max_Fields>;
  min?: Maybe<Urban_Constraint_Min_Fields>;
};


/** aggregate fields of "urban_constraint" */
export type Urban_Constraint_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Urban_Constraint_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "urban_constraint". All fields are combined with a logical 'AND'. */
export type Urban_Constraint_Bool_Exp = {
  _and?: InputMaybe<Array<Urban_Constraint_Bool_Exp>>;
  _not?: InputMaybe<Urban_Constraint_Bool_Exp>;
  _or?: InputMaybe<Array<Urban_Constraint_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "urban_constraint" */
export enum Urban_Constraint_Constraint {
  /** unique or primary key constraint on columns "id" */
  UrbanConstraintPkey = 'urban_constraint_pkey'
}

/** input type for inserting data into table "urban_constraint" */
export type Urban_Constraint_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Urban_Constraint_Max_Fields = {
  __typename?: 'urban_constraint_max_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Urban_Constraint_Min_Fields = {
  __typename?: 'urban_constraint_min_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "urban_constraint" */
export type Urban_Constraint_Mutation_Response = {
  __typename?: 'urban_constraint_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Urban_Constraint>;
};

/** input type for inserting object relation for remote table "urban_constraint" */
export type Urban_Constraint_Obj_Rel_Insert_Input = {
  data: Urban_Constraint_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Urban_Constraint_On_Conflict>;
};

/** on_conflict condition type for table "urban_constraint" */
export type Urban_Constraint_On_Conflict = {
  constraint: Urban_Constraint_Constraint;
  update_columns?: Array<Urban_Constraint_Update_Column>;
  where?: InputMaybe<Urban_Constraint_Bool_Exp>;
};

/** Ordering options when selecting data from "urban_constraint". */
export type Urban_Constraint_Order_By = {
  id?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: urban_constraint */
export type Urban_Constraint_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "urban_constraint" */
export enum Urban_Constraint_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "urban_constraint" */
export type Urban_Constraint_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "urban_constraint" */
export type Urban_Constraint_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Urban_Constraint_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Urban_Constraint_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** update columns of table "urban_constraint" */
export enum Urban_Constraint_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

export type Urban_Constraint_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Urban_Constraint_Set_Input>;
  /** filter the rows which have to be updated */
  where: Urban_Constraint_Bool_Exp;
};

/** List of all of possible urban site type of a location */
export type Urban_Site = {
  __typename?: 'urban_site';
  id: Scalars['uuid'];
  slug: Scalars['String'];
};

/** aggregated selection of "urban_site" */
export type Urban_Site_Aggregate = {
  __typename?: 'urban_site_aggregate';
  aggregate?: Maybe<Urban_Site_Aggregate_Fields>;
  nodes: Array<Urban_Site>;
};

/** aggregate fields of "urban_site" */
export type Urban_Site_Aggregate_Fields = {
  __typename?: 'urban_site_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Urban_Site_Max_Fields>;
  min?: Maybe<Urban_Site_Min_Fields>;
};


/** aggregate fields of "urban_site" */
export type Urban_Site_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Urban_Site_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** Boolean expression to filter rows from the table "urban_site". All fields are combined with a logical 'AND'. */
export type Urban_Site_Bool_Exp = {
  _and?: InputMaybe<Array<Urban_Site_Bool_Exp>>;
  _not?: InputMaybe<Urban_Site_Bool_Exp>;
  _or?: InputMaybe<Array<Urban_Site_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  slug?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "urban_site" */
export enum Urban_Site_Constraint {
  /** unique or primary key constraint on columns "id" */
  UrbanSitePkey = 'urban_site_pkey'
}

/** input type for inserting data into table "urban_site" */
export type Urban_Site_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Urban_Site_Max_Fields = {
  __typename?: 'urban_site_max_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Urban_Site_Min_Fields = {
  __typename?: 'urban_site_min_fields';
  id?: Maybe<Scalars['uuid']>;
  slug?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "urban_site" */
export type Urban_Site_Mutation_Response = {
  __typename?: 'urban_site_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Urban_Site>;
};

/** input type for inserting object relation for remote table "urban_site" */
export type Urban_Site_Obj_Rel_Insert_Input = {
  data: Urban_Site_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Urban_Site_On_Conflict>;
};

/** on_conflict condition type for table "urban_site" */
export type Urban_Site_On_Conflict = {
  constraint: Urban_Site_Constraint;
  update_columns?: Array<Urban_Site_Update_Column>;
  where?: InputMaybe<Urban_Site_Bool_Exp>;
};

/** Ordering options when selecting data from "urban_site". */
export type Urban_Site_Order_By = {
  id?: InputMaybe<Order_By>;
  slug?: InputMaybe<Order_By>;
};

/** primary key columns input for table: urban_site */
export type Urban_Site_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "urban_site" */
export enum Urban_Site_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

/** input type for updating data in table "urban_site" */
export type Urban_Site_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** Streaming cursor of the table "urban_site" */
export type Urban_Site_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Urban_Site_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Urban_Site_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  slug?: InputMaybe<Scalars['String']>;
};

/** update columns of table "urban_site" */
export enum Urban_Site_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Slug = 'slug'
}

export type Urban_Site_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Urban_Site_Set_Input>;
  /** filter the rows which have to be updated */
  where: Urban_Site_Bool_Exp;
};

/** columns and relationships of "user_entity" */
export type User_Entity = {
  __typename?: 'user_entity';
  created_timestamp?: Maybe<Scalars['bigint']>;
  email?: Maybe<Scalars['String']>;
  email_constraint?: Maybe<Scalars['String']>;
  email_verified: Scalars['Boolean'];
  enabled: Scalars['Boolean'];
  federation_link?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  last_name?: Maybe<Scalars['String']>;
  not_before: Scalars['Int'];
  realm_id?: Maybe<Scalars['String']>;
  service_account_client_link?: Maybe<Scalars['String']>;
  username?: Maybe<Scalars['String']>;
};

/** aggregated selection of "user_entity" */
export type User_Entity_Aggregate = {
  __typename?: 'user_entity_aggregate';
  aggregate?: Maybe<User_Entity_Aggregate_Fields>;
  nodes: Array<User_Entity>;
};

/** aggregate fields of "user_entity" */
export type User_Entity_Aggregate_Fields = {
  __typename?: 'user_entity_aggregate_fields';
  avg?: Maybe<User_Entity_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<User_Entity_Max_Fields>;
  min?: Maybe<User_Entity_Min_Fields>;
  stddev?: Maybe<User_Entity_Stddev_Fields>;
  stddev_pop?: Maybe<User_Entity_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<User_Entity_Stddev_Samp_Fields>;
  sum?: Maybe<User_Entity_Sum_Fields>;
  var_pop?: Maybe<User_Entity_Var_Pop_Fields>;
  var_samp?: Maybe<User_Entity_Var_Samp_Fields>;
  variance?: Maybe<User_Entity_Variance_Fields>;
};


/** aggregate fields of "user_entity" */
export type User_Entity_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<User_Entity_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type User_Entity_Avg_Fields = {
  __typename?: 'user_entity_avg_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "user_entity". All fields are combined with a logical 'AND'. */
export type User_Entity_Bool_Exp = {
  _and?: InputMaybe<Array<User_Entity_Bool_Exp>>;
  _not?: InputMaybe<User_Entity_Bool_Exp>;
  _or?: InputMaybe<Array<User_Entity_Bool_Exp>>;
  created_timestamp?: InputMaybe<Bigint_Comparison_Exp>;
  email?: InputMaybe<String_Comparison_Exp>;
  email_constraint?: InputMaybe<String_Comparison_Exp>;
  email_verified?: InputMaybe<Boolean_Comparison_Exp>;
  enabled?: InputMaybe<Boolean_Comparison_Exp>;
  federation_link?: InputMaybe<String_Comparison_Exp>;
  first_name?: InputMaybe<String_Comparison_Exp>;
  id?: InputMaybe<String_Comparison_Exp>;
  last_name?: InputMaybe<String_Comparison_Exp>;
  not_before?: InputMaybe<Int_Comparison_Exp>;
  realm_id?: InputMaybe<String_Comparison_Exp>;
  service_account_client_link?: InputMaybe<String_Comparison_Exp>;
  username?: InputMaybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "user_entity" */
export enum User_Entity_Constraint {
  /** unique or primary key constraint on columns "id" */
  ConstraintFb = 'constraint_fb',
  /** unique or primary key constraint on columns "realm_id", "email_constraint" */
  UkDykn684sl8up1crfei6eckhd7 = 'uk_dykn684sl8up1crfei6eckhd7',
  /** unique or primary key constraint on columns "realm_id", "username" */
  UkRu8tt6t700s9v50bu18ws5ha6 = 'uk_ru8tt6t700s9v50bu18ws5ha6'
}

/** input type for incrementing numeric columns in table "user_entity" */
export type User_Entity_Inc_Input = {
  created_timestamp?: InputMaybe<Scalars['bigint']>;
  not_before?: InputMaybe<Scalars['Int']>;
};

/** input type for inserting data into table "user_entity" */
export type User_Entity_Insert_Input = {
  created_timestamp?: InputMaybe<Scalars['bigint']>;
  email?: InputMaybe<Scalars['String']>;
  email_constraint?: InputMaybe<Scalars['String']>;
  email_verified?: InputMaybe<Scalars['Boolean']>;
  enabled?: InputMaybe<Scalars['Boolean']>;
  federation_link?: InputMaybe<Scalars['String']>;
  first_name?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  last_name?: InputMaybe<Scalars['String']>;
  not_before?: InputMaybe<Scalars['Int']>;
  realm_id?: InputMaybe<Scalars['String']>;
  service_account_client_link?: InputMaybe<Scalars['String']>;
  username?: InputMaybe<Scalars['String']>;
};

/** aggregate max on columns */
export type User_Entity_Max_Fields = {
  __typename?: 'user_entity_max_fields';
  created_timestamp?: Maybe<Scalars['bigint']>;
  email?: Maybe<Scalars['String']>;
  email_constraint?: Maybe<Scalars['String']>;
  federation_link?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  last_name?: Maybe<Scalars['String']>;
  not_before?: Maybe<Scalars['Int']>;
  realm_id?: Maybe<Scalars['String']>;
  service_account_client_link?: Maybe<Scalars['String']>;
  username?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type User_Entity_Min_Fields = {
  __typename?: 'user_entity_min_fields';
  created_timestamp?: Maybe<Scalars['bigint']>;
  email?: Maybe<Scalars['String']>;
  email_constraint?: Maybe<Scalars['String']>;
  federation_link?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  last_name?: Maybe<Scalars['String']>;
  not_before?: Maybe<Scalars['Int']>;
  realm_id?: Maybe<Scalars['String']>;
  service_account_client_link?: Maybe<Scalars['String']>;
  username?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "user_entity" */
export type User_Entity_Mutation_Response = {
  __typename?: 'user_entity_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<User_Entity>;
};

/** input type for inserting object relation for remote table "user_entity" */
export type User_Entity_Obj_Rel_Insert_Input = {
  data: User_Entity_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<User_Entity_On_Conflict>;
};

/** on_conflict condition type for table "user_entity" */
export type User_Entity_On_Conflict = {
  constraint: User_Entity_Constraint;
  update_columns?: Array<User_Entity_Update_Column>;
  where?: InputMaybe<User_Entity_Bool_Exp>;
};

/** Ordering options when selecting data from "user_entity". */
export type User_Entity_Order_By = {
  created_timestamp?: InputMaybe<Order_By>;
  email?: InputMaybe<Order_By>;
  email_constraint?: InputMaybe<Order_By>;
  email_verified?: InputMaybe<Order_By>;
  enabled?: InputMaybe<Order_By>;
  federation_link?: InputMaybe<Order_By>;
  first_name?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  last_name?: InputMaybe<Order_By>;
  not_before?: InputMaybe<Order_By>;
  realm_id?: InputMaybe<Order_By>;
  service_account_client_link?: InputMaybe<Order_By>;
  username?: InputMaybe<Order_By>;
};

/** primary key columns input for table: user_entity */
export type User_Entity_Pk_Columns_Input = {
  id: Scalars['String'];
};

/** select columns of table "user_entity" */
export enum User_Entity_Select_Column {
  /** column name */
  CreatedTimestamp = 'created_timestamp',
  /** column name */
  Email = 'email',
  /** column name */
  EmailConstraint = 'email_constraint',
  /** column name */
  EmailVerified = 'email_verified',
  /** column name */
  Enabled = 'enabled',
  /** column name */
  FederationLink = 'federation_link',
  /** column name */
  FirstName = 'first_name',
  /** column name */
  Id = 'id',
  /** column name */
  LastName = 'last_name',
  /** column name */
  NotBefore = 'not_before',
  /** column name */
  RealmId = 'realm_id',
  /** column name */
  ServiceAccountClientLink = 'service_account_client_link',
  /** column name */
  Username = 'username'
}

/** input type for updating data in table "user_entity" */
export type User_Entity_Set_Input = {
  created_timestamp?: InputMaybe<Scalars['bigint']>;
  email?: InputMaybe<Scalars['String']>;
  email_constraint?: InputMaybe<Scalars['String']>;
  email_verified?: InputMaybe<Scalars['Boolean']>;
  enabled?: InputMaybe<Scalars['Boolean']>;
  federation_link?: InputMaybe<Scalars['String']>;
  first_name?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  last_name?: InputMaybe<Scalars['String']>;
  not_before?: InputMaybe<Scalars['Int']>;
  realm_id?: InputMaybe<Scalars['String']>;
  service_account_client_link?: InputMaybe<Scalars['String']>;
  username?: InputMaybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type User_Entity_Stddev_Fields = {
  __typename?: 'user_entity_stddev_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type User_Entity_Stddev_Pop_Fields = {
  __typename?: 'user_entity_stddev_pop_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type User_Entity_Stddev_Samp_Fields = {
  __typename?: 'user_entity_stddev_samp_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** Streaming cursor of the table "user_entity" */
export type User_Entity_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: User_Entity_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type User_Entity_Stream_Cursor_Value_Input = {
  created_timestamp?: InputMaybe<Scalars['bigint']>;
  email?: InputMaybe<Scalars['String']>;
  email_constraint?: InputMaybe<Scalars['String']>;
  email_verified?: InputMaybe<Scalars['Boolean']>;
  enabled?: InputMaybe<Scalars['Boolean']>;
  federation_link?: InputMaybe<Scalars['String']>;
  first_name?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  last_name?: InputMaybe<Scalars['String']>;
  not_before?: InputMaybe<Scalars['Int']>;
  realm_id?: InputMaybe<Scalars['String']>;
  service_account_client_link?: InputMaybe<Scalars['String']>;
  username?: InputMaybe<Scalars['String']>;
};

/** aggregate sum on columns */
export type User_Entity_Sum_Fields = {
  __typename?: 'user_entity_sum_fields';
  created_timestamp?: Maybe<Scalars['bigint']>;
  not_before?: Maybe<Scalars['Int']>;
};

/** update columns of table "user_entity" */
export enum User_Entity_Update_Column {
  /** column name */
  CreatedTimestamp = 'created_timestamp',
  /** column name */
  Email = 'email',
  /** column name */
  EmailConstraint = 'email_constraint',
  /** column name */
  EmailVerified = 'email_verified',
  /** column name */
  Enabled = 'enabled',
  /** column name */
  FederationLink = 'federation_link',
  /** column name */
  FirstName = 'first_name',
  /** column name */
  Id = 'id',
  /** column name */
  LastName = 'last_name',
  /** column name */
  NotBefore = 'not_before',
  /** column name */
  RealmId = 'realm_id',
  /** column name */
  ServiceAccountClientLink = 'service_account_client_link',
  /** column name */
  Username = 'username'
}

export type User_Entity_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<User_Entity_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<User_Entity_Set_Input>;
  /** filter the rows which have to be updated */
  where: User_Entity_Bool_Exp;
};

/** aggregate var_pop on columns */
export type User_Entity_Var_Pop_Fields = {
  __typename?: 'user_entity_var_pop_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type User_Entity_Var_Samp_Fields = {
  __typename?: 'user_entity_var_samp_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type User_Entity_Variance_Fields = {
  __typename?: 'user_entity_variance_fields';
  created_timestamp?: Maybe<Scalars['Float']>;
  not_before?: Maybe<Scalars['Float']>;
};

/** Boolean expression to compare columns of type "uuid". All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
  _eq?: InputMaybe<Scalars['uuid']>;
  _gt?: InputMaybe<Scalars['uuid']>;
  _gte?: InputMaybe<Scalars['uuid']>;
  _in?: InputMaybe<Array<Scalars['uuid']>>;
  _is_null?: InputMaybe<Scalars['Boolean']>;
  _lt?: InputMaybe<Scalars['uuid']>;
  _lte?: InputMaybe<Scalars['uuid']>;
  _neq?: InputMaybe<Scalars['uuid']>;
  _nin?: InputMaybe<Array<Scalars['uuid']>>;
};

/** List of all vegetated areas */
export type Vegetated_Area = {
  __typename?: 'vegetated_area';
  address?: Maybe<Scalars['String']>;
  afforestation_trees_data?: Maybe<Scalars['jsonb']>;
  /** An array relationship */
  boundaries_vegetated_areas: Array<Boundaries_Vegetated_Areas>;
  /** An aggregate relationship */
  boundaries_vegetated_areas_aggregate: Boundaries_Vegetated_Areas_Aggregate;
  coords: Scalars['geometry'];
  creation_date: Scalars['timestamptz'];
  edition_date?: Maybe<Scalars['timestamptz']>;
  has_differentiated_mowing: Scalars['Boolean'];
  hedge_type?: Maybe<Scalars['String']>;
  herbaceous_data?: Maybe<Scalars['jsonb']>;
  id: Scalars['uuid'];
  inconvenience_risk?: Maybe<Scalars['String']>;
  is_accessible: Scalars['Boolean'];
  landscape_type?: Maybe<Scalars['String']>;
  linear_meters?: Maybe<Scalars['numeric']>;
  note?: Maybe<Scalars['String']>;
  potential_area_state?: Maybe<Scalars['String']>;
  shrubs_data?: Maybe<Scalars['jsonb']>;
  surface: Scalars['numeric'];
  type: Scalars['String'];
  urban_site_id?: Maybe<Scalars['uuid']>;
  /** fetch data from the table: "vegetated_areas_locations" */
  vegetated_areas_locations: Array<Vegetated_Areas_Locations>;
  /** fetch aggregated fields from the table: "vegetated_areas_locations" */
  vegetated_areas_locations_aggregate: Vegetated_Areas_Locations_Aggregate;
  /** fetch data from the table: "vegetated_areas_residential_usage_types" */
  vegetated_areas_residential_usage_types: Array<Vegetated_Areas_Residential_Usage_Types>;
  /** fetch aggregated fields from the table: "vegetated_areas_residential_usage_types" */
  vegetated_areas_residential_usage_types_aggregate: Vegetated_Areas_Residential_Usage_Types_Aggregate;
  /** fetch data from the table: "vegetated_areas_urban_constraints" */
  vegetated_areas_urban_constraints: Array<Vegetated_Areas_Urban_Constraints>;
  /** fetch aggregated fields from the table: "vegetated_areas_urban_constraints" */
  vegetated_areas_urban_constraints_aggregate: Vegetated_Areas_Urban_Constraints_Aggregate;
};


/** List of all vegetated areas */
export type Vegetated_AreaAfforestation_Trees_DataArgs = {
  path?: InputMaybe<Scalars['String']>;
};


/** List of all vegetated areas */
export type Vegetated_AreaBoundaries_Vegetated_AreasArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Vegetated_Areas_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Vegetated_Areas_Order_By>>;
  where?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
};


/** List of all vegetated areas */
export type Vegetated_AreaBoundaries_Vegetated_Areas_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Boundaries_Vegetated_Areas_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Boundaries_Vegetated_Areas_Order_By>>;
  where?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
};


/** List of all vegetated areas */
export type Vegetated_AreaHerbaceous_DataArgs = {
  path?: InputMaybe<Scalars['String']>;
};


/** List of all vegetated areas */
export type Vegetated_AreaShrubs_DataArgs = {
  path?: InputMaybe<Scalars['String']>;
};


/** List of all vegetated areas */
export type Vegetated_AreaVegetated_Areas_LocationsArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Locations_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
};


/** List of all vegetated areas */
export type Vegetated_AreaVegetated_Areas_Locations_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Locations_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Locations_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
};


/** List of all vegetated areas */
export type Vegetated_AreaVegetated_Areas_Residential_Usage_TypesArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
};


/** List of all vegetated areas */
export type Vegetated_AreaVegetated_Areas_Residential_Usage_Types_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
};


/** List of all vegetated areas */
export type Vegetated_AreaVegetated_Areas_Urban_ConstraintsArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
};


/** List of all vegetated areas */
export type Vegetated_AreaVegetated_Areas_Urban_Constraints_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Order_By>>;
  where?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
};

/** aggregated selection of "vegetated_area" */
export type Vegetated_Area_Aggregate = {
  __typename?: 'vegetated_area_aggregate';
  aggregate?: Maybe<Vegetated_Area_Aggregate_Fields>;
  nodes: Array<Vegetated_Area>;
};

/** aggregate fields of "vegetated_area" */
export type Vegetated_Area_Aggregate_Fields = {
  __typename?: 'vegetated_area_aggregate_fields';
  avg?: Maybe<Vegetated_Area_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Vegetated_Area_Max_Fields>;
  min?: Maybe<Vegetated_Area_Min_Fields>;
  stddev?: Maybe<Vegetated_Area_Stddev_Fields>;
  stddev_pop?: Maybe<Vegetated_Area_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Vegetated_Area_Stddev_Samp_Fields>;
  sum?: Maybe<Vegetated_Area_Sum_Fields>;
  var_pop?: Maybe<Vegetated_Area_Var_Pop_Fields>;
  var_samp?: Maybe<Vegetated_Area_Var_Samp_Fields>;
  variance?: Maybe<Vegetated_Area_Variance_Fields>;
};


/** aggregate fields of "vegetated_area" */
export type Vegetated_Area_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Vegetated_Area_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** append existing jsonb value of filtered columns with new jsonb value */
export type Vegetated_Area_Append_Input = {
  afforestation_trees_data?: InputMaybe<Scalars['jsonb']>;
  herbaceous_data?: InputMaybe<Scalars['jsonb']>;
  shrubs_data?: InputMaybe<Scalars['jsonb']>;
};

/** aggregate avg on columns */
export type Vegetated_Area_Avg_Fields = {
  __typename?: 'vegetated_area_avg_fields';
  linear_meters?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "vegetated_area". All fields are combined with a logical 'AND'. */
export type Vegetated_Area_Bool_Exp = {
  _and?: InputMaybe<Array<Vegetated_Area_Bool_Exp>>;
  _not?: InputMaybe<Vegetated_Area_Bool_Exp>;
  _or?: InputMaybe<Array<Vegetated_Area_Bool_Exp>>;
  address?: InputMaybe<String_Comparison_Exp>;
  afforestation_trees_data?: InputMaybe<Jsonb_Comparison_Exp>;
  boundaries_vegetated_areas?: InputMaybe<Boundaries_Vegetated_Areas_Bool_Exp>;
  boundaries_vegetated_areas_aggregate?: InputMaybe<Boundaries_Vegetated_Areas_Aggregate_Bool_Exp>;
  coords?: InputMaybe<Geometry_Comparison_Exp>;
  creation_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  edition_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  has_differentiated_mowing?: InputMaybe<Boolean_Comparison_Exp>;
  hedge_type?: InputMaybe<String_Comparison_Exp>;
  herbaceous_data?: InputMaybe<Jsonb_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  inconvenience_risk?: InputMaybe<String_Comparison_Exp>;
  is_accessible?: InputMaybe<Boolean_Comparison_Exp>;
  landscape_type?: InputMaybe<String_Comparison_Exp>;
  linear_meters?: InputMaybe<Numeric_Comparison_Exp>;
  note?: InputMaybe<String_Comparison_Exp>;
  potential_area_state?: InputMaybe<String_Comparison_Exp>;
  shrubs_data?: InputMaybe<Jsonb_Comparison_Exp>;
  surface?: InputMaybe<Numeric_Comparison_Exp>;
  type?: InputMaybe<String_Comparison_Exp>;
  urban_site_id?: InputMaybe<Uuid_Comparison_Exp>;
  vegetated_areas_locations?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
  vegetated_areas_locations_aggregate?: InputMaybe<Vegetated_Areas_Locations_Aggregate_Bool_Exp>;
  vegetated_areas_residential_usage_types?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
  vegetated_areas_residential_usage_types_aggregate?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Aggregate_Bool_Exp>;
  vegetated_areas_urban_constraints?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
  vegetated_areas_urban_constraints_aggregate?: InputMaybe<Vegetated_Areas_Urban_Constraints_Aggregate_Bool_Exp>;
};

/** unique or primary key constraints on table "vegetated_area" */
export enum Vegetated_Area_Constraint {
  /** unique or primary key constraint on columns "id" */
  VegetatedAreaPkey = 'vegetated_area_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Vegetated_Area_Delete_At_Path_Input = {
  afforestation_trees_data?: InputMaybe<Array<Scalars['String']>>;
  herbaceous_data?: InputMaybe<Array<Scalars['String']>>;
  shrubs_data?: InputMaybe<Array<Scalars['String']>>;
};

/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
export type Vegetated_Area_Delete_Elem_Input = {
  afforestation_trees_data?: InputMaybe<Scalars['Int']>;
  herbaceous_data?: InputMaybe<Scalars['Int']>;
  shrubs_data?: InputMaybe<Scalars['Int']>;
};

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Vegetated_Area_Delete_Key_Input = {
  afforestation_trees_data?: InputMaybe<Scalars['String']>;
  herbaceous_data?: InputMaybe<Scalars['String']>;
  shrubs_data?: InputMaybe<Scalars['String']>;
};

/** input type for incrementing numeric columns in table "vegetated_area" */
export type Vegetated_Area_Inc_Input = {
  linear_meters?: InputMaybe<Scalars['numeric']>;
  surface?: InputMaybe<Scalars['numeric']>;
};

/** input type for inserting data into table "vegetated_area" */
export type Vegetated_Area_Insert_Input = {
  address?: InputMaybe<Scalars['String']>;
  afforestation_trees_data?: InputMaybe<Scalars['jsonb']>;
  boundaries_vegetated_areas?: InputMaybe<Boundaries_Vegetated_Areas_Arr_Rel_Insert_Input>;
  coords?: InputMaybe<Scalars['geometry']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  has_differentiated_mowing?: InputMaybe<Scalars['Boolean']>;
  hedge_type?: InputMaybe<Scalars['String']>;
  herbaceous_data?: InputMaybe<Scalars['jsonb']>;
  id?: InputMaybe<Scalars['uuid']>;
  inconvenience_risk?: InputMaybe<Scalars['String']>;
  is_accessible?: InputMaybe<Scalars['Boolean']>;
  landscape_type?: InputMaybe<Scalars['String']>;
  linear_meters?: InputMaybe<Scalars['numeric']>;
  note?: InputMaybe<Scalars['String']>;
  potential_area_state?: InputMaybe<Scalars['String']>;
  shrubs_data?: InputMaybe<Scalars['jsonb']>;
  surface?: InputMaybe<Scalars['numeric']>;
  type?: InputMaybe<Scalars['String']>;
  urban_site_id?: InputMaybe<Scalars['uuid']>;
  vegetated_areas_locations?: InputMaybe<Vegetated_Areas_Locations_Arr_Rel_Insert_Input>;
  vegetated_areas_residential_usage_types?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Arr_Rel_Insert_Input>;
  vegetated_areas_urban_constraints?: InputMaybe<Vegetated_Areas_Urban_Constraints_Arr_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type Vegetated_Area_Max_Fields = {
  __typename?: 'vegetated_area_max_fields';
  address?: Maybe<Scalars['String']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  hedge_type?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  inconvenience_risk?: Maybe<Scalars['String']>;
  landscape_type?: Maybe<Scalars['String']>;
  linear_meters?: Maybe<Scalars['numeric']>;
  note?: Maybe<Scalars['String']>;
  potential_area_state?: Maybe<Scalars['String']>;
  surface?: Maybe<Scalars['numeric']>;
  type?: Maybe<Scalars['String']>;
  urban_site_id?: Maybe<Scalars['uuid']>;
};

/** aggregate min on columns */
export type Vegetated_Area_Min_Fields = {
  __typename?: 'vegetated_area_min_fields';
  address?: Maybe<Scalars['String']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  hedge_type?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  inconvenience_risk?: Maybe<Scalars['String']>;
  landscape_type?: Maybe<Scalars['String']>;
  linear_meters?: Maybe<Scalars['numeric']>;
  note?: Maybe<Scalars['String']>;
  potential_area_state?: Maybe<Scalars['String']>;
  surface?: Maybe<Scalars['numeric']>;
  type?: Maybe<Scalars['String']>;
  urban_site_id?: Maybe<Scalars['uuid']>;
};

/** response of any mutation on the table "vegetated_area" */
export type Vegetated_Area_Mutation_Response = {
  __typename?: 'vegetated_area_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Vegetated_Area>;
};

/** input type for inserting object relation for remote table "vegetated_area" */
export type Vegetated_Area_Obj_Rel_Insert_Input = {
  data: Vegetated_Area_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Vegetated_Area_On_Conflict>;
};

/** on_conflict condition type for table "vegetated_area" */
export type Vegetated_Area_On_Conflict = {
  constraint: Vegetated_Area_Constraint;
  update_columns?: Array<Vegetated_Area_Update_Column>;
  where?: InputMaybe<Vegetated_Area_Bool_Exp>;
};

/** Ordering options when selecting data from "vegetated_area". */
export type Vegetated_Area_Order_By = {
  address?: InputMaybe<Order_By>;
  afforestation_trees_data?: InputMaybe<Order_By>;
  boundaries_vegetated_areas_aggregate?: InputMaybe<Boundaries_Vegetated_Areas_Aggregate_Order_By>;
  coords?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  has_differentiated_mowing?: InputMaybe<Order_By>;
  hedge_type?: InputMaybe<Order_By>;
  herbaceous_data?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  inconvenience_risk?: InputMaybe<Order_By>;
  is_accessible?: InputMaybe<Order_By>;
  landscape_type?: InputMaybe<Order_By>;
  linear_meters?: InputMaybe<Order_By>;
  note?: InputMaybe<Order_By>;
  potential_area_state?: InputMaybe<Order_By>;
  shrubs_data?: InputMaybe<Order_By>;
  surface?: InputMaybe<Order_By>;
  type?: InputMaybe<Order_By>;
  urban_site_id?: InputMaybe<Order_By>;
  vegetated_areas_locations_aggregate?: InputMaybe<Vegetated_Areas_Locations_Aggregate_Order_By>;
  vegetated_areas_residential_usage_types_aggregate?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Aggregate_Order_By>;
  vegetated_areas_urban_constraints_aggregate?: InputMaybe<Vegetated_Areas_Urban_Constraints_Aggregate_Order_By>;
};

/** primary key columns input for table: vegetated_area */
export type Vegetated_Area_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Vegetated_Area_Prepend_Input = {
  afforestation_trees_data?: InputMaybe<Scalars['jsonb']>;
  herbaceous_data?: InputMaybe<Scalars['jsonb']>;
  shrubs_data?: InputMaybe<Scalars['jsonb']>;
};

/** select columns of table "vegetated_area" */
export enum Vegetated_Area_Select_Column {
  /** column name */
  Address = 'address',
  /** column name */
  AfforestationTreesData = 'afforestation_trees_data',
  /** column name */
  Coords = 'coords',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  HasDifferentiatedMowing = 'has_differentiated_mowing',
  /** column name */
  HedgeType = 'hedge_type',
  /** column name */
  HerbaceousData = 'herbaceous_data',
  /** column name */
  Id = 'id',
  /** column name */
  InconvenienceRisk = 'inconvenience_risk',
  /** column name */
  IsAccessible = 'is_accessible',
  /** column name */
  LandscapeType = 'landscape_type',
  /** column name */
  LinearMeters = 'linear_meters',
  /** column name */
  Note = 'note',
  /** column name */
  PotentialAreaState = 'potential_area_state',
  /** column name */
  ShrubsData = 'shrubs_data',
  /** column name */
  Surface = 'surface',
  /** column name */
  Type = 'type',
  /** column name */
  UrbanSiteId = 'urban_site_id'
}

/** input type for updating data in table "vegetated_area" */
export type Vegetated_Area_Set_Input = {
  address?: InputMaybe<Scalars['String']>;
  afforestation_trees_data?: InputMaybe<Scalars['jsonb']>;
  coords?: InputMaybe<Scalars['geometry']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  has_differentiated_mowing?: InputMaybe<Scalars['Boolean']>;
  hedge_type?: InputMaybe<Scalars['String']>;
  herbaceous_data?: InputMaybe<Scalars['jsonb']>;
  id?: InputMaybe<Scalars['uuid']>;
  inconvenience_risk?: InputMaybe<Scalars['String']>;
  is_accessible?: InputMaybe<Scalars['Boolean']>;
  landscape_type?: InputMaybe<Scalars['String']>;
  linear_meters?: InputMaybe<Scalars['numeric']>;
  note?: InputMaybe<Scalars['String']>;
  potential_area_state?: InputMaybe<Scalars['String']>;
  shrubs_data?: InputMaybe<Scalars['jsonb']>;
  surface?: InputMaybe<Scalars['numeric']>;
  type?: InputMaybe<Scalars['String']>;
  urban_site_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type Vegetated_Area_Stddev_Fields = {
  __typename?: 'vegetated_area_stddev_fields';
  linear_meters?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Vegetated_Area_Stddev_Pop_Fields = {
  __typename?: 'vegetated_area_stddev_pop_fields';
  linear_meters?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Vegetated_Area_Stddev_Samp_Fields = {
  __typename?: 'vegetated_area_stddev_samp_fields';
  linear_meters?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** Streaming cursor of the table "vegetated_area" */
export type Vegetated_Area_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Vegetated_Area_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Vegetated_Area_Stream_Cursor_Value_Input = {
  address?: InputMaybe<Scalars['String']>;
  afforestation_trees_data?: InputMaybe<Scalars['jsonb']>;
  coords?: InputMaybe<Scalars['geometry']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  has_differentiated_mowing?: InputMaybe<Scalars['Boolean']>;
  hedge_type?: InputMaybe<Scalars['String']>;
  herbaceous_data?: InputMaybe<Scalars['jsonb']>;
  id?: InputMaybe<Scalars['uuid']>;
  inconvenience_risk?: InputMaybe<Scalars['String']>;
  is_accessible?: InputMaybe<Scalars['Boolean']>;
  landscape_type?: InputMaybe<Scalars['String']>;
  linear_meters?: InputMaybe<Scalars['numeric']>;
  note?: InputMaybe<Scalars['String']>;
  potential_area_state?: InputMaybe<Scalars['String']>;
  shrubs_data?: InputMaybe<Scalars['jsonb']>;
  surface?: InputMaybe<Scalars['numeric']>;
  type?: InputMaybe<Scalars['String']>;
  urban_site_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate sum on columns */
export type Vegetated_Area_Sum_Fields = {
  __typename?: 'vegetated_area_sum_fields';
  linear_meters?: Maybe<Scalars['numeric']>;
  surface?: Maybe<Scalars['numeric']>;
};

/** update columns of table "vegetated_area" */
export enum Vegetated_Area_Update_Column {
  /** column name */
  Address = 'address',
  /** column name */
  AfforestationTreesData = 'afforestation_trees_data',
  /** column name */
  Coords = 'coords',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  HasDifferentiatedMowing = 'has_differentiated_mowing',
  /** column name */
  HedgeType = 'hedge_type',
  /** column name */
  HerbaceousData = 'herbaceous_data',
  /** column name */
  Id = 'id',
  /** column name */
  InconvenienceRisk = 'inconvenience_risk',
  /** column name */
  IsAccessible = 'is_accessible',
  /** column name */
  LandscapeType = 'landscape_type',
  /** column name */
  LinearMeters = 'linear_meters',
  /** column name */
  Note = 'note',
  /** column name */
  PotentialAreaState = 'potential_area_state',
  /** column name */
  ShrubsData = 'shrubs_data',
  /** column name */
  Surface = 'surface',
  /** column name */
  Type = 'type',
  /** column name */
  UrbanSiteId = 'urban_site_id'
}

export type Vegetated_Area_Updates = {
  /** append existing jsonb value of filtered columns with new jsonb value */
  _append?: InputMaybe<Vegetated_Area_Append_Input>;
  /** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
  _delete_at_path?: InputMaybe<Vegetated_Area_Delete_At_Path_Input>;
  /** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
  _delete_elem?: InputMaybe<Vegetated_Area_Delete_Elem_Input>;
  /** delete key/value pair or string element. key/value pairs are matched based on their key value */
  _delete_key?: InputMaybe<Vegetated_Area_Delete_Key_Input>;
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Vegetated_Area_Inc_Input>;
  /** prepend existing jsonb value of filtered columns with new jsonb value */
  _prepend?: InputMaybe<Vegetated_Area_Prepend_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Vegetated_Area_Set_Input>;
  /** filter the rows which have to be updated */
  where: Vegetated_Area_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Vegetated_Area_Var_Pop_Fields = {
  __typename?: 'vegetated_area_var_pop_fields';
  linear_meters?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Vegetated_Area_Var_Samp_Fields = {
  __typename?: 'vegetated_area_var_samp_fields';
  linear_meters?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Vegetated_Area_Variance_Fields = {
  __typename?: 'vegetated_area_variance_fields';
  linear_meters?: Maybe<Scalars['Float']>;
  surface?: Maybe<Scalars['Float']>;
};

/** Relation table between VegetatedAreas and Locations */
export type Vegetated_Areas_Locations = {
  __typename?: 'vegetated_areas_locations';
  id: Scalars['uuid'];
  /** An object relationship */
  location: Location;
  location_id: Scalars['uuid'];
  vegetated_area_id: Scalars['uuid'];
};

/** aggregated selection of "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Aggregate = {
  __typename?: 'vegetated_areas_locations_aggregate';
  aggregate?: Maybe<Vegetated_Areas_Locations_Aggregate_Fields>;
  nodes: Array<Vegetated_Areas_Locations>;
};

export type Vegetated_Areas_Locations_Aggregate_Bool_Exp = {
  count?: InputMaybe<Vegetated_Areas_Locations_Aggregate_Bool_Exp_Count>;
};

export type Vegetated_Areas_Locations_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Vegetated_Areas_Locations_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Aggregate_Fields = {
  __typename?: 'vegetated_areas_locations_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Vegetated_Areas_Locations_Max_Fields>;
  min?: Maybe<Vegetated_Areas_Locations_Min_Fields>;
};


/** aggregate fields of "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Vegetated_Areas_Locations_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Vegetated_Areas_Locations_Max_Order_By>;
  min?: InputMaybe<Vegetated_Areas_Locations_Min_Order_By>;
};

/** input type for inserting array relation for remote table "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Arr_Rel_Insert_Input = {
  data: Array<Vegetated_Areas_Locations_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Vegetated_Areas_Locations_On_Conflict>;
};

/** Boolean expression to filter rows from the table "vegetated_areas_locations". All fields are combined with a logical 'AND'. */
export type Vegetated_Areas_Locations_Bool_Exp = {
  _and?: InputMaybe<Array<Vegetated_Areas_Locations_Bool_Exp>>;
  _not?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
  _or?: InputMaybe<Array<Vegetated_Areas_Locations_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  location?: InputMaybe<Location_Bool_Exp>;
  location_id?: InputMaybe<Uuid_Comparison_Exp>;
  vegetated_area_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "vegetated_areas_locations" */
export enum Vegetated_Areas_Locations_Constraint {
  /** unique or primary key constraint on columns "id" */
  VegetatedAreasLocationsPkey = 'vegetated_areas_locations_pkey'
}

/** input type for inserting data into table "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  location?: InputMaybe<Location_Obj_Rel_Insert_Input>;
  location_id?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Vegetated_Areas_Locations_Max_Fields = {
  __typename?: 'vegetated_areas_locations_max_fields';
  id?: Maybe<Scalars['uuid']>;
  location_id?: Maybe<Scalars['uuid']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Max_Order_By = {
  id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Vegetated_Areas_Locations_Min_Fields = {
  __typename?: 'vegetated_areas_locations_min_fields';
  id?: Maybe<Scalars['uuid']>;
  location_id?: Maybe<Scalars['uuid']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Min_Order_By = {
  id?: InputMaybe<Order_By>;
  location_id?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Mutation_Response = {
  __typename?: 'vegetated_areas_locations_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Vegetated_Areas_Locations>;
};

/** on_conflict condition type for table "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_On_Conflict = {
  constraint: Vegetated_Areas_Locations_Constraint;
  update_columns?: Array<Vegetated_Areas_Locations_Update_Column>;
  where?: InputMaybe<Vegetated_Areas_Locations_Bool_Exp>;
};

/** Ordering options when selecting data from "vegetated_areas_locations". */
export type Vegetated_Areas_Locations_Order_By = {
  id?: InputMaybe<Order_By>;
  location?: InputMaybe<Location_Order_By>;
  location_id?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: vegetated_areas_locations */
export type Vegetated_Areas_Locations_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "vegetated_areas_locations" */
export enum Vegetated_Areas_Locations_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  VegetatedAreaId = 'vegetated_area_id'
}

/** input type for updating data in table "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** Streaming cursor of the table "vegetated_areas_locations" */
export type Vegetated_Areas_Locations_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Vegetated_Areas_Locations_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Vegetated_Areas_Locations_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "vegetated_areas_locations" */
export enum Vegetated_Areas_Locations_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  VegetatedAreaId = 'vegetated_area_id'
}

export type Vegetated_Areas_Locations_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Vegetated_Areas_Locations_Set_Input>;
  /** filter the rows which have to be updated */
  where: Vegetated_Areas_Locations_Bool_Exp;
};

/** columns and relationships of "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types = {
  __typename?: 'vegetated_areas_residential_usage_types';
  id: Scalars['uuid'];
  residential_usage_type: Scalars['uuid'];
  /** An object relationship */
  residential_usage_types: Residential_Usage_Type;
  vegetated_area_id: Scalars['uuid'];
};

/** aggregated selection of "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Aggregate = {
  __typename?: 'vegetated_areas_residential_usage_types_aggregate';
  aggregate?: Maybe<Vegetated_Areas_Residential_Usage_Types_Aggregate_Fields>;
  nodes: Array<Vegetated_Areas_Residential_Usage_Types>;
};

export type Vegetated_Areas_Residential_Usage_Types_Aggregate_Bool_Exp = {
  count?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Aggregate_Bool_Exp_Count>;
};

export type Vegetated_Areas_Residential_Usage_Types_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Aggregate_Fields = {
  __typename?: 'vegetated_areas_residential_usage_types_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Vegetated_Areas_Residential_Usage_Types_Max_Fields>;
  min?: Maybe<Vegetated_Areas_Residential_Usage_Types_Min_Fields>;
};


/** aggregate fields of "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Max_Order_By>;
  min?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Min_Order_By>;
};

/** input type for inserting array relation for remote table "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Arr_Rel_Insert_Input = {
  data: Array<Vegetated_Areas_Residential_Usage_Types_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_On_Conflict>;
};

/** Boolean expression to filter rows from the table "vegetated_areas_residential_usage_types". All fields are combined with a logical 'AND'. */
export type Vegetated_Areas_Residential_Usage_Types_Bool_Exp = {
  _and?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>>;
  _not?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
  _or?: InputMaybe<Array<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  residential_usage_type?: InputMaybe<Uuid_Comparison_Exp>;
  residential_usage_types?: InputMaybe<Residential_Usage_Type_Bool_Exp>;
  vegetated_area_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "vegetated_areas_residential_usage_types" */
export enum Vegetated_Areas_Residential_Usage_Types_Constraint {
  /** unique or primary key constraint on columns "id" */
  VegetatedAreasResidentialUsageTypesPkey = 'vegetated_areas_residential_usage_types_pkey'
}

/** input type for inserting data into table "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  residential_usage_type?: InputMaybe<Scalars['uuid']>;
  residential_usage_types?: InputMaybe<Residential_Usage_Type_Obj_Rel_Insert_Input>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Vegetated_Areas_Residential_Usage_Types_Max_Fields = {
  __typename?: 'vegetated_areas_residential_usage_types_max_fields';
  id?: Maybe<Scalars['uuid']>;
  residential_usage_type?: Maybe<Scalars['uuid']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Max_Order_By = {
  id?: InputMaybe<Order_By>;
  residential_usage_type?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Vegetated_Areas_Residential_Usage_Types_Min_Fields = {
  __typename?: 'vegetated_areas_residential_usage_types_min_fields';
  id?: Maybe<Scalars['uuid']>;
  residential_usage_type?: Maybe<Scalars['uuid']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Min_Order_By = {
  id?: InputMaybe<Order_By>;
  residential_usage_type?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Mutation_Response = {
  __typename?: 'vegetated_areas_residential_usage_types_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Vegetated_Areas_Residential_Usage_Types>;
};

/** on_conflict condition type for table "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_On_Conflict = {
  constraint: Vegetated_Areas_Residential_Usage_Types_Constraint;
  update_columns?: Array<Vegetated_Areas_Residential_Usage_Types_Update_Column>;
  where?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Bool_Exp>;
};

/** Ordering options when selecting data from "vegetated_areas_residential_usage_types". */
export type Vegetated_Areas_Residential_Usage_Types_Order_By = {
  id?: InputMaybe<Order_By>;
  residential_usage_type?: InputMaybe<Order_By>;
  residential_usage_types?: InputMaybe<Residential_Usage_Type_Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: vegetated_areas_residential_usage_types */
export type Vegetated_Areas_Residential_Usage_Types_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "vegetated_areas_residential_usage_types" */
export enum Vegetated_Areas_Residential_Usage_Types_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  ResidentialUsageType = 'residential_usage_type',
  /** column name */
  VegetatedAreaId = 'vegetated_area_id'
}

/** input type for updating data in table "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  residential_usage_type?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** Streaming cursor of the table "vegetated_areas_residential_usage_types" */
export type Vegetated_Areas_Residential_Usage_Types_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Vegetated_Areas_Residential_Usage_Types_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Vegetated_Areas_Residential_Usage_Types_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  residential_usage_type?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "vegetated_areas_residential_usage_types" */
export enum Vegetated_Areas_Residential_Usage_Types_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  ResidentialUsageType = 'residential_usage_type',
  /** column name */
  VegetatedAreaId = 'vegetated_area_id'
}

export type Vegetated_Areas_Residential_Usage_Types_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Vegetated_Areas_Residential_Usage_Types_Set_Input>;
  /** filter the rows which have to be updated */
  where: Vegetated_Areas_Residential_Usage_Types_Bool_Exp;
};

/** columns and relationships of "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints = {
  __typename?: 'vegetated_areas_urban_constraints';
  id: Scalars['uuid'];
  /** An object relationship */
  urban_constraint: Urban_Constraint;
  urban_constraint_id: Scalars['uuid'];
  vegetated_area_id: Scalars['uuid'];
};

/** aggregated selection of "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Aggregate = {
  __typename?: 'vegetated_areas_urban_constraints_aggregate';
  aggregate?: Maybe<Vegetated_Areas_Urban_Constraints_Aggregate_Fields>;
  nodes: Array<Vegetated_Areas_Urban_Constraints>;
};

export type Vegetated_Areas_Urban_Constraints_Aggregate_Bool_Exp = {
  count?: InputMaybe<Vegetated_Areas_Urban_Constraints_Aggregate_Bool_Exp_Count>;
};

export type Vegetated_Areas_Urban_Constraints_Aggregate_Bool_Exp_Count = {
  arguments?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
  filter?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
  predicate: Int_Comparison_Exp;
};

/** aggregate fields of "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Aggregate_Fields = {
  __typename?: 'vegetated_areas_urban_constraints_aggregate_fields';
  count: Scalars['Int'];
  max?: Maybe<Vegetated_Areas_Urban_Constraints_Max_Fields>;
  min?: Maybe<Vegetated_Areas_Urban_Constraints_Min_Fields>;
};


/** aggregate fields of "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Aggregate_Order_By = {
  count?: InputMaybe<Order_By>;
  max?: InputMaybe<Vegetated_Areas_Urban_Constraints_Max_Order_By>;
  min?: InputMaybe<Vegetated_Areas_Urban_Constraints_Min_Order_By>;
};

/** input type for inserting array relation for remote table "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Arr_Rel_Insert_Input = {
  data: Array<Vegetated_Areas_Urban_Constraints_Insert_Input>;
  /** upsert condition */
  on_conflict?: InputMaybe<Vegetated_Areas_Urban_Constraints_On_Conflict>;
};

/** Boolean expression to filter rows from the table "vegetated_areas_urban_constraints". All fields are combined with a logical 'AND'. */
export type Vegetated_Areas_Urban_Constraints_Bool_Exp = {
  _and?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Bool_Exp>>;
  _not?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
  _or?: InputMaybe<Array<Vegetated_Areas_Urban_Constraints_Bool_Exp>>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  urban_constraint?: InputMaybe<Urban_Constraint_Bool_Exp>;
  urban_constraint_id?: InputMaybe<Uuid_Comparison_Exp>;
  vegetated_area_id?: InputMaybe<Uuid_Comparison_Exp>;
};

/** unique or primary key constraints on table "vegetated_areas_urban_constraints" */
export enum Vegetated_Areas_Urban_Constraints_Constraint {
  /** unique or primary key constraint on columns "id" */
  VegetatedAreasUrbanConstraintsPkey = 'vegetated_areas_urban_constraints_pkey'
}

/** input type for inserting data into table "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Insert_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  urban_constraint?: InputMaybe<Urban_Constraint_Obj_Rel_Insert_Input>;
  urban_constraint_id?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type Vegetated_Areas_Urban_Constraints_Max_Fields = {
  __typename?: 'vegetated_areas_urban_constraints_max_fields';
  id?: Maybe<Scalars['uuid']>;
  urban_constraint_id?: Maybe<Scalars['uuid']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Max_Order_By = {
  id?: InputMaybe<Order_By>;
  urban_constraint_id?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** aggregate min on columns */
export type Vegetated_Areas_Urban_Constraints_Min_Fields = {
  __typename?: 'vegetated_areas_urban_constraints_min_fields';
  id?: Maybe<Scalars['uuid']>;
  urban_constraint_id?: Maybe<Scalars['uuid']>;
  vegetated_area_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Min_Order_By = {
  id?: InputMaybe<Order_By>;
  urban_constraint_id?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** response of any mutation on the table "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Mutation_Response = {
  __typename?: 'vegetated_areas_urban_constraints_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Vegetated_Areas_Urban_Constraints>;
};

/** on_conflict condition type for table "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_On_Conflict = {
  constraint: Vegetated_Areas_Urban_Constraints_Constraint;
  update_columns?: Array<Vegetated_Areas_Urban_Constraints_Update_Column>;
  where?: InputMaybe<Vegetated_Areas_Urban_Constraints_Bool_Exp>;
};

/** Ordering options when selecting data from "vegetated_areas_urban_constraints". */
export type Vegetated_Areas_Urban_Constraints_Order_By = {
  id?: InputMaybe<Order_By>;
  urban_constraint?: InputMaybe<Urban_Constraint_Order_By>;
  urban_constraint_id?: InputMaybe<Order_By>;
  vegetated_area_id?: InputMaybe<Order_By>;
};

/** primary key columns input for table: vegetated_areas_urban_constraints */
export type Vegetated_Areas_Urban_Constraints_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "vegetated_areas_urban_constraints" */
export enum Vegetated_Areas_Urban_Constraints_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  UrbanConstraintId = 'urban_constraint_id',
  /** column name */
  VegetatedAreaId = 'vegetated_area_id'
}

/** input type for updating data in table "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Set_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  urban_constraint_id?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** Streaming cursor of the table "vegetated_areas_urban_constraints" */
export type Vegetated_Areas_Urban_Constraints_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Vegetated_Areas_Urban_Constraints_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Vegetated_Areas_Urban_Constraints_Stream_Cursor_Value_Input = {
  id?: InputMaybe<Scalars['uuid']>;
  urban_constraint_id?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
};

/** update columns of table "vegetated_areas_urban_constraints" */
export enum Vegetated_Areas_Urban_Constraints_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  UrbanConstraintId = 'urban_constraint_id',
  /** column name */
  VegetatedAreaId = 'vegetated_area_id'
}

export type Vegetated_Areas_Urban_Constraints_Updates = {
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Vegetated_Areas_Urban_Constraints_Set_Input>;
  /** filter the rows which have to be updated */
  where: Vegetated_Areas_Urban_Constraints_Bool_Exp;
};

/** Table for managing worksite group */
export type Worksite = {
  __typename?: 'worksite';
  cost?: Maybe<Scalars['numeric']>;
  creation_date: Scalars['timestamptz'];
  deletion_date?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  id: Scalars['uuid'];
  /** An array relationship */
  interventions: Array<Intervention>;
  /** An aggregate relationship */
  interventions_aggregate: Intervention_Aggregate;
  name: Scalars['String'];
  realization_date?: Maybe<Scalars['date']>;
  realization_report?: Maybe<Scalars['String']>;
  realization_user_id?: Maybe<Scalars['String']>;
  scheduled_end_date?: Maybe<Scalars['date']>;
  scheduled_start_date?: Maybe<Scalars['date']>;
  /** An object relationship */
  user_entity?: Maybe<User_Entity>;
};


/** Table for managing worksite group */
export type WorksiteInterventionsArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Order_By>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};


/** Table for managing worksite group */
export type WorksiteInterventions_AggregateArgs = {
  distinct_on?: InputMaybe<Array<Intervention_Select_Column>>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  order_by?: InputMaybe<Array<Intervention_Order_By>>;
  where?: InputMaybe<Intervention_Bool_Exp>;
};

/** aggregated selection of "worksite" */
export type Worksite_Aggregate = {
  __typename?: 'worksite_aggregate';
  aggregate?: Maybe<Worksite_Aggregate_Fields>;
  nodes: Array<Worksite>;
};

/** aggregate fields of "worksite" */
export type Worksite_Aggregate_Fields = {
  __typename?: 'worksite_aggregate_fields';
  avg?: Maybe<Worksite_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Worksite_Max_Fields>;
  min?: Maybe<Worksite_Min_Fields>;
  stddev?: Maybe<Worksite_Stddev_Fields>;
  stddev_pop?: Maybe<Worksite_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Worksite_Stddev_Samp_Fields>;
  sum?: Maybe<Worksite_Sum_Fields>;
  var_pop?: Maybe<Worksite_Var_Pop_Fields>;
  var_samp?: Maybe<Worksite_Var_Samp_Fields>;
  variance?: Maybe<Worksite_Variance_Fields>;
};


/** aggregate fields of "worksite" */
export type Worksite_Aggregate_FieldsCountArgs = {
  columns?: InputMaybe<Array<Worksite_Select_Column>>;
  distinct?: InputMaybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type Worksite_Avg_Fields = {
  __typename?: 'worksite_avg_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "worksite". All fields are combined with a logical 'AND'. */
export type Worksite_Bool_Exp = {
  _and?: InputMaybe<Array<Worksite_Bool_Exp>>;
  _not?: InputMaybe<Worksite_Bool_Exp>;
  _or?: InputMaybe<Array<Worksite_Bool_Exp>>;
  cost?: InputMaybe<Numeric_Comparison_Exp>;
  creation_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  deletion_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  description?: InputMaybe<String_Comparison_Exp>;
  edition_date?: InputMaybe<Timestamptz_Comparison_Exp>;
  id?: InputMaybe<Uuid_Comparison_Exp>;
  interventions?: InputMaybe<Intervention_Bool_Exp>;
  interventions_aggregate?: InputMaybe<Intervention_Aggregate_Bool_Exp>;
  name?: InputMaybe<String_Comparison_Exp>;
  realization_date?: InputMaybe<Date_Comparison_Exp>;
  realization_report?: InputMaybe<String_Comparison_Exp>;
  realization_user_id?: InputMaybe<String_Comparison_Exp>;
  scheduled_end_date?: InputMaybe<Date_Comparison_Exp>;
  scheduled_start_date?: InputMaybe<Date_Comparison_Exp>;
  user_entity?: InputMaybe<User_Entity_Bool_Exp>;
};

/** unique or primary key constraints on table "worksite" */
export enum Worksite_Constraint {
  /** unique or primary key constraint on columns "id" */
  WorksitePkey = 'worksite_pkey'
}

/** input type for incrementing numeric columns in table "worksite" */
export type Worksite_Inc_Input = {
  cost?: InputMaybe<Scalars['numeric']>;
};

/** input type for inserting data into table "worksite" */
export type Worksite_Insert_Input = {
  cost?: InputMaybe<Scalars['numeric']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  deletion_date?: InputMaybe<Scalars['timestamptz']>;
  description?: InputMaybe<Scalars['String']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  interventions?: InputMaybe<Intervention_Arr_Rel_Insert_Input>;
  name?: InputMaybe<Scalars['String']>;
  realization_date?: InputMaybe<Scalars['date']>;
  realization_report?: InputMaybe<Scalars['String']>;
  realization_user_id?: InputMaybe<Scalars['String']>;
  scheduled_end_date?: InputMaybe<Scalars['date']>;
  scheduled_start_date?: InputMaybe<Scalars['date']>;
  user_entity?: InputMaybe<User_Entity_Obj_Rel_Insert_Input>;
};

/** aggregate max on columns */
export type Worksite_Max_Fields = {
  __typename?: 'worksite_max_fields';
  cost?: Maybe<Scalars['numeric']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  deletion_date?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  realization_date?: Maybe<Scalars['date']>;
  realization_report?: Maybe<Scalars['String']>;
  realization_user_id?: Maybe<Scalars['String']>;
  scheduled_end_date?: Maybe<Scalars['date']>;
  scheduled_start_date?: Maybe<Scalars['date']>;
};

/** aggregate min on columns */
export type Worksite_Min_Fields = {
  __typename?: 'worksite_min_fields';
  cost?: Maybe<Scalars['numeric']>;
  creation_date?: Maybe<Scalars['timestamptz']>;
  deletion_date?: Maybe<Scalars['timestamptz']>;
  description?: Maybe<Scalars['String']>;
  edition_date?: Maybe<Scalars['timestamptz']>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  realization_date?: Maybe<Scalars['date']>;
  realization_report?: Maybe<Scalars['String']>;
  realization_user_id?: Maybe<Scalars['String']>;
  scheduled_end_date?: Maybe<Scalars['date']>;
  scheduled_start_date?: Maybe<Scalars['date']>;
};

/** response of any mutation on the table "worksite" */
export type Worksite_Mutation_Response = {
  __typename?: 'worksite_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Worksite>;
};

/** input type for inserting object relation for remote table "worksite" */
export type Worksite_Obj_Rel_Insert_Input = {
  data: Worksite_Insert_Input;
  /** upsert condition */
  on_conflict?: InputMaybe<Worksite_On_Conflict>;
};

/** on_conflict condition type for table "worksite" */
export type Worksite_On_Conflict = {
  constraint: Worksite_Constraint;
  update_columns?: Array<Worksite_Update_Column>;
  where?: InputMaybe<Worksite_Bool_Exp>;
};

/** Ordering options when selecting data from "worksite". */
export type Worksite_Order_By = {
  cost?: InputMaybe<Order_By>;
  creation_date?: InputMaybe<Order_By>;
  deletion_date?: InputMaybe<Order_By>;
  description?: InputMaybe<Order_By>;
  edition_date?: InputMaybe<Order_By>;
  id?: InputMaybe<Order_By>;
  interventions_aggregate?: InputMaybe<Intervention_Aggregate_Order_By>;
  name?: InputMaybe<Order_By>;
  realization_date?: InputMaybe<Order_By>;
  realization_report?: InputMaybe<Order_By>;
  realization_user_id?: InputMaybe<Order_By>;
  scheduled_end_date?: InputMaybe<Order_By>;
  scheduled_start_date?: InputMaybe<Order_By>;
  user_entity?: InputMaybe<User_Entity_Order_By>;
};

/** primary key columns input for table: worksite */
export type Worksite_Pk_Columns_Input = {
  id: Scalars['uuid'];
};

/** select columns of table "worksite" */
export enum Worksite_Select_Column {
  /** column name */
  Cost = 'cost',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  DeletionDate = 'deletion_date',
  /** column name */
  Description = 'description',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  RealizationDate = 'realization_date',
  /** column name */
  RealizationReport = 'realization_report',
  /** column name */
  RealizationUserId = 'realization_user_id',
  /** column name */
  ScheduledEndDate = 'scheduled_end_date',
  /** column name */
  ScheduledStartDate = 'scheduled_start_date'
}

/** input type for updating data in table "worksite" */
export type Worksite_Set_Input = {
  cost?: InputMaybe<Scalars['numeric']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  deletion_date?: InputMaybe<Scalars['timestamptz']>;
  description?: InputMaybe<Scalars['String']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  name?: InputMaybe<Scalars['String']>;
  realization_date?: InputMaybe<Scalars['date']>;
  realization_report?: InputMaybe<Scalars['String']>;
  realization_user_id?: InputMaybe<Scalars['String']>;
  scheduled_end_date?: InputMaybe<Scalars['date']>;
  scheduled_start_date?: InputMaybe<Scalars['date']>;
};

/** aggregate stddev on columns */
export type Worksite_Stddev_Fields = {
  __typename?: 'worksite_stddev_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Worksite_Stddev_Pop_Fields = {
  __typename?: 'worksite_stddev_pop_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Worksite_Stddev_Samp_Fields = {
  __typename?: 'worksite_stddev_samp_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** Streaming cursor of the table "worksite" */
export type Worksite_Stream_Cursor_Input = {
  /** Stream column input with initial value */
  initial_value: Worksite_Stream_Cursor_Value_Input;
  /** cursor ordering */
  ordering?: InputMaybe<Cursor_Ordering>;
};

/** Initial value of the column from where the streaming should start */
export type Worksite_Stream_Cursor_Value_Input = {
  cost?: InputMaybe<Scalars['numeric']>;
  creation_date?: InputMaybe<Scalars['timestamptz']>;
  deletion_date?: InputMaybe<Scalars['timestamptz']>;
  description?: InputMaybe<Scalars['String']>;
  edition_date?: InputMaybe<Scalars['timestamptz']>;
  id?: InputMaybe<Scalars['uuid']>;
  name?: InputMaybe<Scalars['String']>;
  realization_date?: InputMaybe<Scalars['date']>;
  realization_report?: InputMaybe<Scalars['String']>;
  realization_user_id?: InputMaybe<Scalars['String']>;
  scheduled_end_date?: InputMaybe<Scalars['date']>;
  scheduled_start_date?: InputMaybe<Scalars['date']>;
};

/** aggregate sum on columns */
export type Worksite_Sum_Fields = {
  __typename?: 'worksite_sum_fields';
  cost?: Maybe<Scalars['numeric']>;
};

/** update columns of table "worksite" */
export enum Worksite_Update_Column {
  /** column name */
  Cost = 'cost',
  /** column name */
  CreationDate = 'creation_date',
  /** column name */
  DeletionDate = 'deletion_date',
  /** column name */
  Description = 'description',
  /** column name */
  EditionDate = 'edition_date',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  RealizationDate = 'realization_date',
  /** column name */
  RealizationReport = 'realization_report',
  /** column name */
  RealizationUserId = 'realization_user_id',
  /** column name */
  ScheduledEndDate = 'scheduled_end_date',
  /** column name */
  ScheduledStartDate = 'scheduled_start_date'
}

export type Worksite_Updates = {
  /** increments the numeric columns with given value of the filtered values */
  _inc?: InputMaybe<Worksite_Inc_Input>;
  /** sets the columns of the filtered rows to the given values */
  _set?: InputMaybe<Worksite_Set_Input>;
  /** filter the rows which have to be updated */
  where: Worksite_Bool_Exp;
};

/** aggregate var_pop on columns */
export type Worksite_Var_Pop_Fields = {
  __typename?: 'worksite_var_pop_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Worksite_Var_Samp_Fields = {
  __typename?: 'worksite_var_samp_fields';
  cost?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Worksite_Variance_Fields = {
  __typename?: 'worksite_variance_fields';
  cost?: Maybe<Scalars['Float']>;
};

export type CreateLocationMutationVariables = Exact<{
  address?: InputMaybe<Scalars['String']>;
  coords?: InputMaybe<Scalars['geometry']>;
  foot_type?: InputMaybe<Scalars['String']>;
  plantation_type?: InputMaybe<Scalars['String']>;
  sidewalk_type?: InputMaybe<Scalars['String']>;
  urban_site_id?: InputMaybe<Scalars['uuid']>;
  landscape_type?: InputMaybe<Scalars['String']>;
  id_status?: InputMaybe<Scalars['uuid']>;
  organization_id?: InputMaybe<Scalars['uuid']>;
  is_protected?: InputMaybe<Scalars['Boolean']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
}>;


export type CreateLocationMutation = { __typename?: 'mutation_root', insert_location_one?: { __typename?: 'location', address?: string | null, coords: any, foot_type?: string | null, plantation_type?: string | null, urban_site_id?: any | null, sidewalk_type?: string | null, id: any, status: { __typename?: 'location_status', id: any, status: string } } | null };

export type InsertLocationUrbanConstraintMutationVariables = Exact<{
  location_id?: InputMaybe<Scalars['uuid']>;
  urban_constraint_id?: InputMaybe<Scalars['uuid']>;
}>;


export type InsertLocationUrbanConstraintMutation = { __typename?: 'mutation_root', insert_location_urban_constraint?: { __typename?: 'location_urban_constraint_mutation_response', affected_rows: number } | null };

export type CreateTreeMutationVariables = Exact<{
  watering?: InputMaybe<Scalars['Boolean']>;
  scientific_name?: InputMaybe<Scalars['String']>;
  vernacular_name?: InputMaybe<Scalars['String']>;
  plantation_date?: InputMaybe<Scalars['String']>;
  serial_number?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  is_tree_of_interest?: InputMaybe<Scalars['Boolean']>;
  variety?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['numeric']>;
  estimated_date?: InputMaybe<Scalars['Boolean']>;
  habit?: InputMaybe<Scalars['String']>;
  circumference?: InputMaybe<Scalars['numeric']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  taxon_id?: InputMaybe<Scalars['Int']>;
}>;


export type CreateTreeMutation = { __typename?: 'mutation_root', insert_tree_one?: { __typename?: 'tree', id: any, location_id: any } | null };

export type UpdateLocationMutationVariables = Exact<{
  locationId?: InputMaybe<Scalars['uuid']>;
  address?: InputMaybe<Scalars['String']>;
  coords?: InputMaybe<Scalars['geometry']>;
  foot_type?: InputMaybe<Scalars['String']>;
  plantation_type?: InputMaybe<Scalars['String']>;
  is_protected?: InputMaybe<Scalars['Boolean']>;
  urban_site_id?: InputMaybe<Scalars['uuid']>;
  landscape_type?: InputMaybe<Scalars['String']>;
  sidewalk_type?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  inventory_source?: InputMaybe<Scalars['jsonb']>;
}>;


export type UpdateLocationMutation = { __typename?: 'mutation_root', update_location_by_pk?: { __typename?: 'location', id: any, status: { __typename?: 'location_status', id: any, status: string } } | null };

export type DeleteLocationBoundariesMutationVariables = Exact<{
  location_id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteLocationBoundariesMutation = { __typename?: 'mutation_root', delete_boundaries_locations?: { __typename?: 'boundaries_locations_mutation_response', affected_rows: number } | null };

export type DeleteLocationUrbanConstraintsMutationVariables = Exact<{
  location_id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteLocationUrbanConstraintsMutation = { __typename?: 'mutation_root', delete_location_urban_constraint?: { __typename?: 'location_urban_constraint_mutation_response', affected_rows: number } | null };

export type InsertLocationBoundariesMutationVariables = Exact<{
  boundary_id?: InputMaybe<Scalars['uuid']>;
  location_id?: InputMaybe<Scalars['uuid']>;
}>;


export type InsertLocationBoundariesMutation = { __typename?: 'mutation_root', insert_boundaries_locations_one?: { __typename?: 'boundaries_locations', id: any } | null };

export type InsertVegetatedAreaBoundariesMutationVariables = Exact<{
  boundary_id?: InputMaybe<Scalars['uuid']>;
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
}>;


export type InsertVegetatedAreaBoundariesMutation = { __typename?: 'mutation_root', insert_boundaries_vegetated_areas_one?: { __typename?: 'boundaries_vegetated_areas', id: any } | null };

export type InsertLocationVegetatedAreaMutationVariables = Exact<{
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
  location_id?: InputMaybe<Scalars['uuid']>;
}>;


export type InsertLocationVegetatedAreaMutation = { __typename?: 'mutation_root', insert_vegetated_areas_locations_one?: { __typename?: 'vegetated_areas_locations', id: any } | null };

export type UpdateTreeMutationVariables = Exact<{
  treeId?: InputMaybe<Scalars['uuid']>;
  circumference?: InputMaybe<Scalars['numeric']>;
  variety?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['numeric']>;
  is_tree_of_interest?: InputMaybe<Scalars['Boolean']>;
  note?: InputMaybe<Scalars['String']>;
  plantation_date?: InputMaybe<Scalars['String']>;
  vernacular_name?: InputMaybe<Scalars['String']>;
  scientific_name?: InputMaybe<Scalars['String']>;
  serial_number?: InputMaybe<Scalars['String']>;
  estimated_date?: InputMaybe<Scalars['Boolean']>;
  habit?: InputMaybe<Scalars['String']>;
  watering?: InputMaybe<Scalars['Boolean']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  taxon_id?: InputMaybe<Scalars['Int']>;
  felling_date?: InputMaybe<Scalars['date']>;
  address?: InputMaybe<Scalars['String']>;
  coords?: InputMaybe<Scalars['geometry']>;
}>;


export type UpdateTreeMutation = { __typename?: 'mutation_root', update_tree_by_pk?: { __typename?: 'tree', id: any, location_id: any, taxon_id?: number | null, location: { __typename?: 'location', location_status: { __typename?: 'location_status', status: string } } } | null };

export type UpdateTreeWithFellingDateMutationVariables = Exact<{
  treeId?: InputMaybe<Scalars['uuid']>;
  felling_date?: InputMaybe<Scalars['date']>;
}>;


export type UpdateTreeWithFellingDateMutation = { __typename?: 'mutation_root', update_tree_by_pk?: { __typename?: 'tree', id: any, location_id: any } | null };

export type UpdateTreeAndLocationMutationVariables = Exact<{
  treeId?: InputMaybe<Scalars['uuid']>;
  circumference?: InputMaybe<Scalars['numeric']>;
  variety?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['numeric']>;
  is_tree_of_interest?: InputMaybe<Scalars['Boolean']>;
  note?: InputMaybe<Scalars['String']>;
  plantation_date?: InputMaybe<Scalars['String']>;
  vernacular_name?: InputMaybe<Scalars['String']>;
  scientific_name?: InputMaybe<Scalars['String']>;
  serial_number?: InputMaybe<Scalars['String']>;
  estimated_date?: InputMaybe<Scalars['Boolean']>;
  habit?: InputMaybe<Scalars['String']>;
  watering?: InputMaybe<Scalars['Boolean']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  taxon_id?: InputMaybe<Scalars['Int']>;
  urbasense_subject_id?: InputMaybe<Scalars['String']>;
  felling_date?: InputMaybe<Scalars['date']>;
  _setLocation?: InputMaybe<Location_Set_Input>;
}>;


export type UpdateTreeAndLocationMutation = { __typename?: 'mutation_root', update_tree_by_pk?: { __typename?: 'tree', id: any, location_id: any, taxon_id?: number | null, location: { __typename?: 'location', location_status: { __typename?: 'location_status', status: string } } } | null, update_location?: { __typename?: 'location_mutation_response', returning: Array<{ __typename?: 'location', id: any }> } | null };

export type UpdateTreesWithFellingDateMutationVariables = Exact<{
  _in?: InputMaybe<Array<Scalars['uuid']> | Scalars['uuid']>;
  felling_date?: InputMaybe<Scalars['date']>;
}>;


export type UpdateTreesWithFellingDateMutation = { __typename?: 'mutation_root', update_tree?: { __typename?: 'tree_mutation_response', returning: Array<{ __typename?: 'tree', id: any, location_id: any }> } | null };

export type UpdateTreesMutationVariables = Exact<{
  _in?: InputMaybe<Array<Scalars['uuid']> | Scalars['uuid']>;
  circumference?: InputMaybe<Scalars['numeric']>;
  variety?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['numeric']>;
  is_tree_of_interest?: InputMaybe<Scalars['Boolean']>;
  note?: InputMaybe<Scalars['String']>;
  plantation_date?: InputMaybe<Scalars['String']>;
  vernacular_name?: InputMaybe<Scalars['String']>;
  scientific_name?: InputMaybe<Scalars['String']>;
  serial_number?: InputMaybe<Scalars['String']>;
  estimated_date?: InputMaybe<Scalars['Boolean']>;
  habit?: InputMaybe<Scalars['String']>;
  watering?: InputMaybe<Scalars['Boolean']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  taxon_id?: InputMaybe<Scalars['Int']>;
  felling_date?: InputMaybe<Scalars['date']>;
}>;


export type UpdateTreesMutation = { __typename?: 'mutation_root', update_tree?: { __typename?: 'tree_mutation_response', returning: Array<{ __typename?: 'tree', id: any, location_id: any, taxon_id?: number | null, location: { __typename?: 'location', location_status: { __typename?: 'location_status', status: string } } }> } | null };

export type DeleteTreeMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteTreeMutation = { __typename?: 'mutation_root', delete_tree_by_pk?: { __typename?: 'tree', id: any, location_id: any } | null };

export type DeleteLocationMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteLocationMutation = { __typename?: 'mutation_root', delete_location_by_pk?: { __typename?: 'location', id: any } | null };

export type DeleteWorksiteByPkMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteWorksiteByPkMutation = { __typename?: 'mutation_root', delete_worksite_by_pk?: { __typename?: 'worksite', id: any } | null };

export type UpdateLocationStatusWithTreeIdMutationVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
  id_status?: InputMaybe<Scalars['uuid']>;
}>;


export type UpdateLocationStatusWithTreeIdMutation = { __typename?: 'mutation_root', update_location?: { __typename?: 'location_mutation_response', returning: Array<{ __typename?: 'location', id: any }> } | null };

export type UpdateLocationStatusWithLocationIdMutationVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
  id_status?: InputMaybe<Scalars['uuid']>;
}>;


export type UpdateLocationStatusWithLocationIdMutation = { __typename?: 'mutation_root', update_location?: { __typename?: 'location_mutation_response', returning: Array<{ __typename?: 'location', id: any }> } | null };

export type UpdateLocationsStatusesWithLocationsIdsMutationVariables = Exact<{
  _in?: InputMaybe<Array<Scalars['uuid']> | Scalars['uuid']>;
  id_status?: InputMaybe<Scalars['uuid']>;
}>;


export type UpdateLocationsStatusesWithLocationsIdsMutation = { __typename?: 'mutation_root', update_location?: { __typename?: 'location_mutation_response', returning: Array<{ __typename?: 'location', id: any }> } | null };

export type CreateInterventionMutationVariables = Exact<{
  realization_date?: InputMaybe<Scalars['date']>;
  scheduled_date?: InputMaybe<Scalars['date']>;
  note?: InputMaybe<Scalars['String']>;
  tree_id?: InputMaybe<Scalars['uuid']>;
  location_id?: InputMaybe<Scalars['uuid']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  intervention_partner_id?: InputMaybe<Scalars['uuid']>;
  intervention_type_id?: InputMaybe<Scalars['uuid']>;
  cost?: InputMaybe<Scalars['numeric']>;
  request_origin?: InputMaybe<Scalars['String']>;
  is_parking_banned?: InputMaybe<Scalars['Boolean']>;
}>;


export type CreateInterventionMutation = { __typename?: 'mutation_root', insert_intervention_one?: { __typename?: 'intervention', id: any, location_id?: any | null, realization_date?: any | null, cost?: number | null, tree?: { __typename?: 'tree', id: any, location_id: any } | null, intervention_type: { __typename?: 'intervention_type', slug: string } } | null };

export type CreateInterventionsMutationVariables = Exact<{
  interventions: Array<Intervention_Insert_Input> | Intervention_Insert_Input;
}>;


export type CreateInterventionsMutation = { __typename?: 'mutation_root', insert_intervention?: { __typename?: 'intervention_mutation_response', returning: Array<{ __typename?: 'intervention', id: any, location_id?: any | null, realization_date?: any | null, cost?: number | null, tree?: { __typename?: 'tree', id: any, location_id: any } | null, intervention_type: { __typename?: 'intervention_type', slug: string } }> } | null };

export type CreateInterventionPartnerMutationVariables = Exact<{
  name?: InputMaybe<Scalars['String']>;
}>;


export type CreateInterventionPartnerMutation = { __typename?: 'mutation_root', insert_intervention_partner_one?: { __typename?: 'intervention_partner', id: any, name: string } | null };

export type CreateDiagnosisMutationVariables = Exact<{
  diagnosis_date?: InputMaybe<Scalars['date']>;
  note?: InputMaybe<Scalars['String']>;
  tree_id?: InputMaybe<Scalars['uuid']>;
  tree_condition?: InputMaybe<Scalars['String']>;
  crown_condition?: InputMaybe<Scalars['String']>;
  trunk_condition?: InputMaybe<Scalars['String']>;
  collar_condition?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  recommendation?: InputMaybe<Scalars['String']>;
  diagnosis_type_id?: InputMaybe<Scalars['uuid']>;
  tree_is_dangerous?: InputMaybe<Scalars['Boolean']>;
  tree_vigor?: InputMaybe<Scalars['String']>;
  analysis_results?: InputMaybe<Scalars['String']>;
  carpenters_condition?: InputMaybe<Scalars['String']>;
}>;


export type CreateDiagnosisMutation = { __typename?: 'mutation_root', insert_diagnosis_one?: { __typename?: 'diagnosis', id: any, tree_id: any, recommendation?: string | null } | null };

export type InsertDiagnosisPathogenMutationVariables = Exact<{
  diagnosis_id?: InputMaybe<Scalars['uuid']>;
  pathogen_id?: InputMaybe<Scalars['uuid']>;
}>;


export type InsertDiagnosisPathogenMutation = { __typename?: 'mutation_root', insert_diagnosis_pathogen?: { __typename?: 'diagnosis_pathogen_mutation_response', affected_rows: number } | null };

export type DeleteDiagnosisPathogenMutationVariables = Exact<{
  diagnosis_id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteDiagnosisPathogenMutation = { __typename?: 'mutation_root', delete_diagnosis_pathogen?: { __typename?: 'diagnosis_pathogen_mutation_response', affected_rows: number } | null };

export type DeleteDiagnosisAnalysisToolMutationVariables = Exact<{
  diagnosis_id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteDiagnosisAnalysisToolMutation = { __typename?: 'mutation_root', delete_diagnosis_analysis_tool?: { __typename?: 'diagnosis_analysis_tool_mutation_response', affected_rows: number } | null };

export type InsertDiagnosisAnalysisToolMutationVariables = Exact<{
  diagnosis_id?: InputMaybe<Scalars['uuid']>;
  analysis_tool_id?: InputMaybe<Scalars['uuid']>;
}>;


export type InsertDiagnosisAnalysisToolMutation = { __typename?: 'mutation_root', insert_diagnosis_analysis_tool?: { __typename?: 'diagnosis_analysis_tool_mutation_response', affected_rows: number } | null };

export type UpdateInterventionMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
  realization_date?: InputMaybe<Scalars['date']>;
  scheduled_date?: InputMaybe<Scalars['date']>;
  note?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  intervention_partner_id?: InputMaybe<Scalars['uuid']>;
  intervention_type_id?: InputMaybe<Scalars['uuid']>;
  cost?: InputMaybe<Scalars['numeric']>;
  validation_note?: InputMaybe<Scalars['String']>;
  request_origin?: InputMaybe<Scalars['String']>;
  is_parking_banned?: InputMaybe<Scalars['Boolean']>;
}>;


export type UpdateInterventionMutation = { __typename?: 'mutation_root', update_intervention_by_pk?: { __typename?: 'intervention', id: any, location_id?: any | null, realization_date?: any | null, tree?: { __typename?: 'tree', location_id: any, id: any } | null, intervention_type: { __typename?: 'intervention_type', slug: string } } | null };

export type UpdateDiagnosisMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
  diagnosis_date?: InputMaybe<Scalars['date']>;
  recommendation?: InputMaybe<Scalars['String']>;
  tree_condition?: InputMaybe<Scalars['String']>;
  crown_condition?: InputMaybe<Scalars['String']>;
  trunk_condition?: InputMaybe<Scalars['String']>;
  collar_condition?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  diagnosis_type_id?: InputMaybe<Scalars['uuid']>;
  tree_is_dangerous?: InputMaybe<Scalars['Boolean']>;
  tree_vigor?: InputMaybe<Scalars['String']>;
  analysis_results?: InputMaybe<Scalars['String']>;
  carpenters_condition?: InputMaybe<Scalars['String']>;
}>;


export type UpdateDiagnosisMutation = { __typename?: 'mutation_root', update_diagnosis_by_pk?: { __typename?: 'diagnosis', id: any, tree_id: any } | null };

export type DeleteInterventionMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteInterventionMutation = { __typename?: 'mutation_root', delete_intervention_by_pk?: { __typename?: 'intervention', id: any, location_id?: any | null, tree?: { __typename?: 'tree', location_id: any, id: any } | null } | null };

export type DeleteInterventionsMutationVariables = Exact<{
  _in?: InputMaybe<Array<Scalars['uuid']> | Scalars['uuid']>;
}>;


export type DeleteInterventionsMutation = { __typename?: 'mutation_root', delete_intervention?: { __typename?: 'intervention_mutation_response', affected_rows: number } | null };

export type DeleteDiagnosesMutationVariables = Exact<{
  _in?: InputMaybe<Array<Scalars['uuid']> | Scalars['uuid']>;
}>;


export type DeleteDiagnosesMutation = { __typename?: 'mutation_root', delete_diagnosis?: { __typename?: 'diagnosis_mutation_response', affected_rows: number } | null };

export type DeleteDiagnosisMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteDiagnosisMutation = { __typename?: 'mutation_root', delete_diagnosis_by_pk?: { __typename?: 'diagnosis', id: any, tree_id: any } | null };

export type ValidateInterventionsMutationVariables = Exact<{
  _in?: InputMaybe<Array<Scalars['uuid']> | Scalars['uuid']>;
  realization_date?: InputMaybe<Scalars['date']>;
  intervention_partner_id?: InputMaybe<Scalars['uuid']>;
  validation_note?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
}>;


export type ValidateInterventionsMutation = { __typename?: 'mutation_root', update_intervention?: { __typename?: 'intervention_mutation_response', returning: Array<{ __typename?: 'intervention', id: any, location_id?: any | null, realization_date?: any | null, intervention_type: { __typename?: 'intervention_type', slug: string }, tree?: { __typename?: 'tree', location_id: any } | null, location?: { __typename?: 'location', location_status: { __typename?: 'location_status', status: string } } | null }> } | null };

export type ValidateInterventionMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
  realization_date?: InputMaybe<Scalars['date']>;
  intervention_partner_id?: InputMaybe<Scalars['uuid']>;
  validation_note?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
}>;


export type ValidateInterventionMutation = { __typename?: 'mutation_root', update_intervention_by_pk?: { __typename?: 'intervention', id: any, location_id?: any | null, realization_date?: any | null, tree?: { __typename?: 'tree', location_id: any } | null, intervention_type: { __typename?: 'intervention_type', slug: string }, location?: { __typename?: 'location', location_status: { __typename?: 'location_status', status: string } } | null } | null };

export type CreateImportMutationVariables = Exact<{
  data_entry_user_id?: InputMaybe<Scalars['String']>;
  source_file_path?: InputMaybe<Scalars['String']>;
  task_id?: InputMaybe<Scalars['String']>;
  source_crs?: InputMaybe<Scalars['String']>;
  organization_id?: InputMaybe<Scalars['uuid']>;
}>;


export type CreateImportMutation = { __typename?: 'mutation_root', insert_import_one?: { __typename?: 'import', id: any, source_file_path: string } | null };

export type DeleteImportsMutationVariables = Exact<{
  _in?: InputMaybe<Array<Scalars['uuid']> | Scalars['uuid']>;
}>;


export type DeleteImportsMutation = { __typename?: 'mutation_root', delete_import?: { __typename?: 'import_mutation_response', affected_rows: number } | null };

export type CreateOneWorksiteWithRelationshipsMutationVariables = Exact<{
  object?: Worksite_Insert_Input;
}>;


export type CreateOneWorksiteWithRelationshipsMutation = { __typename?: 'mutation_root', insert_worksite_one?: { __typename?: 'worksite', id: any, name: string, description?: string | null } | null };

export type WorksiteFieldsFragment = { __typename?: 'worksite', id: any, name: string, cost?: number | null, scheduled_start_date?: any | null, scheduled_end_date?: any | null, realization_date?: any | null, description?: string | null };

export type UpdateWorksiteMutationVariables = Exact<{
  id: Scalars['uuid'];
  input_data?: InputMaybe<Worksite_Set_Input>;
}>;


export type UpdateWorksiteMutation = { __typename?: 'mutation_root', update_worksite_by_pk?: { __typename?: 'worksite', id: any, name: string, cost?: number | null, scheduled_start_date?: any | null, scheduled_end_date?: any | null, realization_date?: any | null, description?: string | null } | null };

export type CreateBoundaryMutationVariables = Exact<{
  coords?: InputMaybe<Scalars['geometry']>;
  name?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
}>;


export type CreateBoundaryMutation = { __typename?: 'mutation_root', insert_boundary_one?: { __typename?: 'boundary', id: any } | null };

export type CreateBoundaryAndDeleteExistingAssociationsMutationVariables = Exact<{
  coords: Scalars['geometry'];
  name: Scalars['String'];
  type: Scalars['String'];
  locationsIds: Array<Scalars['uuid']> | Scalars['uuid'];
  vegetatedAreasIds: Array<Scalars['uuid']> | Scalars['uuid'];
  data_entry_user_id?: InputMaybe<Scalars['String']>;
}>;


export type CreateBoundaryAndDeleteExistingAssociationsMutation = { __typename?: 'mutation_root', delete_boundaries_locations?: { __typename?: 'boundaries_locations_mutation_response', affected_rows: number } | null, delete_boundaries_vegetated_areas?: { __typename?: 'boundaries_vegetated_areas_mutation_response', affected_rows: number } | null, insert_boundary_one?: { __typename?: 'boundary', id: any } | null };

export type UpdateBoundaryAndDeleteExistingAssociationsMutationVariables = Exact<{
  id: Scalars['uuid'];
  coords: Scalars['geometry'];
  name: Scalars['String'];
  type: Scalars['String'];
  locationsIds: Array<Scalars['uuid']> | Scalars['uuid'];
  vegetatedAreasIds: Array<Scalars['uuid']> | Scalars['uuid'];
  data_entry_user_id?: InputMaybe<Scalars['String']>;
}>;


export type UpdateBoundaryAndDeleteExistingAssociationsMutation = { __typename?: 'mutation_root', delete_boundaries_locations?: { __typename?: 'boundaries_locations_mutation_response', affected_rows: number } | null, delete_boundaries_vegetated_areas?: { __typename?: 'boundaries_vegetated_areas_mutation_response', affected_rows: number } | null, update_boundary_by_pk?: { __typename?: 'boundary', id: any } | null };

export type UpdateBoundaryMutationVariables = Exact<{
  id: Scalars['uuid'];
  name?: InputMaybe<Scalars['String']>;
  coords?: InputMaybe<Scalars['geometry']>;
  type?: InputMaybe<Scalars['String']>;
  data_entry_user_id?: InputMaybe<Scalars['String']>;
}>;


export type UpdateBoundaryMutation = { __typename?: 'mutation_root', update_boundary_by_pk?: { __typename?: 'boundary', id: any } | null };

export type DeleteBoundaryMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteBoundaryMutation = { __typename?: 'mutation_root', delete_boundary_by_pk?: { __typename?: 'boundary', id: any } | null };

export type CreateVegetatedAreaMutationVariables = Exact<{
  object?: InputMaybe<Vegetated_Area_Insert_Input>;
}>;


export type CreateVegetatedAreaMutation = { __typename?: 'mutation_root', insert_vegetated_area_one?: { __typename?: 'vegetated_area', id: any } | null };

export type DeleteVegetatedAreaMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteVegetatedAreaMutation = { __typename?: 'mutation_root', delete_vegetated_area_by_pk?: { __typename?: 'vegetated_area', id: any } | null };

export type UpdateVegetatedAreaMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
  _set?: InputMaybe<Vegetated_Area_Set_Input>;
}>;


export type UpdateVegetatedAreaMutation = { __typename?: 'mutation_root', update_vegetated_area_by_pk?: { __typename?: 'vegetated_area', id: any } | null };

export type DeleteVegetatedAreaRelationsMutationVariables = Exact<{
  id?: InputMaybe<Scalars['uuid']>;
}>;


export type DeleteVegetatedAreaRelationsMutation = { __typename?: 'mutation_root', delete_vegetated_areas_urban_constraints?: { __typename?: 'vegetated_areas_urban_constraints_mutation_response', affected_rows: number } | null, delete_vegetated_areas_residential_usage_types?: { __typename?: 'vegetated_areas_residential_usage_types_mutation_response', affected_rows: number } | null, delete_boundaries_vegetated_areas?: { __typename?: 'boundaries_vegetated_areas_mutation_response', affected_rows: number } | null };

export type InsertVegetatedAreaRelationsMutationVariables = Exact<{
  boundaryObjects: Array<Boundaries_Vegetated_Areas_Insert_Input> | Boundaries_Vegetated_Areas_Insert_Input;
  urbanConstraintObjects: Array<Vegetated_Areas_Urban_Constraints_Insert_Input> | Vegetated_Areas_Urban_Constraints_Insert_Input;
  residentialUsageTypeObjects: Array<Vegetated_Areas_Residential_Usage_Types_Insert_Input> | Vegetated_Areas_Residential_Usage_Types_Insert_Input;
}>;


export type InsertVegetatedAreaRelationsMutation = { __typename?: 'mutation_root', insert_boundaries_vegetated_areas?: { __typename?: 'boundaries_vegetated_areas_mutation_response', affected_rows: number } | null, insert_vegetated_areas_urban_constraints?: { __typename?: 'vegetated_areas_urban_constraints_mutation_response', affected_rows: number } | null, insert_vegetated_areas_residential_usage_types?: { __typename?: 'vegetated_areas_residential_usage_types_mutation_response', affected_rows: number } | null };

export type UpdateVegetatedAreaAndDeleteExistingLocationsMutationVariables = Exact<{
  id: Scalars['uuid'];
  coords: Scalars['geometry'];
  locationsIds: Array<Scalars['uuid']> | Scalars['uuid'];
}>;


export type UpdateVegetatedAreaAndDeleteExistingLocationsMutation = { __typename?: 'mutation_root', delete_vegetated_areas_locations?: { __typename?: 'vegetated_areas_locations_mutation_response', affected_rows: number } | null, update_vegetated_area_by_pk?: { __typename?: 'vegetated_area', id: any } | null };

export type FetchLocationStatusQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchLocationStatusQuery = { __typename?: 'query_root', status: Array<{ __typename?: 'location_status', id: any, status: string, color?: string | null }> };

export type FetchPageMapQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchPageMapQuery = { __typename?: 'query_root', locations: Array<{ __typename?: 'location', id: any, address?: string | null, coords: any, location_status: { __typename?: 'location_status', id: any, status: string, color?: string | null }, tree: Array<{ __typename?: 'tree', id: any, serial_number?: string | null, scientific_name?: string | null, plantation_date?: string | null, height?: number | null }> }> };

export type FetchLocationQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchLocationQuery = { __typename?: 'query_root', status: Array<{ __typename?: 'location_status', id: any, status: string, color?: string | null }>, locations: Array<{ __typename?: 'location', id: any, coords: any, location_status: { __typename?: 'location_status', id: any, status: string, color?: string | null }, tree: Array<{ __typename?: 'tree', id: any }> }> };

export type FetchLocationsWithStatusFiltersQueryVariables = Exact<{
  _nin?: InputMaybe<Array<Scalars['uuid']> | Scalars['uuid']>;
}>;


export type FetchLocationsWithStatusFiltersQuery = { __typename?: 'query_root', location: Array<{ __typename?: 'location', coords: any, address?: string | null, id: any, tree: Array<{ __typename?: 'tree', id: any }>, location_status: { __typename?: 'location_status', color?: string | null, status: string } }> };

export type FetchLocationsWithFiltersQueryVariables = Exact<{
  where?: InputMaybe<Location_Bool_Exp>;
}>;


export type FetchLocationsWithFiltersQuery = { __typename?: 'query_root', locations: Array<{ __typename?: 'location', id: any, address?: string | null, coords: any, location_status: { __typename?: 'location_status', id: any, status: string, color?: string | null }, tree: Array<{ __typename?: 'tree', id: any }> }> };

export type FetchAllInterventionTypesFamiliesQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchAllInterventionTypesFamiliesQuery = { __typename?: 'query_root', family_intervention_type: Array<{ __typename?: 'family_intervention_type', slug: string, intervention_types: Array<{ __typename?: 'intervention_type', id: any, slug: string, location_types?: any | null }> }> };

export type FetchOneLocationWithTreeIdQueryVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchOneLocationWithTreeIdQuery = { __typename?: 'query_root', location: Array<{ __typename?: 'location', id: any, address?: string | null, coords: any, foot_type?: string | null, plantation_type?: string | null, sidewalk_type?: string | null, landscape_type?: string | null, inventory_source?: any | null, is_protected?: boolean | null, note?: string | null, urban_site?: { __typename?: 'urban_site', id: any, slug: string } | null, location_vegetated_area: Array<{ __typename?: 'vegetated_areas_locations', vegetated_area_id: any }>, location_boundaries: Array<{ __typename?: 'boundaries_locations', boundary: { __typename?: 'boundary', name: string, id: any, type: string } }>, location_urban_constraints: Array<{ __typename?: 'location_urban_constraint', urban_constraint: { __typename?: 'urban_constraint', slug: string, id: any } }>, location_status: { __typename?: 'location_status', status: string }, tree: Array<{ __typename?: 'tree', id: any, serial_number?: string | null }> }> };

export type FetchTreeEventsQueryVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchTreeEventsQuery = { __typename?: 'query_root', intervention: Array<{ __typename?: 'intervention', id: any, scheduled_date: any, realization_date?: any | null, tree?: { __typename?: 'tree', location_id: any } | null, intervention_partner?: { __typename?: 'intervention_partner', name: string, id: any } | null, intervention_type: { __typename?: 'intervention_type', slug: string }, worksite?: { __typename?: 'worksite', name: string } | null }> };

export type FetchLocationEventsQueryVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchLocationEventsQuery = { __typename?: 'query_root', intervention: Array<{ __typename?: 'intervention', id: any, location_id?: any | null, scheduled_date: any, realization_date?: any | null, intervention_partner?: { __typename?: 'intervention_partner', name: string, id: any } | null, intervention_type: { __typename?: 'intervention_type', slug: string }, worksite?: { __typename?: 'worksite', name: string } | null }> };

export type FetchOneLocationQueryVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchOneLocationQuery = { __typename?: 'query_root', location: Array<{ __typename?: 'location', id: any, address?: string | null, coords: any, foot_type?: string | null, plantation_type?: string | null, sidewalk_type?: string | null, landscape_type?: string | null, inventory_source?: any | null, is_protected?: boolean | null, note?: string | null, id_status: any, urban_site?: { __typename?: 'urban_site', id: any, slug: string } | null, location_vegetated_area: Array<{ __typename?: 'vegetated_areas_locations', vegetated_area_id: any }>, location_boundaries: Array<{ __typename?: 'boundaries_locations', boundary: { __typename?: 'boundary', name: string, id: any, type: string } }>, location_urban_constraints: Array<{ __typename?: 'location_urban_constraint', urban_constraint: { __typename?: 'urban_constraint', slug: string, id: any } }>, location_status: { __typename?: 'location_status', id: any, status: string }, tree: Array<{ __typename?: 'tree', id: any }>, interventions_aggregate: { __typename?: 'intervention_aggregate', aggregate?: { __typename?: 'intervention_aggregate_fields', count: number } | null } }> };

export type FetchAllTreesNotableQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchAllTreesNotableQuery = { __typename?: 'query_root', tree: Array<{ __typename?: 'tree', id: any }> };

export type FetchAllImportsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchAllImportsQuery = { __typename?: 'query_root', import: Array<{ __typename?: 'import', id: any, source_file_path: string, creation_date: string, user_entity: { __typename?: 'user_entity', username?: string | null } }> };

export type FetchOneTreeQueryVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchOneTreeQuery = { __typename?: 'query_root', tree: Array<{ __typename?: 'tree', circumference?: number | null, estimated_date?: boolean | null, habit?: string | null, height?: number | null, id: any, is_tree_of_interest?: boolean | null, urbasense_subject_id?: string | null, note?: string | null, felling_date?: any | null, taxon_id?: number | null, plantation_date?: string | null, scientific_name?: string | null, serial_number?: string | null, variety?: string | null, vernacular_name?: string | null, watering?: boolean | null, location: { __typename?: 'location', address?: string | null, coords: any, id: any, location_status: { __typename?: 'location_status', status: string }, location_vegetated_area: Array<{ __typename?: 'vegetated_areas_locations', vegetated_area_id: any }>, location_boundaries: Array<{ __typename?: 'boundaries_locations', boundary: { __typename?: 'boundary', name: string, id: any, type: string } }>, interventions_aggregate: { __typename?: 'intervention_aggregate', aggregate?: { __typename?: 'intervention_aggregate_fields', count: number } | null } }, last_diagnoses: Array<{ __typename?: 'diagnosis', id: any, diagnosis_date: any, tree_condition: string, tree_is_dangerous?: boolean | null, trunk_condition?: string | null, crown_condition?: string | null, collar_condition?: string | null, tree: { __typename?: 'tree', id: any } }> }> };

export type FetchDefaultOrganizationQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchDefaultOrganizationQuery = { __typename?: 'query_root', organization: Array<{ __typename?: 'organization', image: string, name: string, population: number, surface: number, id: any, default: boolean, bucket?: any | null, domain?: string | null, coords?: any | null }> };

export type FetchInterventionPartnersQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchInterventionPartnersQuery = { __typename?: 'query_root', intervention_partner: Array<{ __typename?: 'intervention_partner', id: any, name: string }> };

export type FetchInterventionTypesAndPartnersQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchInterventionTypesAndPartnersQuery = { __typename?: 'query_root', family_intervention_type: Array<{ __typename?: 'family_intervention_type', slug: string, intervention_types: Array<{ __typename?: 'intervention_type', id: any, slug: string, location_types?: any | null }> }>, intervention_partner: Array<{ __typename?: 'intervention_partner', id: any, name: string }> };

export type FetchDiagnosisFormDataQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchDiagnosisFormDataQuery = { __typename?: 'query_root', diagnosis_type: Array<{ __typename?: 'diagnosis_type', id: any, slug: string }>, pathogen: Array<{ __typename?: 'pathogen', id: any, slug: string }>, analysis_tool: Array<{ __typename?: 'analysis_tool', id: any, slug: string }> };

export type FetchDiagnosisTypesQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchDiagnosisTypesQuery = { __typename?: 'query_root', diagnosis_type: Array<{ __typename?: 'diagnosis_type', id: any, slug: string }> };

export type FetchOneInterventionQueryVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchOneInterventionQuery = { __typename?: 'query_root', intervention: Array<{ __typename?: 'intervention', id: any, note?: string | null, cost?: number | null, realization_date?: any | null, scheduled_date: any, validation_note?: string | null, request_origin?: string | null, is_parking_banned?: boolean | null, intervention_partner?: { __typename?: 'intervention_partner', name: string, id: any } | null, user_entity: { __typename?: 'user_entity', username?: string | null }, intervention_type: { __typename?: 'intervention_type', slug: string, id: any }, location?: { __typename?: 'location', address?: string | null, coords: any, id: any, location_status: { __typename?: 'location_status', status: string } } | null, tree?: { __typename?: 'tree', serial_number?: string | null, id: any, location_id: any, location: { __typename?: 'location', coords: any, address?: string | null, id: any } } | null, worksite?: { __typename?: 'worksite', id: any } | null }> };

export type FetchOneDiagnosisQueryVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchOneDiagnosisQuery = { __typename?: 'query_root', diagnosis: Array<{ __typename?: 'diagnosis', id: any, tree_id: any, note?: string | null, diagnosis_date: any, tree_condition: string, crown_condition?: string | null, trunk_condition?: string | null, collar_condition?: string | null, carpenters_condition?: string | null, tree_is_dangerous?: boolean | null, tree_vigor?: string | null, analysis_results?: string | null, recommendation?: string | null, user_entity: { __typename?: 'user_entity', username?: string | null }, diagnosis_type: { __typename?: 'diagnosis_type', slug: string, id: any }, pathogens: Array<{ __typename?: 'diagnosis_pathogen', pathogen: { __typename?: 'pathogen', slug: string, id: any } }>, analysis_tools: Array<{ __typename?: 'diagnosis_analysis_tool', analysis_tool: { __typename?: 'analysis_tool', slug: string, id: any } }>, tree: { __typename?: 'tree', serial_number?: string | null, location: { __typename?: 'location', id: any, address?: string | null } } }> };

export type FetchAllDiagnosesQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchAllDiagnosesQuery = { __typename?: 'query_root', diagnosis: Array<{ __typename?: 'diagnosis', id: any, tree_id: any, note?: string | null, diagnosis_date: any, tree_condition: string, crown_condition?: string | null, trunk_condition?: string | null, collar_condition?: string | null, recommendation?: string | null, user_entity: { __typename?: 'user_entity', username?: string | null }, diagnosis_type: { __typename?: 'diagnosis_type', slug: string, id: any }, tree: { __typename?: 'tree', serial_number?: string | null, location: { __typename?: 'location', id: any, address?: string | null } } }>, diagnosis_type: Array<{ __typename?: 'diagnosis_type', slug: string, id: any }> };

export type FetchAllDiagnosesOfATreeQueryVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchAllDiagnosesOfATreeQuery = { __typename?: 'query_root', diagnosis: Array<{ __typename?: 'diagnosis', id: any, tree_id: any, note?: string | null, diagnosis_date: any, tree_condition: string, crown_condition?: string | null, trunk_condition?: string | null, collar_condition?: string | null, recommendation?: string | null, user_entity: { __typename?: 'user_entity', username?: string | null }, diagnosis_type: { __typename?: 'diagnosis_type', slug: string, id: any }, tree: { __typename?: 'tree', serial_number?: string | null, location: { __typename?: 'location', id: any, address?: string | null } } }>, diagnosis_type: Array<{ __typename?: 'diagnosis_type', slug: string, id: any }> };

export type FetchDangerousConditionQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchDangerousConditionQuery = { __typename?: 'query_root', diagnosis: Array<{ __typename?: 'diagnosis', id: any, tree_is_dangerous?: boolean | null }> };

export type FetchDataAllInterventionsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchDataAllInterventionsQuery = { __typename?: 'query_root', intervention: Array<{ __typename?: 'intervention', id: any, validation_note?: string | null, location_id?: any | null, tree_id?: any | null, cost?: number | null, request_origin?: string | null, is_parking_banned?: boolean | null, note?: string | null, scheduled_date: any, realization_date?: any | null, tree?: { __typename?: 'tree', location_id: any, scientific_name?: string | null, serial_number?: string | null, location: { __typename?: 'location', address?: string | null, location_status: { __typename?: 'location_status', status: string } } } | null, location?: { __typename?: 'location', address?: string | null, location_status: { __typename?: 'location_status', status: string } } | null, intervention_partner?: { __typename?: 'intervention_partner', name: string, id: any } | null, intervention_type: { __typename?: 'intervention_type', slug: string, location_types?: any | null, family_intervention_type?: { __typename?: 'family_intervention_type', slug: string } | null }, worksite?: { __typename?: 'worksite', name: string } | null }> };

export type FetchAllInterventionsOfATreeQueryVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchAllInterventionsOfATreeQuery = { __typename?: 'query_root', intervention: Array<{ __typename?: 'intervention', id: any, validation_note?: string | null, cost?: number | null, note?: string | null, scheduled_date: any, realization_date?: any | null, request_origin?: string | null, is_parking_banned?: boolean | null, tree?: { __typename?: 'tree', scientific_name?: string | null, serial_number?: string | null, location: { __typename?: 'location', address?: string | null, location_status: { __typename?: 'location_status', status: string } } } | null, intervention_partner?: { __typename?: 'intervention_partner', name: string, id: any } | null, intervention_type: { __typename?: 'intervention_type', slug: string, family_intervention_type?: { __typename?: 'family_intervention_type', slug: string } | null }, worksite?: { __typename?: 'worksite', id: any, name: string } | null }> };

export type FetchDataInterventionsGridQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchDataInterventionsGridQuery = { __typename?: 'query_root', family_intervention_type: Array<{ __typename?: 'family_intervention_type', slug: string, intervention_types: Array<{ __typename?: 'intervention_type', id: any, slug: string, location_types?: any | null }> }>, intervention_partner: Array<{ __typename?: 'intervention_partner', id: any, name: string }> };

export type FetchAllInterventionsOfALocationQueryVariables = Exact<{
  _eq?: InputMaybe<Scalars['uuid']>;
}>;


export type FetchAllInterventionsOfALocationQuery = { __typename?: 'query_root', intervention: Array<{ __typename?: 'intervention', id: any, location_id?: any | null, validation_note?: string | null, cost?: number | null, request_origin?: string | null, is_parking_banned?: boolean | null, note?: string | null, scheduled_date: any, realization_date?: any | null, intervention_partner?: { __typename?: 'intervention_partner', name: string, id: any } | null, intervention_type: { __typename?: 'intervention_type', slug: string, family_intervention_type?: { __typename?: 'family_intervention_type', slug: string } | null }, worksite?: { __typename?: 'worksite', id: any, name: string } | null, location?: { __typename?: 'location', address?: string | null, location_status: { __typename?: 'location_status', status: string } } | null }> };

export type FetchTaxaSearchResultsQueryVariables = Exact<{
  query?: InputMaybe<Scalars['String']>;
}>;


export type FetchTaxaSearchResultsQuery = { __typename?: 'query_root', taxaSearch?: { __typename?: 'searchTaxaOutput', hits?: Array<{ __typename?: 'TaxonHit', id: number, scientific_name?: string | null, vernacular_name?: string | null, canonical_name?: string | null, family?: string | null } | null> | null } | null };

export type FetchAllWorksitesQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchAllWorksitesQuery = { __typename?: 'query_root', worksite: Array<{ __typename?: 'worksite', id: any, name: string, description?: string | null, scheduled_start_date?: any | null, scheduled_end_date?: any | null, realization_date?: any | null, interventions: Array<{ __typename?: 'intervention', scheduled_date: any, realization_date?: any | null, intervention_type: { __typename?: 'intervention_type', slug: string }, intervention_partner?: { __typename?: 'intervention_partner', name: string } | null, location?: { __typename?: 'location', id: any, address?: string | null, tree: Array<{ __typename?: 'tree', scientific_name?: string | null, vernacular_name?: string | null }> } | null }> }> };

export type CountAllTreesQueryVariables = Exact<{
  _lt?: InputMaybe<Scalars['String']>;
  _lte?: InputMaybe<Scalars['String']>;
  _gte?: InputMaybe<Scalars['String']>;
  _felledBefore?: InputMaybe<Scalars['date']>;
  _felledAfter?: InputMaybe<Scalars['date']>;
  where?: Tree_Bool_Exp;
}>;


export type CountAllTreesQuery = { __typename?: 'query_root', allTrees: { __typename?: 'tree_aggregate', aggregate?: { __typename?: 'tree_aggregate_fields', count: number } | null }, plantedTrees: { __typename?: 'tree_aggregate', aggregate?: { __typename?: 'tree_aggregate_fields', count: number } | null }, felledTrees: { __typename?: 'tree_aggregate', aggregate?: { __typename?: 'tree_aggregate_fields', count: number } | null } };

export type CountAllInterventionsQueryVariables = Exact<{
  _lte?: InputMaybe<Scalars['date']>;
  _gte?: InputMaybe<Scalars['date']>;
  where?: Intervention_Bool_Exp;
}>;


export type CountAllInterventionsQuery = { __typename?: 'query_root', treatment: { __typename?: 'intervention_aggregate', aggregate?: { __typename?: 'intervention_aggregate_fields', count: number } | null, nodes: Array<{ __typename?: 'intervention', scheduled_date: any }> }, watering: { __typename?: 'intervention_aggregate', aggregate?: { __typename?: 'intervention_aggregate_fields', count: number } | null, nodes: Array<{ __typename?: 'intervention', scheduled_date: any }> }, planting: { __typename?: 'intervention_aggregate', aggregate?: { __typename?: 'intervention_aggregate_fields', count: number } | null, nodes: Array<{ __typename?: 'intervention', scheduled_date: any }> }, pruning: { __typename?: 'intervention_aggregate', aggregate?: { __typename?: 'intervention_aggregate_fields', count: number } | null, nodes: Array<{ __typename?: 'intervention', scheduled_date: any }> } };

export type FetchTreesAggByScientificNamesQueryVariables = Exact<{
  where?: Tree_Scientific_Names_Count_Bool_Exp;
}>;


export type FetchTreesAggByScientificNamesQuery = { __typename?: 'query_root', tree_scientific_names_count: Array<{ __typename?: 'tree_scientific_names_count', scientific_name?: string | null, count?: any | null }> };

export type FetchAllBoundariesQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchAllBoundariesQuery = { __typename?: 'query_root', boundary: Array<{ __typename?: 'boundary', id: any, name: string, type: string }> };

export type FetchBoundariesExceptStationsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchBoundariesExceptStationsQuery = { __typename?: 'query_root', boundary: Array<{ __typename?: 'boundary', id: any, name: string, type: string }> };

export type FetchOneWorksiteQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type FetchOneWorksiteQuery = { __typename?: 'query_root', worksite?: { __typename?: 'worksite', id: any, description?: string | null, creation_date: string, name: string, scheduled_start_date?: any | null, scheduled_end_date?: any | null, realization_date?: any | null, cost?: number | null, realization_report?: string | null, user_entity?: { __typename?: 'user_entity', username?: string | null } | null, interventions: Array<{ __typename?: 'intervention', id: any, realization_date?: any | null, scheduled_date: any, cost?: number | null, intervention_type: { __typename?: 'intervention_type', slug: string }, intervention_partner?: { __typename?: 'intervention_partner', name: string } | null, tree?: { __typename?: 'tree', id: any, felling_date?: any | null, scientific_name?: string | null } | null }> } | null };

export type FetchOneWorksiteLocationsQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type FetchOneWorksiteLocationsQuery = { __typename?: 'query_root', worksite?: { __typename?: 'worksite', id: any, interventions: Array<{ __typename?: 'intervention', id: any, realization_date?: any | null, scheduled_date: any, data_entry_user_id: string, intervention_type: { __typename?: 'intervention_type', slug: string, id: any }, intervention_partner?: { __typename?: 'intervention_partner', id: any, name: string } | null, location?: { __typename?: 'location', coords: any, address?: string | null, id: any, location_status: { __typename?: 'location_status', status: string } } | null, tree?: { __typename?: 'tree', id: any, scientific_name?: string | null, felling_date?: any | null, location: { __typename?: 'location', address?: string | null, coords: any, id: any, location_status: { __typename?: 'location_status', status: string } } } | null }> } | null };

export type FetchOneWorksiteInterventionsQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type FetchOneWorksiteInterventionsQuery = { __typename?: 'query_root', worksite?: { __typename?: 'worksite', id: any, interventions: Array<{ __typename?: 'intervention', id: any, realization_date?: any | null, scheduled_date: any, data_entry_user_id: string, intervention_type: { __typename?: 'intervention_type', slug: string, id: any, location_types?: any | null }, intervention_partner?: { __typename?: 'intervention_partner', id: any, name: string } | null, location?: { __typename?: 'location', coords: any, address?: string | null, id: any, location_status: { __typename?: 'location_status', status: string } } | null, tree?: { __typename?: 'tree', id: any, scientific_name?: string | null, felling_date?: any | null, location: { __typename?: 'location', address?: string | null, coords: any, id: any, location_status: { __typename?: 'location_status', status: string } } } | null }> } | null };

export type FetchOneWorksiteLocationsAllQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type FetchOneWorksiteLocationsAllQuery = { __typename?: 'query_root', worksite?: { __typename?: 'worksite', id: any, interventions: Array<{ __typename?: 'intervention', id: any, realization_date?: any | null, scheduled_date: any, data_entry_user_id: string, intervention_type: { __typename?: 'intervention_type', slug: string, id: any, location_types?: any | null }, intervention_partner?: { __typename?: 'intervention_partner', id: any, name: string } | null, location?: { __typename?: 'location', coords: any, address?: string | null, id: any, location_status: { __typename?: 'location_status', status: string } } | null, tree?: { __typename?: 'tree', id: any, scientific_name?: string | null, felling_date?: any | null, location: { __typename?: 'location', address?: string | null, coords: any, id: any, location_status: { __typename?: 'location_status', status: string } } } | null }> } | null };

export type GetUrbasenseSubjectMonitoringQueryVariables = Exact<{
  subjectId: Scalars['Int'];
}>;


export type GetUrbasenseSubjectMonitoringQuery = { __typename?: 'query_root', urbasenseGetSubjectMonitoring?: { __typename?: 'MainOutput', tensio?: Array<{ __typename?: 'SubjectTensio', Date?: string | null, series?: string | null, series_name?: string | null, unite?: string | null, value?: number | null } | null> | null } | null };

export type GetUrbasenseSubjectMonitoringComboQueryVariables = Exact<{
  subjectId: Scalars['Int'];
}>;


export type GetUrbasenseSubjectMonitoringComboQuery = { __typename?: 'query_root', urbasenseGetSubjectMonitoringCombo?: { __typename?: 'ComboOutput', data?: Array<{ __typename?: 'Data', date_mes?: string | null, S_t?: number | null, S_1?: number | null, S_2?: number | null, S_3?: number | null, pp_mmjour?: string | null, tair_max?: string | null } | null> | null, metadata?: Array<{ __typename?: 'Metadata', id?: string | null, alias?: string | null, unite?: string | null } | null> | null } | null };

export type GetUrbasenseSubjectIdsByBoundaryIdsQueryVariables = Exact<{
  _in?: InputMaybe<Array<Scalars['uuid']> | Scalars['uuid']>;
}>;


export type GetUrbasenseSubjectIdsByBoundaryIdsQuery = { __typename?: 'query_root', tree: Array<{ __typename?: 'tree', urbasense_subject_id?: string | null }> };

export type FetchLocationFormDataQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchLocationFormDataQuery = { __typename?: 'query_root', urban_site: Array<{ __typename?: 'urban_site', slug: string, id: any }>, urban_constraint: Array<{ __typename?: 'urban_constraint', slug: string, id: any }>, foot_type: Array<{ __typename?: 'foot_type', slug: string, id: any }> };

export type FetchVegetatedAreaFormDataQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchVegetatedAreaFormDataQuery = { __typename?: 'query_root', boundary: Array<{ __typename?: 'boundary', id: any, name: string, type: string }>, urban_site: Array<{ __typename?: 'urban_site', slug: string, id: any }>, urban_constraint: Array<{ __typename?: 'urban_constraint', slug: string, id: any }>, residential_usage_type: Array<{ __typename?: 'residential_usage_type', slug: string, id: any }> };

export type GetLocationsInsidePolygonQueryVariables = Exact<{
  polygon: Scalars['geometry'];
}>;


export type GetLocationsInsidePolygonQuery = { __typename?: 'query_root', location: Array<{ __typename?: 'location', id: any }>, vegetated_area: Array<{ __typename?: 'vegetated_area', id: any }> };

export type GetBoundariesWithCoordsQueryVariables = Exact<{
  coords: Scalars['geometry'];
}>;


export type GetBoundariesWithCoordsQuery = { __typename?: 'query_root', boundary: Array<{ __typename?: 'boundary', id: any, name: string, type: string }> };

export type GetVegetatedAreaWithCoordsQueryVariables = Exact<{
  coords: Scalars['geometry'];
}>;


export type GetVegetatedAreaWithCoordsQuery = { __typename?: 'query_root', vegetated_area: Array<{ __typename?: 'vegetated_area', id: any }> };

export type FetchOneBoundaryQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type FetchOneBoundaryQuery = { __typename?: 'query_root', boundary?: { __typename?: 'boundary', id: any, name: string, coords?: any | null, type: string } | null, boundaries_locations: Array<{ __typename?: 'boundaries_locations', id: any, location_id: any }>, boundaries_vegetated_areas: Array<{ __typename?: 'boundaries_vegetated_areas', id: any, vegetated_area_id: any }> };

export type FetchOneVegetatedAreaQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type FetchOneVegetatedAreaQuery = { __typename?: 'query_root', vegetated_area?: { __typename?: 'vegetated_area', id: any, coords: any, type: string, address?: string | null, surface: number, urban_site_id?: any | null, has_differentiated_mowing: boolean, is_accessible: boolean, inconvenience_risk?: string | null, note?: string | null, shrubs_data?: any | null, afforestation_trees_data?: any | null, herbaceous_data?: any | null, landscape_type?: string | null, hedge_type?: string | null, linear_meters?: number | null, potential_area_state?: string | null } | null, administrative_boundary: Array<{ __typename?: 'boundaries_vegetated_areas', id: any, boundary_id: any }>, geographic_boundary: Array<{ __typename?: 'boundaries_vegetated_areas', id: any, boundary_id: any }>, station_boundary: Array<{ __typename?: 'boundaries_vegetated_areas', id: any, boundary_id: any }>, urban_constraint: Array<{ __typename?: 'vegetated_areas_urban_constraints', urban_constraint: { __typename?: 'urban_constraint', id: any, slug: string } }>, residential_usage_type: Array<{ __typename?: 'vegetated_areas_residential_usage_types', residential_usage_types: { __typename?: 'residential_usage_type', id: any, slug: string } }>, locations: Array<{ __typename?: 'vegetated_areas_locations', location_id: any, location: { __typename?: 'location', location_status: { __typename?: 'location_status', status: string }, tree_aggregate: { __typename?: 'tree_aggregate', aggregate?: { __typename?: 'tree_aggregate_fields', count: number } | null, nodes: Array<{ __typename?: 'tree', height?: number | null, scientific_name?: string | null, id: any }> } } }> };

export type GetStationBoundaryVegetatedAreaQueryVariables = Exact<{
  vegetated_area_id?: InputMaybe<Scalars['uuid']>;
}>;


export type GetStationBoundaryVegetatedAreaQuery = { __typename?: 'query_root', boundaries_vegetated_areas: Array<{ __typename?: 'boundaries_vegetated_areas', boundary: { __typename?: 'boundary', id: any, name: string, type: string } }> };

export type GetVegetatedAreasFromStationBoundaryQueryVariables = Exact<{
  boundary_id?: InputMaybe<Scalars['uuid']>;
}>;


export type GetVegetatedAreasFromStationBoundaryQuery = { __typename?: 'query_root', vegetated_area_aggregate: { __typename?: 'vegetated_area_aggregate', aggregate?: { __typename?: 'vegetated_area_aggregate_fields', count: number, sum?: { __typename?: 'vegetated_area_sum_fields', surface?: number | null } | null } | null, nodes: Array<{ __typename?: 'vegetated_area', type: string, shrubs_data?: any | null, herbaceous_data?: any | null, afforestation_trees_data?: any | null }> } };

export type CountTreesFromBoundaryQueryVariables = Exact<{
  boundary_id?: InputMaybe<Scalars['uuid']>;
}>;


export type CountTreesFromBoundaryQuery = { __typename?: 'query_root', boundaries_locations_aggregate: { __typename?: 'boundaries_locations_aggregate', aggregate?: { __typename?: 'boundaries_locations_aggregate_fields', count: number } | null } };

export const WorksiteFieldsFragmentDoc = gql`
    fragment worksiteFields on worksite {
  id
  name
  cost
  scheduled_start_date
  scheduled_end_date
  realization_date
  description
}
    `;
export const CreateLocationDocument = gql`
    mutation createLocation($address: String = "address", $coords: geometry = "coords", $foot_type: String = "description", $plantation_type: String = "", $sidewalk_type: String = "", $urban_site_id: uuid = "", $landscape_type: String = "", $id_status: uuid = "is_status", $organization_id: uuid, $is_protected: Boolean = false, $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb") {
  insert_location_one(
    object: {address: $address, coords: $coords, foot_type: $foot_type, plantation_type: $plantation_type, urban_site_id: $urban_site_id, sidewalk_type: $sidewalk_type, landscape_type: $landscape_type, id_status: $id_status, organization_id: $organization_id, is_protected: $is_protected, data_entry_user_id: $data_entry_user_id}
  ) {
    address
    coords
    foot_type
    plantation_type
    urban_site_id
    sidewalk_type
    id
    status: location_status {
      id
      status
    }
  }
}
    `;
export type CreateLocationMutationFn = Apollo.MutationFunction<CreateLocationMutation, CreateLocationMutationVariables>;

/**
 * __useCreateLocationMutation__
 *
 * To run a mutation, you first call `useCreateLocationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateLocationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createLocationMutation, { data, loading, error }] = useCreateLocationMutation({
 *   variables: {
 *      address: // value for 'address'
 *      coords: // value for 'coords'
 *      foot_type: // value for 'foot_type'
 *      plantation_type: // value for 'plantation_type'
 *      sidewalk_type: // value for 'sidewalk_type'
 *      urban_site_id: // value for 'urban_site_id'
 *      landscape_type: // value for 'landscape_type'
 *      id_status: // value for 'id_status'
 *      organization_id: // value for 'organization_id'
 *      is_protected: // value for 'is_protected'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *   },
 * });
 */
export function useCreateLocationMutation(baseOptions?: Apollo.MutationHookOptions<CreateLocationMutation, CreateLocationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateLocationMutation, CreateLocationMutationVariables>(CreateLocationDocument, options);
      }
export type CreateLocationMutationHookResult = ReturnType<typeof useCreateLocationMutation>;
export type CreateLocationMutationResult = Apollo.MutationResult<CreateLocationMutation>;
export type CreateLocationMutationOptions = Apollo.BaseMutationOptions<CreateLocationMutation, CreateLocationMutationVariables>;
export const InsertLocationUrbanConstraintDocument = gql`
    mutation insertLocationUrbanConstraint($location_id: uuid = "", $urban_constraint_id: uuid = "") {
  insert_location_urban_constraint(
    objects: {location_id: $location_id, urban_constraint_id: $urban_constraint_id}
  ) {
    affected_rows
  }
}
    `;
export type InsertLocationUrbanConstraintMutationFn = Apollo.MutationFunction<InsertLocationUrbanConstraintMutation, InsertLocationUrbanConstraintMutationVariables>;

/**
 * __useInsertLocationUrbanConstraintMutation__
 *
 * To run a mutation, you first call `useInsertLocationUrbanConstraintMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInsertLocationUrbanConstraintMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [insertLocationUrbanConstraintMutation, { data, loading, error }] = useInsertLocationUrbanConstraintMutation({
 *   variables: {
 *      location_id: // value for 'location_id'
 *      urban_constraint_id: // value for 'urban_constraint_id'
 *   },
 * });
 */
export function useInsertLocationUrbanConstraintMutation(baseOptions?: Apollo.MutationHookOptions<InsertLocationUrbanConstraintMutation, InsertLocationUrbanConstraintMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InsertLocationUrbanConstraintMutation, InsertLocationUrbanConstraintMutationVariables>(InsertLocationUrbanConstraintDocument, options);
      }
export type InsertLocationUrbanConstraintMutationHookResult = ReturnType<typeof useInsertLocationUrbanConstraintMutation>;
export type InsertLocationUrbanConstraintMutationResult = Apollo.MutationResult<InsertLocationUrbanConstraintMutation>;
export type InsertLocationUrbanConstraintMutationOptions = Apollo.BaseMutationOptions<InsertLocationUrbanConstraintMutation, InsertLocationUrbanConstraintMutationVariables>;
export const CreateTreeDocument = gql`
    mutation createTree($watering: Boolean = false, $scientific_name: String = "", $vernacular_name: String = "", $plantation_date: String = "", $serial_number: String = "", $note: String = "", $location_id: uuid = "", $is_tree_of_interest: Boolean = false, $variety: String = "", $height: numeric = null, $estimated_date: Boolean = false, $habit: String = "", $circumference: numeric = null, $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb", $taxon_id: Int = null) {
  insert_tree_one(
    object: {circumference: $circumference, variety: $variety, height: $height, is_tree_of_interest: $is_tree_of_interest, location_id: $location_id, note: $note, plantation_date: $plantation_date, vernacular_name: $vernacular_name, scientific_name: $scientific_name, serial_number: $serial_number, estimated_date: $estimated_date, habit: $habit, watering: $watering, data_entry_user_id: $data_entry_user_id, taxon_id: $taxon_id}
  ) {
    id
    location_id
  }
}
    `;
export type CreateTreeMutationFn = Apollo.MutationFunction<CreateTreeMutation, CreateTreeMutationVariables>;

/**
 * __useCreateTreeMutation__
 *
 * To run a mutation, you first call `useCreateTreeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateTreeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createTreeMutation, { data, loading, error }] = useCreateTreeMutation({
 *   variables: {
 *      watering: // value for 'watering'
 *      scientific_name: // value for 'scientific_name'
 *      vernacular_name: // value for 'vernacular_name'
 *      plantation_date: // value for 'plantation_date'
 *      serial_number: // value for 'serial_number'
 *      note: // value for 'note'
 *      location_id: // value for 'location_id'
 *      is_tree_of_interest: // value for 'is_tree_of_interest'
 *      variety: // value for 'variety'
 *      height: // value for 'height'
 *      estimated_date: // value for 'estimated_date'
 *      habit: // value for 'habit'
 *      circumference: // value for 'circumference'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *      taxon_id: // value for 'taxon_id'
 *   },
 * });
 */
export function useCreateTreeMutation(baseOptions?: Apollo.MutationHookOptions<CreateTreeMutation, CreateTreeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateTreeMutation, CreateTreeMutationVariables>(CreateTreeDocument, options);
      }
export type CreateTreeMutationHookResult = ReturnType<typeof useCreateTreeMutation>;
export type CreateTreeMutationResult = Apollo.MutationResult<CreateTreeMutation>;
export type CreateTreeMutationOptions = Apollo.BaseMutationOptions<CreateTreeMutation, CreateTreeMutationVariables>;
export const UpdateLocationDocument = gql`
    mutation updateLocation($locationId: uuid = "", $address: String = "", $coords: geometry = "", $foot_type: String = "", $plantation_type: String = "", $is_protected: Boolean = false, $urban_site_id: uuid = "", $landscape_type: String = "", $sidewalk_type: String = "", $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb", $inventory_source: jsonb = "") {
  update_location_by_pk(
    pk_columns: {id: $locationId}
    _set: {address: $address, coords: $coords, foot_type: $foot_type, plantation_type: $plantation_type, urban_site_id: $urban_site_id, sidewalk_type: $sidewalk_type, landscape_type: $landscape_type, is_protected: $is_protected, data_entry_user_id: $data_entry_user_id, inventory_source: $inventory_source}
  ) {
    id
    status: location_status {
      id
      status
    }
  }
}
    `;
export type UpdateLocationMutationFn = Apollo.MutationFunction<UpdateLocationMutation, UpdateLocationMutationVariables>;

/**
 * __useUpdateLocationMutation__
 *
 * To run a mutation, you first call `useUpdateLocationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateLocationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateLocationMutation, { data, loading, error }] = useUpdateLocationMutation({
 *   variables: {
 *      locationId: // value for 'locationId'
 *      address: // value for 'address'
 *      coords: // value for 'coords'
 *      foot_type: // value for 'foot_type'
 *      plantation_type: // value for 'plantation_type'
 *      is_protected: // value for 'is_protected'
 *      urban_site_id: // value for 'urban_site_id'
 *      landscape_type: // value for 'landscape_type'
 *      sidewalk_type: // value for 'sidewalk_type'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *      inventory_source: // value for 'inventory_source'
 *   },
 * });
 */
export function useUpdateLocationMutation(baseOptions?: Apollo.MutationHookOptions<UpdateLocationMutation, UpdateLocationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateLocationMutation, UpdateLocationMutationVariables>(UpdateLocationDocument, options);
      }
export type UpdateLocationMutationHookResult = ReturnType<typeof useUpdateLocationMutation>;
export type UpdateLocationMutationResult = Apollo.MutationResult<UpdateLocationMutation>;
export type UpdateLocationMutationOptions = Apollo.BaseMutationOptions<UpdateLocationMutation, UpdateLocationMutationVariables>;
export const DeleteLocationBoundariesDocument = gql`
    mutation deleteLocationBoundaries($location_id: uuid = "") {
  delete_boundaries_locations(where: {location_id: {_eq: $location_id}}) {
    affected_rows
  }
}
    `;
export type DeleteLocationBoundariesMutationFn = Apollo.MutationFunction<DeleteLocationBoundariesMutation, DeleteLocationBoundariesMutationVariables>;

/**
 * __useDeleteLocationBoundariesMutation__
 *
 * To run a mutation, you first call `useDeleteLocationBoundariesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteLocationBoundariesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteLocationBoundariesMutation, { data, loading, error }] = useDeleteLocationBoundariesMutation({
 *   variables: {
 *      location_id: // value for 'location_id'
 *   },
 * });
 */
export function useDeleteLocationBoundariesMutation(baseOptions?: Apollo.MutationHookOptions<DeleteLocationBoundariesMutation, DeleteLocationBoundariesMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteLocationBoundariesMutation, DeleteLocationBoundariesMutationVariables>(DeleteLocationBoundariesDocument, options);
      }
export type DeleteLocationBoundariesMutationHookResult = ReturnType<typeof useDeleteLocationBoundariesMutation>;
export type DeleteLocationBoundariesMutationResult = Apollo.MutationResult<DeleteLocationBoundariesMutation>;
export type DeleteLocationBoundariesMutationOptions = Apollo.BaseMutationOptions<DeleteLocationBoundariesMutation, DeleteLocationBoundariesMutationVariables>;
export const DeleteLocationUrbanConstraintsDocument = gql`
    mutation deleteLocationUrbanConstraints($location_id: uuid = "") {
  delete_location_urban_constraint(where: {location_id: {_eq: $location_id}}) {
    affected_rows
  }
}
    `;
export type DeleteLocationUrbanConstraintsMutationFn = Apollo.MutationFunction<DeleteLocationUrbanConstraintsMutation, DeleteLocationUrbanConstraintsMutationVariables>;

/**
 * __useDeleteLocationUrbanConstraintsMutation__
 *
 * To run a mutation, you first call `useDeleteLocationUrbanConstraintsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteLocationUrbanConstraintsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteLocationUrbanConstraintsMutation, { data, loading, error }] = useDeleteLocationUrbanConstraintsMutation({
 *   variables: {
 *      location_id: // value for 'location_id'
 *   },
 * });
 */
export function useDeleteLocationUrbanConstraintsMutation(baseOptions?: Apollo.MutationHookOptions<DeleteLocationUrbanConstraintsMutation, DeleteLocationUrbanConstraintsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteLocationUrbanConstraintsMutation, DeleteLocationUrbanConstraintsMutationVariables>(DeleteLocationUrbanConstraintsDocument, options);
      }
export type DeleteLocationUrbanConstraintsMutationHookResult = ReturnType<typeof useDeleteLocationUrbanConstraintsMutation>;
export type DeleteLocationUrbanConstraintsMutationResult = Apollo.MutationResult<DeleteLocationUrbanConstraintsMutation>;
export type DeleteLocationUrbanConstraintsMutationOptions = Apollo.BaseMutationOptions<DeleteLocationUrbanConstraintsMutation, DeleteLocationUrbanConstraintsMutationVariables>;
export const InsertLocationBoundariesDocument = gql`
    mutation insertLocationBoundaries($boundary_id: uuid = "", $location_id: uuid = "") {
  insert_boundaries_locations_one(
    object: {boundary_id: $boundary_id, location_id: $location_id}
  ) {
    id
  }
}
    `;
export type InsertLocationBoundariesMutationFn = Apollo.MutationFunction<InsertLocationBoundariesMutation, InsertLocationBoundariesMutationVariables>;

/**
 * __useInsertLocationBoundariesMutation__
 *
 * To run a mutation, you first call `useInsertLocationBoundariesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInsertLocationBoundariesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [insertLocationBoundariesMutation, { data, loading, error }] = useInsertLocationBoundariesMutation({
 *   variables: {
 *      boundary_id: // value for 'boundary_id'
 *      location_id: // value for 'location_id'
 *   },
 * });
 */
export function useInsertLocationBoundariesMutation(baseOptions?: Apollo.MutationHookOptions<InsertLocationBoundariesMutation, InsertLocationBoundariesMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InsertLocationBoundariesMutation, InsertLocationBoundariesMutationVariables>(InsertLocationBoundariesDocument, options);
      }
export type InsertLocationBoundariesMutationHookResult = ReturnType<typeof useInsertLocationBoundariesMutation>;
export type InsertLocationBoundariesMutationResult = Apollo.MutationResult<InsertLocationBoundariesMutation>;
export type InsertLocationBoundariesMutationOptions = Apollo.BaseMutationOptions<InsertLocationBoundariesMutation, InsertLocationBoundariesMutationVariables>;
export const InsertVegetatedAreaBoundariesDocument = gql`
    mutation insertVegetatedAreaBoundaries($boundary_id: uuid = "", $vegetated_area_id: uuid = "") {
  insert_boundaries_vegetated_areas_one(
    object: {boundary_id: $boundary_id, vegetated_area_id: $vegetated_area_id}
  ) {
    id
  }
}
    `;
export type InsertVegetatedAreaBoundariesMutationFn = Apollo.MutationFunction<InsertVegetatedAreaBoundariesMutation, InsertVegetatedAreaBoundariesMutationVariables>;

/**
 * __useInsertVegetatedAreaBoundariesMutation__
 *
 * To run a mutation, you first call `useInsertVegetatedAreaBoundariesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInsertVegetatedAreaBoundariesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [insertVegetatedAreaBoundariesMutation, { data, loading, error }] = useInsertVegetatedAreaBoundariesMutation({
 *   variables: {
 *      boundary_id: // value for 'boundary_id'
 *      vegetated_area_id: // value for 'vegetated_area_id'
 *   },
 * });
 */
export function useInsertVegetatedAreaBoundariesMutation(baseOptions?: Apollo.MutationHookOptions<InsertVegetatedAreaBoundariesMutation, InsertVegetatedAreaBoundariesMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InsertVegetatedAreaBoundariesMutation, InsertVegetatedAreaBoundariesMutationVariables>(InsertVegetatedAreaBoundariesDocument, options);
      }
export type InsertVegetatedAreaBoundariesMutationHookResult = ReturnType<typeof useInsertVegetatedAreaBoundariesMutation>;
export type InsertVegetatedAreaBoundariesMutationResult = Apollo.MutationResult<InsertVegetatedAreaBoundariesMutation>;
export type InsertVegetatedAreaBoundariesMutationOptions = Apollo.BaseMutationOptions<InsertVegetatedAreaBoundariesMutation, InsertVegetatedAreaBoundariesMutationVariables>;
export const InsertLocationVegetatedAreaDocument = gql`
    mutation insertLocationVegetatedArea($vegetated_area_id: uuid = "", $location_id: uuid = "") {
  insert_vegetated_areas_locations_one(
    object: {vegetated_area_id: $vegetated_area_id, location_id: $location_id}
  ) {
    id
  }
}
    `;
export type InsertLocationVegetatedAreaMutationFn = Apollo.MutationFunction<InsertLocationVegetatedAreaMutation, InsertLocationVegetatedAreaMutationVariables>;

/**
 * __useInsertLocationVegetatedAreaMutation__
 *
 * To run a mutation, you first call `useInsertLocationVegetatedAreaMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInsertLocationVegetatedAreaMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [insertLocationVegetatedAreaMutation, { data, loading, error }] = useInsertLocationVegetatedAreaMutation({
 *   variables: {
 *      vegetated_area_id: // value for 'vegetated_area_id'
 *      location_id: // value for 'location_id'
 *   },
 * });
 */
export function useInsertLocationVegetatedAreaMutation(baseOptions?: Apollo.MutationHookOptions<InsertLocationVegetatedAreaMutation, InsertLocationVegetatedAreaMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InsertLocationVegetatedAreaMutation, InsertLocationVegetatedAreaMutationVariables>(InsertLocationVegetatedAreaDocument, options);
      }
export type InsertLocationVegetatedAreaMutationHookResult = ReturnType<typeof useInsertLocationVegetatedAreaMutation>;
export type InsertLocationVegetatedAreaMutationResult = Apollo.MutationResult<InsertLocationVegetatedAreaMutation>;
export type InsertLocationVegetatedAreaMutationOptions = Apollo.BaseMutationOptions<InsertLocationVegetatedAreaMutation, InsertLocationVegetatedAreaMutationVariables>;
export const UpdateTreeDocument = gql`
    mutation updateTree($treeId: uuid = "", $circumference: numeric = null, $variety: String = "", $height: numeric = null, $is_tree_of_interest: Boolean = false, $note: String = "", $plantation_date: String = "", $vernacular_name: String = "", $scientific_name: String = "", $serial_number: String = "", $estimated_date: Boolean = false, $habit: String = "", $watering: Boolean = false, $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb", $taxon_id: Int = null, $felling_date: date = null, $address: String = "", $coords: geometry = "") {
  update_tree_by_pk(
    pk_columns: {id: $treeId}
    _set: {circumference: $circumference, variety: $variety, height: $height, is_tree_of_interest: $is_tree_of_interest, note: $note, plantation_date: $plantation_date, vernacular_name: $vernacular_name, scientific_name: $scientific_name, estimated_date: $estimated_date, serial_number: $serial_number, habit: $habit, watering: $watering, data_entry_user_id: $data_entry_user_id, taxon_id: $taxon_id, felling_date: $felling_date}
  ) {
    id
    location_id
    taxon_id
    location {
      location_status {
        status
      }
    }
  }
}
    `;
export type UpdateTreeMutationFn = Apollo.MutationFunction<UpdateTreeMutation, UpdateTreeMutationVariables>;

/**
 * __useUpdateTreeMutation__
 *
 * To run a mutation, you first call `useUpdateTreeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTreeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTreeMutation, { data, loading, error }] = useUpdateTreeMutation({
 *   variables: {
 *      treeId: // value for 'treeId'
 *      circumference: // value for 'circumference'
 *      variety: // value for 'variety'
 *      height: // value for 'height'
 *      is_tree_of_interest: // value for 'is_tree_of_interest'
 *      note: // value for 'note'
 *      plantation_date: // value for 'plantation_date'
 *      vernacular_name: // value for 'vernacular_name'
 *      scientific_name: // value for 'scientific_name'
 *      serial_number: // value for 'serial_number'
 *      estimated_date: // value for 'estimated_date'
 *      habit: // value for 'habit'
 *      watering: // value for 'watering'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *      taxon_id: // value for 'taxon_id'
 *      felling_date: // value for 'felling_date'
 *      address: // value for 'address'
 *      coords: // value for 'coords'
 *   },
 * });
 */
export function useUpdateTreeMutation(baseOptions?: Apollo.MutationHookOptions<UpdateTreeMutation, UpdateTreeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateTreeMutation, UpdateTreeMutationVariables>(UpdateTreeDocument, options);
      }
export type UpdateTreeMutationHookResult = ReturnType<typeof useUpdateTreeMutation>;
export type UpdateTreeMutationResult = Apollo.MutationResult<UpdateTreeMutation>;
export type UpdateTreeMutationOptions = Apollo.BaseMutationOptions<UpdateTreeMutation, UpdateTreeMutationVariables>;
export const UpdateTreeWithFellingDateDocument = gql`
    mutation updateTreeWithFellingDate($treeId: uuid = "", $felling_date: date = null) {
  update_tree_by_pk(
    pk_columns: {id: $treeId}
    _set: {felling_date: $felling_date}
  ) {
    id
    location_id
  }
}
    `;
export type UpdateTreeWithFellingDateMutationFn = Apollo.MutationFunction<UpdateTreeWithFellingDateMutation, UpdateTreeWithFellingDateMutationVariables>;

/**
 * __useUpdateTreeWithFellingDateMutation__
 *
 * To run a mutation, you first call `useUpdateTreeWithFellingDateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTreeWithFellingDateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTreeWithFellingDateMutation, { data, loading, error }] = useUpdateTreeWithFellingDateMutation({
 *   variables: {
 *      treeId: // value for 'treeId'
 *      felling_date: // value for 'felling_date'
 *   },
 * });
 */
export function useUpdateTreeWithFellingDateMutation(baseOptions?: Apollo.MutationHookOptions<UpdateTreeWithFellingDateMutation, UpdateTreeWithFellingDateMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateTreeWithFellingDateMutation, UpdateTreeWithFellingDateMutationVariables>(UpdateTreeWithFellingDateDocument, options);
      }
export type UpdateTreeWithFellingDateMutationHookResult = ReturnType<typeof useUpdateTreeWithFellingDateMutation>;
export type UpdateTreeWithFellingDateMutationResult = Apollo.MutationResult<UpdateTreeWithFellingDateMutation>;
export type UpdateTreeWithFellingDateMutationOptions = Apollo.BaseMutationOptions<UpdateTreeWithFellingDateMutation, UpdateTreeWithFellingDateMutationVariables>;
export const UpdateTreeAndLocationDocument = gql`
    mutation updateTreeAndLocation($treeId: uuid = "", $circumference: numeric = null, $variety: String = "", $height: numeric = null, $is_tree_of_interest: Boolean = false, $note: String = "", $plantation_date: String = "", $vernacular_name: String = "", $scientific_name: String = "", $serial_number: String = "", $estimated_date: Boolean = false, $habit: String = "", $watering: Boolean = false, $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb", $taxon_id: Int = null, $urbasense_subject_id: String = null, $felling_date: date = null, $_setLocation: location_set_input = {}) {
  update_tree_by_pk(
    pk_columns: {id: $treeId}
    _set: {circumference: $circumference, variety: $variety, height: $height, is_tree_of_interest: $is_tree_of_interest, note: $note, plantation_date: $plantation_date, vernacular_name: $vernacular_name, scientific_name: $scientific_name, estimated_date: $estimated_date, serial_number: $serial_number, habit: $habit, watering: $watering, data_entry_user_id: $data_entry_user_id, taxon_id: $taxon_id, felling_date: $felling_date, urbasense_subject_id: $urbasense_subject_id}
  ) {
    id
    location_id
    taxon_id
    location {
      location_status {
        status
      }
    }
  }
  update_location(where: {tree: {id: {_eq: $treeId}}}, _set: $_setLocation) {
    returning {
      id
    }
  }
}
    `;
export type UpdateTreeAndLocationMutationFn = Apollo.MutationFunction<UpdateTreeAndLocationMutation, UpdateTreeAndLocationMutationVariables>;

/**
 * __useUpdateTreeAndLocationMutation__
 *
 * To run a mutation, you first call `useUpdateTreeAndLocationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTreeAndLocationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTreeAndLocationMutation, { data, loading, error }] = useUpdateTreeAndLocationMutation({
 *   variables: {
 *      treeId: // value for 'treeId'
 *      circumference: // value for 'circumference'
 *      variety: // value for 'variety'
 *      height: // value for 'height'
 *      is_tree_of_interest: // value for 'is_tree_of_interest'
 *      note: // value for 'note'
 *      plantation_date: // value for 'plantation_date'
 *      vernacular_name: // value for 'vernacular_name'
 *      scientific_name: // value for 'scientific_name'
 *      serial_number: // value for 'serial_number'
 *      estimated_date: // value for 'estimated_date'
 *      habit: // value for 'habit'
 *      watering: // value for 'watering'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *      taxon_id: // value for 'taxon_id'
 *      urbasense_subject_id: // value for 'urbasense_subject_id'
 *      felling_date: // value for 'felling_date'
 *      _setLocation: // value for '_setLocation'
 *   },
 * });
 */
export function useUpdateTreeAndLocationMutation(baseOptions?: Apollo.MutationHookOptions<UpdateTreeAndLocationMutation, UpdateTreeAndLocationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateTreeAndLocationMutation, UpdateTreeAndLocationMutationVariables>(UpdateTreeAndLocationDocument, options);
      }
export type UpdateTreeAndLocationMutationHookResult = ReturnType<typeof useUpdateTreeAndLocationMutation>;
export type UpdateTreeAndLocationMutationResult = Apollo.MutationResult<UpdateTreeAndLocationMutation>;
export type UpdateTreeAndLocationMutationOptions = Apollo.BaseMutationOptions<UpdateTreeAndLocationMutation, UpdateTreeAndLocationMutationVariables>;
export const UpdateTreesWithFellingDateDocument = gql`
    mutation updateTreesWithFellingDate($_in: [uuid!] = "", $felling_date: date = null) {
  update_tree(
    where: {location_id: {_in: $_in}}
    _set: {felling_date: $felling_date}
  ) {
    returning {
      id
      location_id
    }
  }
}
    `;
export type UpdateTreesWithFellingDateMutationFn = Apollo.MutationFunction<UpdateTreesWithFellingDateMutation, UpdateTreesWithFellingDateMutationVariables>;

/**
 * __useUpdateTreesWithFellingDateMutation__
 *
 * To run a mutation, you first call `useUpdateTreesWithFellingDateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTreesWithFellingDateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTreesWithFellingDateMutation, { data, loading, error }] = useUpdateTreesWithFellingDateMutation({
 *   variables: {
 *      _in: // value for '_in'
 *      felling_date: // value for 'felling_date'
 *   },
 * });
 */
export function useUpdateTreesWithFellingDateMutation(baseOptions?: Apollo.MutationHookOptions<UpdateTreesWithFellingDateMutation, UpdateTreesWithFellingDateMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateTreesWithFellingDateMutation, UpdateTreesWithFellingDateMutationVariables>(UpdateTreesWithFellingDateDocument, options);
      }
export type UpdateTreesWithFellingDateMutationHookResult = ReturnType<typeof useUpdateTreesWithFellingDateMutation>;
export type UpdateTreesWithFellingDateMutationResult = Apollo.MutationResult<UpdateTreesWithFellingDateMutation>;
export type UpdateTreesWithFellingDateMutationOptions = Apollo.BaseMutationOptions<UpdateTreesWithFellingDateMutation, UpdateTreesWithFellingDateMutationVariables>;
export const UpdateTreesDocument = gql`
    mutation updateTrees($_in: [uuid!] = "", $circumference: numeric = null, $variety: String = "", $height: numeric = null, $is_tree_of_interest: Boolean = false, $note: String = "", $plantation_date: String = "", $vernacular_name: String = "", $scientific_name: String = "", $serial_number: String = "", $estimated_date: Boolean = false, $habit: String = "", $watering: Boolean = false, $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb", $taxon_id: Int = null, $felling_date: date = null) {
  update_tree(
    where: {location_id: {_in: $_in}}
    _set: {circumference: $circumference, variety: $variety, height: $height, is_tree_of_interest: $is_tree_of_interest, note: $note, plantation_date: $plantation_date, vernacular_name: $vernacular_name, scientific_name: $scientific_name, estimated_date: $estimated_date, serial_number: $serial_number, habit: $habit, watering: $watering, data_entry_user_id: $data_entry_user_id, taxon_id: $taxon_id, felling_date: $felling_date}
  ) {
    returning {
      id
      location_id
      taxon_id
      location {
        location_status {
          status
        }
      }
    }
  }
}
    `;
export type UpdateTreesMutationFn = Apollo.MutationFunction<UpdateTreesMutation, UpdateTreesMutationVariables>;

/**
 * __useUpdateTreesMutation__
 *
 * To run a mutation, you first call `useUpdateTreesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTreesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTreesMutation, { data, loading, error }] = useUpdateTreesMutation({
 *   variables: {
 *      _in: // value for '_in'
 *      circumference: // value for 'circumference'
 *      variety: // value for 'variety'
 *      height: // value for 'height'
 *      is_tree_of_interest: // value for 'is_tree_of_interest'
 *      note: // value for 'note'
 *      plantation_date: // value for 'plantation_date'
 *      vernacular_name: // value for 'vernacular_name'
 *      scientific_name: // value for 'scientific_name'
 *      serial_number: // value for 'serial_number'
 *      estimated_date: // value for 'estimated_date'
 *      habit: // value for 'habit'
 *      watering: // value for 'watering'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *      taxon_id: // value for 'taxon_id'
 *      felling_date: // value for 'felling_date'
 *   },
 * });
 */
export function useUpdateTreesMutation(baseOptions?: Apollo.MutationHookOptions<UpdateTreesMutation, UpdateTreesMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateTreesMutation, UpdateTreesMutationVariables>(UpdateTreesDocument, options);
      }
export type UpdateTreesMutationHookResult = ReturnType<typeof useUpdateTreesMutation>;
export type UpdateTreesMutationResult = Apollo.MutationResult<UpdateTreesMutation>;
export type UpdateTreesMutationOptions = Apollo.BaseMutationOptions<UpdateTreesMutation, UpdateTreesMutationVariables>;
export const DeleteTreeDocument = gql`
    mutation deleteTree($id: uuid = "") {
  delete_tree_by_pk(id: $id) {
    id
    location_id
  }
}
    `;
export type DeleteTreeMutationFn = Apollo.MutationFunction<DeleteTreeMutation, DeleteTreeMutationVariables>;

/**
 * __useDeleteTreeMutation__
 *
 * To run a mutation, you first call `useDeleteTreeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteTreeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteTreeMutation, { data, loading, error }] = useDeleteTreeMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteTreeMutation(baseOptions?: Apollo.MutationHookOptions<DeleteTreeMutation, DeleteTreeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteTreeMutation, DeleteTreeMutationVariables>(DeleteTreeDocument, options);
      }
export type DeleteTreeMutationHookResult = ReturnType<typeof useDeleteTreeMutation>;
export type DeleteTreeMutationResult = Apollo.MutationResult<DeleteTreeMutation>;
export type DeleteTreeMutationOptions = Apollo.BaseMutationOptions<DeleteTreeMutation, DeleteTreeMutationVariables>;
export const DeleteLocationDocument = gql`
    mutation deleteLocation($id: uuid = "") {
  delete_location_by_pk(id: $id) {
    id
  }
}
    `;
export type DeleteLocationMutationFn = Apollo.MutationFunction<DeleteLocationMutation, DeleteLocationMutationVariables>;

/**
 * __useDeleteLocationMutation__
 *
 * To run a mutation, you first call `useDeleteLocationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteLocationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteLocationMutation, { data, loading, error }] = useDeleteLocationMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteLocationMutation(baseOptions?: Apollo.MutationHookOptions<DeleteLocationMutation, DeleteLocationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteLocationMutation, DeleteLocationMutationVariables>(DeleteLocationDocument, options);
      }
export type DeleteLocationMutationHookResult = ReturnType<typeof useDeleteLocationMutation>;
export type DeleteLocationMutationResult = Apollo.MutationResult<DeleteLocationMutation>;
export type DeleteLocationMutationOptions = Apollo.BaseMutationOptions<DeleteLocationMutation, DeleteLocationMutationVariables>;
export const DeleteWorksiteByPkDocument = gql`
    mutation deleteWorksiteByPk($id: uuid = "") {
  delete_worksite_by_pk(id: $id) {
    id
  }
}
    `;
export type DeleteWorksiteByPkMutationFn = Apollo.MutationFunction<DeleteWorksiteByPkMutation, DeleteWorksiteByPkMutationVariables>;

/**
 * __useDeleteWorksiteByPkMutation__
 *
 * To run a mutation, you first call `useDeleteWorksiteByPkMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteWorksiteByPkMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteWorksiteByPkMutation, { data, loading, error }] = useDeleteWorksiteByPkMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteWorksiteByPkMutation(baseOptions?: Apollo.MutationHookOptions<DeleteWorksiteByPkMutation, DeleteWorksiteByPkMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteWorksiteByPkMutation, DeleteWorksiteByPkMutationVariables>(DeleteWorksiteByPkDocument, options);
      }
export type DeleteWorksiteByPkMutationHookResult = ReturnType<typeof useDeleteWorksiteByPkMutation>;
export type DeleteWorksiteByPkMutationResult = Apollo.MutationResult<DeleteWorksiteByPkMutation>;
export type DeleteWorksiteByPkMutationOptions = Apollo.BaseMutationOptions<DeleteWorksiteByPkMutation, DeleteWorksiteByPkMutationVariables>;
export const UpdateLocationStatusWithTreeIdDocument = gql`
    mutation updateLocationStatusWithTreeId($_eq: uuid = "", $id_status: uuid = "") {
  update_location(where: {tree: {id: {_eq: $_eq}}}, _set: {id_status: $id_status}) {
    returning {
      id
    }
  }
}
    `;
export type UpdateLocationStatusWithTreeIdMutationFn = Apollo.MutationFunction<UpdateLocationStatusWithTreeIdMutation, UpdateLocationStatusWithTreeIdMutationVariables>;

/**
 * __useUpdateLocationStatusWithTreeIdMutation__
 *
 * To run a mutation, you first call `useUpdateLocationStatusWithTreeIdMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateLocationStatusWithTreeIdMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateLocationStatusWithTreeIdMutation, { data, loading, error }] = useUpdateLocationStatusWithTreeIdMutation({
 *   variables: {
 *      _eq: // value for '_eq'
 *      id_status: // value for 'id_status'
 *   },
 * });
 */
export function useUpdateLocationStatusWithTreeIdMutation(baseOptions?: Apollo.MutationHookOptions<UpdateLocationStatusWithTreeIdMutation, UpdateLocationStatusWithTreeIdMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateLocationStatusWithTreeIdMutation, UpdateLocationStatusWithTreeIdMutationVariables>(UpdateLocationStatusWithTreeIdDocument, options);
      }
export type UpdateLocationStatusWithTreeIdMutationHookResult = ReturnType<typeof useUpdateLocationStatusWithTreeIdMutation>;
export type UpdateLocationStatusWithTreeIdMutationResult = Apollo.MutationResult<UpdateLocationStatusWithTreeIdMutation>;
export type UpdateLocationStatusWithTreeIdMutationOptions = Apollo.BaseMutationOptions<UpdateLocationStatusWithTreeIdMutation, UpdateLocationStatusWithTreeIdMutationVariables>;
export const UpdateLocationStatusWithLocationIdDocument = gql`
    mutation updateLocationStatusWithLocationId($_eq: uuid = "", $id_status: uuid = "") {
  update_location(where: {id: {_eq: $_eq}}, _set: {id_status: $id_status}) {
    returning {
      id
    }
  }
}
    `;
export type UpdateLocationStatusWithLocationIdMutationFn = Apollo.MutationFunction<UpdateLocationStatusWithLocationIdMutation, UpdateLocationStatusWithLocationIdMutationVariables>;

/**
 * __useUpdateLocationStatusWithLocationIdMutation__
 *
 * To run a mutation, you first call `useUpdateLocationStatusWithLocationIdMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateLocationStatusWithLocationIdMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateLocationStatusWithLocationIdMutation, { data, loading, error }] = useUpdateLocationStatusWithLocationIdMutation({
 *   variables: {
 *      _eq: // value for '_eq'
 *      id_status: // value for 'id_status'
 *   },
 * });
 */
export function useUpdateLocationStatusWithLocationIdMutation(baseOptions?: Apollo.MutationHookOptions<UpdateLocationStatusWithLocationIdMutation, UpdateLocationStatusWithLocationIdMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateLocationStatusWithLocationIdMutation, UpdateLocationStatusWithLocationIdMutationVariables>(UpdateLocationStatusWithLocationIdDocument, options);
      }
export type UpdateLocationStatusWithLocationIdMutationHookResult = ReturnType<typeof useUpdateLocationStatusWithLocationIdMutation>;
export type UpdateLocationStatusWithLocationIdMutationResult = Apollo.MutationResult<UpdateLocationStatusWithLocationIdMutation>;
export type UpdateLocationStatusWithLocationIdMutationOptions = Apollo.BaseMutationOptions<UpdateLocationStatusWithLocationIdMutation, UpdateLocationStatusWithLocationIdMutationVariables>;
export const UpdateLocationsStatusesWithLocationsIdsDocument = gql`
    mutation updateLocationsStatusesWithLocationsIds($_in: [uuid!] = "", $id_status: uuid = "") {
  update_location(where: {id: {_in: $_in}}, _set: {id_status: $id_status}) {
    returning {
      id
    }
  }
}
    `;
export type UpdateLocationsStatusesWithLocationsIdsMutationFn = Apollo.MutationFunction<UpdateLocationsStatusesWithLocationsIdsMutation, UpdateLocationsStatusesWithLocationsIdsMutationVariables>;

/**
 * __useUpdateLocationsStatusesWithLocationsIdsMutation__
 *
 * To run a mutation, you first call `useUpdateLocationsStatusesWithLocationsIdsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateLocationsStatusesWithLocationsIdsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateLocationsStatusesWithLocationsIdsMutation, { data, loading, error }] = useUpdateLocationsStatusesWithLocationsIdsMutation({
 *   variables: {
 *      _in: // value for '_in'
 *      id_status: // value for 'id_status'
 *   },
 * });
 */
export function useUpdateLocationsStatusesWithLocationsIdsMutation(baseOptions?: Apollo.MutationHookOptions<UpdateLocationsStatusesWithLocationsIdsMutation, UpdateLocationsStatusesWithLocationsIdsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateLocationsStatusesWithLocationsIdsMutation, UpdateLocationsStatusesWithLocationsIdsMutationVariables>(UpdateLocationsStatusesWithLocationsIdsDocument, options);
      }
export type UpdateLocationsStatusesWithLocationsIdsMutationHookResult = ReturnType<typeof useUpdateLocationsStatusesWithLocationsIdsMutation>;
export type UpdateLocationsStatusesWithLocationsIdsMutationResult = Apollo.MutationResult<UpdateLocationsStatusesWithLocationsIdsMutation>;
export type UpdateLocationsStatusesWithLocationsIdsMutationOptions = Apollo.BaseMutationOptions<UpdateLocationsStatusesWithLocationsIdsMutation, UpdateLocationsStatusesWithLocationsIdsMutationVariables>;
export const CreateInterventionDocument = gql`
    mutation createIntervention($realization_date: date = "", $scheduled_date: date = "", $note: String = "", $tree_id: uuid = "", $location_id: uuid = "", $data_entry_user_id: String = "", $intervention_partner_id: uuid = "", $intervention_type_id: uuid = "", $cost: numeric = null, $request_origin: String = "", $is_parking_banned: Boolean = null) {
  insert_intervention_one(
    object: {tree_id: $tree_id, location_id: $location_id, note: $note, realization_date: $realization_date, scheduled_date: $scheduled_date, intervention_partner_id: $intervention_partner_id, intervention_type_id: $intervention_type_id, data_entry_user_id: $data_entry_user_id, cost: $cost, request_origin: $request_origin, is_parking_banned: $is_parking_banned}
  ) {
    id
    location_id
    tree {
      id
      location_id
    }
    intervention_type {
      slug
    }
    realization_date
    cost
  }
}
    `;
export type CreateInterventionMutationFn = Apollo.MutationFunction<CreateInterventionMutation, CreateInterventionMutationVariables>;

/**
 * __useCreateInterventionMutation__
 *
 * To run a mutation, you first call `useCreateInterventionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateInterventionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createInterventionMutation, { data, loading, error }] = useCreateInterventionMutation({
 *   variables: {
 *      realization_date: // value for 'realization_date'
 *      scheduled_date: // value for 'scheduled_date'
 *      note: // value for 'note'
 *      tree_id: // value for 'tree_id'
 *      location_id: // value for 'location_id'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *      intervention_partner_id: // value for 'intervention_partner_id'
 *      intervention_type_id: // value for 'intervention_type_id'
 *      cost: // value for 'cost'
 *      request_origin: // value for 'request_origin'
 *      is_parking_banned: // value for 'is_parking_banned'
 *   },
 * });
 */
export function useCreateInterventionMutation(baseOptions?: Apollo.MutationHookOptions<CreateInterventionMutation, CreateInterventionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateInterventionMutation, CreateInterventionMutationVariables>(CreateInterventionDocument, options);
      }
export type CreateInterventionMutationHookResult = ReturnType<typeof useCreateInterventionMutation>;
export type CreateInterventionMutationResult = Apollo.MutationResult<CreateInterventionMutation>;
export type CreateInterventionMutationOptions = Apollo.BaseMutationOptions<CreateInterventionMutation, CreateInterventionMutationVariables>;
export const CreateInterventionsDocument = gql`
    mutation createInterventions($interventions: [intervention_insert_input!]!) {
  insert_intervention(objects: $interventions) {
    returning {
      id
      location_id
      tree {
        id
        location_id
      }
      intervention_type {
        slug
      }
      realization_date
      cost
    }
  }
}
    `;
export type CreateInterventionsMutationFn = Apollo.MutationFunction<CreateInterventionsMutation, CreateInterventionsMutationVariables>;

/**
 * __useCreateInterventionsMutation__
 *
 * To run a mutation, you first call `useCreateInterventionsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateInterventionsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createInterventionsMutation, { data, loading, error }] = useCreateInterventionsMutation({
 *   variables: {
 *      interventions: // value for 'interventions'
 *   },
 * });
 */
export function useCreateInterventionsMutation(baseOptions?: Apollo.MutationHookOptions<CreateInterventionsMutation, CreateInterventionsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateInterventionsMutation, CreateInterventionsMutationVariables>(CreateInterventionsDocument, options);
      }
export type CreateInterventionsMutationHookResult = ReturnType<typeof useCreateInterventionsMutation>;
export type CreateInterventionsMutationResult = Apollo.MutationResult<CreateInterventionsMutation>;
export type CreateInterventionsMutationOptions = Apollo.BaseMutationOptions<CreateInterventionsMutation, CreateInterventionsMutationVariables>;
export const CreateInterventionPartnerDocument = gql`
    mutation createInterventionPartner($name: String = "") {
  insert_intervention_partner_one(
    object: {name: $name}
    on_conflict: {constraint: intervention_partner_name_key, update_columns: name}
  ) {
    id
    name
  }
}
    `;
export type CreateInterventionPartnerMutationFn = Apollo.MutationFunction<CreateInterventionPartnerMutation, CreateInterventionPartnerMutationVariables>;

/**
 * __useCreateInterventionPartnerMutation__
 *
 * To run a mutation, you first call `useCreateInterventionPartnerMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateInterventionPartnerMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createInterventionPartnerMutation, { data, loading, error }] = useCreateInterventionPartnerMutation({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function useCreateInterventionPartnerMutation(baseOptions?: Apollo.MutationHookOptions<CreateInterventionPartnerMutation, CreateInterventionPartnerMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateInterventionPartnerMutation, CreateInterventionPartnerMutationVariables>(CreateInterventionPartnerDocument, options);
      }
export type CreateInterventionPartnerMutationHookResult = ReturnType<typeof useCreateInterventionPartnerMutation>;
export type CreateInterventionPartnerMutationResult = Apollo.MutationResult<CreateInterventionPartnerMutation>;
export type CreateInterventionPartnerMutationOptions = Apollo.BaseMutationOptions<CreateInterventionPartnerMutation, CreateInterventionPartnerMutationVariables>;
export const CreateDiagnosisDocument = gql`
    mutation createDiagnosis($diagnosis_date: date = "", $note: String = "", $tree_id: uuid = "", $tree_condition: String = "", $crown_condition: String = "", $trunk_condition: String = "", $collar_condition: String = "", $data_entry_user_id: String = "", $recommendation: String = "", $diagnosis_type_id: uuid = "", $tree_is_dangerous: Boolean = false, $tree_vigor: String = "", $analysis_results: String = "", $carpenters_condition: String = "") {
  insert_diagnosis_one(
    object: {tree_id: $tree_id, note: $note, diagnosis_date: $diagnosis_date, tree_condition: $tree_condition, crown_condition: $crown_condition, trunk_condition: $trunk_condition, collar_condition: $collar_condition, recommendation: $recommendation, diagnosis_type_id: $diagnosis_type_id, data_entry_user_id: $data_entry_user_id, tree_is_dangerous: $tree_is_dangerous, tree_vigor: $tree_vigor, analysis_results: $analysis_results, carpenters_condition: $carpenters_condition}
  ) {
    id
    tree_id
    recommendation
  }
}
    `;
export type CreateDiagnosisMutationFn = Apollo.MutationFunction<CreateDiagnosisMutation, CreateDiagnosisMutationVariables>;

/**
 * __useCreateDiagnosisMutation__
 *
 * To run a mutation, you first call `useCreateDiagnosisMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateDiagnosisMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createDiagnosisMutation, { data, loading, error }] = useCreateDiagnosisMutation({
 *   variables: {
 *      diagnosis_date: // value for 'diagnosis_date'
 *      note: // value for 'note'
 *      tree_id: // value for 'tree_id'
 *      tree_condition: // value for 'tree_condition'
 *      crown_condition: // value for 'crown_condition'
 *      trunk_condition: // value for 'trunk_condition'
 *      collar_condition: // value for 'collar_condition'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *      recommendation: // value for 'recommendation'
 *      diagnosis_type_id: // value for 'diagnosis_type_id'
 *      tree_is_dangerous: // value for 'tree_is_dangerous'
 *      tree_vigor: // value for 'tree_vigor'
 *      analysis_results: // value for 'analysis_results'
 *      carpenters_condition: // value for 'carpenters_condition'
 *   },
 * });
 */
export function useCreateDiagnosisMutation(baseOptions?: Apollo.MutationHookOptions<CreateDiagnosisMutation, CreateDiagnosisMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateDiagnosisMutation, CreateDiagnosisMutationVariables>(CreateDiagnosisDocument, options);
      }
export type CreateDiagnosisMutationHookResult = ReturnType<typeof useCreateDiagnosisMutation>;
export type CreateDiagnosisMutationResult = Apollo.MutationResult<CreateDiagnosisMutation>;
export type CreateDiagnosisMutationOptions = Apollo.BaseMutationOptions<CreateDiagnosisMutation, CreateDiagnosisMutationVariables>;
export const InsertDiagnosisPathogenDocument = gql`
    mutation insertDiagnosisPathogen($diagnosis_id: uuid = "", $pathogen_id: uuid = "") {
  insert_diagnosis_pathogen(
    objects: {pathogen_id: $pathogen_id, diagnosis_id: $diagnosis_id}
  ) {
    affected_rows
  }
}
    `;
export type InsertDiagnosisPathogenMutationFn = Apollo.MutationFunction<InsertDiagnosisPathogenMutation, InsertDiagnosisPathogenMutationVariables>;

/**
 * __useInsertDiagnosisPathogenMutation__
 *
 * To run a mutation, you first call `useInsertDiagnosisPathogenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInsertDiagnosisPathogenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [insertDiagnosisPathogenMutation, { data, loading, error }] = useInsertDiagnosisPathogenMutation({
 *   variables: {
 *      diagnosis_id: // value for 'diagnosis_id'
 *      pathogen_id: // value for 'pathogen_id'
 *   },
 * });
 */
export function useInsertDiagnosisPathogenMutation(baseOptions?: Apollo.MutationHookOptions<InsertDiagnosisPathogenMutation, InsertDiagnosisPathogenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InsertDiagnosisPathogenMutation, InsertDiagnosisPathogenMutationVariables>(InsertDiagnosisPathogenDocument, options);
      }
export type InsertDiagnosisPathogenMutationHookResult = ReturnType<typeof useInsertDiagnosisPathogenMutation>;
export type InsertDiagnosisPathogenMutationResult = Apollo.MutationResult<InsertDiagnosisPathogenMutation>;
export type InsertDiagnosisPathogenMutationOptions = Apollo.BaseMutationOptions<InsertDiagnosisPathogenMutation, InsertDiagnosisPathogenMutationVariables>;
export const DeleteDiagnosisPathogenDocument = gql`
    mutation deleteDiagnosisPathogen($diagnosis_id: uuid = "") {
  delete_diagnosis_pathogen(where: {diagnosis_id: {_eq: $diagnosis_id}}) {
    affected_rows
  }
}
    `;
export type DeleteDiagnosisPathogenMutationFn = Apollo.MutationFunction<DeleteDiagnosisPathogenMutation, DeleteDiagnosisPathogenMutationVariables>;

/**
 * __useDeleteDiagnosisPathogenMutation__
 *
 * To run a mutation, you first call `useDeleteDiagnosisPathogenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteDiagnosisPathogenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteDiagnosisPathogenMutation, { data, loading, error }] = useDeleteDiagnosisPathogenMutation({
 *   variables: {
 *      diagnosis_id: // value for 'diagnosis_id'
 *   },
 * });
 */
export function useDeleteDiagnosisPathogenMutation(baseOptions?: Apollo.MutationHookOptions<DeleteDiagnosisPathogenMutation, DeleteDiagnosisPathogenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteDiagnosisPathogenMutation, DeleteDiagnosisPathogenMutationVariables>(DeleteDiagnosisPathogenDocument, options);
      }
export type DeleteDiagnosisPathogenMutationHookResult = ReturnType<typeof useDeleteDiagnosisPathogenMutation>;
export type DeleteDiagnosisPathogenMutationResult = Apollo.MutationResult<DeleteDiagnosisPathogenMutation>;
export type DeleteDiagnosisPathogenMutationOptions = Apollo.BaseMutationOptions<DeleteDiagnosisPathogenMutation, DeleteDiagnosisPathogenMutationVariables>;
export const DeleteDiagnosisAnalysisToolDocument = gql`
    mutation deleteDiagnosisAnalysisTool($diagnosis_id: uuid = "") {
  delete_diagnosis_analysis_tool(where: {diagnosis_id: {_eq: $diagnosis_id}}) {
    affected_rows
  }
}
    `;
export type DeleteDiagnosisAnalysisToolMutationFn = Apollo.MutationFunction<DeleteDiagnosisAnalysisToolMutation, DeleteDiagnosisAnalysisToolMutationVariables>;

/**
 * __useDeleteDiagnosisAnalysisToolMutation__
 *
 * To run a mutation, you first call `useDeleteDiagnosisAnalysisToolMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteDiagnosisAnalysisToolMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteDiagnosisAnalysisToolMutation, { data, loading, error }] = useDeleteDiagnosisAnalysisToolMutation({
 *   variables: {
 *      diagnosis_id: // value for 'diagnosis_id'
 *   },
 * });
 */
export function useDeleteDiagnosisAnalysisToolMutation(baseOptions?: Apollo.MutationHookOptions<DeleteDiagnosisAnalysisToolMutation, DeleteDiagnosisAnalysisToolMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteDiagnosisAnalysisToolMutation, DeleteDiagnosisAnalysisToolMutationVariables>(DeleteDiagnosisAnalysisToolDocument, options);
      }
export type DeleteDiagnosisAnalysisToolMutationHookResult = ReturnType<typeof useDeleteDiagnosisAnalysisToolMutation>;
export type DeleteDiagnosisAnalysisToolMutationResult = Apollo.MutationResult<DeleteDiagnosisAnalysisToolMutation>;
export type DeleteDiagnosisAnalysisToolMutationOptions = Apollo.BaseMutationOptions<DeleteDiagnosisAnalysisToolMutation, DeleteDiagnosisAnalysisToolMutationVariables>;
export const InsertDiagnosisAnalysisToolDocument = gql`
    mutation insertDiagnosisAnalysisTool($diagnosis_id: uuid = "", $analysis_tool_id: uuid = "") {
  insert_diagnosis_analysis_tool(
    objects: {analysis_tool_id: $analysis_tool_id, diagnosis_id: $diagnosis_id}
  ) {
    affected_rows
  }
}
    `;
export type InsertDiagnosisAnalysisToolMutationFn = Apollo.MutationFunction<InsertDiagnosisAnalysisToolMutation, InsertDiagnosisAnalysisToolMutationVariables>;

/**
 * __useInsertDiagnosisAnalysisToolMutation__
 *
 * To run a mutation, you first call `useInsertDiagnosisAnalysisToolMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInsertDiagnosisAnalysisToolMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [insertDiagnosisAnalysisToolMutation, { data, loading, error }] = useInsertDiagnosisAnalysisToolMutation({
 *   variables: {
 *      diagnosis_id: // value for 'diagnosis_id'
 *      analysis_tool_id: // value for 'analysis_tool_id'
 *   },
 * });
 */
export function useInsertDiagnosisAnalysisToolMutation(baseOptions?: Apollo.MutationHookOptions<InsertDiagnosisAnalysisToolMutation, InsertDiagnosisAnalysisToolMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InsertDiagnosisAnalysisToolMutation, InsertDiagnosisAnalysisToolMutationVariables>(InsertDiagnosisAnalysisToolDocument, options);
      }
export type InsertDiagnosisAnalysisToolMutationHookResult = ReturnType<typeof useInsertDiagnosisAnalysisToolMutation>;
export type InsertDiagnosisAnalysisToolMutationResult = Apollo.MutationResult<InsertDiagnosisAnalysisToolMutation>;
export type InsertDiagnosisAnalysisToolMutationOptions = Apollo.BaseMutationOptions<InsertDiagnosisAnalysisToolMutation, InsertDiagnosisAnalysisToolMutationVariables>;
export const UpdateInterventionDocument = gql`
    mutation updateIntervention($id: uuid = "", $realization_date: date = "", $scheduled_date: date = "", $note: String = "", $data_entry_user_id: String = "", $intervention_partner_id: uuid = "", $intervention_type_id: uuid = "", $cost: numeric = null, $validation_note: String = "", $request_origin: String = "", $is_parking_banned: Boolean = null) {
  update_intervention_by_pk(
    pk_columns: {id: $id}
    _set: {note: $note, realization_date: $realization_date, scheduled_date: $scheduled_date, intervention_partner_id: $intervention_partner_id, intervention_type_id: $intervention_type_id, data_entry_user_id: $data_entry_user_id, cost: $cost, validation_note: $validation_note, request_origin: $request_origin, is_parking_banned: $is_parking_banned}
  ) {
    id
    location_id
    realization_date
    tree {
      location_id
      id
    }
    intervention_type {
      slug
    }
  }
}
    `;
export type UpdateInterventionMutationFn = Apollo.MutationFunction<UpdateInterventionMutation, UpdateInterventionMutationVariables>;

/**
 * __useUpdateInterventionMutation__
 *
 * To run a mutation, you first call `useUpdateInterventionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateInterventionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateInterventionMutation, { data, loading, error }] = useUpdateInterventionMutation({
 *   variables: {
 *      id: // value for 'id'
 *      realization_date: // value for 'realization_date'
 *      scheduled_date: // value for 'scheduled_date'
 *      note: // value for 'note'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *      intervention_partner_id: // value for 'intervention_partner_id'
 *      intervention_type_id: // value for 'intervention_type_id'
 *      cost: // value for 'cost'
 *      validation_note: // value for 'validation_note'
 *      request_origin: // value for 'request_origin'
 *      is_parking_banned: // value for 'is_parking_banned'
 *   },
 * });
 */
export function useUpdateInterventionMutation(baseOptions?: Apollo.MutationHookOptions<UpdateInterventionMutation, UpdateInterventionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateInterventionMutation, UpdateInterventionMutationVariables>(UpdateInterventionDocument, options);
      }
export type UpdateInterventionMutationHookResult = ReturnType<typeof useUpdateInterventionMutation>;
export type UpdateInterventionMutationResult = Apollo.MutationResult<UpdateInterventionMutation>;
export type UpdateInterventionMutationOptions = Apollo.BaseMutationOptions<UpdateInterventionMutation, UpdateInterventionMutationVariables>;
export const UpdateDiagnosisDocument = gql`
    mutation updateDiagnosis($id: uuid = "", $diagnosis_date: date = "", $recommendation: String = "", $tree_condition: String = "", $crown_condition: String = "", $trunk_condition: String = "", $collar_condition: String = "", $note: String = "", $data_entry_user_id: String = "", $diagnosis_type_id: uuid = "", $tree_is_dangerous: Boolean = false, $tree_vigor: String = "", $analysis_results: String = "", $carpenters_condition: String = "") {
  update_diagnosis_by_pk(
    pk_columns: {id: $id}
    _set: {note: $note, diagnosis_date: $diagnosis_date, recommendation: $recommendation, tree_condition: $tree_condition, crown_condition: $crown_condition, trunk_condition: $trunk_condition, collar_condition: $collar_condition, diagnosis_type_id: $diagnosis_type_id, data_entry_user_id: $data_entry_user_id, tree_is_dangerous: $tree_is_dangerous, tree_vigor: $tree_vigor, analysis_results: $analysis_results, carpenters_condition: $carpenters_condition}
  ) {
    id
    tree_id
  }
}
    `;
export type UpdateDiagnosisMutationFn = Apollo.MutationFunction<UpdateDiagnosisMutation, UpdateDiagnosisMutationVariables>;

/**
 * __useUpdateDiagnosisMutation__
 *
 * To run a mutation, you first call `useUpdateDiagnosisMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateDiagnosisMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateDiagnosisMutation, { data, loading, error }] = useUpdateDiagnosisMutation({
 *   variables: {
 *      id: // value for 'id'
 *      diagnosis_date: // value for 'diagnosis_date'
 *      recommendation: // value for 'recommendation'
 *      tree_condition: // value for 'tree_condition'
 *      crown_condition: // value for 'crown_condition'
 *      trunk_condition: // value for 'trunk_condition'
 *      collar_condition: // value for 'collar_condition'
 *      note: // value for 'note'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *      diagnosis_type_id: // value for 'diagnosis_type_id'
 *      tree_is_dangerous: // value for 'tree_is_dangerous'
 *      tree_vigor: // value for 'tree_vigor'
 *      analysis_results: // value for 'analysis_results'
 *      carpenters_condition: // value for 'carpenters_condition'
 *   },
 * });
 */
export function useUpdateDiagnosisMutation(baseOptions?: Apollo.MutationHookOptions<UpdateDiagnosisMutation, UpdateDiagnosisMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateDiagnosisMutation, UpdateDiagnosisMutationVariables>(UpdateDiagnosisDocument, options);
      }
export type UpdateDiagnosisMutationHookResult = ReturnType<typeof useUpdateDiagnosisMutation>;
export type UpdateDiagnosisMutationResult = Apollo.MutationResult<UpdateDiagnosisMutation>;
export type UpdateDiagnosisMutationOptions = Apollo.BaseMutationOptions<UpdateDiagnosisMutation, UpdateDiagnosisMutationVariables>;
export const DeleteInterventionDocument = gql`
    mutation deleteIntervention($id: uuid = "") {
  delete_intervention_by_pk(id: $id) {
    id
    location_id
    tree {
      location_id
      id
    }
  }
}
    `;
export type DeleteInterventionMutationFn = Apollo.MutationFunction<DeleteInterventionMutation, DeleteInterventionMutationVariables>;

/**
 * __useDeleteInterventionMutation__
 *
 * To run a mutation, you first call `useDeleteInterventionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteInterventionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteInterventionMutation, { data, loading, error }] = useDeleteInterventionMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteInterventionMutation(baseOptions?: Apollo.MutationHookOptions<DeleteInterventionMutation, DeleteInterventionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteInterventionMutation, DeleteInterventionMutationVariables>(DeleteInterventionDocument, options);
      }
export type DeleteInterventionMutationHookResult = ReturnType<typeof useDeleteInterventionMutation>;
export type DeleteInterventionMutationResult = Apollo.MutationResult<DeleteInterventionMutation>;
export type DeleteInterventionMutationOptions = Apollo.BaseMutationOptions<DeleteInterventionMutation, DeleteInterventionMutationVariables>;
export const DeleteInterventionsDocument = gql`
    mutation deleteInterventions($_in: [uuid!] = "") {
  delete_intervention(where: {id: {_in: $_in}}) {
    affected_rows
  }
}
    `;
export type DeleteInterventionsMutationFn = Apollo.MutationFunction<DeleteInterventionsMutation, DeleteInterventionsMutationVariables>;

/**
 * __useDeleteInterventionsMutation__
 *
 * To run a mutation, you first call `useDeleteInterventionsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteInterventionsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteInterventionsMutation, { data, loading, error }] = useDeleteInterventionsMutation({
 *   variables: {
 *      _in: // value for '_in'
 *   },
 * });
 */
export function useDeleteInterventionsMutation(baseOptions?: Apollo.MutationHookOptions<DeleteInterventionsMutation, DeleteInterventionsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteInterventionsMutation, DeleteInterventionsMutationVariables>(DeleteInterventionsDocument, options);
      }
export type DeleteInterventionsMutationHookResult = ReturnType<typeof useDeleteInterventionsMutation>;
export type DeleteInterventionsMutationResult = Apollo.MutationResult<DeleteInterventionsMutation>;
export type DeleteInterventionsMutationOptions = Apollo.BaseMutationOptions<DeleteInterventionsMutation, DeleteInterventionsMutationVariables>;
export const DeleteDiagnosesDocument = gql`
    mutation deleteDiagnoses($_in: [uuid!] = "") {
  delete_diagnosis(where: {id: {_in: $_in}}) {
    affected_rows
  }
}
    `;
export type DeleteDiagnosesMutationFn = Apollo.MutationFunction<DeleteDiagnosesMutation, DeleteDiagnosesMutationVariables>;

/**
 * __useDeleteDiagnosesMutation__
 *
 * To run a mutation, you first call `useDeleteDiagnosesMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteDiagnosesMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteDiagnosesMutation, { data, loading, error }] = useDeleteDiagnosesMutation({
 *   variables: {
 *      _in: // value for '_in'
 *   },
 * });
 */
export function useDeleteDiagnosesMutation(baseOptions?: Apollo.MutationHookOptions<DeleteDiagnosesMutation, DeleteDiagnosesMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteDiagnosesMutation, DeleteDiagnosesMutationVariables>(DeleteDiagnosesDocument, options);
      }
export type DeleteDiagnosesMutationHookResult = ReturnType<typeof useDeleteDiagnosesMutation>;
export type DeleteDiagnosesMutationResult = Apollo.MutationResult<DeleteDiagnosesMutation>;
export type DeleteDiagnosesMutationOptions = Apollo.BaseMutationOptions<DeleteDiagnosesMutation, DeleteDiagnosesMutationVariables>;
export const DeleteDiagnosisDocument = gql`
    mutation deleteDiagnosis($id: uuid = "") {
  delete_diagnosis_by_pk(id: $id) {
    id
    tree_id
  }
}
    `;
export type DeleteDiagnosisMutationFn = Apollo.MutationFunction<DeleteDiagnosisMutation, DeleteDiagnosisMutationVariables>;

/**
 * __useDeleteDiagnosisMutation__
 *
 * To run a mutation, you first call `useDeleteDiagnosisMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteDiagnosisMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteDiagnosisMutation, { data, loading, error }] = useDeleteDiagnosisMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteDiagnosisMutation(baseOptions?: Apollo.MutationHookOptions<DeleteDiagnosisMutation, DeleteDiagnosisMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteDiagnosisMutation, DeleteDiagnosisMutationVariables>(DeleteDiagnosisDocument, options);
      }
export type DeleteDiagnosisMutationHookResult = ReturnType<typeof useDeleteDiagnosisMutation>;
export type DeleteDiagnosisMutationResult = Apollo.MutationResult<DeleteDiagnosisMutation>;
export type DeleteDiagnosisMutationOptions = Apollo.BaseMutationOptions<DeleteDiagnosisMutation, DeleteDiagnosisMutationVariables>;
export const ValidateInterventionsDocument = gql`
    mutation validateInterventions($_in: [uuid!] = "", $realization_date: date = "", $intervention_partner_id: uuid = null, $validation_note: String = "", $data_entry_user_id: String = "") {
  update_intervention(
    where: {id: {_in: $_in}}
    _set: {realization_date: $realization_date, intervention_partner_id: $intervention_partner_id, validation_note: $validation_note, data_entry_user_id: $data_entry_user_id}
  ) {
    returning {
      id
      intervention_type {
        slug
      }
      tree {
        location_id
      }
      location_id
      location {
        location_status {
          status
        }
      }
      realization_date
    }
  }
}
    `;
export type ValidateInterventionsMutationFn = Apollo.MutationFunction<ValidateInterventionsMutation, ValidateInterventionsMutationVariables>;

/**
 * __useValidateInterventionsMutation__
 *
 * To run a mutation, you first call `useValidateInterventionsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useValidateInterventionsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [validateInterventionsMutation, { data, loading, error }] = useValidateInterventionsMutation({
 *   variables: {
 *      _in: // value for '_in'
 *      realization_date: // value for 'realization_date'
 *      intervention_partner_id: // value for 'intervention_partner_id'
 *      validation_note: // value for 'validation_note'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *   },
 * });
 */
export function useValidateInterventionsMutation(baseOptions?: Apollo.MutationHookOptions<ValidateInterventionsMutation, ValidateInterventionsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ValidateInterventionsMutation, ValidateInterventionsMutationVariables>(ValidateInterventionsDocument, options);
      }
export type ValidateInterventionsMutationHookResult = ReturnType<typeof useValidateInterventionsMutation>;
export type ValidateInterventionsMutationResult = Apollo.MutationResult<ValidateInterventionsMutation>;
export type ValidateInterventionsMutationOptions = Apollo.BaseMutationOptions<ValidateInterventionsMutation, ValidateInterventionsMutationVariables>;
export const ValidateInterventionDocument = gql`
    mutation validateIntervention($id: uuid = "", $realization_date: date = "", $intervention_partner_id: uuid = "", $validation_note: String = "", $data_entry_user_id: String = "") {
  update_intervention_by_pk(
    pk_columns: {id: $id}
    _set: {realization_date: $realization_date, intervention_partner_id: $intervention_partner_id, data_entry_user_id: $data_entry_user_id, validation_note: $validation_note}
  ) {
    id
    location_id
    tree {
      location_id
    }
    intervention_type {
      slug
    }
    location {
      location_status {
        status
      }
    }
    realization_date
  }
}
    `;
export type ValidateInterventionMutationFn = Apollo.MutationFunction<ValidateInterventionMutation, ValidateInterventionMutationVariables>;

/**
 * __useValidateInterventionMutation__
 *
 * To run a mutation, you first call `useValidateInterventionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useValidateInterventionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [validateInterventionMutation, { data, loading, error }] = useValidateInterventionMutation({
 *   variables: {
 *      id: // value for 'id'
 *      realization_date: // value for 'realization_date'
 *      intervention_partner_id: // value for 'intervention_partner_id'
 *      validation_note: // value for 'validation_note'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *   },
 * });
 */
export function useValidateInterventionMutation(baseOptions?: Apollo.MutationHookOptions<ValidateInterventionMutation, ValidateInterventionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ValidateInterventionMutation, ValidateInterventionMutationVariables>(ValidateInterventionDocument, options);
      }
export type ValidateInterventionMutationHookResult = ReturnType<typeof useValidateInterventionMutation>;
export type ValidateInterventionMutationResult = Apollo.MutationResult<ValidateInterventionMutation>;
export type ValidateInterventionMutationOptions = Apollo.BaseMutationOptions<ValidateInterventionMutation, ValidateInterventionMutationVariables>;
export const CreateImportDocument = gql`
    mutation createImport($data_entry_user_id: String = "", $source_file_path: String = "", $task_id: String = null, $source_crs: String = "", $organization_id: uuid) {
  insert_import_one(
    object: {data_entry_user_id: $data_entry_user_id, source_file_path: $source_file_path, task_id: $task_id, source_crs: $source_crs, organization_id: $organization_id}
  ) {
    id
    source_file_path
  }
}
    `;
export type CreateImportMutationFn = Apollo.MutationFunction<CreateImportMutation, CreateImportMutationVariables>;

/**
 * __useCreateImportMutation__
 *
 * To run a mutation, you first call `useCreateImportMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateImportMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createImportMutation, { data, loading, error }] = useCreateImportMutation({
 *   variables: {
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *      source_file_path: // value for 'source_file_path'
 *      task_id: // value for 'task_id'
 *      source_crs: // value for 'source_crs'
 *      organization_id: // value for 'organization_id'
 *   },
 * });
 */
export function useCreateImportMutation(baseOptions?: Apollo.MutationHookOptions<CreateImportMutation, CreateImportMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateImportMutation, CreateImportMutationVariables>(CreateImportDocument, options);
      }
export type CreateImportMutationHookResult = ReturnType<typeof useCreateImportMutation>;
export type CreateImportMutationResult = Apollo.MutationResult<CreateImportMutation>;
export type CreateImportMutationOptions = Apollo.BaseMutationOptions<CreateImportMutation, CreateImportMutationVariables>;
export const DeleteImportsDocument = gql`
    mutation deleteImports($_in: [uuid!]) {
  delete_import(where: {id: {_in: $_in}}) {
    affected_rows
  }
}
    `;
export type DeleteImportsMutationFn = Apollo.MutationFunction<DeleteImportsMutation, DeleteImportsMutationVariables>;

/**
 * __useDeleteImportsMutation__
 *
 * To run a mutation, you first call `useDeleteImportsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteImportsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteImportsMutation, { data, loading, error }] = useDeleteImportsMutation({
 *   variables: {
 *      _in: // value for '_in'
 *   },
 * });
 */
export function useDeleteImportsMutation(baseOptions?: Apollo.MutationHookOptions<DeleteImportsMutation, DeleteImportsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteImportsMutation, DeleteImportsMutationVariables>(DeleteImportsDocument, options);
      }
export type DeleteImportsMutationHookResult = ReturnType<typeof useDeleteImportsMutation>;
export type DeleteImportsMutationResult = Apollo.MutationResult<DeleteImportsMutation>;
export type DeleteImportsMutationOptions = Apollo.BaseMutationOptions<DeleteImportsMutation, DeleteImportsMutationVariables>;
export const CreateOneWorksiteWithRelationshipsDocument = gql`
    mutation createOneWorksiteWithRelationships($object: worksite_insert_input! = {name: "Third Worksite"}) {
  insert_worksite_one(object: $object) {
    id
    name
    description
  }
}
    `;
export type CreateOneWorksiteWithRelationshipsMutationFn = Apollo.MutationFunction<CreateOneWorksiteWithRelationshipsMutation, CreateOneWorksiteWithRelationshipsMutationVariables>;

/**
 * __useCreateOneWorksiteWithRelationshipsMutation__
 *
 * To run a mutation, you first call `useCreateOneWorksiteWithRelationshipsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateOneWorksiteWithRelationshipsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createOneWorksiteWithRelationshipsMutation, { data, loading, error }] = useCreateOneWorksiteWithRelationshipsMutation({
 *   variables: {
 *      object: // value for 'object'
 *   },
 * });
 */
export function useCreateOneWorksiteWithRelationshipsMutation(baseOptions?: Apollo.MutationHookOptions<CreateOneWorksiteWithRelationshipsMutation, CreateOneWorksiteWithRelationshipsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateOneWorksiteWithRelationshipsMutation, CreateOneWorksiteWithRelationshipsMutationVariables>(CreateOneWorksiteWithRelationshipsDocument, options);
      }
export type CreateOneWorksiteWithRelationshipsMutationHookResult = ReturnType<typeof useCreateOneWorksiteWithRelationshipsMutation>;
export type CreateOneWorksiteWithRelationshipsMutationResult = Apollo.MutationResult<CreateOneWorksiteWithRelationshipsMutation>;
export type CreateOneWorksiteWithRelationshipsMutationOptions = Apollo.BaseMutationOptions<CreateOneWorksiteWithRelationshipsMutation, CreateOneWorksiteWithRelationshipsMutationVariables>;
export const UpdateWorksiteDocument = gql`
    mutation updateWorksite($id: uuid!, $input_data: worksite_set_input = {}) {
  update_worksite_by_pk(pk_columns: {id: $id}, _set: $input_data) {
    ...worksiteFields
  }
}
    ${WorksiteFieldsFragmentDoc}`;
export type UpdateWorksiteMutationFn = Apollo.MutationFunction<UpdateWorksiteMutation, UpdateWorksiteMutationVariables>;

/**
 * __useUpdateWorksiteMutation__
 *
 * To run a mutation, you first call `useUpdateWorksiteMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateWorksiteMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateWorksiteMutation, { data, loading, error }] = useUpdateWorksiteMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input_data: // value for 'input_data'
 *   },
 * });
 */
export function useUpdateWorksiteMutation(baseOptions?: Apollo.MutationHookOptions<UpdateWorksiteMutation, UpdateWorksiteMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateWorksiteMutation, UpdateWorksiteMutationVariables>(UpdateWorksiteDocument, options);
      }
export type UpdateWorksiteMutationHookResult = ReturnType<typeof useUpdateWorksiteMutation>;
export type UpdateWorksiteMutationResult = Apollo.MutationResult<UpdateWorksiteMutation>;
export type UpdateWorksiteMutationOptions = Apollo.BaseMutationOptions<UpdateWorksiteMutation, UpdateWorksiteMutationVariables>;
export const CreateBoundaryDocument = gql`
    mutation createBoundary($coords: geometry = "", $name: String = "", $type: String = "", $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb") {
  insert_boundary_one(
    object: {coords: $coords, name: $name, type: $type, data_entry_user_id: $data_entry_user_id}
  ) {
    id
  }
}
    `;
export type CreateBoundaryMutationFn = Apollo.MutationFunction<CreateBoundaryMutation, CreateBoundaryMutationVariables>;

/**
 * __useCreateBoundaryMutation__
 *
 * To run a mutation, you first call `useCreateBoundaryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateBoundaryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createBoundaryMutation, { data, loading, error }] = useCreateBoundaryMutation({
 *   variables: {
 *      coords: // value for 'coords'
 *      name: // value for 'name'
 *      type: // value for 'type'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *   },
 * });
 */
export function useCreateBoundaryMutation(baseOptions?: Apollo.MutationHookOptions<CreateBoundaryMutation, CreateBoundaryMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateBoundaryMutation, CreateBoundaryMutationVariables>(CreateBoundaryDocument, options);
      }
export type CreateBoundaryMutationHookResult = ReturnType<typeof useCreateBoundaryMutation>;
export type CreateBoundaryMutationResult = Apollo.MutationResult<CreateBoundaryMutation>;
export type CreateBoundaryMutationOptions = Apollo.BaseMutationOptions<CreateBoundaryMutation, CreateBoundaryMutationVariables>;
export const CreateBoundaryAndDeleteExistingAssociationsDocument = gql`
    mutation createBoundaryAndDeleteExistingAssociations($coords: geometry!, $name: String!, $type: String!, $locationsIds: [uuid!]!, $vegetatedAreasIds: [uuid!]!, $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb") {
  delete_boundaries_locations(
    where: {location_id: {_in: $locationsIds}, boundary: {type: {_eq: $type}}}
  ) {
    affected_rows
  }
  delete_boundaries_vegetated_areas(
    where: {vegetated_area_id: {_in: $vegetatedAreasIds}, boundary: {type: {_eq: $type}}}
  ) {
    affected_rows
  }
  insert_boundary_one(
    object: {coords: $coords, name: $name, type: $type, data_entry_user_id: $data_entry_user_id}
  ) {
    id
  }
}
    `;
export type CreateBoundaryAndDeleteExistingAssociationsMutationFn = Apollo.MutationFunction<CreateBoundaryAndDeleteExistingAssociationsMutation, CreateBoundaryAndDeleteExistingAssociationsMutationVariables>;

/**
 * __useCreateBoundaryAndDeleteExistingAssociationsMutation__
 *
 * To run a mutation, you first call `useCreateBoundaryAndDeleteExistingAssociationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateBoundaryAndDeleteExistingAssociationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createBoundaryAndDeleteExistingAssociationsMutation, { data, loading, error }] = useCreateBoundaryAndDeleteExistingAssociationsMutation({
 *   variables: {
 *      coords: // value for 'coords'
 *      name: // value for 'name'
 *      type: // value for 'type'
 *      locationsIds: // value for 'locationsIds'
 *      vegetatedAreasIds: // value for 'vegetatedAreasIds'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *   },
 * });
 */
export function useCreateBoundaryAndDeleteExistingAssociationsMutation(baseOptions?: Apollo.MutationHookOptions<CreateBoundaryAndDeleteExistingAssociationsMutation, CreateBoundaryAndDeleteExistingAssociationsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateBoundaryAndDeleteExistingAssociationsMutation, CreateBoundaryAndDeleteExistingAssociationsMutationVariables>(CreateBoundaryAndDeleteExistingAssociationsDocument, options);
      }
export type CreateBoundaryAndDeleteExistingAssociationsMutationHookResult = ReturnType<typeof useCreateBoundaryAndDeleteExistingAssociationsMutation>;
export type CreateBoundaryAndDeleteExistingAssociationsMutationResult = Apollo.MutationResult<CreateBoundaryAndDeleteExistingAssociationsMutation>;
export type CreateBoundaryAndDeleteExistingAssociationsMutationOptions = Apollo.BaseMutationOptions<CreateBoundaryAndDeleteExistingAssociationsMutation, CreateBoundaryAndDeleteExistingAssociationsMutationVariables>;
export const UpdateBoundaryAndDeleteExistingAssociationsDocument = gql`
    mutation updateBoundaryAndDeleteExistingAssociations($id: uuid!, $coords: geometry!, $name: String!, $type: String!, $locationsIds: [uuid!]!, $vegetatedAreasIds: [uuid!]!, $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb") {
  delete_boundaries_locations(
    where: {location_id: {_in: $locationsIds}, boundary: {type: {_eq: $type}}}
  ) {
    affected_rows
  }
  delete_boundaries_vegetated_areas(
    where: {vegetated_area_id: {_in: $vegetatedAreasIds}, boundary: {type: {_eq: $type}}}
  ) {
    affected_rows
  }
  update_boundary_by_pk(
    pk_columns: {id: $id}
    _set: {name: $name, coords: $coords, type: $type, data_entry_user_id: $data_entry_user_id}
  ) {
    id
  }
}
    `;
export type UpdateBoundaryAndDeleteExistingAssociationsMutationFn = Apollo.MutationFunction<UpdateBoundaryAndDeleteExistingAssociationsMutation, UpdateBoundaryAndDeleteExistingAssociationsMutationVariables>;

/**
 * __useUpdateBoundaryAndDeleteExistingAssociationsMutation__
 *
 * To run a mutation, you first call `useUpdateBoundaryAndDeleteExistingAssociationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateBoundaryAndDeleteExistingAssociationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateBoundaryAndDeleteExistingAssociationsMutation, { data, loading, error }] = useUpdateBoundaryAndDeleteExistingAssociationsMutation({
 *   variables: {
 *      id: // value for 'id'
 *      coords: // value for 'coords'
 *      name: // value for 'name'
 *      type: // value for 'type'
 *      locationsIds: // value for 'locationsIds'
 *      vegetatedAreasIds: // value for 'vegetatedAreasIds'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *   },
 * });
 */
export function useUpdateBoundaryAndDeleteExistingAssociationsMutation(baseOptions?: Apollo.MutationHookOptions<UpdateBoundaryAndDeleteExistingAssociationsMutation, UpdateBoundaryAndDeleteExistingAssociationsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateBoundaryAndDeleteExistingAssociationsMutation, UpdateBoundaryAndDeleteExistingAssociationsMutationVariables>(UpdateBoundaryAndDeleteExistingAssociationsDocument, options);
      }
export type UpdateBoundaryAndDeleteExistingAssociationsMutationHookResult = ReturnType<typeof useUpdateBoundaryAndDeleteExistingAssociationsMutation>;
export type UpdateBoundaryAndDeleteExistingAssociationsMutationResult = Apollo.MutationResult<UpdateBoundaryAndDeleteExistingAssociationsMutation>;
export type UpdateBoundaryAndDeleteExistingAssociationsMutationOptions = Apollo.BaseMutationOptions<UpdateBoundaryAndDeleteExistingAssociationsMutation, UpdateBoundaryAndDeleteExistingAssociationsMutationVariables>;
export const UpdateBoundaryDocument = gql`
    mutation updateBoundary($id: uuid!, $name: String = "", $coords: geometry = "", $type: String = "", $data_entry_user_id: String = "c8c8b8a0-f8f6-4581-b8a7-3a812eb199cb") {
  update_boundary_by_pk(
    pk_columns: {id: $id}
    _set: {name: $name, coords: $coords, type: $type, data_entry_user_id: $data_entry_user_id}
  ) {
    id
  }
}
    `;
export type UpdateBoundaryMutationFn = Apollo.MutationFunction<UpdateBoundaryMutation, UpdateBoundaryMutationVariables>;

/**
 * __useUpdateBoundaryMutation__
 *
 * To run a mutation, you first call `useUpdateBoundaryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateBoundaryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateBoundaryMutation, { data, loading, error }] = useUpdateBoundaryMutation({
 *   variables: {
 *      id: // value for 'id'
 *      name: // value for 'name'
 *      coords: // value for 'coords'
 *      type: // value for 'type'
 *      data_entry_user_id: // value for 'data_entry_user_id'
 *   },
 * });
 */
export function useUpdateBoundaryMutation(baseOptions?: Apollo.MutationHookOptions<UpdateBoundaryMutation, UpdateBoundaryMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateBoundaryMutation, UpdateBoundaryMutationVariables>(UpdateBoundaryDocument, options);
      }
export type UpdateBoundaryMutationHookResult = ReturnType<typeof useUpdateBoundaryMutation>;
export type UpdateBoundaryMutationResult = Apollo.MutationResult<UpdateBoundaryMutation>;
export type UpdateBoundaryMutationOptions = Apollo.BaseMutationOptions<UpdateBoundaryMutation, UpdateBoundaryMutationVariables>;
export const DeleteBoundaryDocument = gql`
    mutation deleteBoundary($id: uuid = "") {
  delete_boundary_by_pk(id: $id) {
    id
  }
}
    `;
export type DeleteBoundaryMutationFn = Apollo.MutationFunction<DeleteBoundaryMutation, DeleteBoundaryMutationVariables>;

/**
 * __useDeleteBoundaryMutation__
 *
 * To run a mutation, you first call `useDeleteBoundaryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteBoundaryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteBoundaryMutation, { data, loading, error }] = useDeleteBoundaryMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteBoundaryMutation(baseOptions?: Apollo.MutationHookOptions<DeleteBoundaryMutation, DeleteBoundaryMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteBoundaryMutation, DeleteBoundaryMutationVariables>(DeleteBoundaryDocument, options);
      }
export type DeleteBoundaryMutationHookResult = ReturnType<typeof useDeleteBoundaryMutation>;
export type DeleteBoundaryMutationResult = Apollo.MutationResult<DeleteBoundaryMutation>;
export type DeleteBoundaryMutationOptions = Apollo.BaseMutationOptions<DeleteBoundaryMutation, DeleteBoundaryMutationVariables>;
export const CreateVegetatedAreaDocument = gql`
    mutation createVegetatedArea($object: vegetated_area_insert_input = {}) {
  insert_vegetated_area_one(object: $object) {
    id
  }
}
    `;
export type CreateVegetatedAreaMutationFn = Apollo.MutationFunction<CreateVegetatedAreaMutation, CreateVegetatedAreaMutationVariables>;

/**
 * __useCreateVegetatedAreaMutation__
 *
 * To run a mutation, you first call `useCreateVegetatedAreaMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateVegetatedAreaMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createVegetatedAreaMutation, { data, loading, error }] = useCreateVegetatedAreaMutation({
 *   variables: {
 *      object: // value for 'object'
 *   },
 * });
 */
export function useCreateVegetatedAreaMutation(baseOptions?: Apollo.MutationHookOptions<CreateVegetatedAreaMutation, CreateVegetatedAreaMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateVegetatedAreaMutation, CreateVegetatedAreaMutationVariables>(CreateVegetatedAreaDocument, options);
      }
export type CreateVegetatedAreaMutationHookResult = ReturnType<typeof useCreateVegetatedAreaMutation>;
export type CreateVegetatedAreaMutationResult = Apollo.MutationResult<CreateVegetatedAreaMutation>;
export type CreateVegetatedAreaMutationOptions = Apollo.BaseMutationOptions<CreateVegetatedAreaMutation, CreateVegetatedAreaMutationVariables>;
export const DeleteVegetatedAreaDocument = gql`
    mutation deleteVegetatedArea($id: uuid = "") {
  delete_vegetated_area_by_pk(id: $id) {
    id
  }
}
    `;
export type DeleteVegetatedAreaMutationFn = Apollo.MutationFunction<DeleteVegetatedAreaMutation, DeleteVegetatedAreaMutationVariables>;

/**
 * __useDeleteVegetatedAreaMutation__
 *
 * To run a mutation, you first call `useDeleteVegetatedAreaMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteVegetatedAreaMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteVegetatedAreaMutation, { data, loading, error }] = useDeleteVegetatedAreaMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteVegetatedAreaMutation(baseOptions?: Apollo.MutationHookOptions<DeleteVegetatedAreaMutation, DeleteVegetatedAreaMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteVegetatedAreaMutation, DeleteVegetatedAreaMutationVariables>(DeleteVegetatedAreaDocument, options);
      }
export type DeleteVegetatedAreaMutationHookResult = ReturnType<typeof useDeleteVegetatedAreaMutation>;
export type DeleteVegetatedAreaMutationResult = Apollo.MutationResult<DeleteVegetatedAreaMutation>;
export type DeleteVegetatedAreaMutationOptions = Apollo.BaseMutationOptions<DeleteVegetatedAreaMutation, DeleteVegetatedAreaMutationVariables>;
export const UpdateVegetatedAreaDocument = gql`
    mutation updateVegetatedArea($id: uuid = "", $_set: vegetated_area_set_input = {}) {
  update_vegetated_area_by_pk(pk_columns: {id: $id}, _set: $_set) {
    id
  }
}
    `;
export type UpdateVegetatedAreaMutationFn = Apollo.MutationFunction<UpdateVegetatedAreaMutation, UpdateVegetatedAreaMutationVariables>;

/**
 * __useUpdateVegetatedAreaMutation__
 *
 * To run a mutation, you first call `useUpdateVegetatedAreaMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateVegetatedAreaMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateVegetatedAreaMutation, { data, loading, error }] = useUpdateVegetatedAreaMutation({
 *   variables: {
 *      id: // value for 'id'
 *      _set: // value for '_set'
 *   },
 * });
 */
export function useUpdateVegetatedAreaMutation(baseOptions?: Apollo.MutationHookOptions<UpdateVegetatedAreaMutation, UpdateVegetatedAreaMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateVegetatedAreaMutation, UpdateVegetatedAreaMutationVariables>(UpdateVegetatedAreaDocument, options);
      }
export type UpdateVegetatedAreaMutationHookResult = ReturnType<typeof useUpdateVegetatedAreaMutation>;
export type UpdateVegetatedAreaMutationResult = Apollo.MutationResult<UpdateVegetatedAreaMutation>;
export type UpdateVegetatedAreaMutationOptions = Apollo.BaseMutationOptions<UpdateVegetatedAreaMutation, UpdateVegetatedAreaMutationVariables>;
export const DeleteVegetatedAreaRelationsDocument = gql`
    mutation deleteVegetatedAreaRelations($id: uuid = "") {
  delete_vegetated_areas_urban_constraints(where: {vegetated_area_id: {_eq: $id}}) {
    affected_rows
  }
  delete_vegetated_areas_residential_usage_types(
    where: {vegetated_area_id: {_eq: $id}}
  ) {
    affected_rows
  }
  delete_boundaries_vegetated_areas(where: {vegetated_area_id: {_eq: $id}}) {
    affected_rows
  }
}
    `;
export type DeleteVegetatedAreaRelationsMutationFn = Apollo.MutationFunction<DeleteVegetatedAreaRelationsMutation, DeleteVegetatedAreaRelationsMutationVariables>;

/**
 * __useDeleteVegetatedAreaRelationsMutation__
 *
 * To run a mutation, you first call `useDeleteVegetatedAreaRelationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteVegetatedAreaRelationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteVegetatedAreaRelationsMutation, { data, loading, error }] = useDeleteVegetatedAreaRelationsMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteVegetatedAreaRelationsMutation(baseOptions?: Apollo.MutationHookOptions<DeleteVegetatedAreaRelationsMutation, DeleteVegetatedAreaRelationsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteVegetatedAreaRelationsMutation, DeleteVegetatedAreaRelationsMutationVariables>(DeleteVegetatedAreaRelationsDocument, options);
      }
export type DeleteVegetatedAreaRelationsMutationHookResult = ReturnType<typeof useDeleteVegetatedAreaRelationsMutation>;
export type DeleteVegetatedAreaRelationsMutationResult = Apollo.MutationResult<DeleteVegetatedAreaRelationsMutation>;
export type DeleteVegetatedAreaRelationsMutationOptions = Apollo.BaseMutationOptions<DeleteVegetatedAreaRelationsMutation, DeleteVegetatedAreaRelationsMutationVariables>;
export const InsertVegetatedAreaRelationsDocument = gql`
    mutation insertVegetatedAreaRelations($boundaryObjects: [boundaries_vegetated_areas_insert_input!]!, $urbanConstraintObjects: [vegetated_areas_urban_constraints_insert_input!]!, $residentialUsageTypeObjects: [vegetated_areas_residential_usage_types_insert_input!]!) {
  insert_boundaries_vegetated_areas(objects: $boundaryObjects) {
    affected_rows
  }
  insert_vegetated_areas_urban_constraints(objects: $urbanConstraintObjects) {
    affected_rows
  }
  insert_vegetated_areas_residential_usage_types(
    objects: $residentialUsageTypeObjects
  ) {
    affected_rows
  }
}
    `;
export type InsertVegetatedAreaRelationsMutationFn = Apollo.MutationFunction<InsertVegetatedAreaRelationsMutation, InsertVegetatedAreaRelationsMutationVariables>;

/**
 * __useInsertVegetatedAreaRelationsMutation__
 *
 * To run a mutation, you first call `useInsertVegetatedAreaRelationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInsertVegetatedAreaRelationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [insertVegetatedAreaRelationsMutation, { data, loading, error }] = useInsertVegetatedAreaRelationsMutation({
 *   variables: {
 *      boundaryObjects: // value for 'boundaryObjects'
 *      urbanConstraintObjects: // value for 'urbanConstraintObjects'
 *      residentialUsageTypeObjects: // value for 'residentialUsageTypeObjects'
 *   },
 * });
 */
export function useInsertVegetatedAreaRelationsMutation(baseOptions?: Apollo.MutationHookOptions<InsertVegetatedAreaRelationsMutation, InsertVegetatedAreaRelationsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InsertVegetatedAreaRelationsMutation, InsertVegetatedAreaRelationsMutationVariables>(InsertVegetatedAreaRelationsDocument, options);
      }
export type InsertVegetatedAreaRelationsMutationHookResult = ReturnType<typeof useInsertVegetatedAreaRelationsMutation>;
export type InsertVegetatedAreaRelationsMutationResult = Apollo.MutationResult<InsertVegetatedAreaRelationsMutation>;
export type InsertVegetatedAreaRelationsMutationOptions = Apollo.BaseMutationOptions<InsertVegetatedAreaRelationsMutation, InsertVegetatedAreaRelationsMutationVariables>;
export const UpdateVegetatedAreaAndDeleteExistingLocationsDocument = gql`
    mutation updateVegetatedAreaAndDeleteExistingLocations($id: uuid!, $coords: geometry!, $locationsIds: [uuid!]!) {
  delete_vegetated_areas_locations(
    where: {location_id: {_in: $locationsIds}, vegetated_area_id: {_eq: $id}}
  ) {
    affected_rows
  }
  update_vegetated_area_by_pk(pk_columns: {id: $id}, _set: {coords: $coords}) {
    id
  }
}
    `;
export type UpdateVegetatedAreaAndDeleteExistingLocationsMutationFn = Apollo.MutationFunction<UpdateVegetatedAreaAndDeleteExistingLocationsMutation, UpdateVegetatedAreaAndDeleteExistingLocationsMutationVariables>;

/**
 * __useUpdateVegetatedAreaAndDeleteExistingLocationsMutation__
 *
 * To run a mutation, you first call `useUpdateVegetatedAreaAndDeleteExistingLocationsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateVegetatedAreaAndDeleteExistingLocationsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateVegetatedAreaAndDeleteExistingLocationsMutation, { data, loading, error }] = useUpdateVegetatedAreaAndDeleteExistingLocationsMutation({
 *   variables: {
 *      id: // value for 'id'
 *      coords: // value for 'coords'
 *      locationsIds: // value for 'locationsIds'
 *   },
 * });
 */
export function useUpdateVegetatedAreaAndDeleteExistingLocationsMutation(baseOptions?: Apollo.MutationHookOptions<UpdateVegetatedAreaAndDeleteExistingLocationsMutation, UpdateVegetatedAreaAndDeleteExistingLocationsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateVegetatedAreaAndDeleteExistingLocationsMutation, UpdateVegetatedAreaAndDeleteExistingLocationsMutationVariables>(UpdateVegetatedAreaAndDeleteExistingLocationsDocument, options);
      }
export type UpdateVegetatedAreaAndDeleteExistingLocationsMutationHookResult = ReturnType<typeof useUpdateVegetatedAreaAndDeleteExistingLocationsMutation>;
export type UpdateVegetatedAreaAndDeleteExistingLocationsMutationResult = Apollo.MutationResult<UpdateVegetatedAreaAndDeleteExistingLocationsMutation>;
export type UpdateVegetatedAreaAndDeleteExistingLocationsMutationOptions = Apollo.BaseMutationOptions<UpdateVegetatedAreaAndDeleteExistingLocationsMutation, UpdateVegetatedAreaAndDeleteExistingLocationsMutationVariables>;
export const FetchLocationStatusDocument = gql`
    query fetchLocationStatus {
  status: location_status {
    id
    status
    color
  }
}
    `;

/**
 * __useFetchLocationStatusQuery__
 *
 * To run a query within a React component, call `useFetchLocationStatusQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchLocationStatusQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchLocationStatusQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchLocationStatusQuery(baseOptions?: Apollo.QueryHookOptions<FetchLocationStatusQuery, FetchLocationStatusQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchLocationStatusQuery, FetchLocationStatusQueryVariables>(FetchLocationStatusDocument, options);
      }
export function useFetchLocationStatusLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchLocationStatusQuery, FetchLocationStatusQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchLocationStatusQuery, FetchLocationStatusQueryVariables>(FetchLocationStatusDocument, options);
        }
export type FetchLocationStatusQueryHookResult = ReturnType<typeof useFetchLocationStatusQuery>;
export type FetchLocationStatusLazyQueryHookResult = ReturnType<typeof useFetchLocationStatusLazyQuery>;
export type FetchLocationStatusQueryResult = Apollo.QueryResult<FetchLocationStatusQuery, FetchLocationStatusQueryVariables>;
export const FetchPageMapDocument = gql`
    query fetchPageMap {
  locations: location(where: {coords: {_is_null: false}}) {
    id
    address
    location_status {
      id
      status
      color
    }
    tree {
      id
      serial_number
      scientific_name
      plantation_date
      height
    }
    coords
  }
}
    `;

/**
 * __useFetchPageMapQuery__
 *
 * To run a query within a React component, call `useFetchPageMapQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchPageMapQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchPageMapQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchPageMapQuery(baseOptions?: Apollo.QueryHookOptions<FetchPageMapQuery, FetchPageMapQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchPageMapQuery, FetchPageMapQueryVariables>(FetchPageMapDocument, options);
      }
export function useFetchPageMapLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchPageMapQuery, FetchPageMapQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchPageMapQuery, FetchPageMapQueryVariables>(FetchPageMapDocument, options);
        }
export type FetchPageMapQueryHookResult = ReturnType<typeof useFetchPageMapQuery>;
export type FetchPageMapLazyQueryHookResult = ReturnType<typeof useFetchPageMapLazyQuery>;
export type FetchPageMapQueryResult = Apollo.QueryResult<FetchPageMapQuery, FetchPageMapQueryVariables>;
export const FetchLocationDocument = gql`
    query fetchLocation {
  status: location_status {
    id
    status
    color
  }
  locations: location {
    id
    location_status {
      id
      status
      color
    }
    tree {
      id
    }
    coords
  }
}
    `;

/**
 * __useFetchLocationQuery__
 *
 * To run a query within a React component, call `useFetchLocationQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchLocationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchLocationQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchLocationQuery(baseOptions?: Apollo.QueryHookOptions<FetchLocationQuery, FetchLocationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchLocationQuery, FetchLocationQueryVariables>(FetchLocationDocument, options);
      }
export function useFetchLocationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchLocationQuery, FetchLocationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchLocationQuery, FetchLocationQueryVariables>(FetchLocationDocument, options);
        }
export type FetchLocationQueryHookResult = ReturnType<typeof useFetchLocationQuery>;
export type FetchLocationLazyQueryHookResult = ReturnType<typeof useFetchLocationLazyQuery>;
export type FetchLocationQueryResult = Apollo.QueryResult<FetchLocationQuery, FetchLocationQueryVariables>;
export const FetchLocationsWithStatusFiltersDocument = gql`
    query fetchLocationsWithStatusFilters($_nin: [uuid!] = [""]) {
  location(where: {id_status: {_nin: $_nin}}) {
    coords
    address
    id
    tree {
      id
    }
    location_status {
      color
      status
    }
  }
}
    `;

/**
 * __useFetchLocationsWithStatusFiltersQuery__
 *
 * To run a query within a React component, call `useFetchLocationsWithStatusFiltersQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchLocationsWithStatusFiltersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchLocationsWithStatusFiltersQuery({
 *   variables: {
 *      _nin: // value for '_nin'
 *   },
 * });
 */
export function useFetchLocationsWithStatusFiltersQuery(baseOptions?: Apollo.QueryHookOptions<FetchLocationsWithStatusFiltersQuery, FetchLocationsWithStatusFiltersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchLocationsWithStatusFiltersQuery, FetchLocationsWithStatusFiltersQueryVariables>(FetchLocationsWithStatusFiltersDocument, options);
      }
export function useFetchLocationsWithStatusFiltersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchLocationsWithStatusFiltersQuery, FetchLocationsWithStatusFiltersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchLocationsWithStatusFiltersQuery, FetchLocationsWithStatusFiltersQueryVariables>(FetchLocationsWithStatusFiltersDocument, options);
        }
export type FetchLocationsWithStatusFiltersQueryHookResult = ReturnType<typeof useFetchLocationsWithStatusFiltersQuery>;
export type FetchLocationsWithStatusFiltersLazyQueryHookResult = ReturnType<typeof useFetchLocationsWithStatusFiltersLazyQuery>;
export type FetchLocationsWithStatusFiltersQueryResult = Apollo.QueryResult<FetchLocationsWithStatusFiltersQuery, FetchLocationsWithStatusFiltersQueryVariables>;
export const FetchLocationsWithFiltersDocument = gql`
    query fetchLocationsWithFilters($where: location_bool_exp = {_and: {}}) {
  locations: location(where: $where) {
    id
    address
    location_status {
      id
      status
      color
    }
    tree {
      id
    }
    coords
  }
}
    `;

/**
 * __useFetchLocationsWithFiltersQuery__
 *
 * To run a query within a React component, call `useFetchLocationsWithFiltersQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchLocationsWithFiltersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchLocationsWithFiltersQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useFetchLocationsWithFiltersQuery(baseOptions?: Apollo.QueryHookOptions<FetchLocationsWithFiltersQuery, FetchLocationsWithFiltersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchLocationsWithFiltersQuery, FetchLocationsWithFiltersQueryVariables>(FetchLocationsWithFiltersDocument, options);
      }
export function useFetchLocationsWithFiltersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchLocationsWithFiltersQuery, FetchLocationsWithFiltersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchLocationsWithFiltersQuery, FetchLocationsWithFiltersQueryVariables>(FetchLocationsWithFiltersDocument, options);
        }
export type FetchLocationsWithFiltersQueryHookResult = ReturnType<typeof useFetchLocationsWithFiltersQuery>;
export type FetchLocationsWithFiltersLazyQueryHookResult = ReturnType<typeof useFetchLocationsWithFiltersLazyQuery>;
export type FetchLocationsWithFiltersQueryResult = Apollo.QueryResult<FetchLocationsWithFiltersQuery, FetchLocationsWithFiltersQueryVariables>;
export const FetchAllInterventionTypesFamiliesDocument = gql`
    query fetchAllInterventionTypesFamilies {
  family_intervention_type {
    slug
    intervention_types {
      id
      slug
      location_types
    }
  }
}
    `;

/**
 * __useFetchAllInterventionTypesFamiliesQuery__
 *
 * To run a query within a React component, call `useFetchAllInterventionTypesFamiliesQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchAllInterventionTypesFamiliesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchAllInterventionTypesFamiliesQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchAllInterventionTypesFamiliesQuery(baseOptions?: Apollo.QueryHookOptions<FetchAllInterventionTypesFamiliesQuery, FetchAllInterventionTypesFamiliesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchAllInterventionTypesFamiliesQuery, FetchAllInterventionTypesFamiliesQueryVariables>(FetchAllInterventionTypesFamiliesDocument, options);
      }
export function useFetchAllInterventionTypesFamiliesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchAllInterventionTypesFamiliesQuery, FetchAllInterventionTypesFamiliesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchAllInterventionTypesFamiliesQuery, FetchAllInterventionTypesFamiliesQueryVariables>(FetchAllInterventionTypesFamiliesDocument, options);
        }
export type FetchAllInterventionTypesFamiliesQueryHookResult = ReturnType<typeof useFetchAllInterventionTypesFamiliesQuery>;
export type FetchAllInterventionTypesFamiliesLazyQueryHookResult = ReturnType<typeof useFetchAllInterventionTypesFamiliesLazyQuery>;
export type FetchAllInterventionTypesFamiliesQueryResult = Apollo.QueryResult<FetchAllInterventionTypesFamiliesQuery, FetchAllInterventionTypesFamiliesQueryVariables>;
export const FetchOneLocationWithTreeIdDocument = gql`
    query fetchOneLocationWithTreeId($_eq: uuid = "") {
  location(where: {tree: {id: {_eq: $_eq}}}) {
    id
    address
    coords
    foot_type
    plantation_type
    sidewalk_type
    landscape_type
    inventory_source
    urban_site {
      id
      slug
    }
    landscape_type
    is_protected
    note
    location_vegetated_area {
      vegetated_area_id
    }
    location_boundaries {
      boundary {
        name
        id
        type
      }
    }
    location_urban_constraints {
      urban_constraint {
        slug
        id
      }
    }
    location_status {
      status
    }
    tree {
      id
      serial_number
    }
  }
}
    `;

/**
 * __useFetchOneLocationWithTreeIdQuery__
 *
 * To run a query within a React component, call `useFetchOneLocationWithTreeIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneLocationWithTreeIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneLocationWithTreeIdQuery({
 *   variables: {
 *      _eq: // value for '_eq'
 *   },
 * });
 */
export function useFetchOneLocationWithTreeIdQuery(baseOptions?: Apollo.QueryHookOptions<FetchOneLocationWithTreeIdQuery, FetchOneLocationWithTreeIdQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneLocationWithTreeIdQuery, FetchOneLocationWithTreeIdQueryVariables>(FetchOneLocationWithTreeIdDocument, options);
      }
export function useFetchOneLocationWithTreeIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneLocationWithTreeIdQuery, FetchOneLocationWithTreeIdQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneLocationWithTreeIdQuery, FetchOneLocationWithTreeIdQueryVariables>(FetchOneLocationWithTreeIdDocument, options);
        }
export type FetchOneLocationWithTreeIdQueryHookResult = ReturnType<typeof useFetchOneLocationWithTreeIdQuery>;
export type FetchOneLocationWithTreeIdLazyQueryHookResult = ReturnType<typeof useFetchOneLocationWithTreeIdLazyQuery>;
export type FetchOneLocationWithTreeIdQueryResult = Apollo.QueryResult<FetchOneLocationWithTreeIdQuery, FetchOneLocationWithTreeIdQueryVariables>;
export const FetchTreeEventsDocument = gql`
    query fetchTreeEvents($_eq: uuid = "e998c127-f285-4a31-aa17-76eb65bb6882") {
  intervention(where: {tree_id: {_eq: $_eq}}, order_by: {scheduled_date: desc}) {
    id
    tree {
      location_id
    }
    intervention_partner {
      name
      id
    }
    intervention_type {
      slug
    }
    scheduled_date
    realization_date
    worksite {
      name
    }
  }
}
    `;

/**
 * __useFetchTreeEventsQuery__
 *
 * To run a query within a React component, call `useFetchTreeEventsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchTreeEventsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchTreeEventsQuery({
 *   variables: {
 *      _eq: // value for '_eq'
 *   },
 * });
 */
export function useFetchTreeEventsQuery(baseOptions?: Apollo.QueryHookOptions<FetchTreeEventsQuery, FetchTreeEventsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchTreeEventsQuery, FetchTreeEventsQueryVariables>(FetchTreeEventsDocument, options);
      }
export function useFetchTreeEventsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchTreeEventsQuery, FetchTreeEventsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchTreeEventsQuery, FetchTreeEventsQueryVariables>(FetchTreeEventsDocument, options);
        }
export type FetchTreeEventsQueryHookResult = ReturnType<typeof useFetchTreeEventsQuery>;
export type FetchTreeEventsLazyQueryHookResult = ReturnType<typeof useFetchTreeEventsLazyQuery>;
export type FetchTreeEventsQueryResult = Apollo.QueryResult<FetchTreeEventsQuery, FetchTreeEventsQueryVariables>;
export const FetchLocationEventsDocument = gql`
    query fetchLocationEvents($_eq: uuid = "e998c127-f285-4a31-aa17-76eb65bb6882") {
  intervention(
    where: {location_id: {_eq: $_eq}}
    order_by: {scheduled_date: desc}
  ) {
    id
    location_id
    intervention_partner {
      name
      id
    }
    intervention_type {
      slug
    }
    scheduled_date
    realization_date
    worksite {
      name
    }
  }
}
    `;

/**
 * __useFetchLocationEventsQuery__
 *
 * To run a query within a React component, call `useFetchLocationEventsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchLocationEventsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchLocationEventsQuery({
 *   variables: {
 *      _eq: // value for '_eq'
 *   },
 * });
 */
export function useFetchLocationEventsQuery(baseOptions?: Apollo.QueryHookOptions<FetchLocationEventsQuery, FetchLocationEventsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchLocationEventsQuery, FetchLocationEventsQueryVariables>(FetchLocationEventsDocument, options);
      }
export function useFetchLocationEventsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchLocationEventsQuery, FetchLocationEventsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchLocationEventsQuery, FetchLocationEventsQueryVariables>(FetchLocationEventsDocument, options);
        }
export type FetchLocationEventsQueryHookResult = ReturnType<typeof useFetchLocationEventsQuery>;
export type FetchLocationEventsLazyQueryHookResult = ReturnType<typeof useFetchLocationEventsLazyQuery>;
export type FetchLocationEventsQueryResult = Apollo.QueryResult<FetchLocationEventsQuery, FetchLocationEventsQueryVariables>;
export const FetchOneLocationDocument = gql`
    query fetchOneLocation($_eq: uuid = "c977674a-ed69-474a-94c6-0259f814fe91") {
  location(where: {id: {_eq: $_eq}}) {
    id
    address
    coords
    foot_type
    plantation_type
    sidewalk_type
    landscape_type
    inventory_source
    urban_site {
      id
      slug
    }
    landscape_type
    is_protected
    note
    id_status
    location_vegetated_area {
      vegetated_area_id
    }
    location_boundaries {
      boundary {
        name
        id
        type
      }
    }
    location_urban_constraints {
      urban_constraint {
        slug
        id
      }
    }
    location_status {
      id
      status
    }
    tree {
      id
    }
    interventions_aggregate(where: {worksite_id: {_is_null: false}}) {
      aggregate {
        count
      }
    }
  }
}
    `;

/**
 * __useFetchOneLocationQuery__
 *
 * To run a query within a React component, call `useFetchOneLocationQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneLocationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneLocationQuery({
 *   variables: {
 *      _eq: // value for '_eq'
 *   },
 * });
 */
export function useFetchOneLocationQuery(baseOptions?: Apollo.QueryHookOptions<FetchOneLocationQuery, FetchOneLocationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneLocationQuery, FetchOneLocationQueryVariables>(FetchOneLocationDocument, options);
      }
export function useFetchOneLocationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneLocationQuery, FetchOneLocationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneLocationQuery, FetchOneLocationQueryVariables>(FetchOneLocationDocument, options);
        }
export type FetchOneLocationQueryHookResult = ReturnType<typeof useFetchOneLocationQuery>;
export type FetchOneLocationLazyQueryHookResult = ReturnType<typeof useFetchOneLocationLazyQuery>;
export type FetchOneLocationQueryResult = Apollo.QueryResult<FetchOneLocationQuery, FetchOneLocationQueryVariables>;
export const FetchAllTreesNotableDocument = gql`
    query fetchAllTreesNotable {
  tree(where: {is_tree_of_interest: {_eq: true}}) {
    id
  }
}
    `;

/**
 * __useFetchAllTreesNotableQuery__
 *
 * To run a query within a React component, call `useFetchAllTreesNotableQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchAllTreesNotableQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchAllTreesNotableQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchAllTreesNotableQuery(baseOptions?: Apollo.QueryHookOptions<FetchAllTreesNotableQuery, FetchAllTreesNotableQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchAllTreesNotableQuery, FetchAllTreesNotableQueryVariables>(FetchAllTreesNotableDocument, options);
      }
export function useFetchAllTreesNotableLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchAllTreesNotableQuery, FetchAllTreesNotableQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchAllTreesNotableQuery, FetchAllTreesNotableQueryVariables>(FetchAllTreesNotableDocument, options);
        }
export type FetchAllTreesNotableQueryHookResult = ReturnType<typeof useFetchAllTreesNotableQuery>;
export type FetchAllTreesNotableLazyQueryHookResult = ReturnType<typeof useFetchAllTreesNotableLazyQuery>;
export type FetchAllTreesNotableQueryResult = Apollo.QueryResult<FetchAllTreesNotableQuery, FetchAllTreesNotableQueryVariables>;
export const FetchAllImportsDocument = gql`
    query fetchAllImports {
  import {
    id
    source_file_path
    user_entity {
      username
    }
    creation_date
  }
}
    `;

/**
 * __useFetchAllImportsQuery__
 *
 * To run a query within a React component, call `useFetchAllImportsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchAllImportsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchAllImportsQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchAllImportsQuery(baseOptions?: Apollo.QueryHookOptions<FetchAllImportsQuery, FetchAllImportsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchAllImportsQuery, FetchAllImportsQueryVariables>(FetchAllImportsDocument, options);
      }
export function useFetchAllImportsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchAllImportsQuery, FetchAllImportsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchAllImportsQuery, FetchAllImportsQueryVariables>(FetchAllImportsDocument, options);
        }
export type FetchAllImportsQueryHookResult = ReturnType<typeof useFetchAllImportsQuery>;
export type FetchAllImportsLazyQueryHookResult = ReturnType<typeof useFetchAllImportsLazyQuery>;
export type FetchAllImportsQueryResult = Apollo.QueryResult<FetchAllImportsQuery, FetchAllImportsQueryVariables>;
export const FetchOneTreeDocument = gql`
    query fetchOneTree($_eq: uuid = "") {
  tree(where: {id: {_eq: $_eq}}) {
    circumference
    estimated_date
    habit
    height
    id
    is_tree_of_interest
    urbasense_subject_id
    note
    felling_date
    taxon_id
    location {
      address
      coords
      id
      location_status {
        status
      }
      location_vegetated_area {
        vegetated_area_id
      }
      location_boundaries {
        boundary {
          name
          id
          type
        }
      }
      interventions_aggregate {
        aggregate {
          count
        }
      }
    }
    plantation_date
    scientific_name
    serial_number
    variety
    vernacular_name
    watering
    last_diagnoses: diagnoses(order_by: {diagnosis_date: desc}, limit: 1) {
      id
      diagnosis_date
      tree_condition
      tree_is_dangerous
      trunk_condition
      crown_condition
      collar_condition
      tree {
        id
      }
    }
  }
}
    `;

/**
 * __useFetchOneTreeQuery__
 *
 * To run a query within a React component, call `useFetchOneTreeQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneTreeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneTreeQuery({
 *   variables: {
 *      _eq: // value for '_eq'
 *   },
 * });
 */
export function useFetchOneTreeQuery(baseOptions?: Apollo.QueryHookOptions<FetchOneTreeQuery, FetchOneTreeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneTreeQuery, FetchOneTreeQueryVariables>(FetchOneTreeDocument, options);
      }
export function useFetchOneTreeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneTreeQuery, FetchOneTreeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneTreeQuery, FetchOneTreeQueryVariables>(FetchOneTreeDocument, options);
        }
export type FetchOneTreeQueryHookResult = ReturnType<typeof useFetchOneTreeQuery>;
export type FetchOneTreeLazyQueryHookResult = ReturnType<typeof useFetchOneTreeLazyQuery>;
export type FetchOneTreeQueryResult = Apollo.QueryResult<FetchOneTreeQuery, FetchOneTreeQueryVariables>;
export const FetchDefaultOrganizationDocument = gql`
    query fetchDefaultOrganization {
  organization(where: {default: {_eq: true}}) {
    image
    name
    population
    surface
    id
    default
    bucket
    domain
    coords
  }
}
    `;

/**
 * __useFetchDefaultOrganizationQuery__
 *
 * To run a query within a React component, call `useFetchDefaultOrganizationQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchDefaultOrganizationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchDefaultOrganizationQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchDefaultOrganizationQuery(baseOptions?: Apollo.QueryHookOptions<FetchDefaultOrganizationQuery, FetchDefaultOrganizationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchDefaultOrganizationQuery, FetchDefaultOrganizationQueryVariables>(FetchDefaultOrganizationDocument, options);
      }
export function useFetchDefaultOrganizationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchDefaultOrganizationQuery, FetchDefaultOrganizationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchDefaultOrganizationQuery, FetchDefaultOrganizationQueryVariables>(FetchDefaultOrganizationDocument, options);
        }
export type FetchDefaultOrganizationQueryHookResult = ReturnType<typeof useFetchDefaultOrganizationQuery>;
export type FetchDefaultOrganizationLazyQueryHookResult = ReturnType<typeof useFetchDefaultOrganizationLazyQuery>;
export type FetchDefaultOrganizationQueryResult = Apollo.QueryResult<FetchDefaultOrganizationQuery, FetchDefaultOrganizationQueryVariables>;
export const FetchInterventionPartnersDocument = gql`
    query fetchInterventionPartners {
  intervention_partner {
    id
    name
  }
}
    `;

/**
 * __useFetchInterventionPartnersQuery__
 *
 * To run a query within a React component, call `useFetchInterventionPartnersQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchInterventionPartnersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchInterventionPartnersQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchInterventionPartnersQuery(baseOptions?: Apollo.QueryHookOptions<FetchInterventionPartnersQuery, FetchInterventionPartnersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchInterventionPartnersQuery, FetchInterventionPartnersQueryVariables>(FetchInterventionPartnersDocument, options);
      }
export function useFetchInterventionPartnersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchInterventionPartnersQuery, FetchInterventionPartnersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchInterventionPartnersQuery, FetchInterventionPartnersQueryVariables>(FetchInterventionPartnersDocument, options);
        }
export type FetchInterventionPartnersQueryHookResult = ReturnType<typeof useFetchInterventionPartnersQuery>;
export type FetchInterventionPartnersLazyQueryHookResult = ReturnType<typeof useFetchInterventionPartnersLazyQuery>;
export type FetchInterventionPartnersQueryResult = Apollo.QueryResult<FetchInterventionPartnersQuery, FetchInterventionPartnersQueryVariables>;
export const FetchInterventionTypesAndPartnersDocument = gql`
    query fetchInterventionTypesAndPartners {
  family_intervention_type {
    slug
    intervention_types {
      id
      slug
      location_types
    }
  }
  intervention_partner {
    id
    name
  }
}
    `;

/**
 * __useFetchInterventionTypesAndPartnersQuery__
 *
 * To run a query within a React component, call `useFetchInterventionTypesAndPartnersQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchInterventionTypesAndPartnersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchInterventionTypesAndPartnersQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchInterventionTypesAndPartnersQuery(baseOptions?: Apollo.QueryHookOptions<FetchInterventionTypesAndPartnersQuery, FetchInterventionTypesAndPartnersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchInterventionTypesAndPartnersQuery, FetchInterventionTypesAndPartnersQueryVariables>(FetchInterventionTypesAndPartnersDocument, options);
      }
export function useFetchInterventionTypesAndPartnersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchInterventionTypesAndPartnersQuery, FetchInterventionTypesAndPartnersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchInterventionTypesAndPartnersQuery, FetchInterventionTypesAndPartnersQueryVariables>(FetchInterventionTypesAndPartnersDocument, options);
        }
export type FetchInterventionTypesAndPartnersQueryHookResult = ReturnType<typeof useFetchInterventionTypesAndPartnersQuery>;
export type FetchInterventionTypesAndPartnersLazyQueryHookResult = ReturnType<typeof useFetchInterventionTypesAndPartnersLazyQuery>;
export type FetchInterventionTypesAndPartnersQueryResult = Apollo.QueryResult<FetchInterventionTypesAndPartnersQuery, FetchInterventionTypesAndPartnersQueryVariables>;
export const FetchDiagnosisFormDataDocument = gql`
    query fetchDiagnosisFormData {
  diagnosis_type {
    id
    slug
  }
  pathogen {
    id
    slug
  }
  analysis_tool {
    id
    slug
  }
}
    `;

/**
 * __useFetchDiagnosisFormDataQuery__
 *
 * To run a query within a React component, call `useFetchDiagnosisFormDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchDiagnosisFormDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchDiagnosisFormDataQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchDiagnosisFormDataQuery(baseOptions?: Apollo.QueryHookOptions<FetchDiagnosisFormDataQuery, FetchDiagnosisFormDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchDiagnosisFormDataQuery, FetchDiagnosisFormDataQueryVariables>(FetchDiagnosisFormDataDocument, options);
      }
export function useFetchDiagnosisFormDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchDiagnosisFormDataQuery, FetchDiagnosisFormDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchDiagnosisFormDataQuery, FetchDiagnosisFormDataQueryVariables>(FetchDiagnosisFormDataDocument, options);
        }
export type FetchDiagnosisFormDataQueryHookResult = ReturnType<typeof useFetchDiagnosisFormDataQuery>;
export type FetchDiagnosisFormDataLazyQueryHookResult = ReturnType<typeof useFetchDiagnosisFormDataLazyQuery>;
export type FetchDiagnosisFormDataQueryResult = Apollo.QueryResult<FetchDiagnosisFormDataQuery, FetchDiagnosisFormDataQueryVariables>;
export const FetchDiagnosisTypesDocument = gql`
    query fetchDiagnosisTypes {
  diagnosis_type {
    id
    slug
  }
}
    `;

/**
 * __useFetchDiagnosisTypesQuery__
 *
 * To run a query within a React component, call `useFetchDiagnosisTypesQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchDiagnosisTypesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchDiagnosisTypesQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchDiagnosisTypesQuery(baseOptions?: Apollo.QueryHookOptions<FetchDiagnosisTypesQuery, FetchDiagnosisTypesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchDiagnosisTypesQuery, FetchDiagnosisTypesQueryVariables>(FetchDiagnosisTypesDocument, options);
      }
export function useFetchDiagnosisTypesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchDiagnosisTypesQuery, FetchDiagnosisTypesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchDiagnosisTypesQuery, FetchDiagnosisTypesQueryVariables>(FetchDiagnosisTypesDocument, options);
        }
export type FetchDiagnosisTypesQueryHookResult = ReturnType<typeof useFetchDiagnosisTypesQuery>;
export type FetchDiagnosisTypesLazyQueryHookResult = ReturnType<typeof useFetchDiagnosisTypesLazyQuery>;
export type FetchDiagnosisTypesQueryResult = Apollo.QueryResult<FetchDiagnosisTypesQuery, FetchDiagnosisTypesQueryVariables>;
export const FetchOneInterventionDocument = gql`
    query fetchOneIntervention($_eq: uuid = "3041c22d-c784-4e93-bbaf-f06d23e3befe") {
  intervention(where: {id: {_eq: $_eq}}) {
    id
    intervention_partner {
      name
      id
    }
    user_entity {
      username
    }
    note
    cost
    realization_date
    intervention_type {
      slug
      id
    }
    scheduled_date
    validation_note
    request_origin
    is_parking_banned
    location {
      address
      coords
      location_status {
        status
      }
      id
    }
    tree {
      serial_number
      id
      location {
        coords
        address
        id
      }
      location_id
    }
    worksite {
      id
    }
  }
}
    `;

/**
 * __useFetchOneInterventionQuery__
 *
 * To run a query within a React component, call `useFetchOneInterventionQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneInterventionQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneInterventionQuery({
 *   variables: {
 *      _eq: // value for '_eq'
 *   },
 * });
 */
export function useFetchOneInterventionQuery(baseOptions?: Apollo.QueryHookOptions<FetchOneInterventionQuery, FetchOneInterventionQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneInterventionQuery, FetchOneInterventionQueryVariables>(FetchOneInterventionDocument, options);
      }
export function useFetchOneInterventionLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneInterventionQuery, FetchOneInterventionQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneInterventionQuery, FetchOneInterventionQueryVariables>(FetchOneInterventionDocument, options);
        }
export type FetchOneInterventionQueryHookResult = ReturnType<typeof useFetchOneInterventionQuery>;
export type FetchOneInterventionLazyQueryHookResult = ReturnType<typeof useFetchOneInterventionLazyQuery>;
export type FetchOneInterventionQueryResult = Apollo.QueryResult<FetchOneInterventionQuery, FetchOneInterventionQueryVariables>;
export const FetchOneDiagnosisDocument = gql`
    query fetchOneDiagnosis($_eq: uuid = "") {
  diagnosis(where: {id: {_eq: $_eq}}) {
    id
    tree_id
    user_entity {
      username
    }
    note
    diagnosis_date
    diagnosis_type {
      slug
      id
    }
    tree_condition
    crown_condition
    trunk_condition
    collar_condition
    carpenters_condition
    tree_is_dangerous
    tree_vigor
    analysis_results
    pathogens: diagnosis_pathogens {
      pathogen {
        slug
        id
      }
    }
    analysis_tools: diagnosis_analysis_tools {
      analysis_tool {
        slug
        id
      }
    }
    recommendation
    tree {
      serial_number
      location {
        id
        address
      }
    }
  }
}
    `;

/**
 * __useFetchOneDiagnosisQuery__
 *
 * To run a query within a React component, call `useFetchOneDiagnosisQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneDiagnosisQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneDiagnosisQuery({
 *   variables: {
 *      _eq: // value for '_eq'
 *   },
 * });
 */
export function useFetchOneDiagnosisQuery(baseOptions?: Apollo.QueryHookOptions<FetchOneDiagnosisQuery, FetchOneDiagnosisQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneDiagnosisQuery, FetchOneDiagnosisQueryVariables>(FetchOneDiagnosisDocument, options);
      }
export function useFetchOneDiagnosisLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneDiagnosisQuery, FetchOneDiagnosisQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneDiagnosisQuery, FetchOneDiagnosisQueryVariables>(FetchOneDiagnosisDocument, options);
        }
export type FetchOneDiagnosisQueryHookResult = ReturnType<typeof useFetchOneDiagnosisQuery>;
export type FetchOneDiagnosisLazyQueryHookResult = ReturnType<typeof useFetchOneDiagnosisLazyQuery>;
export type FetchOneDiagnosisQueryResult = Apollo.QueryResult<FetchOneDiagnosisQuery, FetchOneDiagnosisQueryVariables>;
export const FetchAllDiagnosesDocument = gql`
    query fetchAllDiagnoses {
  diagnosis(order_by: {diagnosis_date: desc}) {
    id
    tree_id
    user_entity {
      username
    }
    note
    diagnosis_date
    diagnosis_type {
      slug
      id
    }
    tree_condition
    crown_condition
    trunk_condition
    collar_condition
    recommendation
    tree {
      serial_number
      location {
        id
        address
      }
    }
  }
  diagnosis_type {
    slug
    id
  }
}
    `;

/**
 * __useFetchAllDiagnosesQuery__
 *
 * To run a query within a React component, call `useFetchAllDiagnosesQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchAllDiagnosesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchAllDiagnosesQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchAllDiagnosesQuery(baseOptions?: Apollo.QueryHookOptions<FetchAllDiagnosesQuery, FetchAllDiagnosesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchAllDiagnosesQuery, FetchAllDiagnosesQueryVariables>(FetchAllDiagnosesDocument, options);
      }
export function useFetchAllDiagnosesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchAllDiagnosesQuery, FetchAllDiagnosesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchAllDiagnosesQuery, FetchAllDiagnosesQueryVariables>(FetchAllDiagnosesDocument, options);
        }
export type FetchAllDiagnosesQueryHookResult = ReturnType<typeof useFetchAllDiagnosesQuery>;
export type FetchAllDiagnosesLazyQueryHookResult = ReturnType<typeof useFetchAllDiagnosesLazyQuery>;
export type FetchAllDiagnosesQueryResult = Apollo.QueryResult<FetchAllDiagnosesQuery, FetchAllDiagnosesQueryVariables>;
export const FetchAllDiagnosesOfATreeDocument = gql`
    query fetchAllDiagnosesOfATree($_eq: uuid = "b6acfb72-97d9-4878-ac31-36efc2d59d34") {
  diagnosis(order_by: {diagnosis_date: desc}, where: {tree_id: {_eq: $_eq}}) {
    id
    tree_id
    user_entity {
      username
    }
    note
    diagnosis_date
    diagnosis_type {
      slug
      id
    }
    tree_condition
    crown_condition
    trunk_condition
    collar_condition
    recommendation
    tree {
      serial_number
      location {
        id
        address
      }
    }
  }
  diagnosis_type {
    slug
    id
  }
}
    `;

/**
 * __useFetchAllDiagnosesOfATreeQuery__
 *
 * To run a query within a React component, call `useFetchAllDiagnosesOfATreeQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchAllDiagnosesOfATreeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchAllDiagnosesOfATreeQuery({
 *   variables: {
 *      _eq: // value for '_eq'
 *   },
 * });
 */
export function useFetchAllDiagnosesOfATreeQuery(baseOptions?: Apollo.QueryHookOptions<FetchAllDiagnosesOfATreeQuery, FetchAllDiagnosesOfATreeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchAllDiagnosesOfATreeQuery, FetchAllDiagnosesOfATreeQueryVariables>(FetchAllDiagnosesOfATreeDocument, options);
      }
export function useFetchAllDiagnosesOfATreeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchAllDiagnosesOfATreeQuery, FetchAllDiagnosesOfATreeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchAllDiagnosesOfATreeQuery, FetchAllDiagnosesOfATreeQueryVariables>(FetchAllDiagnosesOfATreeDocument, options);
        }
export type FetchAllDiagnosesOfATreeQueryHookResult = ReturnType<typeof useFetchAllDiagnosesOfATreeQuery>;
export type FetchAllDiagnosesOfATreeLazyQueryHookResult = ReturnType<typeof useFetchAllDiagnosesOfATreeLazyQuery>;
export type FetchAllDiagnosesOfATreeQueryResult = Apollo.QueryResult<FetchAllDiagnosesOfATreeQuery, FetchAllDiagnosesOfATreeQueryVariables>;
export const FetchDangerousConditionDocument = gql`
    query fetchDangerousCondition {
  diagnosis(
    where: {tree_is_dangerous: {_eq: true}, recommendation: {_eq: "felling"}}
  ) {
    id
    tree_is_dangerous
  }
}
    `;

/**
 * __useFetchDangerousConditionQuery__
 *
 * To run a query within a React component, call `useFetchDangerousConditionQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchDangerousConditionQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchDangerousConditionQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchDangerousConditionQuery(baseOptions?: Apollo.QueryHookOptions<FetchDangerousConditionQuery, FetchDangerousConditionQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchDangerousConditionQuery, FetchDangerousConditionQueryVariables>(FetchDangerousConditionDocument, options);
      }
export function useFetchDangerousConditionLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchDangerousConditionQuery, FetchDangerousConditionQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchDangerousConditionQuery, FetchDangerousConditionQueryVariables>(FetchDangerousConditionDocument, options);
        }
export type FetchDangerousConditionQueryHookResult = ReturnType<typeof useFetchDangerousConditionQuery>;
export type FetchDangerousConditionLazyQueryHookResult = ReturnType<typeof useFetchDangerousConditionLazyQuery>;
export type FetchDangerousConditionQueryResult = Apollo.QueryResult<FetchDangerousConditionQuery, FetchDangerousConditionQueryVariables>;
export const FetchDataAllInterventionsDocument = gql`
    query fetchDataAllInterventions {
  intervention(order_by: {scheduled_date: desc, realization_date: desc}) {
    id
    validation_note
    location_id
    tree_id
    cost
    request_origin
    is_parking_banned
    tree {
      location_id
      location {
        address
        location_status {
          status
        }
      }
      scientific_name
      serial_number
    }
    location {
      address
      location_status {
        status
      }
    }
    intervention_partner {
      name
      id
    }
    intervention_type {
      slug
      location_types
      family_intervention_type {
        slug
      }
    }
    note
    scheduled_date
    realization_date
    worksite {
      name
    }
  }
}
    `;

/**
 * __useFetchDataAllInterventionsQuery__
 *
 * To run a query within a React component, call `useFetchDataAllInterventionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchDataAllInterventionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchDataAllInterventionsQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchDataAllInterventionsQuery(baseOptions?: Apollo.QueryHookOptions<FetchDataAllInterventionsQuery, FetchDataAllInterventionsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchDataAllInterventionsQuery, FetchDataAllInterventionsQueryVariables>(FetchDataAllInterventionsDocument, options);
      }
export function useFetchDataAllInterventionsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchDataAllInterventionsQuery, FetchDataAllInterventionsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchDataAllInterventionsQuery, FetchDataAllInterventionsQueryVariables>(FetchDataAllInterventionsDocument, options);
        }
export type FetchDataAllInterventionsQueryHookResult = ReturnType<typeof useFetchDataAllInterventionsQuery>;
export type FetchDataAllInterventionsLazyQueryHookResult = ReturnType<typeof useFetchDataAllInterventionsLazyQuery>;
export type FetchDataAllInterventionsQueryResult = Apollo.QueryResult<FetchDataAllInterventionsQuery, FetchDataAllInterventionsQueryVariables>;
export const FetchAllInterventionsOfATreeDocument = gql`
    query fetchAllInterventionsOfATree($_eq: uuid = "b6acfb72-97d9-4878-ac31-36efc2d59d34") {
  intervention(
    order_by: {scheduled_date: desc, realization_date: desc}
    where: {tree_id: {_eq: $_eq}}
  ) {
    id
    validation_note
    cost
    tree {
      location {
        address
        location_status {
          status
        }
      }
      scientific_name
      serial_number
    }
    intervention_partner {
      name
      id
    }
    intervention_type {
      slug
      family_intervention_type {
        slug
      }
    }
    note
    scheduled_date
    realization_date
    request_origin
    is_parking_banned
    worksite {
      id
      name
    }
  }
}
    `;

/**
 * __useFetchAllInterventionsOfATreeQuery__
 *
 * To run a query within a React component, call `useFetchAllInterventionsOfATreeQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchAllInterventionsOfATreeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchAllInterventionsOfATreeQuery({
 *   variables: {
 *      _eq: // value for '_eq'
 *   },
 * });
 */
export function useFetchAllInterventionsOfATreeQuery(baseOptions?: Apollo.QueryHookOptions<FetchAllInterventionsOfATreeQuery, FetchAllInterventionsOfATreeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchAllInterventionsOfATreeQuery, FetchAllInterventionsOfATreeQueryVariables>(FetchAllInterventionsOfATreeDocument, options);
      }
export function useFetchAllInterventionsOfATreeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchAllInterventionsOfATreeQuery, FetchAllInterventionsOfATreeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchAllInterventionsOfATreeQuery, FetchAllInterventionsOfATreeQueryVariables>(FetchAllInterventionsOfATreeDocument, options);
        }
export type FetchAllInterventionsOfATreeQueryHookResult = ReturnType<typeof useFetchAllInterventionsOfATreeQuery>;
export type FetchAllInterventionsOfATreeLazyQueryHookResult = ReturnType<typeof useFetchAllInterventionsOfATreeLazyQuery>;
export type FetchAllInterventionsOfATreeQueryResult = Apollo.QueryResult<FetchAllInterventionsOfATreeQuery, FetchAllInterventionsOfATreeQueryVariables>;
export const FetchDataInterventionsGridDocument = gql`
    query fetchDataInterventionsGrid {
  family_intervention_type {
    slug
    intervention_types {
      id
      slug
      location_types
    }
  }
  intervention_partner {
    id
    name
  }
}
    `;

/**
 * __useFetchDataInterventionsGridQuery__
 *
 * To run a query within a React component, call `useFetchDataInterventionsGridQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchDataInterventionsGridQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchDataInterventionsGridQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchDataInterventionsGridQuery(baseOptions?: Apollo.QueryHookOptions<FetchDataInterventionsGridQuery, FetchDataInterventionsGridQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchDataInterventionsGridQuery, FetchDataInterventionsGridQueryVariables>(FetchDataInterventionsGridDocument, options);
      }
export function useFetchDataInterventionsGridLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchDataInterventionsGridQuery, FetchDataInterventionsGridQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchDataInterventionsGridQuery, FetchDataInterventionsGridQueryVariables>(FetchDataInterventionsGridDocument, options);
        }
export type FetchDataInterventionsGridQueryHookResult = ReturnType<typeof useFetchDataInterventionsGridQuery>;
export type FetchDataInterventionsGridLazyQueryHookResult = ReturnType<typeof useFetchDataInterventionsGridLazyQuery>;
export type FetchDataInterventionsGridQueryResult = Apollo.QueryResult<FetchDataInterventionsGridQuery, FetchDataInterventionsGridQueryVariables>;
export const FetchAllInterventionsOfALocationDocument = gql`
    query fetchAllInterventionsOfALocation($_eq: uuid = "13bb9a4a-9131-46e2-8007-5474ee5df6a2") {
  intervention(
    order_by: {scheduled_date: desc, realization_date: desc}
    where: {location_id: {_eq: $_eq}}
  ) {
    id
    location_id
    validation_note
    cost
    request_origin
    is_parking_banned
    intervention_partner {
      name
      id
    }
    intervention_type {
      slug
      family_intervention_type {
        slug
      }
    }
    note
    scheduled_date
    realization_date
    worksite {
      id
      name
    }
    location {
      address
      location_status {
        status
      }
    }
  }
}
    `;

/**
 * __useFetchAllInterventionsOfALocationQuery__
 *
 * To run a query within a React component, call `useFetchAllInterventionsOfALocationQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchAllInterventionsOfALocationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchAllInterventionsOfALocationQuery({
 *   variables: {
 *      _eq: // value for '_eq'
 *   },
 * });
 */
export function useFetchAllInterventionsOfALocationQuery(baseOptions?: Apollo.QueryHookOptions<FetchAllInterventionsOfALocationQuery, FetchAllInterventionsOfALocationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchAllInterventionsOfALocationQuery, FetchAllInterventionsOfALocationQueryVariables>(FetchAllInterventionsOfALocationDocument, options);
      }
export function useFetchAllInterventionsOfALocationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchAllInterventionsOfALocationQuery, FetchAllInterventionsOfALocationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchAllInterventionsOfALocationQuery, FetchAllInterventionsOfALocationQueryVariables>(FetchAllInterventionsOfALocationDocument, options);
        }
export type FetchAllInterventionsOfALocationQueryHookResult = ReturnType<typeof useFetchAllInterventionsOfALocationQuery>;
export type FetchAllInterventionsOfALocationLazyQueryHookResult = ReturnType<typeof useFetchAllInterventionsOfALocationLazyQuery>;
export type FetchAllInterventionsOfALocationQueryResult = Apollo.QueryResult<FetchAllInterventionsOfALocationQuery, FetchAllInterventionsOfALocationQueryVariables>;
export const FetchTaxaSearchResultsDocument = gql`
    query fetchTaxaSearchResults($query: String = "") {
  taxaSearch(query: $query) {
    hits {
      id
      scientific_name
      vernacular_name
      canonical_name
      family
    }
  }
}
    `;

/**
 * __useFetchTaxaSearchResultsQuery__
 *
 * To run a query within a React component, call `useFetchTaxaSearchResultsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchTaxaSearchResultsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchTaxaSearchResultsQuery({
 *   variables: {
 *      query: // value for 'query'
 *   },
 * });
 */
export function useFetchTaxaSearchResultsQuery(baseOptions?: Apollo.QueryHookOptions<FetchTaxaSearchResultsQuery, FetchTaxaSearchResultsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchTaxaSearchResultsQuery, FetchTaxaSearchResultsQueryVariables>(FetchTaxaSearchResultsDocument, options);
      }
export function useFetchTaxaSearchResultsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchTaxaSearchResultsQuery, FetchTaxaSearchResultsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchTaxaSearchResultsQuery, FetchTaxaSearchResultsQueryVariables>(FetchTaxaSearchResultsDocument, options);
        }
export type FetchTaxaSearchResultsQueryHookResult = ReturnType<typeof useFetchTaxaSearchResultsQuery>;
export type FetchTaxaSearchResultsLazyQueryHookResult = ReturnType<typeof useFetchTaxaSearchResultsLazyQuery>;
export type FetchTaxaSearchResultsQueryResult = Apollo.QueryResult<FetchTaxaSearchResultsQuery, FetchTaxaSearchResultsQueryVariables>;
export const FetchAllWorksitesDocument = gql`
    query fetchAllWorksites {
  worksite {
    id
    name
    description
    scheduled_start_date
    scheduled_end_date
    realization_date
    interventions {
      intervention_type {
        slug
      }
      intervention_partner {
        name
      }
      scheduled_date
      realization_date
      location {
        id
        address
        tree {
          scientific_name
          vernacular_name
        }
      }
    }
  }
}
    `;

/**
 * __useFetchAllWorksitesQuery__
 *
 * To run a query within a React component, call `useFetchAllWorksitesQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchAllWorksitesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchAllWorksitesQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchAllWorksitesQuery(baseOptions?: Apollo.QueryHookOptions<FetchAllWorksitesQuery, FetchAllWorksitesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchAllWorksitesQuery, FetchAllWorksitesQueryVariables>(FetchAllWorksitesDocument, options);
      }
export function useFetchAllWorksitesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchAllWorksitesQuery, FetchAllWorksitesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchAllWorksitesQuery, FetchAllWorksitesQueryVariables>(FetchAllWorksitesDocument, options);
        }
export type FetchAllWorksitesQueryHookResult = ReturnType<typeof useFetchAllWorksitesQuery>;
export type FetchAllWorksitesLazyQueryHookResult = ReturnType<typeof useFetchAllWorksitesLazyQuery>;
export type FetchAllWorksitesQueryResult = Apollo.QueryResult<FetchAllWorksitesQuery, FetchAllWorksitesQueryVariables>;
export const CountAllTreesDocument = gql`
    query countAllTrees($_lt: String = "", $_lte: String = "", $_gte: String = "", $_felledBefore: date = "", $_felledAfter: date = "", $where: tree_bool_exp! = {_and: {}}) {
  allTrees: tree_aggregate(
    where: {_and: [{felling_date: {_is_null: true}}, {plantation_date: {_lte: $_lt}}, $where]}
  ) {
    aggregate {
      count
    }
  }
  plantedTrees: tree_aggregate(
    where: {_and: [{plantation_date: {_lte: $_lte}}, {plantation_date: {_gte: $_gte}}, $where]}
  ) {
    aggregate {
      count
    }
  }
  felledTrees: tree_aggregate(
    where: {_and: [{felling_date: {_lte: $_felledBefore}}, {felling_date: {_gte: $_felledAfter}}, $where]}
  ) {
    aggregate {
      count
    }
  }
}
    `;

/**
 * __useCountAllTreesQuery__
 *
 * To run a query within a React component, call `useCountAllTreesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCountAllTreesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCountAllTreesQuery({
 *   variables: {
 *      _lt: // value for '_lt'
 *      _lte: // value for '_lte'
 *      _gte: // value for '_gte'
 *      _felledBefore: // value for '_felledBefore'
 *      _felledAfter: // value for '_felledAfter'
 *      where: // value for 'where'
 *   },
 * });
 */
export function useCountAllTreesQuery(baseOptions?: Apollo.QueryHookOptions<CountAllTreesQuery, CountAllTreesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CountAllTreesQuery, CountAllTreesQueryVariables>(CountAllTreesDocument, options);
      }
export function useCountAllTreesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CountAllTreesQuery, CountAllTreesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CountAllTreesQuery, CountAllTreesQueryVariables>(CountAllTreesDocument, options);
        }
export type CountAllTreesQueryHookResult = ReturnType<typeof useCountAllTreesQuery>;
export type CountAllTreesLazyQueryHookResult = ReturnType<typeof useCountAllTreesLazyQuery>;
export type CountAllTreesQueryResult = Apollo.QueryResult<CountAllTreesQuery, CountAllTreesQueryVariables>;
export const CountAllInterventionsDocument = gql`
    query countAllInterventions($_lte: date = null, $_gte: date = null, $where: intervention_bool_exp! = {_and: {}}) {
  treatment: intervention_aggregate(
    where: {_and: [{intervention_type: {slug: {_eq: "treatment"}}}, {scheduled_date: {_gte: $_gte, _lte: $_lte}}, {realization_date: {_is_null: true}}, $where]}
  ) {
    aggregate {
      count
    }
    nodes {
      scheduled_date
    }
  }
  watering: intervention_aggregate(
    where: {_and: [{intervention_type: {slug: {_eq: "watering"}}}, {scheduled_date: {_gte: $_gte, _lte: $_lte}}, {realization_date: {_is_null: true}}, $where]}
  ) {
    aggregate {
      count
    }
    nodes {
      scheduled_date
    }
  }
  planting: intervention_aggregate(
    where: {_and: [{intervention_type: {slug: {_eq: "planting"}}}, {scheduled_date: {_gte: $_gte, _lte: $_lte}}, {realization_date: {_is_null: true}}, $where]}
  ) {
    aggregate {
      count
    }
    nodes {
      scheduled_date
    }
  }
  pruning: intervention_aggregate(
    where: {_and: [{intervention_type: {slug: {_eq: "pruning"}}}, {scheduled_date: {_gte: $_gte, _lte: $_lte}}, {realization_date: {_is_null: true}}, $where]}
  ) {
    aggregate {
      count
    }
    nodes {
      scheduled_date
    }
  }
}
    `;

/**
 * __useCountAllInterventionsQuery__
 *
 * To run a query within a React component, call `useCountAllInterventionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useCountAllInterventionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCountAllInterventionsQuery({
 *   variables: {
 *      _lte: // value for '_lte'
 *      _gte: // value for '_gte'
 *      where: // value for 'where'
 *   },
 * });
 */
export function useCountAllInterventionsQuery(baseOptions?: Apollo.QueryHookOptions<CountAllInterventionsQuery, CountAllInterventionsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CountAllInterventionsQuery, CountAllInterventionsQueryVariables>(CountAllInterventionsDocument, options);
      }
export function useCountAllInterventionsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CountAllInterventionsQuery, CountAllInterventionsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CountAllInterventionsQuery, CountAllInterventionsQueryVariables>(CountAllInterventionsDocument, options);
        }
export type CountAllInterventionsQueryHookResult = ReturnType<typeof useCountAllInterventionsQuery>;
export type CountAllInterventionsLazyQueryHookResult = ReturnType<typeof useCountAllInterventionsLazyQuery>;
export type CountAllInterventionsQueryResult = Apollo.QueryResult<CountAllInterventionsQuery, CountAllInterventionsQueryVariables>;
export const FetchTreesAggByScientificNamesDocument = gql`
    query fetchTreesAggByScientificNames($where: tree_scientific_names_count_bool_exp! = {_and: {}}) {
  tree_scientific_names_count(
    order_by: {count: desc_nulls_last}
    where: {_and: [$where]}
  ) {
    scientific_name
    count
  }
}
    `;

/**
 * __useFetchTreesAggByScientificNamesQuery__
 *
 * To run a query within a React component, call `useFetchTreesAggByScientificNamesQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchTreesAggByScientificNamesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchTreesAggByScientificNamesQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useFetchTreesAggByScientificNamesQuery(baseOptions?: Apollo.QueryHookOptions<FetchTreesAggByScientificNamesQuery, FetchTreesAggByScientificNamesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchTreesAggByScientificNamesQuery, FetchTreesAggByScientificNamesQueryVariables>(FetchTreesAggByScientificNamesDocument, options);
      }
export function useFetchTreesAggByScientificNamesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchTreesAggByScientificNamesQuery, FetchTreesAggByScientificNamesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchTreesAggByScientificNamesQuery, FetchTreesAggByScientificNamesQueryVariables>(FetchTreesAggByScientificNamesDocument, options);
        }
export type FetchTreesAggByScientificNamesQueryHookResult = ReturnType<typeof useFetchTreesAggByScientificNamesQuery>;
export type FetchTreesAggByScientificNamesLazyQueryHookResult = ReturnType<typeof useFetchTreesAggByScientificNamesLazyQuery>;
export type FetchTreesAggByScientificNamesQueryResult = Apollo.QueryResult<FetchTreesAggByScientificNamesQuery, FetchTreesAggByScientificNamesQueryVariables>;
export const FetchAllBoundariesDocument = gql`
    query fetchAllBoundaries {
  boundary {
    id
    name
    type
  }
}
    `;

/**
 * __useFetchAllBoundariesQuery__
 *
 * To run a query within a React component, call `useFetchAllBoundariesQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchAllBoundariesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchAllBoundariesQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchAllBoundariesQuery(baseOptions?: Apollo.QueryHookOptions<FetchAllBoundariesQuery, FetchAllBoundariesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchAllBoundariesQuery, FetchAllBoundariesQueryVariables>(FetchAllBoundariesDocument, options);
      }
export function useFetchAllBoundariesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchAllBoundariesQuery, FetchAllBoundariesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchAllBoundariesQuery, FetchAllBoundariesQueryVariables>(FetchAllBoundariesDocument, options);
        }
export type FetchAllBoundariesQueryHookResult = ReturnType<typeof useFetchAllBoundariesQuery>;
export type FetchAllBoundariesLazyQueryHookResult = ReturnType<typeof useFetchAllBoundariesLazyQuery>;
export type FetchAllBoundariesQueryResult = Apollo.QueryResult<FetchAllBoundariesQuery, FetchAllBoundariesQueryVariables>;
export const FetchBoundariesExceptStationsDocument = gql`
    query fetchBoundariesExceptStations {
  boundary(where: {type: {_neq: "station"}}) {
    id
    name
    type
  }
}
    `;

/**
 * __useFetchBoundariesExceptStationsQuery__
 *
 * To run a query within a React component, call `useFetchBoundariesExceptStationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchBoundariesExceptStationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchBoundariesExceptStationsQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchBoundariesExceptStationsQuery(baseOptions?: Apollo.QueryHookOptions<FetchBoundariesExceptStationsQuery, FetchBoundariesExceptStationsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchBoundariesExceptStationsQuery, FetchBoundariesExceptStationsQueryVariables>(FetchBoundariesExceptStationsDocument, options);
      }
export function useFetchBoundariesExceptStationsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchBoundariesExceptStationsQuery, FetchBoundariesExceptStationsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchBoundariesExceptStationsQuery, FetchBoundariesExceptStationsQueryVariables>(FetchBoundariesExceptStationsDocument, options);
        }
export type FetchBoundariesExceptStationsQueryHookResult = ReturnType<typeof useFetchBoundariesExceptStationsQuery>;
export type FetchBoundariesExceptStationsLazyQueryHookResult = ReturnType<typeof useFetchBoundariesExceptStationsLazyQuery>;
export type FetchBoundariesExceptStationsQueryResult = Apollo.QueryResult<FetchBoundariesExceptStationsQuery, FetchBoundariesExceptStationsQueryVariables>;
export const FetchOneWorksiteDocument = gql`
    query fetchOneWorksite($id: uuid!) {
  worksite: worksite_by_pk(id: $id) {
    id
    description
    creation_date
    name
    scheduled_start_date
    scheduled_end_date
    realization_date
    cost
    realization_report
    user_entity {
      username
    }
    interventions {
      id
      realization_date
      scheduled_date
      cost
      intervention_type {
        slug
      }
      intervention_partner {
        name
      }
      tree {
        id
        felling_date
        scientific_name
      }
    }
  }
}
    `;

/**
 * __useFetchOneWorksiteQuery__
 *
 * To run a query within a React component, call `useFetchOneWorksiteQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneWorksiteQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneWorksiteQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFetchOneWorksiteQuery(baseOptions: Apollo.QueryHookOptions<FetchOneWorksiteQuery, FetchOneWorksiteQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneWorksiteQuery, FetchOneWorksiteQueryVariables>(FetchOneWorksiteDocument, options);
      }
export function useFetchOneWorksiteLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneWorksiteQuery, FetchOneWorksiteQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneWorksiteQuery, FetchOneWorksiteQueryVariables>(FetchOneWorksiteDocument, options);
        }
export type FetchOneWorksiteQueryHookResult = ReturnType<typeof useFetchOneWorksiteQuery>;
export type FetchOneWorksiteLazyQueryHookResult = ReturnType<typeof useFetchOneWorksiteLazyQuery>;
export type FetchOneWorksiteQueryResult = Apollo.QueryResult<FetchOneWorksiteQuery, FetchOneWorksiteQueryVariables>;
export const FetchOneWorksiteLocationsDocument = gql`
    query fetchOneWorksiteLocations($id: uuid!) {
  worksite: worksite_by_pk(id: $id) {
    id
    interventions(order_by: {realization_date: asc}) {
      id
      realization_date
      scheduled_date
      data_entry_user_id
      intervention_type {
        slug
        id
      }
      intervention_partner {
        id
        name
      }
      location {
        coords
        address
        id
        location_status {
          status
        }
      }
      tree {
        id
        scientific_name
        felling_date
        location {
          address
          coords
          id
          location_status {
            status
          }
        }
      }
    }
  }
}
    `;

/**
 * __useFetchOneWorksiteLocationsQuery__
 *
 * To run a query within a React component, call `useFetchOneWorksiteLocationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneWorksiteLocationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneWorksiteLocationsQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFetchOneWorksiteLocationsQuery(baseOptions: Apollo.QueryHookOptions<FetchOneWorksiteLocationsQuery, FetchOneWorksiteLocationsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneWorksiteLocationsQuery, FetchOneWorksiteLocationsQueryVariables>(FetchOneWorksiteLocationsDocument, options);
      }
export function useFetchOneWorksiteLocationsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneWorksiteLocationsQuery, FetchOneWorksiteLocationsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneWorksiteLocationsQuery, FetchOneWorksiteLocationsQueryVariables>(FetchOneWorksiteLocationsDocument, options);
        }
export type FetchOneWorksiteLocationsQueryHookResult = ReturnType<typeof useFetchOneWorksiteLocationsQuery>;
export type FetchOneWorksiteLocationsLazyQueryHookResult = ReturnType<typeof useFetchOneWorksiteLocationsLazyQuery>;
export type FetchOneWorksiteLocationsQueryResult = Apollo.QueryResult<FetchOneWorksiteLocationsQuery, FetchOneWorksiteLocationsQueryVariables>;
export const FetchOneWorksiteInterventionsDocument = gql`
    query fetchOneWorksiteInterventions($id: uuid!) {
  worksite: worksite_by_pk(id: $id) {
    id
    interventions {
      id
      realization_date
      scheduled_date
      data_entry_user_id
      intervention_type {
        slug
        id
        location_types
      }
      intervention_partner {
        id
        name
      }
      location {
        coords
        address
        id
        location_status {
          status
        }
      }
      tree {
        id
        scientific_name
        felling_date
        location {
          address
          coords
          id
          location_status {
            status
          }
        }
      }
    }
  }
}
    `;

/**
 * __useFetchOneWorksiteInterventionsQuery__
 *
 * To run a query within a React component, call `useFetchOneWorksiteInterventionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneWorksiteInterventionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneWorksiteInterventionsQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFetchOneWorksiteInterventionsQuery(baseOptions: Apollo.QueryHookOptions<FetchOneWorksiteInterventionsQuery, FetchOneWorksiteInterventionsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneWorksiteInterventionsQuery, FetchOneWorksiteInterventionsQueryVariables>(FetchOneWorksiteInterventionsDocument, options);
      }
export function useFetchOneWorksiteInterventionsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneWorksiteInterventionsQuery, FetchOneWorksiteInterventionsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneWorksiteInterventionsQuery, FetchOneWorksiteInterventionsQueryVariables>(FetchOneWorksiteInterventionsDocument, options);
        }
export type FetchOneWorksiteInterventionsQueryHookResult = ReturnType<typeof useFetchOneWorksiteInterventionsQuery>;
export type FetchOneWorksiteInterventionsLazyQueryHookResult = ReturnType<typeof useFetchOneWorksiteInterventionsLazyQuery>;
export type FetchOneWorksiteInterventionsQueryResult = Apollo.QueryResult<FetchOneWorksiteInterventionsQuery, FetchOneWorksiteInterventionsQueryVariables>;
export const FetchOneWorksiteLocationsAllDocument = gql`
    query fetchOneWorksiteLocationsAll($id: uuid!) {
  worksite: worksite_by_pk(id: $id) {
    id
    interventions {
      id
      realization_date
      scheduled_date
      data_entry_user_id
      intervention_type {
        slug
        id
        location_types
      }
      intervention_partner {
        id
        name
      }
      location {
        coords
        address
        id
        location_status {
          status
        }
      }
      tree {
        id
        scientific_name
        felling_date
        location {
          address
          coords
          id
          location_status {
            status
          }
        }
      }
    }
  }
}
    `;

/**
 * __useFetchOneWorksiteLocationsAllQuery__
 *
 * To run a query within a React component, call `useFetchOneWorksiteLocationsAllQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneWorksiteLocationsAllQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneWorksiteLocationsAllQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFetchOneWorksiteLocationsAllQuery(baseOptions: Apollo.QueryHookOptions<FetchOneWorksiteLocationsAllQuery, FetchOneWorksiteLocationsAllQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneWorksiteLocationsAllQuery, FetchOneWorksiteLocationsAllQueryVariables>(FetchOneWorksiteLocationsAllDocument, options);
      }
export function useFetchOneWorksiteLocationsAllLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneWorksiteLocationsAllQuery, FetchOneWorksiteLocationsAllQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneWorksiteLocationsAllQuery, FetchOneWorksiteLocationsAllQueryVariables>(FetchOneWorksiteLocationsAllDocument, options);
        }
export type FetchOneWorksiteLocationsAllQueryHookResult = ReturnType<typeof useFetchOneWorksiteLocationsAllQuery>;
export type FetchOneWorksiteLocationsAllLazyQueryHookResult = ReturnType<typeof useFetchOneWorksiteLocationsAllLazyQuery>;
export type FetchOneWorksiteLocationsAllQueryResult = Apollo.QueryResult<FetchOneWorksiteLocationsAllQuery, FetchOneWorksiteLocationsAllQueryVariables>;
export const GetUrbasenseSubjectMonitoringDocument = gql`
    query getUrbasenseSubjectMonitoring($subjectId: Int!) {
  urbasenseGetSubjectMonitoring(subjectId: $subjectId) {
    tensio {
      Date
      series
      series_name
      unite
      value
    }
  }
}
    `;

/**
 * __useGetUrbasenseSubjectMonitoringQuery__
 *
 * To run a query within a React component, call `useGetUrbasenseSubjectMonitoringQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUrbasenseSubjectMonitoringQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUrbasenseSubjectMonitoringQuery({
 *   variables: {
 *      subjectId: // value for 'subjectId'
 *   },
 * });
 */
export function useGetUrbasenseSubjectMonitoringQuery(baseOptions: Apollo.QueryHookOptions<GetUrbasenseSubjectMonitoringQuery, GetUrbasenseSubjectMonitoringQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUrbasenseSubjectMonitoringQuery, GetUrbasenseSubjectMonitoringQueryVariables>(GetUrbasenseSubjectMonitoringDocument, options);
      }
export function useGetUrbasenseSubjectMonitoringLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUrbasenseSubjectMonitoringQuery, GetUrbasenseSubjectMonitoringQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUrbasenseSubjectMonitoringQuery, GetUrbasenseSubjectMonitoringQueryVariables>(GetUrbasenseSubjectMonitoringDocument, options);
        }
export type GetUrbasenseSubjectMonitoringQueryHookResult = ReturnType<typeof useGetUrbasenseSubjectMonitoringQuery>;
export type GetUrbasenseSubjectMonitoringLazyQueryHookResult = ReturnType<typeof useGetUrbasenseSubjectMonitoringLazyQuery>;
export type GetUrbasenseSubjectMonitoringQueryResult = Apollo.QueryResult<GetUrbasenseSubjectMonitoringQuery, GetUrbasenseSubjectMonitoringQueryVariables>;
export const GetUrbasenseSubjectMonitoringComboDocument = gql`
    query getUrbasenseSubjectMonitoringCombo($subjectId: Int!) {
  urbasenseGetSubjectMonitoringCombo(subjectId: $subjectId) {
    data {
      date_mes
      S_t
      S_1
      S_2
      S_3
      pp_mmjour
      tair_max
    }
    metadata {
      id
      alias
      unite
    }
  }
}
    `;

/**
 * __useGetUrbasenseSubjectMonitoringComboQuery__
 *
 * To run a query within a React component, call `useGetUrbasenseSubjectMonitoringComboQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUrbasenseSubjectMonitoringComboQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUrbasenseSubjectMonitoringComboQuery({
 *   variables: {
 *      subjectId: // value for 'subjectId'
 *   },
 * });
 */
export function useGetUrbasenseSubjectMonitoringComboQuery(baseOptions: Apollo.QueryHookOptions<GetUrbasenseSubjectMonitoringComboQuery, GetUrbasenseSubjectMonitoringComboQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUrbasenseSubjectMonitoringComboQuery, GetUrbasenseSubjectMonitoringComboQueryVariables>(GetUrbasenseSubjectMonitoringComboDocument, options);
      }
export function useGetUrbasenseSubjectMonitoringComboLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUrbasenseSubjectMonitoringComboQuery, GetUrbasenseSubjectMonitoringComboQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUrbasenseSubjectMonitoringComboQuery, GetUrbasenseSubjectMonitoringComboQueryVariables>(GetUrbasenseSubjectMonitoringComboDocument, options);
        }
export type GetUrbasenseSubjectMonitoringComboQueryHookResult = ReturnType<typeof useGetUrbasenseSubjectMonitoringComboQuery>;
export type GetUrbasenseSubjectMonitoringComboLazyQueryHookResult = ReturnType<typeof useGetUrbasenseSubjectMonitoringComboLazyQuery>;
export type GetUrbasenseSubjectMonitoringComboQueryResult = Apollo.QueryResult<GetUrbasenseSubjectMonitoringComboQuery, GetUrbasenseSubjectMonitoringComboQueryVariables>;
export const GetUrbasenseSubjectIdsByBoundaryIdsDocument = gql`
    query getUrbasenseSubjectIdsByBoundaryIds($_in: [uuid!] = "") {
  tree(where: {location: {location_boundaries: {boundary: {id: {_in: $_in}}}}}) {
    urbasense_subject_id
  }
}
    `;

/**
 * __useGetUrbasenseSubjectIdsByBoundaryIdsQuery__
 *
 * To run a query within a React component, call `useGetUrbasenseSubjectIdsByBoundaryIdsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUrbasenseSubjectIdsByBoundaryIdsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUrbasenseSubjectIdsByBoundaryIdsQuery({
 *   variables: {
 *      _in: // value for '_in'
 *   },
 * });
 */
export function useGetUrbasenseSubjectIdsByBoundaryIdsQuery(baseOptions?: Apollo.QueryHookOptions<GetUrbasenseSubjectIdsByBoundaryIdsQuery, GetUrbasenseSubjectIdsByBoundaryIdsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUrbasenseSubjectIdsByBoundaryIdsQuery, GetUrbasenseSubjectIdsByBoundaryIdsQueryVariables>(GetUrbasenseSubjectIdsByBoundaryIdsDocument, options);
      }
export function useGetUrbasenseSubjectIdsByBoundaryIdsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUrbasenseSubjectIdsByBoundaryIdsQuery, GetUrbasenseSubjectIdsByBoundaryIdsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUrbasenseSubjectIdsByBoundaryIdsQuery, GetUrbasenseSubjectIdsByBoundaryIdsQueryVariables>(GetUrbasenseSubjectIdsByBoundaryIdsDocument, options);
        }
export type GetUrbasenseSubjectIdsByBoundaryIdsQueryHookResult = ReturnType<typeof useGetUrbasenseSubjectIdsByBoundaryIdsQuery>;
export type GetUrbasenseSubjectIdsByBoundaryIdsLazyQueryHookResult = ReturnType<typeof useGetUrbasenseSubjectIdsByBoundaryIdsLazyQuery>;
export type GetUrbasenseSubjectIdsByBoundaryIdsQueryResult = Apollo.QueryResult<GetUrbasenseSubjectIdsByBoundaryIdsQuery, GetUrbasenseSubjectIdsByBoundaryIdsQueryVariables>;
export const FetchLocationFormDataDocument = gql`
    query fetchLocationFormData {
  urban_site {
    slug
    id
  }
  urban_constraint {
    slug
    id
  }
  foot_type {
    slug
    id
  }
}
    `;

/**
 * __useFetchLocationFormDataQuery__
 *
 * To run a query within a React component, call `useFetchLocationFormDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchLocationFormDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchLocationFormDataQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchLocationFormDataQuery(baseOptions?: Apollo.QueryHookOptions<FetchLocationFormDataQuery, FetchLocationFormDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchLocationFormDataQuery, FetchLocationFormDataQueryVariables>(FetchLocationFormDataDocument, options);
      }
export function useFetchLocationFormDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchLocationFormDataQuery, FetchLocationFormDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchLocationFormDataQuery, FetchLocationFormDataQueryVariables>(FetchLocationFormDataDocument, options);
        }
export type FetchLocationFormDataQueryHookResult = ReturnType<typeof useFetchLocationFormDataQuery>;
export type FetchLocationFormDataLazyQueryHookResult = ReturnType<typeof useFetchLocationFormDataLazyQuery>;
export type FetchLocationFormDataQueryResult = Apollo.QueryResult<FetchLocationFormDataQuery, FetchLocationFormDataQueryVariables>;
export const FetchVegetatedAreaFormDataDocument = gql`
    query fetchVegetatedAreaFormData {
  boundary {
    id
    name
    type
  }
  urban_site {
    slug
    id
  }
  urban_constraint {
    slug
    id
  }
  residential_usage_type {
    slug
    id
  }
}
    `;

/**
 * __useFetchVegetatedAreaFormDataQuery__
 *
 * To run a query within a React component, call `useFetchVegetatedAreaFormDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchVegetatedAreaFormDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchVegetatedAreaFormDataQuery({
 *   variables: {
 *   },
 * });
 */
export function useFetchVegetatedAreaFormDataQuery(baseOptions?: Apollo.QueryHookOptions<FetchVegetatedAreaFormDataQuery, FetchVegetatedAreaFormDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchVegetatedAreaFormDataQuery, FetchVegetatedAreaFormDataQueryVariables>(FetchVegetatedAreaFormDataDocument, options);
      }
export function useFetchVegetatedAreaFormDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchVegetatedAreaFormDataQuery, FetchVegetatedAreaFormDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchVegetatedAreaFormDataQuery, FetchVegetatedAreaFormDataQueryVariables>(FetchVegetatedAreaFormDataDocument, options);
        }
export type FetchVegetatedAreaFormDataQueryHookResult = ReturnType<typeof useFetchVegetatedAreaFormDataQuery>;
export type FetchVegetatedAreaFormDataLazyQueryHookResult = ReturnType<typeof useFetchVegetatedAreaFormDataLazyQuery>;
export type FetchVegetatedAreaFormDataQueryResult = Apollo.QueryResult<FetchVegetatedAreaFormDataQuery, FetchVegetatedAreaFormDataQueryVariables>;
export const GetLocationsInsidePolygonDocument = gql`
    query getLocationsInsidePolygon($polygon: geometry!) {
  location(where: {coords: {_st_within: $polygon}}) {
    id
  }
  vegetated_area(where: {coords: {_st_within: $polygon}}) {
    id
  }
}
    `;

/**
 * __useGetLocationsInsidePolygonQuery__
 *
 * To run a query within a React component, call `useGetLocationsInsidePolygonQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetLocationsInsidePolygonQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetLocationsInsidePolygonQuery({
 *   variables: {
 *      polygon: // value for 'polygon'
 *   },
 * });
 */
export function useGetLocationsInsidePolygonQuery(baseOptions: Apollo.QueryHookOptions<GetLocationsInsidePolygonQuery, GetLocationsInsidePolygonQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetLocationsInsidePolygonQuery, GetLocationsInsidePolygonQueryVariables>(GetLocationsInsidePolygonDocument, options);
      }
export function useGetLocationsInsidePolygonLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetLocationsInsidePolygonQuery, GetLocationsInsidePolygonQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetLocationsInsidePolygonQuery, GetLocationsInsidePolygonQueryVariables>(GetLocationsInsidePolygonDocument, options);
        }
export type GetLocationsInsidePolygonQueryHookResult = ReturnType<typeof useGetLocationsInsidePolygonQuery>;
export type GetLocationsInsidePolygonLazyQueryHookResult = ReturnType<typeof useGetLocationsInsidePolygonLazyQuery>;
export type GetLocationsInsidePolygonQueryResult = Apollo.QueryResult<GetLocationsInsidePolygonQuery, GetLocationsInsidePolygonQueryVariables>;
export const GetBoundariesWithCoordsDocument = gql`
    query getBoundariesWithCoords($coords: geometry!) {
  boundary(where: {coords: {_st_contains: $coords}}) {
    id
    name
    type
  }
}
    `;

/**
 * __useGetBoundariesWithCoordsQuery__
 *
 * To run a query within a React component, call `useGetBoundariesWithCoordsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetBoundariesWithCoordsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetBoundariesWithCoordsQuery({
 *   variables: {
 *      coords: // value for 'coords'
 *   },
 * });
 */
export function useGetBoundariesWithCoordsQuery(baseOptions: Apollo.QueryHookOptions<GetBoundariesWithCoordsQuery, GetBoundariesWithCoordsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetBoundariesWithCoordsQuery, GetBoundariesWithCoordsQueryVariables>(GetBoundariesWithCoordsDocument, options);
      }
export function useGetBoundariesWithCoordsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetBoundariesWithCoordsQuery, GetBoundariesWithCoordsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetBoundariesWithCoordsQuery, GetBoundariesWithCoordsQueryVariables>(GetBoundariesWithCoordsDocument, options);
        }
export type GetBoundariesWithCoordsQueryHookResult = ReturnType<typeof useGetBoundariesWithCoordsQuery>;
export type GetBoundariesWithCoordsLazyQueryHookResult = ReturnType<typeof useGetBoundariesWithCoordsLazyQuery>;
export type GetBoundariesWithCoordsQueryResult = Apollo.QueryResult<GetBoundariesWithCoordsQuery, GetBoundariesWithCoordsQueryVariables>;
export const GetVegetatedAreaWithCoordsDocument = gql`
    query getVegetatedAreaWithCoords($coords: geometry!) {
  vegetated_area(where: {coords: {_st_contains: $coords}}) {
    id
  }
}
    `;

/**
 * __useGetVegetatedAreaWithCoordsQuery__
 *
 * To run a query within a React component, call `useGetVegetatedAreaWithCoordsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetVegetatedAreaWithCoordsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetVegetatedAreaWithCoordsQuery({
 *   variables: {
 *      coords: // value for 'coords'
 *   },
 * });
 */
export function useGetVegetatedAreaWithCoordsQuery(baseOptions: Apollo.QueryHookOptions<GetVegetatedAreaWithCoordsQuery, GetVegetatedAreaWithCoordsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetVegetatedAreaWithCoordsQuery, GetVegetatedAreaWithCoordsQueryVariables>(GetVegetatedAreaWithCoordsDocument, options);
      }
export function useGetVegetatedAreaWithCoordsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetVegetatedAreaWithCoordsQuery, GetVegetatedAreaWithCoordsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetVegetatedAreaWithCoordsQuery, GetVegetatedAreaWithCoordsQueryVariables>(GetVegetatedAreaWithCoordsDocument, options);
        }
export type GetVegetatedAreaWithCoordsQueryHookResult = ReturnType<typeof useGetVegetatedAreaWithCoordsQuery>;
export type GetVegetatedAreaWithCoordsLazyQueryHookResult = ReturnType<typeof useGetVegetatedAreaWithCoordsLazyQuery>;
export type GetVegetatedAreaWithCoordsQueryResult = Apollo.QueryResult<GetVegetatedAreaWithCoordsQuery, GetVegetatedAreaWithCoordsQueryVariables>;
export const FetchOneBoundaryDocument = gql`
    query fetchOneBoundary($id: uuid!) {
  boundary: boundary_by_pk(id: $id) {
    id
    name
    coords
    type
  }
  boundaries_locations(where: {boundary_id: {_eq: $id}}) {
    id
    location_id
  }
  boundaries_vegetated_areas(where: {boundary_id: {_eq: $id}}) {
    id
    vegetated_area_id
  }
}
    `;

/**
 * __useFetchOneBoundaryQuery__
 *
 * To run a query within a React component, call `useFetchOneBoundaryQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneBoundaryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneBoundaryQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFetchOneBoundaryQuery(baseOptions: Apollo.QueryHookOptions<FetchOneBoundaryQuery, FetchOneBoundaryQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneBoundaryQuery, FetchOneBoundaryQueryVariables>(FetchOneBoundaryDocument, options);
      }
export function useFetchOneBoundaryLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneBoundaryQuery, FetchOneBoundaryQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneBoundaryQuery, FetchOneBoundaryQueryVariables>(FetchOneBoundaryDocument, options);
        }
export type FetchOneBoundaryQueryHookResult = ReturnType<typeof useFetchOneBoundaryQuery>;
export type FetchOneBoundaryLazyQueryHookResult = ReturnType<typeof useFetchOneBoundaryLazyQuery>;
export type FetchOneBoundaryQueryResult = Apollo.QueryResult<FetchOneBoundaryQuery, FetchOneBoundaryQueryVariables>;
export const FetchOneVegetatedAreaDocument = gql`
    query fetchOneVegetatedArea($id: uuid!) {
  vegetated_area: vegetated_area_by_pk(id: $id) {
    id
    coords
    type
    address
    surface
    urban_site_id
    has_differentiated_mowing
    is_accessible
    inconvenience_risk
    note
    shrubs_data
    afforestation_trees_data
    herbaceous_data
    landscape_type
    hedge_type
    linear_meters
    potential_area_state
  }
  administrative_boundary: boundaries_vegetated_areas(
    where: {vegetated_area_id: {_eq: $id}, boundary: {type: {_eq: "administrative"}}}
  ) {
    id
    boundary_id
  }
  geographic_boundary: boundaries_vegetated_areas(
    where: {vegetated_area_id: {_eq: $id}, boundary: {type: {_eq: "geographic"}}}
  ) {
    id
    boundary_id
  }
  station_boundary: boundaries_vegetated_areas(
    where: {vegetated_area_id: {_eq: $id}, boundary: {type: {_eq: "station"}}}
  ) {
    id
    boundary_id
  }
  urban_constraint: vegetated_areas_urban_constraints(
    where: {vegetated_area_id: {_eq: $id}}
  ) {
    urban_constraint {
      id
      slug
    }
  }
  residential_usage_type: vegetated_areas_residential_usage_types(
    where: {vegetated_area_id: {_eq: $id}}
  ) {
    residential_usage_types {
      id
      slug
    }
  }
  locations: vegetated_areas_locations(where: {vegetated_area_id: {_eq: $id}}) {
    location_id
    location {
      location_status {
        status
      }
      tree_aggregate {
        aggregate {
          count
        }
        nodes {
          height
          scientific_name
          id
        }
      }
    }
  }
}
    `;

/**
 * __useFetchOneVegetatedAreaQuery__
 *
 * To run a query within a React component, call `useFetchOneVegetatedAreaQuery` and pass it any options that fit your needs.
 * When your component renders, `useFetchOneVegetatedAreaQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFetchOneVegetatedAreaQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFetchOneVegetatedAreaQuery(baseOptions: Apollo.QueryHookOptions<FetchOneVegetatedAreaQuery, FetchOneVegetatedAreaQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FetchOneVegetatedAreaQuery, FetchOneVegetatedAreaQueryVariables>(FetchOneVegetatedAreaDocument, options);
      }
export function useFetchOneVegetatedAreaLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FetchOneVegetatedAreaQuery, FetchOneVegetatedAreaQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FetchOneVegetatedAreaQuery, FetchOneVegetatedAreaQueryVariables>(FetchOneVegetatedAreaDocument, options);
        }
export type FetchOneVegetatedAreaQueryHookResult = ReturnType<typeof useFetchOneVegetatedAreaQuery>;
export type FetchOneVegetatedAreaLazyQueryHookResult = ReturnType<typeof useFetchOneVegetatedAreaLazyQuery>;
export type FetchOneVegetatedAreaQueryResult = Apollo.QueryResult<FetchOneVegetatedAreaQuery, FetchOneVegetatedAreaQueryVariables>;
export const GetStationBoundaryVegetatedAreaDocument = gql`
    query getStationBoundaryVegetatedArea($vegetated_area_id: uuid = "") {
  boundaries_vegetated_areas(
    where: {vegetated_area_id: {_eq: $vegetated_area_id}, boundary: {type: {_eq: "station"}}}
  ) {
    boundary {
      id
      name
      type
    }
  }
}
    `;

/**
 * __useGetStationBoundaryVegetatedAreaQuery__
 *
 * To run a query within a React component, call `useGetStationBoundaryVegetatedAreaQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetStationBoundaryVegetatedAreaQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetStationBoundaryVegetatedAreaQuery({
 *   variables: {
 *      vegetated_area_id: // value for 'vegetated_area_id'
 *   },
 * });
 */
export function useGetStationBoundaryVegetatedAreaQuery(baseOptions?: Apollo.QueryHookOptions<GetStationBoundaryVegetatedAreaQuery, GetStationBoundaryVegetatedAreaQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetStationBoundaryVegetatedAreaQuery, GetStationBoundaryVegetatedAreaQueryVariables>(GetStationBoundaryVegetatedAreaDocument, options);
      }
export function useGetStationBoundaryVegetatedAreaLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetStationBoundaryVegetatedAreaQuery, GetStationBoundaryVegetatedAreaQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetStationBoundaryVegetatedAreaQuery, GetStationBoundaryVegetatedAreaQueryVariables>(GetStationBoundaryVegetatedAreaDocument, options);
        }
export type GetStationBoundaryVegetatedAreaQueryHookResult = ReturnType<typeof useGetStationBoundaryVegetatedAreaQuery>;
export type GetStationBoundaryVegetatedAreaLazyQueryHookResult = ReturnType<typeof useGetStationBoundaryVegetatedAreaLazyQuery>;
export type GetStationBoundaryVegetatedAreaQueryResult = Apollo.QueryResult<GetStationBoundaryVegetatedAreaQuery, GetStationBoundaryVegetatedAreaQueryVariables>;
export const GetVegetatedAreasFromStationBoundaryDocument = gql`
    query getVegetatedAreasFromStationBoundary($boundary_id: uuid = "") {
  vegetated_area_aggregate(
    where: {boundaries_vegetated_areas: {boundary_id: {_eq: $boundary_id}, boundary: {type: {_eq: "station"}}}}
  ) {
    aggregate {
      count
      sum {
        surface
      }
    }
    nodes {
      type
      shrubs_data
      herbaceous_data
      afforestation_trees_data
    }
  }
}
    `;

/**
 * __useGetVegetatedAreasFromStationBoundaryQuery__
 *
 * To run a query within a React component, call `useGetVegetatedAreasFromStationBoundaryQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetVegetatedAreasFromStationBoundaryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetVegetatedAreasFromStationBoundaryQuery({
 *   variables: {
 *      boundary_id: // value for 'boundary_id'
 *   },
 * });
 */
export function useGetVegetatedAreasFromStationBoundaryQuery(baseOptions?: Apollo.QueryHookOptions<GetVegetatedAreasFromStationBoundaryQuery, GetVegetatedAreasFromStationBoundaryQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetVegetatedAreasFromStationBoundaryQuery, GetVegetatedAreasFromStationBoundaryQueryVariables>(GetVegetatedAreasFromStationBoundaryDocument, options);
      }
export function useGetVegetatedAreasFromStationBoundaryLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetVegetatedAreasFromStationBoundaryQuery, GetVegetatedAreasFromStationBoundaryQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetVegetatedAreasFromStationBoundaryQuery, GetVegetatedAreasFromStationBoundaryQueryVariables>(GetVegetatedAreasFromStationBoundaryDocument, options);
        }
export type GetVegetatedAreasFromStationBoundaryQueryHookResult = ReturnType<typeof useGetVegetatedAreasFromStationBoundaryQuery>;
export type GetVegetatedAreasFromStationBoundaryLazyQueryHookResult = ReturnType<typeof useGetVegetatedAreasFromStationBoundaryLazyQuery>;
export type GetVegetatedAreasFromStationBoundaryQueryResult = Apollo.QueryResult<GetVegetatedAreasFromStationBoundaryQuery, GetVegetatedAreasFromStationBoundaryQueryVariables>;
export const CountTreesFromBoundaryDocument = gql`
    query countTreesFromBoundary($boundary_id: uuid = "") {
  boundaries_locations_aggregate(
    where: {boundary_id: {_eq: $boundary_id}, location: {location_status: {status: {_eq: "alive"}}}}
  ) {
    aggregate {
      count
    }
  }
}
    `;

/**
 * __useCountTreesFromBoundaryQuery__
 *
 * To run a query within a React component, call `useCountTreesFromBoundaryQuery` and pass it any options that fit your needs.
 * When your component renders, `useCountTreesFromBoundaryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCountTreesFromBoundaryQuery({
 *   variables: {
 *      boundary_id: // value for 'boundary_id'
 *   },
 * });
 */
export function useCountTreesFromBoundaryQuery(baseOptions?: Apollo.QueryHookOptions<CountTreesFromBoundaryQuery, CountTreesFromBoundaryQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CountTreesFromBoundaryQuery, CountTreesFromBoundaryQueryVariables>(CountTreesFromBoundaryDocument, options);
      }
export function useCountTreesFromBoundaryLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CountTreesFromBoundaryQuery, CountTreesFromBoundaryQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CountTreesFromBoundaryQuery, CountTreesFromBoundaryQueryVariables>(CountTreesFromBoundaryDocument, options);
        }
export type CountTreesFromBoundaryQueryHookResult = ReturnType<typeof useCountTreesFromBoundaryQuery>;
export type CountTreesFromBoundaryLazyQueryHookResult = ReturnType<typeof useCountTreesFromBoundaryLazyQuery>;
export type CountTreesFromBoundaryQueryResult = Apollo.QueryResult<CountTreesFromBoundaryQuery, CountTreesFromBoundaryQueryVariables>;