import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

export const getKeycloackUsers = async (userToken): Promise<any> => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      }
    );

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const getKeycloackUser = async (userId, userToken): Promise<any> => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/user`,
      {
        method: "GET",
        headers: {
          "user-id": userId,
          Authorization: `Bearer ${userToken}`,
        },
      }
    );

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};

export const deleteUser = async (userId, userToken) => {
  const response = await fetch(
    `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users/delete`,
    {
      method: "DELETE",
      headers: {
        "user-id": userId,
        Authorization: `Bearer ${userToken}`,
      },
    }
  );
  if (!response.ok) {
    const error = new Error(`HTTP error! status: ${response.status}`);
    (error as any).status = response.status;
    throw error;
  }
  return await response.json();
};

export const disableUser = async (userId, userToken) => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users/disable`,
      {
        method: "PUT",
        headers: {
          "user-id": userId,
          Authorization: `Bearer ${userToken}`,
        },
      }
    );
    return await response.json();
  } catch (error) {
    console.error("Error disabling user: ", error);
    throw error;
  }
};

export const updateUser = async (userId, userData, userToken) => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users/update`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "user-id": userId,
          Authorization: `Bearer ${userToken}`,
        },
        body: JSON.stringify(userData),
      }
    );

    return await response.json();
  } catch (error) {
    console.error("Error updating user: ", error);
    throw error;
  }
};

export const createUser = async (userData, userToken) => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users/create`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userToken}`,
        },
        body: JSON.stringify(userData),
      }
    );
    return await response.json();
  } catch (error) {
    console.error("Error creating user: ", error);
    throw error;
  }
};

export const setPassword = async (userId, newPassword, userToken) => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/users/password`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "user-id": userId,
          Authorization: `Bearer ${userToken}`,
        },
        body: JSON.stringify(newPassword),
      }
    );
    return await response.json();
  } catch (error) {
    console.error("Error resetting password: ", error);
    throw error;
  }
};

export const getKeycloackRoles = async (userToken): Promise<any> => {
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/keycloack/roles`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${userToken}`,
        },
      }
    );

    const data = await response.json();

    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};
