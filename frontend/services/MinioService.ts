import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

export const moveMinioFile = async (
  oldPath: string,
  newPath: string,
  bucketName: string,
  userToken: string
): Promise<any> => {
  const params = {
    old_path: oldPath,
    new_path: newPath,
    bucket_name: bucketName,
  };
  try {
    const response = await fetch(
      `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/minio/move-file-in-minio`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json", // Add this line
          Authorization: `Bearer ${userToken}`,
        },
        body: JSON.stringify(params),
      }
    );

    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error fetching data: ", error);
    throw error;
  }
};
