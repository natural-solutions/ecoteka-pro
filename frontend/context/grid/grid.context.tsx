import React, {
  createContext,
  useState,
  useContext,
  Dispatch,
  SetStateAction,
} from "react";

interface GridContextProps {
  rowSelected: RowSelectionState | {} | undefined;
  setRowSelected: Dispatch<SetStateAction<RowSelectionState | {} | undefined>>;
  selectedLocationIds: string[];
  setSelectedLocationIds: Dispatch<SetStateAction<string[]>>;
}

const GridContext = createContext<GridContextProps | null>(null);

type RowSelectionState = {
  [key: string]: boolean;
};

const GridProvider = ({ children }) => {
  const [rowSelected, setRowSelected] = useState<
    RowSelectionState | {} | undefined
  >({});
  const [selectedLocationIds, setSelectedLocationIds] = useState<string[]>([]);

  return (
    <GridContext.Provider
      value={{
        rowSelected,
        setRowSelected,
        selectedLocationIds,
        setSelectedLocationIds,
      }}
    >
      {children}
    </GridContext.Provider>
  );
};

export default GridProvider;

export const useGridContext = () => {
  return useContext(GridContext);
};
