import React from 'react';
import Box from '@mui/material/Box';
import PersonalInformations from "@components/_pages/Account/UserInfos/PersonalInformation"
import EmailNotification from "@components/_pages/Account/UserInfos/EmailNotification"
import { useDeviceSize } from "@lib/utils";

const UserInfos = () => {
  const { isMobile } = useDeviceSize();

  return (
    <Box sx={{display:"flex", flexDirection: isMobile ? "column" : "row", justifyContent: "space-between", padding: 2, mb:5}}>
      <PersonalInformations />
      <EmailNotification />
    </Box>
  );
};

export default UserInfos;
