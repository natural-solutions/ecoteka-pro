import React, { useState, useEffect } from "react";
import {
  Typography,
  Paper,
  Box,
  TextField,
  IconButton,
  Tooltip,
  Button,
  InputAdornment,
  FormHelperText,
  FormControl,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import CloseIcon from "@mui/icons-material/Close";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { useTranslation } from "react-i18next";
import { useSession } from "next-auth/react";
import { useDeviceSize } from "@lib/utils";
import type { Profile } from "next-auth";
import useBgColor, { UseBgColorType } from "@core/hooks/useBgColor";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import {
  getKeycloackUser,
  setPassword,
  updateUser,
} from "@services/AccountService";
import { GlobalLoader } from "@components/layout/GlobalLoader";
import Loading from "@components/layout/Loading";

interface KeycloakUser {
  id: string;
  sub: string;
  name: string;
  enabled: boolean;
  totp: boolean;
  emailVerified: boolean;
  firstName: string;
  lastName: string;
  username: string;
  given_name: string;
  family_name: string;
  email: string;
  attributes: {
    role: string[];
    passwordLength?: number;
    team?: string;
  };
  disableableCredentialTypes: string[];
  requiredActions: string[];
  notBefore: number;
  access: {
    manageGroupMembership: boolean;
    view: boolean;
    mapRoles: boolean;
    impersonate: boolean;
    manage: boolean;
  };
}

const PersonalInformations = () => {
  const session = useSession();
  const currentUser = session?.data?.user as Profile | undefined;
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";
  const bgColors: UseBgColorType = useBgColor();
  const { t } = useTranslation(["components", "common"]);
  const { isMobile } = useDeviceSize();
  const [fieldsDisabled, setFieldsDisabled] = useState<boolean>(true);
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [actionType, setActionType] = useState<"save" | "cancel" | "">("");
  const [userLogged, setUserLogged] = useState<KeycloakUser>(
    {} as KeycloakUser
  );
  const [passwordValue, setPasswordValue] = useState<string>("");
  const [passwordLength, setPasswordLength] = useState<number>(7);
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [passwordFocus, setPasswordFocus] = useState<boolean>(false);
  const [passwordValid, setPasswordValid] = useState<boolean>(true);
  const [hasChanged, setHasChanged] = useState<boolean>(false);
  const [passwordEdited, setPasswordEdited] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const handleOpenModal = (type: "save" | "cancel") => {
    setModalOpen(true);
    setActionType(type);
  };

  const handleCloseModal = () => {
    setModalOpen(false);
    setActionType("");
  };

  const toggleFieldsDisabled = () => {
    setFieldsDisabled(!fieldsDisabled);
  };

  const getUser = async () => {
    try {
      setIsLoading(true);
      const user = await getKeycloackUser(currentUser?.id, currentUserToken);
      if (user) {
        setUserLogged(user);
        if (user.attributes?.passwordLength) {
          setPasswordValue(" ".repeat(user.attributes.passwordLength));
          setPasswordLength(user.attributes.passwordLength);
        } else {
          setPasswordValue(" ".repeat(7));
        }
      }
    } catch (error) {
      console.error(
        "Erreur lors de la récupération des utilisateurs Keycloak",
        error
      );
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (currentUser) {
      getUser();
    }
  }, [currentUser]);

  const handlePasswordFocus = () => {
    setPasswordFocus(true);
    if (!passwordEdited) {
      setPasswordValue("");
    }
  };

  const handlePasswordBlur = (e: any) => {
    if (e.target.value === "") {
      setPasswordEdited(false);
      setPasswordValue(" ".repeat(passwordLength));
      setIsEditing(false);
      setPasswordFocus(false);
    }
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleFieldChange = (field, value) => {
    if (field === "password") {
      setPasswordValue(value);
      setIsEditing(true);
      setPasswordValid(value.length >= 7);
      if (!passwordEdited) {
        setPasswordEdited(true);
      }
      setUserLogged((prev) => ({
        ...prev,
        attributes: {
          ...prev.attributes,
          passwordLength: value.length,
        },
      }));
    } else {
      setUserLogged((prev) => ({ ...prev, [field]: value }));
    }
    setHasChanged(true);
  };

  const handleSave = async () => {
    if (hasChanged) {
      setIsLoading(true);
      try {
        await updateUser(userLogged.id, userLogged, currentUserToken);
        if (passwordEdited && passwordValid) {
          await setPassword(userLogged.id, passwordValue, currentUserToken);
        }
      } finally {
        setIsLoading(false);
      }
      setHasChanged(false);
      setModalOpen(false);
      setActionType("");
      setPasswordFocus(false);
      setFieldsDisabled(!fieldsDisabled);
    }
  };

  const resetUserInfo = async () => {
    getUser();
    setFieldsDisabled(!fieldsDisabled);
  };

  const handleCloseClick = () => {
    resetUserInfo();
    setModalOpen(false);
  };

  if (isLoading) {
    return <Loading />;
  }

  return (
    <Paper
      elevation={1}
      data-cy="account-personal-information"
      sx={{
        padding: "10px 20px",
        mb: isMobile ? 4 : "",
        width: isMobile ? "100%" : "70%",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "space-between",
          mb: 1,
          mt: 0.5,
        }}
      >
        <Typography sx={{ fontSize: 18, fontWeight: 500 }}>
          {t("Account.personalInformation.title")}
        </Typography>
        <Tooltip
          title={fieldsDisabled ? t("Account.edit") : t("Account.cancel")}
        >
          <IconButton
            onClick={() => {
              if (!fieldsDisabled) {
                handleOpenModal("cancel");
              } else {
                toggleFieldsDisabled();
              }
            }}
            sx={{
              fontSize: 18,
              fontWeight: 500,
              mr: 2,
              mt: "5px",
              backgroundColor: fieldsDisabled
                ? "primary.main"
                : bgColors.errorLight.backgroundColor,
              color: "primary.contrastText",
              "&:hover": {
                backgroundColor: fieldsDisabled
                  ? "primary.dark"
                  : bgColors.errorFilled.backgroundColor,
              },
            }}
          >
            {fieldsDisabled ? (
              <EditIcon sx={{ fontSize: 18 }} />
            ) : (
              <CloseIcon sx={{ fontSize: 18 }} />
            )}
          </IconButton>
        </Tooltip>
      </Box>
      {userLogged && (
        <Box
          component="form"
          sx={{
            display: "flex",
            flexWrap: "wrap",
            flexDirection: isMobile ? "column" : "row",
            justifyContent: "space-between",
          }}
        >
          <TextField
            disabled={fieldsDisabled}
            label={t("Account.personalInformation.name")}
            value={userLogged?.lastName || ""}
            variant="outlined"
            onChange={(e) => handleFieldChange("lastName", e.target.value)}
            sx={{ width: isMobile ? "95%" : "48.5%", my: 1.5 }}
          />
          <TextField
            disabled={fieldsDisabled}
            label={t("Account.personalInformation.firstname")}
            value={userLogged?.firstName || ""}
            onChange={(e) => handleFieldChange("firstName", e.target.value)}
            variant="outlined"
            sx={{ width: isMobile ? "95%" : "48.5%", my: 1.5 }}
          />
          <TextField
            disabled={true}
            label={t("Account.personalInformation.username")}
            value={userLogged?.username || ""}
            variant="outlined"
            sx={{ width: isMobile ? "95%" : "48.5%", my: 1.5 }}
          />
          <TextField
            disabled={fieldsDisabled}
            label={t("SignIn.labelUsername")}
            value={userLogged?.email || ""}
            onChange={(e) => handleFieldChange("email", e.target.value)}
            variant="outlined"
            sx={{ width: isMobile ? "95%" : "48.5%", my: 1.5 }}
          />
          <FormControl
            variant="outlined"
            fullWidth
            sx={{
              mt: 1.5,
              mb: passwordFocus ? 0 : 1.5,
              width: isMobile ? "95%" : "48.5%",
            }}
          >
            <TextField
              disabled={fieldsDisabled}
              label={t("SignIn.labelPassword")}
              type={showPassword ? "text" : "password"}
              value={passwordValue}
              onChange={(e) => handleFieldChange("password", e.target.value)}
              autoComplete="off"
              onFocus={handlePasswordFocus}
              onBlur={handlePasswordBlur}
              error={!passwordValid && passwordFocus}
              InputProps={{
                endAdornment:
                  isEditing && !fieldsDisabled ? (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ) : null,
              }}
            />
            {passwordFocus && isEditing && (
              <FormHelperText
                id="password-helper-text"
                sx={{
                  fontSize: "10px",
                  color: passwordValid ? "primary.main" : "red",
                  fontWeight: 500,
                }}
              >
                {t("Account.passwordPolicies")}
              </FormHelperText>
            )}
          </FormControl>
        </Box>
      )}
      {hasChanged && (!passwordEdited || (passwordEdited && passwordValid)) && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            width: "100%",
            my: 1,
          }}
        >
          <Button onClick={() => handleOpenModal("save")} sx={{ py: 1, px: 5 }}>
            {t("Account.save")}
          </Button>
        </Box>
      )}
      {isLoading ? (
        <GlobalLoader sx={{ backgroundColor: "rgba(0,0,0,.4)" }} />
      ) : (
        <ModalComponent open={modalOpen} handleClose={handleCloseModal}>
          <Box sx={{ textAlign: "center" }}>
            {actionType === "save" ? (
              <Typography sx={{ m: 3, fontSize: 20, fontWeight: 500 }}>
                {t("Account.modal.saveMessage")}
              </Typography>
            ) : actionType === "cancel" ? (
              <Typography sx={{ m: 3, fontSize: 20, fontWeight: 500 }}>
                {t("Account.modal.cancelMessage")}
              </Typography>
            ) : null}
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-around",
                mt: 6,
                mb: 3,
              }}
            >
              <Button
                sx={{ width: "35%" }}
                variant="outlined"
                onClick={handleCloseModal}
              >
                {t("Account.modal.cancel")}
              </Button>
              <Button
                sx={{ width: "35%" }}
                variant="contained"
                onClick={() => {
                  actionType === "save" ? handleSave() : handleCloseClick();
                }}
              >
                {t("Account.modal.confirm")}
              </Button>
            </Box>
          </Box>
        </ModalComponent>
      )}
    </Paper>
  );
};

export default PersonalInformations;
