import React from 'react';
import Typography from '@mui/material/Typography';
import { Paper, Box, Switch, FormControlLabel, InputLabel, Select, FormControl, OutlinedInput, MenuItem, ListItemText, Checkbox } from '@mui/material';
import { useTranslation } from "react-i18next";
import { useDeviceSize } from "@lib/utils";

const EmailNotification= () => {
  const { t } = useTranslation(["components", "common"]);
  const { isMobile } = useDeviceSize();

  return (
    <Paper elevation={1} data-cy="account-email-notification" sx={{minWidth: "30vw", padding: "10px 20px", ml: isMobile ? "" : 3, pb: isMobile ? 3 : ""}}>
      <Typography
       sx={{ fontSize: 18, mb: 1, fontWeight: 500 }}
       >
        {t("Account.emailNotification.title")}
      </Typography>
      <Box
        component="form"
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between"
        }}
      >
        <FormControlLabel disabled control={<Switch />} sx={{ml: 1, mt: 2}} label={t("Account.emailNotification.active")} />
        <FormControl disabled fullWidth sx={{mt: 3}} >
          <InputLabel>{t("Account.emailNotification.title")}</InputLabel>
          <Select
            // value={age}
            multiple
            label={t("Account.emailNotification.title")}
            input={<OutlinedInput label={t("Account.emailNotification.title")} />}
            // onChange={handleChange}
            renderValue={(selected: string[]) => selected.join(', ')}
          >

            {/* {object.map((item) => (
              <MenuItem key={item} value={item}>
                <Checkbox checked={objectState.indexOf(item) > -1} />
                <ListItemText primary={item} />
              </MenuItem>
            ))}
             */}

          </Select>
        </FormControl>
      </Box>
    </Paper>
  );
};

export default EmailNotification;
