import React from "react";
import { Box, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import Weather from "@components/_core/icons/Account/Weather";
import Soil from "@components/_core/icons/Account/Soil";
import Health from "@components/_core/icons/Account/Health";
import Dendro from "@components/_core/icons/Account/Dendro";
import { DashboardWidgetItem } from "./DashboardWidgetItem";
import getConfig from "next/config";

const DashboardWidget = () => {
  const { t } = useTranslation(["components", "common"]);
  const { publicRuntimeConfig } = getConfig();

  const includeMeteoWidget = ["True", "true"].includes(
    publicRuntimeConfig.URBASENSE_DASHBOARD_ACTIVE
  );

  const widgets = [
    includeMeteoWidget
      ? {
          icon: Weather,
          title: t("dashboard.urbasense.meteo.title"),
          description: t("dashboard.urbasense.meteo.desc"),
          dataCy: "account-widget-weather",
        }
      : null,
    {
      icon: Dendro,
      title: "Dendro",
      description: t("dashboard.urbasense.meteo.desc"),
      dataCy: "account-widget-dendro",
    },
    {
      icon: Soil,
      title: t("Account.widget.soil"),
      description: t("Account.widget.soilDesc"),
      dataCy: "account-widget-soil",
    },
    {
      icon: Health,
      title: t("Account.widget.health"),
      description: t("Account.widget.healthDesc"),
      dataCy: "account-widget-health",
    },
  ];

  return (
    <>
      <Typography sx={{ py: 1, pl: 3, fontSize: 18, fontWeight: 500 }}>
        {t("Account.widget.title")}
      </Typography>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "space-between",
          padding: 2,
        }}
      >
        {widgets.map(
          (widget, index) =>
            widget && <DashboardWidgetItem key={index} {...widget} />
        )}
      </Box>
    </>
  );
};

export default DashboardWidget;
