import React, { useMemo, useState, useEffect } from "react";
import {
  MRT_ColumnDef,
  useMaterialReactTable,
  MaterialReactTable,
} from "material-react-table";
import InfoIcon from "@mui/icons-material/Info";
import {
  Box,
  Typography,
  IconButton,
  Button,
  Tooltip,
  TextField,
  Container,
  Grid,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Alert,
} from "@mui/material";
import { format } from "date-fns";
import { useTranslation } from "react-i18next";
import { useDeviceSize } from "@lib/utils";
import {
  getKeycloackUsers,
  deleteUser,
  createUser,
  getKeycloackRoles,
  disableUser,
} from "@services/AccountService";
import DeleteIcon from "@mui/icons-material/Delete";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import { GlobalLoader } from "@components/layout/GlobalLoader";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import {
  UserData,
  KeycloakUser,
  KeycloakRole,
} from "types/Account/Account.types";
import useTableLocale from "@lib/useTableLocale";
import { useSession } from "next-auth/react";
import ConfirmDialog from "@components/forms/dialog/ConfirmDialog";

const AccountGrid = () => {
  const { t } = useTranslation("components");
  const session = useSession();
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";
  const { isMobile } = useDeviceSize();
  const [usersData, setUsersData] = useState<UserData[]>([]);
  const localization = useTableLocale();
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [actionType, setActionType] = useState<
    "edit" | "create" | "delete" | ""
  >("");
  const [selectedUser, setSelectedUser] = useState<UserData>({} as UserData);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [reloadCounter, setReloadCounter] = useState<number>(0);
  const [formErrors, setFormErrors] = useState({});
  const [roles, setRoles] = useState<KeycloakRole[]>([]);
  const [showConfirmDisable, setShowConfirmDisable] = useState(false);
  const [newUser, setNewUser] = useState<UserData>({
    id: "",
    firstName: "",
    lastName: "",
    username: "",
    email: "",
    role: "",
    createdTimestamp: undefined,
  } as UserData);
  const excludedKeys: string[] = ["id", "role", "createdTimestamp", "username"];

  const columns = useMemo(() => {
    if (usersData && usersData.length > 0) {
      const columnName: string[] = Object.keys(usersData[0]).filter(
        (key) => key !== "__typename"
      );

      return columnName.map((key) => ({
        id: key,
        accessorKey: key,
        header: t(`Account.grid.${key}`),
        Cell:
          key === "createdTimestamp"
            ? ({ cell }) =>
                cell.getValue()
                  ? format(
                      new Date(cell.getValue() as string | number),
                      "dd/MM/yyyy"
                    )
                  : ""
            : key === "role"
              ? ({ cell, row }) => {
                  if (row.original.username === "ecoteka support") {
                    return t("common.roles.support");
                  }
                  return t(`common.roles.${cell.getValue()}`);
                }
              : key === "enabled"
                ? ({ cell }) =>
                    cell.getValue() === true
                      ? t("Account.grid.active")
                      : t("Account.grid.disabled")
                : undefined,
        size: key === "createdTimestamp" ? 250 : 5,
        grow: true,
        enableEditing: !["id", "createdTimestamp", "username"].includes(key),
        filterFn:
          key === "createdTimestamp"
            ? "between"
            : ["role", "enabled"].includes(key)
              ? "equals"
              : "contains",
        filterVariant:
          key === "role" || key === "enabled"
            ? "select"
            : key === "createdTimestamp"
              ? "date-range"
              : undefined,
        filterSelectOptions:
          key === "role"
            ? roles.map((role) => ({
                label: t(`common.roles.${role.name}`),
                value: role.name,
              }))
            : key === "enabled"
              ? [
                  {
                    label: t("Account.grid.active"),
                    value: "true",
                  },
                  {
                    label: t("Account.grid.disabled"),
                    value: "false",
                  },
                ]
              : undefined,
      })) as MRT_ColumnDef<UserData>[];
    }
    return [];
  }, [usersData, isMobile, t, roles]);

  const validateField = (id, value) => {
    let newFormErrors = {};
    if (id === "email") {
      const isValidEmail =
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value);
      if (!isValidEmail) {
        newFormErrors[id] = t("Account.error.emailInvalid");
      } else {
        delete newFormErrors[id];
      }
    } else if (value === "") {
      newFormErrors[id] = value ? "" : t("Account.error.required");
    }

    setFormErrors(newFormErrors);
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setNewUser((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    validateField(name, value);
  };

  const handleOpenModal = (type: "edit" | "create" | "delete") => {
    setModalOpen(true);
    setActionType(type);
    setFormErrors({});
  };

  const handleCloseModal = () => {
    setModalOpen(false);
    setActionType("");
    setNewUser({
      id: "",
      firstName: "",
      lastName: "",
      username: "",
      email: "",
      role: "",
      createdTimestamp: undefined,
    });
  };

  const handleDelete = async () => {
    setIsLoading(true);
    try {
      await deleteUser(selectedUser.id, currentUserToken);
      setReloadCounter((prev) => prev + 1);
    } catch (error) {
      const errorStatus = error.response?.status || error.status;
      if (errorStatus === 409) {
        setShowConfirmDisable(true);
        return;
      } else {
        console.error("Error deleting user:", error);
      }
    } finally {
      setIsLoading(false);
      setModalOpen(false);
      setActionType("");
    }
  };

  const handleConfirmDisable = async () => {
    try {
      await disableUser(selectedUser.id, currentUserToken);
      setReloadCounter((prev) => prev + 1);
    } finally {
      setShowConfirmDisable(false);
      setIsLoading(false);
      setModalOpen(false);
      setActionType("");
    }
  };

  useEffect(() => {
    const getUsers = async () => {
      try {
        setIsLoading(true);
        const users: KeycloakUser[] = await getKeycloackUsers(currentUserToken);
        const filteredUsers = users
          .filter((user) => user.createdTimestamp)
          .map((u) => {
            return {
              id: u.id,
              firstName: u.firstName,
              lastName: u.lastName,
              username: u.username,
              email: u.email,
              role: u.attributes.role,
              createdTimestamp: u.createdTimestamp,
              enabled: u.enabled,
            };
          });
        setUsersData(filteredUsers);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des utilisateurs Keycloak",
          error
        );
      } finally {
        setIsLoading(false);
      }
    };
    const getRoles = async () => {
      try {
        const allRoles: KeycloakRole[] =
          await getKeycloackRoles(currentUserToken);
        setRoles(allRoles);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des roles Keycloak",
          error
        );
      }
    };
    getUsers();
    getRoles();
  }, [reloadCounter]);

  const areAllFieldsFilled = () => {
    const allFieldsFilled = Object.keys(newUser)
      .filter((key) => !excludedKeys.includes(key))
      .every((key) => newUser[key]);
    return allFieldsFilled && Object.keys(formErrors).length === 0;
  };

  const handleCreateUser = async () => {
    setIsLoading(true);
    try {
      const updateData = {
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        email: newUser.email,
        attributes: {
          role: newUser.role,
        },
      };
      const new_user = await createUser(updateData, currentUserToken);
      if (typeof new_user !== "string") {
        const badMail = /same email/i;
        const sameUsername = /User exists with same username/i;
        setModalOpen(true);
        if (badMail.test(new_user.detail)) {
          setFormErrors({
            email: t("Account.error.sameEmail"),
          });
        } else if (sameUsername.test(new_user.detail)) {
          setFormErrors({
            firstName: t("Account.error.sameUsername"),
          });
        }
      } else {
        setFormErrors({});
        handleCloseModal();
        setReloadCounter((prev) => prev + 1);
      }
    } finally {
      setIsLoading(false);
    }
  };

  const table = useMaterialReactTable({
    columns: columns as any,
    data: usersData || [],
    localization: localization,
    enableGlobalFilter: false,
    enableTopToolbar: true,
    enablePagination: true,
    enableRowActions: true,
    enableColumnResizing: true,
    initialState: {
      columnVisibility: { id: false },
      pagination: { pageIndex: 0, pageSize: 30 },
    },
    renderRowActions: ({ row, table }) => (
      <Box sx={{ display: "flex", width: 10 }}>
        <Tooltip title={t("Account.grid.delete")}>
          <IconButton
            sx={{ color: 10 }}
            onClick={() => {
              setSelectedUser(row.original);
              handleOpenModal("delete");
            }}
          >
            <DeleteIcon
              sx={{
                fontSize: 18,
                color: "rgba(240,128,128,.8)",
                "&:hover": { color: "rgb(220,20,60)" },
              }}
            />
          </IconButton>
        </Tooltip>
      </Box>
    ),
    renderBottomToolbarCustomActions: ({ table }) => (
      <Tooltip title={t("Account.grid.create")}>
        <AddCircleIcon
          sx={{
            fontSize: 36,
            color: "primary.main",
            opacity: 0.7,
            cursor: "pointer",
            marginLeft: "50%",
            "&:hover": { opacity: 1 },
          }}
          onClick={() => {
            handleOpenModal("create");
          }}
        />
      </Tooltip>
    ),
  });

  return (
    <Box sx={{ mt: 2, mb: 4, padding: "10px 20px" }}>
      <Typography variant="h6" sx={{ my: 3, pl: 1 }}>
        {t("Organization.Tabs.members")}
      </Typography>
      <Alert severity="info" icon={<InfoIcon />}>
        Lorsque vous créez un utilisateur, le mot de passe par défaut est
        "password". L'utilisateur pourra ensuite le changer depuis cette page.
      </Alert>
      <MaterialReactTable table={table} />
      {isLoading ? (
        <GlobalLoader sx={{ backgroundColor: "rgba(0,0,0,.4)" }} />
      ) : (
        <>
          <ModalComponent open={modalOpen} handleClose={handleCloseModal}>
            <Box sx={{ textAlign: "center" }}>
              {actionType === "create" ? (
                <Container sx={{ width: "30vw" }}>
                  <Typography sx={{ mb: 5, fontSize: 20, fontWeight: 500 }}>
                    {t("Account.grid.create")}
                  </Typography>
                  <Grid container spacing={2} direction="column">
                    {Object.keys(newUser)
                      .filter((key) => !excludedKeys.includes(key))
                      .map((key) => {
                        return (
                          <Grid item key={key} sx={{ pb: 1 }}>
                            <TextField
                              key={key}
                              name={key}
                              fullWidth
                              label={t(`Account.grid.${key}`)}
                              value={newUser[key] || ""}
                              onChange={handleChange}
                              error={!!formErrors[key]}
                              helperText={formErrors[key] || ""}
                              FormHelperTextProps={{
                                style: {
                                  fontSize: "0.6rem",
                                  position: "absolute",
                                  bottom: -17,
                                },
                              }}
                              InputProps={{
                                sx: {
                                  borderColor: formErrors[key] ? "red" : "",
                                  "&:hover fieldset": {
                                    borderColor: formErrors[key] ? "red" : "",
                                  },
                                  "&.Mui-focused fieldset": {
                                    borderColor: formErrors[key] ? "red" : "",
                                  },
                                },
                              }}
                              InputLabelProps={{
                                sx: {
                                  color: formErrors[key] ? "red" : "",
                                  ".Mui-focused": {
                                    color: formErrors[key] ? "red" : "",
                                  },
                                },
                              }}
                            />
                          </Grid>
                        );
                      })}
                    <FormControl
                      fullWidth
                      variant="outlined"
                      sx={{ my: 2, ml: 2 }}
                    >
                      <InputLabel id="role-label">
                        {t("Account.grid.role")}
                      </InputLabel>
                      <Select
                        labelId="role-label"
                        name="role"
                        value={newUser.role || ""}
                        onChange={handleChange}
                        label={t("Account.grid.role")}
                      >
                        {roles.map((role) => (
                          <MenuItem key={role.id} value={role.name}>
                            {t(`common.roles.${role.name}`)}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                </Container>
              ) : actionType === "delete" ? (
                <Typography sx={{ m: 3, fontSize: 20, fontWeight: 500 }}>{`${t(
                  "Account.modal.deleteMessage"
                )} ${selectedUser.firstName} ${
                  selectedUser.lastName
                } ?`}</Typography>
              ) : null}

              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-around",
                  mt: 6,
                  mb: 3,
                }}
              >
                <Button
                  sx={{ width: "35%" }}
                  variant="outlined"
                  onClick={handleCloseModal}
                >
                  {t("Account.modal.cancel")}
                </Button>
                {actionType === "delete" ? (
                  <Button
                    sx={{ width: "35%" }}
                    variant="contained"
                    onClick={() => {
                      handleDelete();
                    }}
                  >
                    {t("Account.modal.confirm")}
                  </Button>
                ) : (
                  <Button
                    disabled={!areAllFieldsFilled()}
                    sx={{ width: "35%" }}
                    variant="contained"
                    onClick={() => {
                      handleCreateUser();
                    }}
                  >
                    {t("Account.save")}
                  </Button>
                )}
              </Box>
            </Box>
          </ModalComponent>
          {showConfirmDisable && (
            <ModalComponent
              open={showConfirmDisable}
              handleClose={() => setShowConfirmDisable(false)}
            >
              <ConfirmDialog
                message={t("Account.error.cannotDeleteUser")}
                onConfirm={handleConfirmDisable}
                onAbort={() => setShowConfirmDisable(false)}
              />
            </ModalComponent>
          )}
        </>
      )}
    </Box>
  );
};

export default AccountGrid;
