import ViewStateControl from "@blocks/map/ViewStateControl";
import { Stack } from "@mui/material";
import { useMapZoom, useMapEditorActions } from "@stores/pages/mapEditor";

const Controls = (props) => {
  const zoom = useMapZoom();
  const actions = useMapEditorActions();

  return (
    <Stack spacing={2}>
      <ViewStateControl
        mode="zoom"
        value={zoom}
        minValue={1}
        setValueCallback={(value) => {
          actions.setZoom(value);
        }}
      />
      <ViewStateControl
        mode="bearing"
        value={0}
        setValueCallback={(value) => console.log("Bearing value :>> ", value)}
      />
    </Stack>
  );
};

export default Controls;
