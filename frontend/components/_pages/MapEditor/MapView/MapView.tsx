import React from "react";
import DeckGL from "@deck.gl/react/typed";
import { GeoJsonLayer } from "@deck.gl/layers";
import Map, { AttributionControl } from "react-map-gl";
import maplibregl from "maplibre-gl";
import {
  useViewState,
  useMapLayers,
  useMapEditorActions,
} from "@stores/pages/mapEditor";
import { SelectionLayer } from "@nebula.gl/layers";

const MapView = () => {
  const viewState = useViewState();
  const mapLayers = useMapLayers();
  const { setViewState } = useMapEditorActions();

  const handleViewStateChange = ({ viewState }) => {
    console.log("viewState :>> ", viewState);
    setViewState(viewState);
  };

  const mapStyle = "/mapStyles/map_light.json";

  // Map the mapLayers array to DeckGL-compatible layer components
  const layers = mapLayers.map((layerProps: any) => {
    if (layerProps.type === "geojson") {
      return new GeoJsonLayer(layerProps);
    } else if (layerProps.type === "selection") {
      //@ts-ignore
      return new SelectionLayer(layerProps);
    }
    return null;
  });

  return (
    <React.Fragment>
      <DeckGL
        viewState={viewState}
        onViewStateChange={handleViewStateChange}
        controller={true}
        layers={layers as any}
      />
      <Map
        mapLib={maplibregl as any}
        mapStyle={mapStyle}
        attributionControl={false}
      >
        <AttributionControl position="top-left" />
      </Map>
    </React.Fragment>
  );
};

export default MapView;
