import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import Stepper from "./StepperSection";

const meta = {
  title: "Pages/MapEditor/Stepper",
  component: Stepper,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Stepper work site",
      },
    },
  },
} satisfies Meta<typeof Stepper>;

export default meta;
type Story = StoryObj<typeof Stepper>;

export const CustomStepper: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <Stepper steps={[]} onFinish={() => console.log("onFinish")} />
    </Box>
  ),
};
