import React, { FC, useEffect, useState } from "react";
import { Box, Typography, IconButton, Divider, SvgIcon } from "@mui/material";
import { useTranslation } from "react-i18next";
// ** MUI Imports
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import { useRouter } from "next/router";
import getConfig from "next/config";
import {
  calculateTreeAge,
  checkIfImageExists,
  useDeviceSize,
} from "@lib/utils";
import { VegetatedAreaSurfaceIcon } from "@components/icons/VegetatedAreaSurfaceIcon";
import { VegetatedAreaShrubsIcon } from "@components/icons/VegetatedAreaShrubsIcon";

import TreeToolbarIcon from "@components/_core/icons/Tree/TreeToolbarIcon";

export interface CardInfosProps {
  handleCloseCardInfos: () => void;
  infos: {
    id?: string;
    status_name?: string;
    location_id?: string;
    tree_id?: string;
    tree_scientific_name?: string;
    tree_vernacular_name?: string;
    tree_serial_number?: string;
    plantation_date?: string;
    tree_height?: string;
    vegetated_area_id?: string;
    vegetated_area_type?: string;
    vegetated_area_address?: string;
    vegetated_area_surface?: number;
    vegetated_area_shrubs_data?: any;
    vegetated_area_herbaceous_data?: any;
    vegetated_area_tree_count?: number;
    vegetated_area_afforestation_trees_data?: any;
  };
}

const generateImageUrl = (
  imageUrl: string,
  hasImage: boolean,
  isVegetatedArea: boolean,
  isTree: boolean,
  infos
) => {
  let image;
  if (imageUrl !== "" && hasImage) {
    image = imageUrl;
  } else if (isVegetatedArea) {
    image = "/components/map/polygon/polygon.png";
  } else if (isTree) {
    image = "/components/map/location/tree.png";
  } else if (infos.status_name === "stump") {
    image = "/components/map/location/stump.png";
  } else {
    image = "/components/map/location/empty.png";
  }
  return image;
};

const CardInfos: FC<CardInfosProps> = ({ handleCloseCardInfos, infos }) => {
  const { publicRuntimeConfig } = getConfig();
  const { t } = useTranslation(["common", "components"]);
  const router = useRouter();
  const { isMobile } = useDeviceSize();
  const [imageUrl, setImageUrl] = useState<string>("");
  const [hasImage, setHasImage] = useState<boolean>(false);
  const isTree = Boolean(
    infos.tree_id && !["stump", "empty"].includes(infos.status_name as string)
  );
  const isVegetatedArea = Boolean(infos.vegetated_area_id);
  const image = generateImageUrl(
    imageUrl,
    hasImage,
    isVegetatedArea,
    isTree,
    infos
  );

  const handleClick = () => {
    if (isTree) {
      router.push(`/tree/${infos.tree_id}`);
    } else if (isVegetatedArea) {
      router.push(`/vegetated-area/${infos.vegetated_area_id}`);
    } else {
      router.push(`/location/${infos.location_id}`);
    }
  };

  const calculateJsonDataVegetatedArea = (json_data) => {
    const jsonParsed = JSON.parse(json_data);
    const total = jsonParsed.reduce((acc, current) => acc + current.number, 0);
    return total;
  };

  useEffect(() => {
    if (isTree) {
      setImageUrl(
        `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/${publicRuntimeConfig.DEFAULT_STORAGE_BUCKET_NAME}/trees/${infos.tree_id}/latest.png`
      );
    } else {
      setImageUrl("");
    }
  }, [infos]);

  useEffect(() => {
    checkIfImageExists(imageUrl, (exists) => {
      if (exists) {
        setHasImage(true);
      } else {
        setHasImage(false);
      }
    });
  }, [imageUrl]);

  return (
    <Card
      sx={[
        styles.cardModal,
        {
          width: isMobile ? "100%" : "400px",
          bottom: isMobile ? 0 : 10,
          left: isMobile ? 0 : 10,
        },
      ]}
      data-cy="map-card-infos"
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            flex: "1 0 auto",
          }}
        >
          <CardMedia
            sx={{
              height: "110px",
              width: "140px",
              m: isMobile ? 0 : 1,
              backgroundSize: "contain",
            }}
            image={image}
            data-cy="map-card-media"
          >
            <IconButton
              color="default"
              aria-label="Close"
              sx={{
                position: "absolute",
                top: 0,
                p: 0,
              }}
              onClick={() => handleCloseCardInfos()}
              data-cy="map-card-infos-close"
            >
              <HighlightOffIcon />
            </IconButton>{" "}
          </CardMedia>
        </Box>
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-end",
          flex: "1 0 auto",
        }}
        data-cy="map-card-infos-box"
      >
        <CardHeader
          title={
            <div
              style={{
                overflow: "hidden",
                textOverflow: "ellipsis",
                whiteSpace: "pre-wrap",
                maxWidth: "130px",
                fontSize: "14px",
              }}
            >
              {isTree
                ? infos.tree_scientific_name
                : isVegetatedArea
                  ? t(
                      `components.VegetatedAreaForm.properties.typeLabels.${infos.vegetated_area_type}`
                    )
                  : t(`common.${infos.status_name}`)}
            </div>
          }
          subheader={
            <div
              style={{
                overflow: "hidden",
                textOverflow: "ellipsis",
                whiteSpace: "pre-wrap",
                maxWidth: "130px",
              }}
            >
              {isTree
                ? infos.tree_serial_number
                : isVegetatedArea
                  ? infos.vegetated_area_address
                  : ""}
            </div>
          }
          action={
            <IconButton
              aria-label="go to card"
              sx={{
                overflow: "hidden",
                padding: isMobile ? "12px" : "8px",
                marginRight: isMobile ? "8px" : "0",
              }}
              onClick={handleClick}
            >
              <ArrowForwardIosIcon />
            </IconButton>
          }
          sx={{ paddingBottom: "0px", overflow: "hidden" }}
        />
        <CardContent
          sx={{
            padding: isMobile ? "3px !important" : "16px ! important",
          }}
        >
          <Divider light />
          {isTree && (
            <Box
              sx={{
                paddingTop: "16px",
                display: "flex",
                alignItems: "center",
                color: "text.secondary",
                gap: "24px",
              }}
              data-cy="map-card-infos-tree"
            >
              <Box
                sx={{ display: "flex", gap: "8px", alignItems: "center" }}
                data-cy="map-card-infos-tree-age"
              >
                <CalendarTodayIcon sx={{ fontSize: "1rem" }} />
                {isTree && infos.plantation_date && (
                  <Typography sx={{ fontSize: "0.875rem" }}>
                    {calculateTreeAge(infos.plantation_date)?.years}
                    {t("age.years")},{" "}
                    {calculateTreeAge(infos.plantation_date)?.months}
                    {t("age.months")}
                  </Typography>
                )}
              </Box>
              <Box
                sx={{ display: "flex", gap: "8px", alignItems: "center" }}
                data-cy="map-card-infos-tree-height"
              >
                <ArrowUpwardIcon sx={{ fontSize: "1rem" }} />
                <Typography sx={{ fontSize: "0.875rem" }}>
                  {isTree &&
                    infos.tree_height &&
                    Math.round(Number(infos.tree_height))}{" "}
                  m
                </Typography>
              </Box>
            </Box>
          )}
          {isVegetatedArea && (
            <>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: 1,
                  mt: 1,
                }}
                data-cy="map-card-infos-vegetated-area"
              >
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    gap: isMobile ? "1px" : "4px",
                  }}
                >
                  <SvgIcon>
                    <VegetatedAreaSurfaceIcon />
                  </SvgIcon>
                  {infos?.vegetated_area_surface
                    ? Math.round(infos?.vegetated_area_surface)
                    : ""}
                  m²
                  <SvgIcon>
                    <VegetatedAreaShrubsIcon />
                  </SvgIcon>
                  {infos?.vegetated_area_shrubs_data
                    ? calculateJsonDataVegetatedArea(
                        infos?.vegetated_area_shrubs_data
                      )
                    : 0}{" "}
                  {t(
                    "components.VegetatedAreaForm.properties.vegetatedAreaComposition.shrubs.title"
                  )}
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: 1,
                  flewxWrap: "wrap",
                }}
                data-cy="map-card-infos-vegetated-area"
              >
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    gap: isMobile ? "1px" : "4px",
                  }}
                >
                  <SvgIcon>
                    <VegetatedAreaShrubsIcon />
                  </SvgIcon>
                  {infos?.vegetated_area_herbaceous_data
                    ? calculateJsonDataVegetatedArea(
                        infos?.vegetated_area_herbaceous_data
                      )
                    : 0}{" "}
                  {t(
                    "components.VegetatedAreaForm.properties.vegetatedAreaComposition.herbaceous.title"
                  )}
                  <SvgIcon sx={{ fontSize: 21 }}>
                    <TreeToolbarIcon />
                  </SvgIcon>
                  {infos?.vegetated_area_tree_count || 0}{" "}
                  {t(
                    "components.VegetatedAreaForm.properties.vegetatedAreaComposition.trees.short_title"
                  )}
                </Box>
              </Box>
              {infos?.vegetated_area_type === "afforestation" && (
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    gap: 1,
                    mt: 1,
                  }}
                  data-cy="map-card-infos-vegetated-area"
                >
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      gap: isMobile ? "1px" : "4px",
                    }}
                  >
                    <SvgIcon sx={{ fontSize: 21 }}>
                      <TreeToolbarIcon />
                    </SvgIcon>
                    {infos?.vegetated_area_afforestation_trees_data
                      ? calculateJsonDataVegetatedArea(
                          infos?.vegetated_area_afforestation_trees_data
                        )
                      : 0}{" "}
                    {t(
                      "components.VegetatedAreaForm.properties.vegetatedAreaComposition.afforestation_trees.title"
                    )}
                  </Box>
                </Box>
              )}
            </>
          )}
        </CardContent>
      </Box>
    </Card>
  );
};

export default CardInfos;

const styles = {
  cardModal: {
    position: "absolute",
    height: "180px",
    bgcolor: "background.paper",
    borderRadius: "10px",
    display: "flex",
    zIndex: 5,
  },
  text: {
    top: "30%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    textTransform: "uppercase",
    position: "absolute",
    color: "#696B6D",
    opacity: "0.8",
  },
};
