import { useTranslation } from "react-i18next";
import { TWorksiteStep } from "./Worksite.types";
import { Box } from "@mui/material";

interface IWorksiteStepProps {
  id: TWorksiteStep;
}

const WorksiteStep = ({ id }: IWorksiteStepProps) => {
  const { t } = useTranslation(["components"]);
  return (
    <Box
      sx={{
        fontSize: "1.25rem",
        fontWeight: 700,
        mb: "2rem",
        textTransform: "uppercase",
      }}
    >
      {t(`WorksiteStep.idLabels.${id}`)}
    </Box>
  );
};

export default WorksiteStep;
