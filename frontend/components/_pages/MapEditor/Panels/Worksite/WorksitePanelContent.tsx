import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Stack } from "@mui/material";
import { DateRange, DayPicker } from "react-day-picker";
import "react-day-picker/dist/style.css";
import { format } from "date-fns";
import { fr } from "date-fns/locale";

import _ from "lodash";

import {
  FetchOneWorksiteQuery,
  FetchOneWorksiteInterventionsQuery,
} from "generated/graphql";
import { useWorksiteFormActions } from "@stores/forms/worksiteForm";

import StepperSection from "@ecosystems/MapEditor/Stepper/StepperSection";
import WorkSiteForm from "./WorksiteForm";
import WorksiteInterventions from "./WorksiteInterventionForm";
import { useWorksiteLogic } from "./useWorksiteLogic";

const WorksitePanel: FC<{
  activeWorksite: FetchOneWorksiteQuery["worksite"];
  interventionsWorksiteData: FetchOneWorksiteInterventionsQuery["worksite"];
}> = ({ activeWorksite, interventionsWorksiteData }) => {
  /** HOOKS */
  const { t } = useTranslation(["components", "common"]);
  const { setWorksite } = useWorksiteFormActions();

  const { worksiteData, worksiteInterventions, onSubmitFinalStep } =
    useWorksiteLogic(activeWorksite, interventionsWorksiteData);

  const validateWorksiteForm = () => {
    if (
      !worksiteData.isSelectionValidated ||
      worksiteData.locations.length === 0
    ) {
      return false;
    }
    return true;
  };

  const [selectedRange, setSelectedRange] = useState<DateRange | undefined>({
    from: new Date(),
    to: new Date(),
  });

  useEffect(() => {
    if (activeWorksite) {
      setSelectedRange({
        from: new Date(activeWorksite.scheduled_start_date) || new Date(),
        to: new Date(activeWorksite.scheduled_end_date) || new Date(),
      });
    }
  }, [activeWorksite]);

  const handleDateChange = (nextRange, selectedDay) => {
    setSelectedRange((range) => {
      // If both dates (from and to) are already defined, restart the selection
      if (range?.from && range?.to) {
        return { from: selectedDay };
      }
      return nextRange as DateRange;
    });

    setWorksite({
      ...worksiteData.worksite,
      scheduled_start_date: nextRange?.from
        ? format(nextRange.from, "yyyy-MM-dd")
        : undefined,
      scheduled_end_date: nextRange?.to
        ? format(nextRange.to, "yyyy-MM-dd")
        : undefined,
    });
  };

  const validateInterventionsForm = () => {
    for (const intervention of worksiteInterventions) {
      const location_types = intervention?.intervention_type?.location_types;
      const associatedLocations = intervention?.associated_locations;
      // Validate that associated_locations are present
      if (!associatedLocations || associatedLocations.length === 0) {
        return false;
      }
      // Check if all associated locations are "stump"
      const areAllStumps = associatedLocations.every(
        (location) => location.status_name === "stump"
      );
      // Check if all associated locations are "empty"
      const areAllEmpty = associatedLocations.every(
        (location) => location.status_name === "empty"
      );
      if (areAllStumps && !location_types?.includes("stump")) {
        // If all locations are "stump", location_types need to include stump
        return false;
      }
      if (areAllEmpty && !location_types?.includes("empty_location")) {
        // If all locations are "empty", location_types need to include empty_location
        return false;
      }
      // For location_types "stump" ensure all locations are "stump"
      if (location_types?.includes("stump") && !areAllStumps) {
        return false;
      }
      // For location_types "empty_location" ensure all locations are "empty"
      if (location_types?.includes("empty_location") && !areAllEmpty) {
        return false;
      }
    }
    // If all validations pass
    return true;
  };

  const validateScheduledDateForm = () => {
    // Ensure scheduled_start_date and scheduled_end_date are set
    if (!selectedRange?.from || !selectedRange?.to) {
      return false;
    } else {
      return true;
    }
  };

  const steps = [
    {
      title: t("WorksiteStep.idLabels.worksite"),
      subtitle: t("WorksiteForm.name"),
      description: <WorkSiteForm />,
      validate: validateWorksiteForm,
    },
    {
      title: t("WorksiteStep.idLabels.intervention"),
      description: <WorksiteInterventions />,
      validate: validateInterventionsForm,
      errorMessage: t("components.WorksiteForm.interventions.errorMessage"),
    },
    {
      title: t("WorksiteStep.idLabels.validateWorksiteGroup"),
      description: (
        <DayPicker
          mode="range"
          selected={selectedRange}
          onSelect={handleDateChange}
          locale={fr}
        />
      ),
      validate: validateScheduledDateForm,
    },
  ];

  return (
    <Stack data-cy="worksite-form-container">
      <StepperSection steps={steps} onFinish={onSubmitFinalStep} />
    </Stack>
  );
};

export default WorksitePanel;
