import { useEffect, useCallback } from "react";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import {
  useCreateOneWorksiteWithRelationshipsMutation,
  useCreateInterventionsMutation,
  useDeleteInterventionMutation,
  useUpdateWorksiteMutation,
} from "generated/graphql";
import {
  useWorksiteData,
  useWorksiteFormActions,
  useWorksiteFormData,
  useWorksiteInterventionFormData,
} from "@stores/forms/worksiteForm";
import {
  useMapEditorActions,
  useMapSelectedLocations,
} from "@stores/pages/mapEditor";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import useStore from "@stores/useStore";
import { useTranslation } from "react-i18next";
interface InterventionDataItem {
  location_id?: string | null;
  tree_id?: string | null;
  vegetated_area_id?: string | null;
  scheduled_date?: string;
  realization_date: string | null;
  note: string;
  data_entry_user_id: string;
  intervention_partner_id?: string | null;
  intervention_type_id: string;
  cost?: number | null;
  worksite_id?: string; // Add worksite_id if not already included
}
export const useWorksiteLogic = (activeWorksite, interventionsWorksiteData) => {
  const router = useRouter();
  const isWorksiteEditing = !!router.query.activeWorksite;
  const isDuplicating = router.query.duplicate === "true";
  const [createWorksite] = useCreateOneWorksiteWithRelationshipsMutation();
  const [createInterventions] = useCreateInterventionsMutation();
  const [deleteInterventionByPkMutation] = useDeleteInterventionMutation();
  const [updateWorksite] = useUpdateWorksiteMutation();
  const { setFiltersWithMartin } = useFiltersWithMartinContext()!;
  const mapSelectedLocations = useMapSelectedLocations();
  const session = useSession();
  const currentUserId = session?.data?.user?.id || "";

  const {
    setMapFilteredLocations,
    setMapPanelOpen,
    setMapActiveAction,
    renderSelectionLayer,
    setMapSelectedLocations,
  } = useMapEditorActions();

  const {
    setLocations,
    setWorksite,
    setInterventions,
    setIsSelectionValidated,
  } = useWorksiteFormActions();

  const { t } = useTranslation();
  const { app } = useStore((store) => store);
  const worksiteData = useWorksiteData();
  const worksite = useWorksiteFormData();
  const worksiteInterventions = useWorksiteInterventionFormData();

  const onComplete = async (action: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`components.WorksiteForm.success.${action}`),
            severity: "success",
          },
        ],
      },
    });
    setInterventions([]);
    setWorksite({
      name: "",
      description: "",
      scheduled_start_date: "",
      scheduled_end_date: "",
    });
    setIsSelectionValidated(false);
    setMapSelectedLocations(undefined);
    setMapFilteredLocations(undefined);
    setMapActiveAction(undefined);
    renderSelectionLayer(false);
    setFiltersWithMartin({});
    setMapPanelOpen(false);
    if (action === "create") {
      router.push("/worksites");
    } else if (action === "update") {
      router.push(`/worksites/${router.query.activeWorksite}`);
    } else if (action === "duplicate") {
      router.push("/worksites");
    }
  };

  /** USE EFFECTS */
  useEffect(() => {
    if (
      activeWorksite &&
      interventionsWorksiteData &&
      (isWorksiteEditing || isDuplicating)
    ) {
      setWorksite({
        name: activeWorksite.name,
        description: activeWorksite.description || "",
        scheduled_start_date: activeWorksite.scheduled_start_date,
        scheduled_end_date: activeWorksite.scheduled_end_date,
      });

      const updatedMapSelectedLocations =
        interventionsWorksiteData.interventions
          .map((intervention) => {
            const isDuplicate = router.query.duplicate;
            const isExcludedType = ["felling", "grubbing", "planting"].includes(
              intervention.intervention_type?.slug
            );

            if (isDuplicate && isExcludedType) return null; // Skip excluded types if duplicating
            const location =
              intervention.location || intervention.tree?.location; // Fallback to tree location if available

            return {
              id: intervention.id || "", // Ensure a unique id property
              location_id: location?.id || null,
              intervention_type_id: intervention.intervention_type?.id || "",
              layerName: "filter_locations",
              location_address: location?.address || "",
              location_coords: JSON.stringify(location?.coords) || null,
              plantation_date: intervention.realization_date || null,
              scheduled_intervention: intervention.scheduled_date || null,
              status_color:
                JSON.stringify(location?.location_status?.status) || null,
              status_name: location?.location_status?.status || null,
              tree_id: intervention.tree?.id || null,
              tree_scientific_name: intervention.tree?.scientific_name || "",
              tree_serial_number: intervention.tree?.id || null,
              tree_vernacular_name: "",
            };
          })
          .filter(Boolean);

      // Set interventions with the updated structure
      const updatedInterventions = interventionsWorksiteData.interventions
        .map((intervention, index) => {
          return {
            is_current: index === 0, // Only the first intervention has is_current set to true
            intervention_type: {
              id: intervention.intervention_type?.id || "",
              slug: intervention.intervention_type?.slug || "",
              location_types:
                intervention.intervention_type.location_types || [],
            },
            intervention_partner: intervention.intervention_partner?.id || "",
            associated_locations: [
              intervention.tree
                ? { tree_id: intervention.tree.id, status_name: "alive" }
                : {
                    location_id: intervention.location?.id,
                    status_name: intervention.location?.location_status.status,
                  },
            ],
          };
        })
        .filter(Boolean);

      setMapSelectedLocations(updatedMapSelectedLocations);
      setInterventions(updatedInterventions);
    }
  }, [
    activeWorksite,
    interventionsWorksiteData,
    setWorksite,
    setMapSelectedLocations,
    setInterventions,
    isWorksiteEditing,
    isDuplicating,
  ]);

  useEffect(() => {
    if (mapSelectedLocations && mapSelectedLocations.length > 0) {
      setLocations(
        mapSelectedLocations.map((item) => {
          return {
            location_id: item.location_id, // Ensure location_id is correctly referenced
            vegetated_area_id: item.vegetated_area_id || null, // Handle potential undefined
            tree_id: item.tree_id,
            address: item.location_address || "",
          };
        })
      );
    } else {
      setLocations([]);
    }
  }, [mapSelectedLocations, setLocations]);

  useEffect(() => {
    if (isDuplicating) {
      app.setAppState({
        snackbar: {
          ...app.snackbar,
          alerts: [
            {
              message: t(`components.WorksiteForm.duplicateTooltip`),
              severity: "info",
            },
          ],
        },
      });
    }
  }, [isDuplicating]);

  const duplicateWorksite = useCallback(async () => {
    if (!activeWorksite) {
      console.error("No active worksite to duplicate");
      return;
    }

    try {
      // Create a new worksite with the same data as the active worksite
      const newWorksiteVariables = {
        object: {
          name: `${activeWorksite.name} (Copie)`,
          description: activeWorksite.description,
          scheduled_start_date: activeWorksite.scheduled_start_date,
          scheduled_end_date: activeWorksite.scheduled_end_date,
        },
      };

      let newWorksiteId = "";
      await createWorksite({
        variables: newWorksiteVariables,
        onCompleted: (res) => {
          newWorksiteId = res?.insert_worksite_one?.id;
        },
      });

      if (newWorksiteId) {
        // Duplicate interventions for the new worksite
        const newInterventions = interventionsWorksiteData.interventions.map(
          (intervention) => ({
            location_id: intervention.location?.id || null,
            tree_id: intervention.tree?.id || null,
            vegetated_area_id: intervention.vegetated_area_id || null,
            scheduled_date: intervention.scheduled_date,
            realization_date: null, // Reset realization date for the new worksite
            note: intervention.note,
            data_entry_user_id: currentUserId,
            intervention_partner_id:
              intervention.intervention_partner?.id || null,
            intervention_type_id: intervention.intervention_type?.id || "",
            cost: intervention.cost,
            worksite_id: newWorksiteId,
          })
        );

        await createInterventions({
          variables: { interventions: newInterventions },
        });

        onComplete("duplicate");
        router.push(`/worksites/${newWorksiteId}`);
      }
    } catch (err) {
      console.error("Error duplicating worksite: ", err);
      onComplete("failure");
    }
  }, [
    activeWorksite,
    interventionsWorksiteData,
    createWorksite,
    createInterventions,
    currentUserId,
    onComplete,
    router,
  ]);

  const onSubmitFinalStep = async () => {
    try {
      const interventionData: InterventionDataItem[] = [];

      // Loop through each intervention
      worksiteInterventions.forEach((intervention) => {
        // Loop through each associated location of the current intervention
        intervention?.associated_locations.forEach((location) => {
          const treeId = location.tree_id || null;
          const locationId = !treeId ? location.location_id : null; // Use location_id only if tree_id is not present
          const vegetatedAreaId = location.vegetated_area_id || null;

          interventionData.push({
            location_id: locationId,
            tree_id: treeId,
            vegetated_area_id: vegetatedAreaId,
            scheduled_date: worksite.scheduled_end_date,
            realization_date: null,
            note: "",
            data_entry_user_id: currentUserId,
            intervention_partner_id: intervention?.intervention_partner || null,
            intervention_type_id: intervention?.intervention_type?.id || "",
            cost: intervention.cost || null,
          });
        });
      });

      const variables = {
        object: {
          name: isDuplicating ? `${worksite.name} (Copie)` : worksite.name,
          description: worksite.description,
          scheduled_start_date: worksite.scheduled_start_date,
          scheduled_end_date: worksite.scheduled_end_date,
        },
      };

      // Step 1: Create the worksite and get its ID
      let worksiteId = "";
      if (isDuplicating || !isWorksiteEditing) {
        // Create a new worksite for duplication or new creation
        await createWorksite({
          variables,
          onCompleted: (res) => {
            worksiteId = res?.insert_worksite_one?.id;
          },
        });
      } else {
        // Update existing worksite
        await updateWorksite({
          variables: {
            input_data: { ...variables.object },
            id: router.query.activeWorksite,
          },
          onCompleted: () => {
            worksiteId = router.query.activeWorksite as string;
          },
        });
      }

      if (worksiteId !== "") {
        // Step 2: Update interventionData with worksite_id
        const updatedInterventionData = interventionData.map(
          (intervention) => ({
            ...intervention,
            worksite_id: worksiteId,
          })
        );

        if (isWorksiteEditing && !isDuplicating) {
          // Delete existing interventions and create new ones for editing
          const deletePromises =
            activeWorksite?.interventions?.map((worksiteIntervention) => {
              return deleteInterventionByPkMutation({
                variables: {
                  id: worksiteIntervention.id,
                },
              });
            }) || [];

          await Promise.all(deletePromises);
        }

        // Create interventions for new, duplicated, or edited worksite
        await createInterventions({
          variables: { interventions: updatedInterventionData },
        });

        if (isDuplicating) {
          router.replace(`/worksites/${worksiteId}`);
          onComplete("duplicate");
        } else if (isWorksiteEditing) {
          router.replace("/map");
          onComplete("update");
        } else {
          onComplete("create");
        }
      }
    } catch (err) {
      console.log("Error submitting final step: ", err);
      onComplete("failure");
    }
  };

  return {
    isWorksiteEditing,
    isDuplicating,
    worksiteData,
    worksiteInterventions,
    onComplete,
    onSubmitFinalStep,
    currentUserId,
    duplicateWorksite,
  };
};
