import { TextField } from "@mui/material";
import { Controller, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";
import {
  useWorksiteFormActions,
  useWorksiteFormData,
} from "@stores/forms/worksiteForm";
import { FormInputError } from "@components/forms/error/FormInputError";
import WorksiteLocationSelector from "./LocationSelector";
import { useMapEditorActions } from "@stores/pages/mapEditor";
import { useRouter } from "next/router";

const worksiteSchema = yup.object().shape({
  name: yup.string().required(),
  description: yup.string().nullable(),
});

const WorkSiteForm = ({}) => {
  /** HOOKS **/
  const { t } = useTranslation(["components", "common"]);
  const { setWorksite } = useWorksiteFormActions();
  const { renderSelectionLayer } = useMapEditorActions();
  const formData = useWorksiteFormData();
  const router = useRouter();

  const {
    handleSubmit,
    control,
    watch,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(worksiteSchema),
    defaultValues: {
      name: formData.name,
      description: formData.description,
    },
  });

  /** private methods */
  const name = watch("name");
  const description = watch("description");

  const onSubmit = (data) => {
    setWorksite(data);
  };

  useEffect(() => {
    const worksiteData = {
      name: name,
      description: description,
      scheduled_start_date: formData?.scheduled_start_date,
      scheduled_end_date: formData?.scheduled_end_date,
    };
    setWorksite(worksiteData);
  }, [name, description]);

  useEffect(() => {
    if (router.query.activeWorksite) {
      setValue("name", formData.name || "");
      setValue("description", formData.description || "");
    }
  }, [formData, setValue, router.query.activeWorksite]);

  useEffect(() => {
    renderSelectionLayer(true);
  }, []);

  return (
    <form onSubmit={handleSubmit(onSubmit)} noValidate>
      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            required
            fullWidth
            label={t(`WorksiteForm.name`) as string}
            variant="filled"
            placeholder={t(`WorksiteForm.name`)}
            sx={{ mb: 1 }}
          />
        )}
      />
      <FormInputError name={"name"} errors={errors} />

      <Controller
        name={"description"}
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            fullWidth
            label={t(`WorksiteForm.description`) as string}
            variant="filled"
            placeholder={t(`WorksiteForm.description`)}
            sx={{ mt: 1 }}
          />
        )}
      />

      <WorksiteLocationSelector />
    </form>
  );
};

export default WorkSiteForm;
