import React, { FC, useEffect, useRef } from "react";
import {
  Alert,
  AlertTitle,
  Button,
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from "@mui/material";
import {
  useWorksiteFormActions,
  useWorksiteIsSelectionValidated,
} from "@stores/forms/worksiteForm";
import {
  useHoveredSelectedLocationId,
  useMapEditorActions,
  useMapSelectedLocations,
} from "@stores/pages/mapEditor";
import { useTranslation } from "react-i18next";
import CloseIcon from "@mui/icons-material/Close";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";

export interface IWorksiteLocationSelectorProps {
  goToPreviousStep?: () => void;
  goToNextStep?: () => void;
}

const WorksiteLocationSelector: FC<IWorksiteLocationSelectorProps> = ({}) => {
  /** hooks */
  const { t } = useTranslation(["components"]);
  const { setLocations, setHoveredItemId, setIsSelectionValidated } =
    useWorksiteFormActions();
  const hoveredSelectedLocationId = useHoveredSelectedLocationId();
  const { setFiltersWithMartin, filtersWithMartin, triggerFieldReset } =
    useFiltersWithMartinContext()!;
  const { removeFromMapSelectedLocations, setMapSelectedLocations } =
    useMapEditorActions();
  const mapSelectedLocations = useMapSelectedLocations();
  const isSelectionValidated = useWorksiteIsSelectionValidated();
  const listItemRefs = useRef({});

  const handleMouseEnter = (item) => {
    setHoveredItemId(item.location_id || item.vegetated_area_id);
  };

  const handleMouseLeave = () => {
    setHoveredItemId(undefined);
  };

  useEffect(() => {
    if (mapSelectedLocations) {
      setLocations(
        mapSelectedLocations.map((item) => {
          return {
            location_id: item.location_id,
            tree_id: item.tree_id,
            vegetated_area_id: item.vegetated_area_id,
            address: item.location_address || "",
            vegetated_area_type: item.vegetated_area_type,
          };
        })
      );
    }
  }, [mapSelectedLocations]);

  useEffect(() => {
    if (
      hoveredSelectedLocationId &&
      listItemRefs.current[hoveredSelectedLocationId]
    ) {
      listItemRefs.current[hoveredSelectedLocationId].scrollIntoView({
        behavior: "smooth",
        block: "center",
      });
    }
  }, [hoveredSelectedLocationId]);

  const handleRemoveLocation = (locationId) => {
    removeFromMapSelectedLocations(locationId);
  };

  const handleRemoveSelection = () => {
    setMapSelectedLocations(undefined);
    setIsSelectionValidated(false);
    triggerFieldReset();
    setFiltersWithMartin({
      vegetated_areas_active: true,
    });
  };

  const handleValidateSelection = () => {
    setIsSelectionValidated(true);
    // Extract location IDs
    const locationIds = mapSelectedLocations
      ?.map((item) => item.location_id)
      .filter((id) => id);

    // Extract vegetated area IDs
    const vegetatedAreaIds = mapSelectedLocations
      ?.map((item) => item.vegetated_area_id)
      .filter((id) => id);

    setFiltersWithMartin({
      ...filtersWithMartin,
      location_ids: locationIds,
      vegetated_areas_ids: vegetatedAreaIds,
    });
  };

  return (
    <>
      {!mapSelectedLocations?.length && (
        <Alert severity="info">
          <AlertTitle>{t(`WorksiteForm.locations.title`)}</AlertTitle>
          {t(`WorksiteForm.locations.text`)}
        </Alert>
      )}
      <List sx={{ p: 0, maxHeight: "300px", overflowY: "auto" }}>
        <Alert severity="info">
          <AlertTitle>{t(`WorksiteForm.locations.infos`)}</AlertTitle>
        </Alert>
        {mapSelectedLocations?.map((item, index) => (
          <ListItem
            key={index}
            sx={{
              p: 0,
              cursor: "pointer",
              backgroundColor:
                hoveredSelectedLocationId ===
                (item.location_id || item.vegetated_area_id)
                  ? "#a2faa2"
                  : "transparent",
            }}
            onMouseEnter={() => handleMouseEnter(item)}
            onMouseLeave={handleMouseLeave}
            ref={(el) =>
              (listItemRefs.current[
                item.location_id || item.vegetated_area_id
              ] = el)
            }
          >
            <ListItemText
              primary={item.location_address}
              secondary={
                item.tree_scientific_name ||
                (item.vegetated_area_id &&
                  t(
                    `components.VegetatedAreaForm.properties.typeLabels.${item.vegetated_area_type}`
                  )) ||
                t(`common.${item.status_name}`)
              }
            />
            <ListItemSecondaryAction>
              <IconButton
                edge="end"
                onClick={() => handleRemoveLocation(item.location_id)}
              >
                <CloseIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>

      <Button sx={{ m: 1 }} onClick={handleRemoveSelection}>
        {t(`WorksiteForm.deleteSelection`)}
      </Button>
      <Button
        sx={{ m: 1 }}
        onClick={handleValidateSelection}
        variant="contained"
        disabled={isSelectionValidated}
      >
        {t(`WorksiteForm.validateSelection`)}
      </Button>
    </>
  );
};

export default WorksiteLocationSelector;
