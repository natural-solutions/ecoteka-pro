import WorksiteInterventionForm from "@components/forms/intervention/WorksiteInterventionForm";

import {
  useFetchInterventionPartnersLazyQuery,
  useFetchAllInterventionTypesFamiliesLazyQuery,
} from "generated/graphql";
import { useEffect } from "react";

export interface IWorksiteInterventionFormProps {
  goToPreviousStep: () => void;
  goToNextStep: () => void;
}

const WorksiteInterventions = ({}) => {
  /** Hooks */
  const [fetchInterventionsTypes, { data: interventionTypesGroupedByFamily }] =
    useFetchAllInterventionTypesFamiliesLazyQuery();
  const [fetchInterventionPartners, { data: interventionPartners }] =
    useFetchInterventionPartnersLazyQuery();

  useEffect(() => {
    fetchInterventionPartners();
    fetchInterventionsTypes();
  }, []);

  return (
    <>
      <WorksiteInterventionForm
        interventionTypesGroupedByFamily={
          interventionTypesGroupedByFamily
            ? interventionTypesGroupedByFamily?.family_intervention_type
            : []
        }
        interventionPartners={
          interventionPartners ? interventionPartners?.intervention_partner : []
        }
      />
    </>
  );
};

export default WorksiteInterventions;
