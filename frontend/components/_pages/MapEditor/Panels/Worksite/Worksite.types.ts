export type TWorksiteStep =
  | "worksite"
  | "locations"
  | "intervention"
  | "validateWorksiteGroup";

export interface WorkSiteFormProps {
  goToNextStep?: () => void;
}
