import React, { FC, useEffect, useState } from "react";
import { Alert, AlertTitle, FormControlLabel, Switch } from "@mui/material";

import { useMapEditorActions } from "@stores/pages/mapEditor";
import { useTranslation } from "react-i18next";
import {
  usePolygonCommonData,
  usePolygonFormActions,
} from "@stores/forms/polygonForm";

export interface IDrawPolygonProps {
  goToPreviousStep?: () => void;
  goToNextStep?: () => void;
}

const DrawPolygon: FC<IDrawPolygonProps> = ({}) => {
  /** hooks */
  const { t } = useTranslation(["components", "common"]);
  const polygonData = usePolygonCommonData();
  const { renderSelectionLayer, removeLayer } = useMapEditorActions();
  const { setPolygonCommonData } = usePolygonFormActions();

  useEffect(() => {
    renderSelectionLayer(true);
    setPolygonCommonData({ ...polygonData });
  }, []);

  const handleRemovePolygon = (locationId) => {
    console.log("remove");
  };

  const handleAssociationChange = () => {
    setPolygonCommonData({
      ...polygonData,
      automaticAssociation: !polygonData.automaticAssociation,
    });
  };

  return (
    <>
      <Alert severity="info">
        <AlertTitle>{t(`GreenAreaForm.drawPolygon.title`)}</AlertTitle>
        {t(`PolygonForm.draw`)}
      </Alert>
      <FormControlLabel
        control={
          <Switch
            checked={polygonData.automaticAssociation}
            onChange={handleAssociationChange}
            name="automaticAssociation"
            color="primary"
          />
        }
        label={t(`components.PolygonForm.associationMessage`)}
      />
    </>
  );
};

export default DrawPolygon;
