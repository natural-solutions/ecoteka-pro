import React, { FC } from "react";
import { useTranslation } from "react-i18next";
import { Stack } from "@mui/material";
import _ from "lodash";
import { useSession } from "next-auth/react";
import useStore from "@stores/useStore";
import { useRouter } from "next/router";

import StepperSection from "@ecosystems/MapEditor/Stepper/StepperSection";
import DrawPolygon from "./DrawPolygon";
import {
  useCreateBoundaryAndDeleteExistingAssociationsMutation,
  useCreateBoundaryMutation,
  useCreateVegetatedAreaMutation,
  useDeleteBoundaryMutation,
  useInsertLocationBoundariesMutation,
  useInsertLocationVegetatedAreaMutation,
  useInsertVegetatedAreaBoundariesMutation,
  useUpdateBoundaryAndDeleteExistingAssociationsMutation,
  useUpdateVegetatedAreaAndDeleteExistingLocationsMutation,
} from "@generated/graphql";
import {
  useMapActiveAction,
  useMapEditorActions,
} from "@stores/pages/mapEditor";
import {
  PolygonObjectType,
  useBoundaryFormData,
  usePolygonCommonData,
  usePolygonFormActions,
  usePolygonObjectType,
  useVegetatedAreaFormData,
} from "@stores/forms/polygonForm";
import { DetailsHeader } from "@components/_core/headers";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import PolygonFields from "@components/forms/polygon/PolygonFields";
import VegetatedAreaCompositionFields from "@components/forms/polygon/VegetatedAreaCompositionFields";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

interface PolygonPanelProps {}

export const vegetatedAreaSchema = yup.object().shape({
  type: yup.string().required(),
  address: yup.string().nullable(),
  administrative_boundary: yup.string().nullable(),
  geographic_boundary: yup.string().nullable(),
  station_boundary: yup.string().nullable(),
  landscape_type: yup.string().nullable(),
  urban_site_id: yup.string().nullable(),
  urban_constraint: yup.array().of(yup.string()).nullable(),
  has_differentiated_mowing: yup.boolean().nullable(),
  is_accessible: yup.boolean().nullable(),
  residential_usage_type: yup.array().of(yup.string()).nullable(),
  inconvenience_risk: yup.string().nullable(),
  note: yup.string().nullable(),
  shrubs_data: yup.array().nullable(),
  herbaceous_data: yup.array().nullable(),
});

const PolygonPanel: FC<PolygonPanelProps> = ({}) => {
  /** HOOKS */
  const { t } = useTranslation(["components", "common"]);
  const router = useRouter();
  const polygonData = usePolygonCommonData();
  const objectType = usePolygonObjectType();
  const boundaryFormData = useBoundaryFormData();
  const vegetatedAreaFormData = useVegetatedAreaFormData();

  const { app } = useStore((store) => store);
  const session = useSession();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";

  const [createBoundary] = useCreateBoundaryMutation();
  const [deleteBoundaryMutation] = useDeleteBoundaryMutation();
  const [createBoundaryAndDeleteExistingAssociations] =
    useCreateBoundaryAndDeleteExistingAssociationsMutation();
  const [updateBoundaryAndDeleteExistingAssociations] =
    useUpdateBoundaryAndDeleteExistingAssociationsMutation();
  const [updateVegetatedAreaAndDeleteExistingAssociations] =
    useUpdateVegetatedAreaAndDeleteExistingLocationsMutation();
  const [createVegetatedArea] = useCreateVegetatedAreaMutation();
  const [insertLocationBoundaries] = useInsertLocationBoundariesMutation();
  const [insertVegetatedAreaBoundaries] =
    useInsertVegetatedAreaBoundariesMutation();
  const [insertLocationVegetatedArea] =
    useInsertLocationVegetatedAreaMutation();

  const isReadOnly = () =>
    !router.query.hasOwnProperty("edit") && router.query.activePolygon
      ? true
      : false;
  const userRole = session?.data?.roles;
  const {
    renderSelectionLayer,
    setMapSelectedLocations,
    setMapPanelOpen,
    setForceReloadLayer,
    setMapActiveAction,
  } = useMapEditorActions();
  const mapActiveAction = useMapActiveAction();
  const {
    setPolygonObjectType,
    setBoundaryFormData,
    setVegetatedAreaFormData,
  } = usePolygonFormActions();
  const { setFiltersWithMartin, filtersWithMartin } =
    useFiltersWithMartinContext()!;

  const defaultValues = vegetatedAreaFormData || {};

  const { setValue, watch, control } = useForm({
    resolver: yupResolver(vegetatedAreaSchema),
    defaultValues: defaultValues,
  });

  const validatePolygonForm = () => {
    if (!polygonData.geometry.coordinates.length) {
      return false;
    }
    return true;
  };

  const polygonDrawStep = {
    title: t("GreenAreaStep.idLabels.drawPolygon"),
    description: <DrawPolygon />,
    validate: validatePolygonForm,
  };

  const polygonFieldsStep = {
    title: t(`components.PolygonForm.label`),
    description: (
      <PolygonFields
        objectType={objectType}
        boundaryFormData={boundaryFormData}
        setBoundaryFormData={setBoundaryFormData}
        setPolygonObjectType={setPolygonObjectType}
        isReadOnly={isReadOnly()}
        control={control}
        setValue={setValue}
        watch={watch}
        userRole={userRole}
      />
    ),
    validate: () => {
      if (objectType === "vegetated_area" && !vegetatedAreaFormData?.type) {
        return false;
      } else if (objectType !== "vegetated_area" && !boundaryFormData?.name) {
        return false;
      } else {
        return true;
      }
    },
  };

  const vegetatedAreaCompositionStep = {
    title: t(`components.PolygonForm.vegetatedAreaCompositionLabel`),
    description: (
      <VegetatedAreaCompositionFields
        isReadOnly={isReadOnly()}
        setVegetatedAreaFormData={setVegetatedAreaFormData}
        vegetatedAreaFormData={vegetatedAreaFormData!}
        control={control}
        setValue={setValue}
        watch={watch}
      />
    ),
    validate: () => true,
  };

  /** This function create custom steps for stepper
   *
   * If activePolygon query, hide draw polygon step
   *
   * If edit mode, show it
   */
  const generateSteps = () => {
    return [
      (router.query.hasOwnProperty("edit") && router.query.activePolygon) ||
      (mapActiveAction === "addPolygon" && !router.query.activePolygon)
        ? polygonDrawStep
        : undefined,
      router.query.type !== "vegetated-area" ? polygonFieldsStep : undefined,
      ,
      objectType === "vegetated_area" &&
      !router.query.activePolygon &&
      vegetatedAreaFormData?.type !== "potential_area"
        ? vegetatedAreaCompositionStep
        : undefined,
    ];
  };
  let steps = generateSteps();

  const onComplete = async (result: string, action: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message:
              result === "success"
                ? t(`components.PolygonForm.success.${action}.${objectType}`)
                : "Une erreur est survenue",
            severity: result === "success" ? "success" : "error",
          },
        ],
      },
    });
    setMapActiveAction(undefined);
    setMapSelectedLocations(undefined);
    renderSelectionLayer(false);
    setMapPanelOpen(false);
    setForceReloadLayer(true);

    objectType === "geographic"
      ? setFiltersWithMartin({
          ...filtersWithMartin,
          geographic_boundaries_active: true,
        })
      : objectType === "vegetated_area"
        ? setFiltersWithMartin({
            ...filtersWithMartin,
            vegetated_areas_active: true,
          })
        : setFiltersWithMartin({
            ...filtersWithMartin,
            station_boundaries_active: true,
          });
    router.replace("/map");
  };

  const deleteBoundary = async (id: string) => {
    await deleteBoundaryMutation({
      variables: {
        id: router.query.activePolygon,
      },
      onCompleted: () => {
        onComplete("success", "delete");
      },
    });
    await router.push(`/map`);
  };

  const createBoundaryMutation = async (variables, onComplete) => {
    await createBoundary({
      variables,
      onCompleted: () => onComplete("success", "create"),
    });
  };

  const createVegetatedAreaMutation = async (variables, onComplete) => {
    await createVegetatedArea({
      variables,
      onCompleted: () => onComplete("success", "create"),
    });
  };

  const createBoundaryAndAssociateLocations = async (
    variables,
    locationIds,
    vegetatedAreaIds,
    onComplete
  ) => {
    await createBoundaryAndDeleteExistingAssociations({
      variables,
      onCompleted: async (response) => {
        const boundaryId = response?.insert_boundary_one?.id;
        await Promise.all(
          locationIds.map(async (id) => {
            await insertLocationBoundaries({
              variables: {
                location_id: id,
                boundary_id: boundaryId,
              },
            });
          })
        );
        await Promise.all(
          vegetatedAreaIds.map(async (id) => {
            await insertVegetatedAreaBoundaries({
              variables: {
                vegetated_area_id: id,
                boundary_id: boundaryId,
              },
            });
          })
        );
        onComplete("success", "create");
      },
    });
  };

  const updateBoundaryAndDeleteAssociations = async (
    variables,
    locationIds,
    vegetatedAreaIds,
    onComplete
  ) => {
    await updateBoundaryAndDeleteExistingAssociations({
      variables,
      onCompleted: async (response) => {
        const boundaryId = response?.update_boundary_by_pk?.id;
        await Promise.all(
          locationIds.map(async (id) => {
            await insertLocationBoundaries({
              variables: {
                location_id: id,
                boundary_id: boundaryId,
              },
            });
          })
        );
        await Promise.all(
          vegetatedAreaIds.map(async (id) => {
            await insertVegetatedAreaBoundaries({
              variables: {
                vegetated_area_id: id,
                boundary_id: boundaryId,
              },
            });
          })
        );
        onComplete("success", "update");
      },
    });
  };

  const updateVegetatedAreaAndDeleteAssociations = async (
    variables,
    locationIds,
    onComplete,
    automaticAssociation
  ) => {
    await updateVegetatedAreaAndDeleteExistingAssociations({
      variables,
      onCompleted: async (response) => {
        if (automaticAssociation) {
          const areaId = response?.update_vegetated_area_by_pk?.id;
          await Promise.all(
            locationIds.map(async (id) => {
              await insertLocationVegetatedArea({
                variables: {
                  location_id: id,
                  vegetated_area_id: areaId,
                },
              });
            })
          );
        }
        onComplete("success", "update");
      },
    });
  };

  const onSubmitFinalStep = async () => {
    try {
      const data_entry_user_id = currentUserId;
      const {
        geometry,
        locationsInside,
        vegetatedAreasInside,
        automaticAssociation,
      } = polygonData;

      const coordinates = geometry?.coordinates;
      const id = router.query.activePolygon;

      const commonVariables = {
        coords: { type: "Polygon", coordinates },
        name: boundaryFormData.name,
        type: objectType as PolygonObjectType,
        data_entry_user_id,
      };

      if (objectType && ["geographic", "station"].includes(objectType)) {
        if (automaticAssociation) {
          if (!id) {
            await createBoundaryAndAssociateLocations(
              {
                ...commonVariables,
                locationsIds: locationsInside,
                vegetatedAreasIds: vegetatedAreasInside,
              },
              locationsInside,
              vegetatedAreasInside,
              onComplete
            );
          } else {
            await updateBoundaryAndDeleteAssociations(
              {
                ...commonVariables,
                id: id,
                locationsIds: locationsInside,
                vegetatedAreasIds: vegetatedAreasInside,
              },
              locationsInside,
              vegetatedAreasInside,
              onComplete
            );
          }
        } else {
          if (!id) {
            await createBoundaryMutation(commonVariables, onComplete);
          } else {
            await updateBoundaryAndDeleteExistingAssociations({
              variables: {
                ...commonVariables,
                id: id,
                locationsIds: locationsInside,
                vegetatedAreasIds: vegetatedAreasInside,
              },
              onCompleted: async () => {
                onComplete("success", "update");
              },
            });
          }
        }
      } else {
        const {
          administrative_boundary,
          station_boundary,
          geographic_boundary,
          urban_constraint,
          residential_usage_type,
          urban_site_id,
          afforestation_trees_data,
          ...formDataWithoutObjects
        } = vegetatedAreaFormData || {};
        if (!id) {
          await createVegetatedAreaMutation(
            {
              object: {
                ...formDataWithoutObjects,
                coords: { type: "Polygon", coordinates },
                type: vegetatedAreaFormData?.type,
                linear_meters:
                  vegetatedAreaFormData?.type === "hedge"
                    ? vegetatedAreaFormData?.linear_meters
                    : null,
                hedge_type:
                  vegetatedAreaFormData?.type === "hedge"
                    ? vegetatedAreaFormData?.hedge_type
                    : null,
                potential_area_state:
                  vegetatedAreaFormData?.type === "potential_area"
                    ? vegetatedAreaFormData?.potential_area_state
                    : null,
                afforestation_trees_data:
                  vegetatedAreaFormData?.type === "afforestation"
                    ? vegetatedAreaFormData?.afforestation_trees_data
                    : null,
                surface: polygonData?.area,
                urban_site_id: urban_site_id ? urban_site_id : null,
                boundaries_vegetated_areas: {
                  data: [
                    ...(administrative_boundary !== ""
                      ? [{ boundary_id: administrative_boundary }]
                      : []),
                    ...(geographic_boundary !== ""
                      ? [{ boundary_id: geographic_boundary }]
                      : []),
                    ...(station_boundary !== ""
                      ? [{ boundary_id: station_boundary }]
                      : []),
                  ],
                },
                vegetated_areas_urban_constraints: {
                  data:
                    urban_constraint?.map((constraint) => ({
                      urban_constraint_id: constraint.id,
                    })) || [],
                },
                vegetated_areas_residential_usage_types: {
                  data:
                    residential_usage_type?.map((usage) => ({
                      residential_usage_type: usage.id,
                    })) || [],
                },
                vegetated_areas_locations: {
                  data: automaticAssociation
                    ? locationsInside?.map((id) => ({
                        location_id: id,
                      }))
                    : [],
                },
              },
            },
            onComplete
          );
        } else {
          await updateVegetatedAreaAndDeleteAssociations(
            {
              coords: { type: "Polygon", coordinates },
              id: id,
              locationsIds: locationsInside,
            },
            locationsInside,
            onComplete,
            automaticAssociation
          );
        }
      }
    } catch (err) {
      console.log(`foo = `, err);
      onComplete("failure", "");
    }
  };
  return (
    <Stack data-cy="worksite-form-container">
      {router.query.hasOwnProperty("activePolygon") && (
        <DetailsHeader
          title={`${t(`components.PolygonForm.polygonTypes.${objectType}`)}${
            router.query.type === "boundary"
              ? `: ${boundaryFormData?.name}`
              : ""
          }`}
          withHistoryBack={false}
          withEditMode={
            userRole?.includes("editor") && objectType === "geographic"
              ? false
              : true
          }
          onSave={onSubmitFinalStep}
          onDelete={deleteBoundary}
          withClosePanel
          deleteMessage={t(`components.PolygonForm.message.delete`)}
        />
      )}

      {polygonData && (
        <StepperSection
          onFinish={onSubmitFinalStep}
          steps={steps.filter((step) => step !== undefined)}
        />
      )}
    </Stack>
  );
};

export default PolygonPanel;
