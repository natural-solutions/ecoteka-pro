import { useMemo } from "react";
import { useTranslation } from "react-i18next";
import {
  MRT_ColumnDef,
  useMaterialReactTable,
  MaterialReactTable,
} from "material-react-table";
import useTableLocale from "@lib/useTableLocale";
import { IWorksitesGridProps } from "./WorksitesGrid.types";
import { useRouter } from "next/router";

import { Box, IconButton } from "@mui/material";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { formatDate } from "@lib/utils";

const WorksitesGrid = ({ worksites }: IWorksitesGridProps) => {
  const { t } = useTranslation(["components"]);
  const router = useRouter();
  const localization = useTableLocale();

  const columns = useMemo<MRT_ColumnDef<any>[]>(
    () => [
      {
        accessorFn: (originalRow) => originalRow.name ?? "",
        header: t("WorksiteForm.name"),
      },
      {
        accessorFn: (originalRow) => {
          const interventionTypes = originalRow.interventions
            ?.map((intervention) => intervention?.intervention_type?.slug ?? "")
            .filter((value, index, self) => self.indexOf(value) === index)
            .map((slug) =>
              t(`components.Interventions.interventionsList.${slug}`)
            )
            .join(", ");

          return interventionTypes ?? "";
        },
        header: t(`components.Intervention.properties.interventionType`),
      },
      {
        accessorFn: (originalRow) => {
          const partners = originalRow.interventions
            ?.map(
              (intervention) => intervention?.intervention_partner?.name ?? ""
            )
            .filter((value, index, self) => self.indexOf(value) === index)
            .join(", ");

          return partners ?? "";
        },
        header: t(`components.Intervention.properties.interventionPartner`),
      },
      {
        accessorFn: (originalRow) => {
          const startDate = originalRow?.scheduled_start_date
            ? formatDate(originalRow.scheduled_start_date)
            : "";
          const endDate = originalRow?.scheduled_end_date
            ? formatDate(originalRow.scheduled_end_date)
            : "";
          return { startDate, endDate };
        },
        header: t(`components.Intervention.properties.scheduledDate`),
        filterVariant: "date-range",
        filterFn: "dateRangeBetweenInclusive",
        size: 350,
        Cell: ({ cell }) => {
          const { startDate, endDate } = cell.getValue<{
            startDate: Date;
            endDate: Date;
          }>();
          return `${startDate} - ${endDate}`;
        },
      },
      {
        accessorFn: (originalRow) => {
          const now = new Date();

          if (originalRow.realization_date) {
            return t("common.worksiteRealized");
          } else if (
            !originalRow.realization_date &&
            originalRow.scheduled_end_date < now.toISOString()
          ) {
            return t("common.late");
          } else {
            return t("common.toCome");
          }
        },
        id: "state",
        header: t("Intervention.interventionsTable.state"),
        filterVariant: "select",
        filterSelectOptions: [
          t("common.worksiteRealized"),
          t("common.late"),
          t("common.toCome"),
        ],
        Cell: ({ cell }) => {
          const status = cell.getValue() as string;

          const stateInfo = {
            [t("common.worksiteRealized")]: { color: "green", icon: null },
            [t("common.late")]: { color: "red", icon: null },
            [t("common.toCome")]: { color: "orange", icon: null },
          }[status];

          if (!stateInfo) {
            return <span>{status}</span>;
          }

          const { color, icon } = stateInfo;

          return (
            <Box
              display="flex"
              alignItems="center"
              justifyContent="center"
              sx={{
                borderColor: color,
                border: "solid 1px",
                borderRadius: "4px",
                padding: "2px",
                color: color,
                width: "90px",
              }}
            >
              {icon}
              <span>{status}</span>
            </Box>
          );
        },
      },
    ],
    [t]
  );

  const table = useMaterialReactTable({
    columns: columns as any,
    data: worksites,
    localization: localization,
    enableGlobalFilter: false,
    enableTopToolbar: true,
    enablePagination: true,
    enableRowActions: true,
    enableColumnResizing: true,
    getRowId: (originalRow) => originalRow.id,
    filterFns: {
      dateRangeBetweenInclusive: (row, id, filterValues) => {
        const rowDate = new Date(
          row.original?.interventions[0]?.scheduled_date
        );

        const filterStartDate = filterValues[0] ? filterValues[0] : null;
        const filterEndDate = filterValues[1] ? filterValues[1] : null;

        if (filterStartDate || filterEndDate) {
          rowDate.setTime(
            rowDate.getTime() + rowDate.getTimezoneOffset() * 60 * 1000
          );
        }
        return (
          (!filterStartDate || rowDate >= filterStartDate) &&
          (!filterEndDate || rowDate <= filterEndDate)
        );
      },
    },
    initialState: {
      density: "compact",
      pagination: { pageIndex: 0, pageSize: 30 },
    },
    renderRowActions: ({ row }) => (
      <Box>
        <IconButton
          onClick={() => router.push(`/worksites/${row.original.id}`)}
        >
          <ChevronRightIcon />
        </IconButton>
      </Box>
    ),

    positionActionsColumn: "last",
  });

  return <MaterialReactTable table={table} />;
};

export default WorksitesGrid;
