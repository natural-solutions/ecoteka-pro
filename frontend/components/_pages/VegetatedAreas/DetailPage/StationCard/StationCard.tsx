import { Box, Card, CardContent, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import {
  CountTreesFromBoundaryQuery,
  GetVegetatedAreasFromStationBoundaryQuery,
} from "@generated/graphql";

interface StationCardProps {
  vegetatedAreasFromStation:
    | GetVegetatedAreasFromStationBoundaryQuery["vegetated_area_aggregate"]
    | undefined;
  treesFromStation:
    | CountTreesFromBoundaryQuery["boundaries_locations_aggregate"]
    | undefined;
  name: string;
}

const sumJsonData = (data, key) => {
  return data.reduce((acc, item) => {
    const jsonData = item[key];
    if (Array.isArray(jsonData)) {
      const sum = jsonData.reduce(
        (sumAcc, obj) => sumAcc + (obj.number || 0),
        0
      );
      return acc + sum;
    }
    return acc;
  }, 0);
};

const VegetationData = ({ count, labelKey }) => {
  const { t } = useTranslation(["components"]);
  return (
    <Typography variant="body1" component="div">
      {count} {t(labelKey)}
    </Typography>
  );
};

const StationCard: React.FC<StationCardProps> = ({
  vegetatedAreasFromStation,
  treesFromStation,
  name,
}) => {
  const { t } = useTranslation(["pages", "components"]);

  if (!vegetatedAreasFromStation) {
    return null;
  }

  // Mapping the vegetated areas data to create a summary for each type
  const summaryVegetatedAreaType = vegetatedAreasFromStation?.nodes.reduce(
    (acc, curr) => {
      const type = curr.type;
      if (!acc[type]) {
        acc[type] = { count: 0 };
      }
      acc[type].count += 1;
      return acc;
    },
    {} as Record<string, { count: number }>
  );

  const totalSurface = vegetatedAreasFromStation?.aggregate?.sum?.surface || 0;

  const totalShrubs = sumJsonData(
    vegetatedAreasFromStation.nodes,
    "shrubs_data"
  );
  const totalHerbaceous = sumJsonData(
    vegetatedAreasFromStation.nodes,
    "herbaceous_data"
  );

  return (
    <Card sx={{ minWidth: "275px" }}>
      <CardContent
        sx={{
          display: "flex",
          flexDirection: "column",
          padding: "16px",
        }}
      >
        <Typography
          variant="h5"
          component="div"
          sx={{
            padding: "0px 0px 16px 0px",
          }}
        >
          {t("VegetatedAreas.DetailPage.StationCard.title")}: {name}
        </Typography>
        <Typography
          variant="h6"
          component="div"
          sx={{
            padding: "0px 0px 16px 0px",
          }}
        >
          {t("VegetatedAreas.DetailPage.StationCard.subtitle")}
        </Typography>
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "space-around",
            mb: 1,
          }}
        >
          {Object.entries(summaryVegetatedAreaType).map(([type, data]) => (
            <Box key={type}>
              <Typography variant="body1" component="div">
                {`${data.count} ${t(
                  `components.VegetatedAreaForm.properties.typeLabels.${type}`
                )}`}
              </Typography>
              <Typography variant="body1" component="div">
                {Math.round(totalSurface)} m²
              </Typography>
            </Box>
          ))}
        </Box>
        <Typography
          variant="h6"
          component="div"
          sx={{
            padding: "0px 0px 16px 0px",
          }}
        >
          {t("VegetatedAreas.DetailPage.StationCard.subtitlePlants")}
        </Typography>
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "space-around",
            mb: 1,
          }}
        >
          <VegetationData
            count={treesFromStation?.aggregate?.count}
            labelKey="components.VegetatedAreaForm.properties.vegetatedAreaComposition.trees.title"
          />
          <VegetationData
            count={totalShrubs}
            labelKey="components.VegetatedAreaForm.properties.vegetatedAreaComposition.shrubs.title"
          />
          <VegetationData
            count={totalHerbaceous}
            labelKey="components.VegetatedAreaForm.properties.vegetatedAreaComposition.herbaceous.title"
          />
        </Box>
      </CardContent>
    </Card>
  );
};

export default StationCard;
