import { useRouter } from "next/router";
import { CardMap } from "@components/_core/cards";
import Geolocalisation from "@components/map/Geolocalisation";
import MapNavigation, { Direction } from "@components/map/buttons/Navigation";
import { Box, Button, Card, CardContent, Typography } from "@mui/material";
import { useEffect, useRef, useState } from "react";
import { Layer, MapRef, Source } from "react-map-gl";
import type { FillLayer, LngLatBoundsLike } from "react-map-gl";
import { FlyToInterpolator } from "deck.gl";
import { useTranslation } from "react-i18next";
import bbox from "@turf/bbox";
import VegetatedAreaFields from "@components/forms/polygon/VegetatedAreaFields";
import { usePolygonFormActions } from "@stores/forms/polygonForm";
import center from "@turf/center";
import {
  FetchOneVegetatedAreaQuery,
  FetchVegetatedAreaFormDataQuery,
} from "@generated/graphql";
import {
  useMapEditorActions,
  useMapStyle,
  useModalDataOpen,
} from "@stores/pages/mapEditor";
import {
  Control,
  FieldNamesMarkedBoolean,
  FieldValues,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import ConfirmDialog from "@components/forms/dialog/ConfirmDialog";

const layerStyle: FillLayer = {
  id: "polygon-fill",
  type: "fill",
  source: "sample-polygon",
  paint: {
    "fill-color": "rgb(69, 214, 45)",
    "fill-opacity": 0.6,
  },
};

const styles = {
  topActionsContainerRight: {
    position: "absolute",
    top: { md: 90, xs: 84 },
    width: "100%",
    display: "flex",
    justifyContent: { md: "right ", xs: "right" },
    right: { md: 12, xs: 12 },
  },
};

interface GeoContextCardProps {
  formData: FetchVegetatedAreaFormDataQuery | any;
  activeVegetatedArea: FetchOneVegetatedAreaQuery | undefined;
  control: Control;
  setValue?: UseFormSetValue<FieldValues> | undefined;
  watch?: UseFormWatch<FieldValues> | undefined;
  dirtyFields?: Partial<Readonly<FieldNamesMarkedBoolean<FieldValues>>>;
}

const GeoContextCard: React.FC<GeoContextCardProps> = ({
  activeVegetatedArea,
  formData,
  control,
  setValue,
  watch,
  dirtyFields,
}) => {
  const router = useRouter();
  const { t } = useTranslation(["pages", "components"]);
  const mapRef = useRef<MapRef>(null);
  const { setVegetatedAreaFormData } = usePolygonFormActions();
  const mapStyle = useMapStyle();
  const [openDataModal, setOpenDataModal] = useState<boolean>(false);
  const modalDataOpen = useModalDataOpen();
  const { setModalDataOpen } = useMapEditorActions();

  const [viewState, setViewState] = useState(() => {
    const centerCoords = activeVegetatedArea
      ? center(activeVegetatedArea?.vegetated_area?.coords).geometry.coordinates
      : [6.6339863, 46.5193823]; // Default coordinates if no active vegetated area
    return {
      longitude: centerCoords[0],
      latitude: centerCoords[1],
      zoom: 15,
    };
  });

  const onConfirmModal = () => {
    router.push(
      `/map?activePolygon=${activeVegetatedArea?.vegetated_area?.id}&type=vegetated-area`
    );
  };

  const handleOnZoom = (direction: Direction) => {
    setViewState((prevViewState) => ({
      ...prevViewState,
      zoom:
        direction === Direction.Out
          ? prevViewState.zoom - 1
          : prevViewState.zoom + 1,
    }));
    if (mapRef.current !== null) {
      mapRef.current.setZoom(viewState.zoom);
    }
  };

  const handleOnGeolocate = (x: number, y: number) => {
    const newViewState = {
      longitude: x,
      latitude: y,
      zoom: viewState.zoom,
      pitch: 0,
      bearing: 0,
      transitionDuration: 3000,
      transitionInterpolator: new FlyToInterpolator(),
    };
    if (mapRef.current !== null) {
      mapRef.current?.flyTo({
        center: [newViewState.longitude, newViewState.latitude],
        zoom: viewState.zoom,
        duration: 2000,
      });
    }
  };

  const isReadOnly = () =>
    !router.query.hasOwnProperty("edit") ? true : false;

  useEffect(() => {
    if (mapRef.current && activeVegetatedArea?.vegetated_area?.coords) {
      const polygonCoords = activeVegetatedArea.vegetated_area.coords;
      const bounds = bbox(polygonCoords);
      const adjustedBounds: LngLatBoundsLike = [
        [bounds[0], bounds[1]],
        [bounds[2], bounds[3]],
      ];

      mapRef.current.fitBounds(adjustedBounds, { padding: 20 });
    }
  }, [mapRef.current, activeVegetatedArea]);

  useEffect(() => {
    if (dirtyFields && Object.keys(dirtyFields).length > 0) {
      setModalDataOpen(true);
    } else {
      setModalDataOpen(false);
    }
  }, [dirtyFields && Object.keys(dirtyFields).length]);

  if (!activeVegetatedArea) {
    return null;
  }

  return (
    <Card sx={{ minWidth: "275px" }}>
      <CardMap ref={mapRef} initialViewState={viewState} mapStyle={mapStyle}>
        <MapNavigation onZoom={handleOnZoom} />
        <Box
          sx={[styles.topActionsContainerRight]}
          data-cy="map-actions-container-geolocalisation"
        >
          <Geolocalisation onGeolocate={handleOnGeolocate} />
        </Box>
        {activeVegetatedArea?.vegetated_area?.coords && (
          <Source
            id="sample-polygon"
            type="geojson"
            data={
              {
                type: "FeatureCollection",
                features: [
                  {
                    type: "Feature",
                    id: "vegetated-area",
                    geometry: activeVegetatedArea.vegetated_area.coords,
                  },
                ],
              } as GeoJSON.FeatureCollection<GeoJSON.Geometry>
            }
          >
            <Layer {...layerStyle} />
          </Source>
        )}
      </CardMap>
      <Box sx={{ textAlign: "center" }}>
        <Button
          color="primary"
          variant="contained"
          onClick={() =>
            dirtyFields && Object.keys(dirtyFields).length > 0
              ? setOpenDataModal(true)
              : onConfirmModal()
          }
          sx={{ m: 1 }}
        >
          {t("VegetatedAreas.DetailPage.GeoContextCard.modifyPolygon")}
        </Button>
      </Box>

      <CardContent
        sx={{
          display: "flex",
          flexDirection: "column",
          padding: "16px",
        }}
      >
        <Typography
          variant="h5"
          component="div"
          sx={{
            padding: "0px 0px 16px 0px",
          }}
        >
          {t("VegetatedAreas.DetailPage.GeoContextCard.title")}
        </Typography>

        <VegetatedAreaFields
          setVegetatedAreaFormData={setVegetatedAreaFormData}
          isReadOnly={isReadOnly()}
          formData={formData}
          activeVegetatedArea={activeVegetatedArea}
          control={control}
          setValue={setValue}
          watch={watch}
        />
      </CardContent>
      {modalDataOpen && (
        <Box>
          <ModalComponent
            open={openDataModal}
            handleClose={() => setOpenDataModal(false)}
          >
            <ConfirmDialog
              title={t("common.quitForm")}
              message={t("common.messages.lostData")}
              onConfirm={onConfirmModal}
              onAbort={() => setOpenDataModal(false)}
            />
          </ModalComponent>
        </Box>
      )}
    </Card>
  );
};

export default GeoContextCard;
