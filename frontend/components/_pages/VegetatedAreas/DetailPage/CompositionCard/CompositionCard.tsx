import { useRouter } from "next/router";
import { Card, CardContent, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import { FetchOneVegetatedAreaQuery } from "@generated/graphql";
import VegetatedAreaCompositionFields from "@components/forms/polygon/VegetatedAreaCompositionFields";
import {
  Control,
  FieldValues,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";

interface CompositionCardProps {
  activeVegetatedArea: FetchOneVegetatedAreaQuery | undefined;
  control?: Control;
  setValue?: UseFormSetValue<FieldValues>;
  watch?: UseFormWatch<FieldValues>;
}

const CompositionCard: React.FC<CompositionCardProps> = ({
  activeVegetatedArea,
  control,
  setValue,
  watch,
}) => {
  const router = useRouter();
  const { t } = useTranslation(["pages"]);
  const isReadOnly = () =>
    !router.query.hasOwnProperty("edit") ? true : false;

  if (!activeVegetatedArea) {
    return null;
  }

  return (
    <Card sx={{ minWidth: "275px" }}>
      <CardContent
        sx={{
          display: "flex",
          flexDirection: "column",
          padding: "16px",
        }}
      >
        <Typography
          variant="h5"
          component="div"
          sx={{
            padding: "0px 0px 16px 0px",
          }}
        >
          {t("VegetatedAreas.DetailPage.CompositionCard.title")}
        </Typography>
        <VegetatedAreaCompositionFields
          isReadOnly={isReadOnly()}
          activeVegetatedArea={activeVegetatedArea}
          control={control}
          watch={watch}
          setValue={setValue}
        />
      </CardContent>
    </Card>
  );
};

export default CompositionCard;
