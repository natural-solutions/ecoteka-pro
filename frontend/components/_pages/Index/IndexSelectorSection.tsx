import SelectorSection from "@components/_blocks/dashboard/SelectorSection";
import {
  useDashboardActions,
  useDashboardFilters,
} from "@stores/pages/dashboard";
import { getBoundariesExceptStations } from "@services/AnalyticsService";
import { useEffect, useState } from "react";
import { Boundary } from "types/analytics/analytics.types";
import Loading from "@components/layout/Loading";
import { useSession } from "next-auth/react";

const IndexSelectorSection = () => {
  const dashboardFilters = useDashboardFilters();
  const actions = useDashboardActions();
  const [boundariesData, setBoundariesData] = useState<Boundary[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const session = useSession();
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";

  useEffect(() => {
    const loadData = async () => {
      try {
        setLoading(true);
        const data = await getBoundariesExceptStations(currentUserToken);
        setBoundariesData(data);
      } catch (error) {
        console.error("Error loading boundaries data:", error);
      } finally {
        setLoading(false);
      }
    };

    loadData();
  }, []);

  const handleInputChange = (inputName, inputValue) => {
    const updatedFilters = {
      ...dashboardFilters,
      [inputName]: inputValue,
    };
    handleFilterChange(updatedFilters);
  };

  const handleFilterChange = (updatedFilters) => {
    if (JSON.stringify(dashboardFilters) !== JSON.stringify(updatedFilters)) {
      actions.setDashboardFilters(updatedFilters);
    }
  };

  if (loading) {
    return <Loading />;
  }

  if (!boundariesData.length) {
    return;
  }

  return (
    <SelectorSection
      boundariesData={boundariesData}
      handleInputChange={handleInputChange}
    />
  );
};

export default IndexSelectorSection;
