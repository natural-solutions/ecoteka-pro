import SpeciesDistribution from "@blocks/dashboard/SpeciesDistribution";
import { getTaxonomy } from "@services/AnalyticsService";
import _ from "lodash";
import { useEffect, useState } from "react";
import { useDashboardFilters } from "@stores/pages/dashboard";
import { Alert } from "@mui/material";
import { useSession } from "next-auth/react";

export const SpeciesDistributionWithData = () => {
  const [taxonomy, setTaxonomy] = useState();
  const session = useSession();
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";

  const dashboardFilters = useDashboardFilters();

  useEffect(() => {
    const fetchTaxonomy = async () => {
      try {
        const data = await getTaxonomy(
          dashboardFilters.boundaries,
          currentUserToken
        );
        setTaxonomy(data);
      } catch (err) {
        console.error("Error fetching taxon data:", err);
      }
    };

    fetchTaxonomy();
  }, [dashboardFilters]);

  // if (loading) {
  //   return <Loading />;
  // }

  if (taxonomy) {
    return <SpeciesDistribution data={taxonomy} />;
  }

  return <Alert severity="info">Aucune donnée</Alert>;
};

export default SpeciesDistributionWithData;
