import AnalyticsDendroSection from "@components/_blocks/dashboard/AnalyticsDendroSection/AnalyticsDendroSection";
import { TensioMonitoring } from "@blocks/dashboard/Urbasense";
import Grid from "@mui/material/Grid";
import {
  urbasenseGetMetricsByGroup,
  getUrbasenseMetrics,
} from "@services/AnalyticsService";
import { useEffect, useState } from "react";
import EchartWidget from "@components/_blocks/dashboard/EchartWidget";
import { useTranslation } from "react-i18next";
import { formatDate } from "@lib/utils";
import { useDashboardFilters } from "@stores/pages/dashboard";
import { Paper } from "@mui/material";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import { UrbasenseMetricsByGroupResponse } from "types/analytics/analytics.types";
import Loading from "@components/layout/Loading";
import { useSession } from "next-auth/react";

const IndexAnalytics = () => {
  const { boundaries } = useDashboardFilters();
  const [moduleError, setModuleError] = useState(false);
  const [metricsData, setMetricsData] =
    useState<UrbasenseMetricsByGroupResponse>();
  const [subectIdsData, setSubectIdsData] = useState<string[]>([]);
  const [loading, setLoading] = useState(false);
  const session = useSession();
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";

  const { t } = useTranslation(["components"]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        if (!boundaries.length) {
          const metricsData = await getUrbasenseMetrics(
            { ids: [] },
            currentUserToken
          );
          setMetricsData(metricsData);
          setModuleError(false);
        } else {
          const subjectIdsData = await urbasenseGetMetricsByGroup(
            boundaries,
            currentUserToken
          );
          setSubectIdsData(subjectIdsData);
          setModuleError(false);
        }
      } catch (error) {
        console.error("Error fetching data: ", error);
        setModuleError(true);
      }
      setLoading(false);
    };

    fetchData();
  }, [boundaries]);

  useEffect(() => {
    if (subectIdsData && subectIdsData.length > 0) {
      const fetchData = async () => {
        setLoading(true);
        try {
          const metricsData = await getUrbasenseMetrics(
            { ids: subectIdsData },
            currentUserToken
          );
          setMetricsData(metricsData);
        } catch (error) {
          console.error("Error fetching data: ", error);
          setModuleError(true);
        }
        setLoading(false);
      };
      fetchData();
    } else {
      setModuleError(true);
    }
  }, [subectIdsData]);

  const getTensioMsg = (data: UrbasenseMetricsByGroupResponse) => {
    return !data?.tensio.error ? data?.tensio.msg : undefined;
  };

  const getDendoMsg = (data: UrbasenseMetricsByGroupResponse) => {
    return !data?.dendro?.error ? data?.dendro?.msg : undefined;
  };

  const generateEChartsOptions = (
    data: UrbasenseMetricsByGroupResponse,
    valuesKey1: string,
    valuesKey2: string,
    legendData: string[],
    colors: string[]
  ) => {
    const dates = metricsData?.meteo.msg[valuesKey1].dates;
    const formattedDates = dates?.map(formatDate).slice(-7);

    return {
      color: colors,
      responsive: true,
      tooltip: {
        trigger: "none",
        axisPointer: {
          type: "cross",
        },
      },
      legend: {
        data: legendData,
        textStyle: {
          color: "#89868d",
          fontSize: "12px",
          fontFamily: "Poppins",
        },
      },
      grid: {
        top: 70,
        bottom: 50,
      },
      xAxis: [
        {
          type: "category",
          axisTick: {
            alignWithLabel: true,
          },
          axisLine: {
            onZero: false,
            lineStyle: {
              color: "#89868d",
            },
          },
          axisLabel: {
            rotate: 35,
          },
          data: formattedDates,
        },
      ],
      yAxis: [
        {
          type: "value",
          name: "",
        },
      ],
      series: [
        {
          name: legendData[0],
          type: "line",
          smooth: true,
          emphasis: {
            focus: "series",
          },
          data: data?.meteo.msg[valuesKey1].valeurs,
        },
        {
          name: legendData[1],
          type: "line",
          smooth: true,
          emphasis: {
            focus: "series",
          },
          data: data?.meteo.msg[valuesKey2].valeurs,
        },
      ],
    };
  };

  const getTemperatureData = (data) => {
    const colors = ["#ff4c51", "#3ab4d0"];
    const legendData = [
      t("dashboard.urbasense.meteo.tempAirMax"),
      t("dashboard.urbasense.meteo.UTCI"),
    ];
    return generateEChartsOptions(
      data,
      "tempAirMax",
      "utci",
      legendData,
      colors
    );
  };

  const getAirData = (data) => {
    const colors = ["#61d878", "#9155fd"];
    const legendData = [
      t("dashboard.urbasense.meteo.vitVentMax"),
      t("dashboard.urbasense.meteo.humAirMoy"),
    ];
    return generateEChartsOptions(
      data,
      "humAirMoy",
      "vitVentMax",
      legendData,
      colors
    );
  };

  if (loading) {
    return <Loading />;
  }

  return (
    <Grid container data-cy="dashboard-urbasense-analytics-paper">
      <Grid
        item
        xs={12}
        sm={6}
        lg={6}
        p={2}
        data-cy="dashboard-urbasense-analytics-grid-temperature"
      >
        <DashboardModuleHeader title={t("Dashboard.meteo")} />
        <Paper
          elevation={1}
          id="meteo"
          sx={{ p: 3 }}
          data-cy="dashboard-urbasense-analytics-temperature"
        >
          <EchartWidget
            config={
              !metricsData?.meteo?.error
                ? getTemperatureData(metricsData)
                : undefined
            }
            loading={loading}
            error={metricsData?.meteo?.error ? true : false}
          />
        </Paper>
      </Grid>
      <Grid
        item
        xs={12}
        sm={6}
        lg={6}
        p={2}
        data-cy="dashboard-urbasense-analytics-grid-air"
      >
        <DashboardModuleHeader title={t("Dashboard.meteo")} />
        <Paper
          elevation={1}
          id="meteo"
          sx={{ p: 3 }}
          data-cy="dashboard-urbasense-analytics-air"
        >
          <EchartWidget
            config={
              !metricsData?.meteo?.error ? getAirData(metricsData) : undefined
            }
            loading={loading}
            error={metricsData?.meteo?.error ? true : false}
          />
        </Paper>
      </Grid>

      {getDendoMsg(metricsData!) != undefined && (
        <Grid
          item
          xs={12}
          sm={6}
          lg={6}
          p={2}
          data-cy="dashboard-urbasense-analytics-grid-dendro"
        >
          <AnalyticsDendroSection
            data={getDendoMsg(metricsData!)}
            loading={loading}
            error={moduleError}
          />
        </Grid>
      )}
      {getTensioMsg(metricsData!) != undefined && (
        <Grid
          item
          xs={12}
          sm={6}
          lg={6}
          p={2}
          data-cy="dashboard-urbasense-analytics-grid-tensio"
        >
          <TensioMonitoring
            data={getTensioMsg(metricsData!)}
            loading={loading}
            error={moduleError}
          />
        </Grid>
      )}
    </Grid>
  );
};

export default IndexAnalytics;
