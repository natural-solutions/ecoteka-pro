import Grid from "@mui/material/Grid";
import { useEffect, useState } from "react";
import DangerousTree from "@components/_blocks/dashboard/TreeInformation/DangerousTree";
import TreeInformation from "@components/_blocks/dashboard/TreeInformation/TreeInformation";
import LateIntervention, {
  LateInterventionData,
} from "@components/_blocks/dashboard/TreeInformation/LateIntervention";
import {
  getNotableTrees,
  getDangerousTree,
  getLateInterventionByFamily,
} from "@services/AnalyticsService";
import { useDashboardFilters } from "@stores/pages/dashboard";
import { useSession } from "next-auth/react";

const IndexTreeInformation = () => {
  const [notableTree, setNotableTree] = useState([]);
  const [dangerousTree, setDangerousTree] = useState();
  const [lateIntervention, setLateIntervention] =
    useState<LateInterventionData[]>();
  const { boundaries } = useDashboardFilters();
  const session = useSession();
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";

  const fetchNotableTreeseData = async (params) => {
    try {
      const data = await getNotableTrees(params, currentUserToken);
      setNotableTree(data);
    } catch (err) {
      console.error("Error fetching notable tree data:", err);
    }
  };
  const fetchDangerousTree = async (params) => {
    try {
      const data = await getDangerousTree(params, currentUserToken);
      setDangerousTree(data);
    } catch (err) {
      console.error("Error fetching notable tree data:", err);
    }
  };
  const fetchLateIntervention = async (params) => {
    try {
      const data = await getLateInterventionByFamily(params, currentUserToken);
      setLateIntervention(data);
    } catch (err) {
      console.error("Error fetching notable tree data:", err);
    }
  };
  useEffect(() => {
    fetchDangerousTree(boundaries);
    fetchLateIntervention(boundaries);
    fetchNotableTreeseData(boundaries);
  }, [boundaries]);

  // if (isLoading) {
  //   return <Loading />;
  // }

  // if (error) {
  //   return <Alert severity="info">Aucune donnée</Alert>;
  // }

  return (
    <Grid
      container
      justifyContent="space-between"
      data-cy="dashboard-tree-information-grid"
    >
      <Grid
        item
        xs={12}
        sm={12}
        md={6}
        lg={6}
        xl={3.8}
        data-cy="dashboard-urbasense-analytics-grid-tensio"
      >
        <LateIntervention data={lateIntervention || []} />
      </Grid>
      <Grid
        item
        xs={12}
        sm={12}
        md={6}
        lg={6}
        xl={3.8}
        data-cy="dashboard-urbasense-analytics-grid-tensio"
      >
        <DangerousTree data={dangerousTree} />
      </Grid>
      <Grid
        item
        xs={12}
        sm={12}
        md={6}
        lg={6}
        xl={3.8}
        data-cy="dashboard-urbasense-analytics-grid-tensio"
      >
        <TreeInformation data={notableTree} />
      </Grid>
    </Grid>
  );
};

export default IndexTreeInformation;
