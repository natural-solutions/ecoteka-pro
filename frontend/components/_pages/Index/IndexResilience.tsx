import React, { useState, useEffect } from "react";
import { Alert, Box } from "@mui/material";
import { ResilienceData } from "types/analytics/analytics.types";
import { getResilienceScore } from "@services/AnalyticsService";
import Loading from "@components/layout/Loading";
import { useTranslation } from "react-i18next";
import Resilience from "@components/_blocks/dashboard/Ilo/Resilience";
import EchartWidget from "@components/_blocks/dashboard/EchartWidget";
import { useDashboardFilters } from "@stores/pages/dashboard";
import { useSession } from "next-auth/react";

const IndexResilience = () => {
  const { t } = useTranslation(["components"]);
  const [resilienceData, setResilienceData] = useState<ResilienceData | null>(
    null
  );
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const dashboardFilters = useDashboardFilters();
  const session = useSession();
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";
  useEffect(() => {
    const fetchResilienceData = async () => {
      try {
        const data = await getResilienceScore(
          dashboardFilters.boundaries,
          currentUserToken
        );
        setResilienceData(data);
        setLoading(false);
      } catch (err) {
        console.error("Error fetching resilience data:", err);
        setError(err);
        setLoading(false);
      }
    };

    fetchResilienceData();
  }, [dashboardFilters.boundaries]);

  if (loading) {
    return <Loading />;
  }

  if (!resilienceData) {
    return <Alert severity="info">Aucune donnée</Alert>;
  }

  if (resilienceData) {
    const createPieSeries = (data, keyAccessor, valueAccessor, colors) => ({
      type: "pie",
      radius: ["70%", "60%"],
      color: colors,
      avoidLabelOverlap: false,
      label: {
        show: false,
      },
      emphasis: {
        itemStyle: {
          shadowBlur: 3,
          shadowOffsetX: 0,
          transition: "all 0.3s",
        },
      },
      data: data.map((entry) => ({
        name: keyAccessor(entry),
        value: valueAccessor(entry),
      })),
    });

    const echartsOptions = {
      tooltip: {
        trigger: "item",
      },
      series: [
        createPieSeries(
          [
            { vulnerability_score: resilienceData?.vulnerability_score },
            {
              vulnerability_score:
                Math.round(
                  (5 - (resilienceData?.vulnerability_score || 0)) * 10
                ) / 10,
            },
          ],
          (entry) => {
            const originalValue = entry.vulnerability_score;
            return originalValue === resilienceData?.vulnerability_score
              ? t("blocks.Resilience.analyticsModules.vulnerabilityScore.title")
              : "";
          },
          (entry) => entry.vulnerability_score,
          ["#3AB4D0", "#EEEEEE"] // Colors for the biodiversity chart
        ),
      ],
    };

    const vulnerabilityScore = resilienceData.vulnerability_score || 0;
    const maxScore = 5; // Assuming maximum score is 5
    const percentage = Math.round((vulnerabilityScore / maxScore) * 100); //

    const resilienceScore = [
      {
        component: (
          <Box
            sx={{ height: "auto" }}
            data-cy="dashboard-resilience-vulnerability-echart"
          >
            <EchartWidget
              config={echartsOptions}
              error={false}
              customStyle={customStyle}
            />
          </Box>
        ),
        title: t("blocks.Resilience.analyticsModules.vulnerabilityScore.title"),
        value: `${percentage} %
          ${t("blocks.Resilience.analyticsModules.vulnerabilityScore.label")}`,
      },
    ];

    return <Resilience data={resilienceScore} />;
  }
};

export default IndexResilience;

const customStyle = {
  height: "250px",
  width: "200px",
};
