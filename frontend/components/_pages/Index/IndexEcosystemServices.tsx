import EchartWidget from "@components/_blocks/dashboard/EchartWidget";
import EcosystemServices from "@components/_blocks/dashboard/Ilo/EcosystemServices";
import { Alert, Box } from "@mui/material";
import { AirQuality, Icu } from "@components/_core/icons";
import { getAllEcosystemMetrics } from "@services/AnalyticsService";
import { useEffect, useState } from "react";
import { convertToPercentage, getLabelWithScore } from "@lib/utils";
import { useTranslation } from "react-i18next";
import { useDashboardFilters } from "@stores/pages/dashboard";
import { EcosystemMetrics } from "types/analytics/analytics.types";
import { useSession } from "next-auth/react";

const IndexAnalytics = () => {
  const { t } = useTranslation(["components"]);
  const [ecosystemMetrics, setEcosystemMetrics] =
    useState<EcosystemMetrics | null>(null);
  const session = useSession();
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";

  const dashboardFilters = useDashboardFilters();

  useEffect(() => {
    const fetchEcosystemMetrics = async () => {
      try {
        const data = await getAllEcosystemMetrics(
          dashboardFilters.boundaries,
          currentUserToken
        );
        setEcosystemMetrics(data);
      } catch (err) {
        console.error("Error fetching taxon data:", err);
      }
    };

    fetchEcosystemMetrics();
  }, [dashboardFilters]);

  if (!ecosystemMetrics) {
    return <Alert severity="info">Aucune donnée</Alert>;
  }

  if (ecosystemMetrics) {
    const getCustomLabel = (carbonRange: string | undefined | null) => {
      switch (carbonRange) {
        case "0-1":
          return t("blocks.EcosystemServices.ranges.low");
        case "1-3":
          return t("blocks.EcosystemServices.ranges.medium");
        case "4-5":
          return t("blocks.EcosystemServices.ranges.good");
        case "unknown":
          return t("blocks.EcosystemServices.ranges.unknown");
        default:
          return carbonRange;
      }
    };

    const carbonStorageColors = ["#9055FD", "#16b1ff", "#FCD400", "#eee"];
    const biodivColors = ["#9055FD", "#eeeeee"];
    const biodiversityData = ecosystemMetrics.biodiversity_score?.[0];

    const createPieSeries = (data, keyAccessor, valueAccessor, colors) => ({
      type: "pie",
      radius: ["70%", "60%"],
      color: colors,
      avoidLabelOverlap: false,
      label: {
        show: false,
      },
      emphasis: {
        itemStyle: {
          shadowBlur: 3,
          shadowOffsetX: 0,
          transition: "all 0.3s",
        },
      },

      data:
        data &&
        data.map((entry) => ({
          name: keyAccessor(entry),
          value: valueAccessor(entry),
        })),
    });

    const biodivOptions = {
      tooltip: {
        trigger: "item",
      },
      series: [
        createPieSeries(
          [
            { biodiversity_score: biodiversityData?.biodiversity_score },
            {
              biodiversity_score:
                Math.round(
                  (5 - (biodiversityData?.biodiversity_score || 0)) * 10
                ) / 10,
            },
          ],
          (entry) => {
            const originalValue = entry.biodiversity_score;
            return originalValue === biodiversityData?.biodiversity_score
              ? t(
                  "blocks.EcosystemServices.analyticsModules.biodiversity.title"
                )
              : "";
          },
          (entry) => entry.biodiversity_score,
          biodivColors // Colors for the biodiversity chart
        ),
      ],
    };
    const carbonStorageData = ecosystemMetrics.carbon_storage_score?.data;
    const legendData = Array.isArray(carbonStorageData)
      ? carbonStorageData.map((entry) => getCustomLabel(entry?.carbon_range))
      : [];
    const echartsOptions = {
      tooltip: {
        trigger: "item",
        formatter: "{b} : {c} ({d}%)",
      },
      legend: {
        orient: "horizontal",
        top: "bottom",
        data: ecosystemMetrics.carbon_storage_score.data?.map((entry) =>
          getCustomLabel(entry?.carbon_range)
        ),
      },
      series: [
        createPieSeries(
          ecosystemMetrics.carbon_storage_score.data,
          (entry) => getCustomLabel(entry?.carbon_range),
          (entry) => entry?.tree_count,
          carbonStorageColors
        ),
      ],
    };

    const ecosystemData = [
      {
        component: (
          <Box
            sx={{ height: "100%" }}
            data-cy="dashboard-ecosystem-carbon-echart"
          >
            <EchartWidget
              config={echartsOptions}
              error={false}
              customStyle={customStyle}
            />
          </Box>
        ),
        title: t("blocks.EcosystemServices.analyticsModules.carbon"),
      },
      {
        component: (
          <Box sx={customBox} data-cy="dashboard-ecosystem-air-quality">
            <AirQuality
              level={convertToPercentage(
                ecosystemMetrics.air_quality_score?.[0]?.air_score,
                5
              )}
            />
          </Box>
        ),
        value: ecosystemMetrics.air_quality_score?.[0]?.air_score,
        unit: "/5",
        title: t("blocks.EcosystemServices.analyticsModules.air"),
      },
      {
        component: (
          <Box sx={customBox} data-cy="dashboard-ecosystem-icu">
            <Icu
              level={convertToPercentage(
                ecosystemMetrics.icu_score?.[0]?.icu_score,
                5
              )}
            />
          </Box>
        ),
        value: ecosystemMetrics.icu_score?.[0]?.icu_score,
        unit: "/5",
        title: t("blocks.EcosystemServices.analyticsModules.icu"),
      },
      {
        component: (
          <Box
            sx={{ height: "100%" }}
            data-cy="dashboard-ecosystem-biodiv-echart"
          >
            <EchartWidget
              config={biodivOptions}
              error={false}
              customStyle={customStyle}
            />
          </Box>
        ),

        value: t(
          `blocks.EcosystemServices.analyticsModules.biodiversity.labels.${getLabelWithScore(ecosystemMetrics.biodiversity_score?.[0]?.biodiversity_score)}`
        ),
        unit: "",
        title: t(
          "blocks.EcosystemServices.analyticsModules.biodiversity.title"
        ),
      },
    ];

    return <EcosystemServices data={ecosystemData} />;
  }
};

export default IndexAnalytics;

const customStyle = {
  height: "250px",
  width: "200px",
};

const customBox = {
  ...customStyle,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};
