import { FC } from "react";
import Map from "react-map-gl";
import maplibregl from "maplibre-gl";
import DeckGL from "@deck.gl/react/typed";
import { InitialViewState } from "@components/map/Base";
import { Box } from "@mui/material";
import MapNavigation, { Direction } from "@components/map/buttons/Navigation";

interface InterventionCardMapProps {
  initialViewState?: InitialViewState;
  controller?: boolean;
  mapStyle?: string;
  layers: any[];
  onClick?(info): void;
  handleOnViewStateChange?(viewState: any): void;
  handleOnZoom(direction: Direction): void;
  cursor?: "pointer" | "auto";
  children?: JSX.Element;
}

const InterventionCardMap: FC<InterventionCardMapProps> = ({
  initialViewState,
  mapStyle = "https://studio.ecoteka.org/api/v1/maps/style?theme=light",
  layers,
  onClick,
  handleOnViewStateChange,
  handleOnZoom,
  cursor = "pointer",
}) => {
  return (
    <Box width={"100%"} height={300} sx={{ position: "relative" }}>
      <DeckGL
        controller={{ doubleClickZoom: false, inertia: true }}
        onViewStateChange={handleOnViewStateChange}
        viewState={initialViewState}
        layers={layers}
        onClick={onClick}
        getCursor={() => cursor}
      >
        <MapNavigation onZoom={handleOnZoom} />
        {/*@ts-ignore: MapProps type error on fog props not used here...*/}
        <Map
          mapLib={maplibregl as any}
          mapStyle={mapStyle}
          attributionControl={false}
          reuseMaps
        />
      </DeckGL>
    </Box>
  );
};

export default InterventionCardMap;
