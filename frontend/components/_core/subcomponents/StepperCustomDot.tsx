// ** MUI Imports
import MuiBox, { BoxProps } from "@mui/material/Box";
import { StepIconProps } from "@mui/material/StepIcon";
import { styled, useTheme } from "@mui/material/styles";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

// ** Hooks Imports
import useBgColor, { UseBgColorType } from "@core/hooks/useBgColor";

// Styled Box component
const Box = styled(MuiBox)<BoxProps>(() => ({
  width: 20,
  height: 20,
  borderWidth: 3,
  borderRadius: "50%",
  borderStyle: "solid",
}));

const StepperCustomDot = (props: StepIconProps) => {
  // ** Props
  const { active, completed, error } = props;

  // ** Hooks
  const theme = useTheme();
  const bgColors: UseBgColorType = useBgColor();

  if (error) {
    return <CheckCircleIcon />;
  } else if (completed) {
    return <CheckCircleIcon color="primary" transform="scale(1.2)" />;
  } else {
    return (
      <Box
        sx={{
          borderColor: bgColors.primaryLight.backgroundColor,
          ...(active && {
            borderWidth: 5,
            borderColor: "primary.main",
            backgroundColor:
              theme.palette.mode === "light"
                ? "common.white"
                : "background.default",
          }),
        }}
      />
    );
  }
};

export default StepperCustomDot;
