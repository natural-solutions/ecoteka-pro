import React, { Dispatch, SetStateAction } from "react";

const statusList = {
  stump: "#ac7566",
  alive: "#2fa37c",
  empty: "#fff",
};

interface CircleProps {
  size?: number;
  status: string;
  selectedCircle?: Number | null;
  setSelectedCircle?: Dispatch<SetStateAction<Number | null>>;
  index: Number;
}

function Circle({
  size = 20,
  index,
  selectedCircle,
  setSelectedCircle,
  status,
}: CircleProps) {
  const fillColor = selectedCircle === index ? "#FFF350" : statusList[status];

  const handleCircleClick = () => {
    setSelectedCircle && setSelectedCircle(index);
  };

  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 24 24"
      style={{ cursor: "pointer", stroke: "none", fill: fillColor }}
      onClick={handleCircleClick}
    >
      <circle cx="12" cy="12" r="10" />
    </svg>
  );
}

export default React.memo(Circle);
