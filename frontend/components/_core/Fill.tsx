import React from "react";
import { Box } from "@mui/material";

interface Props {
  color: string;
}

const FullWidthDiv: React.FC<Props> = ({ color }) => {
  return <Box sx={{ width: "100%", height: "100%", bgcolor: color }}></Box>;
};

export default FullWidthDiv;
