import { forwardRef, Ref } from "react";
import MuiAvatar from "@mui/material/Avatar";
import { CustomAvatarProps, ThemeColor } from "./types";
import useBgColor, { UseBgColorType } from "@core/hooks/useBgColor";

type UpdatedThemeColor = ThemeColor | "yellow";

const Avatar = forwardRef((props: CustomAvatarProps, ref: Ref<any>) => {
  // ** Props
  const { sx, src, color } = props;

  const bgColors: UseBgColorType = useBgColor();

  const getAvatarStyles = (color: UpdatedThemeColor) => {
    let avatarStyles = {
      backgroundColor: "",
      color: "",
    };

    if (color === "yellow") {
      avatarStyles.backgroundColor = "#FCD400";
    } else {
      avatarStyles = { ...bgColors[`${color}Filled`] };
    }

    return avatarStyles;
  };

  const colors: UseBgColorType = {
    primary: getAvatarStyles("primary"),
    secondary: getAvatarStyles("secondary"),
    success: getAvatarStyles("success"),
    error: getAvatarStyles("error"),
    warning: getAvatarStyles("warning"),
    info: getAvatarStyles("info"),
    yellow: getAvatarStyles("yellow"),
  };

  return (
    <MuiAvatar
      ref={ref}
      {...props}
      sx={!src && color ? Object.assign(colors[color], sx) : sx}
    />
  );
});
Avatar.displayName = "Avatar";

Avatar.defaultProps = {
  skin: "filled",
  color: "primary",
};
Avatar.displayName = "Avatar";

export default Avatar;
