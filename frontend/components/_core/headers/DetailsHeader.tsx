import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import {
  Box,
  Button,
  Stack,
  SvgIcon,
  Tooltip,
  Typography,
} from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import InfoIcon from "@mui/icons-material/Info";
import CloudDownloadIcon from "@mui/icons-material/CloudDownload";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import { useRouter } from "next/router";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import ConfirmDialog from "@components/forms/dialog/ConfirmDialog";
import {
  LocationType,
  useMapEditorActions,
  useModalDataOpen,
} from "@stores/pages/mapEditor";
import { useGridContext } from "@context/grid/grid.context";
import { FetchInterventionPartnersQuery } from "../../../generated/graphql";
import {
  FieldNamesMarkedBoolean,
  FieldValues,
  UseFormReset,
} from "react-hook-form";
import { exportToPDF } from "@lib/utils";
import { useDeviceSize } from "@lib/utils";
import GridActions from "@components/grid/GridActions";
import getConfig from "next/config";
import { StumpIconCard, TreeIconCard } from "@components/icons";
import AuthCan from "@components/auth/Can";

export interface IDetailsHeaderProps {
  title?: string | undefined | null;
  locationStatus?: LocationType;
  addMessage?: string | undefined | null;
  deleteMessage?: string | undefined;
  withClosePanel?: boolean;
  withHistoryBack?: boolean;
  withEditMode?: boolean;
  isSupport?: boolean;
  withAddMode?: boolean;
  type?: string;
  toggleEditMode?: () => void;
  onSave?: () => void;
  onDelete?: (id: string | string[]) => void;
  onBackNav?: () => void;
  onCancelEditMode?: () => void;
  onDuplicate?: () => void;
  allowExport?: boolean;
  showDuplicate?: boolean;
  allowDuplicate?: boolean;
  onValidateIntervention?(
    interventionIds: string[],
    selectedLocationsIds: string[],
    interventionDate: string,
    interventionPartner: string,
    validationNote: string
  ): void;
  onDeleteObjects?(ids: string[]): void;
  interventionPartners?: FetchInterventionPartnersQuery["intervention_partner"];
  reset?: UseFormReset<FieldValues>;
  dirtyFields?: Partial<Readonly<FieldNamesMarkedBoolean<FieldValues>>>;
}

const DetailsHeader = ({
  title = "Default Title",
  locationStatus = undefined,
  addMessage = "Default add message",
  deleteMessage = "Default delete message",
  type = "",
  withHistoryBack = true,
  withClosePanel = false,
  withEditMode = false,
  withAddMode = false,
  allowExport = false,
  allowDuplicate = false,
  showDuplicate = false,
  onBackNav,
  isSupport = false,
  onSave,
  onDelete,
  onDuplicate,
  toggleEditMode,
  onCancelEditMode,
  onValidateIntervention,
  onDeleteObjects,
  interventionPartners,
  reset,
  dirtyFields,
}: IDetailsHeaderProps) => {
  const { publicRuntimeConfig } = getConfig();
  const { query, replace, back } = useRouter();
  const { t } = useTranslation(["common"]);
  const [action, setAction] = useState<string>("");
  const [message, setMessage] = useState<string>("");
  const [openDataModal, setOpenDataModal] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openDeleteModal, setOpenDeleteModal] = useState<boolean>(false);
  const { rowSelected, setRowSelected, selectedLocationIds } =
    useGridContext() ?? {};

  const modalDataOpen = useModalDataOpen();
  const {
    setModalDataOpen,
    setMapPanelOpen,
    renderSelectionLayer,
    setMapActiveAction,
    setMapSelectedLocations,
    setMapFilteredLocations,
  } = useMapEditorActions();
  const { isMobile } = useDeviceSize();

  const isReadOnly = !query.hasOwnProperty("edit");

  const setEditModeOn = () => {
    replace({ query: { ...query, edit: "active" } }, undefined, {
      shallow: true,
    });
  };
  const setEditModeOff = () => {
    const newQuery = { ...query };
    delete newQuery.edit;
    replace({ query: { ...newQuery } }, undefined, {
      shallow: true,
    });
  };
  const handleEditModeActivation = () => {
    toggleEditMode ? toggleEditMode() : setEditModeOn();
  };

  const handleSave = () => {
    if (action === "delete") {
      setOpenModal(false);
      setOpenDeleteModal(true);
    } else if (action === "save") {
      onSave && onSave();
      setEditModeOff();
      setOpenModal(false);
      setModalDataOpen(false);
    } else {
      // TODO handle default save behaviour
      setEditModeOff();
    }
  };

  const onConfirmDeletionModal = () => {
    setOpenDeleteModal(false);

    const idKey = [
      "treeId",
      "locationId",
      "worksiteId",
      "activePolygon",
      "areaId",
      "diagnosisId",
    ].find((key) => query[key]) as string | undefined;

    if (onDelete && idKey && query[idKey]) {
      const idValue = query[idKey];

      if (typeof idValue === "string") {
        onDelete(idValue);
      }
    }
  };

  const onConfirmModal = () => {
    onCancelEditMode && onCancelEditMode();
    setOpenDataModal(false);
    setModalDataOpen(false);
    setEditModeOff();
    reset && reset();
  };

  const handleBackNavigation = () => {
    onBackNav ? onBackNav() : back();
  };
  const clearRowSelection = () => {
    setRowSelected && setRowSelected({});
  };

  const handleClosePanel = () => {
    setMapActiveAction(undefined);
    renderSelectionLayer(false);
    setMapFilteredLocations(undefined);
    setMapSelectedLocations(undefined);
    setMapPanelOpen(false);
    replace(`/map`);
  };

  const checkIfData = () => {
    if (modalDataOpen === true) {
      setOpenDataModal(true);
    } else {
      onCancelEditMode && onCancelEditMode();
      setEditModeOff();
    }
  };

  useEffect(() => {
    if (dirtyFields && Object.keys(dirtyFields).length > 0) {
      setModalDataOpen(true);
    } else {
      setModalDataOpen(false);
    }
  }, [dirtyFields && Object.keys(dirtyFields).length]);

  return (
    <>
      <Stack
        direction="row"
        spacing={2}
        justifyContent="space-between"
        alignItems="center"
        sx={{ padding: "1rem 0", mb: isMobile ? 5 : "" }}
        flexWrap={isMobile ? "wrap" : "nowrap"}
      >
        {withClosePanel && (
          <Button
            variant="text"
            color="primary"
            size="medium"
            startIcon={<ArrowBackIcon />}
            onClick={handleClosePanel}
          />
        )}
        {withHistoryBack && isReadOnly && (
          <Button
            variant="text"
            color="primary"
            size="medium"
            startIcon={<ArrowBackIcon />}
            onClick={() => handleBackNavigation()}
            sx={{
              padding: "8px 22px",
            }}
          >
            {t("back")}
          </Button>
        )}
        {withHistoryBack && !isReadOnly && (
          <Button
            variant="text"
            color="primary"
            size="medium"
            onClick={checkIfData}
          >
            {t("exitEditMode")}
          </Button>
        )}
        <Typography
          variant="h5"
          gutterBottom
          sx={{
            fontSize: isMobile ? "1.2rem" : "1.5rem",
            display: "flex",
            alignItems: "center",
            gap: 1,
          }}
        >
          {locationStatus === "stump" && <SvgIcon component={StumpIconCard} />}
          {locationStatus === "alive" && <SvgIcon component={TreeIconCard} />}
          {title}
        </Typography>

        <Stack
          direction="row"
          spacing={2}
          justifyContent="flex-end"
          alignItems="center"
          sx={{ padding: "1rem 0", position: "relative" }}
        >
          {allowExport && (
            <Button
              variant="contained"
              color="inherit"
              onClick={exportToPDF}
              startIcon={<CloudDownloadIcon />}
            >
              {t("buttons.exportPdf")}
            </Button>
          )}
          {showDuplicate && (
            <AuthCan role={["admin"]}>
              <Tooltip
                title={
                  !allowDuplicate
                    ? t("components.WorksiteForm.duplicateTooltipNotAllowed")
                    : t("components.WorksiteForm.duplicateTooltip")
                }
              >
                <span>
                  <Button
                    variant="contained"
                    color="inherit"
                    disabled={!allowDuplicate}
                    onClick={onDuplicate}
                    startIcon={<ContentCopyIcon />}
                    endIcon={<InfoIcon />}
                  >
                    {t("buttons.duplicate")}
                  </Button>
                </span>
              </Tooltip>
            </AuthCan>
          )}

          {withEditMode && isReadOnly && type != "account" && (
            <Button
              variant="contained"
              color="primary"
              onClick={(e) => handleEditModeActivation()}
            >
              {t("buttons.edit")}
            </Button>
          )}

          {type === "account" && isSupport && (
            <Button
              variant="contained"
              color="primary"
              onClick={() =>
                (window.location.href = `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/auth/admin/ecoteka/console/#/ecoteka`)
              }
            >
              {t("components.Template.menuItems.account.keycloak")}
            </Button>
          )}

          {withAddMode && isReadOnly && (
            <>
              {["intervention", "diagnosis"].includes(type) &&
                rowSelected &&
                Object.keys(rowSelected).length > 0 && (
                  <GridActions
                    ids={rowSelected && Object.keys(rowSelected)}
                    interventionPartners={interventionPartners}
                    onValidateIntervention={onValidateIntervention}
                    onDeleteObjects={onDeleteObjects}
                    selectedLocationsIds={selectedLocationIds || []}
                    clearRowSelection={clearRowSelection}
                  />
                )}
              <Button
                variant="contained"
                onClick={() => replace(`/${type}?object=all&${type}Modal=open`)}
                color="primary"
                sx={{
                  padding: "8px 22px",
                  position: "relative",
                }}
              >
                {addMessage}
              </Button>
            </>
          )}
          {withEditMode && !isReadOnly && (
            <>
              {type !== "account" && (
                <Button
                  variant="text"
                  color="error"
                  onClick={() => {
                    setAction("delete");
                    setMessage(deleteMessage);
                    setOpenModal(true);
                  }}
                >
                  {t("buttons.delete")}
                </Button>
              )}

              <Button
                variant="contained"
                color="primary"
                onClick={() => {
                  setAction("save");
                  setMessage(t(`common.messages.save`));
                  setOpenModal(true);
                }}
              >
                {t("buttons.save")}
              </Button>
            </>
          )}
        </Stack>
      </Stack>{" "}
      <Box>
        <ModalComponent
          open={openModal}
          handleClose={() => setOpenModal(false)}
        >
          <ConfirmDialog
            message={message}
            onConfirm={handleSave}
            onAbort={() => setOpenModal(false)}
          />
        </ModalComponent>
        <ModalComponent
          open={openDeleteModal}
          handleClose={() => setOpenDeleteModal(false)}
        >
          <ConfirmDialog
            message={t("common.messages.lostData")}
            onConfirm={onConfirmDeletionModal}
            onAbort={() => setOpenDeleteModal(false)}
            showConfirmButton={true}
          />
        </ModalComponent>
      </Box>
      {modalDataOpen && (
        <Box>
          <ModalComponent
            open={openDataModal}
            handleClose={() => setOpenDataModal(false)}
          >
            <ConfirmDialog
              title={t("common.quitForm")}
              message={t("common.messages.lostData")}
              onConfirm={onConfirmModal}
              onAbort={() => setOpenDataModal(false)}
            />
          </ModalComponent>
        </Box>
      )}
    </>
  );
};

export default DetailsHeader;
