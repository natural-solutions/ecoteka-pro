import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import DetailsHeader from "./DetailsHeader";

const meta = {
  title: "Core/Headers/DetailsHeader",
  component: DetailsHeader,
  parameters: {
    docs: {
      description: {
        component: "DetailsHeader",
      },
    },
  },
} satisfies Meta<typeof DetailsHeader>;

export default meta;
type Story = StoryObj<typeof DetailsHeader>;

export const Default: Story = {
  render: () => <DetailsHeader />,
};
