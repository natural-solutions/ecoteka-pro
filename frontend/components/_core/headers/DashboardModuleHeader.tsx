import React from "react";
import { IconButton, Stack, Tooltip, Typography } from "@mui/material";
import InfoIcon from "@mui/icons-material/Info";

export interface IDashboardModuleHeader {
  title?: string | undefined | null;
  type?: "block" | "widget";
  showTooltip?: boolean;
  tooltipText?: string | undefined | null;
}

const DashboardModuleHeader = ({
  title = "Default Title",
  tooltipText = "Default tooltip text",
  showTooltip = false,
  type = "block",
}: IDashboardModuleHeader) => {
  return (
    <>
      <Stack
        direction="row"
        spacing={2}
        justifyContent="space-between"
        alignItems="center"
        sx={{ padding: "1rem 0" }}
        data-cy="dashboard-module-header"
      >
        <Typography
          variant={type === "block" ? "h5" : "h6"}
          gutterBottom
          sx={{
            fontSize: `${type === "block" ? "1.25rem" : "0.85rem"}`,
            fontWeight: 600,
          }}
          data-cy="dashboard-module-header-title"
        >
          {title}
        </Typography>
        {showTooltip && (
          <Tooltip
            title={tooltipText}
            data-cy="dashboard-module-header-tooltip"
          >
            <IconButton>
              <InfoIcon fontSize="medium" />
            </IconButton>
          </Tooltip>
        )}
      </Stack>
    </>
  );
};

export default DashboardModuleHeader;
