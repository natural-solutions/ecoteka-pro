import { Button, Stack, Typography } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { WIPIcon } from "@core/icons";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import { DetailsHeader } from "../headers";
import Image from "next/image";
import wipImg from "./wip.svg";

export interface IWorkInProgressProps {
  type: "page" | "component";
  height?: number;
}
const WorkInProgress = ({ type, height = 200 }: IWorkInProgressProps) => {
  const { back } = useRouter();
  const { t } = useTranslation(["common"]);
  const prefix = (type: "page" | "component"): string => {
    // TODO : use react-i18n to do this
    if (type === "page") {
      return "Cette page";
    }
    if (type === "component") {
      return "Ce composant";
    } else {
      return "Cet élement";
    }
  };

  return (
    <Stack
      direction="column"
      alignItems="center"
      justifyContent="center"
      spacing={4}
      style={{
        height: `${type === "page" ? "100vh" : "auto"}`,
        width: `${type === "page" ? "100vw" : "100%"}`,
      }}
    >
      {type === "page" && (
        <Button
          variant="contained"
          color="primary"
          size="large"
          startIcon={<ArrowBackIcon />}
          onClick={() => back()}
        >
          {t("back")}
        </Button>
      )}

      <Typography variant={`${type === "page" ? "h2" : "h6"}`}>
        {prefix(type)} est en cours de plantation
      </Typography>
      {type === "page" && <WIPIcon />}
      {type === "component" && (
        <Image priority src={wipImg} alt="WIP Image" height={height} />
      )}
    </Stack>
  );
};
export default WorkInProgress;
