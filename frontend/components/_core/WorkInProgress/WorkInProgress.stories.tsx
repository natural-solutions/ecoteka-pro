import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import WorkInProgress from "./WorkInProgress";

const meta = {
  title: "Core/WorkInProgress",
  component: WorkInProgress,
  parameters: {
    docs: {
      description: {
        component: "WorkInProgress",
      },
    },
  },
} satisfies Meta<typeof WorkInProgress>;

export default meta;
type Story = StoryObj<typeof WorkInProgress>;

export const Page: Story = {
  render: () => <WorkInProgress type="page" />,
};

export const Component: Story = {
  render: () => <WorkInProgress type="component" />,
};
