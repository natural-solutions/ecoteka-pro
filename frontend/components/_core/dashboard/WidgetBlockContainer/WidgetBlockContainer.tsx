import WorkInProgress from "@components/_core/WorkInProgress/WorkInProgress";
import DashboardModuleHeader from "@core/headers/DashboardModuleHeader";
import { Card } from "@mui/material";
import { PropsWithChildren } from "react";

export interface IWidgetBlockContainerProps {
  title?: string;
  tooltipText?: string;
}
const WidgetBlockContainer = ({
  title = "Default title",
  tooltipText,
  children,
}: PropsWithChildren<IWidgetBlockContainerProps>) => {
  return (
    <>
      <DashboardModuleHeader
        title={title}
        showTooltip={Boolean(tooltipText)}
        tooltipText={tooltipText}
      />
      <Card sx={{ padding: "1rem" }}>
        {children && children}{" "}
        {!children && <WorkInProgress type="component" />}
      </Card>
    </>
  );
};
export default WidgetBlockContainer;
