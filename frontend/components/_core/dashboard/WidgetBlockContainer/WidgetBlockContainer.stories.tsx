import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import WidgetBlockContainer from "./WidgetBlockContainer";

const meta = {
  title: "Core/Dashboard/WidgetBlockContainer",
  component: WidgetBlockContainer,
  parameters: {
    docs: {
      description: {
        component: "WidgetBlockContainer",
      },
    },
  },
} satisfies Meta<typeof WidgetBlockContainer>;
export default meta;

type Story = StoryObj<typeof WidgetBlockContainer>;

export const Default: Story = {
  render: () => <WidgetBlockContainer />,
};

export const WithTitle: Story = {
  render: () => <WidgetBlockContainer title="Mon Titre" />,
};

export const WithTitleAndTooltip: Story = {
  render: () => (
    <WidgetBlockContainer title="Mon Titre" tooltipText="Mon tooltip" />
  ),
};
