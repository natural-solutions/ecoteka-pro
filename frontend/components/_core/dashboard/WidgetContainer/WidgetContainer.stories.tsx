import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import WidgetContainer from "./WidgetContainer";

const meta = {
  title: "Core/Dashboard/WidgetContainer",
  component: WidgetContainer,
  parameters: {
    docs: {
      description: {
        component: "WidgetContainer",
      },
    },
  },
} satisfies Meta<typeof WidgetContainer>;
export default meta;

type Story = StoryObj<typeof WidgetContainer>;

export const Default: Story = {
  render: () => <WidgetContainer />,
};

export const WithTitle: Story = {
  render: () => <WidgetContainer title="Mon Titre" />,
};

export const WithTitleAndTooltip: Story = {
  render: () => <WidgetContainer title="Mon Titre" tooltipText="Mon tooltip" />,
};
