import WorkInProgress from "@components/_core/WorkInProgress/WorkInProgress";
import DashboardModuleHeader from "@core/headers/DashboardModuleHeader";
import { Box } from "@mui/material";
import { PropsWithChildren } from "react";

export interface IWidgetContainerProps {
  title?: string;
  tooltipText?: string;
}
const WidgetContainer = ({
  title = "Default title",
  tooltipText,
  children,
}: PropsWithChildren<IWidgetContainerProps>) => {
  return (
    <Box>
      <DashboardModuleHeader
        title={title}
        showTooltip={Boolean(tooltipText)}
        tooltipText={tooltipText}
        type="widget"
      />
      {children && children} {!children && <WorkInProgress type="component" />}
    </Box>
  );
};
export default WidgetContainer;
