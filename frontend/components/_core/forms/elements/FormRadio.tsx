import {
  FormControl,
  FormControlLabel,
  FormLabel,
  FormLabelProps,
  Radio,
  RadioGroup,
  RadioProps,
} from "@mui/material";
import { Control, Controller } from "react-hook-form";
import { Options } from "./types";

const defaultOptions = [
  {
    label: "Radio Option 1",
    value: "1",
  },
  {
    label: "Radio Option 2",
    value: "2",
  },
];

export interface IFormInputProps {
  name: string;
  control: Control;
  label: string;
  options: Options;
  formLabelProps?: FormLabelProps;
  radioProps?: RadioProps;
}

export const FormRadio: React.FC<IFormInputProps> = ({
  name,
  control,
  label,
  options = defaultOptions,
  formLabelProps,
  radioProps,
}) => {
  const generateRadioOptions = () => {
    return options.map((option) => (
      <FormControlLabel
        value={option.value}
        label={option.label}
        control={<Radio {...radioProps} />}
      />
    ));
  };
  return (
    <FormControl component="fieldset">
      <FormLabel {...formLabelProps}>{label}</FormLabel>
      <Controller
        name={name}
        control={control}
        render={({
          field: { onChange, value },
          fieldState: { error },
          formState,
        }) => (
          <RadioGroup value={value} onChange={onChange}>
            {generateRadioOptions()}
          </RadioGroup>
        )}
      />
    </FormControl>
  );
};

export default FormRadio;
