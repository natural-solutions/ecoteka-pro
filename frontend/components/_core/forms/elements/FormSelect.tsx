import React from "react";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectProps,
} from "@mui/material";
import { Controller, Control } from "react-hook-form";
import { Options, IFormInput } from "./types";

const defaultOptions = [
  {
    label: "--",
    value: undefined,
  },
  {
    label: "Dropdown Option 1",
    value: "1",
  },
  {
    label: "Dropdown Option 2",
    value: "2",
  },
];

export interface IFormSelect extends IFormInput {
  options: Options;
}

type IFormSelectProps = SelectProps &
  IFormSelect & {
    control: Control<any>;
    defaultValue?: any;
  };

export const FormInputDropdown: React.FC<IFormSelectProps> = ({
  name,
  control,
  label,
  options = defaultOptions,
  defaultValue = "",
  required = false,
  ...selectProps
}) => {
  const generateSingleOptions = () => {
    return options.map((option) => (
      <MenuItem key={option.value} value={option.value}>
        {option.label}
      </MenuItem>
    ));
  };

  return (
    <FormControl fullWidth>
      <InputLabel required={required}>{label}</InputLabel>
      <Controller
        render={({ field: { onChange, value } }) => (
          <Select
            {...selectProps}
            onChange={onChange}
            value={value}
            variant="filled"
            required={required}
          >
            {generateSingleOptions()}
          </Select>
        )}
        control={control}
        name={name}
        defaultValue={defaultValue}
      />
    </FormControl>
  );
};
