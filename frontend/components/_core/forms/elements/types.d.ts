import type { Control } from "react-hook-form";

export interface IFormInput {
  name: string;
  control: Control;
  label: string;
}

export interface IOption {
  label: string;
  value: string | number | Record<string, string | number | undefined>;
}

export type Options = IOption[];
