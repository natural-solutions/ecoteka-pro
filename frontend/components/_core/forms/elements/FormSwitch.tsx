import React from "react";
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  FormLabelProps,
  Switch,
  SwitchProps,
} from "@mui/material";
import { Control, Controller } from "react-hook-form";

export interface IFormSwitchProps {
  name: string;
  control: Control;
  label: string;
  formLabelProps?: FormLabelProps;
  switchProps?: SwitchProps;
  defaultChecked: boolean;
  isReadOnly?: boolean;
}

export const FormSwitch: React.FC<IFormSwitchProps> = ({
  name,
  control,
  label,
  switchProps,
  defaultChecked,
  isReadOnly = false,
}) => {
  return (
    <FormControl component="fieldset">
      <Controller
        name={name}
        control={control}
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <FormControlLabel
            control={
              <Switch
                checked={!!value}
                onChange={onChange}
                {...switchProps}
                defaultChecked={defaultChecked}
                disabled={isReadOnly}
              />
            }
            label={label}
          />
        )}
      />
    </FormControl>
  );
};

export default FormSwitch;
