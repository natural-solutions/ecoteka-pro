import { Control, Controller, UseFormSetValue } from "react-hook-form";
import {
  Grid,
  TextField,
  Switch,
  FormControlLabel,
  Autocomplete,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  Checkbox,
  useTheme,
  useMediaQuery,
  InputAdornment,
  AutocompleteRenderInputParams,
} from "@mui/material";

import { TaxonScientificSearch } from "@components/forms/asyncAutocomplete/TaxonScientificInput";
import { TaxonVernacularSearch } from "@components/forms/asyncAutocomplete/TaxonVernacularInput";

type ComputeRange<
  N extends number,
  Result extends Array<unknown> = []
> = Result["length"] extends N
  ? Result
  : ComputeRange<N, [...Result, Result["length"]]>;

type Add<A extends number, B extends number> = [
  ...ComputeRange<A>,
  ...ComputeRange<B>
]["length"];

type IsGreater<A extends number, B extends number> = IsLiteralNumber<
  [...ComputeRange<B>][Last<[...ComputeRange<A>]>]
> extends true
  ? false
  : true;

type Last<T extends any[]> = T extends [...infer _, infer Last]
  ? Last extends number
    ? Last
    : never
  : never;

type RemoveLast<T extends any[]> = T extends [...infer Rest, infer _]
  ? Rest
  : never;

type IsLiteralNumber<N> = N extends number
  ? number extends N
    ? false
    : true
  : false;

type AddIteration<
  Min extends number,
  Max extends number,
  ScaleBy extends number,
  Result extends Array<unknown> = [Min]
> = IsGreater<Last<Result>, Max> extends true
  ? RemoveLast<Result>
  : AddIteration<Min, Max, ScaleBy, [...Result, Add<Last<Result>, ScaleBy>]>;

export type FieldType =
  | "image"
  | "number"
  | "text"
  | "textarea"
  | "checkbox"
  | "select"
  | "autocomplete"
  | "date";

export interface FieldConfig {
  /** the input type for your field */
  type: FieldType;
  size: AddIteration<1, 12, 1>;
}
