import { Box, Stack, Typography } from "@mui/material";
import LinearProgress, {
  LinearProgressProps,
} from "@mui/material/LinearProgress";

export interface ICountProps {
  value: number;
  unit?: string;
}

const Progress = ({
  unit = "%",
  ...props
}: LinearProgressProps & ICountProps) => {
  return (
    <Stack
      spacing={2}
      direction="row"
      alignItems="center"
      sx={{ width: "100%" }}
    >
      <Box flexGrow={1}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
      <Typography variant="body2" color="text.secondary">
        {props.value} {unit}
      </Typography>
    </Stack>
  );
};

export default Progress;
