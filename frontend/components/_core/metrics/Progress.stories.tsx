import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import Progress from "./Progress";

const meta = {
  title: "Core/Metrics/Progress",
  component: Progress,
  parameters: {
    docs: {
      description: {
        component: "Progress",
      },
    },
  },
} satisfies Meta<typeof Progress>;

export default meta;
type Story = StoryObj<typeof Progress>;

export const DefaultComplete: Story = {
  render: () => <Progress value={100} />,
};

export const DefaultNone: Story = {
  render: () => <Progress value={0} />,
};

export const AchievmentProgress: Story = {
  render: () => <Progress value={30} unit="% réalisé" />,
};
