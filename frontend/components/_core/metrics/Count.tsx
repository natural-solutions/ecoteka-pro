import { Typography } from "@mui/material";

export interface ICountProps {
  name: string;
  data: Object[];
}

const Count = ({ name, data }) => {
  return (
    <Typography
      variant="body1"
      color="text.secondary"
      sx={{
        padding: 0,
        margin: 0,
      }}
    >
      {data.length} {name}
    </Typography>
  );
};

export default Count;
