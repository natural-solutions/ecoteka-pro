import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import Count from "./Count";

const meta = {
  title: "Core/Metrics/Count",
  component: Count,
  parameters: {
    docs: {
      description: {
        component: "Count",
      },
    },
  },
} satisfies Meta<typeof Count>;

export default meta;
type Story = StoryObj<typeof Count>;

const mockedData = [
  {
    color: "red",
    value: "#f00",
  },
  {
    color: "green",
    value: "#0f0",
  },
  {
    color: "blue",
    value: "#00f",
  },
  {
    color: "cyan",
    value: "#0ff",
  },
  {
    color: "magenta",
    value: "#f0f",
  },
  {
    color: "yellow",
    value: "#ff0",
  },
  {
    color: "black",
    value: "#000",
  },
];

export const Default: Story = {
  render: () => <Count name="couleurs" data={mockedData} />,
};
