import { Typography } from "@mui/material";

export interface ICardTitleProps {
  title: string;
}
const CardTitle = ({ title }: ICardTitleProps) => {
  return (
    // JSX here
    <Typography
      variant="h6"
      sx={{
        padding: "0px 0px 16px 0px",
      }}
    >
      {title}
    </Typography>
  );
};
export default CardTitle;
