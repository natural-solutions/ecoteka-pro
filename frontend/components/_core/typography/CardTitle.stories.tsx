import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import CardTitle from "./CardTitle";

const meta = {
  title: "Core/Typography/CardTitle",
  component: CardTitle,
  parameters: {
    docs: {
      description: {
        component: "CardTitle",
      },
    },
  },
} satisfies Meta<typeof CardTitle>;
export default meta;
type Story = StoryObj<typeof CardTitle>;
export const Default: Story = {
  render: () => <CardTitle title="Default card title" />,
};
