const BannedIcon = () => {
  return (
    <svg width="49" height="49" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect y="0.5" width="49" height="48" rx="24" fill="#FFEBEF"/>
      <g clipPath="url(#clip0_987_2673)">
      <path d="M13.5 33.5H35.5L24.5 14.5L13.5 33.5ZM25.5 30.5H23.5V28.5H25.5V30.5ZM25.5 26.5H23.5V22.5H25.5V26.5Z" fill="#FF4C51"/>
      </g>
      <defs>
        <clipPath id="clip0_987_2673">
        <rect width="24" height="24" fill="white" transform="translate(12.5 12.5)"/>
        </clipPath>
      </defs>
    </svg>
  );
};

export default BannedIcon;
