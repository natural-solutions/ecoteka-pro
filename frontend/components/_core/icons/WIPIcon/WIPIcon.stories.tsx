import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import WIPIcon from "./WIPIcon";

const meta = {
  title: "Core/Icons/WIPIcon",
  component: WIPIcon,
  parameters: {
    docs: {
      description: {
        component: "WIPIcon",
      },
    },
  },
} satisfies Meta<typeof WIPIcon>;
export default meta;
type Story = StoryObj<typeof WIPIcon>;
export const Default: Story = {
  render: () => <WIPIcon />,
};
