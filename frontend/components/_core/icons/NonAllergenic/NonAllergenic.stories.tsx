import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import { NonAllergenic } from "@core/icons";

const meta = {
  title: "Core/Icons/NonAllergenic",
  component: NonAllergenic,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Layer manager",
      },
    },
  },
} satisfies Meta<typeof NonAllergenic>;

export default meta;
type Story = StoryObj<typeof NonAllergenic>;

export const NonAllergenicDefaults: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <NonAllergenic />
    </Box>
  ),
};

export const NonAllergenicWithControls: Story = {
  args: {
    level: 80,
  },
  render: (args) => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <NonAllergenic {...args} />
    </Box>
  ),
};
