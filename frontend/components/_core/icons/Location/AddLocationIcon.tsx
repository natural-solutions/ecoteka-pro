const AddLocationIcon = () => {
  return (
    <svg
      width="14"
      height="20"
      viewBox="0 0 14 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M7 -1.51143e-08C3.14 -6.77982e-09 -6.46069e-09 3.14 -1.44028e-08 7C-2.52049e-08 12.25 7 20 7 20C7 20 14 12.25 14 7C14 3.14 10.86 -2.34487e-08 7 -1.51143e-08ZM11 8L8 8L8 11L6 11L6 8L3 8L3 6L6 6L6 3L8 3L8 6L11 6L11 8Z"
        fill="#7E8989"
      />
    </svg>
  );
};

export default AddLocationIcon;
