import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import { SoilWater } from "@core/icons";

const meta = {
  title: "Core/Icons/SoilWater",
  component: SoilWater,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Layer manager",
      },
    },
  },
} satisfies Meta<typeof SoilWater>;

export default meta;
type Story = StoryObj<typeof SoilWater>;

export const SoilWaterDefaults: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <SoilWater />
    </Box>
  ),
};

export const SoilWaterWithControls: Story = {
  args: {
    level: 80,
  },
  render: (args) => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <SoilWater {...args} />
    </Box>
  ),
};
