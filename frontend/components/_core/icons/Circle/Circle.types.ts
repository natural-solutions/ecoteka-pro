import { CSSProperties } from "react";

export interface CircleProps {
  size?: number;
  stroke?: CSSProperties["color"];
  strokeWidth?: number;
  fill?: CSSProperties["color"];
}
