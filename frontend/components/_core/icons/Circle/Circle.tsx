import { CircleProps } from "./Circle.types";

const Circle = ({
  size = 24,
  fill = "black",
  stroke = "teal",
  strokeWidth = 2,
}: CircleProps) => {
  return (
    <svg
      height={size}
      width={size}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle
        cx={size / 2}
        cy={size / 2}
        r={size / 2 - strokeWidth * 2}
        stroke={stroke}
        strokeWidth={strokeWidth}
        fill={fill}
      />
    </svg>
  );
};

export default Circle;
