import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import { Circle } from "@core/icons";

const meta = {
  title: "Core/Icons/Circle",
  component: Circle,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Layer manager",
      },
    },
  },
} satisfies Meta<typeof Circle>;

export default meta;
type Story = StoryObj<typeof Circle>;

export const CircleDefaults: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <Circle />
    </Box>
  ),
};

export const CircleWithControls: Story = {
  args: {
    size: 24,
    fill: "teal",
    stroke: "none",
    strokeWidth: 2,
  },
  render: (args) => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <Circle {...args} />
    </Box>
  ),
};
