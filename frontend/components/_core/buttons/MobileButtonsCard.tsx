import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import {
  Backdrop,
  Box,
  Button,
  Fab,
  SpeedDial,
  SpeedDialAction,
  Stack,
} from "@mui/material";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import DeleteIcon from "@mui/icons-material/Delete";
import CloseIcon from "@mui/icons-material/Close";
import { useRouter } from "next/router";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import ConfirmDialog from "@components/forms/dialog/ConfirmDialog";
import { useMapEditorActions, useModalDataOpen } from "@stores/pages/mapEditor";
import {
  FieldNamesMarkedBoolean,
  FieldValues,
  UseFormReset,
} from "react-hook-form";

export interface IDetailsHeaderProps {
  withEditMode?: boolean;
  toggleEditMode?: () => void;
  onSave?: () => void;
  onDelete?: (id: string | string[]) => void;
  onCancelEditMode?: () => void;
  reset?: UseFormReset<FieldValues>;
  dirtyFields?: Partial<Readonly<FieldNamesMarkedBoolean<FieldValues>>>;
  deleteMessage?: string | undefined;
}

const MobileButtonsCard = ({
  withEditMode = false,
  onSave,
  onDelete,
  toggleEditMode,
  onCancelEditMode,
  reset,
  dirtyFields,
  deleteMessage = "Default delete message",
}: IDetailsHeaderProps) => {
  const { query, replace } = useRouter();
  const { t } = useTranslation(["common"]);
  const [action, setAction] = useState<string>("");
  const [message, setMessage] = useState<string>("");
  const [openDataModal, setOpenDataModal] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openDeleteModal, setOpenDeleteModal] = useState<boolean>(false);
  const [open, setOpen] = useState(false);
  const modalDataOpen = useModalDataOpen();
  const { setModalDataOpen } = useMapEditorActions();

  const isReadOnly = !query.hasOwnProperty("edit");

  const setEditModeOn = () => {
    replace({ query: { ...query, edit: "active" } }, undefined, {
      shallow: true,
    });
  };
  const setEditModeOff = () => {
    const newQuery = { ...query };
    delete newQuery.edit;
    replace({ query: { ...newQuery } }, undefined, {
      shallow: true,
    });
  };
  const handleEditModeActivation = () => {
    toggleEditMode ? toggleEditMode() : setEditModeOn();
  };

  const handleSave = () => {
    if (action === "delete") {
      setOpenModal(false);
      setOpenDeleteModal(true);
    } else if (action === "save") {
      onSave && onSave();
      setEditModeOff();
      setOpenModal(false);
      setModalDataOpen(false);
    } else {
      // TODO handle default save behaviour
      setEditModeOff();
    }
  };

  const onConfirmDeletionModal = () => {
    setOpenDeleteModal(false);
    const idKey = ["treeId", "locationId", "worksiteId"].find(
      (key) => query[key]
    ) as string | undefined;

    if (onDelete && idKey && query[idKey]) {
      const idValue = query[idKey];

      if (typeof idValue === "string") {
        onDelete(idValue);
      }
    }
  };

  const onConfirmModal = () => {
    onCancelEditMode && onCancelEditMode();
    setOpenDataModal(false);
    setModalDataOpen(false);
    setEditModeOff();
    reset && reset();
  };

  const checkIfData = () => {
    if (modalDataOpen === true) {
      setOpenDataModal(true);
    } else {
      onCancelEditMode && onCancelEditMode();
      setEditModeOff();
    }
  };

  const handleOpen = (event: any) => {
    setOpen(!open);
  };

  useEffect(() => {
    if (dirtyFields && Object.keys(dirtyFields).length > 0) {
      setModalDataOpen(true);
    } else {
      setModalDataOpen(false);
    }
  }, [dirtyFields && Object.keys(dirtyFields).length]);
  return (
    <>
      <Stack direction="row" sx={styles.mobileButtonsStack}>
        {withEditMode && isReadOnly && (
          <Button
            variant="contained"
            color="primary"
            onClick={(e) => handleEditModeActivation()}
            sx={{ width: "90%" }}
          >
            {t("buttons.edit")}
          </Button>
        )}

        {withEditMode && !isReadOnly && (
          <>
            <Box sx={{ width: "80%" }}>
              <Button
                variant="contained"
                color="primary"
                sx={{ width: "100%" }}
                onClick={() => {
                  setAction("save");
                  setMessage(t(`common.messages.save`));
                  setOpenModal(true);
                }}
              >
                {t("buttons.save")}
              </Button>
            </Box>

            <SpeedDial
              open={open}
              onClick={handleOpen}
              ariaLabel="SpeedDial"
              icon={
                <Fab color="primary" size="small" aria-label="filter">
                  <MoreVertIcon />
                </Fab>
              }
            >
              <Backdrop open={true} />

              <SpeedDialAction
                tooltipTitle={t("exitEditMode")}
                tooltipOpen
                icon={<CloseIcon color="primary" />}
                onClick={checkIfData}
              />
              <SpeedDialAction
                tooltipTitle={t("buttons.delete")}
                tooltipOpen
                icon={<DeleteIcon color="error" />}
                onClick={() => {
                  setAction("delete");
                  setMessage(deleteMessage);
                  setOpenModal(true);
                }}
              />
            </SpeedDial>
          </>
        )}
      </Stack>

      <Box>
        <ModalComponent
          open={openModal}
          handleClose={() => setOpenModal(false)}
        >
          <ConfirmDialog
            message={message}
            onConfirm={handleSave}
            onAbort={() => setOpenModal(false)}
          />
        </ModalComponent>
        <ModalComponent
          open={openDeleteModal}
          handleClose={() => setOpenDeleteModal(false)}
        >
          <ConfirmDialog
            message={t("common.messages.lostData")}
            onConfirm={onConfirmDeletionModal}
            onAbort={() => setOpenDeleteModal(false)}
            showConfirmButton={true}
          />
        </ModalComponent>
      </Box>
      {modalDataOpen && (
        <Box>
          <ModalComponent
            open={openDataModal}
            handleClose={() => setOpenDataModal(false)}
          >
            <ConfirmDialog
              title={t("common.quitForm")}
              message={t("common.messages.lostData")}
              onConfirm={onConfirmModal}
              onAbort={() => setOpenDataModal(false)}
            />
          </ModalComponent>
        </Box>
      )}
    </>
  );
};

export default MobileButtonsCard;

const styles = {
  mobileButtonsStack: {
    position: "fixed",
    bottom: "0",
    width: "100%",
    maxHeight: "35px",
    mb: 0.5,
    zIndex: 50,
  },
};
