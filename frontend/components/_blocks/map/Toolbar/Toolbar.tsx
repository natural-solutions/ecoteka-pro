import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { IconButton, Menu, Stack, Tooltip } from "@mui/material";
import TreeToolbarIcon from "@components/_core/icons/Tree/TreeToolbarIcon";
import { AddLocationIcon } from "@components/_core/icons/Location";
import AddLocationButton from "@components/map/buttons/AddLocationButton";
import {
  LocationType,
  MapBackground,
  useMapEditorActions,
} from "@stores/pages/mapEditor";
import { useRouter } from "next/router";
import BaseMapSelector from "@components/map/BaseMapSelector";
import { AddPolygonIcon } from "@components/_core/icons/Polygon";
import { usePolygonFormActions } from "@stores/forms/polygonForm";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import AuthCan from "@components/auth/Can";

interface IToolbarProps {
  handleOnAddLocation(type: LocationType): void;
  map?: MapBackground;
  handleOnBaseMapChange?(map: MapBackground): void;
}

const Toolbar: FC<IToolbarProps> = ({
  handleOnAddLocation,
  map,
  handleOnBaseMapChange,
}) => {
  const { t } = useTranslation(["components"]);
  const [anchorEl, setAnchorEl] = useState(null);
  const { filtersWithMartin, setFiltersWithMartin } =
    useFiltersWithMartinContext()!;
  const { setMapActiveAction, setMapPanelOpen } = useMapEditorActions();
  const {
    setPolygonCommonData,
    setPolygonObjectType,
    setBoundaryFormData,
    setPolygonFeatures,
    setVegetatedAreaFormData,
  } = usePolygonFormActions();
  const router = useRouter();
  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOnAddWorksiteGroup = () => {
    setMapActiveAction("addWorksiteGroup");
    setFiltersWithMartin({
      ...filtersWithMartin,
      vegetated_areas_active: true,
    });
    setMapPanelOpen(true);
  };

  const handleOnAddPolygon = () => {
    setMapActiveAction("addPolygon");
    router.replace("/map");
    setPolygonCommonData({
      geometry: "",
      locationsInside: [],
      vegetatedAreasInside: [],
      area: null,
    });
    setPolygonObjectType(undefined);
    setBoundaryFormData({
      name: "",
    });
    setVegetatedAreaFormData(undefined);
    setPolygonFeatures({
      type: "FeatureCollection",
      features: [{ geometry: { coordinates: [] } }],
    });
    setMapPanelOpen(true);
  };

  const menuItems = [
    {
      status: "alive",
    },
    {
      status: "empty",
    },
    {
      status: "stump",
    },
  ];
  const tools = [
    {
      icon: (
        <BaseMapSelector
          map={map}
          handleOnBaseMapChange={handleOnBaseMapChange}
        />
      ),
      tooltip: t("components.Map.Toolbar.layer"),
    },
    {
      icon: <AddLocationIcon />,
      tooltip: t("components.Map.Toolbar.add"),
      onClick: handleClick,
    },
    {
      icon: <AddPolygonIcon />,
      tooltip: t("components.Map.Toolbar.polygon"),
      onClick: handleOnAddPolygon,
    },
    {
      icon: <TreeToolbarIcon />,
      tooltip: t("components.Map.Toolbar.worksite"),
      onClick: handleOnAddWorksiteGroup,
      adminOnly: true,
    },
  ];

  return (
    <Stack
      direction={"row"}
      sx={styles.stack}
      gap={2}
      data-cy="toolbar-container"
    >
      {tools.map((tool, index) =>
        tool.adminOnly ? (
          <AuthCan key={index} role={["admin"]}>
            <Tooltip title={tool.tooltip} data-cy={`toolbar-item-${index}`}>
              <IconButton onClick={tool.onClick}>{tool.icon}</IconButton>
            </Tooltip>
          </AuthCan>
        ) : (
          <Tooltip
            key={index}
            title={tool.tooltip}
            data-cy={`toolbar-item-${index}`}
          >
            <IconButton
              onClick={(e) => (tool.onClick && tool.onClick(e)) || undefined}
            >
              {tool.icon}
            </IconButton>
          </Tooltip>
        )
      )}
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        data-cy="toolbar-menu-add-locations"
      >
        {menuItems.map((item) => (
          <AddLocationButton
            key={item.status}
            handleOnAddLocation={handleOnAddLocation}
            locationStatus={item.status as LocationType}
          />
        ))}
      </Menu>
    </Stack>
  );
};

export default Toolbar;

const styles = {
  stack: {
    backgroundColor: "#fff",
    borderTopLeftRadius: "6px",
    borderTopRightRadius: "6px",
    padding: 1,
  },
};
