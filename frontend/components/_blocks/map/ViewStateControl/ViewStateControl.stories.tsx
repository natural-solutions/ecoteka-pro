import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import ViewStateControl from "@blocks/map/ViewStateControl";

const meta = {
  title: "Blocks/Map/ViewStateControl",
  component: ViewStateControl,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Map zoom controls",
      },
    },
  },
} satisfies Meta<typeof ViewStateControl>;

export default meta;
type Story = StoryObj<typeof ViewStateControl>;

export const ZoomControlWithDefaults: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <ViewStateControl
        mode="zoom"
        value={2}
        setValueCallback={(val) => alert(`Zoom value is set to ${val}`)}
      />
    </Box>
  ),
};

export const ZoomControlWithCustomValues: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <ViewStateControl
        mode="zoom"
        value={2}
        maxValue={4}
        minValue={1}
        decrementValue={0.5}
        incrementValue={1}
        setValueCallback={(val) => alert(`Zoom value is set to ${val}`)}
      />
    </Box>
  ),
};

export const BearingControlWithDefaults: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <ViewStateControl
        mode="bearing"
        value={2}
        setValueCallback={(val) => alert(`Bearing value is set to ${val}`)}
      />
    </Box>
  ),
};

export const BearingControlWithCustomValues: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <ViewStateControl
        mode="bearing"
        value={0}
        maxValue={10}
        minValue={0}
        decrementValue={2}
        incrementValue={2}
        setValueCallback={(val) => alert(`Bearing value is set to ${val}`)}
      />
    </Box>
  ),
};
