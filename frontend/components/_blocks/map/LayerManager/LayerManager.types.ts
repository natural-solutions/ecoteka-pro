export interface ILegendItem {
  label: string;
  symbol: JSX.Element;
}
export interface ILayerConfig {
  id: string;
  label: string;
  legendItems: ILegendItem[];
}
export interface LayerManagerProps {
  isOpen?: boolean;
  showControls?: boolean;
  enableControls?: boolean;
  layers?: ILayerConfig[];
}
