import LayersIcon from "@mui/icons-material/Layers";
import Avatar from "@mui/material/Avatar";
import Fab from "@mui/material/Fab";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { BaseMapSelectorProps, MapBackground } from "./Basemap.types";
import BasemapPopover from "./BasemapPopover";
import { defaultMapBackgrounds } from "./defaults";
import { useDeviceSize } from "@lib/utils";

const BasemapSelector: FC<BaseMapSelectorProps> = ({
  handleOnBaseMapChange,
  map = MapBackground.Map,
  basemapOptions = defaultMapBackgrounds,
}) => {
  const { t } = useTranslation("components");
  const { isTablet } = useDeviceSize();
  const [visible, setVisible] = useState(false);

  return (
    <Stack
      sx={styles.container}
      direction={isTablet ? "column" : "row"}
      alignItems={"end"}
      onMouseEnter={() => !isTablet && setVisible(true)}
      onMouseLeave={() => !isTablet && setVisible(false)}
    >
      {isTablet ? (
        <Fab
          sx={{ order: 3 }}
          color="primary"
          size="medium"
          aria-label="filter"
          onClick={() => {
            setVisible(!visible);
          }}
        >
          <LayersIcon />
        </Fab>
      ) : (
        <Paper
          onClick={() => setVisible(!visible)}
          className="baseMapContainer"
          sx={styles.baseMapContainer}
        >
          <Typography
            sx={styles.caption}
            variant="caption"
            color={map === MapBackground.Satellite ? "white" : "black"}
          >
            {t(`components.Map.SatelliteToggle.${map}`) as string}
          </Typography>
          <Avatar
            sx={styles.baseMap}
            variant="rounded"
            alt={map}
            src={`/components/map/${map}.PNG`}
          />
        </Paper>
      )}
      <BasemapPopover
        visible={visible}
        setVisible={setVisible}
        handleOnBaseMapChange={handleOnBaseMapChange}
        mapBackground={map}
        isTablet={isTablet}
      />
    </Stack>
  );
};
export default BasemapSelector;

const styles = {
  container: {
    ":hover .baseMapContainer": { border: "2px solid  #47B9B2" },
  },
  baseMap: {
    width: 86,
    height: 86,
  },
  baseMapContainer: {
    border: "2px solid rgba(255,255,255,0.5)",
    position: "relative",
    order: 1,
    cursor: "pointer",
    display: "flex",
  },
  caption: {
    textTransform: "Uppercase",
    position: "absolute",
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 10,
    textAlign: "center",
  },
};
