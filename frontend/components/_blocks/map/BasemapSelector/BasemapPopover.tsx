import {
  Avatar,
  FormControl,
  FormControlLabel,
  Paper,
  Radio,
  RadioGroup,
  Stack,
  Typography,
} from "@mui/material";
import { FC } from "react";
import { useTranslation } from "react-i18next";
import { BaseMapPopoverProps, MapBackground } from "./Basemap.types";
import { defaultMapBackgrounds } from "./defaults";

const BasemapPopover: FC<BaseMapPopoverProps> = ({
  visible,
  mapBackground,
  handleOnBaseMapChange,
  setVisible,
  isTablet,
  basemapOptions = defaultMapBackgrounds,
}) => {
  const { t } = useTranslation("components");
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = (event.target as HTMLInputElement).value;
    handleOnBaseMapChange(value as MapBackground);
    setVisible(false);
  };

  return visible ? (
    <Paper
      elevation={3}
      sx={[
        isTablet ? styles.popoverMobile : styles.popoverDesktop,
        { order: 2 },
      ]}
    >
      <FormControl component="fieldset">
        <RadioGroup
          name="background"
          value={mapBackground}
          onChange={handleChange}
          sx={[styles.radio, { flexDirection: isTablet ? "column" : "row" }]}
        >
          {basemapOptions.map((map: MapBackground) => (
            <FormControlLabel
              key={map}
              sx={styles.buttonContainer}
              value={map}
              control={<Radio sx={{ display: "none" }} color="primary" />}
              label={
                <Stack sx={styles.button}>
                  <Avatar
                    sx={[
                      styles.image,
                      {
                        border:
                          map === mapBackground ? "2px solid #47B9B2" : "none",
                      },
                    ]}
                    variant="rounded"
                    alt={map}
                    src={`/components/map/${map}.PNG`}
                  />

                  <Typography
                    variant="caption"
                    color={map === mapBackground ? "primary" : "GrayText"}
                  >
                    {t(`components.Map.SatelliteToggle.${map}`) as string}
                  </Typography>
                </Stack>
              }
            />
          ))}
        </RadioGroup>
      </FormControl>
    </Paper>
  ) : null;
};

export default BasemapPopover;

const styles = {
  popoverMobile: {
    marginBottom: 2,
  },
  popoverDesktop: {
    marginLeft: 1,
    marginRight: 1,
  },
  radio: {
    padding: 0.75,
    alignItems: "space-between",
    justifyContent: "space-between",
  },
  buttonContainer: {
    margin: 0,
    width: 74,
    display: "flex",
    justifyContent: "center",
  },
  button: {
    alignItems: "center",
  },
  image: {
    width: 56,
    height: 56,
    ":hover": { border: "2px solid  #47B9B2" },
  },
};
