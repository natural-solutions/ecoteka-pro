import { MapBackground } from "./Basemap.types";

export const defaultMapBackgrounds: MapBackground[] = [
  MapBackground.Map,
  MapBackground.Aerial,
  MapBackground.Ign,
  MapBackground.MapDark,
];
