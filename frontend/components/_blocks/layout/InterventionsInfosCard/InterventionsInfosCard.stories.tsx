import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import InterventionsInfosCard from "./InterventionsInfosCard";

const meta = {
  title: "Blocks/Layout/InterventionsInfosCard",
  component: InterventionsInfosCard,
  parameters: {
    docs: {
      description: {
        component: "InterventionsInfosCard",
      },
    },
  },
} satisfies Meta<typeof InterventionsInfosCard>;

export default meta;
type Story = StoryObj<typeof InterventionsInfosCard>;

export const Test: Story = {
  render: () => <InterventionsInfosCard data={{}} />,
};
