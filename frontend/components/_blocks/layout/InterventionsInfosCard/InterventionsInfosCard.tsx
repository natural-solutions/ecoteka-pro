import * as React from "react";
import { useTranslation } from "react-i18next";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

import { useRouter } from "next/router";
import { FetchOneWorksiteQuery } from "@generated/graphql";
import {
  Autocomplete,
  AutocompleteRenderInputParams,
  Box,
  Grid,
  InputAdornment,
  Paper,
  TextField,
  IconButton,
  Collapse,
} from "@mui/material";

export interface IInterventionsInfosCardProps {
  data: FetchOneWorksiteQuery | null | undefined;
}

const InterventionsInfosCard = ({ data }: IInterventionsInfosCardProps) => {
  const { t } = useTranslation(["components"]);

  const [open, setOpen] = React.useState<boolean[]>(
    new Array(data?.worksite?.interventions?.length || 0).fill(false)
  );

  const handleToggleOpen = (index: number) => {
    const newOpen = [...open];
    newOpen[index] = !newOpen[index];
    setOpen(newOpen);
  };

  return (
    <Card sx={{ maxHeight: 350, overflow: "auto" }}>
      <CardContent
        sx={{
          display: "flex",
          flexDirection: "column",
          padding: "16px",
        }}
      >
        <Typography
          variant="h6"
          sx={{
            padding: "0px 0px 16px 0px",
          }}
        >
          {t("InterventionsInfosCard.title")}
        </Typography>
        {data?.worksite?.interventions?.map((intervention, index) => (
          <Paper elevation={1} key={index} sx={{ p: 1, m: 1 }}>
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <Typography variant="subtitle2">
                {t(
                  `components.Interventions.interventionsList.${intervention.intervention_type.slug}`
                )}
              </Typography>
              <IconButton onClick={() => handleToggleOpen(index)}>
                <ExpandMoreIcon
                  sx={{
                    transform: open[index] ? "rotate(0deg)" : "rotate(180deg)",
                    transition: "transform 0.3s ease",
                  }}
                />
              </IconButton>
            </Box>

            <Collapse in={open[index]}>
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                mt={1}
              >
                <Autocomplete
                  fullWidth
                  disabled
                  options={[]}
                  getOptionLabel={(option) =>
                    t(
                      `components.Interventions.interventionsList.${option.slug}`
                    )
                  }
                  value={intervention?.intervention_type || null}
                  renderInput={(params: AutocompleteRenderInputParams) => (
                    <TextField
                      {...params}
                      label={t(
                        "components.Intervention.properties.interventionType"
                      )}
                      variant="filled"
                      size="small"
                    />
                  )}
                />
              </Box>

              <Grid container spacing={1} alignItems="center" mt={0.1}>
                <Grid item xs={6}>
                  <Autocomplete
                    fullWidth
                    disabled
                    options={[]}
                    getOptionLabel={(option) => option.name}
                    value={intervention?.intervention_partner || null}
                    renderInput={(params: AutocompleteRenderInputParams) => (
                      <TextField
                        {...params}
                        label={t(
                          "components.Intervention.properties.interventionPartner"
                        )}
                        variant="filled"
                        size="small"
                      />
                    )}
                  />
                </Grid>

                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    disabled
                    type="number"
                    label={t("components.WorksiteForm.interventions.cost")}
                    value={intervention?.cost || ""}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">€</InputAdornment>
                      ),
                    }}
                    variant="filled"
                    size="small"
                  />
                </Grid>
              </Grid>
            </Collapse>
          </Paper>
        ))}
      </CardContent>
    </Card>
  );
};

export default InterventionsInfosCard;
