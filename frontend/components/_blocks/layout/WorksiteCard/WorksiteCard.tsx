import * as React from "react";
import { useTranslation } from "react-i18next";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

import { useRouter } from "next/router";
import { FetchOneWorksiteQuery } from "@generated/graphql";
import WorksiteFormFields from "./WorksiteFormFields";
import { Control, FieldValues, UseFormSetValue } from "react-hook-form";

export interface IWorksiteCardProps {
  data: FetchOneWorksiteQuery | null | undefined;
  control: Control;
  setValue: UseFormSetValue<FieldValues>;
  userRole?: string[] | undefined;
}

const WorksiteCard = ({
  data,
  control,
  setValue,
  userRole,
}: IWorksiteCardProps) => {
  const router = useRouter();
  const { query } = router;

  const isEditable = query.hasOwnProperty("edit");

  const { t } = useTranslation(["components"]);

  return (
    <Card>
      <CardContent
        sx={{
          display: "flex",
          flexDirection: "column",
          padding: "16px",
        }}
      >
        <Typography
          variant="h6"
          sx={{
            padding: "0px 0px 16px 0px",
          }}
        >
          {t("WorksiteCard.title")}
        </Typography>
        <WorksiteFormFields
          data={data}
          control={control}
          setValue={setValue}
          isEditable={isEditable}
          userRole={userRole}
        />
      </CardContent>
    </Card>
  );
};

export default WorksiteCard;
