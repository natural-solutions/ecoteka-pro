import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import WorksiteCard from "./WorksiteCard";
import { FetchOneWorksiteQuery } from "@generated/graphql";

const meta = {
  title: "Blocks/Layout/WorksiteCard",
  component: WorksiteCard,
  parameters: {
    docs: {
      description: {
        component: "WorksiteCard",
      },
    },
  },
} satisfies Meta<typeof WorksiteCard>;

export default meta;
type Story = StoryObj<typeof WorksiteCard>;

export type ConditionalSchema<T> = T extends string
  ? Yup.StringSchema
  : T extends number
    ? Yup.NumberSchema
    : T extends boolean
      ? Yup.BooleanSchema
      : T extends Record<any, any>
        ? Yup.AnyObjectSchema
        : T extends Array<any>
          ? Yup.ArraySchema<any, any>
          : Yup.AnySchema;

export type Shape<Fields> = {
  [Key in keyof Fields]: ConditionalSchema<Fields[Key]>;
};
const transformNumber = (value) => (isNaN(value) ? undefined : Number(value));

const worksiteSchema = Yup.object().shape<Shape<FetchOneWorksiteQuery>>({
  worksite: Yup.object().shape({
    id: Yup.string().nullable(),
    name: Yup.string().required("le champ est requis"),
    location_status: Yup.string(),
    description: Yup.string().nullable(),
    cost: Yup.number().transform(transformNumber).nullable(),
  }),
});

export const FormDisabledNoData: Story = {
  render: () => {
    const {
      handleSubmit,
      reset,
      setValue,
      watch,
      control,
      formState: { dirtyFields, errors },
    } = useForm<FetchOneWorksiteQuery>({
      resolver: yupResolver(worksiteSchema),
      mode: "all",
      defaultValues: {
        worksite: {
          id: "",
          name: "",
          description: "",
          cost: 0,
          creation_date: "",
          interventions: [],
          realization_date: "",
          scheduled_start_date: "",
          scheduled_end_date: "",
        },
      },
    });

    return <WorksiteCard data={{}} control={control} setValue={setValue} />;
  },
};
