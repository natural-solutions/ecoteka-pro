import WorkInProgress from "@core/WorkInProgress/WorkInProgress";
import FormInputText from "@core/forms/elements/FormInputText";
import { FetchOneWorksiteQuery } from "@generated/graphql";
import {
  Autocomplete,
  Box,
  Grid,
  InputAdornment,
  TextField,
  Chip,
  Tooltip,
} from "@mui/material";
import type { Control, FieldValues, UseFormSetValue } from "react-hook-form";
import { useTranslation } from "react-i18next";

export interface IWorksiteFormFieldsProps {
  data: FetchOneWorksiteQuery | null | undefined;
  control: Control;
  setValue: UseFormSetValue<FieldValues>;
  isEditable: boolean;
  userRole: string[] | undefined;
}

const WorksiteFormFields = ({
  data,
  control,
  setValue,
  isEditable,
  userRole,
}: IWorksiteFormFieldsProps) => {
  const { t } = useTranslation(["components"]);

  // Helper function to remove duplicates based on a key
  const uniqueByKey = (array: any[], key: string) => {
    return array.filter(
      (item, index, self) =>
        index === self.findIndex((t) => t[key] === item[key])
    );
  };

  // Extract and filter intervention types, scientific names, and partners
  const interventionTypes = uniqueByKey(
    data?.worksite?.interventions?.map((intervention) => ({
      slug: intervention?.intervention_type?.slug,
      label: t(
        `Interventions.interventionsList.${intervention?.intervention_type?.slug}`
      ),
    })) || [],
    "slug"
  ).map((item) => item.label);

  const scientificNames = uniqueByKey(
    data?.worksite?.interventions
      ?.filter(
        (intervention) =>
          intervention?.tree?.id &&
          intervention?.tree?.scientific_name?.trim() !== ""
      )
      ?.map((intervention) => ({
        tree_id: intervention?.tree?.id,
        name: intervention?.tree?.scientific_name,
      })) || [],
    "tree_id"
  ).map((item) => item.name);

  const partners = Array.from(
    new Set(
      data?.worksite?.interventions
        ?.filter(
          (intervention) =>
            intervention?.intervention_partner?.name &&
            intervention?.intervention_partner?.name.trim() !== ""
        )
        ?.map((intervention) => intervention?.intervention_partner?.name) || []
    )
  );

  const totalCost = data?.worksite?.interventions?.reduce(
    (acc, intervention) => {
      const costPerUnit = intervention?.cost || 0; // Default to 0 if cost is null

      return acc + costPerUnit;
    },
    0
  );

  return (
    <Grid container columns={12} spacing={2}>
      <Grid item xs={6}>
        <FormInputText
          control={control}
          required
          fullWidth
          disabled={!isEditable || userRole?.includes("editor")}
          name="worksite.name"
          label={t(`Worksite.properties.name`)}
          variant="filled"
        />
      </Grid>
      <Grid item xs={6}>
        <FormInputText
          control={control}
          defaultValue={"--"}
          fullWidth
          disabled
          InputLabelProps={{
            shrink: true,
          }}
          name="worksite.station"
          label={t(`Worksite.properties.station`)}
          variant="filled"
        />
      </Grid>
      <Grid item xs={6}>
        <Autocomplete
          multiple
          options={interventionTypes}
          value={interventionTypes}
          disabled
          freeSolo
          disableCloseOnSelect
          renderTags={(value: string[], getTagProps) =>
            value.map((option: string, index: number) => (
              <Chip
                variant="filled"
                label={option}
                {...getTagProps({ index })}
                sx={{
                  "& .MuiChip-deleteIcon": {
                    display: "none",
                  },
                }}
              />
            ))
          }
          renderInput={(params) => (
            <TextField
              {...params}
              fullWidth
              disabled
              label={t(`Worksite.properties.intervention_type`)}
              variant="filled"
              InputLabelProps={{
                shrink: true,
              }}
            />
          )}
        />
      </Grid>
      <Grid item xs={6}>
        <Autocomplete
          multiple
          options={scientificNames}
          value={scientificNames}
          disabled
          freeSolo
          disableCloseOnSelect
          renderTags={(value: string[], getTagProps) =>
            value.map((option: string, index: number) => (
              <Chip
                variant="filled"
                label={option}
                {...getTagProps({ index })}
                sx={{
                  "& .MuiChip-deleteIcon": {
                    display: "none",
                  },
                }}
              />
            ))
          }
          renderInput={(params) => (
            <TextField
              {...params}
              fullWidth
              disabled
              label={t(`Worksite.properties.tree_genuses`)}
              variant="filled"
              InputLabelProps={{
                shrink: true,
              }}
            />
          )}
        />
      </Grid>
      <Grid item xs={6}>
        <Autocomplete
          multiple
          options={partners}
          value={partners}
          disabled
          freeSolo
          disableCloseOnSelect
          renderTags={(value: string[], getTagProps) =>
            value.map((option: string, index: number) => (
              <Chip
                variant="filled"
                label={option}
                {...getTagProps({ index })}
                sx={{
                  "& .MuiChip-deleteIcon": {
                    display: "none",
                  },
                }}
              />
            ))
          }
          renderInput={(params) => (
            <TextField
              {...params}
              fullWidth
              disabled
              label={t(`Worksite.properties.partner`)}
              variant="filled"
              InputLabelProps={{
                shrink: true,
              }}
            />
          )}
        />
      </Grid>
      <Grid item xs={12}>
        <Tooltip
          title={
            !userRole?.includes("admin")
              ? t(`common.errors.roles.adminOnly`)
              : ""
          }
        >
          <TextField
            disabled={!isEditable || !userRole?.includes("admin")}
            defaultValue={totalCost}
            onChange={(e) => setValue("worksite.cost", e.target.value)}
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            label={t(`Worksite.properties.cost`)}
            variant="filled"
            InputProps={{
              endAdornment: <InputAdornment position="end">€</InputAdornment>,
            }}
          />
        </Tooltip>
      </Grid>
      <Grid item xs={12}>
        <FormInputText
          control={control}
          fullWidth
          multiline
          rows={3}
          maxRows={6}
          disabled={!isEditable}
          name="worksite.description"
          label={t(`Worksite.properties.description`)}
          variant="filled"
        />
      </Grid>
    </Grid>
  );
};

export default WorksiteFormFields;
