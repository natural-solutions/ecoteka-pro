import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import DiagnosisCard from "./DiagnosisCard";
import { useTranslation } from 'react-i18next';

const { t } = useTranslation(["components"]);

const diagnosisFixtures = {
  id: t("Diagnosis.labels.id"),
  tree_condition: "good",
  collar_condition: "good",
  trunk_condition: "good",
};

const meta = {
  title: "Blocks/Layout/DiagnosisCard",
  component: DiagnosisCard,
  parameters: {
    docs: {
      description: {
        component: "DiagnosisCard",
      },
    },
  },
} satisfies Meta<typeof DiagnosisCard>;

export default meta;
type Story = StoryObj<typeof DiagnosisCard>;

export const Test: Story = {
  render: () => <DiagnosisCard diagnosis={diagnosisFixtures} />,
};
