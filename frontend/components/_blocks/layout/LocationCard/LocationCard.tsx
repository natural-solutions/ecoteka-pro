import { useCallback, useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import LocationFields from "@components/forms/location/LocationFields";
import CardMap from "@components/_core/cards/CardMap";
import { Marker } from "react-map-gl";
import type { MapRef, MarkerDragEvent, LngLat } from "react-map-gl";
import { useRouter } from "next/router";
import { fetchAdresse } from "@lib/apiAdresse";
import MapNavigation, { Direction } from "@components/map/buttons/Navigation";
import Geolocalisation from "@components/map/Geolocalisation";
import { FlyToInterpolator } from "deck.gl";
import { Box } from "@mui/material";

export default function LocationCard({
  activeLocation,
  customControl,
  customSetValue,
  errors,
  watch,
  boundaries,
  locationFormData,
  isReadOnly = false,
}) {
  const { query } = useRouter();
  const { t } = useTranslation(["components"]);
  const mapRef = useRef<MapRef>(null);
  const defaultViewState = {
    longitude: activeLocation?.coords?.coordinates[0],
    latitude: activeLocation?.coords?.coordinates[1],
    zoom: 16,
  };
  const [marker, setMarker] = useState({
    latitude: activeLocation?.coords?.coordinates[1],
    longitude: activeLocation?.coords?.coordinates[0],
  });
  const [viewState, setViewState] = useState({
    longitude: defaultViewState.longitude,
    latitude: defaultViewState.latitude,
    zoom: defaultViewState.zoom,
  });

  useEffect(() => {
    if (mapRef.current === null) return;
  });

  useEffect(() => {
    if (mapRef.current === null) return;
    if (
      activeLocation?.coords?.coordinates &&
      mapRef.current !== null &&
      defaultViewState
    ) {
      mapRef.current?.flyTo({
        center: [defaultViewState.longitude, defaultViewState.latitude],
        zoom: defaultViewState.zoom,
        duration: 2000,
      });
      setMarker({
        longitude: activeLocation?.coords?.coordinates[0],
        latitude: activeLocation?.coords?.coordinates[1],
      });
    }
  }, [activeLocation]);

  const onMarkerDrag = useCallback((event: MarkerDragEvent) => {
    setMarker({
      longitude: event.lngLat.lng,
      latitude: event.lngLat.lat,
    });
  }, []);

  const onMarkerDragEnd = useCallback(async (event: MarkerDragEvent) => {
    const address = await fetchAdresse(event.lngLat.lng, event.lngLat.lat);
    customSetValue("location.longitude", event.lngLat.lng);
    customSetValue("location.latitude", event.lngLat.lat);
    customSetValue("location.address", address);
  }, []);

  const handleOnZoom = (direction: Direction) => {
    setViewState((prevViewState) => ({
      ...prevViewState,
      zoom:
        direction === Direction.Out
          ? prevViewState.zoom - 1
          : prevViewState.zoom + 1,
    }));
    if (mapRef.current !== null) {
      mapRef.current.setZoom(viewState.zoom);
    }
  };

  const handleOnGeolocate = (x: number, y: number) => {
    const newViewState = {
      longitude: x,
      latitude: y,
      zoom: viewState.zoom,
      pitch: 0,
      bearing: 0,
      transitionDuration: 3000,
      transitionInterpolator: new FlyToInterpolator(),
    };
    if (mapRef.current !== null) {
      mapRef.current?.flyTo({
        center: [newViewState.longitude, newViewState.latitude],
        zoom: viewState.zoom,
        duration: 2000,
      });
    }
  };

  return (
    <Card sx={{ minWidth: "275px" }}>
      <CardMap
        ref={mapRef}
        initialViewState={viewState}
        mapStyle={"/mapStyles/aerial_light.json"}
        attributionControl={false}
      >
        <MapNavigation onZoom={handleOnZoom} />
        <Box
          sx={[styles.topActionsContainerRight]}
          data-cy="map-actions-container-geolocalisation"
        >
          <Geolocalisation onGeolocate={handleOnGeolocate} />
        </Box>

        {marker.longitude && marker.latitude && (
          <Marker
            longitude={marker.longitude}
            latitude={marker.latitude}
            color="teal"
            draggable={!isReadOnly}
            onDrag={onMarkerDrag}
            onDragEnd={onMarkerDragEnd}
          />
        )}
      </CardMap>
      <CardContent
        sx={{
          display: "flex",
          flexDirection: "column",
          padding: "16px",
        }}
      >
        <Typography
          variant="h5"
          component="div"
          sx={{
            padding: "0px 0px 16px 0px",
          }}
        >
          {t("LocationForm.title")}
        </Typography>

        <LocationFields
          extended={false}
          activeLocation={activeLocation}
          customControl={customControl}
          customSetValue={customSetValue}
          errors={errors}
          watch={watch}
          boundaries={boundaries}
          locationFormData={locationFormData}
        />
      </CardContent>
    </Card>
  );
}

const styles = {
  topActionsContainerRight: {
    position: "absolute",
    top: { md: 90, xs: 84 },
    width: "100%",
    display: "flex",
    justifyContent: { md: "right ", xs: "right" },
    right: { md: 12, xs: 12 },
  },
};
