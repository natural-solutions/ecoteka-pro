import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import LocationCard from "./LocationCard";

const meta = {
  title: "Blocks/Layout/LocationCard",
  component: LocationCard,
  parameters: {
    docs: {
      description: {
        component: "LocationCard",
      },
    },
  },
} satisfies Meta<typeof LocationCard>;

export default meta;
type Story = StoryObj<typeof LocationCard>;

export const Test: Story = {
  render: () => (
    <LocationCard
      activeLocation={[]}
      customControl={[]}
      customSetValue={[]}
      errors={[]}
      watch={[]}
      boundaries={[]}
      locationFormData={[]}
    />
  ),
};
