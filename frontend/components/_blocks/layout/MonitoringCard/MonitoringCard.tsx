import ComboPlot from "@blocks/monitoring/Urbasense/SubjectMonitoring/ComboPlot";
import { Card, CardContent, Grid, Stack, Typography } from "@mui/material";
import Image from "next/image";
import { useTranslation } from "react-i18next";

export interface IMonitoringCardProps {
  meteo: any;
}
const MonitoringCard = ({ meteo }: IMonitoringCardProps) => {
  const { t } = useTranslation(["components", "common"]);

  return (
    // JSX here
    <Card>
      <CardContent>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
          spacing={2}
        >
          <Grid item>
            <Typography
              variant="h5"
              component="div"
              sx={{
                padding: "0px 0px 16px 0px",
              }}
            >
              {t("Dashboard.meteo")}
            </Typography>
          </Grid>
          <Grid item>
            <Stack direction="row" spacing={1} alignItems="center">
              <Image
                src="/us_logo.png"
                width={26}
                height={26}
                alt="Logo Urbasense"
              />
              <Typography variant="body2" display="block" component="span">
              {t("Dashboard.meteoUrba")}
              </Typography>
            </Stack>
          </Grid>
        </Grid>
        <ComboPlot {...meteo} />
      </CardContent>
    </Card>
  );
};
export default MonitoringCard;
