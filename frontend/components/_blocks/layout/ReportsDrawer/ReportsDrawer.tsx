import * as React from "react";
import Box, { BoxProps } from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import ErrorIcon from "@mui/icons-material/Error";
import {
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Stack,
} from "@mui/material";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";

export default function BasicCard() {
  return (
    <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
      <Stack direction="row" alignItems="center" spacing={1}>
        <IconButton aria-label="close" color="inherit" size="small">
          <KeyboardArrowRightIcon />
        </IconButton>
        <ErrorIcon color="warning" />
        <Typography variant="h5" component="span">
          Signalements
        </Typography>
      </Stack>
      <Box sx={{ height: "250px", overflow: "hidden" }}>
        <img src="https://www.wired.com/wp-content/uploads/2016/11/GoogleMapTA.jpg" />
      </Box>
      <List sx={{ padding: 0 }}>
        <ListItem sx={{ padding: 0 }}>
          {/* <ListItemIcon>
            <CheckCircleOutline />
          </ListItemIcon> */}
          <ListItemText
            primary="Adresse du signalement"
            secondary="Description du signalement"
          />
          <ListItemSecondaryAction>
            <IconButton edge="end">
              <ChevronRightIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      </List>
    </Box>
  );
}
