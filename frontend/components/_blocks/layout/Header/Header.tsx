import React, { useState } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Container from "@mui/material/Container";
import HeaderLogo from "../../../header/Logo";
import UserMenu, { UserMainMenuAction } from "@components/header/UserMenu";
import {
  Button,
  Link,
  List,
  SwipeableDrawer,
  Typography,
  useMediaQuery,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import MenuIcon from "@mui/icons-material/Menu";
import { useRouter } from "next/router";
/* icons*/
import DashboardIcon from "@mui/icons-material/Dashboard";
import LayersIcon from "@mui/icons-material/Layers";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import ForestIcon from "@mui/icons-material/Forest";
import DateRangeIcon from "@mui/icons-material/DateRange";
import MonitorHeartIcon from "@mui/icons-material/MonitorHeart";
import NotificationsIcon from "@mui/icons-material/Notifications";
import useStore from "@stores/useStore";
import { signOut } from "next-auth/react";
import { useTranslation } from "react-i18next";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useDeviceSize } from "@lib/utils";
import TreeToolbarIcon from "@components/_core/icons/Tree/TreeToolbarIcon";
import AuthCan from "@components/auth/Can";

function CustomHeader() {
  const { app } = useStore((store) => store);
  const { t } = useTranslation(["components", "common"]);
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const router = useRouter();
  const customBreakpointWidth = 1515;
  const isCustomBreakpoint = useMediaQuery(
    `(min-width:${customBreakpointWidth}px)`
  );
  const { isTablet } = useDeviceSize();

  const toggleDrawer = (open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setIsDrawerOpen(open);
  };

  const generateStyledLink = (pathname, label, icon) => {
    const isCurrentPage = router.pathname === pathname;
    const linkSx = {
      display: "flex",
      fontFamily: "Poppins",
      alignItems: "center",
      textTransform: "capitalize",
      fontSize: "14px",
      lineHeight: "24px",
      px: !isCustomBreakpoint ? "15px" : "32px",
      textDecoration: "none",
      border: "solid 2px transparent",
      borderBottom: `2px solid ${isCurrentPage ? "#2FA37C" : "none"}`,
      color: isCurrentPage ? "#2FA37C" : "#434A4A",
      fontWeight: isCurrentPage ? 500 : 400,
      "&:hover": {
        color: "#2FA37C",
        fontWeight: "500",
        borderBottomColor: "#2FA37C",
      },
    };
    return (
      <Link
        key={label}
        href={pathname}
        sx={{
          flexDirection: !isCustomBreakpoint ? "column" : "row",

          ...linkSx,
        }}
      >
        {icon}
        {label}
      </Link>
    );
  };

  const handleLogout = async () => {
    const response = await (await fetch("/api/auth/logout")).json();
    await signOut({ redirect: false });
    window.location.href = response.path;
  };

  const handleMenuAction = (action: UserMainMenuAction) => {
    switch (action) {
      case UserMainMenuAction.logout:
        handleLogout();
        break;
    }
  };

  const menuItems = [
    {
      path: "/",
      label: t("components.Template.menuItems.dashboard.label"),
      icon: <DashboardIcon />,
      roles: ["reader", "editor", "admin"],
    },
    {
      path: "/map",
      label: t("common.map"),
      icon: <LayersIcon />,
      roles: ["reader", "editor", "admin"],
    },
    {
      path: "/worksites",
      label: t("components.Template.menuItems.worksite.label"),
      icon: <TreeToolbarIcon fillColor="#434A4A" />,
      roles: ["editor", "admin"],
    },
    {
      path: "/intervention?object=all",
      label: t("components.Template.menuItems.intervention.label"),
      icon: <DateRangeIcon />,
      roles: ["editor", "admin"],
    },
    {
      path: "/diagnosis?object=all",
      label: t("components.Template.menuItems.diagnoses.label"),
      icon: <MonitorHeartIcon />,
      roles: ["editor", "admin"],
    },
    {
      path: "/combined_data",
      label: t("components.Template.menuItems.plantHeritage.label"),
      icon: <FormatListBulletedIcon />,
      roles: ["editor", "admin"],
    },
  ];

  const getTitle = () => {
    const { pathname } = router;

    const titleMappings = {
      tree: "components.HeaderCardMobile.tree",
      location: "components.HeaderCardMobile.location",
      worksite: "components.HeaderCardMobile.worksite",
      intervention: "components.HeaderCardMobile.intervention",
      diagnosis: "components.HeaderCardMobile.diagnosis",
    };

    const matchedKey = Object.keys(titleMappings).find((key) =>
      pathname.includes(key)
    );

    return matchedKey ? (
      <Typography
        variant="h6"
        sx={{ color: "#2fa37c", textTransform: "uppercase" }}
      >
        {t(titleMappings[matchedKey])}
      </Typography>
    ) : null;
  };

  return (
    <>
      <AppBar position="static" color="default">
        <Container maxWidth={false}>
          <Toolbar>
            {isTablet ? (
              <>
                {!router.query.hasOwnProperty("edit") && (
                  <IconButton
                    color="primary"
                    aria-label={t("common.back")}
                    sx={{ mr: "16px" }}
                    onClick={() => router.back()}
                  >
                    <ArrowBackIcon />
                  </IconButton>
                )}

                {Object.keys(router.query).some((key) =>
                  key.toLowerCase().includes("id")
                ) && getTitle()}
                <Typography variant="h5" sx={{ flexGrow: 1 }}>
                  {
                    menuItems.find((item) => item.path === router.pathname)
                      ?.label
                  }
                </Typography>
                <IconButton
                  color="primary"
                  aria-label="Menu burger"
                  sx={{ mr: "16px" }}
                  onClick={toggleDrawer(true)}
                >
                  <MenuIcon />
                </IconButton>
                <UserMenu
                  onAction={handleMenuAction}
                  isDesktop={isCustomBreakpoint}
                />

                <SwipeableDrawer
                  anchor="right"
                  open={isDrawerOpen}
                  onClose={toggleDrawer(false)}
                  onOpen={toggleDrawer(true)}
                >
                  <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
                    <IconButton
                      onClick={toggleDrawer(false)}
                      size="large"
                      sx={{ color: "#434A4A" }}
                    >
                      <CloseIcon />
                    </IconButton>
                  </Box>

                  <List>
                    {menuItems.map((item, index) => (
                      <AuthCan key={index} role={item.roles}>
                        <Box
                          key={index}
                          sx={{
                            m: 2,
                            display: "flex",
                            justifyContent: "flex-start",
                          }}
                        >
                          <Link href={item.path} sx={{ borderBottom: "none" }}>
                            {item.label}
                          </Link>
                        </Box>
                      </AuthCan>
                    ))}
                  </List>
                </SwipeableDrawer>
              </>
            ) : (
              <>
                <HeaderLogo
                  onClick={() => router.push("/")}
                  mode={app.appMode}
                  isDesktop={isCustomBreakpoint}
                />
                {menuItems.map((item, index) => (
                  <AuthCan key={index} role={item.roles}>
                    <Box
                      key={index}
                      sx={{
                        flexGrow: 1,
                        display: {
                          xs: isTablet ? "none" : "flex",
                          justifyContent: "center",
                          alignSelf: "stretch",
                        },
                      }}
                    >
                      {generateStyledLink(item.path, item.label, item.icon)}
                    </Box>
                  </AuthCan>
                ))}

                <IconButton
                  color="primary"
                  aria-label="Notifications"
                  sx={{ mr: "16px" }}
                >
                  <NotificationsIcon />
                </IconButton>
                <Box sx={{ flexGrow: 0 }}>
                  <UserMenu
                    onAction={handleMenuAction}
                    isDesktop={isCustomBreakpoint}
                  />
                </Box>
              </>
            )}
          </Toolbar>
        </Container>
      </AppBar>
    </>
  );
}
export default CustomHeader;
