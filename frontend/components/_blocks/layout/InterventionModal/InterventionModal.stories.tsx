import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import InterventionModal from "./InterventionModal";

const meta = {
  title: "Blocks/Layout/InterventionModal",
  component: InterventionModal,
  parameters: {
    docs: {
      description: {
        component: "InterventionModal",
      },
    },
  },
} satisfies Meta<typeof InterventionModal>;

export default meta;
type Story = StoryObj<typeof InterventionModal>;

/* const treeInfosFixtures = {
  id: "Id",
  serial_number: "serial number",
  height: 5,
  scientific_name: "Scientific name",
  plantation_date: "2020-04-25",
}; */

export const Test: Story = {
  render: () => <InterventionModal />,
};
