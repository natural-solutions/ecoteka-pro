import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import HistoricCard from "./HistoricCard";

const meta = {
  title: "Blocks/Layout/HistoricCard",
  component: HistoricCard,
  parameters: {
    docs: {
      description: {
        component: "HistoricCard",
      },
    },
  },
} satisfies Meta<typeof HistoricCard>;

export default meta;
type Story = StoryObj<typeof HistoricCard>;

const eventsFixtures = [
  {
    id: "3790fcea-a6e7-405f-96b6-dd253558fe2c",
    intervention_partner: {
      id: "52b24d26-ed1f-4318-ab9a-f8c210da3b67",
      name: "NaturElagage",
    },
    intervention_type: {
      slug: "trim",
    },
    scheduled_date: "2023-10-05",
    tree: {
      location_id: "",
    },
    intervention_worksite_interventions: {},
  },
];

export const Test: Story = {
  render: () => <HistoricCard events={eventsFixtures} locationId="" />,
};
