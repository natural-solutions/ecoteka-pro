import * as React from "react";
import { useTranslation } from "react-i18next";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import InfoIcon from "@mui/icons-material/Info";
import { Box, Button, List, Stack, TextField, Tooltip } from "@mui/material";
import Progress from "@components/_core/metrics/Progress";
import ListLocationItem from "@components/_blocks/lists/LocationList/ListItem";
import Count from "@components/_core/metrics/Count";
import { FetchOneWorksiteLocationsQuery } from "@generated/graphql";
import { CardMap } from "@components/_core/cards";
import { MapRef, LngLatBoundsLike, Marker } from "react-map-gl";
import { useEffect, useState } from "react";
import { CheckedIntervention } from "@pages/worksites/[worksiteId]";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import { useRouter } from "next/router";
import AuthCan from "@components/auth/Can";

type LatLngCoords = [number, number];

const getBbox = (coords: LatLngCoords[] | undefined): LngLatBoundsLike => {
  if (coords && coords.length > 0) {
    const latitudes = coords.map((coord) => coord[1]);
    const longitudes = coords.map((coord) => coord[0]);
    return [
      [Math.min(...longitudes), Math.min(...latitudes)],
      [Math.max(...longitudes), Math.max(...latitudes)],
    ];
  }
  return [
    [-180, -180],
    [180, 180],
  ];
};

export interface ILocationListProps {
  data: FetchOneWorksiteLocationsQuery | undefined | null;
  onCheckboxChange(interventionId: string, locationId: string): void;
  checkedInterventions: any;
  validateAllInterventions(
    validateAll: boolean,
    realizationReport: string
  ): void;
  isWorksiteRealized: boolean;
}

export interface WorksiteProps {
  interventions?: Array<{
    __typename?: "intervention";
    id: any;
    realization_date?: any | null;
    scheduled_date: any;
    intervention_type: { __typename?: "intervention_type"; slug: string };
    intervention_partner?: {
      __typename?: "intervention_partner";
      name: string;
    } | null;
    tree?: {
      __typename?: "tree";
      id: any;
      scientific_name?: string | null;
      location?: {
        __typename?: "location";
        address?: string | null;
        coords: any;
        id: any;
        location_status: { __typename?: "location_status"; status: string };
      } | null;
    } | null;
    location?: {
      __typename?: "location";
      address?: string | null;
      coords: any;
      id: any;
      location_status: { __typename?: "location_status"; status: string };
    } | null;
  }>;
}

const LocationList = ({
  data,
  onCheckboxChange,
  checkedInterventions,
  validateAllInterventions,
  isWorksiteRealized,
}: ILocationListProps) => {
  const { t } = useTranslation(["components", "common"]);
  const router = useRouter();
  const mapRef = React.useRef<MapRef>(null);
  const [openModal, setOpenModal] = useState(false);
  const [realizationReport, setRealizationReport] = useState("");
  const [reportError, setReportError] = useState("");

  const defaultViewState = {
    longitude:
      data?.worksite?.interventions[0]?.tree?.location?.coords.coordinates[0] ??
      data?.worksite?.interventions[0]?.location?.coords.coordinates[0],
    latitude:
      data?.worksite?.interventions[0]?.tree?.location?.coords.coordinates[1] ??
      data?.worksite?.interventions[0]?.location?.coords.coordinates[1],
    zoom: 12,
  };

  const onEditWorksite = () => {
    router.push(`/map?activeWorksite=${data?.worksite?.id}`);
  };

  const getWorksiteLocationsCoords = (
    worksite: WorksiteProps
  ): LatLngCoords[] | undefined => {
    return worksite?.interventions?.map((item) => {
      return (
        (item.tree?.location?.coords?.coordinates as LatLngCoords) ||
        (item.location?.coords?.coordinates as LatLngCoords)
      );
    });
  };
  const getInterventionsProgress = (
    worksite: WorksiteProps | null | undefined
  ): number => {
    if (!worksite?.interventions) return 0;

    const totalInterventions = worksite.interventions.length;
    const doneInterventions = worksite.interventions.filter((i) =>
      Boolean(i.realization_date)
    ).length;
    const checkedNotDoneInterventions = checkedInterventions.filter(
      (i: CheckedIntervention) =>
        worksite.interventions?.some(
          (item) => item.id === i.interventionId && !item.realization_date
        )
    ).length;

    const totalCompleted = doneInterventions + checkedNotDoneInterventions;
    return Math.round((totalCompleted / totalInterventions) * 100);
  };

  const handleConfirm = async () => {
    if (!realizationReport.trim()) {
      setReportError(t("common.errors.fillInRequiredFields"));
      return;
    }
    setReportError("");
    await validateAllInterventions(true, realizationReport);
    setOpenModal(false);
    setRealizationReport("");
  };

  const handleClose = () => {
    setOpenModal(false);
    setRealizationReport("");
    setReportError("");
  };

  useEffect(() => {
    if (mapRef.current === null) return;
  }, []);

  useEffect(() => {
    if (mapRef.current === null) return;
    if (data?.worksite) {
      mapRef.current?.fitBounds(
        getBbox(getWorksiteLocationsCoords(data?.worksite)),
        {
          padding: 40,
          duration: 1000,
        }
      );
    }
  }, [data]);

  return (
    <Card sx={{ width: "100%", minWidth: "275px" }}>
      <CardMap
        ref={mapRef}
        initialViewState={{
          ...defaultViewState,
          zoom: defaultViewState.zoom + 4,
        }}
        mapStyle="/mapStyles/aerial_light.json"
      >
        {data?.worksite?.interventions?.map((i) => {
          const coords =
            i.tree?.location?.coords?.coordinates ||
            i.location?.coords?.coordinates;
          return coords ? (
            <Marker
              key={`location-${i.id}`}
              longitude={coords[0]}
              latitude={coords[1]}
              color="teal"
            />
          ) : null;
        })}
      </CardMap>
      {!isWorksiteRealized && (
        <AuthCan role={["admin"]}>
          <Box
            display="flex"
            justifyContent="center"
            sx={{ width: "100%", mt: 1 }}
          >
            <Box display="flex" alignItems="center">
              <Tooltip title={t("components.WorksiteForm.editTooltip")}>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={onEditWorksite}
                  endIcon={<InfoIcon />}
                >
                  {t("components.WorksiteForm.edit")}
                </Button>
              </Tooltip>
            </Box>
          </Box>
        </AuthCan>
      )}
      <CardContent sx={{ width: "100%", minWidth: "275px", mb: 2 }}>
        <Stack
          direction="column"
          alignItems="start"
          spacing={2}
          sx={{ width: "100%" }}
        >
          <Stack
            direction="column"
            alignItems="start"
            spacing={1}
            sx={{ width: "100%" }}
          >
            <Typography variant="h6" component="div" sx={{ padding: 0 }}>
              {t("components.LocationList.title")}
            </Typography>
            <Count
              name="emplacements"
              data={data?.worksite?.interventions}
            ></Count>
          </Stack>
          <Progress
            value={getInterventionsProgress(data?.worksite)}
            unit="% réalisé"
          ></Progress>
          <List sx={{ width: "100%", overflow: "auto", maxHeight: 434 }}>
            {data?.worksite?.interventions?.map((i) => (
              <ListLocationItem
                key={`location-item-${i.id}`}
                intervention={i}
                isCompleted={Boolean(i.realization_date)}
                showWarning={false}
                onCheckboxChange={() =>
                  onCheckboxChange(i.id, i.tree?.location?.id || i.location?.id)
                }
              />
            ))}
          </List>
          {!isWorksiteRealized && (
            <Box display="flex" justifyContent="center" sx={{ width: "100%" }}>
              <Button
                color="primary"
                variant="contained"
                onClick={() => setOpenModal(true)}
              >
                {t("components.WorksiteForm.declareRealized")}
              </Button>
            </Box>
          )}
        </Stack>
      </CardContent>

      <ModalComponent open={openModal} handleClose={handleClose}>
        <Box sx={{ p: 2, width: "300px" }}>
          <Typography variant="h6" sx={{ mb: 3 }}>
            {t("components.WorksiteForm.declareRealized")}
          </Typography>
          <TextField
            fullWidth
            required
            multiline
            rows={6}
            label={t("components.WorksiteForm.realizationReport")}
            value={realizationReport}
            onChange={(e) => {
              setRealizationReport(e.target.value);
              setReportError("");
            }}
            error={!!reportError}
            helperText={reportError}
            sx={{ mb: 4 }}
          />
          <Stack direction="row" spacing={5} justifyContent="flex-end">
            <Button variant="outlined" onClick={handleClose} fullWidth>
              {t("common.buttons.cancel")}
            </Button>
            <Button variant="contained" onClick={handleConfirm} fullWidth>
              {t("common.buttons.confirm")}
            </Button>
          </Stack>
        </Box>
      </ModalComponent>
    </Card>
  );
};

export default LocationList;
