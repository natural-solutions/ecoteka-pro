import {
  ListItemIcon,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Stack,
  Typography,
  Checkbox,
} from "@mui/material";
import ListItem, { ListItemProps } from "@mui/material/ListItem";
import CheckIcon from "@mui/icons-material/Check";
import CircleCheckedFilled from "@mui/icons-material/CheckCircle";
import CircleUnchecked from "@mui/icons-material/RadioButtonUnchecked";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { green } from "@mui/material/colors";
import ErrorIcon from "@mui/icons-material/Error";
import router, { useRouter } from "next/router";
import { FetchOneWorksiteLocationsQuery } from "@generated/graphql";
import { useTranslation } from "react-i18next";
export interface IListLocationItemProps {
  intervention: NonNullable<
    NonNullable<FetchOneWorksiteLocationsQuery["worksite"]>["interventions"]
  >[0];
  active?: boolean;
  isCompleted?: boolean;
  showWarning?: boolean;
  onCheckboxChange(interventionId: string): void;
}

const ListLocationItem = ({
  intervention,
  active = false,
  isCompleted = false,
  showWarning = false,
  onCheckboxChange,
  ...props
}: IListLocationItemProps & ListItemProps) => {
  const { query } = useRouter();
  const { t } = useTranslation(["components"]);
  const isReadOnly = () => !query.hasOwnProperty("edit");

  const locationId =
    intervention.tree?.location?.id || intervention.location?.id;
  const address =
    intervention.tree?.location?.address || intervention.location?.address;
  const treeId = intervention.tree?.id ? intervention.tree.id : undefined;

  const getLocationStatus = (location) => {
    if (!location) return null;
    const status = location.location_status?.status;
    if (status === "empty")
      return t("LayerManager.layers.LocationsItems.Empty");
    if (status === "stump")
      return t("LayerManager.layers.LocationsItems.Stump");
    return null;
  };

  const getTreeStatus = (tree) => {
    if (!tree) return null;
    if (tree) {
      return (
        tree.scientific_name || t("LayerManager.layers.LocationsItems.Tree")
      );
    }
    return getLocationStatus(tree.location);
  };

  const specie = intervention.tree
    ? getTreeStatus(intervention.tree)
    : getLocationStatus(intervention.location);

  const getLocationURI = () => {
    return treeId
      ? `/tree/${treeId}?interventionModal=open&activeId=${intervention.id}`
      : `/location/${locationId}?interventionModal=open&activeId=${intervention.id}`;
  };

  return (
    <ListItem
      sx={{
        padding: "0 1.5rem",
        background: isCompleted ? green[100] : "none",
      }}
    >
      <ListItemIcon>
        {isCompleted ? (
          <CheckIcon sx={{ color: green[400] }} />
        ) : (
          <Checkbox
            disabled={isReadOnly()}
            onChange={() => onCheckboxChange(intervention.id)}
            icon={<CircleUnchecked />}
            checkedIcon={<CircleCheckedFilled />}
          />
        )}
      </ListItemIcon>
      <ListItemText
        primary={
          <Stack
            direction="row"
            alignItems="start"
            spacing={1}
            sx={{ width: "100%" }}
          >
            {showWarning && <ErrorIcon fontSize="small" color="error" />}
            <Typography component="span" variant="body2" color="text.primary">
              {address}
            </Typography>
          </Stack>
        }
        secondary={
          <Stack direction="column" spacing={0.5}>
            <Typography variant="body2">
              {t(
                `Interventions.interventionsList.${intervention?.intervention_type?.slug}`
              )}
              {specie && " - "}
              {specie}
            </Typography>
          </Stack>
        }
      />
      <ListItemSecondaryAction>
        <IconButton edge="end" onClick={() => router.push(getLocationURI())}>
          <ChevronRightIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};
export default ListLocationItem;
