import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import LocationListItem from "./ListItem";

const meta = {
  title: "Blocks/Lists/LocationListItem",
  component: LocationListItem,
  parameters: {
    docs: {
      description: {
        component: "LocationListItem",
      },
    },
  },
} satisfies Meta<typeof LocationListItem>;

export default meta;
type Story = StoryObj<typeof LocationListItem>;

const defaultData = {
  isCompleted: true,
  showWarning: true,
};

export const Default: Story = {
  render: () => (
    <LocationListItem
      {...defaultData}
      onCheckboxChange={() => console.log("on change")}
      intervention={{
        id: "test-id",
        scheduled_date: new Date().toISOString(),
        data_entry_user_id: "user-1",
        intervention_type: {
          slug: "test-slug",
          id: "type-1",
        },
      }}
    />
  ),
};

// export const WithWarning: Story = {
//   render: () => <LocationListItem />,
// };
