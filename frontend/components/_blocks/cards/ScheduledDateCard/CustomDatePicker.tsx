import { LocalizationProvider, StaticDatePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
// import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { fr } from "date-fns/locale";

export interface ICustomDatePickerProps {
  date: Date | null;
  handleChangeDate: (date: Date | null) => void;
}
const CustomDatePicker = ({
  date,
  handleChangeDate,
}: ICustomDatePickerProps) => {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={fr}>
      <StaticDatePicker
        displayStaticWrapperAs="desktop"
        value={date}
        onChange={(newDate) => handleChangeDate(newDate)}
        slotProps={{
          toolbar: { toolbarFormat: "d LLLL y", hidden: false },
        }}
      />
    </LocalizationProvider>
  );
};
export default CustomDatePicker;
