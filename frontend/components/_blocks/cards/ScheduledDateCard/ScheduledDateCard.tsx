import { CardTitle } from "@core/typography";
import { Button, Card, CardContent, Stack, Typography } from "@mui/material";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { differenceInDays, isBefore } from "date-fns";
import { fr } from "date-fns/locale";
import { ScheduleIcon } from "@components/icons";
import type { IScheduleIconProps } from "@components/icons";
import AuthCan from "@components/auth/Can";
import { DayPicker, DateRange } from "react-day-picker";
import "react-day-picker/dist/style.css";
import { dateToString } from "@lib/utils";

export interface IScheduledDateCardProps {
  itemId: string;
  scheduledStartDate: Date | undefined;
  scheduledEndDate: Date | undefined;
  onChangeScheduledDate?: (date: DateRange | undefined) => void;
  onConfirmScheduledDateChange?: (
    id: string,
    date: DateRange | undefined
  ) => void;
}

const ScheduledDateCard = ({
  itemId,
  scheduledStartDate,
  scheduledEndDate,
  onConfirmScheduledDateChange = (id, date) => console.log(id, date),
}: IScheduledDateCardProps) => {
  const { t } = useTranslation(["components"]);

  const [dateRange, setDateRange] = useState<DateRange | undefined>({
    from: scheduledStartDate ?? undefined,
    to: scheduledEndDate ?? undefined,
  });
  const [showDatepicker, setDatePickerDisplay] = useState<boolean>(false);

  const getSchedulingStatus = (
    scheduledDates: DateRange | undefined
  ): IScheduleIconProps["variant"] => {
    if (scheduledDates?.from) {
      const diffInDays = differenceInDays(
        new Date(scheduledDates.from),
        Date.now()
      );
      if (isBefore(new Date(scheduledDates.from), Date.now())) {
        return "late";
      }
      if (
        !isBefore(new Date(scheduledDates.from), Date.now()) &&
        diffInDays >= 0 &&
        diffInDays < 7
      ) {
        return "soon";
      }
      if (
        !isBefore(new Date(scheduledDates.from), Date.now()) &&
        diffInDays >= 7
      ) {
        return "planned";
      }
      return "unknown";
    } else {
      return "unknown";
    }
  };
  const handleDateChange = (nextRange, selectedDay) => {
    setDateRange((range) => {
      // If both dates (from and to) are already defined, restart the selection
      if (range?.from && range?.to) {
        // Restart the selection by setting the new "from" date
        return { from: selectedDay, to: undefined }; // Reset "to" if restarting
      }
      // If only "from" is defined, set "to" with the selected date
      if (range?.from && !range?.to) {
        return { ...range, to: selectedDay };
      }
      return nextRange as DateRange; // Otherwise, return the next range
    });
  };

  const handleConfirm = () => {
    onConfirmScheduledDateChange(itemId, dateRange);
    setDatePickerDisplay(false);
  };

  const handleCancel = () => {
    setDatePickerDisplay(false);
    setDateRange({
      from: scheduledStartDate ?? undefined,
      to: scheduledEndDate ?? undefined,
    });
  };

  return (
    <>
      <Card>
        <CardContent
          sx={{
            padding: "16px",
          }}
        >
          <Stack
            direction="row"
            spacing={2}
            alignContent="center"
            justifyContent="space-between"
          >
            <Stack
              direction="column"
              justifyContent="center"
              alignItems="flex-start"
              spacing={0}
            >
              <CardTitle title={`${t("ScheduledDateCard.title")}`} />
              <Stack
                direction="row"
                spacing={0.5}
                alignContent="center"
                justifyContent="space-between"
              >
                <ScheduleIcon variant={getSchedulingStatus(dateRange)} />
                <Typography variant="subtitle2" gutterBottom>
                  {dateToString(dateRange?.from)}{" "}
                  {dateRange?.to ? ` - ${dateToString(dateRange.to)}` : ""}
                </Typography>
              </Stack>
            </Stack>
            <Stack
              direction="column"
              justifyContent="center"
              alignItems="flex-start"
              spacing={0}
            >
              <AuthCan role={["admin"]}>
                <>
                  {!showDatepicker && (
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => setDatePickerDisplay(true)}
                    >{`${t("ScheduledDateCard.replan")}`}</Button>
                  )}
                  {showDatepicker && (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => handleConfirm()}
                    >{`${t("ScheduledDateCard.confirm")}`}</Button>
                  )}
                </>
              </AuthCan>
            </Stack>
          </Stack>
          {showDatepicker && (
            <>
              <DayPicker
                mode="range"
                selected={dateRange}
                onSelect={handleDateChange}
                locale={fr}
              />
              <Button
                sx={{ margin: 1 }}
                variant="outlined"
                onClick={() => handleCancel()}
              >
                {`${t("ScheduledDateCard.cancel")}`}
              </Button>
            </>
          )}
        </CardContent>
      </Card>
    </>
  );
};
export default ScheduledDateCard;
