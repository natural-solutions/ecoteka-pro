import ReactECharts, { EChartsOption } from "echarts-for-react";

export interface DataItem {
  value: number;
  name: string;
}

export interface ISustainabilityWidgetProps {
  data?: DataItem[];
}
const defaultData: DataItem[] = [
  { value: 150, name: "Nouveaux" },
  { value: 50, name: "Abattus remplacés" },
  { value: 190, name: "Abattus non-remplacés" },
];

const SustainabilityWidget = ({
  data = defaultData,
}: ISustainabilityWidgetProps) => {
  const setOption = (data: DataItem[]): EChartsOption => {
    return {
      tooltip: {
        trigger: "item",
      },
      legend: {
        top: "bottom",
      },
      series: [
        {
          name: "Indice de pérennité",
          type: "pie",
          radius: ["50%", "60%"],
          center: ["50%", "40%"],
          label: {
            show: false,
          },
          avoidLabelOverlap: false,
          itemStyle: {
            borderRadius: 5,
            borderColor: "#fff",
            borderWidth: 2,
          },
          color: ["#61D878", "#16B1FF", "#FF4C51"],
          data: data,
        },
      ],
    };
  };
  return <ReactECharts option={setOption(data)} />;
};
export default SustainabilityWidget;
