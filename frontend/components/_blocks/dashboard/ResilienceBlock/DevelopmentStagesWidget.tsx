import ReactECharts, { EChartsOption } from "echarts-for-react";
import { useTranslation } from "react-i18next";

export interface DataItem {
  value: number;
  name: string;
}

export interface IDevelopmentStagesWidgetProps {
  data?: DataItem[];
}

const { t } = useTranslation(["components"]);

const defaultData: DataItem[] = [
  { value: 56, name: "Jeune" },
  { value: 22, name: "Adulte" },
  { value: 48, name: "Sénescent" },
  { value: 32, name: "Mature" },
];

const DevelopmentStagesWidget = ({
  data = defaultData,
}: IDevelopmentStagesWidgetProps) => {
  const setOption = (data: DataItem[]): EChartsOption => {
    return {
      tooltip: {
        trigger: "item",
      },
      legend: {
        top: "bottom",
      },
      series: [
        {
          name: t("blocks.Resilience.analyticsModules.durabilityScore"),
          type: "pie",
          radius: ["50%", "60%"],
          center: ["50%", "40%"],
          label: {
            show: false,
          },
          avoidLabelOverlap: false,
          itemStyle: {
            borderRadius: 5,
            borderColor: "#fff",
            borderWidth: 2,
          },
          color: ["#9155FD", "#16B1FF", "#FFB400", "#FCD400"],
          data: data,
        },
      ],
    };
  };
  return <ReactECharts option={setOption(data)} />;
};
export default DevelopmentStagesWidget;
