import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import ResilienceBlock from "./ResilienceBlock";

const meta = {
  title: "Blocks/Dashboard/ResilienceBlock",
  component: ResilienceBlock,
  parameters: {
    docs: {
      description: {
        component: "ResilienceBlock",
      },
    },
  },
} satisfies Meta<typeof ResilienceBlock>;
export default meta;
type Story = StoryObj<typeof ResilienceBlock>;
export const Default: Story = {
  render: () => <ResilienceBlock />,
};
