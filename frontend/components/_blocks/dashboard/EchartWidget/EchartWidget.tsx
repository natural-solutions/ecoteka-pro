import ReactECharts from "echarts-for-react";
import { EChartsOption } from "echarts-for-react/lib/types";
import { Alert } from "@mui/material";

export interface IEchartWidgetProps {
  config: EChartsOption;
  loading?: boolean;
  error: boolean;
  customStyle?: {};
  onEvents?: {
    [event: string]: (params: any) => void; // Vous pouvez définir les types d'événements spécifiques si nécessaire
  };
}

const EchartWidget = ({
  config,
  error,
  customStyle,
  onEvents,
}: IEchartWidgetProps) => {
  if (error) {
    return <Alert severity="info">Aucune donnée</Alert>;
  }

  if (config) {
    return (
      <ReactECharts
        option={config}
        style={customStyle ? customStyle : {}}
        onEvents={onEvents}
      />
    );
  } else {
    return <Alert severity="error">Erreur inattendue!</Alert>;
  }
};
export default EchartWidget;
