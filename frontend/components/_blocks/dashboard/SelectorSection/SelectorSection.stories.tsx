import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import SelectorSection from "./SelectorSection";

const meta = {
  title: "Blocks/Dashboard/SelectorSection",
  component: SelectorSection,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Selector section",
      },
    },
  },
} satisfies Meta<typeof SelectorSection>;

export default meta;
type Story = StoryObj<typeof SelectorSection>;

export const Selector: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <SelectorSection
        handleInputChange={() => console.log("handleinputchange")}
        boundariesData={[]}
      />
    </Box>
  ),
};
