import React, { FC, useEffect, useState } from "react";
import {
  Box,
  Stack,
  Button,
  FormLabel,
  FormControl,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Badge,
  Tooltip,
  Typography,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import CloseIcon from "@mui/icons-material/Close";
import { useTranslation } from "react-i18next";
import { useDeviceSize } from "@lib/utils";
import { useDashboardFilters } from "@stores/pages/dashboard";
import { Boundary } from "types/analytics/analytics.types";
export interface SelectorSectionProps {
  boundariesData: Boundary[];
  handleInputChange: (inputName: string, inputValue: any) => void;
}

type CheckedItems = {
  [type: string]: string[];
};

const SelectorSection: FC<SelectorSectionProps> = ({
  boundariesData,
  handleInputChange,
}) => {
  const { isDesktop, isTablet } = useDeviceSize();
  const { t } = useTranslation(["components", "common"]);
  const [checkedItems, setCheckedItems] = useState<CheckedItems>({});
  const [isExpanded, setIsExpanded] = useState<boolean>(true);
  const dashboardFilters = useDashboardFilters();

  const handleIsExpanded = () => {
    setIsExpanded(!isExpanded);
  };

  const clearAllCheckboxes = () => {
    setCheckedItems({});
    handleInputChange("boundaries", []);
  };

  const updateCheckedItems = (
    typeName: string,
    itemName: string,
    checkedItems: CheckedItems
  ) => {
    const updatedItems = { ...checkedItems };
    if (!updatedItems[typeName]) {
      updatedItems[typeName] = [];
    }
    if (updatedItems[typeName].includes(itemName)) {
      updatedItems[typeName] = updatedItems[typeName].filter(
        (item) => item !== itemName
      );
    } else {
      updatedItems[typeName].push(itemName);
    }
    return updatedItems;
  };

  const handleCheckboxChange = (typeName: string, itemName: string) => {
    setCheckedItems((prev) => {
      const updatedItems = updateCheckedItems(typeName, itemName, prev);

      handleInputChange(
        "boundaries",
        Object.values(updatedItems).flatMap((ids) => ids)
      );

      return updatedItems;
    });
  };

  const groupedBoundaries = boundariesData?.reduce((acc, boundary) => {
    const type = boundary.type;
    if (!acc[type]) {
      acc[type] = [];
    }
    acc[type].push(boundary);
    return acc;
  }, {});

  const totalChecked = Object.values(checkedItems).reduce(
    (total, items) => total + items.length,
    0
  );

  const getTooltipContent = () => {
    if (!isExpanded || totalChecked === 0) {
      return null;
    }

    return (
      <Box sx={{ padding: "10px 10px 0 10px" }}>
        {Object.entries(checkedItems).map(
          ([type, items], index) =>
            items.length > 0 && (
              <Typography key={index} variant="inherit" sx={{ mb: 1.2 }}>
                <span>{t(`components.LocationForm.properties.${type}`)}</span>
                {` : ${items.join(", ")}`}
              </Typography>
            )
        )}
      </Box>
    );
  };

  useEffect(() => {
    if (dashboardFilters.boundaries) {
      const updatedCheckedItems: CheckedItems = {};
      boundariesData?.forEach((boundary: Boundary) => {
        const boundaryType = boundary.type;

        if (dashboardFilters.boundaries.includes(boundary.id)) {
          if (!updatedCheckedItems[boundaryType]) {
            updatedCheckedItems[boundaryType] = [];
          }
          updatedCheckedItems[boundaryType].push(boundary.id);
        }
      });
      setCheckedItems(updatedCheckedItems);
    }
  }, [dashboardFilters.boundaries, boundariesData]);

  return (
    <Stack
      alignItems={"flex-end"}
      direction={"row"}
      justifyContent={"flex-end"}
      spacing={1}
      mt={1}
      data-cy="dashboard-selector-stack"
      position={"fixed"}
      left={isDesktop ? 60 : 10}
      zIndex={50}
    >
      {boundariesData && boundariesData.length > 0 && (
        <Box
          sx={{
            width: "auto",
            display: "flex",
            alignItems: "flex-start",
            flexDirection: "column",
          }}
          data-cy="dashboard-selector-autocomplete-container"
        >
          <Tooltip title={getTooltipContent()}>
            <Badge
              badgeContent={isExpanded ? totalChecked : null}
              color="primary"
              anchorOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              sx={{
                ".MuiBadge-badge": {
                  left: 10,
                  top: 3,
                },
              }}
            >
              <Button
                variant="outlined"
                color="primary"
                size="medium"
                onClick={handleIsExpanded}
                data-cy="dashboard-selector-autocomplete-label"
                sx={{
                  padding: "8px 16px 8px 22px",
                  border: "1px solid #2FA37C",
                  borderRadius: isExpanded ? "50px" : "28px 28px 0 0",
                  borderBottom: isExpanded ? "1px solid #2FA37C" : "0",
                  backgroundColor: "#fff",
                  "&:hover": {
                    backgroundColor: "#EAFAF1",
                    borderColor: "primary.main",
                  },
                }}
              >
                {t("Dashboard.allBoundaries")}
                {isExpanded ? (
                  <ExpandMoreIcon sx={{ ml: 1 }} />
                ) : (
                  <ExpandLessIcon sx={{ ml: 1 }} />
                )}
              </Button>
            </Badge>
          </Tooltip>

          <Box
            sx={{
              display: isExpanded ? "none" : "flex",
              position: "relative",
              overflow: isTablet ? "auto" : "hidden",
              width: "auto",
              height: "250px",
              alignItems: "center",
              flexDirection: "row",
              backgroundColor: "white",
              border: "1px solid #2FA37C",
              borderRadius: "0 6px 6px 6px",
              boxShadow: 6,
              flexWrap: "wrap",
            }}
          >
            <CloseIcon
              onClick={clearAllCheckboxes}
              sx={{
                display: totalChecked ? "block" : "none",
                cursor: "pointer",
                position: "absolute",
                top: 20,
                right: 20,
                zIndex: 2,
              }}
            />
            {groupedBoundaries &&
              Object.keys(groupedBoundaries).map((type, index, array) => (
                <Box sx={{ height: "100%" }} key={type}>
                  <FormControl
                    key={type}
                    sx={{ m: 3 }}
                    component="fieldset"
                    variant="standard"
                  >
                    <FormLabel component="legend" sx={{ mb: 1 }}>
                      {t(`components.LocationForm.properties.${type}`)}
                    </FormLabel>
                    <FormGroup
                      sx={{
                        height: isTablet ? "auto" : "170px",
                      }}
                    >
                      {groupedBoundaries[type].map((boundary) => (
                        <FormControlLabel
                          key={boundary.id}
                          control={
                            <Checkbox
                              checked={
                                checkedItems[type]?.includes(boundary.id) ||
                                false
                              }
                              onChange={() =>
                                handleCheckboxChange(type, boundary.id)
                              }
                              name={boundary.name}
                              value={boundary.id}
                            />
                          }
                          label={boundary.name}
                          sx={{ width: isTablet ? "auto" : "200px" }}
                        />
                      ))}
                    </FormGroup>
                  </FormControl>
                  {index < array.length - 1 && (
                    <Box
                      sx={{
                        border: "0.1px solid rgba(235,235,235,.6)",
                        height: "195px",
                        width: ".1px",
                        mx: 4,
                      }}
                    ></Box>
                  )}
                </Box>
              ))}
          </Box>
        </Box>
      )}
    </Stack>
  );
};

export default SelectorSection;
