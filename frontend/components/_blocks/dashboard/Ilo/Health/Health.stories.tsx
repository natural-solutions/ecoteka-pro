import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Health from "./Health";
import { Rating } from "@mui/material";

const meta = {
  title: "Blocks/Dashboard/Ilo/Health",
  component: Health,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Health",
      },
    },
  },
} satisfies Meta<typeof Health>;
export default meta;
type Story = StoryObj<typeof Health>;

const dataFixture = [
  {
    component: <Rating name="read-only" value={3} readOnly />,
    value: 3,
    unit: "/5",
    title: "Bien-être",
  },
  {
    component: <Rating name="read-only" value={3} readOnly />,
    value: 3,
    unit: "/5",
    title: "Caractère non-allergène",
  },
];
export const Default: Story = {
  render: () => <Health data={dataFixture} />,
};
