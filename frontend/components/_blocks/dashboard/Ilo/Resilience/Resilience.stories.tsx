import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Resilience from "./Resilience";
import { Rating } from "@mui/material";

const meta = {
  title: "Blocks/Dashboard/Ilo/Resilience",
  component: Resilience,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Resilience",
      },
    },
  },
} satisfies Meta<typeof Resilience>;
export default meta;
type Story = StoryObj<typeof Resilience>;

const dataFixture = [
  {
    component: <Rating name="read-only" value={3} readOnly />,
    value: 3,
    unit: "/5",
    title: "Résilience",
  },
];
export const Default: Story = {
  render: () => <Resilience data={dataFixture} />,
};
