export type TAggregatedTaxonomy = {
  scientific_name: string;
  count: number;
};
export interface ISpeciesDistributionProps {
  data: any;
}
