import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import SpeciesDistribution from "@blocks/dashboard/SpeciesDistribution";

const meta = {
  title: "Blocks/Dashboard/SpeciesDistribution",
  component: SpeciesDistribution,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Species distribution component",
      },
    },
  },
} satisfies Meta<typeof SpeciesDistribution>;

const allTreesCount = 200;

const speciesAggData = [
  {
    scientific_name: "Acer campestre",
    count: 500,
  },
  {
    scientific_name: "Betula alba",
    count: 5000,
  },
  {
    scientific_name: "Celtis orientalis",
    count: 15000,
  },
];

export default meta;
type Story = StoryObj<typeof SpeciesDistribution>;

export const SpeciesDistributionSection: Story = {
  render: () => (
    <Box sx={{ with: "100px", height: "300px" }}>
      <SpeciesDistribution data={speciesAggData} />
    </Box>
  ),
};
