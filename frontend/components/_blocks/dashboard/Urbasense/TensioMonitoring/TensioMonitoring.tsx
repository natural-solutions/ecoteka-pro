/** MUI Import */
import Box from "@mui/material/Box";
import { styled } from "@mui/material/styles";
import TimelineDot from "@mui/lab/TimelineDot";
import TimelineItem from "@mui/lab/TimelineItem";
import Typography from "@mui/material/Typography";
import TimelineContent from "@mui/lab/TimelineContent";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import TimelineConnector from "@mui/lab/TimelineConnector";
import MuiTimeline, { TimelineProps } from "@mui/lab/Timeline";
import Grid from "@mui/material/Unstable_Grid2";
import { grey } from "@mui/material/colors";

/** Date manipulation */
import format from "date-fns/format";
import { formatDistanceToNow, parse } from "date-fns";
import { fr } from "date-fns/locale";

/** Custom Components */
import { TreeIcon, SoilWater } from "@core/icons";
import Avatar from "@core/avatar";

/** Components Types */
import { useTranslation } from "react-i18next";
import { useElementSize } from "usehooks-ts";
import Alert from "@mui/material/Alert";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import { Paper } from "@mui/material";
import { convertDatetimeToDate } from "@lib/utils";

// Styled Timeline component
const Timeline = styled(MuiTimeline)<TimelineProps>({
  paddingLeft: 0,
  paddingRight: 0,
  "& .MuiTimelineItem-root": {
    width: "100%",
    "&:before": {
      display: "none",
    },
  },
});

const TensioMonitoring = ({ data, loading, error }) => {
  const { t } = useTranslation(["components"]);
  const nextWateringValue = data?.arrosageProchain?.valeur;
  const lastWateringValue = data?.arrosageDernier?.valeur;
  const nbSubjectsCount = data?.nbSujets?.valeur;
  const level =
    ((data?.rurMax?.valeur ?? 0) + (data?.rurMin?.valeur ?? 100)) / 2;

  const nextWateringDate = convertDatetimeToDate(nextWateringValue);
  const lastWateringDate = convertDatetimeToDate(lastWateringValue);

  const [widgetRef, { width }] = useElementSize();

  if (error) {
    return <Alert severity="info">{t("Dashboard.noData")}</Alert>;
  }

  if (data) {
    return (
      <>
        <DashboardModuleHeader
          title={t("dashboard.urbasense.tensioMonitoring.title")}
        />
        <Paper
          elevation={1}
          id="soilwater"
          ref={widgetRef}
          data-cy="dashboard-urbasense-analytics-tensio-component"
        >
          <Box sx={{ flexGrow: 1, p: 3 }}>
            <Grid container spacing={8}>
              <Grid xs={12} sm={12} md={6}>
                <Typography
                  data-cy="dashboard-urbasense-analytics-tensio-watering-title"
                  sx={{
                    fontWeight: "bold",
                    color: grey[500],
                    mb: 2,
                  }}
                >
                  {t("dashboard.urbasense.tensioMonitoring.watering")}
                </Typography>
                <Box sx={{ display: "flex", alignItems: "center" }}>
                  <Avatar
                    variant="rounded"
                    color="success"
                    sx={{
                      mr: 3,
                      boxShadow: 3,
                      width: 44,
                      height: 44,
                      "& svg": { fontSize: "1.75rem" },
                    }}
                  >
                    <TreeIcon />
                  </Avatar>
                  <Box sx={{ display: "flex", flexDirection: "column" }}>
                    <Typography
                      variant="caption"
                      data-cy="dashboard-urbasense-analytics-tensio-subjects"
                    >
                      {t("dashboard.urbasense.tensioMonitoring.nbSubjects")}
                    </Typography>
                    <Typography
                      variant="h6"
                      data-cy="dashboard-urbasense-analytics-tensio-subjectsCount"
                    >
                      {nbSubjectsCount}
                    </Typography>
                  </Box>
                </Box>
                <Timeline sx={{ my: 0, py: 0 }}>
                  <TimelineItem>
                    <TimelineSeparator>
                      <TimelineDot
                        sx={{
                          backgroundColor: "#06B6AE",
                          boxShadow: "0 0 0 4px rgba(6, 182, 174, 0.12)",
                        }}
                      />
                      <TimelineConnector />
                    </TimelineSeparator>
                    <TimelineContent
                      sx={{
                        pr: 0,
                        mt: 0,
                        mb: (theme) => `${theme.spacing(1.5)} !important`,
                      }}
                    >
                      <Box
                        sx={{
                          mb: 2.5,
                          display: "flex",
                          flexWrap: "wrap",
                          alignItems: "center",
                          justifyContent: "space-between",
                        }}
                      >
                        <Typography
                          sx={{
                            mr: 2,
                            fontWeight: 600,
                            color: "text.primary",
                          }}
                        >
                          {nextWateringDate &&
                            format(new Date(nextWateringDate), "P", {
                              locale: fr,
                            })}
                        </Typography>
                        <Typography
                          variant="caption"
                          sx={{ color: "text.disabled" }}
                        >
                          {nextWateringDate &&
                            formatDistanceToNow(new Date(nextWateringDate), {
                              locale: fr,
                              addSuffix: true,
                            })}
                        </Typography>
                      </Box>
                      <Typography variant="body2">
                        {t(
                          "dashboard.urbasense.tensioMonitoring.nextSuggestedWatering"
                        )}
                      </Typography>
                    </TimelineContent>
                  </TimelineItem>

                  <TimelineItem>
                    <TimelineSeparator>
                      <TimelineDot
                        sx={{
                          backgroundColor: "#9155FD",
                          boxShadow: "0 0 0 4px rgba(145, 85, 253, 0.12)",
                        }}
                      />
                      <TimelineConnector />
                    </TimelineSeparator>
                    <TimelineContent
                      sx={{
                        pr: 0,
                        mt: 0,
                        mb: (theme) => `${theme.spacing(1.5)} !important`,
                      }}
                    >
                      <Box
                        sx={{
                          mb: 2.5,
                          display: "flex",
                          flexWrap: "wrap",
                          alignItems: "center",
                          justifyContent: "space-between",
                        }}
                      >
                        <Typography
                          sx={{
                            mr: 2,
                            fontWeight: 600,
                            color: "text.primary",
                          }}
                        >
                          {lastWateringDate &&
                            format(new Date(lastWateringDate), "P", {
                              locale: fr,
                            })}
                        </Typography>
                        <Typography
                          variant="caption"
                          sx={{ color: "text.disabled" }}
                        >
                          {lastWateringDate &&
                            formatDistanceToNow(new Date(lastWateringDate), {
                              locale: fr,
                              addSuffix: true,
                            })}
                        </Typography>
                      </Box>
                      <Typography variant="body2" sx={{ mb: 2 }}>
                        {t(
                          "dashboard.urbasense.tensioMonitoring.lastMonitoredWatering"
                        )}
                      </Typography>
                    </TimelineContent>
                  </TimelineItem>
                </Timeline>
              </Grid>
              <Grid xs={12} sm={6}>
                <Typography
                  sx={{
                    fontWeight: "bold",
                    color: grey[500],
                    mb: 2,
                  }}
                  data-cy="dashboard-urbasense-analytics-tensio-waterAvailability-title"
                >
                  {t("dashboard.urbasense.tensioMonitoring.waterAvailability")}
                </Typography>
                <Box
                  sx={{
                    flexGrow: 1,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Box>
                    <SoilWater level={level} />
                  </Box>
                  <Box sx={{}}>
                    <Typography
                      variant="h6"
                      component="p"
                      sx={{
                        fontFamily: "Roboto",
                        fontWeight: "bold",
                        textAlign: "center",
                      }}
                    >
                      {level}%
                    </Typography>
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Paper>
      </>
    );
  } else {
    return (
      <Alert severity="info" data-cy="alert">
        Aucune donnée
      </Alert>
    );
  }
};

export default TensioMonitoring;
