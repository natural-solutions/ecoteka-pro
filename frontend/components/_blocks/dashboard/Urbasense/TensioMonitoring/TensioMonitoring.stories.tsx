import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import TensioMonitoring from "./TensioMonitoring";

const meta = {
  title: "Blocks/Dashboard/Urbasense/TensioMonitoring",
  component: TensioMonitoring,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Tensio monitoriing",
      },
    },
  },
} satisfies Meta<typeof TensioMonitoring>;

const tensioMonitoringData = {
  arrosageDernier: {
    badge: "alerte",
    couleur: "#d54927",
    nom: "Nombre de jours depuis le dernier arrosage",
    tooltip:
      "Nombre de jours depuis le dernier arrosage observé parmi les sujets sélectionnés.",
    unite: "date",
    valeur: "2023-07-09",
  },
  arrosageProchain: {
    badge: "alerte",
    couleur: "#d54927",
    nom: "Nombre de jours avant le prochain arrosage",
    tooltip:
      "Nombre de jours avant le prochain arrosage nécessaire par l'un des sujets sélectionnés.",
    unite: "date",
    valeur: "2023-07-22",
  },
  error: null,
  listeParams: [
    "nbSujets",
    "rurMin",
    "rurMax",
    "arrosageProchain",
    "arrosageDernier",
  ],
  nbSujets: {
    badge: "ok",
    couleur: "#55c277",
    nom: "Nombre de sujets",
    tooltip: "Nombre de sujets concernés par la requête.",
    unite: "sujets",
    valeur: 10,
  },
  rurMax: {
    badge: "ok",
    couleur: "#55c277",
    nom: "Réserve Utile aux Racines maximale",
    tooltip:
      "Remplissage de la réserve d'eau utilisable par les racines la plus élevée parmi les sujets sélectionnés.",
    unite: "%",
    valeur: 89,
  },
  rurMin: {
    badge: "ok",
    couleur: "#55c277",
    nom: "Réserve Utile aux Racines minimale",
    tooltip:
      "Remplissage de la réserve d'eau utilisable par les racines la plus faible parmi les sujets sélectionnés.",
    unite: "%",
    valeur: 20,
  },
  status: null,
};

export default meta;
type Story = StoryObj<typeof TensioMonitoring>;

export const AnalyticsDendro: Story = {
  render: () => (
    <Box>
      <TensioMonitoring
        data={tensioMonitoringData}
        loading={false}
        error={undefined}
      />
    </Box>
  ),
};
