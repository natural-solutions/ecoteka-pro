export interface UrbasenseApiMetricsRoot {
  data: Data;
}

export interface Data {
  urbasenseGetMetricsByGroup: UrbasenseGetMetricsByGroup;
}

export interface UrbasenseGetMetricsByGroup {
  dendro: Dendro;
  meteo: Meteo;
  tensio: Tensio;
}

export interface Dendro {
  msg: DendroMsg;
}

export interface DendroMsg {
  croissanceMoyMois: CroissanceMoyMois;
  amcMoyJour: AmcMoyJour;
  error: any;
  status: any;
  stressThermMoyJour: StressThermMoyJour;
}

export interface CroissanceMoyMois {
  badge: string;
  couleur: string;
  nom: string;
  tooltip: string;
  unite: string;
  valeur: number;
}

export interface AmcMoyJour {
  badge: string;
  couleur: string;
  nom: string;
  tooltip: string;
  unite: string;
  valeur: number;
}

export interface StressThermMoyJour {
  badge: string;
  couleur: string;
  nom: string;
  tooltip: string;
  unite: string;
  valeur: number;
}

export interface Meteo {
  msgMeteo: Msg2;
}

export interface Msg2 {
  utci: Utci;
  error: any;
  humAirMoy: HumAirMoy;
  listeParams: string[];
  nbSujets: NbSujets;
  status: any;
  tempAirMax: TempAirMax;
  vitVentMax: VitVentMax;
}

export interface Utci {
  abbrege: string;
  dates: string[];
  nom: string;
  tooltip: string;
  unite: string;
  valeurs: number[];
  valeursParSujet: ValeursParSujet;
}

export interface HumAirMoy {
  abbrege: string;
  dates: string[];
  nom: string;
  tooltip: string;
  unite: string;
  valeurs: number[];
  valeursParSujet: ValeursParSujet;
}

type ValeursParSujet = {
  [key: string]: number[] | null | undefined;
};

export interface NbSujets {
  abbrege: string;
  dates: any;
  nom: string;
  tooltip: string;
  unite: string;
  valeurs: any;
}

export interface TempAirMax {
  abbrege: string;
  dates: string[];
  nom: string;
  tooltip: string;
  unite: string;
  valeurs: number[];
  valeursParSujet: ValeursParSujet;
}

export interface VitVentMax {
  abbrege: string;
  dates: string[];
  nom: string;
  tooltip: string;
  unite: string;
  valeurs: number[];
  valeursParSujet: ValeursParSujet;
}

export interface Tensio {
  msg: Msg3;
}

export interface Msg3 {
  arrosageDernier: ArrosageDernier;
  arrosageProchain: ArrosageProchain;
  error: any;
  listeParams: string[];
  nbSujets: NbSujets2;
  rurMax: RurMax;
  rurMin: RurMin;
  status: any;
}

export interface ArrosageDernier {
  badge: string;
  couleur: string;
  nom: string;
  tooltip: string;
  unite: string;
  valeur: number;
}

export interface ArrosageProchain {
  badge: string;
  couleur: string;
  nom: string;
  tooltip: string;
  unite: string;
  valeur: number;
}

export interface NbSujets2 {
  badge: string;
  couleur: string;
  nom: string;
  tooltip: string;
  unite: string;
}

export interface RurMax {
  badge: string;
  couleur: string;
  nom: string;
  tooltip: string;
  unite: string;
  valeur: number;
}

export interface RurMin {
  badge: string;
  couleur: string;
  nom: string;
  tooltip: string;
  unite: string;
  valeur: number;
}
