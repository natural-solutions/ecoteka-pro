import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import Box from "@mui/material/Box";
import AnalyticsDendroSection from "@components/_blocks/dashboard/AnalyticsDendroSection";

const meta = {
  title: "Blocks/Dashboard/AnalyticsDendroSection",
  component: AnalyticsDendroSection,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Analytics dendro section",
      },
    },
  },
} satisfies Meta<typeof AnalyticsDendroSection>;

const analyticsDendroValues = {
  amcMoyJour: {
    nom: "Dashboard.analyticsDendro.amc",
    valeur: 0.014,
    unite: "mm",
    badge: "ok",
    tooltip: "ok",
  },
  stressThermMoyJour: {
    nom: "Dashboard.analyticsDendro.thermalStress",
    valeur: 12,
    unite: "mm",
    badge: "alerte",
    tooltip: "alerte",
  },
  croissanceMoyMois: {
    nom: "Dashboard.analyticsDendro.dailyGrowth",
    valeur: 0.021,
    unite: "",
    badge: "vigilance",
    tooltip: "vigilance",
  },
};

export default meta;
type Story = StoryObj<typeof AnalyticsDendroSection>;

export const AnalyticsDendro: Story = {
  render: () => (
    <Box>
      <AnalyticsDendroSection data={analyticsDendroValues} error={false} />
    </Box>
  ),
};
