import { FC } from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import CircleIcon from "@mui/icons-material/Circle";
import { useTranslation } from "react-i18next";
import { DendroMessage, MetricMessage } from "types/analytics/analytics.types";
import { omit } from "lodash";
import Alert from "@mui/material/Alert";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import { Paper } from "@mui/material";
import { DendroIcon } from "@components/icons/DendroIcon";

interface IAnalyticsDendroSectionProps {
  data: DendroMessage | undefined;
  loading?: boolean;
  error: boolean;
}

const AnalyticsDendroSection: FC<IAnalyticsDendroSectionProps> = ({
  data,
  error,
}) => {
  const { t } = useTranslation(["components"]);
  const formatLabel = (value: string): string => {
    const localesKeyRegex = /[.:]/;
    return localesKeyRegex.test(value) ? t(value) : value;
  };

  const colorsBadges = {
    ok: "#5FCA74",
    nd: "#757575",
    alerte: "#E31B0C",
    vigilance: "#FF9800",
  };

  const renderStats = (dendroData: DendroMessage) => {
    return Object.entries(omit(dendroData, "listeParams")).map(
      ([key, value]: [string, MetricMessage]) => (
        <Grid item xs={12} sm={12} lg={6} key={key}>
          <Box sx={{ display: "flex", flexDirection: "column" }}>
            <Typography variant="caption" fontSize={16} color="#89868D">
              {formatLabel(value.nom!)}
            </Typography>
            <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
              <CircleIcon sx={{ color: colorsBadges[value.badge!] }} />
              <Typography color="#89868D" variant="h4" fontWeight="bold">
                {value.valeur} {value.unite}
              </Typography>
            </Box>
          </Box>
        </Grid>
      )
    );
  };

  if (error) {
    return <Alert severity="info">{t("Dashboard.noData")}</Alert>;
  }

  if (data) {
    return (
      <>
        <DashboardModuleHeader title="Dendro" />
        <Paper
          elevation={1}
          id="dendro"
          data-cy="dashboard-urbasense-analytics-dendro-component"
        >
          <Box sx={{ flexGrow: 1, p: 3 }}>
            <Grid
              container
              alignItems="baseline"
              flexWrap="wrap"
              spacing={1}
              columns={12}
              data-cy="dashboard-urbasense-analytics-dendro-stats"
            >
              {renderStats(data)}
            </Grid>
            <Grid
              container
              columns={12}
              direction="row"
              alignItems={"flex-end"}
            >
              <Grid item xs={6} md={6}>
                <Grid container columns={4}>
                  {Object.entries(colorsBadges).map(([color, value]) => (
                    <Grid
                      item
                      xs={4}
                      md={2}
                      key={color}
                      data-cy={`dashboard-urbasense-analytics-dendro-badge-${color}`}
                    >
                      <Box
                        sx={{ display: "flex", alignItems: "center", gap: 1 }}
                      >
                        <CircleIcon sx={{ color: value, ml: 1 }} />
                        <Typography
                          color="#89868D"
                          data-cy={`dashboard-urbasense-analytics-dendro-badge-label-${color}`}
                        >
                          {formatLabel(
                            `Dashboard.analyticsDendro.caption.${color}`
                          )}
                        </Typography>
                      </Box>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
              <Grid
                item
                xs={6}
                sx={{ textAlign: "center" }}
                data-cy="dashboard-urbasense-analytics-dendro-icon"
              >
                <DendroIcon />
              </Grid>
            </Grid>
          </Box>
        </Paper>
      </>
    );
  } else {
    return <Alert severity="error">{t("Core.Error.message.unexpected")}</Alert>;
  }
};

export default AnalyticsDendroSection;
