/** Components Types */
import React, { useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import { useResizeObserver } from "usehooks-ts";
import Alert from "@mui/material/Alert";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import { Paper, Box, Tooltip, Typography } from "@mui/material";
import InfoIcon from "@mui/icons-material/Info";
import {
  getAllFamiliesInterventionType,
  getLateInterventionByFamily,
  LateInterventionsByFamilyId,
} from "@services/AnalyticsService";
import { useRouter } from "next/router";
import { useDeviceSize } from "@lib/utils";
import { useDashboardFilters } from "@stores/pages/dashboard";
import { useSession } from "next-auth/react";
import EchartWidget from "@components/_blocks/dashboard/EchartWidget";

export interface PieChartData {
  family_slug: any;
  id: string;
  label: string;
  value: number;
  color: string;
}

export interface LegendData {
  family_id: string;
  family_slug: string;
  family_color: number;
}

export interface LateInterventionData {
  family_name: string;
  family_color: string;
  intervention_type: string;
  intervention_id: string;
  intervention_type_id: string;
  scheduled_date: string;
  tree_id: string | null;
}

interface LateInterventionProps {
  data: LateInterventionData[];
}

const LateIntervention = ({ data }: LateInterventionProps) => {
  const { t } = useTranslation(["components", "common"]);
  const ref = useRef<HTMLDivElement>(null);
  const { width = 0, height = 0 } = useResizeObserver({
    ref,
    box: "border-box",
  });
  const { isMobile } = useDeviceSize();
  const [pieData, setPieData] = useState<PieChartData[]>();
  const [legendData, setLegendData] = useState<LegendData[]>();
  const router = useRouter();
  const session = useSession();
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";
  const { boundaries } = useDashboardFilters();

  const handleSliceClick = (itemId: string, itemLabel: string) => {
    if (itemId && itemLabel) {
      router.push({
        pathname: "/intervention",
        query: { object: "all", type: itemId, state: itemLabel },
      });
    }
  };

  const getEChartsOption = () => {
    return {
      tooltip: {
        trigger: "item",
        formatter: "{b}: {c} ({d}%)",
      },
      legend: {
        orient: "vertical",
        right: "10%",
        top: isMobile ? "20%" : "30%",
        data: pieData?.map((d) => d.label),
        textStyle: {
          color: "#999",
        },
        formatter: (name) => {
          const maxLineLength = 15; // Limite de caractères par ligne
          const words = name.split(" "); // Divise le nom en mots
          let formattedName = "";
          let line = "";

          // Ajoute les mots à une ligne jusqu'à atteindre la limite de longueur
          words.forEach((word) => {
            if ((line + word).length > maxLineLength) {
              formattedName += line.trim() + "\n"; // Ajoute la ligne et un saut de ligne
              line = word + " "; // Commence une nouvelle ligne avec le mot actuel
            } else {
              line += word + " "; // Ajoute le mot à la ligne actuelle
            }
          });

          // Ajoute la dernière ligne si elle existe
          if (line) {
            formattedName += line.trim();
          }

          return formattedName; // Retourne le nom formaté
        },
      },
      series: [
        {
          name: t("Dashboard.interventions.family.name"),
          type: "pie",
          radius: ["60%", "70%"],
          avoidLabelOverlap: false,
          center: ["30%", "50%"],
          itemStyle: {
            borderRadius: 8,
            borderColor: "#fff",
            borderWidth: 2,
          },
          label: {
            show: false,
          },
          emphasis: {
            label: {
              width: 100,
              show: true,
              fontSize: 13,
              fontWeight: "bold",
              overflow: "break", // Permettre le retour à la ligne
            },
          },

          data: pieData?.map((d) => ({
            name: d.label,
            value: d.value,
            itemStyle: {
              color: d.color,
            },
            family_slug: d.family_slug,
          })),
        },
      ],
    };
  };

  useEffect(() => {
    const fetchFilterData = async (params) => {
      try {
        const response: LateInterventionsByFamilyId =
          await getLateInterventionByFamily(params, currentUserToken);

        const pieChartData = Object.entries(response).map(
          ([familyId, interventions]) => {
            return {
              id: t(
                `Dashboard.interventions.family.${interventions[0].family_name}`
              ),
              label: t(
                `Dashboard.interventions.family.${interventions[0].family_name}`
              ),
              value: interventions.length,
              color: interventions[0].family_color,
              family_slug: interventions[0].family_name,
            };
          }
        );
        setPieData(pieChartData);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des données de filtre",
          error
        );
      }
    };

    const fetchDataForLegend = async () => {
      try {
        const response = await getAllFamiliesInterventionType(currentUserToken);
        setLegendData(response);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des données de filtre",
          error
        );
      }
    };

    fetchFilterData(boundaries);
    fetchDataForLegend();
  }, [boundaries]);

  if (data) {
    return (
      <>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            px: 1,
          }}
        >
          <DashboardModuleHeader
            title={t("Template.menuItems.dashboard.lateIntervention")}
          />
          <Tooltip title={t("Template.menuItems.dashboard.lateIntervention")}>
            <InfoIcon />
          </Tooltip>
        </Box>
        <Paper
          elevation={1}
          ref={ref}
          data-cy="dashboard-late-intervention"
          sx={{ height: "20vh" }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              width: "100%",
              height: "100%",
              position: "relative",
              cursor: "pointer",
            }}
          >
            {pieData && pieData.length > 0 && legendData ? (
              <>
                <Typography
                  sx={{
                    position: "absolute",
                    top: isMobile ? "0px" : "20px",
                    right: isMobile ? "10px" : "58px",
                  }}
                >
                  {t("Dashboard.interventions.family.name")}
                </Typography>
                <EchartWidget
                  config={getEChartsOption()}
                  error={!pieData || pieData.length === 0}
                  customStyle={{
                    height: "100%",
                    width: "100%",
                  }}
                  onEvents={{
                    click: (params) => {
                      if (
                        params.componentType === "series" &&
                        params.seriesType === "pie"
                      ) {
                        const clickedSliceValue = params.data.family_slug; // Récupérer la valeur de la tranche cliquée
                        handleSliceClick(clickedSliceValue, t("common.late")); // Appel à votre méthode
                      }
                    },
                  }}
                />
              </>
            ) : (
              <Typography>{t("Dashboard.interventions.notLate")}</Typography>
            )}
          </Box>
        </Paper>
      </>
    );
  } else {
    return (
      <Alert severity="error" data-cy="alert">
        {t("Core.Error.message.unexpected")}
      </Alert>
    );
  }
};

export default LateIntervention;
