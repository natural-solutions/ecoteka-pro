import { ApolloError } from "@apollo/client";
import {
  FetchAllTreesNotableQuery,
  FetchDangerousConditionQuery,
} from "@generated/graphql";

export interface ITreeInformationProps {
  data: FetchDangerousConditionQuery | FetchAllTreesNotableQuery | undefined;
}
