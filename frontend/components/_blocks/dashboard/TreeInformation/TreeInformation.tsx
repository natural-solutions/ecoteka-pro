/** MUI Import */
import { Box, Tooltip, Typography } from "@mui/material";
import InfoIcon from "@mui/icons-material/Info";

/** Components Types */
import { useTranslation } from "react-i18next";
import { useElementSize } from "usehooks-ts";
import Alert from "@mui/material/Alert";
import DashboardModuleHeader from "@components/_core/headers/DashboardModuleHeader";
import { Paper } from "@mui/material";
import NotableTreeSVG from "@components/_core/icons/Tree/NotableTree";
import { useEffect, useState } from "react";
import { NotableTree } from "types/analytics/analytics.types";

const TreeInformation = ({ data }) => {
  const { t } = useTranslation(["components"]);
  const [widgetRef, { width }] = useElementSize();
  const [treeNotable, setTreeNotable] = useState<number>();

  useEffect(() => {
    const queryData = data.length;
    if (data) {
      const formattedNumber = parseInt(
        queryData.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "),
        10
      );
      setTreeNotable(formattedNumber);
    }
  }, [data]);

  if (data) {
    return (
      <>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            px: 1,
          }}
        >
          <DashboardModuleHeader
            title={t("Template.menuItems.dashboard.treeHeritage")}
          />
          <Tooltip title={t("Template.menuItems.dashboard.treeHeritage")}>
            <InfoIcon />
          </Tooltip>
        </Box>
        <Paper
          elevation={1}
          id="soilwater"
          ref={widgetRef}
          data-cy="dashboard-tree-heritage-component"
          sx={{
            p: 3,
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            px: 1,
            pl: 2,
            height: "20vh",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "flex-end",
              width: "100%",
            }}
          >
            <Box
              sx={{
                maxWidth: "35%",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                pb: 1.5,
              }}
            >
              <NotableTreeSVG />
              <Typography
                sx={{ maxWidth: "80%", fontSize: 15, textAlign: "center" }}
              >
                {treeNotable + " " + t("Dashboard.treeHeritage.notable")}
              </Typography>
            </Box>
          </Box>
        </Paper>
      </>
    );
  } else {
    return (
      <Alert severity="error" data-cy="alert">
        {t("Core.Error.message.unexpected")}
      </Alert>
    );
  }
};

export default TreeInformation;
