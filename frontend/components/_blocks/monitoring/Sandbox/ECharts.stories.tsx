import React from "react";
import type { Meta, StoryObj } from "@storybook/react";
import ECharts from "./ECharts";

const meta = {
  title: "Blocks/Monitoring/Sandbox/ECharts",
  component: ECharts,
  parameters: {
    docs: {
      description: {
        component: "ECharts",
      },
    },
  },
} satisfies Meta<typeof ECharts>;
export default meta;
type Story = StoryObj<typeof ECharts>;
export const Default: Story = {
  render: () => <ECharts />,
};
