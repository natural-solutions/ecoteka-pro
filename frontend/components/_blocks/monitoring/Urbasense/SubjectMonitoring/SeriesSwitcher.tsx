import { useState } from "react";
import { Switch } from "@mui/material";
import { alpha, styled } from "@mui/material/styles";
import { blue } from "@mui/material/colors";
import type { SwitchPropsColorOverrides } from "@mui/material";
import { OverridableStringUnion } from "@mui/types";

export interface ISeriesSwitcherProps {
  checked: boolean;
  color?:
    | OverridableStringUnion<
        | "default"
        | "primary"
        | "secondary"
        | "error"
        | "info"
        | "success"
        | "warning",
        SwitchPropsColorOverrides
      >
    | undefined
    | "pp";
  onChangeCallback: () => void;
}

const PPSwitch = styled(Switch)(({ theme }) => ({
  "& .MuiSwitch-switchBase.Mui-checked": {
    color: blue[900],
    "&:hover": {
      backgroundColor: alpha(blue[900], theme.palette.action.hoverOpacity),
    },
  },
  "& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track": {
    backgroundColor: blue[900],
  },
}));

const SeriesSwitcher = ({
  checked,
  color = "primary",
  onChangeCallback,
}: ISeriesSwitcherProps) => {
  const [isChecked, setChecked] = useState(checked);
  const handleOnChange = () => {
    onChangeCallback();
    setChecked(!isChecked);
  };
  // JSX here
  if (color === "pp") {
    return (
      <PPSwitch checked={isChecked} onChange={handleOnChange} size="small" />
    );
  } else {
    return (
      <Switch
        checked={isChecked}
        onChange={handleOnChange}
        size="small"
        color={color}
      />
    );
  }
};
export default SeriesSwitcher;
