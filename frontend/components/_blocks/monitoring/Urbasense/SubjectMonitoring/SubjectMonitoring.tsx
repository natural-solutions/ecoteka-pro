import React, { useEffect, useRef, useState, useCallback } from "react";
import { Line, G2 } from "@antv/g2plot";
import * as _ from "@antv/util";
import insertCss from "insert-css";
import { listClasses } from "@mui/material";

function useStateCallback<T>(
  initialState: T
): [T, (state: T, cb?: (state: T) => void) => void] {
  const [state, setState] = useState(initialState);
  const cbRef = useRef<((state: T) => void) | undefined>(undefined); // init mutable ref container for callbacks

  const setStateCallback = useCallback((state: T, cb?: (state: T) => void) => {
    cbRef.current = cb; // store current, passed callback in ref
    setState(state);
  }, []); // keep object reference stable, exactly like `useState`

  useEffect(() => {
    // cb.current is `undefined` on initial render,
    // so we only invoke callback on state *updates*
    if (cbRef.current) {
      cbRef.current(state);
      cbRef.current = undefined; // reset callback after execution
    }
  }, [state]);

  return [state, setStateCallback];
}

export interface IMultiLineTimeSeries {
  Date: string;
  series: string;
  value: number;
  series_name: string;
  unite: string;
}

export interface ISubjectMonitoringProps {
  id?: string;
  chartData?: IMultiLineTimeSeries[];
}
const SubjectMonitoring = ({ id, chartData }: ISubjectMonitoringProps) => {
  const theme = G2.getTheme("light");
  const chartNodeRef = useRef(null);
  //   let chartRef = useRef(null);

  const [tooltipItems, setTooltipItems] = useState<any[]>([]);
  const [activeTooltipTitle, setActiveTooltipTitle] = useState(null);
  const [activeSeriesList, setActiveSeriesList] = useStateCallback<any[]>([]);
  const [data, setData] = useState<
    any[] | IMultiLineTimeSeries[] | null | undefined
  >(chartData);
  const [chart, setChart] = useState<any>(null);

  const any = (data: any): boolean => {
    if (Array.isArray(data)) {
      return data.length > 0;
    } else {
      return false;
    }
  };

  async function fetchData() {
    try {
      const response = await fetch(
        "https://gw.alipayobjects.com/os/antfincdn/3PtP0m%26VuK/trend-data.json"
      );
      const responseJson = await response.json();
      if (responseJson) {
        setData(responseJson);
      }
    } catch (error) {}
    // waits until the request completes...
  }

  const changeActiveSeries = (activeSeries) => {
    let newList: any[] = [];
    if (!activeSeriesList.includes(activeSeries)) {
      newList = [...activeSeriesList, activeSeries];
    } else {
      newList = activeSeriesList.filter((s) => s !== activeSeries);
    }
    setActiveSeriesList(newList, () => {
      // @ts-ignore
      if (chart.chart && activeSeries) {
        chart.chart.filter("series", (series) => {
          return newList.includes(series) ? false : true;
        });
        chart.chart.render(true);
        chart.chart.geometries
          .find((geom) => geom.type === "point")
          .elements.forEach((ele) => {
            const { Date, series } = ele.getModel().data;
            if (Date === activeTooltipTitle && series === activeSeries) {
              ele.setState("active", true);
            }
          });
      }
    });
  };

  const generateTooltip = () => {
    // @ts-ignore
    if (!chart) {
      return;
    }

    const { colors10 } = chart.chart.themeObject;
    return (
      <div className="g2-tooltip">
        <div className="g2-tooltip-title">{activeTooltipTitle}</div>
        <div className="g2-tooltip-items">
          {tooltipItems.map((item, idx) => {
            return (
              <div
                className={`g2-tooltip-item tooltip-${item.series} ${
                  activeSeriesList.includes(item.series) ? "inactive" : ""
                }`}
                onClick={() => changeActiveSeries(item.series)}
              >
                <div
                  className="g2-tooltip-item-marker"
                  style={{ background: colors10[idx] }}
                ></div>
                <div className="g2-tooltip-item-label">
                  {item.series_name ? item.series_name : item.series}
                </div>
                <div className="g2-tooltip-item-value">{item.value || "-"}</div>
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  useEffect(() => {
    if (!chartData) {
      fetchData();
    }
  }, []);

  // useEffect(() => {
  //   console.log("data :>> ", data);
  // }, [data]);

  // useEffect(() => {
  //   console.log("chart :>> ", chart);
  // }, [chart]);

  // useEffect(() => {
  //   console.log("tooltipItems :>> ", tooltipItems);
  // }, [tooltipItems]);

  useEffect(() => {
    if (chartNodeRef.current !== "loading" && data && any(data)) {
      /**
         * Clear chartRef
         *         if (this.chartRef) {
          this.chartRef?.current?.clear();
        }
         */
      // @ts-ignore
      const line = new Line(chartNodeRef?.current, {
        data,
        autoFit: true,
        xField: "Date",
        yField: "value",
        seriesField: "series",
        slider: {
          start: 0.1,
          end: 0.9,
        },
        xAxis: {
          type: "time",
          label: {
            autoRotate: false,
            formatter: (v) => {
              return v.split("/").reverse().join("-");
            },
          },
        },
        yAxis: {
          grid: {
            line: {
              style: {
                lineWidth: 0.5,
              },
            },
          },
        },
        meta: {
          Date: {
            range: [0.04, 0.96],
          },
        },
        point: {
          shape: "circle",
          size: 2,
          style: () => {
            return {
              fillOpacity: 0,
              stroke: "transparent",
            };
          },
        },
        appendPadding: [0, 0, 0, 0],
        legend: false,
        smooth: true,
        lineStyle: {
          lineWidth: 1.5,
        },
        tooltip: {
          showMarkers: false,
          follow: false,
          position: "top",
          customContent: () => null,
        },
        theme: _.deepMix({}, theme, {
          geometries: {
            point: {
              circle: {
                active: {
                  style: {
                    r: 4,
                    fillOpacity: 1,
                    stroke: "#000",
                    lineWidth: 1,
                  },
                },
              },
            },
          },
        }),
        colorField: "seriesField", // or seriesField in some cases
        color: ["red", "yellow", "orange", "teal"],
        interactions: [{ type: "marker-active" }],
      });
      line.render();
      let chartRef = line;
      setChart(line);
      const lastData = _.last(data);
      const point = line.chart.getXY(lastData);
      line.chart.showTooltip(point);
      const activeTitle = lastData.Date;
      if (data && any(data)) {
        const activeItems = data.filter((d) => d.Date === activeTooltipTitle);
        setTooltipItems(activeItems);
        setActiveTooltipTitle(activeTitle);
      }
      line.on("plot:mouseleave", () => {
        line.chart.hideTooltip();
      });
      line.on("tooltip:change", (evt) => {
        if (data && any(data)) {
          const item = _.last(evt.data.items);
          const { Date } = item.data;
          const changedItems = data.filter((d) => d.Date === Date);
          setTooltipItems(changedItems);
          setActiveTooltipTitle(Date);
        }
      });
    }
  }, [data]);

  return (
    <section className={"wrapper trend-wrapper"}>
      <div className={"chart-wrapper"} ref={chartNodeRef} />
      {data && any(data) && generateTooltip()}
    </section>
  );
};
export default SubjectMonitoring;
