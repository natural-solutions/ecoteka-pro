import React, { useEffect, useRef, useState, useCallback } from "react";
import { Line, Mix, G2, MixOptions, Axis } from "@antv/g2plot";
import * as _ from "@antv/util";
import {
  Button,
  FormControlLabel,
  FormGroup,
  Stack,
  Switch,
  listClasses,
  useTheme,
} from "@mui/material";
import { amber, blue, lightBlue, orange } from "@mui/material/colors";
import { PollTwoTone } from "@mui/icons-material";
import { format, formatDistance, subDays } from "date-fns";
import fr from "date-fns/locale/fr";
import SeriesSwitcher from "./SeriesSwitcher";

function useStateCallback<T>(
  initialState: T
): [T, (state: T, cb?: (state: T) => void) => void] {
  const [state, setState] = useState(initialState);
  const cbRef = useRef<((state: T) => void) | undefined>(undefined); // init mutable ref container for callbacks

  const setStateCallback = useCallback((state: T, cb?: (state: T) => void) => {
    cbRef.current = cb; // store current, passed callback in ref
    setState(state);
  }, []); // keep object reference stable, exactly like `useState`

  useEffect(() => {
    // cb.current is `undefined` on initial render,
    // so we only invoke callback on state *updates*
    if (cbRef.current) {
      cbRef.current(state);
      cbRef.current = undefined; // reset callback after execution
    }
  }, [state]);

  return [state, setStateCallback];
}

export interface Datum {
  date_mes: string;
  S_t?: number | null;
  S_1?: number | null;
  S_2?: number | null;
  S_3?: number | null;
  pp_mmjour?: number | null;
  tair_max?: number | null;
}

export interface Dimensions {
  id: string;
  alias: string;
  unite: string;
}

export interface ISubjectMonitoringProps {
  data: Datum[];
  metadata: Dimensions[];
}

function ComboPlot<T>({ data, metadata }: ISubjectMonitoringProps) {
  const chartNodeRef = React.useRef<HTMLDivElement | null>(null);
  const chartRef = React.useRef<Mix>();
  const theme = useTheme();
  const [displayGeomSwitches, setDisplay] = useState(false);
  const [seriesVisibility, setVisibility] = useState({
    tair_max: true,
    pp_mmjour: true,
    S_t: true,
    S_1: true,
    S_2: true,
    S_3: true,
  });

  const getTensioAlias = (device_id: string): string => {
    const infos = metadata.filter((m) => m.id === device_id);
    return infos.length > 0 ? infos[0].alias : device_id;
  };

  const textDisabled = theme.palette.text.disabled;
  const textPrimary = theme.palette.text.primary;

  const xAxisCmnCfg: Axis = {
    // type: "time",
    tickLine: {
      length: 8,
    },
    animate: true,
    label: {
      formatter: (text, item, index) => {
        return `${format(new Date(text), "P", { locale: fr })}`;
      },
      rotate: 12,
      offset: 24,
      style: {
        fill: textPrimary,
      },
    },
  };

  // TODO : should be dynamically defined based on
  const mixConfig: MixOptions = {
    appendPadding: 8,
    autoFit: true,
    syncViewPadding: true,
    tooltip: {
      shared: true,
      showCrosshairs: true,
    },
    slider: {
      start: 0.66,
      end: 1,
      textStyle: {
        fill: textPrimary,
      },
      formatter: (text) => {
        return formatDistance(new Date(text), new Date(), {
          addSuffix: true,
          locale: fr,
        });
      },
    },
    data,
    plots: [
      {
        type: "column",
        top: true,
        options: {
          xField: "date_mes",
          yAxis: false,
          xAxis: { ...xAxisCmnCfg },
          yField: "pp_mmjour",
          columnStyle: {
            fill: "l(270) 0:#032d54 0.5:#2a9aea 1:#99ebf6",
            width: 16,
          },
          // color: lightBlue[300],
          meta: {
            pp_mmjour: {
              alias: "Précipations journalières (mm)",
            },
          },
        },
      },
      {
        type: "line",
        // 共享顶层 data
        top: true,
        options: {
          xField: "date_mes",
          yField: "S_t",
          smooth: true,
          xAxis: { ...xAxisCmnCfg },
          yAxis: {
            position: "right",
            max: 50,
            title: {
              text: "Température (°C)",
              style: {
                fill: textDisabled,
              },
            },
            line: null,
            grid: null,
            tickLine: null,
          },
          label: {
            style: {
              fill: orange[500],
              opacity: 0.6,
              fontSize: 24,
            },
          },
          color: orange[500],
          meta: {
            S_t: {
              alias: getTensioAlias("S_t"),
            },
          },
        },
      },
      {
        type: "line",
        top: true,
        options: {
          xField: "date_mes",
          yField: "tair_max",
          xAxis: { ...xAxisCmnCfg },
          yAxis: {
            position: "right",
            max: 50,
            title: null,
            line: null,
            grid: null,
            tickLine: null,
          },
          smooth: true,
          color: orange[700],
          meta: {
            tair_max: {
              alias: getTensioAlias("tair_max"),
            },
          },
        },
      },
      {
        type: "line",
        // 共享顶层 data
        top: true,
        options: {
          xField: "date_mes",
          yField: "S_1",
          smooth: true,
          xAxis: { ...xAxisCmnCfg },
          yAxis: {
            position: "left",
            max: 100,
            // title: "Tensiométrie (centibar)",
            title: {
              text: "Tensiométrie (cb)",
              position: "center",
              style: {
                fill: textDisabled,
              },
            },
            line: null,
            grid: null,
            tickLine: null,
          },
          color: lightBlue[900],
          meta: {
            S_1: {
              alias: getTensioAlias("S_1"),
            },
          },
        },
      },
      {
        type: "line",
        // 共享顶层 data
        top: true,
        options: {
          xField: "date_mes",
          yField: "S_2",
          xAxis: { ...xAxisCmnCfg },
          yAxis: false,
          color: lightBlue[700],
          meta: {
            S_2: {
              alias: getTensioAlias("S_2"),
            },
          },
        },
      },
      {
        type: "line",
        top: true,
        options: {
          xField: "date_mes",
          yField: "S_3",
          xAxis: { ...xAxisCmnCfg },
          yAxis: false,
          meta: {
            S_3: {
              alias: getTensioAlias("S_3"),
            },
          },
          color: lightBlue[500],
        },
      },
    ],
  };

  /** Rendering charts */
  useEffect(() => {
    const chartDom = chartNodeRef?.current;
    if (!chartDom) return;

    let plot = chartRef.current;
    if (!plot) {
      plot = new Mix(chartDom, mixConfig);
      plot.render();
      chartRef.current = plot;
    } else {
      plot.update(mixConfig);
    }
  }, [mixConfig]);

  useEffect(() => {
    setDisplay(Boolean(chartRef?.current));
  }, [chartRef]);

  const hideGeom = (field: string) => {
    if (chartRef?.current) {
      chartRef.current.chart.geometries.map((geom) => {
        // console.log("geom :>> ", geom);
        // console.log("elements :>> ", geom.getElements());
        // console.log("fields :>> ", geom.getFields());
        let fields = geom.getFields();
        if (fields.includes(field)) {
          geom.changeVisible(!geom.visible);
        }
      });
    }
  };

  const isGeomVisible = (field: string): boolean => {
    if (chartRef?.current) {
      let isVisible = false;
      chartRef.current.chart.geometries.map((geom) => {
        let fields = geom.getFields();
        if (fields.includes(field)) {
          isVisible = geom.visible;
        }
      });
      return isVisible;
    } else {
      return false;
    }
  };
  return (
    <>
      <div style={{ height: "400px" }} ref={chartNodeRef} />
      {displayGeomSwitches && (
        <Stack
          direction={{ xs: "column", sm: "row" }}
          spacing={{ xs: 1, sm: 2, md: 4 }}
        >
          <FormGroup>
            {metadata.map((meta, index) => {
              if (["S_t", "tair_max"].includes(meta.id)) {
                return (
                  <FormControlLabel
                    key={index}
                    control={
                      <SeriesSwitcher
                        checked={isGeomVisible(meta.id)}
                        color="warning"
                        onChangeCallback={() => hideGeom(`${meta.id}`)}
                      />
                    }
                    label={meta.alias}
                  />
                );
              }
            })}
          </FormGroup>
          <FormGroup>
            {metadata.map((meta, index) => {
              if (["pp_mmjour"].includes(meta.id)) {
                return (
                  <FormControlLabel
                    key={index}
                    control={
                      <SeriesSwitcher
                        checked={isGeomVisible(meta.id)}
                        color="pp"
                        onChangeCallback={() => hideGeom(`${meta.id}`)}
                      />
                    }
                    label={meta.alias}
                  />
                );
              }
            })}
          </FormGroup>
          <FormGroup>
            {metadata.map((meta, index) => {
              if (["S_1", "S_2", "S_3"].includes(meta.id)) {
                return (
                  <FormControlLabel
                    key={index}
                    control={
                      <SeriesSwitcher
                        checked={isGeomVisible(meta.id)}
                        color="info"
                        onChangeCallback={() => hideGeom(`${meta.id}`)}
                      />
                    }
                    label={meta.alias}
                  />
                );
              }
            })}
          </FormGroup>
        </Stack>
      )}
    </>
  );
}

export default ComboPlot;
