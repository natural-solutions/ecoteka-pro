export const VegetatedAreaSurfaceIcon = () => {
  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 15 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_2260_1709)">
        <path
          d="M7.50625 3.9375L6.25 5.5H8.75L7.50625 3.9375ZM11.25 6.75V9.25L12.8125 8.00625L11.25 6.75ZM3.75 6.75L2.1875 8.00625L3.75 9.25V6.75ZM8.75 10.5H6.25L7.50625 12.0625L8.75 10.5ZM13.125 2.375H1.875C1.1875 2.375 0.625 2.9375 0.625 3.625V12.375C0.625 13.0625 1.1875 13.625 1.875 13.625H13.125C13.8125 13.625 14.375 13.0625 14.375 12.375V3.625C14.375 2.9375 13.8125 2.375 13.125 2.375ZM13.125 12.3813H1.875V3.61875H13.125V12.3813Z"
          fill="#7E8989"
        />
      </g>
      <defs>
        <clipPath id="clip0_2260_1709">
          <rect
            width="15"
            height="15"
            fill="white"
            transform="translate(0 0.5)"
          />
        </clipPath>
      </defs>
    </svg>
  );
};
