import SoonIcon from "@mui/icons-material/AccessTimeFilledOutlined";
import WarnIcon from "@mui/icons-material/WarningRounded";
import UnkwownIcon from "@mui/icons-material/HelpRounded";
import ErrorIcon from "@mui/icons-material/ErrorRounded";

export interface IScheduleIconProps {
  variant: "planned" | "late" | "soon" | "unknown";
}
const ScheduleIcon = ({ variant = "planned" }: IScheduleIconProps) => {
  switch (variant) {
    case "planned":
      return <SoonIcon color="info" />;
      break;
    case "late":
      return <ErrorIcon color="error" />;
      break;
    case "soon":
      return <WarnIcon color="warning" />;
      break;
    case "unknown":
      return <UnkwownIcon color="disabled" />;
      break;
    default:
      return <UnkwownIcon color="disabled" />;
      break;
  }
};
export default ScheduleIcon;
