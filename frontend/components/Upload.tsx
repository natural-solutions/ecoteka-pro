import { FC } from "react";
import { usePresignedUpload } from "next-s3-upload";
import { Button } from "@mui/material";
import { useTranslation } from "react-i18next";
import { TreePhotoIcon } from "./icons/TreePhotoIcon";

type context = "creation" | "edition" | "import";

export interface UploadComponentProps {
  hasImage?: boolean;
  path: string;
  onUploaded(url: string, file: any): void;
  context: context;
}

const UploadComponent: FC<UploadComponentProps> = ({
  path,
  hasImage,
  onUploaded,
  context = "edition",
}) => {
  const { t } = useTranslation(["components"]);
  let { FileInput, openFileDialog, uploadToS3 } = usePresignedUpload();

  const getButtonLabel = () => {
    if (hasImage && context === "edition") {
      return t("TreeForm.imagesUpload.edit");
    }
    switch (context) {
      case "creation":
        return "Joindre une photo";
      case "import":
        return "Joindre un fichier";
      default:
        return t("TreeForm.imagesUpload.upload");
    }
  };

  let handleFileChange = async (file) => {
    try {
      let { url } = await uploadToS3(file, {
        endpoint: {
          request: {
            body: {
              path,
            },
          },
        },
      });
      onUploaded(["edition", "creation"].includes(context) ? url : path, file);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <FileInput onChange={handleFileChange} />
      <Button
        onClick={openFileDialog}
        sx={context !== "edition" ? styles.button : styles.buttonCard}
        startIcon={context !== "import" && <TreePhotoIcon />}
      >
        {getButtonLabel()}
      </Button>
    </>
  );
};

export default UploadComponent;

const styles = {
  buttonCard: {
    bottom: "50%",
    right: "50%",
    top: "35%",
    width: "200px",
    height: "100px",
    margin: "0px -100px -15px 0px",
    position: "absolute",
    color: "#696B6D",
    display: "flex",
    flexDirection: "column",
    opacity: "0.8",
    backgroundColor: "#EBECF0",
    "&:hover": {
      backgroundColor: "#EBECF0",
    },
  },
  button: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#EBECF0",
    margin: "auto",
    "&:hover": {
      backgroundColor: "#EBECF0",
    },
  },
};
