import { useSession } from "next-auth/react";
import { FC, ReactElement } from "react";

interface AuthCanProps {
  role: string[];
  children: ReactElement;
}

const AuthCan: FC<AuthCanProps> = ({ role = [], children }) => {
  const session = useSession();
  const hasRole = (element: string) => role.includes(element);
  const roles = session.data?.roles as string[];

  if (!roles) {
    return null;
  }

  return roles.some(hasRole) ? children : null;
};

export default AuthCan;
