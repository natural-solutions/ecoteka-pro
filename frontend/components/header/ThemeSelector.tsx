import { FC } from "react";
import { IconButton } from "@mui/material";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import Brightness4Icon from "@mui/icons-material/Brightness4";

export enum ThemeSelectorMode {
  "light" = "light",
  "dark" = "dark",
}

export interface ThemeSelectorProps {
  mode?: ThemeSelectorMode;
  onChange?(mode: ThemeSelectorMode): void;
}

const ThemeSelector: FC<ThemeSelectorProps> = ({
  mode = ThemeSelectorMode.light,
  onChange = () => {},
}) => {
  return (
    <IconButton
      color="primary"
      onClick={() => {
        onChange(
          mode === ThemeSelectorMode.light
            ? ThemeSelectorMode.dark
            : ThemeSelectorMode.light
        );
      }}
    >
      {mode === ThemeSelectorMode.light ? (
        <Brightness7Icon fontSize="small" />
      ) : (
        <Brightness4Icon fontSize="small" />
      )}
    </IconButton>
  );
};

export default ThemeSelector;
