import { FC } from "react";
import { useTranslation } from "react-i18next";

import {
  FormControl,
  Select,
  MenuItem,
  Typography,
  SelectChangeEvent,
} from "@mui/material";
import { useRouter } from "next/router";

const LanguageSelector: FC = () => {
  const { t, i18n } = useTranslation("components");
  const router = useRouter();

  const handlerLanguage = (e: SelectChangeEvent) => {
    router.push(
      {
        ...router,
      },
      {},
      { locale: e.target.value }
    );
  };

  return (
    <FormControl
      sx={{
        "& .MuiOutlinedInput-notchedOutline": {
          border: "none",
        },
        "& .MuiSvgIcon-root": {
          fill: "#47B9B2",
        },
      }}
    >
      <Select
        value={i18n.language}
        displayEmpty
        onChange={handlerLanguage}
        renderValue={(value) => (
          <Typography color="primary" sx={{ textTransform: "uppercase" }}>
            {value}
          </Typography>
        )}
      >
        <MenuItem value="en">
          {t("components.LanguageSelector.en") as string}
        </MenuItem>
        <MenuItem value="fr">
          {t("components.LanguageSelector.fr") as string}
        </MenuItem>
        <MenuItem value="es">
          {t("components.LanguageSelector.es") as string}
        </MenuItem>
      </Select>
    </FormControl>
  );
};

export default LanguageSelector;
