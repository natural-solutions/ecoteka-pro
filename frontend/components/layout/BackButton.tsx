import { Button, IconButton, Stack, Typography } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { FC, MouseEventHandler } from "react";
import { useDeviceSize } from "@lib/utils";

export interface BackButtonProps {
  title?: string;
  onClick?: MouseEventHandler<HTMLButtonElement>;
}
const BackButton: FC<BackButtonProps> = ({ title, onClick }) => {
  const { isDesktop } = useDeviceSize();

  return isDesktop ? (
    <Stack
      direction="row"
      justifyContent="flex-start"
      data-cy="back-button-container-desktop"
    >
      <Button
        variant="outlined"
        startIcon={<ArrowBackIcon />}
        onClick={onClick}
        data-cy="back-button-desktop"
      >
        <Typography
          sx={{
            textTransform: "uppercase",
          }}
          color="primary"
          variant="h6"
        >
          {title}
        </Typography>
      </Button>
    </Stack>
  ) : (
    <Stack
      direction="row"
      justifyContent="flex-end"
      data-cy="back-button-container-mobile"
    >
      <IconButton
        color="primary"
        size="large"
        onClick={onClick}
        data-cy="back-button-mobile"
      >
        <ArrowBackIcon />
      </IconButton>
    </Stack>
  );
};

export default BackButton;
