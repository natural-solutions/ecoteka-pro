import React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import { styled } from "@mui/material/styles";
import { useDeviceSize } from "@lib/utils";

interface MainViewProps {
  View: React.ReactNode;
  Actions?: React.ReactNode;
  Controls?: React.ReactNode;
  Legend?: React.ReactNode;
  Panel?: React.ReactNode;
  Filters?: React.ReactNode;
  Navigation?: React.ReactNode;
  Settings?: React.ReactNode;
  Basemaps?: React.ReactNode;
  drawerActive: boolean;
  drawerWidth?: number;
  closePanelCallback: () => void;
}

const styles = {
  root: {
    position: "relative",
    width: "100%",
    height: "100%",
    overflow: "hidden",
  },
  view: {
    position: "relative",
    width: "100%",
    height: "100%",
  },
  absolute: {
    position: "absolute",
    width: "100px",
    height: "100px",
    top: "1rem",
    left: "1rem",
  },
  gridLayoutDesktop: {
    position: "absolute",
    width: "100%",
    height: "100%",
    display: "grid",
    pointerEvents: "none",
    gridTemplateColumns: "1fr 1fr 1fr 1fr 1fr 54px",
    gridTemplateRows: " 0.25fr 1fr 1fr 0.25fr",
    gap: "0.25em 0.25em",
    gridTemplateAreas: `
    ". . filters filters . ."
    ". . . . . controls"
    ". . . . . navigation"
    ". . actions actions . ."`,
  },
  gridLayoutMobile: {
    width: "100%",
    height: "100%",
    position: "absolute",
    display: "grid",
    pointerEvents: "none",
    gridTemplateColumns: "1fr 1fr 1fr 54px",
    gridTemplateRows: "0.25fr 1fr 1fr 54px 54px 54px",
    gap: "0.25em 0.25em",
    gridAutoFlow: "row",
    alignItems: "center",
    gridTemplateAreas: `
    ". . . settings"
    ". . . ."
    ". . . ."
    ". . . actions"
    ". . . baasemap"
    ". . . navigation"`,
  },
  Actions: { gridArea: "actions", pointerEvents: "initial" },
  Navigation: { gridArea: "navigation", pointerEvents: "initial" },
  Controls: { gridArea: "controls", pointerEvents: "initial" },
  Filters: { gridArea: "filters", pointerEvents: "initial" },
  Settings: { gridArea: "settings", pointerEvents: "initial" },
  Basemaps: { gridArea: "basemaps", pointerEvents: "initial" },
  Panel: { height: "100vh" },
};

const ViewContainer = styled("div", {
  shouldForwardProp: (prop) => prop !== "open",
})<{
  open?: boolean;
  drawerWidth?: number;
}>(({ theme, open, drawerWidth = 250 }) => ({
  position: "relative",
  // width: "100%",
  flexGrow: 1,
  height: "100%",
  overflow: "hidden",
  transition: theme.transitions.create("margin", {
    easing: theme.transitions.easing.easeOut,
    duration: theme.transitions.duration.enteringScreen,
  }),
  // marginLeft: `-${drawerWidth}px`,
  ...(open && {
    // width: `calc(100% - ${drawerWidth}px)`,
    // flexGrow: 1,
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `${drawerWidth}px`,
  }),
}));

const MainView: React.FC<MainViewProps> = ({
  View,
  Actions,
  Controls,
  Legend,
  Panel,
  Settings,
  Filters,
  Navigation,
  Basemaps,
  drawerActive,
  drawerWidth = 250,
  closePanelCallback,
}) => {
  const { isTablet } = useDeviceSize();
  return (
    <React.Fragment key="left">
      {!isTablet && Panel && (
        <Drawer
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            "& .MuiDrawer-paper": {
              width: drawerWidth,
              boxSizing: "border-box",
            },
          }}
          variant="persistent"
          anchor="left"
          open={drawerActive}
        >
          <Box
            sx={{
              width: drawerWidth,
            }}
            role="presentation"
          >
            <Box sx={styles.Panel}>{Panel}</Box>
          </Box>
        </Drawer>
      )}
      {!isTablet && (
        <ViewContainer open={drawerActive} drawerWidth={drawerWidth}>
          {View && (
            <Box sx={styles.view}>
              {View}
              <Box sx={styles.gridLayoutDesktop}>
                {Actions && <Box sx={styles.Actions}>{Actions}</Box>}
                {Controls && <Box sx={styles.Controls}>{Controls}</Box>}
                {Filters && <Box sx={styles.Filters}>{Filters}</Box>}
                {Settings && <Box sx={styles.Settings}>{Settings}</Box>}
                {Navigation && <Box sx={styles.Navigation}>{Navigation}</Box>}
                {Basemaps && <Box sx={styles.Basemaps}>{Basemaps}</Box>}
              </Box>
            </Box>
          )}
        </ViewContainer>
      )}
      {isTablet && (
        <Box sx={styles.view}>
          {View}
          <Box sx={styles.gridLayoutMobile}>
            {Actions && <Box sx={styles.Actions}>{Actions}</Box>}
            {Filters && <Box sx={styles.Filters}>{Filters}</Box>}
            {Basemaps && <Box sx={styles.Basemaps}>{Basemaps}</Box>}
            {Navigation && <Box sx={styles.Navigation}>{Navigation}</Box>}
          </Box>
        </Box>
      )}
    </React.Fragment>
  );
};

export default MainView;
