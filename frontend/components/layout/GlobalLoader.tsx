import CircularProgress from "@mui/material/CircularProgress";
import { Stack, Box } from "@mui/material";

export const GlobalLoader = ({ sx = {} }) => {
  return (
    <Stack
      sx={{
        height: "100vh",
        width: "100vw",
        position: "fixed",
        top: 0,
        left: 0,
        zIndex: 2000,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        ...sx,
      }}
    >
      <CircularProgress color="primary" size={40} thickness={6} />
    </Stack>
  );
};
