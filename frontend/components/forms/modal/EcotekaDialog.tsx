import { styled } from "@mui/material/styles";
import Dialog, { DialogProps } from "@mui/material/Dialog";

const EcotekaDialog = styled(Dialog)<DialogProps>(({ theme }) => ({
  backdropFilter: "blur(5px)",

  // other styles here...
}));

export default EcotekaDialog;
