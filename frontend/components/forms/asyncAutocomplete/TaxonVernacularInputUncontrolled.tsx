import TextField from "@mui/material/TextField";
import Autocomplete, {
  AutocompleteRenderInputParams,
} from "@mui/material/Autocomplete";
import ParkIcon from "@mui/icons-material/Park";
import { FC, useEffect, useState, useCallback } from "react";
import { Box, Grid, Stack, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import _ from "lodash";
import {
  TaxonHit,
  useFetchTaxaSearchResultsLazyQuery,
} from "../../../generated/graphql";

interface TaxonVernacularSearchProps {
  value: TaxonHit | null;
  onChange: (value: TaxonHit | null) => void;
  defaultValue: TaxonHit | null;
  required: boolean;
  label?: string;
  readOnly?: boolean;
}

const TaxonVernacularSearchUncontrolled: FC<TaxonVernacularSearchProps> = ({
  value,
  onChange,
  defaultValue,
  required,
  label,
  readOnly,
}) => {
  const { t } = useTranslation(["components"]);

  const [options, setOptions] = useState<TaxonHit[]>([]);
  const [fetchTaxaSearchResults, { loading, data, error }] =
    useFetchTaxaSearchResultsLazyQuery();

  const debouncedFetchTaxaSearchResults = useCallback(
    _.debounce(fetchTaxaSearchResults, 350),
    []
  );

  useEffect(() => {
    let hits = data?.taxaSearch?.hits;
    if (hits) {
      setOptions(hits as TaxonHit[]);
    }
  }, [data]);

  return (
    <Stack
      sx={{
        flexDirection: "column",
        flexGrow: 1,
      }}
    >
      <Autocomplete
        id="taxon-vernacular-search-input"
        disabled={readOnly}
        value={value}
        isOptionEqualToValue={(option, value) => {
          if (typeof value === "string") {
            return option.vernacular_name === value;
          } else {
            // Considering value is an object
            return _.isEqual(option, value);
          }
        }}
        filterOptions={(x) => x}
        options={options}
        autoComplete
        includeInputInList
        filterSelectedOptions
        noOptionsText="Aucun taxon trouvé"
        getOptionLabel={(option) => {
          if (typeof option === "string") {
            return option;
          }
          if (typeof option === "object") {
            return option.vernacular_name ?? "";
          }
          return "";
        }}
        onChange={(
          event: React.SyntheticEvent<Element, Event>,
          newValue: TaxonHit | null,
          reason,
          details
        ) => {
          if (reason == "clear") {
            onChange(null);
          }
          if (newValue) {
            onChange(newValue);
          }
        }}
        renderInput={(params: AutocompleteRenderInputParams) => (
          <TextField
            {...params}
            label={label}
            variant="filled"
            InputLabelProps={{ shrink: true }}
          />
        )}
        onInputChange={(event, newInputValue, reason) => {
          debouncedFetchTaxaSearchResults({
            variables: { query: newInputValue },
          });
        }}
        renderOption={(props, option) => {
          return (
            <li {...props} key={option.id}>
              <Grid container alignItems="center">
                <Grid item sx={{ display: "flex", width: 44 }}>
                  <ParkIcon sx={{ color: "text.secondary" }} />
                </Grid>
                <Grid
                  item
                  sx={{
                    width: "calc(100% - 44px)",
                    wordWrap: "break-word",
                  }}
                >
                  <Box
                    component="span"
                    sx={{
                      fontWeight: "bold",
                      fontStyle: "italic",
                    }}
                  >
                    {option.vernacular_name}
                  </Box>
                  <Typography variant="body2" color="text.secondary">
                    {option.canonical_name}
                  </Typography>
                </Grid>
              </Grid>
            </li>
          );
        }}
      />
    </Stack>
  );
};

export { TaxonVernacularSearchUncontrolled };
