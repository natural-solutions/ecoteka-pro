import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import {
  Control,
  Controller,
  FieldErrors,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import CancelIcon from "@mui/icons-material/Cancel";
import _without from "lodash/without";
import {
  Grid,
  TextField,
  Autocomplete,
  AutocompleteRenderInputParams,
  Box,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  FormControlLabel,
  Switch,
  SelectChangeEvent,
  Chip,
  ListItemText,
  CircularProgress,
  CardMedia,
} from "@mui/material";
import ErrorIcon from "@mui/icons-material/Error";

import { useSession } from "next-auth/react";
import { FormInputError } from "../error/FormInputError";
import { useRouter } from "next/router";
import {
  Analysis_Tool,
  Diagnosis,
  Diagnosis_Type,
  FetchDiagnosisFormDataQuery,
  FetchOneDiagnosisQuery,
  FetchOneTreeQuery,
  Pathogen,
} from "../../../generated/graphql";
import _ from "lodash";

import {
  TREE_CONDITIONS,
  RECOMMENDATIONS,
  COLLAR_TRUNK_CONDITIONS,
  CROWN_CARPENTERS_CONDITIONS,
  VIGOR_CONDITIONS,
} from "@lib/constants";
import { checkIfImageExists } from "@lib/utils";
import UploadComponent from "@components/Upload";
import getConfig from "next/config";

export interface IDiagnosis extends Diagnosis {
  pathogens: Pathogen[];
  analysis_tools: Analysis_Tool[];
}

export interface DiagnosisFormProps {
  diagnosisFormData: FetchDiagnosisFormDataQuery;
  activeDiagnosis?: FetchOneDiagnosisQuery["diagnosis"][0];
  activeTree?: FetchOneTreeQuery["tree"][0];
  onCreateDiagnosis?(data: IDiagnosis): void;
  onUpdateDiagnosis?(data: IDiagnosis): void;
  onDeleteDiagnosis?(id: string): void;
  setValue: UseFormSetValue<any>;
  control: Control;
  watch: UseFormWatch<IDiagnosis>;
  errors: FieldErrors<IDiagnosis>;
}

const fields = [
  { type: "date", label: "diagnosis_date", column: "diagnosis_date", size: 6 },
  {
    type: "autocomplete",
    label: "diagnosisType",
    column: "diagnosis_type",
    size: 6,
  },
  {
    type: "textReadOnly",
    label: "dataEntryUsername",
    size: 6,
  },
  {
    type: "select",
    label: "tree_condition",
    column: "tree_condition",
    size: 6,
  },
  { type: "select", label: "tree_vigor", column: "tree_vigor", size: 12 },

  {
    type: "select",
    label: "collar_condition",
    column: "collar_condition",
    size: 6,
  },
  {
    type: "select",
    label: "trunk_condition",
    column: "trunk_condition",
    size: 6,
  },
  {
    type: "select",
    label: "carpenters_condition",
    column: "carpenters_condition",
    size: 6,
  },
  {
    type: "select",
    label: "crown_condition",
    column: "crown_condition",
    size: 6,
  },
  {
    type: "boolean",
    label: "tree_is_dangerous",
    column: "tree_is_dangerous",
    size: 12,
  },
  { type: "multiselect", label: "pathogen", column: "pathogens", size: 12 },

  {
    type: "multiselect",
    label: "analysis_tool",
    column: "analysis_tools",
    size: 12,
  },
  {
    type: "text",
    label: "analysis_results",
    column: "analysis_results",
    size: 12,
  },
  { type: "textArea", label: "note", column: "note", size: 12 },
  {
    type: "select",
    label: "recommendation",
    column: "recommendation",
    size: 6,
  },
];

const selectOptions = {
  tree_condition: TREE_CONDITIONS,
  tree_vigor: VIGOR_CONDITIONS,
  crown_condition: CROWN_CARPENTERS_CONDITIONS,
  trunk_condition: COLLAR_TRUNK_CONDITIONS,
  collar_condition: COLLAR_TRUNK_CONDITIONS,
  carpenters_condition: CROWN_CARPENTERS_CONDITIONS,
  recommendation: RECOMMENDATIONS,
};

const DiagnosisForm: FC<DiagnosisFormProps> = ({
  diagnosisFormData,
  activeDiagnosis,
  activeTree,
  setValue,
  watch,
  control,
  errors,
}) => {
  const { t } = useTranslation(["components", "common"]);
  const session = useSession();
  const router = useRouter();
  const [showDetailedForm, setShowDetailedForm] = useState<boolean>(false);
  const [selectedPathogen, setSelectedPathogen] = React.useState<Pathogen[]>(
    []
  );
  const [selectedAnalysisTool, setSelectedAnalysisTool] = React.useState<
    Analysis_Tool[]
  >([]);

  const [hasImage, setHasImage] = useState<boolean>(false);
  const [imageUrl, setImageUrl] = useState("");
  const [imageIsLoading, setImageIsLoading] = useState(false);
  const { publicRuntimeConfig } = getConfig();
  const now = new Date();

  const currentUsername = session?.data?.user?.name
    ? session?.data?.user?.name
    : "";
  const diagnosisType = watch("diagnosis_type");

  const isReadOnly = !router.query.hasOwnProperty("edit");

  const handleImageUpload = (url: string, file: File) => {
    setImageIsLoading(true);
    setTimeout(() => {
      setImageUrl(url + `?t=${Date.now()}`);
      setImageIsLoading(false);
    }, 2000); // Simulate a 2-second upload time
  };

  const handleChange = (
    event: SelectChangeEvent<
      typeof selectedPathogen | typeof selectedAnalysisTool
    >,
    type: string
  ) => {
    const {
      target: { value },
    } = event;
    if (type === "pathogen") {
      setSelectedPathogen(value as Pathogen[]);
      setValue("pathogens", value as Pathogen[]);
    } else if (type === "analysis_tool") {
      setSelectedAnalysisTool(value as Analysis_Tool[]);
      setValue("analysis_tools", value as Analysis_Tool[]);
    }
  };

  const handleDelete = (
    event: React.MouseEvent,
    type: string,
    value: Pathogen | Analysis_Tool
  ) => {
    event.preventDefault();

    if (type === "pathogen") {
      setSelectedPathogen((current) => {
        const updatedPathogens = _without(current, value as Pathogen);
        setValue("pathogens", updatedPathogens);
        return updatedPathogens;
      });
    } else if (type === "analysis_tool") {
      setSelectedAnalysisTool((current) => {
        const updatedAnalysisTools = _without(current, value as Analysis_Tool);
        setValue("analysis_tools", updatedAnalysisTools);
        return updatedAnalysisTools;
      });
    }
  };

  useEffect(() => {
    if (!activeDiagnosis && router.query.type) {
      const diagnosisSelected = diagnosisFormData?.diagnosis_type.find(
        (diag) => diag.slug === router.query.type
      );
      setValue("diagnosis_type", diagnosisSelected as Diagnosis_Type);
    }
  }, [diagnosisFormData]);

  useEffect(() => {
    if (diagnosisType && diagnosisType?.slug === "detailed_diagnosis") {
      setShowDetailedForm(true);
    } else {
      setShowDetailedForm(false);
    }
  }, [diagnosisType]);

  useEffect(() => {
    if (activeDiagnosis) {
      activeDiagnosis.tree_id &&
        setImageUrl(
          `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/${
            publicRuntimeConfig.DEFAULT_STORAGE_BUCKET_NAME
          }/trees/${activeDiagnosis.tree_id}/diagnoses/${activeDiagnosis?.id}/latest.png?t=${Date.now()}`
        );
    }
  }, [activeDiagnosis]);

  useEffect(() => {
    checkIfImageExists(imageUrl, (exists) => {
      if (exists) {
        setHasImage(true);
        setImageIsLoading(false);
      } else {
        setHasImage(false);
        setImageIsLoading(false);
      }
    });
  }, [imageUrl]);

  useEffect(() => {
    if (activeDiagnosis?.pathogens) {
      const pathogens = activeDiagnosis.pathogens.map((item) => item.pathogen);
      setSelectedPathogen(pathogens);
      setValue("pathogens", pathogens);
    }
    if (activeDiagnosis?.analysis_tools) {
      const analysisTools = activeDiagnosis.analysis_tools.map(
        (item) => item.analysis_tool
      );
      setSelectedAnalysisTool(analysisTools);
      setValue("analysis_tools", activeDiagnosis.analysis_tools);
    }
  }, [activeDiagnosis]);

  if (!activeDiagnosis && !router.query.treeId) {
    return null;
  }

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12} md={3} xl={3}>
          <Box sx={styles.box}>
            <CardMedia
              sx={{
                height: !hasImage ? 150 : "320px",
                backgroundSize: "contain",
              }}
              image={!hasImage ? `/components/map/location/tree.png` : imageUrl}
              key={hasImage ? "hasImage" : "noImage"}
            />

            {!isReadOnly && (
              <UploadComponent
                path={`trees/${activeDiagnosis?.tree_id}/diagnoses/${activeDiagnosis?.id}/latest.png`}
                hasImage={hasImage}
                onUploaded={handleImageUpload}
                context="edition"
              />
            )}
            {imageIsLoading && <CircularProgress sx={styles.loader} />}
          </Box>
        </Grid>

        <Grid item xs={12} md={9} lg={9} xl={9}>
          <Grid container spacing={2}>
            {fields.map((fieldName) => (
              <Grid
                item
                xs={12}
                sm={fieldName.size}
                lg={fieldName.size}
                xl={fieldName.size}
                key={fieldName.label}
              >
                {fieldName?.type === "select" && (
                  <FormControl fullWidth>
                    <InputLabel
                      required={
                        ["recommendation", "tree_condition"].includes(
                          fieldName?.label
                        ) && true
                      }
                    >
                      {
                        t(
                          `components.Diagnosis.labels.${fieldName.label}`
                        ) as string
                      }
                    </InputLabel>
                    <Controller
                      defaultValue={
                        (activeDiagnosis &&
                          activeDiagnosis?.[fieldName.column!]) ||
                        ""
                      }
                      name={`${fieldName.column as keyof Diagnosis}`}
                      control={control}
                      render={({ field: { onChange, value } }) => (
                        <Select
                          value={value}
                          onChange={onChange}
                          disabled={isReadOnly}
                          label={
                            t(
                              `components.Diagnosis.labels.${fieldName.label}`
                            ) as string
                          }
                        >
                          {selectOptions[fieldName?.label]?.map(
                            (option: string, i: React.Key) => {
                              return (
                                <MenuItem key={i} value={option}>
                                  {
                                    t(
                                      `components.Diagnosis.${
                                        fieldName?.label === "recommendation"
                                          ? "recommendation"
                                          : "conditions"
                                      }.${option}`
                                    ) as string
                                  }
                                </MenuItem>
                              );
                            }
                          )}
                        </Select>
                      )}
                    />
                    <FormInputError
                      name={fieldName.column || ""}
                      errors={errors}
                    />
                  </FormControl>
                )}

                {fieldName?.label === "pathogen" && (
                  <FormControl fullWidth>
                    <InputLabel>
                      {
                        t(
                          `components.Diagnosis.labels.${fieldName.label}`
                        ) as string
                      }
                    </InputLabel>
                    <Controller
                      name={`pathogens`}
                      control={control}
                      render={() => (
                        <Select
                          value={selectedPathogen}
                          onChange={(event) =>
                            handleChange(event, fieldName?.label)
                          }
                          disabled={isReadOnly ? true : false}
                          multiple
                          renderValue={(selectedPathogen) => (
                            <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                              {selectedPathogen.map((option) => (
                                <Chip
                                  key={option.id}
                                  label={t(
                                    `components.Diagnosis.pathogen.${option.slug}`
                                  )}
                                  disabled={isReadOnly}
                                  clickable
                                  deleteIcon={
                                    <CancelIcon
                                      onMouseDown={(event) =>
                                        event.stopPropagation()
                                      }
                                    />
                                  }
                                  onDelete={(e) =>
                                    handleDelete(e, fieldName?.label, option)
                                  }
                                />
                              ))}
                            </Box>
                          )}
                          label={
                            t(
                              `components.Diagnosis.${fieldName.label}`
                            ) as string
                          }
                        >
                          {diagnosisFormData[fieldName?.label]?.map(
                            (option: any) => (
                              <MenuItem
                                key={option.id}
                                value={option}
                                selected={selectedPathogen.includes(option)}
                              >
                                <ListItemText
                                  primary={t(
                                    `components.Diagnosis.pathogen.${option.slug}`
                                  )}
                                />
                              </MenuItem>
                            )
                          )}
                        </Select>
                      )}
                    />
                    <FormInputError
                      name={fieldName.column || ""}
                      errors={errors}
                    />
                  </FormControl>
                )}

                {fieldName?.label === "analysis_tool" && showDetailedForm && (
                  <FormControl fullWidth>
                    <InputLabel>
                      {
                        t(
                          `components.Diagnosis.labels.${fieldName.label}`
                        ) as string
                      }
                    </InputLabel>

                    <Controller
                      name={`analysis_tools`}
                      control={control}
                      render={() => (
                        <Select
                          value={selectedAnalysisTool}
                          onChange={(event) =>
                            handleChange(event, fieldName?.label)
                          }
                          renderValue={(selectedAnalysisTool) => (
                            <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                              {selectedAnalysisTool.map((option) => (
                                <Chip
                                  key={option.id}
                                  label={t(
                                    `components.Diagnosis.analysis_tool.${option.slug}`
                                  )}
                                  disabled={isReadOnly}
                                  clickable
                                  deleteIcon={
                                    <CancelIcon
                                      onMouseDown={(event) =>
                                        event.stopPropagation()
                                      }
                                    />
                                  }
                                  onDelete={(e) =>
                                    handleDelete(e, fieldName?.label, option)
                                  }
                                />
                              ))}
                            </Box>
                          )}
                          disabled={isReadOnly ? true : false}
                          multiple
                          label={
                            t(
                              `components.Diagnosis.labels.${fieldName.label}`
                            ) as string
                          }
                        >
                          {diagnosisFormData[fieldName?.label]?.map(
                            (option: any) => (
                              <MenuItem
                                key={option.id}
                                value={option}
                                selected={selectedAnalysisTool.includes(option)}
                              >
                                <ListItemText
                                  primary={t(
                                    `components.Diagnosis.analysis_tool.${option.slug}`
                                  )}
                                />
                              </MenuItem>
                            )
                          )}
                        </Select>
                      )}
                    />
                    <FormInputError
                      name={fieldName.column || ""}
                      errors={errors}
                    />
                  </FormControl>
                )}
                {fieldName?.label === "analysis_results" &&
                  showDetailedForm && (
                    <Controller
                      name={`${fieldName?.column as keyof Diagnosis}`}
                      control={control}
                      defaultValue={activeDiagnosis?.[fieldName.column!] || ""}
                      render={({ field: { onChange, ref, value } }) => (
                        <TextField
                          value={value}
                          inputRef={ref}
                          fullWidth
                          disabled={isReadOnly}
                          sx={{
                            display: "flex",
                          }}
                          multiline
                          label={
                            t(
                              `components.Diagnosis.labels.${fieldName?.label}`
                            ) as string
                          }
                          variant="filled"
                          placeholder={t(
                            `components.Diagnosis.labels.${fieldName?.label}`
                          )}
                          error={!!errors[fieldName?.label]}
                          helperText={errors[fieldName?.label]?.message}
                          onChange={onChange}
                          type={fieldName?.type}
                        />
                      )}
                    />
                  )}

                {fieldName?.label === "diagnosisType" && (
                  <>
                    <Controller
                      defaultValue={activeDiagnosis?.[fieldName.column!] || ""}
                      name={`${fieldName.column as keyof Diagnosis}`}
                      rules={{ required: true }}
                      control={control}
                      render={({ field: { onChange, value } }) => {
                        value = value ?? null;
                        return (
                          <Autocomplete
                            id={`autocomplete-${name}`}
                            options={diagnosisFormData?.diagnosis_type}
                            value={value}
                            renderInput={(
                              params: AutocompleteRenderInputParams
                            ) => (
                              <TextField
                                {...params}
                                required
                                label={
                                  t(
                                    `components.Diagnosis.labels.${fieldName?.label}`
                                  ) as string
                                }
                                variant="filled"
                                InputLabelProps={{ shrink: true }}
                              />
                            )}
                            getOptionLabel={(option) => {
                              if (option.slug !== undefined) {
                                return t(
                                  `components.Interventions.interventionsList.${option.slug}`
                                );
                              } else {
                                return "";
                              }
                            }}
                            onChange={(e, newValue) => {
                              onChange(newValue);
                            }}
                            disabled={isReadOnly ? true : false}
                          />
                        );
                      }}
                    />
                    <FormInputError name={fieldName.column!} errors={errors} />
                  </>
                )}
                {fieldName?.type === "boolean" && (
                  <Controller
                    defaultValue={activeDiagnosis?.[fieldName.column!] || false}
                    name={`${fieldName.column as keyof Diagnosis}`}
                    control={control}
                    render={({ field: { onChange, ref, value } }) => (
                      <FormControlLabel
                        label={
                          <div
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <ErrorIcon color="error" sx={{ mr: 1 }} />
                            <span>
                              {
                                t(
                                  `components.Diagnosis.labels.${fieldName?.label}`
                                ) as string
                              }
                            </span>
                          </div>
                        }
                        control={
                          <Switch
                            disabled={isReadOnly}
                            ref={ref}
                            checked={value as boolean}
                            color="primary"
                            onChange={(e) => onChange(e.target.checked)}
                          />
                        }
                        labelPlacement="end"
                      />
                    )}
                  />
                )}
                {fieldName?.type === "textReadOnly" && (
                  <TextField
                    defaultValue={
                      activeDiagnosis?.[fieldName.column!] || currentUsername
                    }
                    fullWidth
                    disabled={true}
                    sx={{
                      display: "flex",
                    }}
                    label={
                      t(
                        `components.Intervention.properties.${fieldName?.label}`
                      ) as string
                    }
                    variant="filled"
                    placeholder={t(
                      `components.Intervention.placeholder.${fieldName?.label}`
                    )}
                    error={!!errors[fieldName?.label]}
                    helperText={errors[fieldName?.label]?.message}
                    type={fieldName?.type}
                  />
                )}
                {(fieldName?.type === "textArea" ||
                  fieldName?.type === "number") && (
                  <Controller
                    name={`${fieldName?.column as keyof Diagnosis}`}
                    control={control}
                    defaultValue={activeDiagnosis?.[fieldName.column!] || ""}
                    render={({ field: { onChange, ref, value } }) => (
                      <TextField
                        value={value}
                        inputRef={ref}
                        fullWidth
                        disabled={isReadOnly}
                        sx={{
                          display: "flex",
                        }}
                        multiline={
                          fieldName?.type === "textArea" ? true : false
                        }
                        label={
                          t(
                            `components.Intervention.properties.${fieldName?.label}`
                          ) as string
                        }
                        variant="filled"
                        placeholder={t(
                          `components.Intervention.placeholder.${fieldName?.label}`
                        )}
                        error={!!errors[fieldName?.label]}
                        helperText={errors[fieldName?.label]?.message}
                        onChange={onChange}
                        type={fieldName?.type}
                        inputProps={
                          fieldName?.type === "number" ? { min: 0 } : {}
                        }
                      />
                    )}
                  />
                )}
                {fieldName.type === "date" && (
                  <>
                    <Controller
                      name={`${fieldName?.column as keyof Diagnosis}`}
                      control={control}
                      defaultValue={
                        activeDiagnosis?.[fieldName.column!] ||
                        now.toISOString().split("T")[0]
                      }
                      render={({ field: { onChange, value } }) => {
                        return (
                          <TextField
                            required
                            label={
                              t(
                                `components.Diagnosis.labels.${fieldName?.label}`
                              ) as string
                            }
                            variant="filled"
                            type="date"
                            disabled={isReadOnly}
                            sx={{ width: "100%" }}
                            inputProps={{
                              max: now.toISOString().split("T")[0],
                            }}
                            InputLabelProps={{
                              shrink: true,
                            }}
                            value={value}
                            onChange={(e) => onChange(e.target.value)}
                          />
                        );
                      }}
                    />
                    <FormInputError name={fieldName.column!} errors={errors} />
                  </>
                )}
              </Grid>
            ))}
            <Grid item xs={12} sm={6} lg={6} xl={6}>
              <TextField
                value={
                  (activeTree
                    ? activeTree.location?.address
                    : activeDiagnosis?.tree?.location?.address) ||
                  activeDiagnosis?.tree?.serial_number ||
                  ""
                }
                fullWidth
                disabled={true}
                sx={{
                  display: "flex",
                }}
                label={t(`components.Diagnosis.labels.linkedTree`) as string}
                variant="filled"
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default DiagnosisForm;

const styles = {
  loader: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "250px",
  },
  cardMedia: {
    backgroundSize: "contain",
    margin: "auto",
  },
  box: {
    width: "100%",
    position: "relative",
    textAlign: "center",
  },
};
