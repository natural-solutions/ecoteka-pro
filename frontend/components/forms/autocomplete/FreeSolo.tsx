import TextField from "@mui/material/TextField";
import Autocomplete, {
  AutocompleteRenderInputParams,
  createFilterOptions,
} from "@mui/material/Autocomplete";
import React, { FC, useEffect, useState } from "react";
import { Box, Stack } from "@mui/material";
import { Control, Controller, UseFormSetValue } from "react-hook-form";
import { useTranslation } from "react-i18next";
import PartnerForm from "../partner/PartnerForm";

export interface IFreeSoloOption {
  id?: string;
  name?: string;
  inputValue?: string;
}

type FreeSoloProps = {
  control: Control<any>;
  defaultValue?: string;
  setValue: UseFormSetValue<any>;
  name: string;
  listOptions: IFreeSoloOption[];
  onChange?: (freeSoloOption: IFreeSoloOption) => void;
  required: boolean;
  defaultError?: boolean;
  label?: string;
  readOnly?: boolean;
  object?: string;
};

const FreeSolo: FC<FreeSoloProps> = ({
  control,
  defaultValue,
  setValue,
  name,
  listOptions,
  onChange = (options: IFreeSoloOption) => {},
  required,
  label,
  readOnly,
  object,
}) => {
  const { t } = useTranslation(["components"]);
  const filter = createFilterOptions<IFreeSoloOption>();

  const [open, toggleOpen] = useState(false);
  const [options, setOptions] = useState<IFreeSoloOption[]>([]);

  useEffect(() => {
    setOptions(listOptions);
  }, [listOptions]);

  const handleClose = () => {
    setDialogValue({
      name: "",
    });
    toggleOpen(false);
  };

  const [dialogValue, setDialogValue] = React.useState({
    name: "",
  });

  const onSubmit = (newOption: IFreeSoloOption) => {
    setOptions([...options, newOption]);
    setValue(name, newOption);
    handleClose();
  };

  const setElement = (newOption: IFreeSoloOption) => {
    // event for update global intervention form with new value
    if (onChange) {
      onChange(newOption as IFreeSoloOption);
    }
  };

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      rules={{ required: required }}
      render={({ field: { onChange, value } }) => {
        value = value ?? null;
        return (
          <>
            <Stack
              sx={{
                flexDirection: "column",
                flexGrow: 1,
              }}
            >
              <Autocomplete
                disabled={readOnly}
                value={value}
                onChange={(event, newValue: any) => {
                  if (typeof newValue === "string") {
                    let existingOption: IFreeSoloOption | undefined =
                      options?.find((o) => o.name?.toLowerCase() == newValue);
                    if (existingOption) {
                      setElement(existingOption);
                      onChange(newValue);
                      return;
                    }
                    toggleOpen(true);

                    setDialogValue({
                      name: newValue,
                    });
                  } else if (newValue && newValue.inputValue) {
                    toggleOpen(true);
                    setDialogValue({
                      name: newValue.inputValue,
                    });
                  } else {
                    setElement(newValue as IFreeSoloOption);
                    onChange(newValue);
                  }
                }}
                filterOptions={(options, params) => {
                  const filtered = filter(options, params);
                  if (params.inputValue !== "") {
                    filtered.push({
                      inputValue: params.inputValue,
                      name: `Ajouter ${params.inputValue}`,
                    });
                  }
                  return filtered;
                }}
                id="free-solo-dialog"
                options={options}
                getOptionLabel={(option) => {
                  // e.g value selected with enter, right from the input
                  if (typeof option === "string") {
                    return option;
                  }
                  if (option.inputValue) {
                    return option.inputValue;
                  }
                  return option.name || "";
                }}
                selectOnFocus
                clearOnBlur
                handleHomeEndKeys
                renderOption={(props, option) => (
                  <Box key={option.id}>
                    {option.name && <li {...props}>{option.name}</li>}
                  </Box>
                )}
                freeSolo
                renderInput={(params: AutocompleteRenderInputParams) => (
                  <TextField
                    {...params}
                    label={label}
                    required={required}
                    variant="filled"
                    InputLabelProps={{ shrink: true }}
                  />
                )}
              />
            </Stack>
            <PartnerForm
              open={open}
              handleClose={handleClose}
              name={dialogValue.name}
              onSubmit={onSubmit}
              object={object}
            />
          </>
        );
      }}
    />
  );
};

export { FreeSolo };
