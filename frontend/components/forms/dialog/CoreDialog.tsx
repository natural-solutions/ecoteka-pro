import { Dialog, DialogProps } from "@mui/material";
import Backdrop from "@mui/material/Backdrop";
import { FC, ReactElement } from "react";
import { useDeviceSize } from "@lib/utils";

interface CoreDialogCustomProps extends DialogProps {
  handleClose: () => void;
}

const CoreDialog: FC<CoreDialogCustomProps> = (props) => {
  const { open, handleClose, ...modalProps } = props;
  const { isTablet } = useDeviceSize();
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      {...modalProps}
      fullScreen={isTablet}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
      slots={{ backdrop: Backdrop }}
      slotProps={{
        backdrop: {
          sx: {
            backdropFilter: "blur(0.75px)",
          },
        },
      }}
    >
      {props.children}
    </Dialog>
  );
};

export { CoreDialog as CoreDialog };
