import { FC, ReactElement } from "react";
import { Box, Button, Stack, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";

const ConfirmDialog: FC<{
  title?: string;
  message?: string;
  showConfirmButton?: boolean;
  onConfirm: (data: any) => void;
  onAbort?: () => void;
  children?: ReactElement;
}> = ({
  title,
  message,
  showConfirmButton,
  onConfirm = () => {},
  onAbort = () => {
    return false;
  },
  children,
}) => {
  const { t } = useTranslation(["common"]);
  return (
    <>
      <Stack
        sx={{
          flexDirection: "column",
          flexWrap: "wrap",
        }}
      >
        <Typography
          align="center"
          fontWeight={500}
          textTransform="uppercase"
          sx={{ mb: 2 }}
        >
          {title}
        </Typography>
        <Typography align="center">{message}</Typography>
        {children}
      </Stack>
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <Button sx={{ margin: 1 }} variant="outlined" onClick={() => onAbort()}>
          {t(`common.buttons.cancel`) as string}
        </Button>
        <Button sx={{ margin: 1 }} variant="contained" onClick={onConfirm}>
          {showConfirmButton
            ? (t(`common.buttons.confirm`) as string)
            : (t(`common.buttons.continue`) as string)}
        </Button>
      </Box>
    </>
  );
};

export default ConfirmDialog;
