import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { without as _without } from "lodash";
import CancelIcon from "@mui/icons-material/Cancel";
import { FormInputDropdown } from "@components/_core/forms/elements/FormSelect";

import {
  FetchOneVegetatedAreaQuery,
  FetchVegetatedAreaFormDataQuery,
  Residential_Usage_Type,
  Urban_Constraint,
  useGetBoundariesWithCoordsLazyQuery,
} from "@generated/graphql";
import {
  Box,
  Chip,
  Grid,
  ListItemText,
  MenuItem,
  Select,
  SelectChangeEvent,
  InputLabel,
  FormControl,
  InputAdornment,
} from "@mui/material";
import {
  HEDGE_TYPE,
  LANDSCAPE_TYPE,
  POTENTIAL_AREA_STATE,
} from "@lib/constants";
import {
  VegetatedAreaFormData,
  usePolygonCommonData,
} from "@stores/forms/polygonForm";
import FormSwitch from "@components/_core/forms/elements/FormSwitch";
import FormInputText from "@components/_core/forms/elements/FormInputText";
import {
  Control,
  Controller,
  FieldValues,
  UseFormSetValue,
  UseFormWatch,
  useForm,
} from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

export const vegetatedAreaSchema = yup.object().shape({
  type: yup.string().required(),
  address: yup.string().nullable(),
  administrative_boundary: yup.string().nullable(),
  geographic_boundary: yup.string().nullable(),
  station_boundary: yup.string().nullable(),
  landscape_type: yup.string().nullable(),
  urban_site_id: yup.string().nullable(),
  urban_constraint: yup.array().of(yup.string()).nullable(),
  has_differentiated_mowing: yup.boolean().nullable(),
  is_accessible: yup.boolean().nullable(),
  residential_usage_type: yup.array().of(yup.string()).nullable(),
  inconvenience_risk: yup.string().nullable(),
  note: yup.string().nullable(),
  shrubs_data: yup.array().nullable(),
  herbaceous_data: yup.array().nullable(),
});

interface VegetatedAreaFieldsProps {
  vegetatedAreaFormData?: VegetatedAreaFormData | undefined;
  setVegetatedAreaFormData: (data: VegetatedAreaFormData) => void;
  isReadOnly: boolean;
  formData: FetchVegetatedAreaFormDataQuery | any;
  activeVegetatedArea?: FetchOneVegetatedAreaQuery | undefined;
  control: Control;
  setValue?: UseFormSetValue<FieldValues>;
  watch?: UseFormWatch<FieldValues>;
}

const VegetatedAreaFields: React.FC<VegetatedAreaFieldsProps> = ({
  vegetatedAreaFormData,
  setVegetatedAreaFormData,
  isReadOnly,
  formData,
  activeVegetatedArea,
  control,
  setValue,
  watch,
}) => {
  const { t } = useTranslation(["components", "common"]);
  const [selectedUrbanConstraint, setSelectedUrbanConstraint] = useState<
    Urban_Constraint[]
  >([]);
  const [selectedResidentialUsageType, setSelectedResidentialUsageType] =
    useState<Residential_Usage_Type[]>([]);
  const polygonData = usePolygonCommonData();
  const [getBoundariesWithCoords, { data: boundariesWithCoords }] =
    useGetBoundariesWithCoordsLazyQuery();

  useEffect(() => {
    if (!activeVegetatedArea) {
      const storedData = localStorage.getItem("vegetatedAreaFormData");
      if (storedData) {
        setVegetatedAreaFormData(JSON.parse(storedData));
      }
    }
  }, [setVegetatedAreaFormData]);

  useEffect(() => {
    if (!activeVegetatedArea && watch) {
      const subscription = watch((value) => {
        if (Object.values(value).some((val) => val !== undefined)) {
          setVegetatedAreaFormData(value as VegetatedAreaFormData);
          localStorage.setItem("vegetatedAreaFormData", JSON.stringify(value));
        }
      });
      // Clean up the subscription on unmount for avoid infinite loops
      return () => subscription.unsubscribe();
    }
  }, [watch, setVegetatedAreaFormData]);

  const createOptions = (data, prefix) => {
    return data.map((item) => ({
      label: t(`${prefix}.${item.slug || item.name}`),
      value: item.id,
      slug: item.slug,
      id: item.id,
    }));
  };

  const areaTypesOptions = [
    { label: "--", value: "" },
    {
      label: t(
        `components.VegetatedAreaForm.properties.typeLabels.potential_area`
      ),
      value: "potential_area",
    },
    {
      label: t(`components.VegetatedAreaForm.properties.typeLabels.green_area`),
      value: "green_area",
    },
    {
      label: t(`components.VegetatedAreaForm.properties.typeLabels.hedge`),
      value: "hedge",
    },
    {
      label: t(`components.VegetatedAreaForm.properties.typeLabels.lawn`),
      value: "lawn",
    },
    {
      label: t(`components.VegetatedAreaForm.properties.typeLabels.flower_bed`),
      value: "flower_bed",
    },
    {
      label: t(
        `components.VegetatedAreaForm.properties.typeLabels.afforestation`
      ),
      value: "afforestation",
    },
  ];

  const boundaryOptions = (type) =>
    formData?.boundary
      .filter((b) => b.type === type)
      .map((b) => ({
        label: b.name,
        value: b.id,
      }));

  const hedgeTypeOptions = HEDGE_TYPE.map((type) => ({
    label: t(`components.VegetatedAreaForm.properties.hedge_type.${type}`),
    value: type,
  }));

  const potentialAreaStateOptions = POTENTIAL_AREA_STATE.map((state) => ({
    label: t(
      `components.VegetatedAreaForm.properties.potential_area_state.${state}`
    ),
    value: state,
  }));

  const landscapeOptions = LANDSCAPE_TYPE.map((type) => ({
    label: t(`components.LocationForm.properties.landscape_type.${type}`),
    value: type,
  }));

  const urbanSiteOptions = createOptions(
    formData?.urban_site || [],
    "components.LocationForm.properties.urban_site"
  );
  const urbanConstraintOptions = createOptions(
    formData?.urban_constraint || [],
    "components.LocationForm.properties.urban_constraint"
  );

  const residentialUsageTypeOptions = createOptions(
    formData?.residential_usage_type || [],
    "components.VegetatedAreaForm.properties.residential_usage_type"
  );

  const handleChange = (event: SelectChangeEvent<any>, type: string) => {
    const {
      target: { value },
    } = event;

    const combinedValues =
      type === "urban_constraint"
        ? [...selectedUrbanConstraint, ...value]
        : [...selectedResidentialUsageType, ...value];
    const uniqueValues = Array.from(
      new Set(combinedValues.map((item) => item.id))
    ).map((value) => combinedValues.find((item) => item.id === value));
    if (type === "urban_constraint") {
      setSelectedUrbanConstraint(uniqueValues);
      setValue && setValue("urban_constraint", uniqueValues);
    } else if (type === "residential_usage_type") {
      setSelectedResidentialUsageType(uniqueValues);
      setValue && setValue("residential_usage_type", uniqueValues);
    }
  };
  const handleDelete = (
    event: React.MouseEvent,
    value: Urban_Constraint | Residential_Usage_Type,
    type: string
  ) => {
    event.preventDefault();
    if (type === "urban_constraint") {
      setSelectedUrbanConstraint((current) => {
        const updatedConstraints = _without(current, value as Urban_Constraint);
        setSelectedUrbanConstraint(updatedConstraints as Urban_Constraint[]);
        setValue && setValue("urban_constraint", updatedConstraints);
        return updatedConstraints;
      });
    } else if (type === "residential_usage_type") {
      setSelectedResidentialUsageType((current) => {
        const upatedResidential = _without(
          current,
          value as Residential_Usage_Type
        );
        setValue && setValue("residential_usage_type", upatedResidential);
        return upatedResidential;
      });
    }
  };

  useEffect(() => {
    if (activeVegetatedArea && setValue) {
      const data = activeVegetatedArea;
      setValue(
        "geographic_boundary",
        data?.geographic_boundary?.[0]?.boundary_id
      );
      setValue("station_boundary", data?.station_boundary?.[0]?.boundary_id);
      const urbanConstraints = data?.urban_constraint?.map(
        (u) => u.urban_constraint
      ) as Urban_Constraint[];
      const residentialUsageTypes = data?.residential_usage_type?.map(
        (r) => r.residential_usage_types
      ) as Residential_Usage_Type[];
      setSelectedUrbanConstraint(urbanConstraints);
      setValue("urban_constraint", urbanConstraints as Urban_Constraint[]);
      setSelectedResidentialUsageType(residentialUsageTypes);
      setValue(
        "residential_usage_type",
        residentialUsageTypes as Residential_Usage_Type[]
      );
    }
  }, [activeVegetatedArea]);

  useEffect(() => {
    if (!activeVegetatedArea) {
      getBoundariesWithCoords({
        variables: {
          coords: {
            type: "Polygon",
            coordinates: polygonData?.geometry.coordinates,
          },
        },
      });
    }
  }, [polygonData?.geometry.coordinates]);

  return (
    <Grid container columns={12} spacing={1}>
      <Grid item xs={12} mt={1}>
        <FormInputDropdown
          name="type"
          control={control}
          label={t(`components.VegetatedAreaForm.properties.type`)}
          options={areaTypesOptions}
          fullWidth
          variant="filled"
          required
          defaultValue={
            vegetatedAreaFormData?.type ||
            activeVegetatedArea?.vegetated_area?.type
          }
          disabled={isReadOnly}
        />
      </Grid>
      <Grid item xs={6}>
        <FormInputText
          name="address"
          control={control}
          label={t(`components.VegetatedAreaForm.properties.address`)}
          fullWidth
          defaultValue={vegetatedAreaFormData?.address}
          disabled={isReadOnly}
        />
      </Grid>
      <Grid item xs={6}>
        <FormInputText
          name="surface"
          control={control}
          label={t(`components.VegetatedAreaForm.properties.surface`)}
          fullWidth
          defaultValue={vegetatedAreaFormData?.surface || polygonData?.area}
          InputProps={{
            endAdornment: <InputAdornment position="end">m²</InputAdornment>,
          }}
          disabled={isReadOnly}
        />
      </Grid>
      {(vegetatedAreaFormData?.type === "potential_area" ||
        activeVegetatedArea?.vegetated_area?.type === "potential_area") && (
        <Grid item xs={6}>
          <FormInputDropdown
            name="potential_area_state"
            control={control}
            label={t(
              `components.VegetatedAreaForm.properties.potential_area_state_label`
            )}
            options={potentialAreaStateOptions}
            fullWidth
            variant="filled"
            defaultValue={vegetatedAreaFormData?.potential_area_state}
            disabled={isReadOnly}
          />
        </Grid>
      )}
      {(vegetatedAreaFormData?.type === "hedge" ||
        activeVegetatedArea?.vegetated_area?.type === "hedge") && (
        <>
          <Grid item xs={6}>
            <FormInputText
              name="linear_meters"
              control={control}
              label={t(`components.VegetatedAreaForm.properties.linear_meters`)}
              fullWidth
              defaultValue={vegetatedAreaFormData?.linear_meters}
              InputProps={{
                endAdornment: <InputAdornment position="end">m</InputAdornment>,
              }}
              disabled={isReadOnly}
            />
          </Grid>
          <Grid item xs={6}>
            <FormInputDropdown
              name="hedge_type"
              control={control}
              label={t(
                `components.VegetatedAreaForm.properties.hedge_type_label`
              )}
              options={hedgeTypeOptions}
              fullWidth
              variant="filled"
              defaultValue={vegetatedAreaFormData?.hedge_type}
              disabled={isReadOnly}
            />
          </Grid>
        </>
      )}

      <Grid item xs={6}>
        <FormInputDropdown
          name="administrative_boundary"
          control={control}
          label={t(
            `components.LocationForm.properties.administrative_boundary`
          )}
          options={boundaryOptions("administrative")}
          fullWidth
          defaultValue={vegetatedAreaFormData?.administrative_boundary}
          disabled={isReadOnly}
        />
      </Grid>
      {(boundariesWithCoords || activeVegetatedArea) && (
        <>
          <Grid item xs={6}>
            <FormInputDropdown
              name="geographic_boundary"
              control={control}
              label={t(`components.LocationForm.properties.geographic`)}
              options={boundaryOptions("geographic")}
              fullWidth
              defaultValue={
                boundariesWithCoords?.boundary?.find(
                  (b) => b?.type === "geographic"
                )?.id || ""
              }
              disabled={isReadOnly}
            />
          </Grid>
          <Grid item xs={6}>
            <FormInputDropdown
              name="station_boundary"
              control={control}
              label={t(`components.LocationForm.properties.station_boundary`)}
              options={boundaryOptions("station")}
              fullWidth
              defaultValue={
                (!activeVegetatedArea &&
                  boundariesWithCoords?.boundary?.find(
                    (b) => b?.type === "station"
                  )?.id) ||
                ""
              }
              disabled={isReadOnly}
            />
          </Grid>
        </>
      )}
      <Grid item xs={6}>
        <FormInputDropdown
          name="landscape_type"
          control={control}
          label={t("components.LocationForm.labels.landscape_type")}
          options={landscapeOptions}
          fullWidth
          defaultValue={vegetatedAreaFormData?.landscape_type}
          disabled={isReadOnly}
        />
      </Grid>
      <Grid item xs={6}>
        <FormInputDropdown
          name="urban_site_id"
          control={control}
          label={t("components.LocationForm.labels.urban_site")}
          options={urbanSiteOptions}
          fullWidth
          defaultValue={vegetatedAreaFormData?.urban_site_id}
          disabled={isReadOnly}
        />
      </Grid>
      <Grid item xs={6}>
        <FormControl fullWidth>
          <InputLabel>
            {t("components.LocationForm.labels.urban_constraint")}
          </InputLabel>
          <Controller
            name="urban_constraint"
            control={control}
            render={({ field: { onChange, value } }) => (
              <Select
                value={
                  selectedUrbanConstraint ||
                  vegetatedAreaFormData?.urban_constraint
                }
                variant="filled"
                label={t("components.LocationForm.labels.urban_constraint")}
                onChange={(event) => handleChange(event, "urban_constraint")}
                multiple
                disabled={isReadOnly}
                renderValue={(selectedUrbanConstraint) => (
                  <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                    {selectedUrbanConstraint.map((option) => (
                      <Chip
                        key={option.id}
                        label={t(
                          `components.LocationForm.properties.urban_constraint.${option.slug}`
                        )}
                        disabled={isReadOnly}
                        clickable
                        deleteIcon={
                          <CancelIcon
                            onMouseDown={(event) => event.stopPropagation()}
                          />
                        }
                        onDelete={(e) =>
                          handleDelete(e, option, "urban_constraint")
                        }
                      />
                    ))}
                  </Box>
                )}
              >
                {urbanConstraintOptions.map((option: any) => (
                  <MenuItem
                    key={option.id}
                    value={option}
                    selected={selectedUrbanConstraint?.includes(option)}
                  >
                    <ListItemText
                      primary={t(
                        `components.LocationForm.properties.urban_constraint.${option.slug}`
                      )}
                    />
                  </MenuItem>
                ))}
              </Select>
            )}
          />
        </FormControl>
      </Grid>
      {vegetatedAreaFormData?.type !== "potential_area" && (
        <>
          <Grid item xs={6}>
            <FormSwitch
              name="has_differentiated_mowing"
              control={control}
              label={t(
                `components.VegetatedAreaForm.properties.differentiated_mowing`
              )}
              defaultChecked={
                vegetatedAreaFormData?.has_differentiated_mowing ? true : false
              }
              isReadOnly={isReadOnly}
            />
          </Grid>
          <Grid item xs={6}>
            <FormSwitch
              name="is_accessible"
              control={control}
              label={t(`components.VegetatedAreaForm.properties.is_accessible`)}
              defaultChecked={
                vegetatedAreaFormData?.is_accessible ? true : false
              }
              isReadOnly={isReadOnly}
            />
          </Grid>
          {(vegetatedAreaFormData?.is_accessible ||
            activeVegetatedArea?.vegetated_area?.is_accessible) && (
            <Grid item xs={6}>
              <FormControl fullWidth>
                <InputLabel>
                  {t(
                    "components.VegetatedAreaForm.properties.residential_usage_type.label"
                  )}
                </InputLabel>
                <Controller
                  name="residential_usage_type"
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <Select
                      value={
                        selectedResidentialUsageType ||
                        vegetatedAreaFormData?.residential_usage_type
                      }
                      variant="filled"
                      label={t(
                        "components.VegetatedAreaForm.properties.residential_usage_type.label"
                      )}
                      onChange={(event) =>
                        handleChange(event, "residential_usage_type")
                      }
                      multiple
                      disabled={isReadOnly}
                      renderValue={(selectedResidentialUsageType) => (
                        <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                          {selectedResidentialUsageType.map((option) => (
                            <Chip
                              key={option.id}
                              label={t(
                                `components.VegetatedAreaForm.properties.residential_usage_type.${option.slug}`
                              )}
                              disabled={isReadOnly}
                              clickable
                              deleteIcon={
                                <CancelIcon
                                  onMouseDown={(event) =>
                                    event.stopPropagation()
                                  }
                                />
                              }
                              onDelete={(e) =>
                                handleDelete(
                                  e,
                                  option,
                                  "residential_usage_type"
                                )
                              }
                            />
                          ))}
                        </Box>
                      )}
                    >
                      {residentialUsageTypeOptions.map((option: any) => (
                        <MenuItem
                          key={option.id}
                          value={option}
                          selected={selectedResidentialUsageType?.includes(
                            option
                          )}
                        >
                          <ListItemText
                            primary={t(
                              `components.VegetatedAreaForm.properties.residential_usage_type.${option.slug}`
                            )}
                          />
                        </MenuItem>
                      ))}
                    </Select>
                  )}
                />
              </FormControl>
            </Grid>
          )}
          <Grid item xs={6}>
            <FormInputText
              name="inconvenience_risk"
              control={control}
              label={t(
                `components.VegetatedAreaForm.properties.inconvenience_risk`
              )}
              fullWidth
              defaultValue={vegetatedAreaFormData?.inconvenience_risk}
              disabled={isReadOnly}
            />
          </Grid>
        </>
      )}

      <Grid item xs={12}>
        <FormInputText
          name="note"
          control={control}
          label={t(`components.VegetatedAreaForm.properties.note`)}
          fullWidth
          defaultValue={vegetatedAreaFormData?.note}
          disabled={isReadOnly}
        />
      </Grid>
    </Grid>
  );
};

export default VegetatedAreaFields;
