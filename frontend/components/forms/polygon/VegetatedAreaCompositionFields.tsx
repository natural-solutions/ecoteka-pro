import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { without as _without } from "lodash";

import { FetchOneVegetatedAreaQuery } from "@generated/graphql";
import { Alert, Grid } from "@mui/material";
import { VegetatedAreaFormData } from "@stores/forms/polygonForm";
import {
  Control,
  Controller,
  FieldValues,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import PlantInputRow, { PlantRow } from "./PlantInputRow";

interface VegetatedAreaCompositionFieldsProps {
  vegetatedAreaFormData?: VegetatedAreaFormData;
  setVegetatedAreaFormData?: (data: VegetatedAreaFormData) => void;
  isReadOnly: boolean;
  activeVegetatedArea?: FetchOneVegetatedAreaQuery | undefined;
  control?: Control | undefined;
  setValue?: UseFormSetValue<FieldValues>;
  watch?: UseFormWatch<FieldValues> | undefined;
}

const VegetatedAreaCompositionFields: React.FC<
  VegetatedAreaCompositionFieldsProps
> = ({
  vegetatedAreaFormData,
  setVegetatedAreaFormData,
  activeVegetatedArea,
  isReadOnly,
  control,
  watch,
  setValue,
}) => {
  const { t } = useTranslation(["components", "common"]);
  // Initialize with one default empty row
  const [shrubRows, setShrubRows] = useState<PlantRow[]>([
    { specie: "", number: 0, height: null },
  ]);
  const [herbaceousRows, setHerbaceousRows] = useState<PlantRow[]>([
    { specie: "", number: 0, height: null },
  ]);
  const [afforestationTreeRows, setAfforestationTreeRows] = useState<
    PlantRow[]
  >([{ specie: "", number: 0, height: 0 }]);
  const [treeRows, setTreeRows] = useState<PlantRow[]>([
    { id: "", specie: "", number: 0, height: null },
  ]);

  useEffect(() => {
    if (!activeVegetatedArea) {
      const storedData = localStorage.getItem("vegetatedAreaFormData");
      if (storedData && vegetatedAreaFormData) {
        setVegetatedAreaFormData &&
          setVegetatedAreaFormData(JSON.parse(storedData));
      } else {
        setShrubRows(vegetatedAreaFormData?.shrubs_data || []);
        setHerbaceousRows(vegetatedAreaFormData?.herbaceous_data || []);
        setTreeRows(vegetatedAreaFormData?.afforestation_trees_data || []);
      }
    }
  }, [setVegetatedAreaFormData]);

  useEffect(() => {
    if (!activeVegetatedArea && watch) {
      const subscription = watch((value) => {
        if (Object.values(value).some((val) => val !== undefined)) {
          try {
            setVegetatedAreaFormData &&
              setVegetatedAreaFormData(value as VegetatedAreaFormData);
            localStorage.setItem(
              "vegetatedAreaFormData",
              JSON.stringify(value)
            );
          } catch (error) {
            console.error("Error setting form data:", error); // Log any errors
          }
        }
      });
      // Clean up the subscription on unmount for avoid infinite loops
      return () => subscription.unsubscribe();
    }
  }, [watch, setVegetatedAreaFormData]);

  useEffect(() => {
    if (activeVegetatedArea) {
      setShrubRows(activeVegetatedArea?.vegetated_area?.shrubs_data || []);
      setHerbaceousRows(
        activeVegetatedArea?.vegetated_area?.herbaceous_data || []
      );
      const treeRowsData = activeVegetatedArea?.locations
        .filter(
          (node) => node?.location?.tree_aggregate?.aggregate?.count !== 0
        )
        .map((node) => ({
          id: node?.location?.tree_aggregate?.nodes[0].id || "",
          specie:
            node?.location?.tree_aggregate?.nodes[0]?.scientific_name || "",
          number: node?.location?.tree_aggregate?.aggregate?.count || 0,
          height: node?.location?.tree_aggregate?.nodes[0]?.height || null,
        }));
      setTreeRows(treeRowsData);
      setAfforestationTreeRows(
        activeVegetatedArea?.vegetated_area?.afforestation_trees_data || []
      );
    }
  }, [activeVegetatedArea]);

  useEffect(() => {
    if (setValue) {
      setValue("shrubs_data", shrubRows);
      setValue("herbaceous_data", herbaceousRows);
      setValue("afforestation_trees_data", afforestationTreeRows);
    }
  }, [shrubRows, herbaceousRows, afforestationTreeRows, setValue]);

  return (
    <Grid container columns={12} spacing={1}>
      <Alert severity="info">
        {activeVegetatedArea
          ? t(
              "VegetatedAreaForm.properties.vegetatedAreaComposition.infoMessageActiveVegetatedArea"
            )
          : t(
              "VegetatedAreaForm.properties.vegetatedAreaComposition.infoMessage"
            )}
      </Alert>
      {(activeVegetatedArea?.vegetated_area?.type === "afforestation" ||
        vegetatedAreaFormData?.type === "afforestation") && (
        <Grid item xs={12}>
          <Controller
            name="afforestation_trees_data"
            control={control}
            defaultValue={vegetatedAreaFormData?.afforestation_trees_data}
            render={({
              field: { onChange, value },
              fieldState: { error },
              formState,
            }) => (
              <PlantInputRow
                onChange={onChange}
                plantType={t(
                  "VegetatedAreaForm.properties.vegetatedAreaComposition.afforestation_trees.title"
                )}
                rows={afforestationTreeRows}
                setRows={setAfforestationTreeRows}
                isReadOnly={isReadOnly}
                type="afforestation_trees"
              />
            )}
          />
        </Grid>
      )}
      {treeRows && activeVegetatedArea && (
        <Grid item xs={12}>
          <PlantInputRow
            onChange={() => console.log("onchange")}
            plantType={t(
              "VegetatedAreaForm.properties.vegetatedAreaComposition.trees.title"
            )}
            rows={treeRows}
            setRows={setTreeRows}
            isReadOnly={true}
            type="trees"
          />
        </Grid>
      )}
      <Grid item xs={12}>
        <Controller
          name="shrubs_data"
          control={control}
          defaultValue={vegetatedAreaFormData?.shrubs_data}
          render={({
            field: { onChange, value },
            fieldState: { error },
            formState,
          }) => (
            <PlantInputRow
              onChange={onChange}
              plantType={t(
                "VegetatedAreaForm.properties.vegetatedAreaComposition.shrubs.title"
              )}
              rows={shrubRows}
              setRows={setShrubRows}
              isReadOnly={isReadOnly}
              type="shrubs"
            />
          )}
        />
        <Controller
          name="herbaceous_data"
          control={control}
          defaultValue={vegetatedAreaFormData?.herbaceous_data}
          render={({
            field: { onChange, value },
            fieldState: { error },
            formState,
          }) => (
            <PlantInputRow
              onChange={onChange}
              plantType={t(
                "VegetatedAreaForm.properties.vegetatedAreaComposition.herbaceous.title"
              )}
              rows={herbaceousRows}
              setRows={setHerbaceousRows}
              isReadOnly={isReadOnly}
              type="herbaceous"
            />
          )}
        />
      </Grid>
    </Grid>
  );
};

export default VegetatedAreaCompositionFields;
