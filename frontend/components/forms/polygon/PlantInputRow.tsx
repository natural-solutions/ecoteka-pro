import React from "react";
import {
  Box,
  Button,
  Grid,
  IconButton,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import InfoIcon from "@mui/icons-material/Info";
import AddIcon from "@mui/icons-material/Add";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import DeleteIcon from "@mui/icons-material/Delete";
import { useTranslation } from "react-i18next";
import { TaxonVernacularSearchUncontrolled } from "../asyncAutocomplete/TaxonVernacularInputUncontrolled";
import { TaxonHit } from "@generated/graphql";
import { useRouter } from "next/router";

export interface PlantRow {
  specie: string | TaxonHit | null;
  number: number;
  height: number | null;
  id?: string;
}

const PlantInputRow: React.FC<{
  plantType: string;
  rows: PlantRow[];
  setRows: React.Dispatch<React.SetStateAction<PlantRow[]>>;
  onChange: (value: PlantRow[], index: number, field: keyof PlantRow) => void;
  isReadOnly: boolean;
  type: string;
}> = ({ plantType, rows, setRows, onChange, isReadOnly, type }) => {
  const { t } = useTranslation(["components"]);
  const router = useRouter();

  const handleAddRow = () => {
    setRows([...(rows || []), { specie: "", number: 0, height: null }]);
  };

  const handleDeleteRow = (index: number) => {
    setRows((rows || []).filter((_, i) => i !== index));
  };

  const handleChange = (
    index: number,
    field: keyof PlantRow,
    value: string | number | TaxonHit
  ) => {
    const updatedRows = (rows || []).map((row, i) =>
      i === index ? { ...row, [field]: value } : row
    );
    setRows(updatedRows);
    const plantArray = updatedRows.map((row) => ({
      specie: row.specie,
      number: row.number,
      height: row.height,
    }));
    onChange(plantArray, index, field);
  };

  return (
    <Box mb={4}>
      <Typography variant="h6" gutterBottom>
        {["afforestation_trees", "trees"].includes(type) ? (
          <Tooltip
            title={t(
              `components.VegetatedAreaForm.properties.vegetatedAreaComposition.${type}.tooltipText`
            )}
            placement="top"
          >
            <Box sx={{ display: "flex", alignItems: "center" }}>
              {plantType}
              <InfoIcon fontSize="small" color="primary" />
            </Box>
          </Tooltip>
        ) : (
          plantType
        )}
      </Typography>
      {(rows || []).map((row, index) => (
        <Grid container spacing={1} key={index} alignItems="center" mt={0.1}>
          <Grid item xs={4}>
            {type === "afforestation_trees" ? (
              <TaxonVernacularSearchUncontrolled
                value={row.specie as TaxonHit | null}
                onChange={(newValue) =>
                  handleChange(
                    index,
                    "specie",
                    newValue?.vernacular_name || newValue?.scientific_name || ""
                  )
                }
                defaultValue={row.specie as TaxonHit | null}
                required={false}
                label={t(
                  "VegetatedAreaForm.properties.vegetatedAreaComposition.specie"
                )}
                readOnly={isReadOnly}
              />
            ) : (
              <TextField
                label={t(
                  "VegetatedAreaForm.properties.vegetatedAreaComposition.specie"
                )}
                variant="outlined"
                fullWidth
                value={row.specie}
                onChange={(e) => handleChange(index, "specie", e.target.value)}
                disabled={isReadOnly}
              />
            )}
          </Grid>
          <Grid item xs={3}>
            <TextField
              label={t(
                "VegetatedAreaForm.properties.vegetatedAreaComposition.number"
              )}
              variant="outlined"
              fullWidth
              type="number"
              value={row.number}
              onChange={(e) =>
                handleChange(index, "number", parseInt(e.target.value))
              }
              disabled={isReadOnly}
            />
          </Grid>
          <Grid item xs={3}>
            <TextField
              label={t(
                "VegetatedAreaForm.properties.vegetatedAreaComposition.height"
              )}
              variant="outlined"
              fullWidth
              type="number"
              value={row.height || null}
              inputProps={{
                step: "0.1",
              }}
              onChange={(e) =>
                handleChange(index, "height", parseFloat(e.target.value))
              }
              disabled={isReadOnly}
            />
          </Grid>

          {type === "trees" && (
            <Grid item xs={2}>
              <IconButton
                color="default"
                onClick={() => router.push(`/tree/${row.id}`)}
              >
                <ArrowForwardIosIcon />
              </IconButton>
            </Grid>
          )}

          {!isReadOnly && (
            <Grid item xs={2}>
              <IconButton
                color="secondary"
                onClick={() => handleDeleteRow(index)}
              >
                <DeleteIcon />
              </IconButton>
            </Grid>
          )}
        </Grid>
      ))}
      {!isReadOnly && (
        <Button
          variant="contained"
          color="primary"
          startIcon={<AddIcon />}
          onClick={handleAddRow}
          sx={{ mt: 1 }}
        >
          {t("common.add")}
        </Button>
      )}
    </Box>
  );
};

export default PlantInputRow;
