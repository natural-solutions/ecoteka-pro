import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";
import {
  BoundaryFormData,
  PolygonObjectType,
  usePolygonFormActions,
  useVegetatedAreaFormData,
} from "@stores/forms/polygonForm";
import VegetatedAreaFields from "./VegetatedAreaFields";
import { useFetchVegetatedAreaFormDataLazyQuery } from "@generated/graphql";
import {
  Control,
  FieldValues,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import { useRouter } from "next/router";

interface PolygonFieldsProps {
  objectType: PolygonObjectType | undefined;
  boundaryFormData: BoundaryFormData;
  setBoundaryFormData: (data: BoundaryFormData) => void;
  setPolygonObjectType: (data: PolygonObjectType) => void;
  isReadOnly: boolean;
  control: Control;
  setValue?: UseFormSetValue<FieldValues> | undefined;
  watch?: UseFormWatch<FieldValues> | undefined;
  userRole?: string[] | undefined;
}

const PolygonFields: React.FC<PolygonFieldsProps> = ({
  objectType,
  boundaryFormData,
  setBoundaryFormData,
  setPolygonObjectType,
  isReadOnly,
  control,
  setValue,
  watch,
  userRole,
}) => {
  const { t } = useTranslation(["components", "common"]);
  const { query } = useRouter();
  const vegetatedAreaFormData = useVegetatedAreaFormData();
  const { setVegetatedAreaFormData } = usePolygonFormActions();
  const [fetchVegetatedAreaData, { data: formData }] =
    useFetchVegetatedAreaFormDataLazyQuery();

  useEffect(() => {
    objectType === "vegetated_area" && fetchVegetatedAreaData();
  }, [objectType]);

  return (
    <>
      <InputLabel required>
        {t(`components.PolygonForm.polygonTypes.label`)}
      </InputLabel>
      <Select
        value={objectType}
        onChange={(e) =>
          setPolygonObjectType(e.target.value as PolygonObjectType)
        }
        fullWidth
        variant="filled"
        required
        disabled={isReadOnly}
      >
        <MenuItem value="geographic" disabled={userRole?.includes("editor")}>
          {t(`components.PolygonForm.polygonTypes.geographic`)}
        </MenuItem>
        <MenuItem value="station">
          {t(`components.PolygonForm.polygonTypes.station`)}
        </MenuItem>
        <MenuItem
          value="vegetated_area"
          disabled={
            query.activePolygon && query.type === "boundary" ? true : false
          }
        >
          {t(`components.PolygonForm.polygonTypes.vegetated_area`)}
        </MenuItem>
      </Select>
      {objectType && ["station", "geographic"].includes(objectType) && (
        <TextField
          required
          fullWidth
          onChange={(e) =>
            setBoundaryFormData({ ...boundaryFormData, name: e.target.value })
          }
          label={t(`PolygonForm.name`) as string}
          variant="filled"
          placeholder={t(`GreenAreaForm.name`)}
          sx={{ mb: 1 }}
          value={boundaryFormData?.name}
          disabled={isReadOnly}
        />
      )}
      {objectType === "vegetated_area" && (
        <VegetatedAreaFields
          isReadOnly={isReadOnly}
          formData={formData}
          setVegetatedAreaFormData={setVegetatedAreaFormData}
          vegetatedAreaFormData={vegetatedAreaFormData}
          control={control}
          setValue={setValue}
          watch={watch}
        />
      )}
    </>
  );
};

export default PolygonFields;
