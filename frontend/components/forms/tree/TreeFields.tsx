import React, { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { Control, Controller, UseFormSetValue } from "react-hook-form";
import {
  Grid,
  TextField,
  Switch,
  FormControlLabel,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  Checkbox,
  InputAdornment,
} from "@mui/material";
import _ from "lodash";

import { FormInputError } from "../error/FormInputError";

import { useRouter } from "next/router";

import { FetchOneTreeQuery, Tree } from "../../../generated/graphql";
import { TaxonScientificSearch } from "../asyncAutocomplete/TaxonScientificInput";
import { TaxonVernacularSearch } from "../asyncAutocomplete/TaxonVernacularInput";

import getConfig from "next/config";
import { TREE_HABIT } from "@lib/constants";
import { useMapActiveAction } from "@stores/pages/mapEditor";

export interface TreeFieldsProps {
  activeTree: FetchOneTreeQuery["tree"][0] | undefined;
  customControl: Control<any>;
  customSetValue: UseFormSetValue<any>;
  errors: any;
  extended: boolean;
}

const fields = [
  { type: "image", label: "picture", column: "picture" },
  {
    type: "taxonScientificSearch",
    label: "scientificName",
    column: "scientific_name",
    size: 6,
  },
  {
    type: "taxonVernacularSearch",
    label: "vernacularName",
    column: "vernacular_name",
    size: 6,
  },
  { type: "text", label: "variety", column: "variety", size: 6 },
  { type: "text", label: "serialNumber", column: "serial_number", size: 6 },
  { type: "number", label: "height", column: "height", size: 4, unit: "m" },
  { type: "select", label: "habit", column: "habit", size: 4 },
  {
    type: "number",
    label: "circumference",
    column: "circumference",
    size: 4,
    unit: "cm",
  },
  {
    type: "checkbox",
    label: "estimatedDate",
    column: "estimated_date",
    size: 12,
  },
  {
    type: "date",
    label: "plantationDate",
    column: "plantation_date",
    size: 12,
  },
  { type: "boolean", label: "watering", column: "watering", size: 6 },
  {
    type: "boolean",
    label: "isTreeOfInterest",
    column: "is_tree_of_interest",
    size: 6,
  },
  { type: "textArea", label: "note", column: "note", size: 12 },
];

const TreeFields: FC<TreeFieldsProps> = ({
  activeTree,
  customControl,
  customSetValue,
  errors,
  extended,
}) => {
  const { t } = useTranslation(["components", "common"]);
  const mapActiveAction = useMapActiveAction();

  const { publicRuntimeConfig } = getConfig();

  const { query } = useRouter();
  const [isEstimatedDate, setIsEstimatedDate] = useState<boolean>(false);

  const isReadOnly =
    !query.hasOwnProperty("edit") && mapActiveAction !== "addLocation";

  return (
    <>
      <Grid container columns={12}>
        <Grid container spacing={1}>
          {fields.map((field) => (
            <Grid
              item
              sm={extended ? 12 : field.size}
              xs={12}
              key={field.label}
            >
              {field?.type === "select" && (
                <FormControl fullWidth>
                  <InputLabel>
                    {t(`components.Tree.properties.habit.label`)}
                  </InputLabel>
                  <Controller
                    defaultValue={
                      (activeTree && activeTree[field.column]) || ""
                    }
                    name={`tree.${field.column as keyof Tree}`}
                    control={customControl}
                    render={({ field: { onChange, value } }) => (
                      <Select
                        value={value}
                        fullWidth
                        variant="filled"
                        label={t(`components.Tree.properties.habit.label`)}
                        onChange={onChange}
                        disabled={isReadOnly}
                      >
                        <MenuItem value="">
                          <em>---</em>
                        </MenuItem>
                        {TREE_HABIT.map((option, i) => {
                          return (
                            <MenuItem key={i} value={option}>
                              {t(
                                `components.Tree.properties.habit.values.${option}`
                              )}
                            </MenuItem>
                          );
                        })}
                      </Select>
                    )}
                  />
                  <FormInputError name={field.column} errors={errors["tree"]} />
                </FormControl>
              )}
              {field?.type === "taxonScientificSearch" && (
                <>
                  <TaxonScientificSearch
                    defaultValue={
                      (activeTree && activeTree[field.column]) || ""
                    }
                    name={`tree.${field.column as keyof Tree}`}
                    control={customControl}
                    setValue={customSetValue}
                    required={true}
                    label={
                      t(`components.Tree.properties.${field?.label}`) as string
                    }
                    readOnly={isReadOnly}
                  />
                  <FormInputError name={field.column} errors={errors["tree"]} />
                </>
              )}
              {field?.type === "taxonVernacularSearch" && (
                <>
                  <TaxonVernacularSearch
                    defaultValue={
                      (activeTree && activeTree[field.column]) || ""
                    }
                    name={`tree.${field.column as keyof Tree}`}
                    control={customControl}
                    setValue={customSetValue}
                    required={true}
                    label={
                      t(`components.Tree.properties.${field?.label}`) as string
                    }
                    readOnly={isReadOnly}
                  />
                  <FormInputError name={field.column} errors={errors["tree"]} />
                </>
              )}
              {(field?.type === "text" ||
                field?.type === "textArea" ||
                field?.type === "number") && (
                <>
                  <Controller
                    name={`tree.${field?.column as keyof Tree}`}
                    control={customControl}
                    rules={{
                      required: field.column === "serial_number" ? true : false,
                    }}
                    defaultValue={
                      (activeTree && activeTree[field.column]) || ""
                    }
                    render={({ field: { onChange, ref, value } }) => (
                      <TextField
                        value={value}
                        inputRef={ref}
                        fullWidth
                        required={
                          field.column === "serial_number" ? true : false
                        }
                        disabled={isReadOnly}
                        sx={{
                          display: "flex",
                        }}
                        multiline={field?.type === "textArea" ? true : false}
                        label={
                          t(
                            `components.Tree.properties.${field?.label}`
                          ) as string
                        }
                        variant="filled"
                        placeholder={t(
                          `components.Tree.properties.${field?.label}`
                        )}
                        error={
                          field.column === "serial_number"
                            ? !!errors["tree"]
                            : false
                        }
                        onChange={onChange}
                        type={field?.type}
                        InputProps={{
                          endAdornment: (field?.label === "circumference" ||
                            field?.label === "height") && (
                            <InputAdornment position="end">
                              {field?.unit}
                            </InputAdornment>
                          ),
                        }}
                      />
                    )}
                  />
                  <FormInputError name={field.column} errors={errors["tree"]} />
                </>
              )}
              {field?.type === "boolean" && (
                <Controller
                  name={`tree.${field?.column as keyof Tree}`}
                  control={customControl}
                  defaultValue={
                    (activeTree && activeTree[field.column]) || false
                  }
                  render={({ field: { onChange, ref, value } }) => (
                    <FormControlLabel
                      label={
                        t(
                          `components.Tree.properties.${field?.label}`
                        ) as string
                      }
                      control={
                        <Switch
                          disabled={isReadOnly}
                          ref={ref}
                          checked={value as boolean}
                          color="primary"
                          onChange={(e) => onChange(e.target.checked)}
                        />
                      }
                      labelPlacement="end"
                    />
                  )}
                />
              )}
              {field?.type === "checkbox" && (
                <Controller
                  name={`tree.${field?.column as keyof Tree}`}
                  control={customControl}
                  defaultValue={
                    (activeTree && activeTree[field.column]) || false
                  }
                  render={({ field: { onChange, ref, value } }) => (
                    <FormControlLabel
                      label={
                        t(
                          `components.Tree.properties.${field?.label}`
                        ) as string
                      }
                      control={
                        <Checkbox
                          disabled={isReadOnly}
                          ref={ref}
                          checked={value as boolean}
                          color="primary"
                          onChange={(e) => {
                            onChange(e.target.checked);
                            setIsEstimatedDate(e.target.checked);
                          }}
                        />
                      }
                      labelPlacement="end"
                    />
                  )}
                />
              )}
              {field.type === "date" && field.label === "plantationDate" && (
                <FormControl key={field.label} fullWidth>
                  <Controller
                    name={`tree.plantation_date`}
                    control={customControl}
                    defaultValue={
                      (activeTree && activeTree[field.column]) || ""
                    }
                    render={({ field: { onChange, value } }) => (
                      <TextField
                        label={
                          t(
                            `components.Tree.properties.${field?.label}.${
                              isEstimatedDate ? "estimated" : "exact"
                            }`
                          ) as string
                        }
                        variant="filled"
                        type="date"
                        disabled={isReadOnly}
                        sx={{ width: "100%" }}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        value={value}
                        onChange={(e) => onChange(e.target.value)}
                      />
                    )}
                  />
                </FormControl>
              )}
            </Grid>
          ))}
          {["True", "true"].includes(
            publicRuntimeConfig.URBASENSE_DASHBOARD_ACTIVE
          ) &&
            !isReadOnly &&
            mapActiveAction !== "addLocation" && (
              //@ts-ignores
              <Grid item xs={12} sm={12}>
                <Controller
                  name={`tree.urbasense_subject_id`}
                  control={customControl}
                  rules={{
                    required: false,
                  }}
                  defaultValue={
                    (activeTree && activeTree["urbasense_subject_id"]) || ""
                  }
                  render={({ field: { onChange, ref, value } }) => (
                    <TextField
                      value={value}
                      inputRef={ref}
                      fullWidth
                      required={false}
                      disabled={false}
                      sx={{
                        display: "flex",
                      }}
                      multiline={false}
                      label={
                        t("Tree.properties.urbasense_subject_id") as string
                      }
                      variant="filled"
                      placeholder={t("Tree.properties.urbasense_subject_id")}
                      error={!!errors["tree"]}
                      onChange={onChange}
                      type="text"
                    />
                  )}
                />
                <FormInputError
                  name="urbasense_subject_id"
                  errors={errors["tree"]}
                />
              </Grid>
            )}
        </Grid>
      </Grid>
    </>
  );
};

export default TreeFields;
