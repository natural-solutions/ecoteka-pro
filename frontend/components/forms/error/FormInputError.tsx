import { FC } from "react";
import get from "lodash/get";
import { useTranslation } from "react-i18next";
import {
  FieldError,
  FieldErrors,
  FieldErrorsImpl,
  Merge,
} from "react-hook-form";

export const FormInputError: FC<{
  name: string;
  errors: FieldErrors | Merge<FieldError, FieldErrorsImpl<any>> | undefined;
  message?: string;
}> = (props) => {
  const { t } = useTranslation(["common"]);
  const error = get(props.errors, props.name);

  if (!error?.message) {
    return null;
  }
  return (
    <p className="form-item-error" style={{ color: "red" }}>
      {props.message ? props.message : t("common.errors.required")}
    </p>
  );
};
