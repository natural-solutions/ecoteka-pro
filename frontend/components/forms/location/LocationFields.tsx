import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Control, Controller, UseFormSetValue } from "react-hook-form";
import {
  Grid,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Typography,
  IconButton,
  SelectChangeEvent,
  Chip,
  ListItemText,
  Box,
  Switch,
  FormControlLabel,
  Autocomplete,
} from "@mui/material";
import CancelIcon from "@mui/icons-material/Cancel";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import _ from "lodash";
import { FormInputError } from "../error/FormInputError";
import { useRouter } from "next/router";
import {
  CreateTreeMutationVariables,
  FetchAllBoundariesQuery,
  FetchLocationFormDataQuery,
  FetchOneLocationQuery,
  FetchOneLocationWithTreeIdQuery,
  Urban_Constraint,
  useGetBoundariesWithCoordsLazyQuery,
  useGetVegetatedAreaWithCoordsLazyQuery,
} from "../../../generated/graphql";
import { fetchAdresse } from "@lib/apiAdresse";
import { without as _without } from "lodash";
import {
  LANDSCAPE_TYPE,
  PLANTATION_TYPE,
  SIDEWALK_TYPE,
  INVENTORY_SOURCE,
  TINVENTORY_SOURCE,
} from "@lib/constants";
import { useMapActiveAction } from "@stores/pages/mapEditor";

export interface LocationFieldsProps {
  extended?: boolean;
  activeLocation:
    | FetchOneLocationQuery["location"][0]
    | FetchOneLocationWithTreeIdQuery["location"][0];
  statusOptions?: string[];
  loading?: boolean;
  customControl: Control<any>;
  customSetValue: UseFormSetValue<any>;
  watch: any;
  errors: any;
  boundaries: FetchAllBoundariesQuery["boundary"];
  locationFormData: FetchLocationFormDataQuery;
}

export interface Location {
  id: string;
  latitude: number;
  longitude: number;
  address: string;
  foot_type: string;
  plantation_type: string;
  urban_site_id: string;
  sidewalk_type: string;
  tree?: CreateTreeMutationVariables;
  status?: string;
  note: string;
  boundaries: {
    boundary: {
      id: string;
    };
  };
}

const LocationFields: FC<LocationFieldsProps> = ({
  activeLocation,
  customControl,
  customSetValue,
  errors,
  watch,
  boundaries,
  locationFormData,
  extended,
}) => {
  const [suggestedAddress, setSuggestedAddress] = useState("");
  const { t } = useTranslation(["components", "common"]);
  const { query, push } = useRouter();
  const mapActiveAction = useMapActiveAction();
  const [getBoundariesWithCoords, { data: boundariesWithCoords }] =
    useGetBoundariesWithCoordsLazyQuery();
  const [getVegetatedAreaWithCoords, { data: vegetatedAreaWithCoords }] =
    useGetVegetatedAreaWithCoordsLazyQuery();

  const [selectedUrbanConstraint, setSelectedUrbanConstraint] = React.useState<
    Urban_Constraint[]
  >([]);
  const [selectedInventorySource, setSelectedInventorySource] = React.useState<
    TINVENTORY_SOURCE[]
  >([]);

  const handleChange = (event: SelectChangeEvent<any>, type: string) => {
    const {
      target: { value },
    } = event;

    const combinedValues =
      type === "urban_constraint"
        ? [...selectedUrbanConstraint, ...value]
        : selectedInventorySource
          ? [...selectedInventorySource, ...value]
          : [...value];
    const uniqueValues =
      type === "inventory_source"
        ? Array.from(new Set(combinedValues))
        : Array.from(new Set(combinedValues.map((item) => item.id))).map(
            (value) => combinedValues.find((item) => item.id === value)
          );
    if (type === "urban_constraint") {
      setSelectedUrbanConstraint(uniqueValues);
      customSetValue("location.urban_constraint", uniqueValues);
    } else if (type === "inventory_source") {
      setSelectedInventorySource(uniqueValues);
      customSetValue("location.inventory_source", uniqueValues);
    }
  };
  const handleDelete = (
    event: React.MouseEvent,
    value: Urban_Constraint | TINVENTORY_SOURCE,
    type: string
  ) => {
    event.preventDefault();
    if (type === "urban_constraint") {
      setSelectedUrbanConstraint((current) => {
        const updatedConstraints = _without(current, value as Urban_Constraint);
        customSetValue("location.urban_constraint", updatedConstraints);
        return updatedConstraints;
      });
    } else if (type === "inventory_source") {
      setSelectedInventorySource((current) => {
        const updatedDetectionOrigin = _without(
          current,
          value as TINVENTORY_SOURCE
        );
        customSetValue("location.inventory_source", updatedDetectionOrigin);
        return updatedDetectionOrigin;
      });
    }
  };

  const latitude = watch("location.latitude");
  const longitude = watch("location.longitude");

  const isReadOnly = () =>
    !query.hasOwnProperty("edit") && mapActiveAction !== "addLocation";

  const fetchAddressAndUpdate = async () => {
    const fetchedAddress = await fetchAdresse(longitude, latitude);
    setSuggestedAddress(
      fetchedAddress || t("addressNotFound", { ns: "common" })
    );
  };

  useEffect(() => {
    const debouncedFetch = _.debounce(fetchAddressAndUpdate, 500);

    debouncedFetch();
    if (latitude && longitude) {
      getBoundariesWithCoords({
        variables: {
          coords: {
            type: "Point",
            coordinates: [longitude, latitude],
          },
        },
      });
      getVegetatedAreaWithCoords({
        variables: {
          coords: {
            type: "Point",
            coordinates: [longitude, latitude],
          },
        },
      });
    }

    return () => debouncedFetch.cancel();
  }, [latitude, longitude]);

  useEffect(() => {
    if (boundariesWithCoords) {
      const geographicBoundary =
        boundariesWithCoords.boundary?.find((b) => b?.type === "geographic")
          ?.id || "";

      const stationBoundary =
        boundariesWithCoords.boundary?.find((b) => b?.type === "station")?.id ||
        "";

      customSetValue(
        "location.boundaries.geographic_boundary",
        geographicBoundary
      );
      customSetValue("location.boundaries.station_boundary", stationBoundary);
    }
  }, [boundariesWithCoords, customSetValue]);

  useEffect(() => {
    if (vegetatedAreaWithCoords) {
      customSetValue(
        "location.vegetated_area_id",
        vegetatedAreaWithCoords?.vegetated_area[0]?.id
      );
    }
  }, [vegetatedAreaWithCoords]);

  useEffect(() => {
    const urbanConstraints = activeLocation?.location_urban_constraints.map(
      (o) => o.urban_constraint
    );
    const inventorySources = activeLocation?.inventory_source?.map((o) => o);
    setSelectedUrbanConstraint(urbanConstraints);
    setSelectedInventorySource(inventorySources);
    customSetValue(
      "location.urban_constraint",
      urbanConstraints as Urban_Constraint[]
    );
    customSetValue(
      "location.inventory_source",
      inventorySources as TINVENTORY_SOURCE[]
    );
  }, [activeLocation]);

  return (
    <>
      <Grid container columns={12} spacing={1}>
        <Grid item sm={extended ? 12 : 6} xs={12}>
          <Controller
            name="location.latitude"
            control={customControl}
            defaultValue={
              activeLocation?.coords?.coordinates[1].toFixed(8) || ""
            }
            render={({ field: { onChange, ref, value } }) => (
              <TextField
                disabled={isReadOnly()}
                value={value}
                inputRef={ref}
                fullWidth
                label={t(`components.Tree.latitude`) as string}
                variant="filled"
                placeholder={t(`components.Tree.latitude`)}
                InputLabelProps={{ shrink: true }}
                error={!!errors?.latitude}
                helperText={errors?.latitude?.message}
                onChange={(e) => {
                  onChange(e);
                }}
                type="text"
              />
            )}
          />
        </Grid>
        <Grid item sm={extended ? 12 : 6} xs={12}>
          <Controller
            name="location.longitude"
            control={customControl}
            defaultValue={
              activeLocation?.coords?.coordinates[0].toFixed(8) || ""
            }
            render={({ field: { onChange, ref, value } }) => (
              <TextField
                disabled={isReadOnly()}
                value={value}
                inputRef={ref}
                fullWidth
                label={t(`components.Tree.longitude`) as string}
                variant="filled"
                placeholder={t(`components.Tree.longitude`)}
                InputLabelProps={{ shrink: true }}
                error={!!errors?.latitude}
                helperText={errors?.latitude?.message}
                onChange={(e) => {
                  onChange(e);
                }}
                type="text"
              />
            )}
          />
        </Grid>
        <Grid item sm={extended ? 12 : 6} xs={12}>
          <>
            <Controller
              name="location.address"
              control={customControl}
              defaultValue={activeLocation?.address || ""}
              render={({ field: { onChange, ref, value } }) => (
                <TextField
                  inputRef={ref}
                  fullWidth
                  label={
                    t(`components.LocationForm.properties.address`) as string
                  }
                  variant="filled"
                  InputLabelProps={{ shrink: true }}
                  error={!!errors["address"]}
                  onChange={(e) => {
                    onChange(e);
                  }}
                  type="text"
                  value={value}
                  disabled={isReadOnly()}
                  helperText={
                    !isReadOnly() &&
                    mapActiveAction !== "addLocation" &&
                    suggestedAddress !== "" &&
                    suggestedAddress !== value ? (
                      <span>
                        <Typography
                          color="textSecondary"
                          variant="body2"
                          component="span"
                        >
                          {suggestedAddress}
                        </Typography>
                        <IconButton
                          onClick={() => {
                            customSetValue(
                              "location.address",
                              suggestedAddress
                            );
                          }}
                          edge="end"
                          disabled={isReadOnly()}
                        >
                          <CheckCircleOutlineIcon />
                        </IconButton>
                      </span>
                    ) : null
                  }
                />
              )}
            />
            <FormInputError name="address" errors={errors} />
          </>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t(`components.LocationForm.properties.administrative_boundary`)}
            </InputLabel>
            <Controller
              name="location.boundaries.administrative_boundary"
              defaultValue={
                activeLocation?.location_boundaries.find(
                  (boundary) => boundary.boundary.type === "administrative"
                )?.boundary.id || ""
              }
              control={customControl}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  variant="filled"
                  label={t(
                    `components.LocationForm.properties.administrative_boundary`
                  )}
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {boundaries &&
                    boundaries
                      .filter((b) => b.type === "administrative")
                      .map((option, i) => {
                        return (
                          <MenuItem key={i} value={option.id}>
                            {option.name}
                          </MenuItem>
                        );
                      })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <Controller
              name="location.boundaries.geographic_boundary"
              defaultValue={
                activeLocation?.location_boundaries.find(
                  (boundary) => boundary.boundary.type === "geographic"
                )?.boundary.id || ""
              }
              control={customControl}
              render={({ field: { onChange, value } }) => (
                <Autocomplete
                  value={boundaries?.find((b) => b.id === value) || null}
                  options={
                    boundaries?.filter((b) => b.type === "geographic") || []
                  }
                  getOptionLabel={(option) => option.name}
                  onChange={(event, newValue) => {
                    onChange(newValue ? newValue.id : "");
                  }}
                  disabled={isReadOnly()}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label={t(`components.LocationForm.properties.geographic`)}
                      variant="filled"
                    />
                  )}
                />
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <Controller
              name="location.boundaries.station_boundary"
              control={customControl}
              defaultValue={
                activeLocation?.location_boundaries.find(
                  (boundary) => boundary.boundary.type === "station"
                )?.boundary.id || ""
              }
              render={({ field: { onChange, value } }) => (
                <Autocomplete
                  value={boundaries?.find((b) => b.id === value) || null}
                  options={
                    boundaries?.filter((b) => b.type === "station") || []
                  }
                  getOptionLabel={(option) => option.name}
                  onChange={(event, newValue) => {
                    onChange(newValue ? newValue.id : "");
                  }}
                  disabled={isReadOnly()}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label={t(
                        `components.LocationForm.properties.station_boundary`
                      )}
                      variant="filled"
                    />
                  )}
                />
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t("components.LocationForm.labels.landscape_type")}
            </InputLabel>
            <Controller
              name="location.landscape_type"
              control={customControl}
              defaultValue={activeLocation?.landscape_type || ""}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  variant="filled"
                  label={t("components.LocationForm.labels.landscape_type")}
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {LANDSCAPE_TYPE.map((option, i) => {
                    return (
                      <MenuItem key={i} value={option}>
                        {
                          t(
                            `components.LocationForm.properties.landscape_type.${option}`
                          ) as string
                        }
                      </MenuItem>
                    );
                  })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>Environnement urbain</InputLabel>
            <Controller
              name="location.urban_site_id"
              control={customControl}
              defaultValue={activeLocation?.urban_site?.id || ""}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  variant="filled"
                  label="Environnement urbain"
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {locationFormData?.urban_site.map((option, i) => {
                    return (
                      <MenuItem key={i} value={option.id}>
                        {
                          t(
                            `components.LocationForm.properties.urban_site.${option.slug}`
                          ) as string
                        }
                      </MenuItem>
                    );
                  })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>Trottoir</InputLabel>
            <Controller
              name="location.sidewalk_type"
              control={customControl}
              defaultValue={activeLocation?.sidewalk_type || ""}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  variant="filled"
                  label={t(`components.LocationForm.properties.sidewalk_type`)}
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {SIDEWALK_TYPE.map((option, i) => {
                    return (
                      <MenuItem key={i} value={option}>
                        {
                          t(
                            `components.LocationForm.properties.sidewalk_type.${option}`
                          ) as string
                        }
                      </MenuItem>
                    );
                  })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t("components.LocationForm.labels.urban_constraint")}
            </InputLabel>
            <Controller
              name="location.urban_constraint"
              control={customControl}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={selectedUrbanConstraint || []}
                  variant="filled"
                  label={t("components.LocationForm.labels.urban_constraint")}
                  onChange={(event) => handleChange(event, "urban_constraint")}
                  multiple
                  disabled={isReadOnly()}
                  renderValue={(selectedUrbanConstraint) => (
                    <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                      {selectedUrbanConstraint.map((option) => (
                        <Chip
                          key={option.id}
                          label={t(
                            `components.LocationForm.properties.urban_constraint.${option.slug}`
                          )}
                          disabled={isReadOnly()}
                          clickable
                          deleteIcon={
                            <CancelIcon
                              onMouseDown={(event) => event.stopPropagation()}
                            />
                          }
                          onDelete={(e) =>
                            handleDelete(e, option, "urban_constraint")
                          }
                        />
                      ))}
                    </Box>
                  )}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {locationFormData?.urban_constraint.map((option: any) => (
                    <MenuItem
                      key={option.id}
                      value={option}
                      selected={selectedUrbanConstraint?.includes(option)}
                    >
                      <ListItemText
                        primary={t(
                          `components.LocationForm.properties.urban_constraint.${option.slug}`
                        )}
                      />
                    </MenuItem>
                  ))}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t(`components.LocationForm.labels.foot_type`) as string}
            </InputLabel>
            <Controller
              defaultValue={activeLocation?.foot_type || ""}
              name="location.foot_type"
              control={customControl}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  error={!!errors["foot_type"]}
                  variant="filled"
                  label={
                    t(`components.LocationForm.labels.foot_type`) as string
                  }
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {locationFormData?.foot_type.map((option, i) => {
                    return (
                      <MenuItem key={i} value={option.slug}>
                        {
                          t(
                            `components.LocationForm.properties.foot_type.${option.slug}`
                          ) as string
                        }
                      </MenuItem>
                    );
                  })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>
        <Grid item sm={extended ? 12 : 6} xs={12}>
          <Controller
            name={`location.is_protected`}
            control={customControl}
            defaultValue={activeLocation?.is_protected || false}
            render={({ field: { onChange, ref, value } }) => (
              <FormControlLabel
                label={
                  t(`components.LocationForm.labels.is_protected`) as string
                }
                control={
                  <Switch
                    disabled={isReadOnly()}
                    ref={ref}
                    checked={value as boolean}
                    color="primary"
                    onChange={(e) => onChange(e.target.checked)}
                  />
                }
                labelPlacement="end"
              />
            )}
          />
        </Grid>
        <Grid item sm={extended ? 12 : 6} xs={12}>
          <FormControl fullWidth>
            <InputLabel>
              {t(`components.LocationForm.labels.plantation_type`) as string}
            </InputLabel>
            <Controller
              defaultValue={activeLocation?.plantation_type || ""}
              name="location.plantation_type"
              control={customControl}
              render={({ field: { onChange, value } }) => (
                <Select
                  value={value}
                  error={!!errors["plantation_type"]}
                  variant="filled"
                  label={
                    t(
                      `components.LocationForm.labels.plantation_type`
                    ) as string
                  }
                  onChange={onChange}
                  disabled={isReadOnly()}
                >
                  <MenuItem value="">
                    <em>---</em>
                  </MenuItem>
                  {PLANTATION_TYPE.map((option, i) => {
                    return (
                      <MenuItem key={i} value={option}>
                        {
                          t(
                            `components.LocationForm.properties.plantation_type.${option}`
                          ) as string
                        }
                      </MenuItem>
                    );
                  })}
                </Select>
              )}
            />
          </FormControl>
        </Grid>

        <Grid item sm={extended ? 12 : 6} xs={12}>
          <Controller
            name="location.note"
            control={customControl}
            defaultValue={activeLocation?.note || ""}
            render={({ field: { onChange, ref, value } }) => (
              <TextField
                disabled={isReadOnly()}
                value={value}
                inputRef={ref}
                fullWidth
                label="Remarques"
                variant="filled"
                InputLabelProps={{ shrink: true }}
                onChange={(e) => {
                  onChange(e);
                }}
                type="text"
              />
            )}
          />
        </Grid>
        <Controller
          name="location.vegetated_area_id"
          control={customControl}
          defaultValue={
            activeLocation?.location_vegetated_area[0]?.vegetated_area_id ||
            undefined
          }
          render={({ field: { onChange, ref, value } }) => (
            <TextField
              value={value}
              inputRef={ref}
              onChange={(e) => {
                onChange(e);
              }}
              type="text"
              sx={{ display: "none" }}
            />
          )}
        />
      </Grid>
      <Grid item sm={extended ? 12 : 12} xs={12} sx={{ mt: 1 }}>
        <FormControl fullWidth>
          <InputLabel>
            {t("components.LocationForm.labels.inventory_source")}
          </InputLabel>
          <Controller
            name={`location.inventory_source`}
            control={customControl}
            render={() => (
              <Select
                value={selectedInventorySource || []}
                variant="filled"
                onChange={(event) => handleChange(event, "inventory_source")}
                disabled={isReadOnly()}
                multiple
                renderValue={(selectedInventorySource) => (
                  <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                    {selectedInventorySource.map((option) => (
                      <Chip
                        key={option}
                        label={t(
                          `components.LocationForm.properties.inventory_source.${option}`
                        )}
                        disabled={isReadOnly()}
                        clickable
                        deleteIcon={
                          <CancelIcon
                            onMouseDown={(event) => event.stopPropagation()}
                          />
                        }
                        onDelete={(e) =>
                          handleDelete(e, option, "inventory_source")
                        }
                      />
                    ))}
                  </Box>
                )}
                label={"test"}
              >
                {INVENTORY_SOURCE.map((option: any) => (
                  <MenuItem
                    key={option.id}
                    value={option}
                    selected={selectedInventorySource?.includes(option)}
                  >
                    <ListItemText
                      primary={t(
                        `components.LocationForm.properties.inventory_source.${option}`
                      )}
                    />
                  </MenuItem>
                ))}
              </Select>
            )}
          />
        </FormControl>
      </Grid>
    </>
  );
};
export default LocationFields;
