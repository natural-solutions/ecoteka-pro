import React, { useEffect, useState } from "react";
import {
  Alert,
  AlertTitle,
  Autocomplete,
  AutocompleteRenderInputParams,
  Box,
  Button,
  Collapse,
  Grid,
  IconButton,
  InputAdornment,
  Paper,
  TextField,
  Typography,
  LinearProgress,
} from "@mui/material";
import ParkIcon from "@mui/icons-material/Park";
import StumpIcon from "@mui/icons-material/Adjust";
import EmptyIcon from "@mui/icons-material/PanoramaFishEye";
import DeleteIcon from "@mui/icons-material/Delete";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { useTranslation } from "react-i18next";
import {
  FetchAllInterventionTypesFamiliesQuery,
  FetchInterventionPartnersQuery,
} from "@generated/graphql";
import {
  useWorksiteFormActions,
  useWorksiteInterventionFormData,
  useWorksiteLocationFormData,
} from "@stores/forms/worksiteForm";

export interface WorksiteInterventionRow {
  intervention_type: { slug: string; id: string; location_types: string[] };
  intervention_partner: string;
  is_current: boolean;
  associated_locations: [];
  cost: number | null;
}

const WorksiteInterventionRow: React.FC<{
  interventionTypesGroupedByFamily:
    | FetchAllInterventionTypesFamiliesQuery["family_intervention_type"]
    | [];
  interventionPartners:
    | FetchInterventionPartnersQuery["intervention_partner"]
    | [];

  isReadOnly?: boolean;
}> = ({
  isReadOnly,
  interventionTypesGroupedByFamily,
  interventionPartners,
}) => {
  const { t } = useTranslation();
  const worksiteInterventions = useWorksiteInterventionFormData();
  const worksiteTotalLocations = useWorksiteLocationFormData();
  const { setInterventions } = useWorksiteFormActions(); // Access the store's setInterventions action

  // Initialize the state to open the first row by default
  const [open, setOpen] = useState<boolean[]>(
    worksiteInterventions.map((_, index) => index === 0)
  );
  const [titles, setTitles] = useState<string[]>(
    worksiteInterventions.map((i) => i?.intervention_type?.slug || "")
  );
  // Initialize state for dynamic total associated locations
  const [totalAssociatedLocations, setTotalAssociatedLocations] =
    useState<number>(0);
  const [progress, setProgress] = useState<number>(0);

  // Function to check if an ID is in worksite total locations
  const isLocationIdInGlobalLocations = (location, globalLocations) => {
    const locationId =
      location.tree_id || location.location_id || location.vegetated_area_id;
    return globalLocations.some((globalLocation) =>
      [
        globalLocation.tree_id,
        globalLocation.location_id,
        globalLocation.vegetated_area_id,
      ].includes(locationId)
    );
  };

  const countAssociatedLocations = (interventions, globalLocations) => {
    const uniqueValidLocationIds = new Set(); // To store unique IDs that are valid

    interventions.forEach((intervention) => {
      intervention?.associated_locations?.forEach((location) => {
        // Prioritize tree_id, then location_id, then vegetated_area_id
        const locationId =
          location.tree_id ||
          location.location_id ||
          location.vegetated_area_id;

        // Ensure that tree_id or location_id is unique and valid
        if (
          locationId &&
          isLocationIdInGlobalLocations(location, globalLocations) &&
          !uniqueValidLocationIds.has(locationId)
        ) {
          uniqueValidLocationIds.add(locationId); // Add only if it's unique
        }
      });
    });

    return uniqueValidLocationIds.size;
  };

  // Handle adding a new row
  const handleAddRow = () => {
    const newRow = {
      intervention_type: { slug: "", id: "", location_types: [] },
      intervention_partner: "",
      is_current: false,
      associated_locations: [],
    };
    const newRows = [...worksiteInterventions, newRow];

    setOpen([...open, false]); // New row should be closed by default
    setTitles([...titles, ""]);

    // Update the store with the new row
    setInterventions(newRows);
  };

  // Handle deleting a row
  const handleDeleteRow = (index: number) => {
    const newRows = worksiteInterventions.filter((_, i) => i !== index);
    setOpen(open.filter((_, i) => i !== index));
    setTitles(titles.filter((_, i) => i !== index));
    setInterventions(newRows);
  };

  // Handle toggling a row open/closed
  const handleToggleOpen = (index: number) => {
    const updatedInterventions = worksiteInterventions.map(
      (intervention, i) => ({
        ...intervention,
        is_current: i === index, // Only the selected row becomes the current one
      })
    );
    setInterventions(updatedInterventions);
    // Update the open state for the rows
    setOpen(open.map((state, i) => (i === index ? !state : false)));
  };

  const handleChange = (
    index: number,
    field: keyof WorksiteInterventionRow,
    value:
      | string
      | number
      | { id: string; slug: string; location_types: string[] },
    label: string = ""
  ) => {
    const updatedInterventions = worksiteInterventions.map((intervention, i) =>
      i === index && intervention?.is_current
        ? { ...intervention, [field]: value }
        : intervention
    );
    setInterventions(updatedInterventions);
    // Update the title if the field is "intervention_type"
    if (field === "intervention_type") {
      setTitles(titles.map((title, i) => (i === index ? label : title)));
    }
  };

  useEffect(() => {
    const newTotalAssociatedLocations = countAssociatedLocations(
      worksiteInterventions,
      worksiteTotalLocations
    );
    setTotalAssociatedLocations(newTotalAssociatedLocations);

    // Calculate progress percentage
    const totalLocations = worksiteTotalLocations.length;
    const progressPercentage =
      totalLocations > 0
        ? (newTotalAssociatedLocations / totalLocations) * 100
        : 0;
    setProgress(progressPercentage);
  }, [worksiteInterventions, worksiteTotalLocations]);
  return (
    <Box mb={4}>
      {!isReadOnly && (
        <Box sx={{ textAlign: "center", mb: 1 }}>
          <Button variant="contained" color="primary" onClick={handleAddRow}>
            {t(`components.WorksiteForm.interventions.add`)}
          </Button>
        </Box>
      )}
      {worksiteInterventions.map((row, index) => (
        <Paper elevation={1} key={index} sx={{ p: 1, m: 1 }}>
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
          >
            {titles[index] && (
              <Typography variant="subtitle1" sx={{ mr: 2 }}>
                {titles[index] !== ""
                  ? t(
                      `components.Interventions.interventionsList.${titles[index]}`
                    )
                  : ""}
              </Typography>
            )}
            <IconButton onClick={() => handleToggleOpen(index)}>
              <ExpandMoreIcon
                sx={{
                  transform: open[index] ? "rotate(0deg)" : "rotate(180deg)",
                  transition: "transform 0.3s ease",
                }}
              />
            </IconButton>
          </Box>

          <Collapse in={open[index]}>
            <Grid
              container
              spacing={1}
              key={index}
              alignItems="center"
              mt={0.1}
            >
              <Alert severity="info" sx={{ flexGrow: 1 }}>
                <AlertTitle>
                  {t(`components.WorksiteForm.interventions.infos`)}
                </AlertTitle>
              </Alert>
              <Grid item xs={6}>
                <Autocomplete
                  id={`autocomplete-${index}`}
                  options={interventionTypesGroupedByFamily.flatMap((type) =>
                    type.intervention_types.map((t) => ({
                      ...t,
                      family: type.slug,
                    }))
                  )}
                  value={
                    interventionTypesGroupedByFamily
                      .flatMap((type) =>
                        type.intervention_types.map((t) => ({
                          ...t,
                          family: type.slug,
                        }))
                      )
                      .find(
                        (option: any) =>
                          option.slug ===
                          worksiteInterventions[index]?.intervention_type?.slug
                      ) || null
                  }
                  groupBy={(option) => option.family}
                  renderInput={(params: AutocompleteRenderInputParams) => (
                    <TextField
                      {...params}
                      required
                      label={
                        t(
                          `components.Intervention.properties.interventionType`
                        ) as string
                      }
                      variant="filled"
                      InputLabelProps={{ shrink: true }}
                    />
                  )}
                  getOptionLabel={(option) => {
                    if (option.slug !== undefined) {
                      return t(
                        `components.Interventions.interventionsList.${option.slug}`
                      );
                    } else {
                      return "";
                    }
                  }}
                  renderOption={(props, option) => {
                    const icon = option.location_types.includes("tree") ? (
                      <ParkIcon style={{ color: "#6F6F70" }} />
                    ) : option.location_types.includes("stump") ? (
                      <StumpIcon style={{ color: "#6F6F70" }} />
                    ) : option.location_types.includes("empty_location") ? (
                      <EmptyIcon style={{ color: "#6F6F70" }} />
                    ) : null;

                    return (
                      <li {...props}>
                        <span style={{ display: "flex", alignItems: "center" }}>
                          {icon}
                          <span style={{ marginLeft: 5 }}>
                            {t(
                              `components.Interventions.interventionsList.${option.slug}`
                            )}
                          </span>
                        </span>
                      </li>
                    );
                  }}
                  renderGroup={(params) => (
                    <div key={params.key}>
                      <Typography
                        variant="subtitle1"
                        sx={{ fontWeight: "bold", ml: 2 }}
                      >
                        {t(
                          `components.Dashboard.interventions.family.${params.group}`
                        )}
                      </Typography>
                      <ul
                        style={{
                          listStyleType: "none",
                          padding: 0,
                          margin: 0,
                        }}
                      >
                        {React.Children.map(params.children, (child, index) => (
                          <React.Fragment key={`${params.group}-${index}`}>
                            <li>{child}</li>
                          </React.Fragment>
                        ))}
                      </ul>
                    </div>
                  )}
                  onChange={(e, newValue) => {
                    if (newValue) {
                      handleChange(
                        index,
                        "intervention_type",
                        {
                          id: newValue.id,
                          slug: newValue.slug,
                          location_types: newValue.location_types || [],
                        },
                        newValue.slug || ""
                      );
                    }
                  }}
                />
              </Grid>
              <Grid item xs={6}>
                <Autocomplete
                  id={`autocomplete-${index}`}
                  options={interventionPartners}
                  value={
                    interventionPartners.find(
                      (option: any) =>
                        option.id ===
                        worksiteInterventions[index]?.intervention_partner
                    ) || null
                  }
                  renderInput={(params: AutocompleteRenderInputParams) => (
                    <TextField
                      {...params}
                      label={
                        t(
                          `components.Intervention.properties.interventionPartner`
                        ) as string
                      }
                      variant="filled"
                      InputLabelProps={{ shrink: true }}
                    />
                  )}
                  getOptionLabel={(option) => {
                    return option.name;
                  }}
                  onChange={(e, newValue) => {
                    handleChange(index, "intervention_partner", newValue?.id);
                  }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  label={
                    t(`components.WorksiteForm.interventions.cost`) as string
                  }
                  variant="filled"
                  InputLabelProps={{ shrink: true }}
                  type={"number"}
                  inputProps={{ min: 0 }}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">€</InputAdornment>
                    ),
                  }}
                  onChange={(e) => handleChange(index, "cost", e.target.value)}
                />
              </Grid>

              <Grid item xs={8}>
                <Typography
                  variant="body2"
                  display={"flex"}
                  sx={{
                    backgroundColor: "#E0E0E0",
                    color: "#434A4A",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    gap: 2,
                    p: 1,
                    borderRadius: 6, // Rounded corners for the box
                    m: "auto",
                  }}
                >
                  <Box
                    sx={{
                      width: 24, // Circle diameter
                      height: 24,
                      backgroundColor: "darkgray", // Darker gray circle
                      borderRadius: "50%", // Make it a circle
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      color: worksiteInterventions[index]?.associated_locations
                        ?.length
                        ? "#616161"
                        : "red",
                    }}
                  >
                    {worksiteInterventions[index]?.associated_locations
                      ?.length || 0}
                  </Box>
                  {t(
                    `components.WorksiteForm.interventions.associatedLocations`
                  )}
                </Typography>
              </Grid>

              {!isReadOnly && (
                <Grid
                  item
                  xs={4}
                  display={"flex"}
                  justifyContent={"flex-end"}
                  alignItems={"flex-end"}
                >
                  <IconButton
                    color="secondary"
                    onClick={() => handleDeleteRow(index)}
                  >
                    <DeleteIcon />
                  </IconButton>
                </Grid>
              )}
            </Grid>
          </Collapse>
        </Paper>
      ))}

      <Box p={1}>
        <LinearProgress variant="determinate" value={progress} />
        <Typography variant="body1" color="#3A354199" mt={1}>
          {totalAssociatedLocations}/{worksiteTotalLocations.length}{" "}
          {t("components.WorksiteForm.interventions.locationsWithIntervention")}
        </Typography>
      </Box>
    </Box>
  );
};

export default WorksiteInterventionRow;
