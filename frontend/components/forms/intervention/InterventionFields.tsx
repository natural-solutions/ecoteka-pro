import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Control, Controller, UseFormSetValue } from "react-hook-form";
import { MVTLayer } from "@deck.gl/geo-layers";
import { MVTLayerProps } from "@deck.gl/geo-layers/typed";
import {
  Grid,
  TextField,
  Autocomplete,
  AutocompleteRenderInputParams,
  Box,
  Switch,
  InputAdornment,
  Typography,
  Alert,
  AlertTitle,
  Tooltip,
  FormControlLabel,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";

import { FreeSolo, IFreeSoloOption } from "../autocomplete/FreeSolo";
import { FormInputError } from "../error/FormInputError";
import {
  FetchDataInterventionsGridQuery,
  FetchInterventionPartnersQuery,
  FetchOneInterventionQuery,
  Intervention_Type,
} from "../../../generated/graphql";
import {
  useMapActiveAction,
  useMapStyle,
} from "../../../lib/store/pages/mapEditor";
import { InterventionCardMap } from "@components/_core/cards";
import useStore from "@stores/useStore";
import getConfig from "next/config";
import { Feature } from "maplibre-gl";
import { useRouter } from "next/router";
import Link from "next/link";
import useMapHandlers from "@helpers/map/useMapHandlers";
import { REQUEST_ORIGIN } from "@lib/constants";

export interface Intervention {
  id?: string;
  realization_date?: string;
  scheduled_date?: string;
  location: {
    id: string;
    address: string;
    locationId?: string;
    treeId?: string;
  };
  intervention_type?: any;
  intervention_partner?: any;
  note: string;
  validation_note?: string;
  cost?: Number;
  request_origin?: string;
  is_parking_banned?: boolean;
}

export interface InterventionFieldsProps {
  interventionTypesGroupedByFamily: FetchDataInterventionsGridQuery["family_intervention_type"];
  interventionPartners: FetchInterventionPartnersQuery["intervention_partner"];
  activeIntervention?: FetchOneInterventionQuery["intervention"][0];
  customControl: Control<any>;
  customSetValue: UseFormSetValue<any>;
  errors: any;
  onUpdateIntervention?(data: Intervention): void;
  onCreateIntervention?(data: Intervention): void;
  onDeleteIntervention?(id: string): void;
  onValidateIntervention?(
    id: string,
    interventionDate: string,
    interventionPartner: string,
    interventionNote: string
  ): void;
  hasTree?: boolean;
  goToNextStep?: () => void;
  location?: any;
  reset?: any;
  userRole?: string[] | undefined;
}

export interface SelectedLocationProps {
  status_name: string;
  location_id: string;
  location_address: string;
  tree_serial_number: string | undefined | null;
  tree_id: string;
}

const InterventionFields: FC<InterventionFieldsProps> = ({
  interventionTypesGroupedByFamily,
  interventionPartners,
  activeIntervention,
  customControl,
  customSetValue,
  errors,
  hasTree,
  location,
  reset,
  userRole,
}) => {
  const { t } = useTranslation(["components", "common"]);
  const { app } = useStore((store) => store);
  const router = useRouter();
  const activeInterventionId = router.query.activeId;
  const { publicRuntimeConfig } = getConfig();
  const [selectedLocation, setSelectedLocation] =
    useState<SelectedLocationProps>();

  const initialViewStateConfig = {
    longitude: app?.organization?.coords.coordinates[0],
    latitude: app?.organization?.coords.coordinates[1],
    zoom: selectedLocation || activeIntervention ? 17 : 13,
    bearing: 0,
    pitch: 0,
  };
  const {
    viewState,
    setViewState,
    handleOnViewStateChange,
    handleOnZoom,
    debouncedHandleOnMapClick,
    handleOnMapClick,
  } = useMapHandlers(initialViewStateConfig, setSelectedLocation);

  const mapStyle = useMapStyle();
  const [layers, setLayers] = useState<any[]>();
  const [interventionTypesFiltered, setInterventionTypesFiltered] = useState(
    interventionTypesGroupedByFamily
  );
  const [interventionIsDone, setInterventionIsDone] = useState<boolean>(false);
  const [readOnly, setReadOnly] = useState(false);
  // get default organization
  const mapActiveAction = useMapActiveAction();
  const now = new Date();
  const fields = [
    {
      type: "textReadOnly",
      label: "location",
      column: hasTree ? "tree_id" : "location_id",
    },
    {
      type: "autocomplete",
      label: "interventionType",
      column: "intervention_type",
    },
    {
      type: "autocomplete",
      label: "interventionPartner",
      column: "intervention_partner",
    },
    { type: "date", label: "realizationDate", column: "realization_date" },
    { type: "date", label: "scheduledDate", column: "scheduled_date" },
    { type: "number", label: "cost", column: "cost" },
    { type: "select", label: "request_origin", column: "request_origin" },
    {
      type: "boolean",
      label: "is_parking_banned",
      column: "is_parking_banned",
    },
    { type: "textArea", label: "note", column: "note" },
  ];

  const isFeatureSelected = (currentFeature: Feature): boolean => {
    const locationId = currentFeature.properties.location_id;
    return selectedLocation?.location_id === locationId;
  };

  const getFillColor = (d) => {
    if (isFeatureSelected(d)) {
      return [255, 243, 80, 255];
    }
    if (d?.properties?.status_color) {
      return JSON.parse(d?.properties?.status_color);
    }
    return [0, 0, 0, 255];
  };

  const getLineColor = (d) => {
    if (d.properties.status_name === "empty") {
      return [47, 163, 124, 255];
    }
    return [0, 0, 0, 0];
  };

  const createLocationsLayer = () => {
    const ecotekaLocations: MVTLayerProps = new MVTLayer({
      // TODO : add to NEXT_PUBLIC_TILESERVICE_URL
      data: `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/martin/filter_locations/{z}/{x}/{y}`,
      id: "locations",
      // @ts-ignore
      getRadius: 3,
      opacity: 0.9,
      stroked: true,
      pickable: activeIntervention || location ? false : true,
      filled: true,
      extruded: true,
      pointType: "circle",
      pointRadiusUnits: "meters",
      pointRadiusMinPixels: 2,
      pointRadiusMaxPixels: 10,
      pointRadiusScale: 1,
      autoHighlight: true,
      visible: true,
      lineWidthMaxPixels: 1,
      getFillColor: (d) => getFillColor(d),
      getLineColor: (d) => getLineColor(d),
      updateTriggers: {
        getFillColor: [(!activeIntervention || !location) && selectedLocation],
      },
      onClick: (d) => handleOnMapClick(d),
    });
    setLayers([ecotekaLocations]);
    return ecotekaLocations;
  };
  useEffect(() => {
    createLocationsLayer();
  }, [selectedLocation]);

  useEffect(() => {
    if (selectedLocation && !activeIntervention) {
      const status = selectedLocation?.status_name;
      const locationTypeFilter = {
        stump: "stump",
        empty: "empty_location",
        default: "tree",
      };
      customSetValue("location", {
        locationId: selectedLocation?.location_id || null,
        treeId: selectedLocation?.tree_id || null,
        address: selectedLocation?.location_address,
      });
      const interventionSelected = interventionTypesGroupedByFamily
        .flatMap((type) => type.intervention_types)
        .find((intervention) => intervention.slug === router.query.type);
      customSetValue(
        "intervention_type",
        router.query.type ? interventionSelected : ""
      );
      const filteredTypes = interventionTypesGroupedByFamily
        ?.map((type) => ({
          __typename: type.__typename,
          slug: type.slug,
          intervention_types: type.intervention_types.filter((intervention) =>
            intervention.location_types?.includes(
              locationTypeFilter[status] || locationTypeFilter.default
            )
          ),
        }))
        .filter((type) => type.intervention_types.length > 0);

      setInterventionTypesFiltered(filteredTypes);

      location &&
        setViewState({
          ...viewState,
          longitude: location?.coords?.coordinates[0],
          latitude: location?.coords?.coordinates[1],
          zoom: 20,
        });
    }
  }, [selectedLocation, location]);

  useEffect(() => {
    const handleLocationAndIntervention = () => {
      let transformedLocation;
      let initialValues = {
        intervention_partner: "",
        intervention_type: "",
        cost: "",
        note: "",
        location: {
          locationId: "",
          treeId: "",
          address: "",
        },
        scheduled_date: "",
        request_origin: "",
        is_parking_banned: null,
      };

      if (activeIntervention) {
        const { location, tree } = activeIntervention;
        transformedLocation = {
          status_name: location?.location_status?.status || "",
          location_id: tree ? tree.location_id : location?.id || "",
          location_address: location?.address || "",
          tree_serial_number: tree ? tree.serial_number : "",
          tree_id: tree ? tree?.id : "",
        };
        let coordinates = location?.coords;

        if (tree && tree.location?.coords) {
          coordinates = tree.location.coords;
        }
        setViewState((prevViewState) => ({
          ...prevViewState,
          longitude: coordinates?.coordinates[0] || 0,
          latitude: coordinates?.coordinates[1] || 0,
        }));

        // Set location data for active intervention
        customSetValue("location", {
          locationId: tree ? tree.location_id : location?.id || "",
          treeId: tree ? tree?.id : "",
          address: location?.address || "",
        });

        reset({
          ...activeIntervention,
          location: {
            locationId: tree ? tree.location_id : location?.id || "",
            treeId: tree ? tree?.id : "",
            address: location?.address || "",
          },
        });

        if (Boolean(activeIntervention?.realization_date)) {
          setInterventionIsDone(true);
          setReadOnly(true);
        }
        if (activeIntervention?.worksite?.id) {
          setReadOnly(true);
        }
      } else {
        if (location) {
          transformedLocation = {
            status_name: location?.location_status?.status || "",
            location_id: location?.id,
            location_address: location?.address,
            tree_serial_number: location?.tree?.[0]?.serial_number || "",
            tree_id: location?.tree?.[0]?.id || location?.tree?.id || "",
          };

          initialValues.location = {
            locationId: location?.id || "",
            treeId: location?.tree?.[0]?.id || location?.tree?.id || "",
            address: location?.address || "",
          };
        }

        const interventionSelected = interventionTypesGroupedByFamily
          .flatMap((type) => type.intervention_types)
          .find((intervention) => intervention.slug === router.query.type);
        if (router.query.type && interventionSelected) {
          initialValues.intervention_type = interventionSelected.slug;
          customSetValue(
            "intervention_type",
            interventionSelected as Intervention_Type
          );
        }

        reset(initialValues);
      }
      setSelectedLocation(transformedLocation);
    };

    handleLocationAndIntervention();
  }, [
    location,
    activeIntervention,
    interventionTypesGroupedByFamily,
    reset,
    customSetValue,
  ]);

  return (
    <Grid container spacing={0}>
      {(!location || (!activeInterventionId && !location.id)) && (
        <Alert severity="info" sx={{ width: "100%" }}>
          <AlertTitle>{t(`Intervention.interventionMap.infos`)}</AlertTitle>
        </Alert>
      )}
      <InterventionCardMap
        handleOnViewStateChange={handleOnViewStateChange}
        initialViewState={viewState}
        mapStyle={mapStyle}
        layers={layers ? layers : []}
        onClick={
          activeIntervention || location ? () => {} : debouncedHandleOnMapClick
        }
        handleOnZoom={handleOnZoom}
        cursor={(activeIntervention || location) && "auto"}
      />
      {mapActiveAction !== "addWorksiteGroup" && !activeInterventionId && (
        <Box>
          {t(`components.InterventionForm.message.alreadyDone`) as string}
          <Switch
            checked={interventionIsDone}
            color="primary"
            disabled={Boolean(activeIntervention?.realization_date)}
            onChange={(e) => {
              setInterventionIsDone(!interventionIsDone);
              customSetValue("scheduled_date", now.toISOString().split("T")[0]);
              customSetValue(
                "realization_date",
                now.toISOString().split("T")[0]
              );
            }}
          />
        </Box>
      )}
      {activeIntervention?.worksite && activeIntervention?.worksite?.id && (
        <Typography>
          Cette intervention appartient à un{" "}
          <Link
            href={`/worksites/${activeIntervention?.worksite?.id}`}
            style={{ textDecoration: "underline" }}
          >
            chantier
          </Link>
          , vous pouvez seulement la déclarer réalisée ou non
        </Typography>
      )}
      <Grid container spacing={1}>
        {fields.map((fieldName, i) => (
          <Grid item xs={12} key={i}>
            {fieldName?.label === "interventionType" && (
              <>
                <Controller
                  name={`${fieldName.column as keyof Intervention}`}
                  rules={{ required: true }}
                  control={customControl}
                  render={({ field: { onChange, value } }) => {
                    value = value ?? null;

                    return (
                      <Autocomplete
                        id={`autocomplete-${name}`}
                        options={interventionTypesFiltered.flatMap((type) =>
                          type.intervention_types.map((t) => ({
                            ...t,
                            family: type.slug,
                          }))
                        )}
                        value={value}
                        groupBy={(option) => option.family}
                        renderInput={(
                          params: AutocompleteRenderInputParams
                        ) => (
                          <TextField
                            {...params}
                            required
                            label={
                              t(
                                `components.Intervention.properties.${fieldName?.label}`
                              ) as string
                            }
                            variant="filled"
                            InputLabelProps={{ shrink: true }}
                          />
                        )}
                        getOptionLabel={(option) => {
                          if (option.slug !== undefined) {
                            return t(
                              `components.Interventions.interventionsList.${option.slug}`
                            );
                          }
                          return "";
                        }}
                        renderGroup={(params) => (
                          <div key={params.key}>
                            <Typography
                              variant="subtitle1"
                              sx={{ fontWeight: "bold", ml: 2 }}
                            >
                              {t(
                                `components.Dashboard.interventions.family.${params.group}`
                              )}
                            </Typography>
                            <ul
                              style={{
                                listStyleType: "none",
                                padding: 0,
                                margin: 0,
                              }}
                            >
                              {React.Children.map(
                                params.children,
                                (child, index) => (
                                  <React.Fragment
                                    key={`${params.group}-${index}`}
                                  >
                                    <li>{child}</li>
                                  </React.Fragment>
                                )
                              )}
                            </ul>
                          </div>
                        )}
                        onChange={(e, newValue) => {
                          onChange(newValue);
                        }}
                        isOptionEqualToValue={(option, value) =>
                          option.slug === value.slug
                        }
                        disabled={readOnly}
                      />
                    );
                  }}
                />
                <FormInputError name={fieldName.column!} errors={errors} />
              </>
            )}

            {fieldName?.label === "interventionPartner" && (
              <FreeSolo
                control={customControl}
                setValue={customSetValue}
                name={`${fieldName.column as keyof Intervention}`}
                listOptions={interventionPartners ?? ([] as IFreeSoloOption[])}
                required={false}
                label={
                  t(
                    `components.Intervention.properties.${fieldName?.label}`
                  ) as string
                }
                readOnly={readOnly}
                object={"intervention"}
              />
            )}

            {fieldName?.type === "select" && (
              <FormControl fullWidth>
                <InputLabel>
                  {
                    t(
                      `components.Intervention.properties.${fieldName.label}`
                    ) as string
                  }
                </InputLabel>
                <Controller
                  name={`${fieldName?.column as keyof Intervention}`}
                  control={customControl}
                  render={({ field: { onChange, value } }) => (
                    <Select
                      value={value}
                      onChange={onChange}
                      disabled={readOnly}
                      label={
                        t(
                          `components.Intervention.properties.${fieldName.label}`
                        ) as string
                      }
                    >
                      {REQUEST_ORIGIN.map((option: string, i: React.Key) => {
                        return (
                          <MenuItem key={i} value={option}>
                            {
                              t(
                                `components.Intervention.origins.${option}`
                              ) as string
                            }
                          </MenuItem>
                        );
                      })}
                    </Select>
                  )}
                />
                <FormInputError name={fieldName.column || ""} errors={errors} />
              </FormControl>
            )}
            {fieldName.column === "scheduled_date" && (
              <>
                <Controller
                  name={`${fieldName?.column as keyof Intervention}`}
                  control={customControl}
                  rules={{ required: true }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextField
                        required
                        InputProps={{
                          inputProps: {
                            min: now.toISOString().split("T")[0],
                          },
                        }}
                        label={
                          t(
                            `components.Intervention.properties.${fieldName?.label}`
                          ) as string
                        }
                        variant="filled"
                        type="date"
                        disabled={readOnly}
                        sx={{
                          width: "100%",
                          display: interventionIsDone ? "none" : "auto",
                        }}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        value={value}
                        onChange={(e) => onChange(e.target.value)}
                      />
                    );
                  }}
                />
                <FormInputError name={fieldName.column} errors={errors} />
              </>
            )}
            {fieldName?.label === "location" && (
              <>
                <Controller
                  name={"location.address"}
                  control={customControl}
                  defaultValue={{}}
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextField
                        fullWidth
                        disabled={true}
                        value={
                          selectedLocation?.location_address !== ""
                            ? selectedLocation?.location_address
                            : selectedLocation?.tree_serial_number
                              ? selectedLocation.tree_serial_number
                              : t(`common.${selectedLocation?.status_name}`) // Display status name if neither address nor serial number exists
                        }
                        required
                        sx={{
                          display: "flex",
                        }}
                        label={
                          t(
                            `components.Intervention.properties.${fieldName?.label}`
                          ) as string
                        }
                        variant="filled"
                        InputLabelProps={{ shrink: true }}
                        type={fieldName?.type}
                        error={!!errors.location}
                        helperText={t("common.errors.required")}
                      />
                    );
                  }}
                />
              </>
            )}
            {(fieldName?.type === "text" ||
              fieldName?.type === "textArea" ||
              fieldName?.type === "number") && (
              <Controller
                name={`${fieldName?.column as keyof Intervention}`}
                control={customControl}
                render={({ field: { onChange, ref, value } }) => (
                  <Tooltip
                    title={
                      !userRole?.includes("admin") &&
                      fieldName?.label === "cost"
                        ? t(`common.errors.roles.adminOnly`)
                        : ""
                    }
                  >
                    <TextField
                      value={value}
                      inputRef={ref}
                      fullWidth
                      disabled={
                        readOnly ||
                        (!userRole?.includes("admin") &&
                          fieldName?.label === "cost")
                      }
                      sx={{
                        display: "flex",
                      }}
                      multiline={fieldName?.type === "textArea" ? true : false}
                      label={
                        t(
                          `components.Intervention.properties.${fieldName?.label}`
                        ) as string
                      }
                      variant="filled"
                      placeholder={t(
                        `components.Intervention.placeholder.${fieldName?.label}`
                      )}
                      error={!!errors[fieldName?.label]}
                      helperText={errors[fieldName?.label]?.message}
                      onChange={onChange}
                      type={fieldName?.type}
                      inputProps={
                        fieldName?.type === "number" ? { min: 0 } : {}
                      }
                      InputProps={{
                        endAdornment: fieldName?.label === "cost" && (
                          <InputAdornment position="end">€</InputAdornment>
                        ),
                      }}
                    />
                  </Tooltip>
                )}
              />
            )}
            {fieldName?.type === "boolean" && (
              <Controller
                name={`${fieldName?.column as keyof Intervention}`}
                control={customControl}
                defaultValue={
                  (activeIntervention &&
                    activeIntervention[fieldName.column]) ||
                  false
                }
                render={({ field: { onChange, ref, value } }) => (
                  <FormControlLabel
                    label={
                      t(
                        `components.Intervention.properties.${fieldName?.label}`
                      ) as string
                    }
                    control={
                      <Switch
                        disabled={readOnly}
                        ref={ref}
                        checked={value as boolean}
                        color="primary"
                        onChange={(e) => onChange(e.target.checked)}
                      />
                    }
                    labelPlacement="end"
                  />
                )}
              />
            )}

            {fieldName.column === "realization_date" && interventionIsDone && (
              <>
                <Controller
                  name={`${fieldName?.column as keyof Intervention}`}
                  control={customControl}
                  render={({ field: { onChange, value } }) => {
                    return (
                      <TextField
                        InputProps={{
                          inputProps: {
                            max: now.toISOString().split("T")[0],
                          },
                        }}
                        label={
                          t(
                            `components.Intervention.properties.${fieldName?.label}`
                          ) as string
                        }
                        variant="filled"
                        type="date"
                        disabled={readOnly}
                        sx={{ width: "100%" }}
                        InputLabelProps={{
                          shrink: true,
                        }}
                        value={value}
                        onChange={(e) => onChange(e.target.value)}
                      />
                    );
                  }}
                />
                <FormInputError name={fieldName.column} errors={errors} />
              </>
            )}
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

export default InterventionFields;
