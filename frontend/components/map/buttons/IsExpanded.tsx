import React, { useState } from "react";
import {
  Button
} from "@mui/material";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';

interface IsExpandedProps {
  onToggleExpanded: (newState: boolean) => void;
  onHoverChange: (isHovered: boolean) => void;
}

const IsExpanded: React.FC<IsExpandedProps> = ({ onToggleExpanded, onHoverChange }) => {
  const [isExpanded, setIsExpanded] = useState<boolean>(true);
  const [isButtonHovered, setIsButtonHovered] = useState<boolean>(false);

  const toggleIsExpanded = () => {
    setIsExpanded(!isExpanded);
    onToggleExpanded(!isExpanded);
    setIsButtonHovered(false);
    onHoverChange(isButtonHovered)
  }

  return (
    <Button className="btn-expanded" onMouseEnter={() => onHoverChange(true)} onMouseLeave={() => onHoverChange(false)} variant="outlined" onClick={toggleIsExpanded} sx={{margin: "0 16px 16px 0", padding: '0px 6px', border: '1px solid rgba(67, 74, 74,.3)', borderTop: 'none', borderRadius: '0 0 5px 5px', minWidth: 'fit-content', alignSelf: 'flex-end', height: '20px'}}>
      {isExpanded ?
        <KeyboardArrowUpIcon className="icon-expanded" />
                :
        <KeyboardArrowDownIcon className="icon-expanded" />
      }
    </Button>
  );
};

export default IsExpanded;
