import { FC } from "react";
import { Fab, Button } from "@mui/material";
import TuneIcon from "@mui/icons-material/Tune";
import { MapActionsBarActionType } from "../../../lib/store/pages/mapEditor";
import RenderFilterBadge from "../badges/RenderFilterBadge";
import { useDeviceSize } from "@lib/utils";
import { useTranslation } from "react-i18next";

export interface FilterButtonProps {
  handleOnActionClick(action: MapActionsBarActionType): void;
}

const FilterButton: FC<FilterButtonProps> = ({ handleOnActionClick }) => {
  const { isTablet, isMobile } = useDeviceSize();
  const { t } = useTranslation(["components"]);

  const handleFilterClick = async () => {
    handleOnActionClick("filter");
  };

  const button = (
    <Button
      sx={{ width: isMobile ? "320px" : "180px" }}
      startIcon={<TuneIcon color="primary" />}
      size="medium"
      variant="contained"
      color="inherit"
      aria-label="filter"
      onClick={handleFilterClick}
      data-cy="map-actions-filter-button-desktop"
    >
      {t("components.Map.Filter.verb")}
    </Button>
  );

  return <RenderFilterBadge isTablet={isTablet}>{button}</RenderFilterBadge>;
};

export default FilterButton;
