import { WorksiteGroupIcon } from "@components/icons/WorksiteGroupIcon";
import { MenuItem, SvgIcon } from "@mui/material";
import { useMapEditorActions } from "@stores/pages/mapEditor";
import { FC } from "react";
import { useTranslation } from "react-i18next";

const AddWorksiteGroupButton: FC = () => {
  const { t } = useTranslation(["common"]);
  const { setMapActiveAction, setMapPanelOpen } = useMapEditorActions();

  const handleOnAddWorksiteGroup = () => {
    setMapActiveAction("addWorksiteGroup");
    setMapPanelOpen(true);
  };

  return (
    <MenuItem
      sx={{
        zIndex: 4,
        bgcolor: "#ffff",
        "&:hover": {
          backgroundColor: "#EEEEEE",
        },
        width: "auto",
        justifyContent: "flex-start",
      }}
      onClick={handleOnAddWorksiteGroup}
    >
      <SvgIcon sx={{ mr: 1 }}>
        <WorksiteGroupIcon />
      </SvgIcon>
      {t("buttons.addInterventionGroup") as string}
    </MenuItem>
  );
};

export default AddWorksiteGroupButton;
