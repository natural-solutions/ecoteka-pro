import { FC } from "react";
import { MenuItem, SvgIcon, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import { LocationType } from "../../../lib/store/pages/mapEditor";
import { AddTreeIcon } from "../../icons/AddTreeIcon";
import { AddStumpIcon } from "../../icons/AddStumpIcon";
import { AddLocationIcon } from "../../icons/AddLocationIcon";

export interface AddLocationbuttonProps {
  handleOnAddLocation(type: LocationType): void;
  locationStatus: LocationType;
  modalContext?: boolean;
}

const AddLocationButton: FC<AddLocationbuttonProps> = ({
  handleOnAddLocation,
  locationStatus,
  modalContext,
}) => {
  const { t } = useTranslation();

  const handleOnClick = () => {
    handleOnAddLocation(locationStatus);
  };

  return (
    <MenuItem
      onClick={handleOnClick}
      sx={{
        zIndex: 4,
        bgcolor: modalContext ? "#06b6ae" : "#ffff",
        "&:hover": {
          backgroundColor: modalContext ? "#06b6ae" : "#EEEEEE",
        },
        borderRadius: modalContext ? "4px" : "50px 0 0 50px",
        width: modalContext ? "250px" : "auto",
        justifyContent: modalContext ? "center" : "flex-start",
        color: modalContext ? "#fff" : "primary",
      }}
    >
      <SvgIcon>
        {locationStatus === "alive" && (
          <AddTreeIcon fillColor="#6F6F70" strokeColor="#6F6F70" />
        )}
        {locationStatus === "stump" && (
          <AddStumpIcon fillColor="black" fillOpacity="0.54" />
        )}
        {locationStatus === "empty" && (
          <AddLocationIcon fillColor={"black"} fillOpacity="0.54" />
        )}
      </SvgIcon>
      <Typography color="fff">
        {locationStatus === "alive"
          ? (t("common.addTree") as string)
          : locationStatus === "empty"
          ? (t("common.addEmptyLocation") as string)
          : (t("common.addStump") as string)}
      </Typography>
    </MenuItem>
  );
};
export default AddLocationButton;
