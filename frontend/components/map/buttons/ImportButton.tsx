import { MenuItem, SvgIcon } from "@mui/material";
import { useRouter } from "next/router";
import { FC } from "react";
import { useTranslation } from "react-i18next";

const ImportIcon = () => {
  return (
    <svg
      width="24"
      height="16"
      viewBox="0 0 24 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L12 4L17 9H14Z"
        fill="black"
        fillOpacity="0.54"
      />
    </svg>
  );
};

export interface ImportButtonProps {
  modalContext?: boolean;
}

const ImportButton: FC<ImportButtonProps> = ({ modalContext }) => {
  const { t } = useTranslation();
  const router = useRouter();
  return (
    <MenuItem
      sx={{
        zIndex: 4,
        bgcolor: modalContext ? "#06B6AE" : "#ffff",
        "&:hover": {
          backgroundColor: modalContext ? "#06B6AE" : "#EEEEEE",
        },
        color: modalContext ? "#fff" : "primary",
        borderRadius: modalContext ? "4px" : 0,
        width: modalContext ? "250px" : "auto",
        justifyContent: modalContext ? "center" : "flex-start",
      }}
      onClick={() => router.push("/import/add")}
    >
      <SvgIcon sx={{ mr: 1 }}>
        <ImportIcon />
      </SvgIcon>
      {t("common.buttons.import") as string}
    </MenuItem>
  );
};

export default ImportButton;
