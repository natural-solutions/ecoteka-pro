import { Box, Modal, Stack, Typography } from "@mui/material";
import Image from "next/image";
import React, { FC } from "react";
import { useTranslation } from "react-i18next";
import { LocationType } from "../../lib/store/pages/mapEditor";
import AddLocationButton from "./buttons/AddLocationButton";

export interface InitialModalProps {
  open: boolean;
  handleClose: () => void;
  handleOnAddLocation(type: LocationType): void;
}

const InitialModal: FC<InitialModalProps> = ({
  open,
  handleClose,
  handleOnAddLocation,
}) => {
  const { t } = useTranslation();
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
      data-cy="map-initial-modal"
    >
      <Box sx={style}>
        <Typography
          sx={{
            fontWeight: 500,
            textTransform: "uppercase",
            mb: 2,
          }}
          data-cy="map-initial-modal-title"
        >
          {t("components.PanelWelcome.title") as string}
        </Typography>
        <Image
          alt="tree"
          src={`/components/map/treeModal.png`}
          width={197}
          height={169}
          data-cy="map-initial-modal-image"
        />
        <Typography
          id="modal-modal-description"
          sx={{ mt: 1, mb: 1, fontSize: 14 }}
          data-cy="map-initial-modal-message"
        >
          {t("components.Map.InitialModal.message") as string}
        </Typography>
        <Stack
          justifyContent={"center"}
          gap={1}
          alignItems={"center"}
          data-cy="map-initial-modal-buttons-stack"
        >
          <Box onClick={handleClose} role="button">
            <AddLocationButton
              handleOnAddLocation={handleOnAddLocation}
              locationStatus="alive"
              modalContext={true}
            />
          </Box>
        </Stack>
      </Box>
    </Modal>
  );
};

export default InitialModal;

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 382,
  height: 371,
  bgcolor: "background.paper",
  borderRadius: "4px",
  boxShadow: 24,
  p: 2,
  textAlign: "center",
};
