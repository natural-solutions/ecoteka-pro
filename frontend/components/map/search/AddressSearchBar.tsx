import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import SearchIcon from "@mui/icons-material/Search";
import { InputAdornment, styled } from "@mui/material";
import { useMapEditorActions, useViewState } from "@stores/pages/mapEditor";
import { FlyToInterpolator } from "deck.gl";
import { fetchLatLong } from "../../../lib/apiAdresse";
import { useTranslation } from "react-i18next";

const StyledInputAdornment = styled(InputAdornment)({
  display: "flex",
  alignItems: "center",
  "& .MuiTypography-root": {
    marginLeft: 8,
  },
});

const AddressSearchBar = () => {
  const [options, setOptions] = useState([]);
  const [inputValue, setInputValue] = useState("");
  const [selectedValue, setSelectedValue] = useState(null);
  const viewState = useViewState();
  const { setViewState } = useMapEditorActions();
  const { t } = useTranslation(["components"]);

  const handleInputChange = (event, value) => {
    setInputValue(value);
    if (value.length > 3) {
      fetch(`https://api-adresse.data.gouv.fr/search/?q=${value}`)
        .then((response) => response.json())
        .then((data) => {
          setOptions(data.features.map((feature) => feature.properties.label));
        })
        .catch((error) => {
          console.error("Error fetching data: ", error);
        });
    }
  };

  const handleOnClickAddress = (x: number, y: number) => {
    const newViewState = {
      longitude: x,
      latitude: y,
      zoom: viewState.zoom,
      pitch: 0,
      bearing: 0,
      transitionDuration: 3000,
      transitionInterpolator: new FlyToInterpolator(),
    };
    setViewState(newViewState);
  };

  const handleOptionSelected = async (event, value) => {
    setSelectedValue(value);
    const coordinates = await fetchLatLong(value);
    coordinates &&
      handleOnClickAddress(coordinates.longitude, coordinates.latitude);
  };

  return (
    <Autocomplete
      sx={styles.searchbar}
      freeSolo
      options={options}
      inputValue={inputValue}
      onInputChange={handleInputChange}
      onChange={handleOptionSelected}
      renderInput={(params) => (
        <TextField
          {...params}
          placeholder={t("Map.Search.label")}
          variant="standard"
          InputProps={{
            ...params.InputProps,
            startAdornment: (
              <StyledInputAdornment position="start">
                <SearchIcon />
              </StyledInputAdornment>
            ),
          }}
        />
      )}
    />
  );
};

export default AddressSearchBar;

const styles = {
  searchbar: {
    width: "320px",
    backgroundColor: "#fff",
    border: "transparent",
    p: 0.2,
    borderRadius: "4px",
  },
};
