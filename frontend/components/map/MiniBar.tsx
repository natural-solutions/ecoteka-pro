import React from "react";
import Box from "@mui/material/Box";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { IconButton } from "@mui/material";

const MiniBar = ({ onClick }) => {
  return (
    <Box
      sx={{
        position: "fixed",
        bottom: 0,
        right: 20,
        display: "flex",
        justifyContent: "center",
        backgroundColor: "rgba(255, 255, 255, 0.7)",
        height: "40px",
        width: "40px",
        borderRadius: "5px 5px 0 0",
      }}
    >
      <IconButton size="medium" aria-label="open-drawer" onClick={onClick}>
        <KeyboardArrowUpIcon />
      </IconButton>
    </Box>
  );
};

export default MiniBar;
