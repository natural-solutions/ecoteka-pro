import React, { FC, useState } from "react";
import {
  Paper,
  Typography,
  Fab,
  FormControl,
  RadioGroup,
  FormControlLabel,
  Radio,
  Avatar,
  Stack,
  IconButton,
} from "@mui/material";
import LayersIcon from "@mui/icons-material/Layers";
import { useTranslation } from "react-i18next";
import { useDeviceSize } from "@lib/utils";
import {
  MapBackground,
  defaultMapBackgrounds,
} from "../../lib/store/pages/mapEditor";

interface BaseMapSelectorProps {
  map?: MapBackground;
  handleOnBaseMapChange?(map: MapBackground): void;
}

interface BaseMapPopoverProps {
  handleOnBaseMapChange?(map: MapBackground): void;
  visible?: boolean;
  mapBackground: MapBackground;
  setVisible?(visible: boolean): void;
  isTablet?: boolean;
}

const BaseMapPopover: FC<BaseMapPopoverProps> = ({
  visible,
  mapBackground,
  handleOnBaseMapChange,
  setVisible,
  isTablet,
}) => {
  const { t } = useTranslation("components");
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = (event.target as HTMLInputElement).value;
    handleOnBaseMapChange && handleOnBaseMapChange(value as MapBackground);
    setVisible && setVisible(false);
  };

  const popoverStyles = {
    position: "absolute",
    top: "-440px", // Adjust this value as needed to position the popover above the LayersIcon
    left: 0,
    zIndex: 999, // Adjust the z-index to make sure it's above other elements
  };

  return visible ? (
    <Paper
      elevation={3}
      sx={[
        popoverStyles,
        isTablet ? styles.popoverMobile : styles.popoverDesktop,
        { order: 2 },
      ]}
      data-cy="basemap-popover-container"
    >
      <FormControl component="fieldset">
        <RadioGroup
          name="background"
          value={mapBackground}
          onChange={handleChange}
          sx={[styles.radio, { flexDirection: isTablet ? "column" : "row" }]}
          data-cy="basemap-popover-radiogroup"
        >
          {defaultMapBackgrounds.map((map: MapBackground) => (
            <FormControlLabel
              key={map}
              sx={styles.buttonContainer}
              value={map}
              control={<Radio sx={{ display: "none" }} color="primary" />}
              data-cy="basemap-popover-item"
              label={
                <Stack sx={styles.button}>
                  <Avatar
                    sx={[
                      styles.image,
                      {
                        border:
                          map === mapBackground ? "2px solid #47B9B2" : "none",
                      },
                    ]}
                    variant="rounded"
                    alt={map}
                    src={`/components/map/${map}.PNG`}
                  />

                  <Typography
                    variant="caption"
                    color={map === mapBackground ? "primary" : "GrayText"}
                  >
                    {t(`components.Map.SatelliteToggle.${map}`) as string}
                  </Typography>
                </Stack>
              }
            />
          ))}
        </RadioGroup>
      </FormControl>
    </Paper>
  ) : null;
};

const BaseMapSelector: FC<BaseMapSelectorProps> = ({
  handleOnBaseMapChange,
  map = MapBackground.Map,
}) => {
  const { t } = useTranslation("components");
  const { isTablet } = useDeviceSize();
  const [visible, setVisible] = useState(false);

  const togglePopover = () => {
    setVisible(!visible);
  };

  return (
    <Stack
      sx={styles.container}
      direction={isTablet ? "column" : "row"}
      alignItems={"end"}
      data-cy="basemap-selector-container"
    >
      <IconButton
        onClick={togglePopover}
        sx={{ p: 0 }}
        data-cy="basemap-selector-button-desktop"
      >
        <LayersIcon sx={{ color: "#7e8989" }} />
      </IconButton>

      <BaseMapPopover
        visible={visible}
        setVisible={setVisible}
        handleOnBaseMapChange={handleOnBaseMapChange}
        mapBackground={map}
        isTablet={isTablet}
      />
    </Stack>
  );
};
export default BaseMapSelector;

const styles = {
  container: {
    ":hover .baseMapContainer": { border: "2px solid  #47B9B2" },
  },
  popoverMobile: {
    marginBottom: 2,
  },
  popoverDesktop: {
    marginLeft: 1,
    marginRight: 1,
  },
  radio: {
    padding: 0.75,
    alignItems: "space-between",
    justifyContent: "space-between",
  },
  buttonContainer: {
    margin: 0,
    width: 74,
    display: "flex",
    justifyContent: "center",
  },
  button: {
    alignItems: "center",
  },
  baseMap: {
    width: 86,
    height: 86,
  },
  image: {
    width: 56,
    height: 56,
    ":hover": { border: "2px solid  #47B9B2" },
  },
  baseMapContainer: {
    border: "2px solid rgba(255,255,255,0.5)",
    position: "relative",
    order: 1,
    cursor: "pointer",
    display: "flex",
  },
  caption: {
    textTransform: "Uppercase",
    position: "absolute",
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 10,
    textAlign: "center",
  },
};
