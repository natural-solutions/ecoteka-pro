import { MVTLayer } from "@deck.gl/geo-layers";
import { TFunction } from "i18next";
import getConfig from "next/config";

const generateLayerDataUrl = (
  layer: string = "",
  forceReload: boolean = false,
  queryStringLocations: string,
  queryStringPolygons: string
) => {
  const { publicRuntimeConfig } = getConfig();
  const timestamp = forceReload ? `?t=${Date.now()}` : "";
  const baseEndpoint = `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/martin/${layer}/{z}/{x}/{y}`;

  let queryString = "";
  if (layer === "filter_locations") {
    queryString = queryStringLocations;
  } else if (layer === "filter_polygons") {
    queryString = queryStringPolygons;
  }

  return `${baseEndpoint}${timestamp}${
    timestamp !== "" ? `&${queryString}` : `?${queryString}`
  }`;
};

const getLineColor = (d) => {
  if (d.properties.status_name === "empty") {
    return [47, 163, 124, 255];
  }
  return [0, 0, 0, 0];
};

let currentTooltip: any = null; // Global reference to the current tooltip

const createTooltip = () => {
  // If a tooltip already exists, remove it
  if (currentTooltip) {
    document.body.removeChild(currentTooltip);
  }
  const tooltip: HTMLDivElement = document.createElement("div");
  tooltip.style.position = "absolute";
  tooltip.style.pointerEvents = "none";
  tooltip.style.display = "none"; // Initially hide tooltip
  document.body.append(tooltip);
  applyTooltipStyles(tooltip, tooltipBaseStyles);
  currentTooltip = tooltip; // Update the global reference
  return tooltip;
};

const applyTooltipStyles = (
  element: HTMLElement,
  styles: { [key: string]: string }
) => {
  for (const [key, value] of Object.entries(styles)) {
    element.style[key] = value;
  }
};

const createLocationsLayer = (
  generateLayerDataUrl: Function,
  forceReloadLayer: boolean,
  handleOnMapClick: (d) => {},
  actions: any,
  queryStringLocations: string,
  isFeatureSelected,
  mapBackground,
  selectedLocationIds,
  mapActiveLocationId,
  worksiteHoveredId,
  setHoveredSelectedLocationId
) => {
  const getFillColor = (d) => {
    const locationId =
      d?.properties?.location_id || d?.properties?.vegetated_area_id;

    if (isFeatureSelected(d)) {
      if (locationId === worksiteHoveredId) {
        return [165, 250, 162]; // Highlight color for hovered feature
      }
      return [255, 243, 80, 255];
    }

    if (d?.properties?.status_color) {
      return JSON.parse(d?.properties?.status_color);
    }

    return [0, 0, 0, 255];
  };
  const ecotekaLocations = new MVTLayer({
    // TODO : add to NEXT_PUBLIC_TILESERVICE_URL
    data: generateLayerDataUrl(
      "filter_locations",
      forceReloadLayer,
      queryStringLocations,
      ""
    ),
    id: "locations",
    // @ts-ignore
    getRadius: 3,
    opacity: 0.9,
    stroked: true,
    pickable: true,
    filled: true,
    extruded: true,
    pointType: "circle",
    pointRadiusUnits: "meters",
    pointRadiusMinPixels: 2,
    pointRadiusMaxPixels: 10,
    pointRadiusScale: 1,
    autoHighlight: true,
    visible: true,
    getFillColor: (d) => getFillColor(d),
    getLineColor: (d) => getLineColor(d),
    lineWidthMaxPixels: 1,
    updateTriggers: {
      getLineColor: [mapBackground, selectedLocationIds, mapActiveLocationId],
      getFillColor: [selectedLocationIds, worksiteHoveredId],
    },
    onClick: (d) => handleOnMapClick(d),
    onHover: (d) => {
      const locationId =
        d?.object?.properties?.location_id ||
        d?.object?.properties?.vegetated_area_id;
      // on hover if item id is one of the selectedLocationIds
      // setHoveredSelectedLocationId
      if (selectedLocationIds.has(locationId)) {
        setHoveredSelectedLocationId(locationId);
      } else {
        setHoveredSelectedLocationId(undefined);
      }
    },
  });
  actions.setMapLayers([ecotekaLocations]);
  return ecotekaLocations;
};

const createPolygonsLayer = (
  generateLayerDataUrl: Function,
  forceReloadLayer: boolean,
  handleOnMapClick: (d) => {},
  actions: any,
  queryStringPolygons: string,
  t: TFunction<["components"], undefined>,
  selectedLocationIds,
  isFeatureSelected,
  worksiteHoveredId,
  setHoveredSelectedLocationId
) => {
  // Function to show tooltip
  const showTooltip = ({ object, x, y }) => {
    // Clear any existing hover timeout
    if (!currentTooltip) {
      createTooltip(); // Create tooltip if none exists
    }
    if (object) {
      currentTooltip.style.display = "block";
      currentTooltip.style.left = `${x}px`;
      currentTooltip.style.top = `${y}px`;

      let backgroundColor;
      if (object.properties.boundary_type === "geographic") {
        backgroundColor = "rgba(255, 68, 225, 0.7)";
      } else if (object.properties.boundary_type === "station") {
        backgroundColor = "rgba(144, 85, 253, 0.7)";
      } else {
        backgroundColor = "rgba(69, 214, 45, 0.7)";
      }
      currentTooltip.style.backgroundColor = backgroundColor;
      currentTooltip.innerText = ["geographic", "station"].includes(
        object.properties.boundary_type
      )
        ? object.properties.boundary_name
        : t(
            `components.VegetatedAreaForm.properties.typeLabels.${object.properties.vegetated_area_type}`
          );
    } else {
      hideTooltip(); // Hide tooltip if no object is hovered
    }
  };

  const hideTooltip = () => {
    if (currentTooltip) {
      currentTooltip.style.display = "none";
      currentTooltip.innerText = ""; // Clear tooltip content
    }
  };

  const removeTooltip = () => {
    if (currentTooltip) {
      document.body.removeChild(currentTooltip);
      currentTooltip = null; // Clear the global reference
    }
  };

  // Function to handle click events and hide the tooltip
  const handleClick = (d) => {
    handleOnMapClick(d);
    removeTooltip(); // Hide tooltip on click
  };

  const handleHover = (d) => {
    if (d.object) {
      showTooltip(d);
      const locationId = d.object.properties?.vegetated_area_id;

      if (selectedLocationIds.has(locationId)) {
        setHoveredSelectedLocationId(locationId);
      }
    } else {
      setHoveredSelectedLocationId(undefined);
      hideTooltip();
    }
  };

  const ecotekaPolygons = new MVTLayer({
    data: generateLayerDataUrl(
      "filter_polygons",
      forceReloadLayer,
      "",
      queryStringPolygons
    ),
    id: "polygons",
    // @ts-ignore
    getRadius: 3,
    opacity: 1,
    stroked: true,
    pickable: true,
    filled: true,
    lineWidthMinPixels: 2,
    lineWidthMaxPixels: 2,
    getLineColor: (d) => {
      const polygonType = d.properties.boundary_type || d.properties.type;
      if (polygonType === "geographic") {
        return [255, 68, 225, 255];
      } else if (polygonType === "station") {
        return [144, 85, 253, 255];
      } else {
        return [69, 214, 45, 255];
      }
    },
    getFillColor: (d) => {
      const vegetatedAreaId = d.properties.vegetated_area_id;

      if (isFeatureSelected(d)) {
        if (vegetatedAreaId === worksiteHoveredId) {
          return [165, 250, 162]; // Highlight color for hovered feature
        }
        return [255, 243, 80, 255];
      } else if (vegetatedAreaId) {
        return [69, 214, 45, 128]; // Fill color for vegetated_area
      } else {
        return [255, 255, 255, 0]; // Transparent for all other types
      }
    },
    onClick: handleClick,
    onHover: handleHover,
    updateTriggers: {
      getFillColor: [selectedLocationIds, worksiteHoveredId],
    },
  });

  actions.setMapLayers([ecotekaPolygons]);
  return ecotekaPolygons;
};

const tooltipBaseStyles = {
  position: "absolute",
  pointerEvents: "none",
  color: "#fff",
  padding: "3px",
  borderRadius: "5px",
};

export { generateLayerDataUrl, createLocationsLayer, createPolygonsLayer };
