import { FC, useMemo } from "react";
import { GeoJsonLayer } from "@deck.gl/layers";
import { WorksiteInterventionFormData } from "@stores/forms/worksiteForm";

export interface WorksiteLayerProps {
  mapSelectedLocations: any;
  interventions: WorksiteInterventionFormData[];
  setAssociatedLocations: (
    currentInterventionIndex: number,
    associatedLocations: any
  ) => void;
}

// @ts-ignore
const WorksiteLayer: FC<WorksiteLayerProps> = ({
  mapSelectedLocations,
  interventions,
  setAssociatedLocations,
}) => {
  const isAssociatedLocation = (properties) => {
    const activeIntervention = interventions.find(
      (intervention) => intervention?.is_current
    );
    if (!activeIntervention || !activeIntervention.associated_locations)
      return false;

    return activeIntervention.associated_locations.some(
      (loc) =>
        (loc.tree_id && loc.tree_id === properties.tree_id) ||
        (loc.location_id && loc.location_id === properties.location_id) ||
        (loc.vegetated_area_id &&
          loc.vegetated_area_id === properties.vegetated_area_id)
    );
  };
  const handleClick = (info: any) => {
    if (!info.object) return;

    const { tree_id, location_id, vegetated_area_id, status_name } =
      info.object.properties;
    const activeInterventionIndex = interventions.findIndex(
      (intervention) => intervention?.is_current
    );

    if (activeInterventionIndex === -1) {
      console.log("No active intervention found.");
      return;
    }

    // Update only the active intervention
    let updatedLocations = [
      ...(interventions[activeInterventionIndex]?.associated_locations || []),
    ];

    // Helper function to toggle the ID and status in the locations array
    const toggleId = (key: string, id: any, status: string) => {
      const index = updatedLocations.findIndex((item) => item[key] === id);

      if (index !== -1) {
        console.log("item already exists");
        // If the ID exists, remove it by splicing it out of the array
        updatedLocations.splice(index, 1);
      } else {
        /*         console.log("item is new"); */
        // If the ID doesn't exist, add both the ID and the status
        updatedLocations.push({ [key]: id, status_name: status });
      }
    };

    if (tree_id && location_id) {
      toggleId("tree_id", tree_id, status_name);
    } else if (location_id && !tree_id) {
      toggleId("location_id", location_id, status_name);
    }

    if (vegetated_area_id) {
      toggleId("vegetated_area_id", vegetated_area_id, status_name);
    }

    setAssociatedLocations(activeInterventionIndex, updatedLocations);
  };

  const { polygonFeatures, pointFeatures } = mapSelectedLocations.reduce(
    (acc, item) => {
      if (item.vegetated_area_id && item.vegetated_area_coords) {
        acc.polygonFeatures.push({
          type: "Feature",
          geometry: JSON.parse(item.vegetated_area_coords),
          properties: {
            type: "polygon",
            vegetated_area_id: item.vegetated_area_id,
          },
        });
      } else if (item.location_coords) {
        acc.pointFeatures.push({
          type: "Feature",
          geometry: JSON.parse(item.location_coords),
          properties: {
            location_id: item.location_id,
            tree_id: item.tree_id,
            type: "point",
            status_name: item.status_name,
            status_color: item.status_color,
          },
        });
      }
      return acc;
    },
    { polygonFeatures: [], pointFeatures: [] }
  );

  const layers = [
    new GeoJsonLayer({
      id: "polygon-layer-worksite",
      data: {
        type: "FeatureCollection",
        features: polygonFeatures,
      },
      getRadius: 3,
      opacity: 1,
      stroked: true,
      pickable: true,
      filled: true,
      autoHighlight: true,
      lineWidthMinPixels: 2,
      lineWidthMaxPixels: 2,
      getFillColor: (d: any) =>
        isAssociatedLocation(d.properties)
          ? [255, 165, 0, 180]
          : [69, 214, 45, 180], // Orange for associated, green for others
      getLineColor: [69, 214, 45, 255],
      onClick: (d) => handleClick(d),
      updateTriggers: {
        onClick: interventions,
      },
    }),
    new GeoJsonLayer({
      id: "point-layer-worksite",
      getRadius: 3,
      opacity: 0.9,
      stroked: true,
      pickable: true,
      filled: true,
      extruded: true,
      pointType: "circle",
      pointRadiusUnits: "meters",
      pointRadiusMinPixels: 2,
      pointRadiusMaxPixels: 10,
      pointRadiusScale: 1,
      autoHighlight: true,
      visible: true,
      data: {
        type: "FeatureCollection",
        features: pointFeatures,
      },
      getFillColor: (d: any) => {
        if (isAssociatedLocation(d.properties)) {
          return [255, 165, 0, 255]; // Orange for associated locations
        }
        return JSON.parse(d.properties.status_color || "[0, 0, 0, 255]");
      },
      getLineColor: (d: any) =>
        d.properties.status_name === "empty"
          ? [47, 163, 124, 255]
          : [0, 0, 0, 0],
      onClick: (d) => handleClick(d),
      updateTriggers: {
        onClick: interventions,
      },
    }),
  ];

  return layers;
};

export default WorksiteLayer;
