import { FC } from "react";
import { GeoJsonLayer } from "@deck.gl/layers";

export interface TempLayerProps {
  mapTempPoint: any;
  position: any;
  setMapTempPoint: any;
}

const getLineColor = (d) => {
  return d.properties.type === "tree" ? [255, 255, 255, 255] : [0, 0, 0, 255];
};
// @ts-ignore
const TempLayer: FC<TempLayerProps> = ({
  mapTempPoint,
  position,
  setMapTempPoint,
}) => {
  const tempFeatures = {
    type: "FeatureCollection",
    features: [
      {
        type: "Feature",
        geometry: {
          type: "Point",
          coordinates: [mapTempPoint.lon, mapTempPoint.lat],
        },
        properties: {
          name: "temp tree",
          type: "tree",
        },
      },
      {
        type: "Feature",
        geometry: {
          type: "Point",
          coordinates: [position.lon, position.lat],
        },
        properties: {
          name: "user position",
          type: "position",
        },
      },
    ],
  };

  return new GeoJsonLayer({
    id: "temp-layer",
    data: tempFeatures,
    pickable: true,
    filled: true,
    extruded: true,
    pointType: "circle",
    lineWidthScale: 20,
    pointRadiusMinPixels: 10,
    pointRadiusScale: 1,
    minRadius: 3,
    radiusMinPixels: 0.5,
    lineWidthMinPixels: 1,
    lineWidthMaxPixels: 3,
    autoHighlight: true,
    visible: true,
    getFillColor: (d: any) => {
      if (d.properties.type === "position") {
        // Customize the color for the user position circle
        return [255, 243, 80, 200]; // Red with alpha 200
      } else {
        return [6, 182, 174, 200]; // Default color
      }
    },
    // @ts-ignore
    getLineColor: (d: any) => {
      if (d.properties.type === "position") {
        // No border color for the user position circle
        return [128, 128, 128, 200];
      } else {
        return getLineColor(d);
      }
    },
    onDragStart: (info, event) => {
      // @ts-ignore
      const [x, y] = info.coordinate;
      setMapTempPoint({ lat: y, lon: x });
    },
    onDragEnd: (info, event) => {
      const [x, y] = info.coordinate;
      setMapTempPoint({ lat: y, lon: x });
    },
  });
};

export default TempLayer;
