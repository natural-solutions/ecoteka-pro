import { FC, useState } from "react";
import GpsFixedIcon from "@mui/icons-material/GpsFixed";
import {
  Fab,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  Alert,
} from "@mui/material";
import { useGeolocated } from "react-geolocated";
import { useTranslation } from "react-i18next";

interface GeolocalisationProps {
  onGeolocate?(longitude: number, latitude: number): void;
}

const Geolocalisation: FC<GeolocalisationProps> = ({ onGeolocate }) => {
  const { t } = useTranslation(["common"]);
  const { getPosition, isGeolocationEnabled, isGeolocationAvailable } =
    useGeolocated({
      suppressLocationOnMount: true,
      onSuccess: (position) => {
        if (onGeolocate) {
          onGeolocate(position.coords.longitude, position.coords.latitude);
        }
      },
    });

  const [isDialogOpen, setIsDialogOpen] = useState(false);

  const handleOnClick = () => {
    if (isGeolocationEnabled && isGeolocationAvailable) {
      getPosition();
    } else {
      setIsDialogOpen(true);
    }
  };

  const handleDialogClose = () => {
    setIsDialogOpen(false);
  };

  const renderInstructions = (browser: string, steps: string[]) => (
    <>
      <strong>{t(`geolocation.instructions.${browser}.title`)}:</strong>
      <br />
      {steps.map((step, index) => (
        <span key={index}>
          {index + 1}. {t(`geolocation.instructions.${browser}.${step}`)}
          <br />
        </span>
      ))}
      <br />
    </>
  );

  return (
    <>
      <Fab
        color="inherit"
        size="medium"
        aria-label="geolocate"
        onClick={handleOnClick}
        sx={{ backgroundColor: "#fff" }}
        data-cy="map-actions-geolocalisation-button"
      >
        <GpsFixedIcon sx={{ color: "#1D7554" }} />
      </Fab>
      <Dialog open={isDialogOpen} onClose={handleDialogClose}>
        <DialogTitle>{t("geolocation.instructions.title")}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Alert severity="info">{t("geolocation.instructions.text")}</Alert>
            <br />
            {renderInstructions("chrome", ["step1", "step2"])}
            {renderInstructions("firefox", ["step1", "step2", "step3"])}
            {renderInstructions("safari", ["step1", "step2", "step3"])}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialogClose} color="primary" autoFocus>
            {t("buttons.close")}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default Geolocalisation;
