import React from "react";
import { Box, Typography } from "@mui/material";
import {
  useFiltersWithMartinContext,
  FiltersWithMartinContextType,
} from "@context/FiltersWithMartinContext";
import { useTranslation } from "react-i18next";
import { renderValue } from "@helpers/filter/FilterHelpers";

const RenderTooltipContent: React.FC = () => {
  const { t } = useTranslation(["components"]);
  const { filtersWithMartin, filterData }: FiltersWithMartinContextType =
    useFiltersWithMartinContext()!;

  const filtersList = Object.entries(filtersWithMartin).filter(
    ([key, value]) => value && (value.length > 0 || value === true)
  );

  return (
    <Box sx={{ padding: "0 10px" }}>
      <Typography
        sx={{
          paddingY: "6px",
          marginBottom: "8px",
          borderBottom: ".1px solid rgba(0,0,0,.2)",
          textAlign: "center",
        }}
      >
        {t("components.Map.Filter.active")}
      </Typography>
      {filtersList.map(([key, value], index) => {
        const displayValue = renderValue(
          key,
          value,
          filterData,
          filtersWithMartin,
          t
        );
        return displayValue !== null ? (
          <Typography key={index} variant="inherit" sx={{ mb: 0.5 }}>
            <em style={{ color: "#fff", fontStyle: "normal" }}>
              {t(`components.Map.Filter.${key}`)}{" "}
            </em>
            {/*      {displayValue} */}
          </Typography>
        ) : null;
      })}
    </Box>
  );
};

export default RenderTooltipContent;
