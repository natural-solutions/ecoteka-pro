import { FC } from "react";
import { Box, Stack } from "@mui/material";
import _ from "lodash";
import Geolocalisation from "./Geolocalisation";
import ExitEditButton from "./buttons/ExitEditButton";
import FilterButton from "./buttons/FilterButton";
import {
  LocationType,
  MapBackground,
  MapActionsBarActionType,
} from "../../lib/store/pages/mapEditor";
import LayerManager from "@components/_blocks/map/LayerManager/LayerManager";
import Toolbar from "@components/_blocks/map/Toolbar";
import { useDeviceSize } from "@lib/utils";
import AddressSearchBar from "./search/AddressSearchBar";
import AuthCan from "@components/auth/Can";

export interface MapActionsProps {
  handleOnActionClick(action: MapActionsBarActionType): void;
  handleOnBaseMapChange?(map: MapBackground): void;
  mapBackground: MapBackground;
  handleOnGeolocate?(x: number, y: number): void;
  handleOnAddLocation(type: LocationType): void;
  editionMode: boolean;
  cardPreview: boolean;
  exitEditMode(): void;
  locationStatusList: { status: string; id: string }[];
}

const MapActions: FC<MapActionsProps> = ({
  handleOnActionClick,
  handleOnBaseMapChange,
  mapBackground,
  handleOnGeolocate,
  editionMode,
  exitEditMode,
  handleOnAddLocation,
  cardPreview,
}) => {
  const { isMobile, isTablet, isDesktop } = useDeviceSize();

  return (
    <>
      {(isDesktop || isTablet) && !isMobile && (
        <Box
          sx={[styles.topActionsContainer, styles.nonClickable]}
          data-cy="map-actions-container-filter"
        >
          <Box
            sx={styles.clickable}
            data-cy="map-actions-container-filter-button"
          >
            <AddressSearchBar />
          </Box>
        </Box>
      )}

      <Box
        sx={[
          styles.topActionsContainerRight,
          styles.nonClickable,
          { zIndex: 2 },
        ]}
        data-cy="map-actions-container-geolocalisation"
      >
        <Box
          sx={styles.clickable}
          data-cy="map-actions-container-geolocalisation-button"
        >
          <Geolocalisation onGeolocate={handleOnGeolocate} />
        </Box>
      </Box>
      {!isMobile && (
        <Box sx={[styles.topLeftActionsContainer, styles.clickable]}>
          <Box data-cy="map-actions-container-layer-manager">
            <LayerManager isOpen={false} />
          </Box>
          <Box
            data-cy="map-actions-container-layer-manager"
            sx={{ position: { xs: "absolute" }, top: { xs: 55 } }}
          >
            <FilterButton handleOnActionClick={handleOnActionClick} />
          </Box>
        </Box>
      )}

      {isMobile && (
        <Box
          sx={[styles.mobileContainer, styles.clickable]}
          data-cy="map-actions-container-mobile"
        >
          <Box sx={styles.mobileActionItem}>
            <AddressSearchBar />
          </Box>
          <Box sx={styles.mobileActionItem}>
            <FilterButton handleOnActionClick={handleOnActionClick} />
          </Box>
        </Box>
      )}
      <AuthCan role={["admin", "editor"]}>
        <Stack
          sx={styles.bottomMiddleActionsContainer}
          spacing={1}
          direction={{ md: "row", xs: "column" }}
          alignItems={{ md: "flex-end", xs: "center" }}
          justifyContent={{ md: "center", xs: "left" }}
          data-cy="map-actions-container-toolbar-desktop"
        >
          {!editionMode ? (
            <Toolbar
              handleOnAddLocation={handleOnAddLocation}
              map={mapBackground}
              handleOnBaseMapChange={handleOnBaseMapChange}
            />
          ) : (
            <Box sx={styles.clickable} data-cy="map-actions-exit-edit-button">
              <ExitEditButton handleExitMode={exitEditMode} />
            </Box>
          )}
        </Stack>
      </AuthCan>
    </>
  );
};

export default MapActions;

const styles = {
  nonClickable: {
    pointerEvents: "none",
  },
  clickable: {
    pointerEvents: "all",
  },
  topActionsContainer: {
    position: "absolute",
    top: 10,
    width: "100%",
    display: "flex",
    justifyContent: "center",
    right: { md: 0, xs: 22 },
  },
  topActionsContainerRight: {
    position: "absolute",
    top: { md: 90 },
    bottom: { xs: 5 },
    width: "100%",
    display: "flex",
    justifyContent: "right",
    right: { md: 10, xs: 5 },
  },
  bottomActionsContainerMobile: {
    position: "absolute",
    bottom: 60,
    right: 4,
    padding: 2,
    width: "100%",
    flexDirection: { md: "row", xs: "column" },
    justifyContent: "space-between",
    alignItems: "end",
  },
  bottomMiddleActionsContainer: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: "50px",
  },
  staticTooltipLabel: {
    width: "150px",
  },
  topLeftActionsContainer: {
    display: "flex",
    position: "absolute",
    top: 10,
    width: "auto",
    justifyContent: "left",
    left: { md: 5, xs: 5 },
  },
  mobileContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    position: "absolute",
    top: 10,
    left: "50%",
    transform: "translateX(-50%)",
    width: "100%",
  },
  mobileActionItem: {
    marginBottom: 0.5,
  },
};
