import React, { useEffect, useState } from "react";
import {
  Typography,
  TextField,
  Box,
  Autocomplete,
  FormControlLabel,
  Switch,
} from "@mui/material";
import DateRangeSelector from "@components/date/DatePicker";
import Chip from "@mui/material/Chip";
import Stack from "@mui/material/Stack";
import { useTranslation } from "react-i18next";
import IsExpanded from "../buttons/IsExpanded";
import {
  useFiltersWithMartinContext,
  Partner,
  InterventionType,
} from "@context/FiltersWithMartinContext";
import moment, { Moment } from "moment";
import { renderValue } from "@helpers/filter/FilterHelpers";
import { useRouter } from "next/router";
import { FilterList } from "@mui/icons-material";

const InterventionFilter = () => {
  const { t } = useTranslation("components");
  const router = useRouter();
  const [isExpanded, setIsExpanded] = useState<boolean>(true);
  const [isButtonHovered, setIsButtonHovered] = useState<boolean>(false);
  const [interventionPartnerData, setInterventionPartnerData] = useState<
    Partner[]
  >([]);
  const [interventionTypeData, setInterventionTypeData] = useState<
    InterventionType[]
  >([]);
  const [selectedInterventionPartners, setSelectedInterventionPartners] =
    useState<Partner[]>([]);
  const [selectedInterventionTypes, setSelectedInterventionTypes] = useState<
    InterventionType[]
  >([]);
  const [isParkingBanned, setIsParkingBanned] = useState<boolean>(false); // Changed initial state to false

  const [activeInterventionStatus, setActiveInterventionStatus] = useState<
    string | null
  >(null);
  const [scheduledDateStart, setScheduledDateStart] = useState<
    Moment | null | string
  >(null);
  const [scheduledDateEnd, setScheduledDateEnd] = useState<
    Moment | null | string
  >(null);
  const [realizationDateStart, setRealizationDateStart] = useState<
    Moment | null | string
  >(null);
  const [realizationDateEnd, setRealizationDateEnd] = useState<
    Moment | null | string
  >(null);

  const {
    filterData,
    setFiltersWithMartin,
    shouldResetFields,
    filtersWithMartin,
  } = useFiltersWithMartinContext()!;

  const handleIsParkingBannedChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newValue = event.target.checked;
    setIsParkingBanned(newValue);
  };

  useEffect(() => {
    if (filterData && filterData?.intervention) {
      setInterventionPartnerData(filterData?.intervention.partner || []);
      setInterventionTypeData(filterData?.intervention.type || []);
    }
  }, [filterData]);

  useEffect(() => {
    setActiveInterventionStatus(filtersWithMartin.intervention_status ?? "");
    setScheduledDateStart(filtersWithMartin.scheduled_date_start ?? null);
    setScheduledDateEnd(filtersWithMartin.scheduled_date_end ?? null);
    setRealizationDateStart(filtersWithMartin.realized_date_start ?? null);
    setRealizationDateEnd(filtersWithMartin.realized_date_end ?? null);
    setIsParkingBanned(filtersWithMartin?.is_parking_banned ?? false);
  }, []);

  useEffect(() => {
    if (filtersWithMartin?.intervention_partner_id?.length) {
      setSelectedInterventionPartners(
        filtersWithMartin?.intervention_partner_id?.map((id) => ({
          id: id,
          name:
            interventionPartnerData.find((item) => item.id === id)?.name ?? "",
        })) ?? []
      );
    }

    if (filtersWithMartin?.intervention_type_id?.length) {
      setSelectedInterventionTypes(
        filtersWithMartin?.intervention_type_id?.map((id) => ({
          id: id,
          slug: interventionTypeData.find((item) => item.id === id)?.slug ?? "",
        })) ?? []
      );
    }
  }, [interventionTypeData, interventionPartnerData]);

  const toComeLabel = t("PanelTree.interventionsToCome");
  const lastInterventionsLabel = t("PanelTree.lastInterventions");
  const lateIntervention = t("Interventions.Card.late");
  const interventionStatusItems = [
    { name: "toCome", label: toComeLabel.substring(14) },
    { name: "realized", label: lastInterventionsLabel.substring(14) },
    { name: "late", label: lateIntervention.substring(14) },
  ];

  const handleToggleExpanded = (newState: boolean) => {
    setIsExpanded(newState);
  };

  const handleHoverChange = (isHovered: boolean) => {
    setIsButtonHovered(isHovered);
  };

  const handleChipClick = (item: string) => {
    router.replace(router.pathname);
    setActiveInterventionStatus((prevState) =>
      prevState === item ? null : item
    );
  };

  const handleSelectionChange = (setterFunction) => (event, newValue) => {
    router.replace(router.pathname);
    setterFunction(newValue);
  };

  const handleDateChange =
    (dateType: "scheduled" | "realization") => (startDate, endDate) => {
      router.replace(router.pathname);

      const formattedStartDate = startDate
        ? moment(startDate).format("YYYY-MM-DD")
        : null;
      const formattedEndDate = endDate
        ? moment(endDate).format("YYYY-MM-DD")
        : null;

      setFiltersWithMartin((prevState) => {
        const newState = { ...prevState };
        if (dateType === "scheduled") {
          if (formattedStartDate) {
            newState.scheduled_date_start = formattedStartDate;
          } else {
            delete newState.scheduled_date_start;
          }
          if (formattedEndDate) {
            newState.scheduled_date_end = formattedEndDate;
          } else {
            delete newState.scheduled_date_end;
          }
        } else if (dateType === "realization") {
          if (formattedStartDate) {
            newState.realized_date_start = formattedStartDate;
          } else {
            delete newState.realized_date_start;
          }
          if (formattedEndDate) {
            newState.realized_date_end = formattedEndDate;
          } else {
            delete newState.realized_date_end;
          }
        }
        return newState;
      });
    };

  const updateFiltersContext = (key, value) => {
    setFiltersWithMartin((prevState) => {
      const newState = { ...prevState };
      if (key === "is_parking_banned") {
        newState[key] = value;
      } else {
        value && value !== "Invalid date" && value.length > 0
          ? (newState[key] = value)
          : delete newState[key];
      }
      return newState;
    });
  };

  useEffect(() => {
    updateFiltersContext(
      "intervention_partner_id",
      selectedInterventionPartners.map((partner: any) => partner?.id)
    );
    updateFiltersContext(
      "intervention_type_id",
      selectedInterventionTypes.map((type: any) => type?.id)
    );
    updateFiltersContext("intervention_status", activeInterventionStatus);
    updateFiltersContext("scheduled_date_start", scheduledDateStart);
    updateFiltersContext("scheduled_date_end", scheduledDateEnd);
    updateFiltersContext("realized_date_start", realizationDateStart);
    updateFiltersContext("realized_date_end", realizationDateEnd);
    updateFiltersContext("is_parking_banned", isParkingBanned);
  }, [
    selectedInterventionPartners,
    selectedInterventionTypes,
    activeInterventionStatus,
    scheduledDateStart,
    scheduledDateEnd,
    realizationDateStart,
    realizationDateEnd,
    isParkingBanned,
  ]);

  useEffect(() => {
    if (shouldResetFields) {
      setSelectedInterventionPartners([]);
      setSelectedInterventionTypes([]);
      setActiveInterventionStatus(null);
      setScheduledDateStart(null);
      setScheduledDateEnd(null);
      setRealizationDateStart(null);
      setRealizationDateEnd(null);
      setIsParkingBanned(false);
    }
  }, [shouldResetFields]);

  const filtersList = Object.entries(filtersWithMartin).filter(
    ([key, value]) =>
      ((key === "intervention_partner_id" ||
        key === "intervention_type_id" ||
        key === "intervention_status" ||
        key === "scheduled_date_start" ||
        key === "realized_date_start") &&
        value &&
        value.length > 0) ||
      (key === "is_parking_banned" && value === true)
  );

  return (
    <>
      <Box
        sx={{
          p: 2,
          border: isButtonHovered
            ? "1px solid #2FA37C"
            : "1px solid rgba(67, 74, 74,.3)",
          borderRadius: "5px",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          position: "relative",
        }}
      >
        {filterData?.intervention && (
          <>
            <Autocomplete
              id="autocomplete-interventionPartner"
              autoComplete
              multiple
              value={selectedInterventionPartners}
              options={interventionPartnerData}
              onChange={handleSelectionChange(setSelectedInterventionPartners)}
              sx={{
                display: isExpanded ? "block" : "none",
                width: "100%",
                marginBottom: 2,
                marginTop: 1,
              }}
              getOptionLabel={(option) => option?.name}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={t(
                    `components.Intervention.properties.interventionPartner`
                  )}
                />
              )}
            />
            <Autocomplete
              id="autocomplete-interventionType"
              autoComplete
              multiple
              value={selectedInterventionTypes}
              options={interventionTypeData}
              onChange={handleSelectionChange(setSelectedInterventionTypes)}
              sx={{
                display: isExpanded ? "block" : "none",
                width: "100%",
                marginTop: 1,
              }}
              getOptionLabel={(option) =>
                t(`components.Interventions.interventionsList.${option?.slug}`)
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={t(
                    `components.Intervention.properties.interventionType`
                  )}
                />
              )}
            />
            <Stack
              direction="row"
              sx={{
                display: isExpanded ? "flex" : "none",
                width: "100%",
                marginTop: 2,
                justifyContent: "center",
              }}
              spacing={1}
            >
              {interventionStatusItems.map((item, index) => (
                <Chip
                  key={index}
                  label={item.label}
                  variant="outlined"
                  onClick={() => handleChipClick(item.name)}
                  sx={{
                    display: isExpanded ? "flex" : "none",
                    textTransform: "capitalize",
                    backgroundColor:
                      activeInterventionStatus == item.name
                        ? "#2FA37C"
                        : undefined,
                  }}
                />
              ))}
            </Stack>
            <DateRangeSelector
              pickerType={t(`components.Intervention.properties.scheduledDate`)}
              onDateChange={handleDateChange("scheduled")}
              isExpanded={isExpanded}
            />
            <DateRangeSelector
              pickerType={t(
                `components.Intervention.properties.realizationDate`
              )}
              isExpanded={isExpanded}
              onDateChange={handleDateChange("realization")}
            />
            <FormControlLabel
              control={
                <Switch
                  checked={isParkingBanned}
                  onChange={handleIsParkingBannedChange}
                />
              }
              label={t(`components.Intervention.properties.is_parking_banned`)}
            />
          </>
        )}

        {filtersList.length > 0 ? (
          <>
            <Typography sx={{ pb: 1, display: isExpanded ? "none" : "block" }}>
              {t("Map.Filter.active")}
            </Typography>
            {filtersList.map(([key, value], index) => {
              const displayValue = renderValue(
                key,
                value,
                filterData,
                filtersWithMartin,
                t
              );
              return displayValue !== null ? (
                <Typography
                  key={index}
                  variant="inherit"
                  sx={{
                    mb: 0.5,
                    fontFamily: "Poppins",
                    alignSelf: "flex-start",
                    display: isExpanded ? "none" : "block",
                  }}
                >
                  <em style={{ color: "#2FA37C", fontStyle: "normal" }}>
                    {`${t(`components.Map.Filter.${key}`)} : `}
                  </em>
                  {key != "intervention_status"
                    ? displayValue
                    : t(`Map.Filter.intervention_status_item.${value}`)}
                </Typography>
              ) : null;
            })}
          </>
        ) : (
          <Typography sx={{ display: isExpanded ? "none" : "block" }}>
            {t("Map.Filter.empty")}
          </Typography>
        )}
        <Typography
          sx={{
            position: "absolute",
            top: "-12px",
            left: "10px",
            backgroundColor: "white",
            padding: "0 4px",
            color: isButtonHovered ? "#2FA37C" : "rgba(67, 74, 74,.9)",
          }}
        >
          {t("ImportHistoryTable.headers.intervention")}
        </Typography>
      </Box>
      <IsExpanded
        onToggleExpanded={handleToggleExpanded}
        onHoverChange={handleHoverChange}
      />
    </>
  );
};

export default InterventionFilter;
