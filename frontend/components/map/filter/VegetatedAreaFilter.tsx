import React, { FC, useEffect, useState } from "react";
import { Typography, Box, Chip, Stack } from "@mui/material";
import { useTranslation } from "react-i18next";
import IsExpanded from "../buttons/IsExpanded";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import { renderVegetatedAreaFilterValue } from "@helpers/filter/FilterHelpers";
import { useRouter } from "next/router";

const VegetatedAreaFilter: FC = () => {
  const { t } = useTranslation("components");
  const router = useRouter();
  const [isExpanded, setIsExpanded] = useState<boolean>(true);
  const [isButtonHovered, setIsButtonHovered] = useState<boolean>(false);
  const { setFiltersWithMartin, shouldResetFields, filtersWithMartin } =
    useFiltersWithMartinContext()!;
  const [isFilterActive, setIsFilterActive] = useState<boolean>(false);

  useEffect(() => {
    filtersWithMartin?.vegetated_areas_active === true
      ? setIsFilterActive(true)
      : setIsFilterActive(false);
  }, []);

  useEffect(() => {
    if (isFilterActive) {
      setFiltersWithMartin(() => ({
        ...filtersWithMartin,
        vegetated_areas_active: true,
      }));
    } else {
      setFiltersWithMartin(() => {
        const newState = { ...filtersWithMartin };
        delete newState.vegetated_areas_active;
        return newState;
      });
    }
  }, [isFilterActive, setFiltersWithMartin]);

  const handleChipClick = () => {
    router.replace(router.pathname);
    setIsFilterActive(!isFilterActive);
  };

  useEffect(() => {
    if (shouldResetFields) {
      setIsFilterActive(false);
    }
  }, [shouldResetFields]);

  const handleToggleExpanded = (newState: boolean) => {
    setIsExpanded(newState);
  };

  const handleHoverChange = (isHovered: boolean) => {
    setIsButtonHovered(isHovered);
  };

  const filtersList = Object.entries(filtersWithMartin).filter(
    ([key, value]) => key === "vegetated_areas_active" && value === true
  );

  return (
    <>
      <Box
        sx={{
          p: 2,
          marginTop: 2,
          border: isButtonHovered
            ? "1px solid #2FA37C"
            : "1px solid rgba(67, 74, 74,.3)",
          borderRadius: "5px",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          position: "relative",
        }}
      >
        {isExpanded ? (
          <>
            <Stack
              direction="row"
              sx={{
                width: "100%",
                marginBottom: 2,
                display: "flex",
                justifyContent: "center",
              }}
              spacing={1}
            >
              <Chip
                label={t(
                  `Worksite.properties.location_status_types.vegetated_area`
                )}
                variant="outlined"
                onClick={() => handleChipClick()}
                sx={{
                  textTransform: "capitalize",
                  color: isFilterActive ? "white" : "primary",
                  backgroundColor: isFilterActive ? "#45D62D" : undefined,
                }}
              />
            </Stack>
          </>
        ) : filtersList.length > 0 ? (
          <>
            <Typography sx={{ pb: 1 }}>{t("Map.Filter.active")}</Typography>
            {filtersList.map(([key, value], index) => {
              const displayValue = renderVegetatedAreaFilterValue(
                key,
                value,
                t
              );
              return displayValue !== null ? (
                <Typography
                  key={index}
                  variant="inherit"
                  sx={{
                    mb: 0.5,
                    fontFamily: "Poppins",
                    alignSelf: "flex-start",
                  }}
                >
                  <em style={{ color: "#2FA37C", fontStyle: "normal" }}>
                    {`${t(`components.Map.Filter.vegetated_areas_active`)}`}
                  </em>
                  {/*       {displayValue} */}
                </Typography>
              ) : null;
            })}
          </>
        ) : (
          <Typography>{t("Map.Filter.empty")}</Typography>
        )}

        <Typography
          sx={{
            position: "absolute",
            top: "-12px",
            left: "10px",
            backgroundColor: "white",
            padding: "0 4px",
            color: isButtonHovered ? "#2FA37C" : "rgba(67, 74, 74,.9)",
          }}
        >
          {t("Worksite.properties.location_status_types.vegetated_area")}
        </Typography>
      </Box>
      <IsExpanded
        onToggleExpanded={handleToggleExpanded}
        onHoverChange={handleHoverChange}
      />
    </>
  );
};

export default VegetatedAreaFilter;
