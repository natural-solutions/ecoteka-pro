import React, { FC, useEffect, useState } from "react";
import { Typography, Box, Chip, Stack } from "@mui/material";
import { useTranslation } from "react-i18next";
import IsExpanded from "../buttons/IsExpanded";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import { renderBoundaryFilterValue } from "@helpers/filter/FilterHelpers";
import { useRouter } from "next/router";

const BoundaryFilter: FC = () => {
  const { t } = useTranslation("components");
  const router = useRouter();
  const [isExpanded, setIsExpanded] = useState<boolean>(true);
  const [isButtonHovered, setIsButtonHovered] = useState<boolean>(false);
  const [boundaryTypesData, setBoundaryTypesData] = useState<string[]>([]);
  const {
    filterData,
    setFiltersWithMartin,
    shouldResetFields,
    filtersWithMartin,
  } = useFiltersWithMartinContext()!;
  const [activeBoundaries, setActiveBoundaries] = useState<{
    [key: string]: boolean;
  }>({});

  useEffect(() => {
    if (filterData && filterData.boundary) {
      setBoundaryTypesData(filterData.boundary.type as string[]);
    }
  }, [filterData]);

  useEffect(() => {
    setActiveBoundaries({
      geograhic:
        filtersWithMartin?.geographic_boundaries_active === true ? true : false,
      station:
        filtersWithMartin?.station_boundaries_active === true ? true : false,
    });
  }, []);

  useEffect(() => {
    setFiltersWithMartin(() => {
      const newState = { ...filtersWithMartin };
      if (activeBoundaries.geographic) {
        newState.geographic_boundaries_active = true;
      } else {
        delete newState.geographic_boundaries_active;
      }
      if (activeBoundaries.station) {
        newState.station_boundaries_active = true;
      } else {
        delete newState.station_boundaries_active;
      }
      return newState;
    });
  }, [activeBoundaries, setFiltersWithMartin]);

  const handleChipClick = (type: string) => {
    router.replace(router.pathname);
    setActiveBoundaries((prevState) => ({
      ...prevState,
      [type]: !prevState[type],
    }));
  };

  useEffect(() => {
    if (shouldResetFields) {
      setActiveBoundaries({
        geographic: false,
        station: false,
      });
    }
  }, [shouldResetFields]);

  const handleToggleExpanded = (newState: boolean) => {
    setIsExpanded(newState);
  };

  const handleHoverChange = (isHovered: boolean) => {
    setIsButtonHovered(isHovered);
  };

  const filtersList = Object.entries(filtersWithMartin).filter(
    ([key, value]) =>
      (key === "geographic_boundaries_active" ||
        key === "station_boundaries_active") &&
      value === true
  );

  return (
    <>
      <Box
        sx={{
          p: 2,
          marginTop: 2,
          border: isButtonHovered
            ? "1px solid #2FA37C"
            : "1px solid rgba(67, 74, 74,.3)",
          borderRadius: "5px",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          position: "relative",
        }}
      >
        {isExpanded ? (
          <>
            <Stack
              direction="row"
              sx={{
                width: "100%",
                marginBottom: 2,
                display: "flex",
                justifyContent: "center",
              }}
              spacing={1}
            >
              {boundaryTypesData.map((type) => (
                <Chip
                  key={type}
                  label={t(`Map.Filter.polygon.types.${type.toLowerCase()}`)}
                  variant="outlined"
                  onClick={() => handleChipClick(type)}
                  sx={{
                    textTransform: "capitalize",
                    color:
                      (type === "geographic" && activeBoundaries.geographic) ||
                      (type === "station" && activeBoundaries.station)
                        ? "white"
                        : "primary",
                    backgroundColor:
                      type === "geographic" && activeBoundaries.geographic
                        ? "#FF44E1"
                        : type === "station" && activeBoundaries.station
                          ? "#9057FD"
                          : undefined,
                  }}
                />
              ))}
            </Stack>
          </>
        ) : filtersList.length > 0 ? (
          <>
            <Typography sx={{ pb: 1 }}>{t("Map.Filter.active")}</Typography>
            {filtersList.map(([key, value], index) => {
              const displayValue = renderBoundaryFilterValue(key, value, t);
              return displayValue !== null ? (
                <Typography
                  key={index}
                  variant="inherit"
                  sx={{
                    mb: 0.5,
                    fontFamily: "Poppins",
                    alignSelf: "flex-start",
                  }}
                >
                  <em style={{ color: "#2FA37C", fontStyle: "normal" }}>
                    {`${t(`components.Map.Filter.polygon.label`)} : `}
                  </em>
                  {displayValue}
                </Typography>
              ) : null;
            })}
          </>
        ) : (
          <Typography>{t("Map.Filter.empty")}</Typography>
        )}

        <Typography
          sx={{
            position: "absolute",
            top: "-12px",
            left: "10px",
            backgroundColor: "white",
            padding: "0 4px",
            color: isButtonHovered ? "#2FA37C" : "rgba(67, 74, 74,.9)",
          }}
        >
          {t(`Map.Filter.polygon.label`)}
        </Typography>
      </Box>
      <IsExpanded
        onToggleExpanded={handleToggleExpanded}
        onHoverChange={handleHoverChange}
      />
    </>
  );
};

export default BoundaryFilter;
