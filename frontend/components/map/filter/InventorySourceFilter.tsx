import React, { useEffect, useState } from "react";
import { Typography, Box, Chip } from "@mui/material";
import { useTranslation } from "react-i18next";
import IsExpanded from "../buttons/IsExpanded";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import { renderValue } from "@helpers/filter/FilterHelpers";
import { useRouter } from "next/router";
import { INVENTORY_SOURCE } from "@lib/constants";

const InventorySourceFilter = () => {
  const { t } = useTranslation("components");
  const router = useRouter();
  const [isExpanded, setIsExpanded] = useState<boolean>(true);
  const {
    filterData,
    setFiltersWithMartin,
    shouldResetFields,
    filtersWithMartin,
  } = useFiltersWithMartinContext()!;
  const [selectedInventorySource, setSelectedInventorySource] = useState<
    string[]
  >([]);
  const [isButtonHovered, setIsButtonHovered] = useState<boolean>(false);

  useEffect(() => {
    setSelectedInventorySource(filtersWithMartin?.inventory_source ?? []);
  }, []);

  const handleToggleExpanded = (newState: boolean) => {
    setIsExpanded(newState);
  };

  const handleHoverChange = (isHovered: boolean) => {
    setIsButtonHovered(isHovered);
  };

  const handleChipClick = (option: string) => {
    router.replace(router.pathname);
    setSelectedInventorySource((prevState) =>
      prevState.includes(option)
        ? prevState.filter((item) => item !== option)
        : [...prevState, option]
    );
  };

  useEffect(() => {
    setFiltersWithMartin(() => {
      let newState = { ...filtersWithMartin };
      selectedInventorySource.length > 0
        ? (newState.inventory_source = selectedInventorySource)
        : delete newState.inventory_source;
      return newState;
    });
  }, [selectedInventorySource, setFiltersWithMartin]);

  useEffect(() => {
    if (shouldResetFields) {
      setSelectedInventorySource([]);
    }
  }, [shouldResetFields]);

  const filtersList = Object.entries(filtersWithMartin).filter(
    ([key, value]) => key === "inventory_source"
  );

  return (
    <>
      <Box
        sx={{
          p: 2,
          border: isButtonHovered
            ? "1px solid #2FA37C"
            : "1px solid rgba(67, 74, 74,.3)",
          borderRadius: "5px",
          display: "flex",
          alignItems: "center",
          flexDirection: "row",
          justifyContent: "center",
          flexWrap: "wrap",
          position: "relative",
        }}
      >
        {isExpanded ? (
          <>
            {INVENTORY_SOURCE.map((option, index) => (
              <Chip
                key={index}
                label={t(
                  `components.LocationForm.properties.inventory_source.${option}`
                )}
                variant="outlined"
                onClick={() => handleChipClick(option)}
                sx={{
                  m: 0.5,
                  backgroundColor: selectedInventorySource?.includes(option)
                    ? "#2FA37C"
                    : undefined,
                }}
              />
            ))}
          </>
        ) : (
          ""
        )}
        {filtersList.length > 0 ? (
          <>
            <Typography sx={{ pb: 1, display: isExpanded ? "none" : "block" }}>
              {t("Map.Filter.active")}
            </Typography>
            {filtersList.map(([key, value], index) => {
              const displayValue = renderValue(
                key,
                value,
                filterData,
                filtersWithMartin,
                t
              );
              return (
                <Typography
                  key={index}
                  variant="inherit"
                  sx={{
                    mb: 0.5,
                    fontFamily: "Poppins",
                    alignSelf: "flex-start",
                    display: isExpanded ? "none" : "block",
                    color:
                      typeof displayValue === "boolean" ? "#2FA37C" : undefined,
                    fontStyle:
                      typeof displayValue === "boolean" ? "normal" : undefined,
                  }}
                >
                  <em style={{ color: "#2FA37C", fontStyle: "normal" }}>{`${t(
                    `components.Map.Filter.${key}`
                  )}${typeof displayValue !== "boolean" ? " : " : ""}`}</em>
                  {typeof displayValue !== "boolean" ? displayValue : ""}
                </Typography>
              );
            })}
          </>
        ) : (
          <Typography sx={{ display: isExpanded ? "none" : "block" }}>
            {t("Map.Filter.empty")}
          </Typography>
        )}
        <Typography
          sx={{
            position: "absolute",
            top: "-12px",
            left: "10px",
            backgroundColor: "white",
            padding: "0 4px",
            color: isButtonHovered ? "#2FA37C" : "rgba(67, 74, 74,.9)",
          }}
        >
          {t("LocationForm.labels.inventory_source")}
        </Typography>
      </Box>

      <IsExpanded
        onToggleExpanded={handleToggleExpanded}
        onHoverChange={handleHoverChange}
      />
    </>
  );
};

export default InventorySourceFilter;
