import { FC, useEffect } from "react";
import Map, { AttributionControl } from "react-map-gl";
import maplibregl from "maplibre-gl";
import DeckGL from "@deck.gl/react/typed";
import { Box } from "@mui/material";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import { getFilterWithMartinDataService } from "@services/FilterService";
import { useSession } from "next-auth/react";

export interface InitialViewState {
  longitude: number;
  latitude: number;
  zoom: number;
  pitch?: number;
  bearing?: number;
  transitionDuration?: number;
  transitionInterpolator?: any;
}

interface MapBaseProps {
  initialViewState: InitialViewState;
  controller?: boolean;
  mapStyle?: string;
  layers: any[];
  editionMode?: boolean;
  onClick(info): void;
  handleOnViewStateChange(viewState: any): void;
  children: JSX.Element;
}

const styles = {
  mapBaseWrapper: {
    height: "100%",
    width: "100%",
    position: "relative",
  },
};

const MapBase: FC<MapBaseProps> = ({
  initialViewState,
  mapStyle = "/mapStyles/map_light.json",
  layers,
  editionMode,
  onClick,
  handleOnViewStateChange,
  children,
}) => {
  const { setFilterData, filterData } = useFiltersWithMartinContext()!;
  const session = useSession();
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";

  useEffect(() => {
    const fetchFilterData = async () => {
      try {
        const data = await getFilterWithMartinDataService(currentUserToken);
        setFilterData(data);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des données de filtre",
          error
        );
      }
    };

    fetchFilterData();
  }, []);
  let isHovering = false;

  return (
    <Box sx={styles.mapBaseWrapper} data-cy="map-base-wrapper">
      <DeckGL
        controller={{ doubleClickZoom: false, inertia: true }}
        onViewStateChange={handleOnViewStateChange}
        viewState={initialViewState}
        layers={layers}
        onClick={onClick}
        getCursor={() => (editionMode === true ? "crosshair" : "pointer")}
        onHover={({ object }) => (isHovering = Boolean(object))}
      >
        <Map
          mapLib={maplibregl as any}
          mapStyle={mapStyle}
          attributionControl={false}
          reuseMaps
        >
          <AttributionControl position="bottom-left" />
        </Map>
      </DeckGL>

      {children}
    </Box>
  );
};

export default MapBase;
