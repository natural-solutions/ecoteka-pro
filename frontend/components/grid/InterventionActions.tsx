import { Box, IconButton, Stack, TextField, Tooltip } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import CheckIcon from "@mui/icons-material/Check";
import { FC, useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { ModalComponent } from "../forms/modal/ModalComponent";
import { Controller, useForm } from "react-hook-form";
import ConfirmDialog from "../forms/dialog/ConfirmDialog";
import { FreeSolo, IFreeSoloOption } from "../forms/autocomplete/FreeSolo";
import {
  FetchInterventionPartnersQuery,
  Intervention,
  useFetchInterventionPartnersLazyQuery,
} from "../../generated/graphql";

export interface IInterventionActions {
  intervention: Intervention;
  onValidateIntervention?(
    id: string,
    interventionDate: string,
    interventionPartner: string,
    validationNote: string
  ): void;
  interventionPartners?: FetchInterventionPartnersQuery["intervention_partner"];
}

const styles = {
  icon: {
    color: "#fff",
  },
};

const InterventionActions: FC<IInterventionActions> = ({
  intervention,
  onValidateIntervention,
}) => {
  const now = new Date();

  const [selectedId, setSelectedId] = useState<string>("");
  const { t } = useTranslation();
  const [openDeletionModal, setOpenDeletionModal] = useState<boolean>(false);
  const [message, setMessage] = useState("");
  const [fetchInterventionPartners, { data: partners }] =
    useFetchInterventionPartnersLazyQuery();

  const { control, getValues, setValue } = useForm();

  const onConfirm = async (id: string) => {
    const interventionDate = getValues("realization_date");
    const interventionPartner = getValues("intervention_partner.id");
    const validationNote = getValues("validation_note");

    onValidateIntervention &&
      onValidateIntervention(
        id,
        interventionDate,
        interventionPartner,
        validationNote
      );
    setOpenDeletionModal(false);
  };

  useEffect(() => {
    setValue("intervention_partner", intervention?.intervention_partner);
  }, [intervention]);

  useEffect(() => {
    fetchInterventionPartners();
  }, [fetchInterventionPartners]);

  return (
    <>
      <Stack direction="row" justifyContent="flex-end" spacing={1}>
        {!Boolean(intervention.realization_date) && (
          <>
            <Tooltip title={t("components.InterventionForm.message.validate")}>
              <IconButton
                onClick={() => {
                  setSelectedId(intervention.id);

                  setMessage(t("components.InterventionForm.message.validate"));
                  setOpenDeletionModal(true);
                }}
                style={{ backgroundColor: "#06B6AE" }}
              >
                <CheckIcon sx={styles.icon} />
              </IconButton>
            </Tooltip>
          </>
        )}
      </Stack>
      <Box>
        <ModalComponent
          open={openDeletionModal}
          handleClose={() => setOpenDeletionModal(false)}
        >
          <ConfirmDialog
            title={message}
            onConfirm={() => onConfirm(selectedId)}
            onAbort={() => setOpenDeletionModal(false)}
          >
            <>
              <Controller
                name={`realization_date`}
                control={control}
                defaultValue={now.toISOString().split("T")[0]}
                render={({ field: { onChange, value } }) => {
                  return (
                    <TextField
                      label={
                        t(
                          `components.Intervention.properties.realizationDate`
                        ) as string
                      }
                      variant="filled"
                      type="date"
                      sx={{ width: "100%", m: 1 }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      inputProps={{
                        max: now.toISOString().split("T")[0],
                      }}
                      value={value}
                      onChange={(e) => onChange(e.target.value)}
                    />
                  );
                }}
              />
              <Controller
                name={`intervention_partner`}
                control={control}
                render={() => {
                  return (
                    <Box sx={{ width: "100%", m: 1 }}>
                      <FreeSolo
                        defaultValue={""}
                        control={control}
                        setValue={setValue}
                        name={"intervention_partner" as keyof Intervention}
                        listOptions={
                          partners?.intervention_partner ??
                          ([] as IFreeSoloOption[])
                        }
                        required={false}
                        label={
                          t(
                            `components.Intervention.properties.interventionPartner`
                          ) as string
                        }
                        object={"intervention"}
                      />
                    </Box>
                  );
                }}
              />
            </>
          </ConfirmDialog>
        </ModalComponent>
      </Box>
    </>
  );
};

export default InterventionActions;
