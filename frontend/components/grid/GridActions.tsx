import { Box, Button, Stack, TextField } from "@mui/material";
import { FC, useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { ModalComponent } from "../forms/modal/ModalComponent";
import { Controller, useForm } from "react-hook-form";
import ConfirmDialog from "../forms/dialog/ConfirmDialog";
import { FreeSolo, IFreeSoloOption } from "../forms/autocomplete/FreeSolo";
import DeleteIcon from "@mui/icons-material/Delete";
import CheckIcon from "@mui/icons-material/Check";
import {
  FetchInterventionPartnersQuery,
  Intervention,
  useFetchInterventionPartnersLazyQuery,
} from "../../generated/graphql";
import { useRouter } from "next/router";
import { useDeviceSize } from "@lib/utils";

export interface IGridActions {
  ids: string[];
  selectedLocationsIds: string[];
  onValidateIntervention?(
    interventionIds: string[],
    selectedLocationsIds: string[],
    interventionDate: string,
    interventionPartner: string,
    validationNote: string
  ): void;
  onDeleteObjects?(ids: string[]): void;
  interventionPartners?: FetchInterventionPartnersQuery["intervention_partner"];
  clearRowSelection: () => void;
}

const GridActions: FC<IGridActions> = ({
  ids,
  selectedLocationsIds,
  onValidateIntervention,
  onDeleteObjects,
  clearRowSelection,
}) => {
  const now = new Date();
  const { t } = useTranslation();
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [action, setAction] = useState("");
  const router = useRouter();
  const pathname = router.pathname;
  const { isMobile } = useDeviceSize();

  const isIntervention = pathname.includes("intervention");
  const isDiagnosis = pathname.includes("diagnosis");

  const [fetchInterventionPartners, { data: partners }] =
    useFetchInterventionPartnersLazyQuery();

  const { control, getValues, setValue } = useForm();

  const onConfirm = async () => {
    const interventionDate = getValues("realization_date");
    const interventionPartner = getValues("intervention_partner.id");
    const validationNote = getValues("validation_note");

    onValidateIntervention &&
      onValidateIntervention(
        ids,
        selectedLocationsIds,
        interventionDate,
        interventionPartner,
        validationNote
      );
    clearRowSelection();
    setOpenModal(false);
  };

  const onConfirmDelete = async () => {
    onDeleteObjects && onDeleteObjects(ids);
    clearRowSelection();
    setOpenModal(false);
  };

  useEffect(() => {
    if (isIntervention) {
      fetchInterventionPartners();
    }
  }, [fetchInterventionPartners, pathname]);

  return (
    <>
      <Stack
        direction="row"
        justifyContent={isMobile ? "flex-start" : "flex-end"}
        spacing={isMobile ? 0 : 1}
        flexWrap={isMobile ? "wrap" : "nowrap"}
      >
        <>
          <Button
            variant="text"
            color="error"
            onClick={() => {
              setAction("delete");
              setOpenModal(true);
            }}
            sx={{
              padding: "8px 22px",
              mb: isMobile ? 1 : 0,
            }}
          >
            {isMobile ? (
              <DeleteIcon color="error" />
            ) : (
              t("common.buttons.delete")
            )}
          </Button>
          {isIntervention && (
            <Button
              variant="text"
              color="primary"
              size="medium"
              onClick={() => {
                setAction("validate");
                setOpenModal(true);
              }}
              sx={{
                padding: "8px 22px",
              }}
            >
              {isMobile ? (
                <CheckIcon color="success" />
              ) : (
                t("components.Template.menuItems.intervention.declareRealized")
              )}
            </Button>
          )}
        </>
      </Stack>
      <Box>
        <ModalComponent
          open={openModal}
          handleClose={() => setOpenModal(false)}
        >
          <ConfirmDialog
            title={
              action === "validate"
                ? t(`components.InterventionForm.message.validate_plural`)
                : isIntervention
                ? t(`components.InterventionForm.message.delete_plural`)
                : t(`components.DiagnosisForm.message.delete_plural`)
            }
            onConfirm={action === "validate" ? onConfirm : onConfirmDelete}
            onAbort={() => setOpenModal(false)}
          >
            <>
              {action === "validate" && (
                <>
                  <Controller
                    name={`realization_date`}
                    control={control}
                    defaultValue={now.toISOString().split("T")[0]}
                    render={({ field: { onChange, value } }) => {
                      return (
                        <TextField
                          label={
                            t(
                              `components.Intervention.properties.realizationDate`
                            ) as string
                          }
                          variant="filled"
                          type="date"
                          sx={{ width: "100%", m: 1 }}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          inputProps={{
                            max: now.toISOString().split("T")[0],
                          }}
                          value={value}
                          onChange={(e) => onChange(e.target.value)}
                        />
                      );
                    }}
                  />
                  <Controller
                    name={`intervention_partner`}
                    control={control}
                    render={() => {
                      return (
                        <Box sx={{ width: "100%", m: 1 }}>
                          <FreeSolo
                            defaultValue={""}
                            control={control}
                            setValue={setValue}
                            name={"intervention_partner" as keyof Intervention}
                            listOptions={
                              partners?.intervention_partner ??
                              ([] as IFreeSoloOption[])
                            }
                            required={false}
                            label={
                              t(
                                `components.Intervention.properties.interventionPartner`
                              ) as string
                            }
                            object={"intervention"}
                          />
                        </Box>
                      );
                    }}
                  />

                  <Controller
                    name={`validation_note`}
                    control={control}
                    defaultValue={""}
                    render={({ field: { onChange, ref, value } }) => (
                      <TextField
                        value={value}
                        inputRef={ref}
                        fullWidth
                        sx={{
                          display: "flex",
                          m: 1,
                        }}
                        multiline={true}
                        label={
                          t(`components.Intervention.properties.note`) as string
                        }
                        variant="filled"
                        placeholder={t(
                          "components.Intervention.placeholder.validation_note"
                        )}
                        onChange={onChange}
                      />
                    )}
                  />
                </>
              )}
            </>
          </ConfirmDialog>
        </ModalComponent>
      </Box>
    </>
  );
};

export default GridActions;
