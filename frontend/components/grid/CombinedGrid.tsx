import { Box, IconButton } from "@mui/material";
import { FC } from "react";
import { Tree, Location, Vegetated_Area } from "../../generated/graphql";
import useTableLocale from "../../lib/useTableLocale";
import {
  MRT_ColumnDef,
  MRT_RowData,
  MaterialReactTable,
  useMaterialReactTable,
} from "material-react-table";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { useRouter } from "next/router";
import { download, generateCsv } from "export-to-csv";
import ExportButtons, { csvConfig } from "./buttons/ExportButtons";
import { useTranslation } from "react-i18next";

export type ColumnVisibility = {
  serial_number: boolean;
  vernacular_name: boolean;
  plantation_date: boolean;
  variety: boolean;
  height: boolean;
  circumference: boolean;
  habit: boolean;
  is_tree_of_interest: boolean;
  watering: boolean;
  coords: any;
  foot_type: boolean;
  landscape_type: boolean;
  sidewalk_type: boolean;
  plantation_type: boolean;
  is_protected: boolean;
  urban_site: boolean;
  urban_constraint: boolean;
  inventory_source: boolean;
  note: boolean;
  creation_date: boolean;
  surface: boolean;
  afforestation_trees_data: boolean;
  shrubs_data: boolean;
  herbaceous_data: boolean;
  has_differentiated_mowing: boolean;
  is_accessible: boolean;
  residential_usage: boolean;
  inconvenience_risk: boolean;
  hedge_type: boolean;
  linear_meters: boolean;
  potential_area_state: boolean;
};

export interface CombinedGridProps {
  data: (Tree | Location | Vegetated_Area)[];
  columns: MRT_ColumnDef<Tree | Location | Vegetated_Area>[];
  pagination: {
    pageIndex: number;
    pageSize: number;
  };
  setPagination: (pagination: { pageIndex: number; pageSize: number }) => void;
  columnVisibility: ColumnVisibility;
  setColumnVisibility: (visibility: ColumnVisibility) => void;
  totalCount: number;
  isLoading: boolean;
  exportData: () => void;
}

export const CombinedGrid: FC<CombinedGridProps> = ({
  data,
  columns,
  pagination,
  setPagination,
  totalCount,
  isLoading,
  exportData,
  columnVisibility,
  setColumnVisibility,
}) => {
  const localization = useTableLocale();
  const { t } = useTranslation();
  const router = useRouter();
  const transformCombinedData = (item: any) => {
    let originalItem = item;
    if (item.original) {
      originalItem = item.original;
    }

    return {
      id: originalItem.id,
      address: originalItem.address || "",
      type:
        originalItem.__typename === "location" ||
        originalItem.__typename === "tree"
          ? t(`common.emplacement`)
          : t(`components.CombinedGrid.vegetated_area`),
      domanialite: originalItem.boundaries_administrative || "",
      secteur_geo: originalItem.boundaries_geographic || "",
      station: originalItem.boundaries_station || "",
      arbre_nom_scientifique: originalItem.scientific_name,
      superficie: originalItem.surface,
      occupation: (() => {
        switch (originalItem.__typename) {
          case "vegetated_area":
            const afforestationTreesCount =
              (originalItem as Vegetated_Area).afforestation_trees_data?.reduce(
                (sum, item) => sum + (item.number || 0),
                0
              ) || 0;
            const locationsCount =
              (originalItem as Vegetated_Area).vegetated_areas_locations || 0;

            return `${(originalItem as Vegetated_Area).type === "afforestation" ? afforestationTreesCount + locationsCount : locationsCount} ${t("common.trees")}`;
          case "location":
            return t(`common.${(originalItem as Location).location_status}`);
          case "tree":
            return t(`common.alive`);
          default:
            return "";
        }
      })(),
      arbre_matricule: originalItem.serial_number,
      arbre_hauteur: originalItem.height || "",
      arbre_circonference: originalItem.circumference,
      arbre_nom_vernaculaire: originalItem.vernacular_name,
      arbre_variete: originalItem.variety,
      arbre_date_plantation: originalItem.plantation_date,
      arbre_port: originalItem.habit,
      arbre_remarquable: originalItem.is_tree_of_interest
        ? t("common.yes")
        : t("common.no"),
      arbre_arrosage: originalItem.watering ? t("common.yes") : t("common.no"),
      coordonnees: (() => {
        if (
          originalItem.__typename === "location" ||
          originalItem.__typename === "tree"
        ) {
          const [longitude, latitude] = originalItem.coords.coordinates;
          return `${latitude} ${longitude}`;
        }
        return "";
      })(),
      description_pied:
        originalItem.__typename === "location" ? originalItem.foot_type : "",
      type_station: originalItem.landscape_type
        ? t(
            `components.LocationForm.properties.landscape_type.${originalItem.landscape_type}`
          )
        : "",
      type_trottoir: originalItem.sidewalk_type
        ? t(
            `components.LocationForm.properties.sidewalk_type.${originalItem.sidewalk_type}`
          )
        : "",
      contexte_plantation: originalItem.plantation_type
        ? t(
            `components.LocationForm.properties.plantation_type.${originalItem.plantation_type}`
          )
        : "",
      protection_pied: Boolean(originalItem.is_protected)
        ? t("common.yes")
        : t("common.no"),
      environnement_urbain: originalItem.urban_site
        ? t(
            `components.LocationForm.properties.urban_site.${originalItem.urban_site}`
          )
        : "",
      contraintes_urbaines: (() => {
        return originalItem.urban_constraint
          ? originalItem.urban_constraint
              ?.map((option) =>
                t(
                  `components.LocationForm.properties.urban_constraint.${option}`
                )
              )
              .join(",")
          : "";
      })(),
      source_inventaire: originalItem.inventory_source
        ? originalItem.inventory_source
            .map((s) =>
              t(`components.LocationForm.properties.inventory_source.${s}`)
            )
            .join(",")
        : "",
      remarques: originalItem.note || "",
      date_creation: originalItem.creation_date || "",
      type_surface: originalItem.type
        ? t(
            `components.VegetatedAreaForm.properties.typeLabels.${originalItem.type}`
          )
        : "",
      arbres_boisement: (() => {
        if (originalItem.__typename === "vegetated_area") {
          return originalItem.afforestation_trees_data
            ?.filter((data) => data.number !== 0 || data.specie !== "")
            .map((data) => `${data.specie} (${data.number})`)
            .join(", ");
        }
        return "";
      })(),
      arbustes: (() => {
        if (originalItem.__typename === "vegetated_area") {
          return originalItem.shrubs_data
            ?.filter((data) => data.number !== 0 || data.specie !== "")
            .map((data) => `${data.specie} (${data.number})`)
            .join(", ");
        }
        return "";
      })(),
      herbacees: (() => {
        if (originalItem.__typename === "vegetated_area") {
          return originalItem.herbaceous_data
            ?.filter((data) => data.number !== 0 || data.specie !== "")
            .map((data) => `${data.specie} (${data.number})`)
            .join(", ");
        }
        return "";
      })(),
      arbres_sur_emplacement:
        originalItem.__typename === "vegetated_area"
          ? originalItem.vegetated_areas_locations
          : "",
      tonte_differenciee:
        originalItem.__typename === "vegetated_area"
          ? originalItem.has_differentiated_mowing
            ? t("common.yes")
            : t("common.no")
          : "",
      accessibilite:
        originalItem.__typename === "vegetated_area"
          ? originalItem.is_accessible
            ? t("common.yes")
            : t("common.no")
          : "",
      type_usage: (() => {
        if (originalItem.__typename === "vegetated_area") {
          return originalItem.residential_usage
            ? originalItem.residential_usage
                ?.map((option) =>
                  t(
                    `components.VegetatedAreaForm.properties.residential_usage_type.${option}`
                  )
                )
                .join(",")
            : "";
        }
        return "";
      })(),
      risque_gene:
        originalItem.__typename === "vegetated_area"
          ? originalItem.inconvenience_risk
          : "",
      type_haie:
        originalItem.__typename === "vegetated_area" && originalItem.hedge_type
          ? t(
              `components.VegetatedAreaForm.properties.hedge_type.${originalItem.hedge_type}`
            )
          : "",
      metres_lineaires:
        originalItem.__typename === "vegetated_area"
          ? originalItem.linear_meters
          : "",
      etat_actuel_surface:
        originalItem.__typename === "vegetated_area" &&
        originalItem.potential_area_state
          ? t(
              `components.VegetatedAreaForm.properties.potential_area_state.${originalItem.potential_area_state}`
            )
          : "",
    };
  };

  const handleExport = (data: MRT_RowData[]) => {
    const rowData = data.map((item) => transformCombinedData(item));

    const csv = generateCsv(csvConfig)(rowData);
    download(csvConfig)(csv);
  };

  const table = useMaterialReactTable({
    columns: columns as any,
    data: data,
    localization: localization,
    enableGlobalFilter: false,
    enableTopToolbar: true,
    enablePagination: true,
    enableRowActions: true,
    enableColumnResizing: true,
    getRowId: (originalRow) => originalRow.id,
    renderTopToolbarCustomActions: (table) => (
      <ExportButtons
        table={table?.table}
        data={data}
        exportFilteredData={handleExport}
        exportAllData={exportData}
      />
    ),
    manualPagination: true,
    pageCount: Math.ceil(totalCount / pagination.pageSize),
    onPaginationChange: setPagination,
    muiPaginationProps: {
      rowsPerPageOptions: [10, 20, 30, 50, 100],
      showFirstButton: false,
      showLastButton: false,
    },
    positionActionsColumn: "last",
    autoResetPageIndex: false,
    rowCount: totalCount,
    enableHiding: true,
    enableDensityToggle: false,
    state: {
      pagination,
      isLoading,
      density: "compact",
      columnVisibility,
    },
    onColumnVisibilityChange: setColumnVisibility,
    renderRowActions: ({ row }) => (
      <Box>
        <IconButton
          onClick={() => {
            const type = row.original.__typename;
            if (type === "tree") {
              router.push(`/tree/${row.original.id}`);
            } else if (type === "location") {
              router.push(`/location/${row.original.id}`);
            } else if (type === "vegetated_area") {
              router.push(`/vegetated-area/${row.original.id}`);
            }
          }}
        >
          <ChevronRightIcon />
        </IconButton>
      </Box>
    ),
  });

  return (
    <Box>
      <MaterialReactTable table={table} />
    </Box>
  );
};

export default CombinedGrid;
