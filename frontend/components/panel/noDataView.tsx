import { Box, Button, Typography } from "@mui/material";
import Image from "next/image";
import React, { FC } from "react";
import { useTranslation } from "react-i18next";
import { FetchOneLocationQuery } from "../../generated/graphql";
import { useRouter } from "next/router";
import AddIcon from "@mui/icons-material/Add";
import { useDeviceSize } from "@lib/utils";

export interface NoDataViewProps {
  element: string;
  activeLocation: FetchOneLocationQuery["location"][0];
  showTitle?: boolean;
  disableAddButtons: boolean;
}

const NoDataView: FC<NoDataViewProps> = ({
  element,
  activeLocation,
  showTitle = true,
  disableAddButtons = false,
}) => {
  const { t } = useTranslation();
  const { isMobile } = useDeviceSize();

  const router = useRouter();

  return (
    <Box sx={styles.box}>
      {showTitle && (
        <Typography variant="h5" textAlign="left">
          {t(`components.PanelTree.noData.${element}.title`) as string}
        </Typography>
      )}

      <Image
        alt="tree"
        src={`/components/map/treeModal.png`}
        width={197}
        height={169}
      />
      <Typography
        id="modal-modal-description"
        sx={{ mt: 1, mb: 1, fontSize: 14 }}
      >
        {t(`components.PanelTree.noData.${element}.message`) as string}
      </Typography>
      <Button
        variant="contained"
        sx={{ width: isMobile ? "100%" : "auto" }}
        startIcon={<AddIcon />}
        disabled={disableAddButtons}
        onClick={() =>
          element === "intervention"
            ? router.replace(
                `/tree/${activeLocation.tree[0].id}?interventionModal=open`
              )
            : router.replace(`/tree/${activeLocation?.id}?diagnosisModal=open`)
        }
      >
        {t(`components.PanelTree.noData.${element}.add`) as string}
      </Button>
    </Box>
  );
};

export default NoDataView;

const styles = {
  box: {
    p: 2,
    textAlign: "center",
    background: "#FBFDFE",
    boxShadow:
      "0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px rgba(0, 0, 0, 0.14), 0px 1px 5px rgba(0, 0, 0, 0.12)",
    borderRadius: "4px",
  },
};
