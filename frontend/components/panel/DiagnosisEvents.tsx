import React, { FC } from "react";
import NoDataView from "./noDataView";
import DiagnosisCard from "../_blocks/layout/DiagnosisCard/DiagnosisCard";

export interface DiagnosisEventProps {
  activeTree: any;
  disableAddButtons: boolean;
}

const DiagnosisEvents: FC<DiagnosisEventProps> = ({
  activeTree,
  disableAddButtons = false,
}) => {
  const lastDiagnosis = activeTree?.last_diagnoses[0];

  const LastDiagnosisBox: FC = () => {
    return (
      <DiagnosisCard
        diagnosis={lastDiagnosis}
        disableAddButtons={disableAddButtons}
      />
    );
  };

  return (
    <>
      {!lastDiagnosis && (
        <NoDataView
          element="diagnosis"
          activeLocation={activeTree}
          disableAddButtons={disableAddButtons}
        />
      )}
      {lastDiagnosis && <LastDiagnosisBox />}
    </>
  );
};

export default DiagnosisEvents;
