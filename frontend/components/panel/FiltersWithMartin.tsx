import React, { FC } from "react";
import { FormGroup } from "@mui/material";
import { MapFilters } from "@lib/store/pages/mapEditor";
import TreeFilter from "@components/map/filter/TreeFilter";
import DiagnosticFilter from "@components/map/filter/DiagnosticFilter";
import LocationFilter from "@components/map/filter/LocationFilter";
import InterventionFilter from "@components/map/filter/InterventionFilter";
import BoundaryFilter from "@components/map/filter/BoundaryFilter";
import WorksiteFilter from "@components/map/filter/WorksiteFilter";
import VegetatedAreaFilter from "@components/map/filter/VegetatedAreaFilter";
import InventorySourceFilter from "@components/map/filter/InventorySourceFilter";
import AdministrativeBoundaryFilter from "@components/map/filter/AdministrativeBoundaryFilter";

export interface PanelInfoProps {
  handleLocationStatusFilterChange?: (
    id: string,
    e: React.ChangeEvent<HTMLInputElement>
  ) => void;
  filtersWithMartin?: MapFilters;
  locationStatusList?: any;
}

const PanelFiltersWithMartin: FC<PanelInfoProps> = ({}) => {
  return (
    <FormGroup data-cy="map-filters">
      <LocationFilter />
      <VegetatedAreaFilter />
      <AdministrativeBoundaryFilter />
      <BoundaryFilter />
      <InventorySourceFilter />
      <WorksiteFilter />
      <TreeFilter />
      <DiagnosticFilter />
      <InterventionFilter />
    </FormGroup>
  );
};

export default PanelFiltersWithMartin;
