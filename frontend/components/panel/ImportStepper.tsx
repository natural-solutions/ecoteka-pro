import {
  Box,
  Button,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  FormControl,
  Step,
  StepLabel,
  Stepper,
  Typography,
} from "@mui/material";
import Link from "@mui/material/Link";
import React, { FC, useState } from "react";
import { useTranslation } from "react-i18next";
import { Container } from "@mui/system";
import UploadComponent from "../Upload";
import { useCreateImportMutation } from "../../generated/graphql";
import { useSession } from "next-auth/react";
import useStore from "../../lib/store/useStore";

export interface ImportStepperProps {}

const ImportStepper: FC<ImportStepperProps> = ({}) => {
  const { app } = useStore((store) => store);
  const { t } = useTranslation(["components"]);
  const [projectionSystem, setProjectionSystem] = useState<string>("");
  const [activeStep, setActiveStep] = React.useState(0);
  const [createImport] = useCreateImportMutation();
  const [loadingSuccess, setLoadingSuccess] = useState<boolean>(false);
  const [filePathUrl, setFilePathUrl] = useState<string>("");
  const [fileName, setFileName] = useState<string>("");
  const [isDisabled, setIsDisabled] = useState<boolean>(false);

  const session = useSession();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";

  const handleChange = (event: SelectChangeEvent) => {
    setProjectionSystem(event.target.value as string);
  };

  const steps = ["one", "two", "three"];
  const projectionSystems = [
    { label: "EPSG:6171 / RGF93", value: "EPSG:6171" },
    { label: "EPSG:4965 (3D) / RGF93", value: "EPSG:4965" },
    { label: "EPSG:4171 (2D) / RGF93", value: "EPSG:4171" },
    { label: "EPSG:4807 (2D, Paris, grade) / NTF", value: "EPSG:4807" },
    { label: "EPSG:4275 (2D, Greenwich, degré) / NTF", value: "EPSG:4275" },
    { label: "EPSG:7400 (3D, Paris, grade) / NTF", value: "EPSG:7400" },
    { label: "EPSG:4937 (3D) / ETRS89", value: "EPSG:4937" },
    { label: "EPSG:4258 (2D) / ETRS90", value: "EPSG:4258" },
    { label: "EPSG:4979 (3D) / WGS84", value: "EPSG:4979" },
    { label: "EPSG:4326 (2D) / WGS84", value: "EPSG:4326" },
    { label: "EPSG:3857 / WGS84", value: "EPSG:3857" },
  ];

  const isStepOptional = (step: number) => {
    return step === 0;
  };

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      throw new Error("You can't skip a step that isn't optional.");
    }
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const onSuccess = () => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`components.Import.Importing.importingText`),
            severity: "success",
          },
        ],
      },
    });
  };

  const handleCreateImport = async () => {
    createImport({
      variables: {
        data_entry_user_id: currentUserId,
        source_crs: projectionSystem,
        source_file_path: filePathUrl,
        organization_id: app.organization?.id,
      },
      onCompleted: () => {
        setIsDisabled(true);
        onSuccess();
      },
    });
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Stepper activeStep={activeStep}>
        {steps.map((label, index) => {
          const stepProps: { completed?: boolean } = {};
          const labelProps: {
            optional?: React.ReactNode;
          } = {};

          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>
                {t(`components.ImportTemplate.steps.${label}`)}
              </StepLabel>
            </Step>
          );
        })}
      </Stepper>
      {activeStep === steps.length ? (
        <React.Fragment>
          <Typography sx={{ mt: 2, mb: 1 }}>
            Import effectué avec succès
          </Typography>
        </React.Fragment>
      ) : (
        <Container maxWidth="sm" sx={{ mt: 10 }}>
          {(activeStep === 0 || activeStep === 2) && (
            <Typography sx={{ mt: 2, mb: 1 }}>
              {t(
                `components.ImportTemplate.stepsMessages.${steps[activeStep]}`
              )}
            </Typography>
          )}
          {activeStep === 0 && (
            <Box>
              <Button
                variant="contained"
                href="/components/import/squelette_import_arbre_v1.xlsx"
              >
                {t(`components.ImportTemplate.labels.downloadSkeleton`)}
              </Button>
            </Box>
          )}
          {activeStep === 1 && (
            <FormControl fullWidth>
              <InputLabel id="select-projection-system">
                {t(`components.ImportTemplate.steps.two`)}
              </InputLabel>
              <Select
                labelId="select-projection-system"
                id="select-projection-system"
                value={projectionSystem}
                label={t(`components.ImportTemplate.steps.two`)}
                onChange={handleChange}
              >
                {projectionSystems.map((item, i) => {
                  return (
                    <MenuItem key={i} value={item.value}>
                      {item.label}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          )}
          {activeStep === 2 && (
            <>
              {!loadingSuccess ? (
                <UploadComponent
                  path={`imports/${Date.now()}`}
                  onUploaded={(path, file) => {
                    setLoadingSuccess(true);
                    setFilePathUrl([path, file.name].join("/"));
                    setFileName(file.name);
                  }}
                  context="import"
                />
              ) : (
                <Typography fontWeight={"bold"}>{fileName}</Typography>
              )}
            </>
          )}
          {activeStep === 2 && loadingSuccess && (
            <Box sx={{ textAlign: "center" }}>
              <Button
                onClick={handleCreateImport}
                variant="contained"
                disabled={isDisabled}
              >
                {"Déclencher l'import"}
              </Button>
            </Box>
          )}
          <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
            <Button
              color="inherit"
              disabled={activeStep === 0}
              onClick={handleBack}
              sx={{ mr: 1 }}
            >
              {t("common.back")}
            </Button>
            <Box sx={{ flex: "1 1 auto" }} />
            {isStepOptional(activeStep) && (
              <Button color="inherit" onClick={handleSkip} sx={{ mr: 1 }}>
                {t("common.buttons.skip")}
              </Button>
            )}
            {!isStepOptional(activeStep) && (
              <Button onClick={handleNext}>
                {activeStep === steps.length - 1
                  ? t("common.buttons.finish")
                  : t("common.buttons.validate")}
              </Button>
            )}
          </Box>
        </Container>
      )}
    </Box>
  );
};

export default ImportStepper;
