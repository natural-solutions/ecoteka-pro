export const TREE_CONDITIONS = [
  "healthy",
  "bad",
  "poor",
  "stressed",
  "resilient",
  "wilting",
] as const;

export const VIGOR_CONDITIONS = ["good", "medium", "low"] as const;

export const COLLAR_TRUNK_CONDITIONS = [
  "healthy",
  "bad",
  "poor",
  "sick",
  "cavities",
  "bark_included",
  "hurt",
  "necrosis",
] as const;

export const CROWN_CARPENTERS_CONDITIONS = [
  "healthy",
  "bad",
  "poor",
  "sick",
  "hurt",
  "dead_wood",
  "bark_included",
  "cavities",
  "necrosis",
] as const;

export const ORGANS = [
  "all",
  "root",
  "collar",
  "trunk",
  "carpenters",
  "summit",
] as const;

export const RECOMMENDATIONS = [
  "dead_wood",
  "pruning",
  "visual_diagnosis",
  "carpenters",
  "felling",
  "none",
] as const;

// TODO: move to constants.types.ts
export type TLocationStatus = "alive" | "stump" | "empty";
export type TLocationStatuses = TLocationStatus[];
export const LOCATION_STATUSES: TLocationStatuses = ["alive", "stump", "empty"];
export const TREE_HABIT = [
  "structured",
  "clump",
  "pollard",
  "free",
  "semi_free",
  "curtain",
  "cat_head",
] as const;
export const SIDEWALK_TYPE = ["coated", "mineral", "sand", "vegetal"] as const;
export const PLANTATION_TYPE = ["pit", "open_ground", "container"] as const;
export const LANDSCAPE_TYPE = [
  "alignment",
  "afforestation",
  "grouped_2_5",
  "grouped_6_more",
  "isolated",
] as const;
export type THedge_Type = "isolated" | "linear" | "massif";
export const HEDGE_TYPE = ["isolated", "linear", "massif"];

export type TPOTENTIAL_AREA_STATE =
  | "parking"
  | "non_common_facade"
  | "vegetalizable_facade"
  | "other_surface";
export const POTENTIAL_AREA_STATE = [
  "parking",
  "non_common_facade",
  "vegetalizable_facade",
  "other_surface",
];

export type TINVENTORY_SOURCE =
  | "agent"
  | "aerial_lidar"
  | "terrestrial_lidar"
  | "ia_satellite"
  | "ia_aerial_photos"
  | "other_technology";
export const INVENTORY_SOURCE = [
  "agent",
  "aerial_lidar",
  "terrestrial_lidar",
  "ia_satellite",
  "ia_aerial_photos",
  "other_technology",
] as const;

export type TREQUEST_ORIGIN =
  | "maintenance"
  | "expertise"
  | "internal"
  | "users"
  | "weather_events"
  | "event"
  | "other";
export const REQUEST_ORIGIN = [
  "maintenance",
  "expertise",
  "internal",
  "users",
  "weather_events",
  "event",
  "other",
] as const;
