import {
  DefaultOptions,
  InMemoryCache,
  NormalizedCacheObject,
} from "@apollo/client";
import { ApolloClient } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { createUploadLink } from "apollo-upload-client";
import { Session } from "next-auth";
import { getSession, signIn, signOut } from "next-auth/react";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();
const isServer = typeof window === "undefined";
export let apolloClient: ApolloClient<NormalizedCacheObject> | undefined;

const authLink = setContext(async (_request, previousContext) => {
  const session = await getSession();
  const token = session?.accessToken;
  const error = session?.error;

  if (error === "RefreshAccessTokenError") {
    await signOut({ redirect: false });
    await signIn("keycloak");
    return;
  }

  if (token) {
    return {
      headers: { Authorization: `Bearer ${token}` },
    };
  }

  return {
    headers: previousContext.headers,
  };
});

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: "no-cache",
    errorPolicy: "ignore",
  },
  query: {
    fetchPolicy: "no-cache",
    errorPolicy: "all",
  },
};

const createApolloClient = (token?: Session) => {
  const newHttpLink = createUploadLink({
    uri: publicRuntimeConfig.GRAPHQL_API_URL,
    credentials: "include",
    headers: {
      Authorization: token?.accessToken ? `Bearer ${token.accessToken}` : "",
    },
  });

  return new ApolloClient({
    ssrMode: isServer,
    // TODO CHANGE any type
    link: authLink.concat(newHttpLink as any),
    cache: new InMemoryCache(),
    defaultOptions,
  });
};

export const initializeApollo = (session: Session) => {
  const _apolloClient = apolloClient ?? createApolloClient(session);

  if (isServer) {
    return _apolloClient;
  }

  if (!apolloClient) {
    apolloClient = _apolloClient;
  }

  return _apolloClient;
};
