import { MRT_Localization_ES } from "material-react-table/locales/es";
import { MRT_Localization_FR } from "material-react-table/locales/fr";
import { MRT_Localization_EN } from "material-react-table/locales/en";
import { useRouter } from "next/router";

const tableLocales = {
  es: MRT_Localization_ES,
  en: MRT_Localization_EN,
  fr: MRT_Localization_FR,
};

export default function useTableLocale() {
  const router = useRouter();

  return tableLocales[router.locale!];
}
