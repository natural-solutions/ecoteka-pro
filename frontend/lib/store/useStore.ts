import createVanilla from "zustand/vanilla";
import create from "zustand";

import createApp, { AppState } from "./createApp";
import createMap, { MapState } from "./createMap";

export interface GlobalState {
  app: AppState;
  map: MapState;
}

export const globalState = createVanilla<GlobalState>((set, get) => ({
  app: createApp(set, get),
  map: createMap(set, get),
}));

export default create(globalState);
