import { Residential_Usage_Type, Urban_Constraint } from "@generated/graphql";
import create, { StateCreator } from "zustand";
import { persist } from "zustand/middleware";
import * as yup from "yup";
import { THedge_Type, TPOTENTIAL_AREA_STATE } from "@lib/constants";

export type PolygonCommonData = {
  geometry?: any;
  area?: Number | null;
  locationsInside?: any;
  vegetatedAreasInside?: any;
  automaticAssociation?: boolean;
  editMode?: boolean;
};

export type PolygonObjectType = "geographic" | "station" | "vegetated_area";

export type VegetatedAreaType =
  | "potential_area"
  | "green_area"
  | "hedge"
  | "lawn"
  | "flower_bed"
  | "afforestation";

type PolygonFeatures = {
  type: string;
  features?: any;
};

export type BoundaryFormData = {
  name: string;
};

export type VegetatedAreaFormData = {
  type: VegetatedAreaType | undefined;
  address: string | undefined;
  surface: number | undefined;
  administrative_boundary: string | undefined;
  geographic_boundary: string | undefined;
  station_boundary: string | undefined;
  landscape_type: string;
  urban_site_id: string;
  urban_constraint: Urban_Constraint[];
  has_differentiated_mowing: boolean | undefined;
  is_accessible: boolean | undefined;
  residential_usage_type: Residential_Usage_Type[];
  inconvenience_risk: string;
  note: string;
  shrubs_data: [];
  herbaceous_data: [];
  afforestation_trees_data: [];
  linear_meters: number | undefined;
  hedge_type: THedge_Type | undefined;
  potential_area_state: TPOTENTIAL_AREA_STATE | undefined;
};

export interface IPolygonFormState {
  polygonCommonData: PolygonCommonData;
  polygonFeatures: PolygonFeatures;
  objectType: PolygonObjectType | undefined;
  boundaryFormData: BoundaryFormData;
  vegetatedAreaFormData: VegetatedAreaFormData | undefined;
  actions: {
    setPolygonCommonData(polygonCommonData: PolygonCommonData): void;
    setPolygonFeatures(polygonFeatures: PolygonFeatures): void;
    setPolygonObjectType(objectType: PolygonObjectType | undefined): void;
    setBoundaryFormData(boundaryFormData: BoundaryFormData): void;
    setVegetatedAreaFormData(
      vegetatedAreaFormData: VegetatedAreaFormData | undefined
    ): void;
  };
}

export const vegetatedAreaFormSchema = yup.object().shape({
  type: yup.string().required("Type is required"),
  administrative_boundary: yup.string().nullable(),
  geographic_boundary: yup.string().nullable(),
  station_boundary: yup.string().nullable(),
  landscape_type: yup.string().nullable(),
  urban_site_id: yup.string().nullable(),
  urban_constraint: yup.array().of(yup.string()).nullable(),
  has_differentiated_mowing: yup.boolean().nullable(),
  is_accessible: yup.boolean().nullable(),
  residential_usage_type: yup.array().of(yup.string()).nullable(),
  inconvenience_risk: yup.string().nullable(),
  note: yup.string().nullable(),
  shrubs_data: yup.array().nullable(),
  herbaceous_data: yup.array().nullable(),
  afforestation_trees_data: yup.array().nullable(),
});

export const defautVegetatedAreaFormData: VegetatedAreaFormData = {
  type: "potential_area",
  administrative_boundary: "",
  geographic_boundary: "",
  station_boundary: "",
  address: "",
  surface: 0,
  landscape_type: "",
  urban_site_id: "",
  urban_constraint: [],
  has_differentiated_mowing: false,
  is_accessible: false,
  residential_usage_type: [],
  inconvenience_risk: "",
  note: "",
  shrubs_data: [],
  herbaceous_data: [],
  afforestation_trees_data: [],
  linear_meters: undefined,
  hedge_type: undefined,
  potential_area_state: undefined,
};

export const initialFormDataState = {
  polygonCommonData: {
    geometry: [],
    area: null,
    locationsInside: [],
    vegetatedAreasInside: [],
    automaticAssociation: false,
    editMode: false,
  },
  polygonFeatures: {
    type: "FeatureCollection",
    features: [{ geometry: { coordinates: [] } }],
  },
  objectType: undefined,
  boundaryFormData: {
    name: "",
  },
  vegetatedAreaFormData: defautVegetatedAreaFormData,
};

export const stateCreator: StateCreator<IPolygonFormState> = (set) => ({
  ...initialFormDataState,
  actions: {
    setPolygonCommonData: (polygonCommonData: PolygonCommonData) => {
      set((state) => ({
        ...state,
        polygonCommonData,
      }));
    },
    setPolygonFeatures: (polygonFeatures: PolygonFeatures) => {
      set((state) => ({
        ...state,
        polygonFeatures,
      }));
    },
    setPolygonObjectType: (objectType: PolygonObjectType | undefined) => {
      set((state) => ({
        ...state,
        objectType,
      }));
    },
    setBoundaryFormData: (boundaryFormData: BoundaryFormData) => {
      set((state) => ({
        ...state,
        boundaryFormData,
      }));
    },
    setVegetatedAreaFormData: (
      vegetatedAreaFormData: VegetatedAreaFormData
    ) => {
      set((state) => ({
        ...state,
        vegetatedAreaFormData,
      }));
    },
  },
});

export const usePolygonFormStore = create<IPolygonFormState>()(
  persist(stateCreator, {
    name: "polygonFormStorage",
    partialize: (state) =>
      Object.fromEntries(
        Object.entries(state).filter(([key]) => !["actions"].includes(key))
      ),
  })
);

// getters hooks

export const usePolygonCommonData = () =>
  usePolygonFormStore((state) => state.polygonCommonData);

export const usePolygonFeatures = () =>
  usePolygonFormStore((state) => state.polygonFeatures);

export const usePolygonObjectType = () =>
  usePolygonFormStore((state) => state.objectType);

export const useBoundaryFormData = () =>
  usePolygonFormStore((state) => state.boundaryFormData);

export const useVegetatedAreaFormData = () =>
  usePolygonFormStore((state) => state.vegetatedAreaFormData);

// 🎉 one selector for all our actions
export const usePolygonFormActions = () =>
  usePolygonFormStore((state) => state.actions);
