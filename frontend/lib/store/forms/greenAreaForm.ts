import create, { StateCreator } from "zustand";
import { persist } from "zustand/middleware";

type GreenAreaFormData = {
  name: string;
  description?: string;
  geometry?: any;
  area?: Number | null;
};

export interface IGreenAreaFormState {
  greenArea: GreenAreaFormData;
  actions: {
    setGreenArea(greenArea: GreenAreaFormData): void;
  };
}

export const initialFormDataState = {
  greenArea: {
    name: "",
    description: "",
    geometry: [],
    area: null,
  },
};

export const stateCreator: StateCreator<IGreenAreaFormState> = (set) => ({
  ...initialFormDataState,
  actions: {
    setGreenArea: (greenArea: GreenAreaFormData) => {
      set((state) => ({
        ...state,
        greenArea,
      }));
    },
  },
});

export const useGreenAreaFormStore = create<IGreenAreaFormState>()(
  persist(stateCreator, {
    name: "greenAreaFormStorage",
    partialize: (state) =>
      Object.fromEntries(
        Object.entries(state).filter(([key]) => !["actions"].includes(key))
      ),
  })
);

// getters hooks

export const useGreenAreaFormData = () =>
  useGreenAreaFormStore((state) => state.greenArea);

// 🎉 one selector for all our actions
export const useGreenAreaFormActions = () =>
  useGreenAreaFormStore((state) => state.actions);
