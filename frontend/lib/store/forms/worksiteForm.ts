import create, { StateCreator } from "zustand";
import { persist } from "zustand/middleware";

type WorksiteFormData = {
  name: string;
  description?: string;
  scheduled_start_date?: string;
  scheduled_end_date?: string;
};

type WorksiteLocationFormData = {
  location_id: string;
  tree_id?: string | undefined;
  address: string;
  vegetated_area_id?: string | undefined;
};

export type WorksiteInterventionFormData =
  | {
      is_current?: boolean;
      associated_locations?: any; // UUID
      intervention_partner?: string;
      intervention_type?: {
        slug: string;
        id: string;
        location_types: string[];
      };
      cost?: number | null;
    }
  | undefined;

export interface IWorksiteFormState {
  worksite: WorksiteFormData;
  locations: WorksiteLocationFormData[];
  interventions: WorksiteInterventionFormData[];
  isSelectionValidated: boolean;
  hoveredItemId: string | undefined;
  activeStep: number;
  actions: {
    setWorksite(worksite: WorksiteFormData): void;
    setIsSelectionValidated(isSelectionValidated: boolean): void;
    setLocations(locations: WorksiteLocationFormData[]): void;
    setInterventions(interventions: WorksiteInterventionFormData[]): void;
    setAssociatedLocations(index: number, associated_locations: any): void;
    setHoveredItemId(hoveredItemId: string | undefined): void;
    setWorksiteActiveStep(activeStep: number): void;
  };
}

export const initialFormDataState = {
  worksite: {
    name: "",
    description: "",
    scheduled_start_date: "",
    scheduled_end_date: "",
  },
  isSelectionValidated: false,
  locations: [],
  interventions: [
    {
      is_current: true,
      intervention_type: { id: "", slug: "", location_types: [] },
      intervention_partner: "",
      associated_locations: [],
      cost: null,
    },
  ],
  hoveredItemId: undefined,
  activeStep: 1,
};

export const stateCreator: StateCreator<IWorksiteFormState> = (set) => ({
  ...initialFormDataState,
  actions: {
    setWorksite: (worksite: WorksiteFormData) => {
      set((state) => ({
        ...state,
        worksite,
      }));
    },
    setIsSelectionValidated: (isSelectionValidated: boolean) => {
      set((state) => ({
        ...state,
        isSelectionValidated,
      }));
    },
    setWorksiteActiveStep: (activeStep: number) => {
      set((state) => ({
        ...state,
        activeStep,
      }));
    },
    setLocations: (locations: WorksiteLocationFormData[]) => {
      set((state) => ({
        ...state,
        locations,
      }));
    },
    setInterventions: (interventions: WorksiteInterventionFormData[]) => {
      set((state) => ({
        ...state,
        interventions,
      }));
    },
    setHoveredItemId: (hoveredItemId: string | undefined) => {
      set((state) => ({
        ...state,
        hoveredItemId,
      }));
    },
    setAssociatedLocations: (index: number, locations: any[]) => {
      set((state) => {
        const updatedInterventions = [...state.interventions];
        if (updatedInterventions[index]) {
          updatedInterventions[index] = {
            ...updatedInterventions[index],
            associated_locations: locations,
          };
        }
        return {
          ...state,
          interventions: updatedInterventions,
        };
      });
    },
  },
});

export const useWorksiteFormStore = create<IWorksiteFormState>()(
  persist(stateCreator, {
    name: "worksiteFormStorage",
    partialize: (state) =>
      Object.fromEntries(
        Object.entries(state).filter(([key]) => !["actions"].includes(key))
      ),
  })
);

// getters hooks
export const useWorksiteData = () =>
  useWorksiteFormStore((state) => ({
    worksite: state.worksite,
    isSelectionValidated: state.isSelectionValidated,
    activeStep: state.activeStep,
    locations: state.locations,
    interventions: state.interventions,
    hoveredItemId: state.hoveredItemId,
  }));
export const useWorksiteFormData = () =>
  useWorksiteFormStore((state) => state.worksite);
export const useWorksiteIsSelectionValidated = () =>
  useWorksiteFormStore((state) => state.isSelectionValidated);
export const useWorksiteLocationFormData = () =>
  useWorksiteFormStore((state) => state.locations);
export const useWorksiteInterventionFormData = () =>
  useWorksiteFormStore((state) => state.interventions);
export const useWorksiteHoveredItemId = () =>
  useWorksiteFormStore((state) => state.hoveredItemId);
export const useWorksiteActiveStep = () =>
  useWorksiteFormStore((state) => state.activeStep);

// 🎉 one selector for all our actions
export const useWorksiteFormActions = () =>
  useWorksiteFormStore((state) => state.actions);
