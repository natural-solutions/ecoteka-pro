import create, { StateCreator } from "zustand";
import { persist } from "zustand/middleware";

export interface DashboardFilters {
  boundaries: string[];
  year: number;
}

export const defaultDashboardFilters: DashboardFilters = {
  boundaries: [],
  year: new Date().getFullYear(),
};

export interface DashboardState {
  dashboardFilters: DashboardFilters;
  actions: {
    setDashboardFilters(dashboardFilters: DashboardFilters): void;
  };
}

export const initialDashboardState = {
  dashboardFilters: defaultDashboardFilters,
};

export const stateCreator: StateCreator<DashboardState> = (set) => ({
  ...initialDashboardState,
  actions: {
    setDashboardFilters: (dashboardFilters: DashboardFilters) => {
      set((state) => ({
        ...state,
        dashboardFilters,
      }));
    },
  },
});

export const useDashboardStore = create<DashboardState>()(
  persist(stateCreator, {
    name: "dashboardStorage",
    partialize: (state) =>
      Object.fromEntries(
        Object.entries(state).filter(([key]) => !["actions"].includes(key))
      ),
  })
);

// getters

export const useDashboardFilters = () =>
  useDashboardStore((state) => state.dashboardFilters);

// 🎉 one selector for all our actions
export const useDashboardActions = () =>
  useDashboardStore((state) => state.actions);
