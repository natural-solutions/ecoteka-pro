import create, { StateCreator } from "zustand";
import { persist } from "zustand/middleware";
import type { GeoJsonLayerProps } from "@deck.gl/layers/typed";
import { CompositeLayerProps } from "@deck.gl/core/typed/types/layer-props";
import { MVTLayerProps } from "@deck.gl/geo-layers/typed";
import { FetchLocationStatusQuery, FetchPageMapQuery } from "generated/graphql";
import { TLocationStatus } from "@lib/constants";
import { Layer } from "deck.gl";
import { FeatureProperties } from "@pages/map";
import { EditableGeojsonLayerProps } from "@nebula.gl/layers/dist-types/layers/editable-geojson-layer";

export enum MapBackground {
  Map = "map",
  Satellite = "satellite",
  Ign = "ign",
  Aerial = "aerial",
  MapDark = "mapdark",
  Pci = "pci",
}

export type MapActionsBarActionType =
  | "filter"
  | "tree"
  | "addLocation"
  | "addWorksiteGroup"
  | "addPolygon";

export const defaultMapBackgrounds: MapBackground[] = [
  MapBackground.Map,
  MapBackground.Aerial,
  MapBackground.Ign,
  MapBackground.Pci,
  MapBackground.MapDark,
];

export type LocationType = "alive" | "stump" | "empty";

export enum LocationTypeColors {
  "alive" = "#06b6ae",
  "stump" = "#966133",
  "empty" = "#f3ede8",
}

export interface MapFilters {
  address: string;
  interventionType: string[];
  diagnosisStatus: string[];
  interventionScheduledDate: string;
  interventionRealizationDate: string;
  locationStatus?: TLocationStatus | string;
}

export const defaultCoordinate = {
  lat: null,
  lon: null,
};

export const defaultMapFilters: MapFilters = {
  address: "",
  interventionType: [],
  diagnosisStatus: [],
  interventionRealizationDate: "",
  interventionScheduledDate: "",
  locationStatus: undefined,
};

export type MapZoomDirection = "in" | "out";

export interface SelectionLayerProps extends CompositeLayerProps<any> {
  layerIds: any[];
  onSelect: (info: any) => any;
  selectionType: string | null;
}

export type TMapEditorLayerProps =
  | SelectionLayerProps
  | GeoJsonLayerProps
  | MVTLayerProps
  | Layer<any>;
export interface ViewState {
  longitude: number;
  latitude: number;
  zoom: number;
  pitch?: number;
  bearing?: number;
  transitionDuration?: number;
  transitionInterpolator?: any;
}

export interface MapEditorState {
  viewState: ViewState;
  firstLoad: boolean;
  forceReloadLayer: boolean;
  zoomMax: number;
  zoomMin: number;
  editionMode: boolean;
  mapStyle: string;
  mapActiveAction: MapActionsBarActionType | undefined;
  selectionLayerActive: boolean;
  mapActiveLocationId: string | undefined;
  mapLocations: FetchPageMapQuery["locations"] | undefined;
  mapFilteredLocations: FetchPageMapQuery["locations"] | undefined;
  mapSelectedLocations: FeatureProperties[] | undefined;
  mapStatuses: FetchLocationStatusQuery["status"] | undefined;
  mapLocationType: LocationType | undefined;
  mapBackground: MapBackground;
  mapPanelOpen: boolean;
  mapFilters: MapFilters;
  mapTempPoint: any;
  mapUserPosition: any;
  modalDataOpen: boolean;
  mapLayers: TMapEditorLayerProps[];
  hoveredSelectedLocationId: string | undefined;
  actions: {
    setEditionMode(editionMode?: boolean): void;
    setMapStyle(mapStyle: string): void;
    setViewState(viewState: ViewState): void;
    setFirstLoad(firstLoad: boolean): void;
    setForceReloadLayer(forceReloadLayer: boolean): void;
    setZoom(zoomValue: number): void;
    setMapEditorState(mapEditorState?: Partial<MapEditorState>): void;
    setMapActiveLocationId(mapActiveLocationId?: string): void;
    setMapActiveAction(
      mapActiveAction?: MapActionsBarActionType | undefined,
      mapLocationType?: LocationType
    ): void;
    renderSelectionLayer(value: boolean): void;
    setMapPanelOpen(mapPanelOpen: boolean): void;
    toggleMapPanel(): void;
    setMapBackground(mapBackground: MapBackground): void;
    setMapTempPoint(mapTempPoint: any | undefined): void;
    setMapUserPosition(mapUserPosition: any | undefined): void;
    setModalDataOpen(modalDataOpen: boolean): void;
    setMapLayers(layers: GeoJsonLayerProps[] | MVTLayerProps[]): void;
    addLayer<D>(
      layer:
        | GeoJsonLayerProps
        | SelectionLayerProps
        | EditableGeojsonLayerProps<D>
    ): void;
    removeLayer(layerId: string): void;
    setMapLocations(
      mapLocations: FetchPageMapQuery["locations"] | undefined
    ): void;
    setMapFilteredLocations(
      mapFilteredLocations: FetchPageMapQuery["locations"] | undefined
    ): void;
    setMapSelectedLocations(
      mapSelectedLocations: FeatureProperties[] | undefined
    ): void;
    removeFromMapSelectedLocations(locationId: string): void;
    fetchMapLocations: (fetchMapLocations: () => Promise<any>) => Promise<void>;
    fetchLocationStatuses: (
      fetchLocationStatuses: () => Promise<any>
    ) => Promise<void>;
    setHoveredSelectedLocationId(
      hoveredSelectedLocationId: string | undefined
    ): void;
  };
}

export const initialMapEditorState = {
  firstLoad: false,
  forceReloadLayer: false,
  viewState: {
    latitude: 43.29402423187586,
    longitude: 5.376938985791525,
    zoom: 14,
    bearing: 0,
    pitch: 0,
  },
  zoomMax: 20,
  zoomMin: 1,
  editionMode: false,
  mapStyle: `/mapStyles/ign_light.json`,
  mapLocations: undefined,
  mapFilteredLocations: undefined,
  mapSelectedLocations: undefined,
  mapStatuses: undefined,
  mapActiveAction: undefined,
  selectionLayerActive: false,
  mapLocationType: undefined,
  mapBackground: MapBackground.Map,
  mapPanelOpen: false,
  mapFilters: defaultMapFilters,
  mapActiveLocation: undefined,
  mapActiveLocationId: undefined,
  mapTempPoint: defaultCoordinate,
  mapUserPosition: defaultCoordinate,
  modalDataOpen: false,
  mapLayers: [],
  hoveredSelectedLocationId: undefined,
};

export const stateCreator: StateCreator<MapEditorState> = (set) => ({
  ...initialMapEditorState,
  actions: {
    setFirstLoad: (firstLoad: boolean) => {
      set((state) => ({
        ...state,
        firstLoad,
      }));
    },
    setForceReloadLayer: (forceReloadLayer: boolean) => {
      set((state) => ({
        ...state,
        forceReloadLayer,
      }));
    },
    setViewState: (viewState?: ViewState) => {
      set((state) => ({
        ...state,
        viewState,
      }));
    },
    setZoom: (zoom: number) => {
      set((state) => ({
        ...state,
        viewState: { ...state.viewState, zoom },
      }));
    },
    setEditionMode: (editionModeNewVal: boolean | undefined) => {
      set((state) => ({
        ...state,
        editionMode: editionModeNewVal,
        mapTempPoint:
          editionModeNewVal === false ? undefined : state.mapTempPoint,
      }));
    },
    setMapStyle: (mapStyle: string) => {
      set((state) => ({
        ...state,
        mapStyle,
      }));
    },
    setMapEditorState: (mapEditorState: Partial<MapEditorState>) => {
      set((state) => ({
        ...state,
        ...mapEditorState,
      }));
    },
    setMapActiveLocationId: (mapActiveLocationId: string) => {
      set((state) => ({
        ...state,
        mapActiveLocationId,
      }));
    },
    setMapActiveAction: (
      mapActiveAction?: MapActionsBarActionType | undefined,
      mapLocationType?: LocationType
    ) => {
      set((state) => ({
        ...state,
        mapActiveAction,
      }));
      if (mapActiveAction === "addLocation") {
        set((state) => ({
          ...state,
          mapLocationType,
        }));
      }
    },
    renderSelectionLayer: (value: boolean) => {
      set((state) => ({
        ...state,
        selectionLayerActive: value,
      }));
    },
    setMapPanelOpen: (mapPanelOpen: boolean) => {
      set((state) => ({
        ...state,
        mapPanelOpen,
      }));
    },
    toggleMapPanel: () => {
      set((state) => ({
        ...state,
        mapPanelOpen: !state.mapPanelOpen,
      }));
    },
    setMapBackground: (mapBackground: MapBackground) => {
      set((state) => ({
        ...state,
        mapBackground,
      }));
    },
    setMapTempPoint: (mapTempPoint: any | undefined) => {
      set((state) => ({
        ...state,
        mapTempPoint,
      }));
    },
    setMapUserPosition: (mapUserPosition: any | undefined) => {
      set((state) => ({
        ...state,
        mapUserPosition,
      }));
    },
    setModalDataOpen: (modalDataOpen: boolean) => {
      set((state) => ({
        ...state,
        modalDataOpen,
      }));
    },
    setMapLayers: (mapLayers: GeoJsonLayerProps[] | MVTLayerProps[]) => {
      set((state) => ({
        ...state,
        mapLayers,
      }));
    },
    addLayer: (
      layer: GeoJsonLayerProps | SelectionLayerProps | MVTLayerProps
    ) => {
      set((state) => ({
        ...state,
        mapLayers: [layer, ...state.mapLayers],
      }));
    },
    removeLayer: (layerId: string) => {
      set((state) => ({
        ...state,
        mapLayers: state.mapLayers?.filter((layer) => layer?.id !== layerId),
      }));
    },
    setMapLocations: (mapLocations) => {
      set((state) => ({
        ...state,
        mapLocations,
      }));
    },
    setMapFilteredLocations: (mapFilteredLocations) => {
      set((state) => ({
        ...state,
        mapFilteredLocations: mapFilteredLocations,
      }));
    },
    setMapSelectedLocations: (mapSelectedLocations) => {
      set((state) => ({
        ...state,
        mapSelectedLocations: mapSelectedLocations,
      }));
    },
    removeFromMapSelectedLocations: (locationId: string) => {
      // Define the function
      set((state) => ({
        ...state,
        mapSelectedLocations: state.mapSelectedLocations?.filter(
          (item) => item.location_id !== locationId
        ),
      }));
    },
    addMapLayers: (mapLayers: GeoJsonLayerProps[] | MVTLayerProps[]) => {
      set((state) => ({
        ...state,
        mapLayers: [...state.mapLayers, ...mapLayers],
      }));
    },
    fetchMapLocations: async (
      fetchMapLocations: () => Promise<any>
    ): Promise<void> => {
      try {
        const response = await fetchMapLocations();
        const data = response?.data.locations;

        set((state) => ({
          ...state,
          mapLocations: data,
        }));
      } catch (error) {
        // Handle error here
        console.error(error);
      }
    },
    fetchLocationStatuses: async (
      fetchLocationStatuses: () => Promise<any>
    ): Promise<void> => {
      try {
        const response = await fetchLocationStatuses();
        const data = response?.data.status;

        set((state) => ({
          ...state,
          mapStatuses: data,
        }));
      } catch (error) {
        // Handle error here
        console.error(error);
      }
    },
    setHoveredSelectedLocationId: (
      hoveredSelectedLocationId: string | undefined
    ) => {
      set((state) => ({
        ...state,
        hoveredSelectedLocationId,
      }));
    },
  },
});

// export const useMapEditorStore = create<MapEditorState>()(stateCreator);

export const useMapEditorStore = create<MapEditorState>()(
  persist(stateCreator, {
    name: "mapEditorStorage",
    partialize: (state) =>
      Object.fromEntries(
        Object.entries(state).filter(
          ([key]) =>
            ![
              "actions",
              "mapLayers",
              "mapLocations",
              "mapFilteredLocations",
            ].includes(key)
        )
      ),
  })
);

// getters

export const useViewState = () => useMapEditorStore((state) => state.viewState);

export const useFirstLoad = () => useMapEditorStore((state) => state.firstLoad);

export const useForceReloadLayer = () =>
  useMapEditorStore((state) => state.forceReloadLayer);

export const useMapEditor = () => useMapEditorStore((state) => state);

export const useEditionMode = () =>
  useMapEditorStore((state) => state.editionMode);

export const useMapStyle = () => useMapEditorStore((state) => state.mapStyle);

export const useMapActiveAction = () =>
  useMapEditorStore((state) => state.mapActiveAction);

export const useSelectionLayerActive = () =>
  useMapEditorStore((state) => state.selectionLayerActive);

export const useMapBackground = () =>
  useMapEditorStore((state) => state.mapBackground);

export const useMapTempPoint = () =>
  useMapEditorStore((state) => state.mapTempPoint);

export const useMapActiveLocationId = () =>
  useMapEditorStore((state) => state.mapActiveLocationId);

export const useMapUserPosition = () =>
  useMapEditorStore((state) => state.mapUserPosition);

export const useMapPanelOpen = () =>
  useMapEditorStore((state) => state.mapPanelOpen);

export const useMapLocationType = () =>
  useMapEditorStore((state) => state.mapLocationType);

export const useModalDataOpen = () =>
  useMapEditorStore((state) => state.modalDataOpen);

export const useMapZoom = () =>
  useMapEditorStore((state) => state.viewState.zoom);

export const useMapLayers = () => useMapEditorStore((state) => state.mapLayers);

export const useMapFilters = () =>
  useMapEditorStore((state) => state.mapFilters);

export const useMapLocations = () =>
  useMapEditorStore((state) => state.mapLocations);

export const useMapFilteredLocations = () =>
  useMapEditorStore((state) => state.mapFilteredLocations);

export const useMapSelectedLocations = () =>
  useMapEditorStore((state) => state.mapSelectedLocations);

export const useLocationStatuses = () =>
  useMapEditorStore((state) => state.mapStatuses);

export const useHoveredSelectedLocationId = () =>
  useMapEditorStore((state) => state.hoveredSelectedLocationId);
// 🎉 one selector for all our actions
export const useMapEditorActions = () =>
  useMapEditorStore((state) => state.actions);
