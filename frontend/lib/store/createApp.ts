import { GetState, SetState } from "zustand";
import { GlobalState } from "./useStore";
import { AlertColor } from "@mui/material";
import { ThemeSelectorMode } from "../../components/header/ThemeSelector";
/* import { getDefaultOrganization } from "@services/AnalyticsService";
import { OrganizationData } from "types/analytics/analytics.types"; */

import {
  FetchDefaultOrganizationDocument,
  Organization,
} from "../../generated/graphql";
import { apolloClient } from "../apolloClient";

export interface alertItem {
  message: string;
  severity: AlertColor;
  persist?: boolean;
  autoHideDuration?: number;
}

export interface AppState {
  appMode: ThemeSelectorMode;
  appContainer: boolean;
  appToggleMode(appMode?: ThemeSelectorMode): void;
  setAppContainer(appContainer: boolean): void;
  snackbar: {
    open: boolean;
    alerts: alertItem[];
  };
  snackbarToggle(open?: boolean): void;
  organization?: Organization;
  setup(): void;
  setAppState(app: Partial<AppState>): void;
}

const createApp = (set: SetState<GlobalState>, get: GetState<GlobalState>) => {
  const storedAppMode =
    typeof window !== "undefined" ? localStorage.getItem("appMode") : null;

  const initialAppMode = storedAppMode
    ? (storedAppMode as ThemeSelectorMode)
    : ThemeSelectorMode.light;
  return {
    appMode: initialAppMode,
    appContainer: true,
    organization: undefined,
    snackbar: {
      open: true,
      alerts: [],
    },

    appToggleMode: (appMode: ThemeSelectorMode) => {
      const currentMode = get().app.appMode;
      const newMode = appMode
        ? appMode
        : currentMode === ThemeSelectorMode.light
        ? ThemeSelectorMode.dark
        : ThemeSelectorMode.light;
      set({
        app: {
          ...get().app,
          appMode: newMode,
        },
      });
      localStorage.setItem("appMode", newMode);
    },
    setAppContainer: (appContainer: boolean) => {
      set(() => ({
        app: {
          ...get().app,
          appContainer,
        },
      }));
    },
    snackbarToggle: (open: boolean) => {
      const snackbar = get().app.snackbar;
      set({
        app: {
          ...get().app,
          snackbar: {
            ...snackbar,
            open: open ?? !snackbar.open,
          },
        },
      });
    },
    setup: async () => {
      if (!get().app.organization && apolloClient) {
        const { data } = await apolloClient.query({
          query: FetchDefaultOrganizationDocument,
        });

        set({
          app: {
            ...get().app,
            organization: data?.organization[0] as Organization,
          },
        });
      }
    },
    setAppState: (app: Partial<AppState>) => {
      set({
        app: {
          ...get().app,
          ...app,
        },
      });
    },
  };
};

export default createApp;
