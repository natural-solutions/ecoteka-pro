import {
  FilterData,
  FiltersWithMartin,
} from "@context/FiltersWithMartinContext";

export const renderValue = (
  key: string,
  value: string | string[] | boolean,
  filterData: FilterData,
  filtersWithMartin: FiltersWithMartin,
  t
): string | null | undefined | boolean => {
  if (Array.isArray(value)) {
    return renderArrayValue(key, value, filterData, t);
  } else {
    return renderSingleValue(key, value, filtersWithMartin);
  }
};

const renderArrayValue = (
  key: string,
  valueArray: string[],
  filterData: FilterData,
  t
): string => {
  switch (key) {
    case "tree_condition":
    case "intervention_status":
      return renderStatusArray(valueArray, key, t);
    case "location_status":
      return renderLocationStatusArray(valueArray, filterData, t);
    case "intervention_partner_id":
      return renderPartnerArray(valueArray, filterData);
    case "intervention_type_id":
      return renderTypeArray(valueArray, filterData, t);
    default:
      return valueArray.join(", ");
  }
};

const renderStatusArray = (valueArray: string[], key: string, t): string =>
  valueArray
    .map((val) => t(`components.Map.Filter.${key}_item.${val}`))
    .join(", ");

const renderLocationStatusArray = (
  valueArray: string[],
  filterData: FilterData,
  t
): string =>
  valueArray
    .map((val) => {
      const status = filterData?.location?.status.find(
        (status) => status.id === val
      );
      return status
        ? t(`components.Map.Filter.location_status_item.${status.status}`)
        : val;
    })
    .join(", ");

const renderPartnerArray = (
  valueArray: string[],
  filterData: FilterData
): string =>
  valueArray
    .map((val) => {
      const partner = filterData?.intervention?.partner.find(
        (e) => e.id === val
      );
      return partner ? partner.name : val;
    })
    .join(", ");

const renderTypeArray = (
  valueArray: string[],
  filterData: FilterData,
  t
): string =>
  valueArray
    .map((val) => {
      const type = filterData?.intervention?.type.find((e) => e.id === val);
      return type
        ? t(`components.Interventions.interventionsList.${type.slug}`)
        : val;
    })
    .join(", ");

const renderSingleValue = (
  key: string,
  value: string | null | undefined | boolean,
  filtersWithMartin: FiltersWithMartin
): string | null | undefined | boolean => {
  // if (typeof value === 'boolean') {
  //   return value ? "yes" : "";
  // }
  if (key.endsWith("_start")) {
    const endKey = key.replace("_start", "_end");
    const endDate = filtersWithMartin[endKey];
    return endDate ? `${value} > ${endDate}` : value;
  } else if (key.endsWith("_end")) {
    return null;
  } else {
    return value;
  }
};

export const renderBoundaryFilterValue = (
  key: string,
  value: boolean,
  t
): string => {
  if (key === "geographic_boundaries_active" && value) {
    return t(`components.Map.Filter.polygon.types.geographic`);
  } else if (key === "station_boundaries_active" && value) {
    return t(`components.Map.Filter.polygon.types.station`);
  }
  return "";
};

export const renderVegetatedAreaFilterValue = (
  key: string,
  value: boolean,
  t
): string => {
  if (key === "vegetated_areas_active" && value) {
    return t(`components.Map.Filter.vegetated_areas_active`);
  }
  return "";
};
