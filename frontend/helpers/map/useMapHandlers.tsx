import { useState, useRef, useCallback } from "react";
import debounce from "lodash/debounce";
import { Direction } from "@components/map/buttons/Navigation";
import { MapRef } from "react-map-gl";

const useMapHandlers = (initialViewStateConfig, setSelectedLocation) => {
  const [viewState, setViewState] = useState(initialViewStateConfig);
  const mapRef = useRef<MapRef>(null);

  const handleOnViewStateChange = useCallback((viewState) => {
    const nextViewState = viewState.viewState;
    setViewState({
      ...nextViewState,
      longitude: nextViewState.longitude,
      latitude: nextViewState.latitude,
      zoom: nextViewState.zoom,
    });
  }, []);

  const handleOnZoom = useCallback(
    (direction: Direction) => {
      setViewState((prevViewState) => ({
        ...prevViewState,
        zoom:
          direction === Direction.Out
            ? prevViewState.zoom - 1
            : prevViewState.zoom + 1,
      }));
      if (mapRef.current !== null) {
        mapRef.current.setZoom(viewState.zoom);
      }
    },
    [viewState.zoom]
  );

  const handleOnMapClick = useCallback(
    async (info) => {
      setSelectedLocation(info?.object?.properties);
    },
    [setSelectedLocation]
  );

  const debouncedHandleOnMapClick = useCallback(
    debounce(handleOnMapClick, 100),
    [handleOnMapClick]
  );

  return {
    viewState,
    setViewState,
    mapRef,
    handleOnViewStateChange,
    handleOnZoom,
    debouncedHandleOnMapClick,
    handleOnMapClick,
  };
};

export default useMapHandlers;
