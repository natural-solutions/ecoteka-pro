import { NextPage } from "next";
import { Stack } from "@mui/material";
/* import { FlyToInterpolator } from "deck.gl";
import { useEffect, useState, useCallback, useRef } from "react";
import { SelectionLayer } from "@nebula.gl/layers";
import _ from "lodash";
import MapBase, { InitialViewState } from "../components/map/Base";
import LocationsLayer from "../components/map/layers/Locations";
import TempLayer from "../components/map/layers/Temp";
import MapActions from "../components/map/MapActions";
import Panel from "../components/map/Panel";
import PanelFilters from "../components/panel/Filters";
import "maplibre-gl/dist/maplibre-gl.css";
import useStore from "../lib/store/useStore";
import { useSession } from "next-auth/react";
import {
  LocationType,
  MapBackground,
  MapActionsBarActionType,
  useViewState,
  initialMapEditorState,
  useEditionMode,
  useMapActiveAction,
  useMapEditorActions,
  useMapBackground,
  useMapTempPoint,
  useMapUserPosition,
  useMapPanelOpen,
  useMapLocationType,
  useMapLayers,
  useMapLocations,
  useMapStyle,
  useLocationStatuses,
  useFirstLoad,
  useMapFilteredLocations,
  useMapFilters,
  useMapSelectedLocations,
  useSelectionLayerActive,
  useMapActiveLocationId,
} from "../lib/store/pages/mapEditor";
import {
  CreateTreeMutationVariables,
  useCreateLocationMutation,
  useCreateTreeMutation,
  useFetchAllBoundariesLazyQuery,
  useFetchLocationStatusLazyQuery,
  useFetchOneLocationLazyQuery,
  useFetchPageMapLazyQuery,
  useInsertLocationBoundariesMutation,
} from "../generated/graphql";
import i18next from "../i18n";
import InitialModal from "../components/map/InitialModal";
import { useRouter } from "next/router";
import PanelLocation from "../components/panel/Location";
import MapNavigation, { Direction } from "../components/map/buttons/Navigation";
import WorksitePanelContent from "@ecosystems/MapEditor/Panels/Worksite/WorksitePanelContent";
import CardInfos from "@components/_pages/MapEditor/CardInfos/CardInfos";
import Toolbar from "@components/_blocks/map/Toolbar";
import { useDeviceSize } from "@lib/utils"; */

const MapEditor: NextPage = () => {
  /*   const { app } = useStore((store) => store);
  const { query, push } = useRouter();
  const session = useSession();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";
  const { isDesktop } = useDeviceSize();
  const router = useRouter();
  const previousMapBackground = useRef(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [cardPreview, setCardPreview] = useState<boolean>(false);
  const [openCardModal, setOpenCardModal] = useState<boolean>(false);
  const [infos, setInfos] = useState();

  const [fetchLocations, { refetch: refetchLocations }] =
    useFetchPageMapLazyQuery();
  const [fetchLocationsStatuses] = useFetchLocationStatusLazyQuery();
  const [fetchLocation, { data: activeLocationData }] =
    useFetchOneLocationLazyQuery();
  const [fetchBoundaries, { data: boundaries }] =
    useFetchAllBoundariesLazyQuery();
  const [insertlocationBoundaries] = useInsertLocationBoundariesMutation();
  const [createLocationMutation] = useCreateLocationMutation();
  const [createTreeMutation] = useCreateTreeMutation();

  // here use our getters from our MapEditorStore
  const firstLoad = useFirstLoad();
  const viewState = useViewState();
  const editionMode = useEditionMode();
  const mapStyle = useMapStyle();
  const mapActiveAction = useMapActiveAction();
  const mapBackground = useMapBackground();
  const mapTempPoint = useMapTempPoint();
  const mapUserPosition = useMapUserPosition();
  const mapPanelOpen = useMapPanelOpen();
  const mapLocationType = useMapLocationType();
  const mapActiveLocationId = useMapActiveLocationId();
  const mapLayers = useMapLayers();
  const isSelectionLayerActive = useSelectionLayerActive();
  const mapLocations = useMapLocations();
  const mapFilteredLocations = useMapFilteredLocations();
  const mapSelectedLocations = useMapSelectedLocations();
  const mapFilters = useMapFilters();
  const mapStatuses = useLocationStatuses();
  // selector for import all of our actions for the map
  const actions = useMapEditorActions();
  const { setMapSelectedLocations } = actions;

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleOnActionClick = (action: MapActionsBarActionType) => {
    actions.setMapActiveAction(action);
    if (mapActiveAction === action) {
      actions.setMapPanelOpen(!mapPanelOpen);
    } else {
      actions.setMapPanelOpen(true);
    }
  };

  const handleOnAddLocation = (locationType: LocationType) => {
    actions.setMapActiveLocationId(undefined);
    setOpenCardModal(false);
    setCardPreview(false);
    actions.setEditionMode(true);
    actions.setMapFilters({
      address: "",
      interventionType: [],
      diagnosisStatus: [],
      interventionScheduledDate: "",
      interventionRealizationDate: "",
      locationStatus: "",
    });
    actions.setMapFilteredLocations(undefined);
    actions.setMapSelectedLocations(undefined);
    actions.setMapActiveAction("addLocation", locationType);
    actions.setMapPanelOpen(false);
    actions.setMapBackground(MapBackground.Satellite);
  };

  const handleCancelProcess = () => {
    actions.setMapActiveAction(undefined);
    actions.renderSelectionLayer(false);
    actions.setMapFilteredLocations(undefined);
    actions.setMapSelectedLocations(undefined);
    actions.setMapPanelOpen(false);
  };

  const exitEditMode = () => {
    actions.setEditionMode(false);
    actions.setMapActiveAction(undefined);
    actions.setMapPanelOpen(false);
    actions.setMapBackground(
      previousMapBackground.current ?? MapBackground.Map
    );
    const newQuery = { ...query };
    delete newQuery.edit;
    push({ query: { ...newQuery } }, undefined, {
      shallow: true,
    });
  };

  const handleOnBaseMapChange = (map: any) => {
    previousMapBackground.current = map;
    actions.setMapBackground(map);
  };

  const handleOnGeolocate = (x: number, y: number) => {
    const newViewState = {
      longitude: x,
      latitude: y,
      zoom: viewState.zoom,
      pitch: 0,
      bearing: 0,
      transitionDuration: 3000,
      transitionInterpolator: new FlyToInterpolator(),
    };
    actions.setViewState(newViewState);
    actions.setMapUserPosition({ lon: x, lat: y });
  };

  const onSuccess = useCallback(
    (migrationType: string, element: any, newItem?: any) => {
      app.setAppState({
        snackbar: {
          ...app.snackbar,
          alerts: [
            {
              message:
                element === "location"
                  ? i18next.t(
                      `components.LocationForm.success.${migrationType}.${newItem?.status?.status}`
                    )
                  : i18next.t(
                      `components.InterventionForm.success.${migrationType}`
                    ),
              severity: "success",
            },
          ],
        },
      });
      if (element === "location") {
        fetchLocation({ variables: { _eq: newItem.id } });
        actions.setMapPanelOpen(false);
      }
    },
    [
      app.snackbar.alerts,
      fetchLocation,
      actions.fetchMapLocations,
      mapPanelOpen,
    ]
  );

  const createTree = async (
    data: CreateTreeMutationVariables,
    locationId: string
  ) => {
    createTreeMutation({
      variables: {
        ...data,
        location_id: locationId,
        data_entry_user_id: currentUserId,
      },
    });
  };

  const handleOnZoom = (direction: Direction) => {
    actions.setViewState({
      ...viewState,
      zoom:
        direction === Direction.Out ? viewState.zoom - 1 : viewState.zoom + 1,
    });
  };

  const handleCloseCardInfos = () => {
    setOpenCardModal(false);
    setCardPreview(false);
  };

  const handleCreateLocation = async (data: Location) => {
    const coordinates = [data.longitude, data.latitude];
    createLocationMutation({
      variables: {
        coords: {
          type: "Point",
          coordinates,
        },
        address: data?.address,
        description: data?.description,
        id_status: data?.status,
        organization_id: app.organization?.id,
        data_entry_user_id: currentUserId,
      },
      onCompleted: async (response) => {
        if (Object.values(data.boundaries).some((value) => value !== "")) {
          const hasBoundaries = Object.values(data.boundaries).some(
            (value) => value !== ""
          );
          if (hasBoundaries) {
            const boundaryIds = Object.values(data.boundaries).filter(
              (id) => id !== ""
            );

            Promise.all(
              boundaryIds.map(async (boundaryId) => {
                await insertlocationBoundaries({
                  variables: {
                    location_id: response?.insert_location_one?.id,
                    boundary_id: boundaryId,
                  },
                });
              })
            );
          }
        }
        if (response?.insert_location_one?.status?.status === "alive") {
          // @ts-ignore
          createTree(data?.tree, response?.insert_location_one?.id);
        }
        await actions.fetchMapLocations(refetchLocations);
        onSuccess("create", "location", response?.insert_location_one);
      },
    });
  };

  const handleOnViewStateChange = (viewState: any) => {
    const nextViewState: InitialViewState = viewState.viewState;
    actions.setViewState({
      ...nextViewState,
      longitude: nextViewState.longitude,
      latitude: nextViewState.latitude,
      zoom: nextViewState.zoom,
    });
  };

  const handleOnMapClick = async (info) => {
    if (editionMode && mapActiveAction === "addLocation") {
      const [x, y] = info.coordinate;
      actions.setMapTempPoint({ lat: y, lon: x });
      actions.setMapPanelOpen(true);
    }
    if (
      !editionMode &&
      info?.layer === null &&
      mapPanelOpen &&
      mapActiveAction !== "addWorksiteGroup"
    ) {
      actions.setMapPanelOpen(false);
    }
    if (
      !editionMode &&
      info?.layer?.id === "locations" &&
      info.object?.properties?.id
    ) {
      actions.setMapActiveLocationId(info?.object?.properties?.id);

      const locationId = info?.object?.properties?.id;
      const selectedLocation = mapLocations?.find(
        (location) => location.id === locationId
      );
      if (selectedLocation) {
        const infos = info.object?.properties;
        setOpenCardModal(true);
        setCardPreview(true);
        setInfos({ ...infos });
      }
    }
  };

  // functions for create different layers
  const createLocationsLayer = () => {
    if (mapLocations && mapLocations?.length > 0) {
      return LocationsLayer({
        selectedLocations: mapSelectedLocations,
        mapFilteredLocations: mapFilteredLocations
          ? {
              type: "FeatureCollection",
              features: mapFilteredLocations.map((location) => {
                return {
                  type: "Feature",
                  properties: location,
                  geometry: location.coords,
                };
              }),
            }
          : undefined,
        locations: {
          type: "FeatureCollection",
          features: mapLocations.map((location) => {
            return {
              type: "Feature",
              properties: location,
              geometry: location.coords,
            };
          }),
        },
        mapActiveLocation: editionMode && activeLocationData?.location[0]?.id,
        mapActiveLocationId: mapActiveLocationId!,
        mapBackground: mapBackground,
        appMode: app.appMode,
      });
    }

    return null;
  };

  const createTempPointLayer = () => {
    if (mapTempPoint) {
      return TempLayer({
        mapTempPoint: mapTempPoint,
        position: mapUserPosition,
        setMapTempPoint: actions.setMapTempPoint,
      });
    }
    return null;
  };

  const createSelectionLayer = () => {
    if (isSelectionLayerActive) {
      return new (SelectionLayer as any)({
        id: "selection",
        selectionType: "polygon",
        onSelect: ({ pickingInfos }) => {
          const featuresProperties = pickingInfos.map(
            (picked) => picked.object.properties
          );
          actions.setMapSelectedLocations(featuresProperties);
        },
        layerIds: ["locations"],
        getTentativeLineColor: () => [192, 192, 192, 255],
        lineWidthMinPixels: 0.2,
      });
    }
    return null;
  };

  const debouncedHandleOnMapClick = _.debounce(handleOnMapClick, 100);
  useEffect(() => {
    const fetchData = async () => {
      try {
        await actions.fetchMapLocations(fetchLocations);
        await actions.fetchLocationStatuses(fetchLocationsStatuses);
        actions.setFirstLoad(true);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);

  useEffect(() => {
    if (mapActiveAction === "addLocation") {
      fetchBoundaries();
    }
  }, [mapActiveAction]);

  useEffect(() => {
    actions.setMapStyle(`/mapStyles/${mapBackground}_${app.appMode}.json`);
  }, [app.appMode, mapBackground]);

  useEffect(() => {
    if (mapLocations && mapLocations?.length > 0 && !firstLoad) {
      const initialCoords =
        app.organization?.coords?.coordinates ||
        mapLocations[0]?.coords?.coordinates;

      const viewState = {
        latitude: initialCoords[1],
        longitude: initialCoords[0],
        zoom: 14,
        bearing: 0,
        pitch: 0,
      };
      actions.setViewState(viewState);
    }
    if (mapLocations?.length === 0) {
      setOpenModal(true);
      actions.setMapEditorState(initialMapEditorState);
    }
  }, [mapLocations]);

  useEffect(() => {
    if (mapActiveAction !== "addWorksiteGroup") {
      setMapSelectedLocations([]);
    }
  }, [mapActiveAction, setMapSelectedLocations]);

  useEffect(() => {
    const locationsLayer = createLocationsLayer();
    const tempPointLayer = createTempPointLayer();
    const selectionLayer = createSelectionLayer();
    // Combine the layers
    const updatedLayers = [
      locationsLayer,
      tempPointLayer,
      selectionLayer,
    ].filter((layer) => layer !== null);

    actions.setMapLayers(updatedLayers);
  }, [
    mapFilters,
    editionMode,
    actions.setMapLayers,
    mapTempPoint,
    mapUserPosition,
    mapLocations,
    mapActiveLocationId,
    mapFilteredLocations,
    mapSelectedLocations,
    activeLocationData,
    actions.setMapActiveAction,
    mapPanelOpen,
    mapActiveAction,
    fetchLocation,
    mapBackground,
    isSelectionLayerActive,
  ]); */

  return (
    <Stack sx={{ height: "100%" }} data-cy="map-stack">
      {/*   <MapBase
        handleOnViewStateChange={handleOnViewStateChange}
        initialViewState={viewState}
        mapStyle={mapStyle}
        layers={mapLayers}
        editionMode={editionMode}
        onClick={debouncedHandleOnMapClick}
      >
        <>
          <MapActions
            handleOnActionClick={handleOnActionClick}
            mapBackground={mapBackground}
            handleOnBaseMapChange={handleOnBaseMapChange}
            handleOnGeolocate={handleOnGeolocate}
            cardPreview={cardPreview}
            editionMode={editionMode}
            exitEditMode={exitEditMode}
            handleOnAddLocation={handleOnAddLocation}
            locationStatusList={mapStatuses!}
          />
          {isDesktop && <MapNavigation onZoom={handleOnZoom} />}
        </>
      </MapBase>
      {openModal && (
        <InitialModal
          open={openModal}
          handleClose={handleCloseModal}
          handleOnAddLocation={handleOnAddLocation}
        />
      )}
      {openCardModal && infos && (
        <CardInfos infos={infos} handleCloseCardInfos={handleCloseCardInfos} />
      )}
      <Panel
        mapLocationType={mapLocationType!}
        mapActiveAction={mapActiveAction!}
        mapPanelOpen={mapPanelOpen}
        handleDrawerClose={() => actions.setMapPanelOpen(false)}
        handleCancelProcess={handleCancelProcess}
        mapActiveLocation={activeLocationData?.location[0]}
        handleOnOpen={() => actions.setMapPanelOpen(true)}
        isDesktop={isDesktop}
      >
        <>
          {mapActiveAction === "filter" && <PanelFilters />}
          {mapActiveAction === "addLocation" && boundaries && (
            <PanelLocation
              // @ts-ignores
              activeLocation={activeLocationData?.location[0]}
              tempPoint={mapTempPoint}
              locationType={mapLocationType!}
              onCreateLocation={handleCreateLocation}
              mapActiveAction={mapActiveAction}
              locationStatusList={mapStatuses!}
              boundaries={boundaries.boundary}
            />
          )}
          {mapActiveAction === "addWorksiteGroup" && <WorksitePanelContent />}
        </>
      </Panel> */}
    </Stack>
  );
};

export default MapEditor;
