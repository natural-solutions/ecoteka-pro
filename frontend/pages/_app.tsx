import "../styles/globals.css";
import localFont from "next/font/local";
import FiltersWithMartinProvider from "@context/FiltersWithMartinContext";
import { useEffect, useMemo, createRef, RefObject } from "react";
import { useRouter } from "next/router";
import { I18nextProvider } from "react-i18next";
import { getDesignTokens } from "../theme/theme";
import i18n from "../i18n";
import LayoutBase from "../components/layout/Base";
import useStore from "../lib/store/useStore";
import type { AppContext, AppProps } from "next/app";
import createEmotionCache from "../lib/createEmotionCache";
import { SessionProvider } from "next-auth/react";
import { initializeApollo } from "../lib/apolloClient";
import { ApolloProvider } from "@apollo/client";
import { CacheProvider, EmotionCache } from "@emotion/react";
import { fr } from "date-fns/locale";
import {
  ThemeProvider,
  CssBaseline,
  createTheme,
  Grow,
  GlobalStyles,
} from "@mui/material";
import { SnackbarProvider } from "notistack";
import Head from "next/head";
import CloseIcon from "@mui/icons-material/Close";
import { Session } from "next-auth";
import App from "next/app";
import GridProvider from "@context/grid/grid.context";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";

const pageWithContainer = ["/"];
const clientSideEmotionCache = createEmotionCache();

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
  pageProps: {
    session: Session;
  };
}

const poppins = localFont({
  src: [
    {
      path: "../styles/typo/Poppins-Thin.ttf",
      weight: "200",
      style: "normal",
    },
    {
      path: "../styles/typo/Poppins-Light.ttf",
      weight: "300",
      style: "normal",
    },
    {
      path: "../styles/typo/Poppins-Regular.ttf",
      weight: "400",
      style: "normal",
    },
    {
      path: "../styles/typo/Poppins-Medium.ttf",
      weight: "500",
      style: "normal",
    },
    {
      path: "../styles/typo/Poppins-SemiBold.ttf",
      weight: "600",
      style: "normal",
    },
    {
      path: "../styles/typo/Poppins-Bold.ttf",
      weight: "700",
      style: "normal",
    },
    {
      path: "../styles/typo/Poppins-ExtraBold.ttf",
      weight: "900",
      style: "normal",
    },
  ],
});

function MyApp({
  Component,
  emotionCache = clientSideEmotionCache,
  pageProps,
}: MyAppProps) {
  const apolloClient = initializeApollo(pageProps.session);
  //globalState.getState().app.setup();

  const { app } = useStore((store) => store);
  const router = useRouter();

  useEffect(() => {
    i18n.changeLanguage(router.locale);
  }, [router.locale]);

  useEffect(() => {
    app.setup();
  }, [!app.organization]);

  useEffect(() => {
    app.setAppContainer(pageWithContainer.includes(router.route));
  }, [router.route, app.setAppContainer]);

  useEffect(() => {
    app.appToggleMode(app.appMode);
  }, [app.appMode]);

  const theme = useMemo(
    () => createTheme(getDesignTokens(app.appMode)),
    [app.appMode]
  );

  const notistackRef: RefObject<SnackbarProvider> = createRef();
  const onClickDismiss = (key: any) => () => {
    notistackRef?.current?.closeSnackbar(key);
  };

  const globalStyles = (
    <GlobalStyles
      styles={{
        "& .SnackbarItem-variantSuccess": {
          backgroundColor: "#47B9B2 !important",
        },
      }}
    />
  );

  return (
    // @ts-ignore
    <SessionProvider session={pageProps.session}>
      <ApolloProvider client={apolloClient}>
        <CacheProvider value={emotionCache}>
          <I18nextProvider i18n={i18n}>
            <GridProvider>
              <ThemeProvider theme={theme}>
                <FiltersWithMartinProvider>
                  {globalStyles}
                  {/*@ts-ignores */}
                  <SnackbarProvider
                    maxSnack={3}
                    ref={notistackRef}
                    action={(key) => (
                      <CloseIcon
                        sx={{ cursor: "pointer" }}
                        onClick={onClickDismiss(key)}
                      />
                    )}
                    anchorOrigin={{
                      horizontal: "right",
                      vertical: "top",
                    }}
                    autoHideDuration={6000}
                    TransitionComponent={Grow}
                  >
                    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                    <CssBaseline />
                    <Head>
                      <title>Ecoteka</title>
                    </Head>
                    <main className={poppins.className}>
                      <LocalizationProvider
                        dateAdapter={AdapterDateFns}
                        adapterLocale={fr}
                      >
                        <LayoutBase>
                          {/*@ts-ignores */}
                          <Component {...pageProps} />
                        </LayoutBase>
                      </LocalizationProvider>
                    </main>
                  </SnackbarProvider>
                </FiltersWithMartinProvider>
              </ThemeProvider>
            </GridProvider>
          </I18nextProvider>
        </CacheProvider>
      </ApolloProvider>
    </SessionProvider>
  );
}

MyApp.getInitialProps = async (appContext: AppContext) => {
  const appProps = await App.getInitialProps(appContext);

  return { ...appProps };
};

export default MyApp;
