import { useEffect, useMemo, useState } from "react";
import { Tree, Location, Vegetated_Area } from "../../generated/graphql";
import { useTranslation } from "react-i18next";
import { NextPage } from "next";
import { Alert, Container } from "@mui/material";
import { MRT_ColumnDef } from "material-react-table";
import CombinedGrid, { ColumnVisibility } from "@components/grid/CombinedGrid";
import { DetailsHeader } from "@components/_core/headers";
import InfoIcon from "@mui/icons-material/Info";
import getConfig from "next/config";
import Loading from "@components/layout/Loading";
import useStore from "@stores/useStore";
import { useSession } from "next-auth/react";
import Custom403 from "@pages/403";
import AuthCan from "@components/auth/Can";

const CombinedPage: NextPage = () => {
  const { t } = useTranslation();
  const { publicRuntimeConfig } = getConfig();
  const [combinedData, setCombinedData] = useState<
    (Tree | Location | Vegetated_Area)[]
  >([]);
  const [pagination, setPagination] = useState({
    pageIndex: 0,
    pageSize: 30,
  });
  const session = useSession();
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";
  const [totalCount, setTotalCount] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [columnVisibility, setColumnVisibility] = useState<ColumnVisibility>({
    serial_number: false,
    vernacular_name: false,
    plantation_date: false,
    variety: false,
    height: false,
    circumference: false,
    habit: false,
    is_tree_of_interest: false,
    watering: false,
    coords: false,
    foot_type: false,
    landscape_type: false,
    sidewalk_type: false,
    plantation_type: false,
    is_protected: false,
    urban_site: false,
    urban_constraint: false,
    inventory_source: false,
    note: false,
    creation_date: false,
    surface: false,
    afforestation_trees_data: false,
    shrubs_data: false,
    herbaceous_data: false,
    has_differentiated_mowing: false,
    is_accessible: false,
    residential_usage: false,
    inconvenience_risk: false,
    hedge_type: false,
    linear_meters: false,
    potential_area_state: false,
  });
  const { app } = useStore((store) => store);

  const columns = useMemo<
    MRT_ColumnDef<Tree | Location | Vegetated_Area | any>[]
  >(
    () => [
      {
        accessorFn: (row) => row.boundaries_geographic,
        header: t("components.LocationForm.properties.geographic"),
      },
      {
        accessorFn: (row) => row.boundaries_station,
        header: t("components.LocationForm.properties.station_boundary"),
      },
      {
        accessorFn: (row) => row.boundaries_administrative,
        header: t("components.LocationForm.properties.administrative"),
      },
      {
        accessorFn: (row) => {
          return row.address ? row.address : "";
        },
        header: t("components.LocationForm.properties.address"),
      },
      {
        accessorFn: (row) => {
          if (row.__typename === "location" || row.__typename === "tree") {
            return t(`common.emplacement`);
          }
          return t(`components.CombinedGrid.vegetated_area`);
        },
        header: t("common.type"),
        filterFn: "equals",
        filterSelectOptions: [
          { text: t("common.emplacement"), value: t("common.emplacement") },
          {
            text: t("components.CombinedGrid.vegetated_area"),
            value: t("components.CombinedGrid.vegetated_area"),
          },
        ],
        filterVariant: "select",
        size: 100,
      },
      {
        accessorFn: (row) => {
          if (row.__typename === "tree") return (row as Tree).scientific_name;
          return "";
        },
        header: t("components.Tree.properties.scientificName"),
      },
      {
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area")
            return (row as Vegetated_Area).surface;
          return "";
        },
        header: t("components.VegetatedAreaForm.properties.surface"),
        size: 130,
      },
      {
        accessorFn: (row) => {
          switch (row.__typename) {
            case "vegetated_area":
              const afforestationTreesCount =
                (row as Vegetated_Area).afforestation_trees_data?.reduce(
                  (sum, item) => sum + (item.number || 0),
                  0
                ) || 0;
              const locationsCount =
                (row as Vegetated_Area).vegetated_areas_locations || 0;
              return `${(row as Vegetated_Area).type === "afforestation" ? afforestationTreesCount + locationsCount : locationsCount} ${t("common.trees")}`;
            case "location":
              return t(`common.${(row as Location).location_status}`);
            case "tree":
              return t(`common.alive`);
            default:
              return "";
          }
        },
        header: t("components.CombinedGrid.occupation"),
        size: 170,
      },
      {
        accessorKey: "serial_number",
        accessorFn: (row) => {
          if (row.__typename === "tree") return (row as Tree).serial_number;
          return "";
        },
        header: t("components.Tree.properties.serialNumber"),
      },
      {
        accessorKey: "vernacular_name",
        accessorFn: (row) => {
          if (row.__typename === "tree") return (row as Tree).vernacular_name;
          return "";
        },
        header: t("components.Tree.properties.vernacularName"),
      },
      {
        accessorKey: "plantation_date",
        accessorFn: (row) => {
          if (row.__typename === "tree" && (row as Tree)?.plantation_date)
            return new Date((row as Tree)?.plantation_date ?? "");
          return "";
        },
        filterVariant: "date-range",
        filterFn: "dateRangeBetweenInclusive",
        size: 350,
        header: t("components.Tree.properties.plantationDate.exact"),
        Cell: ({ cell }) => {
          const dateValue = cell.getValue<Date>();
          return dateValue && dateValue.toLocaleDateString(); // Format date for display
        },
      },
      {
        accessorKey: "variety",
        accessorFn: (row) => {
          if (row.__typename === "tree") return (row as Tree).variety;
          return "";
        },
        header: t("components.Tree.properties.variety"),
      },
      {
        accessorKey: "height",
        accessorFn: (row) => {
          if (row.__typename === "tree") return (row as Tree).height;
          return "";
        },
        header: t("components.Tree.properties.height"),
      },
      {
        accessorKey: "circumference",
        accessorFn: (row) => {
          if (row.__typename === "tree") return (row as Tree).circumference;
          return "";
        },
        header: t("components.Tree.properties.circumference"),
      },
      {
        accessorKey: "habit",
        accessorFn: (row) => {
          if (row.__typename === "tree")
            return row.habit
              ? t(`components.Tree.properties.habit.values.${row.habit}`)
              : "";
        },
        header: t("components.Tree.properties.habit.label"),
      },
      {
        accessorKey: "is_tree_of_interest",
        accessorFn: (row) => {
          if (row.__typename === "tree")
            return (row as Tree).is_tree_of_interest
              ? t("common.yes")
              : t("common.no");
          return "";
        },
        header: t("components.Tree.properties.isTreeOfInterest"),
      },
      {
        accessorKey: "watering",
        accessorFn: (row) => {
          if (row.__typename === "tree")
            return (row as Tree).watering ? t("common.yes") : t("common.no");
          return "";
        },
        header: t("components.Tree.properties.watering"),
      },
      {
        accessorKey: "coords",
        accessorFn: (row) => {
          if (row.__typename === "location" || row.__typename === "tree") {
            const [longitude, latitude] = row.coords.coordinates;
            return `${latitude} ${longitude}`;
          }
          return "";
        },
        header: t("components.LocationForm.labels.coords"),
      },
      {
        accessorKey: "foot_type",
        accessorFn: (row) => {
          return row.foot_type
            ? t(`components.LocationForm.properties.foot_type.${row.foot_type}`)
            : "";
        },
        header: t("components.LocationForm.labels.foot_type"),
      },
      {
        accessorKey: "landscape_type",
        accessorFn: (row) => {
          return row.landscape_type
            ? t(
                `components.LocationForm.properties.landscape_type.${row.landscape_type}`
              )
            : "";
        },
        header: t("components.LocationForm.labels.landscape_type"),
      },
      {
        accessorKey: "sidewalk_type",
        accessorFn: (row) => {
          return row.sidewalk_type
            ? t(
                `components.LocationForm.properties.sidewalk_type.${row.sidewalk_type}`
              )
            : "";
        },
        header: t("components.LocationForm.labels.sidewalk_type"),
      },
      {
        accessorKey: "plantation_type",
        accessorFn: (row) => {
          return row.plantation_type
            ? t(
                `components.LocationForm.properties.plantation_type.${row.plantation_type}`
              )
            : "";
        },
        header: t("components.LocationForm.labels.plantation_type"),
      },
      {
        accessorKey: "is_protected",
        accessorFn: (row) => {
          return (row as Location).is_protected
            ? t("common.yes")
            : t("common.no");
        },
        header: t("components.LocationForm.labels.is_protected"),
      },
      {
        accessorKey: "urban_site",
        accessorFn: (row) => {
          return row.urban_site
            ? t(
                `components.LocationForm.properties.urban_site.${row.urban_site}`
              )
            : "";
        },
        header: t("components.LocationForm.labels.urban_site"),
      },
      {
        accessorKey: "urban_constraint",
        accessorFn: (row) => {
          return row.urban_constraint
            ? row.urban_constraint
                ?.map((option) =>
                  t(
                    `components.LocationForm.properties.urban_constraint.${option}`
                  )
                )
                .join(",")
            : "";
        },
        header: t("components.LocationForm.labels.urban_constraint"),
      },
      {
        accessorKey: "inventory_source",
        accessorFn: (row) => {
          return row.inventory_source
            ? row.inventory_source
                .map((s) =>
                  t(`components.LocationForm.properties.inventory_source.${s}`)
                )
                .join(",")
            : "";
        },
        header: t("components.LocationForm.labels.inventory_source"),
      },
      {
        accessorKey: "note",
        accessorFn: (row) => {
          return row.note ? row.note : "";
        },
        header: t("components.Tree.properties.note"),
      },
      {
        accessorKey: "creation_date",
        accessorFn: (row) => {
          return row.creation_date ? row.creation_date : "";
        },
        header: t("components.LocationForm.properties.creation_date"),
      },
      {
        accessorKey: "surface",
        accessorFn: (row) => {
          return row.surface ? row.surface : "";
        },
        header: t("components.VegetatedAreaForm.properties.surface"),
      },
      {
        accessorFn: (row) => {
          return row.type
            ? t(
                `components.VegetatedAreaForm.properties.typeLabels.${row.type}`
              )
            : "";
        },
        header: t("components.VegetatedAreaForm.properties.type"),
      },
      {
        accessorKey: "afforestation_trees_data",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area") {
            const afforestationData = (row as Vegetated_Area)
              .afforestation_trees_data;
            return afforestationData
              .filter((data) => data.number !== 0 || data.specie !== "")
              .map((data) => `${data.specie} (${data.number})`)
              .join(", ");
          }
          return "";
        },
        header: t(
          "components.VegetatedAreaForm.properties.vegetatedAreaComposition.afforestation_trees.title"
        ),
      },
      {
        accessorKey: "shrubs_data",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area") {
            const shrubsData = (row as Vegetated_Area).shrubs_data;
            return shrubsData
              .filter((data) => data.number !== 0 || data.specie !== "")
              .map((data) => `${data.specie} (${data.number})`)
              .join(", ");
          }
          return "";
        },
        header: t(
          "components.VegetatedAreaForm.properties.vegetatedAreaComposition.shrubs.title"
        ),
      },
      {
        accessorKey: "herbaceous_data",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area") {
            const herbaceousData = (row as Vegetated_Area).herbaceous_data;
            return herbaceousData
              .filter((data) => data.number !== 0 || data.specie !== "")
              .map((data) => `${data.specie} (${data.number})`)
              .join(", ");
          }
          return "";
        },
        header: t(
          "components.VegetatedAreaForm.properties.vegetatedAreaComposition.herbaceous.title"
        ),
      },
      {
        accessorKey: "vegetated_areas_locations",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area")
            return (row as Vegetated_Area).vegetated_areas_locations;
          return "";
        },
        header: t(
          "components.VegetatedAreaForm.properties.vegetatedAreaComposition.trees.title"
        ),
      },
      {
        accessorKey: "has_differentiated_mowing",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area")
            return (row as Vegetated_Area).has_differentiated_mowing
              ? t("common.yes")
              : t("common.no");
          return "";
        },
        header: t(
          "components.VegetatedAreaForm.properties.differentiated_mowing"
        ),
      },
      {
        accessorKey: "is_accessible",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area")
            return (row as Vegetated_Area).is_accessible
              ? t("common.yes")
              : t("common.no");
          return "";
        },
        header: t("components.VegetatedAreaForm.properties.is_accessible"),
      },
      {
        accessorKey: "residential_usage",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area")
            return row.residential_usage
              ? row.residential_usage
                  ?.map((option) =>
                    t(
                      `components.VegetatedAreaForm.properties.residential_usage_type.${option}`
                    )
                  )
                  .join(",")
              : "";
        },
        header: t(
          "components.VegetatedAreaForm.properties.residential_usage_type.label"
        ),
      },
      {
        accessorKey: "inconvenience_risk",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area")
            return (row as Vegetated_Area).inconvenience_risk;
          return "";
        },
        header: t("components.VegetatedAreaForm.properties.inconvenience_risk"),
      },
      {
        accessorKey: "hedge_type",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area")
            row.hedge_type
              ? t(
                  `components.VegetatedAreaForm.properties.hedge_type.${row.hedge_type}`
                )
              : "";
        },
        header: t("components.VegetatedAreaForm.properties.hedge_type_label"),
      },
      {
        accessorKey: "linear_meters",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area")
            return (row as Vegetated_Area).linear_meters;
          return "";
        },
        header: t("components.VegetatedAreaForm.properties.linear_meters"),
      },
      {
        accessorKey: "potential_area_state",
        accessorFn: (row) => {
          if (row.__typename === "vegetated_area")
            row.potential_area_state
              ? t(
                  `components.VegetatedAreaForm.properties.potential_area_state.${row.potential_area_state}`
                )
              : "";
        },
        header: t(
          "components.VegetatedAreaForm.properties.potential_area_state_label"
        ),
      },
    ],
    []
  );

  const fetchData = async (pageIndex: number, pageSize: number) => {
    setIsLoading(true);
    try {
      const response = await fetch(
        `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/grids/get_combined_grid_data?page=${
          pageIndex + 1
        }&size=${pageSize}`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${currentUserToken}`,
          },
        }
      );
      const data = await response.json();
      setCombinedData([
        ...data.locations,
        ...data.trees,
        ...data.vegetated_areas,
      ]);
      setTotalCount(data.totalCount);
    } catch (error) {
      console.error(error);
    }
    setIsLoading(false);
  };

  const exportData = async () => {
    try {
      // Show snackbar message when export is initiated
      app.setAppState({
        snackbar: {
          ...app.snackbar,
          alerts: [
            {
              message: t(`components.Import.Exporting.loading`),
              severity: "info",
            },
          ],
        },
      });

      // Trigger the export task
      const response = await fetch(
        `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/grids/export_all_data_excel`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${currentUserToken}`,
          },
        }
      );

      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }

      // Get task_id from the response
      const data = await response.json();
      const taskId = data.task_id;

      // Poll the task status (you can adjust the polling interval as needed)
      const checkTaskStatus = async () => {
        const statusResponse = await fetch(
          `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/api/rest/grids/download_excel/${taskId}`,
          {
            method: "GET",
            headers: {
              Authorization: `Bearer ${currentUserToken}`,
            },
          }
        );

        // Check the HTTP status code for the response
        if (statusResponse.status === 202) {
          // Task is still in progress
          setTimeout(checkTaskStatus, 2000); // Poll again after 2 seconds
        } else if (statusResponse.status === 500) {
          // Handle task failure (internal server error)
          const statusData = await statusResponse.json();
          alert("Error exporting data: " + statusData.detail);
        } else if (statusResponse.status === 200) {
          // Task completed, proceed to download the file
          const blob = await statusResponse.blob();
          const url = window.URL.createObjectURL(blob);
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", "export.xlsx");
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
          window.URL.revokeObjectURL(url);

          // Show success snackbar message
          app.setAppState({
            snackbar: {
              ...app.snackbar,
              alerts: [
                {
                  message: t(`components.Import.Exporting.success`),
                  severity: "success",
                },
              ],
            },
          });
        } else {
          // Handle unexpected status codes
          const statusData = await statusResponse.json();
          alert("Unexpected status code: " + statusData.status);
        }
      };

      // Start checking the task status
      checkTaskStatus();
    } catch (error) {
      console.error("Error exporting data:", error);
    }
  };

  useEffect(() => {
    fetchData(pagination?.pageIndex, pagination?.pageSize);
  }, [pagination]);

  if (!combinedData.length) {
    return <Loading />;
  }

  return (
    <>
      <AuthCan role={["reader"]}>
        <Custom403 />
      </AuthCan>
      <AuthCan role={["admin", "editor"]}>
        <Container maxWidth="xl" sx={{ mt: 1 }}>
          <DetailsHeader
            title={t("components.Template.menuItems.plantHeritage.gridTitle")}
          />
          <Alert severity="info" icon={<InfoIcon />}>
            Vous avez pour le moment accès à la liste des emplacements et des
            surfaces de pleine terre. Nous travaillons à rendre cette base de
            données plus complète prochainement.
          </Alert>
          {combinedData.length > 0 && (
            <CombinedGrid
              data={combinedData}
              columns={columns}
              pagination={pagination}
              setPagination={setPagination}
              totalCount={totalCount}
              isLoading={isLoading}
              exportData={exportData}
              columnVisibility={columnVisibility}
              setColumnVisibility={setColumnVisibility}
            />
          )}
        </Container>
      </AuthCan>
    </>
  );
};

export default CombinedPage;
