import { NextPage } from "next/types";
import Layout from "@components/layout/MapEditorLayout";
import Fill from "@core/Fill";
import { MapView, Controls, Basemaps } from "@ecosystems/MapEditor";
import { Button } from "@mui/material";
import { useMapEditorActions, useMapPanelOpen } from "@stores/pages/mapEditor";
const MapEditorWithLayout: NextPage = () => {
  const mapPanelOpen = useMapPanelOpen();
  const actions = useMapEditorActions();

  // useEffect((
  //   // Store hydratation
  // ) => {},[])

  return (
    <Layout
      View={<MapView />}
      Controls={<Controls />}
      Basemaps={<Basemaps />}
      drawerActive={mapPanelOpen}
      drawerWidth={400}
      closePanelCallback={() => actions.setMapPanelOpen(false)}
      Panel={<Fill color="teal" />}
      Actions={
        <Button
          variant="contained"
          color="secondary"
          onClick={() => actions.toggleMapPanel()}
        >
          Open panel
        </Button>
      }
    />
  );
};

export default MapEditorWithLayout;
