import { APIRoute } from "next-s3-upload";
import getConfig from "next/config";

const { publicRuntimeConfig, serverRuntimeConfig } = getConfig();

export default APIRoute.configure({
  endpoint: publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL,
  forcePathStyle: true,
  accessKeyId: serverRuntimeConfig.DEFAULT_STORAGE_ACCESS_KEY,
  secretAccessKey: serverRuntimeConfig.DEFAULT_STORAGE_ACCESS_KEY_SECRET,
  bucket: serverRuntimeConfig.DEFAULT_STORAGE_BUCKET_NAME,
  region: "us-east-1",
  async key(req, filename) {
    let path = req.body.path;

    return req.body.path.includes("imports") ? `${path}/${filename}` : path;
  },
});
