import NextAuth, { NextAuthOptions, Session } from "next-auth";
import { JWT } from "next-auth/jwt";
import KeycloakProvider from "next-auth/providers/keycloak";
import getConfig from "next/config";

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

type refreshTokenParams = {
  client_id: string;
  client_secret: string;
  grant_type: string;
  refresh_token: string;
};

async function refreshAccessToken(token: JWT) {
  try {
    const params: refreshTokenParams = {
      client_id: publicRuntimeConfig.KEYCLOAK_CLIENT_ID,
      client_secret: serverRuntimeConfig.KEYCLOAK_CLIENT_SECRET,
      grant_type: "refresh_token",
      refresh_token: token.refreshToken ?? "",
    };

    const url = `${publicRuntimeConfig.KEYCLOAK_ISSUER}/protocol/openid-connect/token`;
    const formBody = [];

    for (const property in params) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(
        params[property as keyof refreshTokenParams]
      );
      //@ts-ignores
      formBody.push(encodedKey + "=" + encodedValue);
    }

    const body = formBody.join("&");
    const response = await fetch(url, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "POST",
      body: body,
    });

    const refreshedTokens = await response.json();

    if (!response.ok) {
      throw refreshedTokens;
    }

    return {
      ...token,
      accessToken: refreshedTokens.access_token,
      accessTokenExpires: (refreshedTokens?.expires_in
        ? Date.now() + refreshedTokens?.expires_in * 1000
        : 0) as number,
      refreshToken: refreshedTokens.refresh_token ?? token.refreshToken,
    };
  } catch (e) {
    const errorToken: JWT = {
      ...token,
      error: "RefreshAccessTokenError",
    };

    return errorToken;
  }
}

export const authOptions: NextAuthOptions = {
  providers: [
    KeycloakProvider({
      clientId: publicRuntimeConfig.KEYCLOAK_CLIENT_ID,
      clientSecret: serverRuntimeConfig.KEYCLOAK_CLIENT_SECRET,
      issuer: publicRuntimeConfig.KEYCLOAK_ISSUER,
      authorization: {
        params: {
          scope: "openid email profile",
        },
      },
      checks: ["pkce", "state"],
      idToken: true,
      profile(profile) {
        return {
          id: profile.sub,
          ...profile,
        };
      },
    }),
  ],
  session: {
    strategy: "jwt",
  },
  pages: {
    signIn: undefined,
  },
  callbacks: {
    async jwt({ token, profile, user, account }) {
      if (account && user && profile) {
        const newToken: JWT = {
          user,
          accessToken: account.access_token,
          accessTokenExpires: account?.expires_at
            ? account?.expires_at * 1000
            : Date.now(),
          refreshToken: account.refresh_token,
          roles: profile ? profile["x-hasura-allowed-roles"] : [],
          idToken: account.id_token,
        };

        return newToken;
      }

      if (Date.now() < token.accessTokenExpires) {
        return token;
      }

      return await refreshAccessToken(token);
    },
    async session({ session, token }) {
      session.idToken = token.idToken as string;
      session.user = token.user as Session["user"];
      session.roles = token.roles ?? [];
      session.accessToken = token.accessToken;
      session.error = token.error;

      return session;
    },
  },
};

export default NextAuth(authOptions);
