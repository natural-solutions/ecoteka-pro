import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/react";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

export default async function logout(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await getSession({ req });
  const redirectUri = encodeURIComponent(process.env["NEXTAUTH_URL"]!);
  const path = `${publicRuntimeConfig.KEYCLOAK_ISSUER}/protocol/openid-connect/logout?post_logout_redirect_uri=${redirectUri}&id_token_hint=${session?.idToken}`;

  res.status(200).json({ path });
}
