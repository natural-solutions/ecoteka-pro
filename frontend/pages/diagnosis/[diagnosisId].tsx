import { Container } from "@mui/material";
import { NextPage } from "next";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useEffect } from "react";
import {
  Analysis_Tool,
  Pathogen,
  useDeleteDiagnosisAnalysisToolMutation,
  useDeleteDiagnosisMutation,
  useDeleteDiagnosisPathogenMutation,
  useFetchDiagnosisFormDataLazyQuery,
  useFetchOneDiagnosisLazyQuery,
  useFetchOneTreeLazyQuery,
  useInsertDiagnosisAnalysisToolMutation,
  useInsertDiagnosisPathogenMutation,
  useUpdateDiagnosisMutation,
} from "../../generated/graphql";
import { useTranslation } from "react-i18next";
import useStore from "../../lib/store/useStore";
import Loading from "../../components/layout/Loading";
import DiagnosisForm, {
  IDiagnosis,
} from "../../components/forms/diagnosis/DiagnosisForm";
import { useDeviceSize } from "@lib/utils";
import { DetailsHeader } from "@components/_core/headers";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import MobileButtonsCard from "@components/_core/buttons/MobileButtonsCard";
import AuthCan from "@components/auth/Can";
import Custom403 from "@pages/403";

const validationSchema = yup.object({
  diagnosis_date: yup.string().required(),
  tree_condition: yup.string().required(),
  crown_condition: yup.string().nullable(),
  trunk_condition: yup.string().nullable(),
  collar_condition: yup.string().nullable(),
  carpenters_condition: yup.string().nullable(),
  recommendation: yup.string().required(),
  diagnosis_type: yup
    .object({
      id: yup.string().required(),
    })
    .required(),
});

const DiagnosisIdPage: NextPage = () => {
  const router = useRouter();
  const { diagnosisId } = router.query;
  const { t } = useTranslation(["components", "common"]);
  const { app } = useStore((store) => store);
  const [fetchTreeData] = useFetchOneTreeLazyQuery();
  const [fetchData, { data: activeDiagnosis, loading }] =
    useFetchOneDiagnosisLazyQuery();
  const [updateDiagnosisMutation] = useUpdateDiagnosisMutation();
  const [deleteDiagnosisMutation] = useDeleteDiagnosisMutation();
  const [insertDiagnosisPathogen] = useInsertDiagnosisPathogenMutation();
  const [insertDiagnosisAnalysisTool] =
    useInsertDiagnosisAnalysisToolMutation();
  const [deleteDiagnosisPathogen] = useDeleteDiagnosisPathogenMutation();
  const [deleteDiagnosisAnalysisTool] =
    useDeleteDiagnosisAnalysisToolMutation();
  const [fetchDiagnosisFormData, { data: diagnosisFormData }] =
    useFetchDiagnosisFormDataLazyQuery();

  const session = useSession();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";
  const { isMobile } = useDeviceSize();

  const {
    handleSubmit,
    control,
    formState: { errors, dirtyFields },
    setValue,
    reset,
    watch,
  } = useForm<IDiagnosis>({
    resolver: yupResolver(validationSchema),
  });

  const onSuccess = async (action: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`components.DiagnosisForm.success.${action}`),
            severity: "success",
          },
        ],
      },
    });
    const treeId = activeDiagnosis?.diagnosis[0].tree_id;
    await fetchTreeData({ variables: { _eq: treeId } });
    action === "delete" && (await router.push(`/tree/${treeId}`));
  };

  const updateDiagnosis = async (data: IDiagnosis) => {
    const {
      diagnosis_type,
      pathogens,
      analysis_tools,
      ...formDataWithoutObjects
    } = data;

    updateDiagnosisMutation({
      variables: {
        //@ts-ignores
        id: diagnosisId,
        //@ts-ignores
        diagnosis_type_id: data?.diagnosis_type.id,
        ...formDataWithoutObjects,
        data_entry_user_id: currentUserId,
      },

      onCompleted: (response) => {
        const hasPathogens = data && data.pathogens ? true : false;
        const hasAnalysisTools = data.analysis_tools ? true : false;
        if (hasPathogens) {
          const pathogens = Object.values(data.pathogens).filter(
            (option: Pathogen) => option?.id !== ""
          );

          deleteDiagnosisPathogen({
            variables: { diagnosis_id: diagnosisId },
          });

          Promise.all(
            pathogens.map(async (pathogen: Pathogen) => {
              await insertDiagnosisPathogen({
                variables: {
                  diagnosis_id: diagnosisId,
                  pathogen_id: pathogen?.id,
                },
              });
            })
          );
        }
        if (hasAnalysisTools) {
          const analysisTools = Object.values(data.analysis_tools).filter(
            (option: Analysis_Tool) => option?.id !== ""
          );
          deleteDiagnosisAnalysisTool({
            variables: { diagnosis_id: diagnosisId },
          });
          Promise.all(
            analysisTools.map(async (analysisTool: Analysis_Tool) => {
              await insertDiagnosisAnalysisTool({
                variables: {
                  diagnosis_id: diagnosisId,
                  analysis_tool_id: analysisTool?.id,
                },
              });
            })
          );
        }
        onSuccess("update");
      },
    });
  };

  const deleteDiagnosis = async (id: string) => {
    deleteDiagnosisMutation({
      variables: {
        id: id,
      },
      onCompleted: () => {
        onSuccess("delete");
      },
    });
  };
  useEffect(() => {
    if (!activeDiagnosis) {
      fetchData({ variables: { _eq: diagnosisId } });
    }
    fetchDiagnosisFormData();
  }, [diagnosisId, activeDiagnosis]);

  if (loading) {
    return <Loading />;
  }

  return (
    <>
      <AuthCan role={["reader"]}>
        <Custom403 />
      </AuthCan>
      <AuthCan role={["admin", "editor"]}>
        <Container maxWidth="xl" sx={{ mt: 1 }}>
          {activeDiagnosis && diagnosisFormData && (
            <>
              {!isMobile && (
                <DetailsHeader
                  title={t(`components.PanelTree.diagnosis`)}
                  withEditMode
                  onSave={handleSubmit(updateDiagnosis)}
                  onDelete={deleteDiagnosis}
                  reset={reset}
                  dirtyFields={dirtyFields}
                  deleteMessage={t(`DiagnosisForm.message.delete`)}
                />
              )}
              <DiagnosisForm
                activeDiagnosis={activeDiagnosis?.diagnosis[0]}
                diagnosisFormData={diagnosisFormData}
                onUpdateDiagnosis={updateDiagnosis}
                onDeleteDiagnosis={deleteDiagnosis}
                setValue={setValue}
                watch={watch}
                control={control as any}
                errors={errors}
              />
              {isMobile && (
                <MobileButtonsCard
                  withEditMode
                  onSave={handleSubmit(updateDiagnosis)}
                  onDelete={deleteDiagnosis}
                  reset={reset}
                  dirtyFields={dirtyFields}
                  deleteMessage={t(`DiagnosisForm.message.delete`)}
                />
              )}
            </>
          )}
        </Container>
      </AuthCan>
    </>
  );
};

export default DiagnosisIdPage;
