import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";
import {
  Analysis_Tool,
  Diagnosis,
  Intervention,
  Pathogen,
  useCreateDiagnosisMutation,
  useDeleteDiagnosesMutation,
  useFetchAllDiagnosesLazyQuery,
  useFetchAllDiagnosesOfATreeLazyQuery,
  useFetchDiagnosisFormDataLazyQuery,
  useInsertDiagnosisAnalysisToolMutation,
  useInsertDiagnosisPathogenMutation,
} from "../../generated/graphql";
import { useTranslation } from "react-i18next";
import { NextPage } from "next";
import * as Yup from "yup";
import { Button, Container, DialogActions, Typography } from "@mui/material";
import { MRT_ColumnDef } from "material-react-table";
import { useSession } from "next-auth/react";
import useStore from "../../lib/store/useStore";
import { DetailsHeader } from "@components/_core/headers";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import ConfirmDialog from "@components/forms/dialog/ConfirmDialog";
import DiagnosisGrid from "@components/grid/DiagnosisGrid";
import DiagnosisFields from "@components/forms/diagnosis/DiagnosisFields";
import AuthCan from "@components/auth/Can";
import Custom403 from "@pages/403";

export const diagnosisSchema = Yup.object({
  diagnosis_date: Yup.string().required(),
  tree_condition: Yup.string().required(),
  crown_condition: Yup.string().nullable(),
  trunk_condition: Yup.string().nullable(),
  collar_condition: Yup.string().nullable(),
  recommendation: Yup.string().required(),
  tree: Yup.object({
    id: Yup.string().required(),
  }).required(),
  diagnosis_type: Yup.object({
    id: Yup.string().required(),
  }).required(),
});

const DiagnosisPage: NextPage = () => {
  const { t } = useTranslation(["components", "common"]);
  const router = useRouter();
  const { app } = useStore((store) => store);
  const [createDiagnosisMutation] = useCreateDiagnosisMutation();
  const [insertDiagnosisPathogen] = useInsertDiagnosisPathogenMutation();
  const [insertDiagnosisAnalysisTool] =
    useInsertDiagnosisAnalysisToolMutation();
  const [openConfirmModal, setOpenConfirmModal] = useState(false);
  const objectId = router.query.id;
  const object = router.query.object;
  const modal = router.query.diagnosisModal;
  const [allDiagnoses, setAllDiagnoses] = useState<any>([]);
  const [fetchDiagnosisFormData, { data: diagnosisFormData }] =
    useFetchDiagnosisFormDataLazyQuery();
  const [
    fetchAllDiagnoses,
    { data: dataAllDiagnoses, refetch: refetchDataAllDiagnoses },
  ] = useFetchAllDiagnosesLazyQuery();
  const [
    fetchAllDiagnosesOfATree,
    { refetch: refetchDataAllDiagnosesOfATree },
  ] = useFetchAllDiagnosesOfATreeLazyQuery();

  const [deleteDiagnosesMutation] = useDeleteDiagnosesMutation();

  const session = useSession();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";

  const {
    handleSubmit,
    setValue,
    control,
    watch,
    reset,
    formState: { errors },
  } = useForm<Intervention>({
    resolver: yupResolver(diagnosisSchema),
  });

  const onSuccess = async (action) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`DiagnosisForm.success.${action}`),
            severity: "success",
          },
        ],
      },
    });

    const result = await refetchDataAllDiagnoses();
    setAllDiagnoses(result?.data?.diagnosis);
    router.replace("/diagnosis?object=all");
  };

  const deleteDiagnoses = async (diagnosisIds: string[]) => {
    deleteDiagnosesMutation({
      variables: {
        _in: diagnosisIds,
      },
      onCompleted: async () => {
        onSuccess("delete_plural");
      },
    });
  };

  const createDiagnosis = async (data) => {
    const {
      diagnosis_type,
      pathogens,
      analysis_tools,
      tree,
      location,
      ...formDataWithoutObjects
    } = data;
    createDiagnosisMutation({
      variables: {
        tree_id: data.tree.id,
        diagnosis_type_id: data?.diagnosis_type.id,
        ...formDataWithoutObjects,
        data_entry_user_id: currentUserId,
      },
      onCompleted: async (response) => {
        const hasPathogens = data && data.pathogens ? true : false;
        const hasAnalysisTools = data.analysis_tools ? true : false;

        if (hasPathogens) {
          const pathogens = Object.values(data.pathogens).filter(
            (option: Pathogen) => option?.id !== ""
          );

          Promise.all(
            pathogens.map(async (pathogen: Pathogen) => {
              await insertDiagnosisPathogen({
                variables: {
                  diagnosis_id: response?.insert_diagnosis_one?.id,
                  pathogen_id: pathogen?.id,
                },
              });
            })
          );
        }
        if (hasAnalysisTools) {
          const analysisTools = Object.values(data.analysis_tools).filter(
            (option: Analysis_Tool) => option?.id !== ""
          );
          Promise.all(
            analysisTools.map(async (analysisTool: Analysis_Tool) => {
              await insertDiagnosisAnalysisTool({
                variables: {
                  diagnosis_id: response?.insert_diagnosis_one?.id,
                  analysis_tool_id: analysisTool?.id,
                },
              });
            })
          );
        }
        app.setAppState({
          snackbar: {
            ...app.snackbar,
            alerts: [
              {
                message: t(`components.DiagnosisForm.success.create`),
                severity: "success",
              },
            ],
          },
        });
        let result;
        switch (object) {
          case "tree":
            result = await refetchDataAllDiagnosesOfATree();
            break;
          case "all":
            result = await refetchDataAllDiagnoses();
            break;
          default:
            break;
        }
        setAllDiagnoses(result?.data?.diagnosis);
        response?.insert_diagnosis_one?.recommendation !== "none"
          ? router.replace(
              `/tree/${response?.insert_diagnosis_one?.tree_id}?interventionModal=open&type=${response?.insert_diagnosis_one?.recommendation}`
            )
          : router.replace("/diagnosis?object=all");
        setOpenConfirmModal(false);
      },
    });
  };

  const fetchDataDiagnoses = async () => {
    try {
      const result = await fetchAllDiagnoses();
      setAllDiagnoses(result?.data?.diagnosis);
    } catch (error) {
      console.log(error);
    }
  };

  const fetchDataDiagnosesOfATree = async () => {
    try {
      const result = await fetchAllDiagnosesOfATree({
        variables: { _eq: objectId },
      });
      setAllDiagnoses(result?.data?.diagnosis);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchDataDiagnoses();
    fetchDiagnosisFormData();
  }, []);

  useEffect(() => {
    if (object === "tree" && objectId) {
      fetchDataDiagnosesOfATree();
    } else if (object === "all") {
      fetchDataDiagnoses();
    }
  }, [object, objectId]);

  useEffect(() => {
    reset();
  }, [modal]);

  const columns = useMemo<MRT_ColumnDef<Diagnosis>[]>(
    () => [
      {
        accessorFn: (originalRow) =>
          Boolean(originalRow.diagnosis_date) &&
          new Date(originalRow.diagnosis_date),
        id: "diagnosis_date",
        header: t("Diagnosis.labels.diagnosis_date"),
        filterVariant: "date",
        filterFn: "dateEquals",
        Cell: ({ cell }) => {
          const dateValue = cell.getValue<Date>();
          return dateValue !== null && dateValue.toLocaleDateString(); // Format date for display
        },
      },
      {
        accessorFn: (originalRow) => originalRow.user_entity.username,
        id: "data_entry_username",
        header: t("Diagnosis.labels.dataEntryUsername"),
      },
      {
        accessorFn: (originalRow) =>
          t(`Diagnosis.conditions.${originalRow.tree_condition}`) as string,
        id: "tree_condition",
        header: t("Diagnosis.labels.tree_condition"),
      },
      {
        accessorFn: (originalRow) =>
          t(
            `PanelTree.diagnosisList.${originalRow.diagnosis_type.slug}`
          ) as string,
        id: "diagnosis_type",
        header: t("Diagnosis.labels.diagnosisType"),
      },
      {
        accessorFn: (originalRow) =>
          t(`Diagnosis.recommendation.${originalRow.recommendation}`) as string,
        header: t("Diagnosis.labels.recommendation"),
        id: "recommendation",
      },
      {
        accessorFn: (originalRow) =>
          originalRow?.tree.location.address || originalRow?.tree.serial_number,
        header: t("Diagnosis.labels.linkedTree"),
        id: "linked_tree",
      },
    ],
    []
  );

  return (
    <>
      <AuthCan role={["reader"]}>
        <Custom403 />
      </AuthCan>
      <AuthCan role={["admin", "editor"]}>
        <Container maxWidth="xl" sx={{ mt: 1 }}>
          <DetailsHeader
            title={t("Template.menuItems.diagnoses.title")}
            withAddMode
            type="diagnosis"
            addMessage={t("common.addDiagnosis")}
            onDeleteObjects={deleteDiagnoses}
          />
          {allDiagnoses?.length > 0 && (
            <DiagnosisGrid allDiagnoses={allDiagnoses} columns={columns} />
          )}

          <>
            <ModalComponent
              open={modal === "open"}
              handleClose={() => router.replace("/diagnosis?object=all")}
            >
              <>
                <Typography
                  variant="h6"
                  component="div"
                  sx={{
                    padding: "8px 22px",
                    textAlign: "center",
                  }}
                >
                  {t("common.addDiagnosis")}
                </Typography>
                {diagnosisFormData && (
                  <DiagnosisFields
                    customControl={control}
                    customSetValue={setValue}
                    customWatch={watch}
                    errors={errors}
                    diagnosisFormData={diagnosisFormData}
                  />
                )}

                <DialogActions
                  className="dialog-actions-dense"
                  sx={{ padding: "16px", justifyContent: "space-between" }}
                >
                  <Button
                    onClick={() => router.replace("/diagnosis?object=all")}
                  >
                    {t("common.buttons.cancel")}
                  </Button>
                  <Button
                    onClick={() => setOpenConfirmModal(true)}
                    variant="contained"
                  >
                    {t("common.buttons.confirm")}
                  </Button>
                </DialogActions>
              </>
            </ModalComponent>
            <ModalComponent
              open={openConfirmModal}
              handleClose={() => setOpenConfirmModal(false)}
            >
              <ConfirmDialog
                message={`${t("common.buttons.confirm")} ?`}
                onConfirm={handleSubmit(createDiagnosis)}
                onAbort={() => setOpenConfirmModal(false)}
              />
            </ModalComponent>
          </>
        </Container>
      </AuthCan>
    </>
  );
};

export default DiagnosisPage;
