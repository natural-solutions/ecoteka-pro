import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";
import {
  Intervention,
  useCreateInterventionMutation,
  useCreateTreeMutation,
  useDeleteInterventionsMutation,
  useFetchAllInterventionsOfALocationLazyQuery,
  useFetchAllInterventionsOfATreeLazyQuery,
  useFetchDataAllInterventionsLazyQuery,
  useFetchDataInterventionsGridLazyQuery,
  useFetchOneInterventionLazyQuery,
  useUpdateInterventionMutation,
  useUpdateLocationStatusWithLocationIdMutation,
  useUpdateLocationsStatusesWithLocationsIdsMutation,
  useUpdateTreeWithFellingDateMutation,
  useUpdateTreesWithFellingDateMutation,
  useValidateInterventionsMutation,
} from "../../generated/graphql";
import InterventionsGrid from "../../components/grid/InterventionsGrid";
import { useTranslation } from "react-i18next";
import { NextPage } from "next";
import * as Yup from "yup";
import {
  Box,
  Button,
  Container,
  DialogActions,
  SvgIcon,
  TextField,
  Typography,
} from "@mui/material";
import PlaceIcon from "@mui/icons-material/Place";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import WarningAmberIcon from "@mui/icons-material/WarningAmber";
import CheckCircleOutline from "@mui/icons-material/CheckCircleOutline";
import { MRT_ColumnDef } from "material-react-table";
import { formatSerialNumber, transformDateFormat } from "@lib/utils";
import { useSession } from "next-auth/react";
import useStore from "../../lib/store/useStore";
import { DetailsHeader } from "@components/_core/headers";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import InterventionFields from "@components/forms/intervention/InterventionFields";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import ConfirmDialog from "@components/forms/dialog/ConfirmDialog";
import TreeToolbarIcon from "@components/_core/icons/Tree/TreeToolbarIcon";
import AuthCan from "@components/auth/Can";
import {
  FreeSolo,
  IFreeSoloOption,
} from "@components/forms/autocomplete/FreeSolo";
import Custom403 from "@pages/403";

export const interventionSchema = Yup.object({
  realization_date: Yup.string().nullable(),
  scheduled_date: Yup.string().required(),
  location: Yup.object()
    .shape({
      address: Yup.string().nullable(),
      status: Yup.string(),
      treeId: Yup.string().nullable(),
      locationId: Yup.string().nullable(),
    })
    .test(
      "either-treeId-or-locationId",
      "Either treeId or locationId must be filled.",
      function (value) {
        const { treeId, locationId } = value || {};
        return !!treeId || !!locationId; // Return true if at least one is filled
      }
    )
    .required(),
  intervention_type: Yup.object({
    id: Yup.string().required(),
  }).required(),
});

const InterventionsPage: NextPage = () => {
  const { t } = useTranslation(["components", "common"]);
  const router = useRouter();
  const { app } = useStore((store) => store);
  const [openConfirmModal, setOpenConfirmModal] = useState(false);
  const [action, setAction] = useState<string>("");
  const objectId = router.query.id;
  const object = router.query.object;
  const modal = router.query.interventionModal;
  const activeId = router.query.activeId;
  const [allInterventions, setAllInterventions] = useState<any>([]);
  const [fetchOneIntervention, { data: activeIntervention }] =
    useFetchOneInterventionLazyQuery();
  const [fetchDataAllInterventions, { refetch: refetchDataAllInterventions }] =
    useFetchDataAllInterventionsLazyQuery();
  const [fetchAllInterventionsOfATree, { refetch: refetchInterventionsTree }] =
    useFetchAllInterventionsOfATreeLazyQuery();
  const [
    fetchAllInterventionsOfALocation,
    { refetch: refetchInterventionsLocation },
  ] = useFetchAllInterventionsOfALocationLazyQuery();
  const [fetchDataGrid, { data: dataGrid }] =
    useFetchDataInterventionsGridLazyQuery();
  const [updateLocationStatus] =
    useUpdateLocationStatusWithLocationIdMutation();
  const [updateTreeMutation] = useUpdateTreeWithFellingDateMutation();
  const [updateTreesMutation] = useUpdateTreesWithFellingDateMutation();
  const [validateInterventionMutation] = useValidateInterventionsMutation();
  const [deleteInterventionMutation] = useDeleteInterventionsMutation();
  const [updateLocationsStatuses] =
    useUpdateLocationsStatusesWithLocationsIdsMutation();
  const [createTreeMutation] = useCreateTreeMutation();
  const [createInterventionMutation] = useCreateInterventionMutation();
  const [updateInterventionMutation] = useUpdateInterventionMutation();

  const session = useSession();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";
  const userRole = session?.data?.roles;

  const stateColors = {
    [t("common.realized")]: {
      color: "success.main",
      icon: <CheckCircleOutline color="success" />,
    },
    [t("common.late")]: {
      color: "warning.main",
      icon: <WarningAmberIcon color="warning" />,
    },
    [t("common.toCome")]: {
      color: "#3AB4D0",
      icon: <AccessTimeIcon sx={{ color: "#3AB4D0" }} />,
    },
  };

  const now = new Date();
  const oneWeekLater = new Date();
  oneWeekLater.setDate(now.getDate() + 7);

  const {
    handleSubmit,
    setValue,
    reset,
    control,
    formState: { errors },
  } = useForm<Intervention>({
    resolver: yupResolver(interventionSchema),
    defaultValues: activeIntervention?.intervention[0] || {},
  });

  const queryType = router.query.type;
  const queryState = router.query.state;

  // Create initial filters object with the query type
  const initialFilters = useMemo(
    () => [
      {
        id: "familyInterventionType",
        value: t(`Dashboard.interventions.family.${queryType}`),
      },
      { id: "state", value: t(`common.${queryState}`) },
    ],
    [queryType, queryState]
  );
  const onSuccess = async (action) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`InterventionForm.success.${action}`),
            severity: "success",
          },
        ],
      },
    });
    let result;
    switch (object) {
      case "tree":
        result = await refetchInterventionsTree({ _eq: objectId });
        break;
      case "location":
        result = await refetchInterventionsLocation({ _eq: objectId });
        break;
      case "all":
        result = await refetchDataAllInterventions();
        break;
      default:
        break;
    }
    setAllInterventions(result?.data?.intervention);
    router.replace("/intervention?object=all");
    setOpenConfirmModal(false);
  };

  const validateIntervention = async (
    interventionIds: string[],
    selectedLocationsIds: string[],
    interventionDate: string,
    interventionPartner: string,
    validationNote: string
  ) => {
    validateInterventionMutation({
      variables: {
        _in: interventionIds,
        realization_date: interventionDate,
        intervention_partner_id: interventionPartner
          ? interventionPartner
          : null,
        data_entry_user_id: currentUserId,
        validation_note: validationNote,
      },
      onCompleted: async (response) => {
        const interventionType =
          response.update_intervention?.returning[0].intervention_type?.slug;

        switch (interventionType) {
          case "planting":
            await handleUpdateLocations(
              "alive",
              selectedLocationsIds,
              response.update_intervention?.returning[0].realization_date
            );
            break;
          case "grubbing":
            handleUpdateLocations("empty", selectedLocationsIds);
            break;
          case "felling":
            handleUpdateLocations("stump", selectedLocationsIds);
            break;
          default:
            onSuccess("update_plural");
        }
      },
    });
  };

  const deleteIntervention = async (interventionIds: string[]) => {
    deleteInterventionMutation({
      variables: {
        _in: interventionIds,
      },
      onCompleted: async () => {
        onSuccess("delete_plural");
      },
    });
  };

  const updateIntervention = async (data: Intervention) => {
    updateInterventionMutation({
      variables: {
        id: activeId,
        realization_date: Boolean(data?.realization_date)
          ? data?.realization_date
          : null,
        scheduled_date: data?.scheduled_date,
        intervention_partner_id: data?.intervention_partner?.id || null,
        intervention_type_id: data?.intervention_type.id,
        cost: data?.cost,
        note: data?.note,
        data_entry_user_id: currentUserId,
        validation_note: data?.validation_note || "",
        request_origin: data?.request_origin,
        is_parking_banned: data?.is_parking_banned,
      },
      onCompleted: async (response) => {
        const interventionType =
          response?.update_intervention_by_pk?.intervention_type.slug;
        const interventionIsDone = Boolean(data?.realization_date);
        // if location is empty, intervention is planting so update status location to alive and create tree with default serial_number XXX
        if (interventionIsDone) {
          switch (interventionType) {
            case "planting":
              await handleUpdateLocation(
                response?.update_intervention_by_pk?.tree?.location_id,
                null,
                "alive",
                data?.realization_date
              );
              break;
            case "felling":
              await handleUpdateLocation(
                response?.update_intervention_by_pk?.location_id,
                response?.update_intervention_by_pk?.tree?.id,
                "stump"
              );
              break;
            case "grubbing":
              await handleUpdateLocation(
                response?.update_intervention_by_pk?.location_id,
                null,
                "empty"
              );
              break;
            case "visual_diagnosis":
            case "detailed_diagnosis":
              router.replace(
                `/tree/${response?.update_intervention_by_pk?.tree?.id}?diagnosisModal=open&type=${interventionType}`
              );
              break;
            default:
              onSuccess("validate");
              break;
          }
        } else {
          onSuccess("update");
        }
      },
    });
  };

  const handleUpdateLocations = async (
    newStatus: string,
    selectedLocationsIds: string[],
    interventionDate?: string
  ) => {
    newStatus === "stump" &&
      (await updateTreesMutation({
        variables: {
          _in: selectedLocationsIds,
          felling_date: now.toISOString().split("T")[0],
        },
        onCompleted: async (response) => {
          await updateLocationsStatuses({
            variables: {
              _in: selectedLocationsIds,
              id_status: "d223091c-4931-47a0-8ebc-44c99a1663ed",
            },
            onCompleted: () => onSuccess("updateAndDeleteTrees"),
          });
        },
      }));

    ["empty", "alive"].includes(newStatus) &&
      (await updateLocationsStatuses({
        variables: {
          _in: selectedLocationsIds,
          id_status:
            newStatus === "alive"
              ? "bf6fcfe3-fbe9-4467-98be-a935efcf67a3"
              : "b2fbee37-c6a6-4431-9c70-f361ade578b2",
        },
        onCompleted: async () => {
          newStatus === "alive"
            ? selectedLocationsIds.map(async (id) => {
                await createTreeMutation({
                  variables: {
                    serial_number: formatSerialNumber(undefined),
                    location_id: id,
                    data_entry_user_id: currentUserId,
                    plantation_date: interventionDate,
                  },
                  onCompleted: async (response) => {
                    onSuccess("updateAndCreateTrees");
                  },
                });
              })
            : onSuccess("validateAndUpdateLocation");
        },
      }));
  };

  const handleUpdateLocation = async (
    locationId: string,
    treeId: string | null,
    newStatus: string,
    interventionDate?: string
  ) => {
    newStatus === "stump" &&
      (await updateTreeMutation({
        variables: {
          treeId: treeId,
          felling_date: now.toISOString().split("T")[0],
        },
        onCompleted: async (response) => {
          await updateLocationStatus({
            variables: {
              _eq: response.update_tree_by_pk?.location_id,
              id_status: "d223091c-4931-47a0-8ebc-44c99a1663ed",
            },
            onCompleted: () => {
              router.push(
                `/location/${response.update_tree_by_pk?.location_id}`
              );
            },
          });
        },
      }));
    ["empty", "alive"].includes(newStatus) &&
      (await updateLocationStatus({
        variables: {
          _eq: locationId,
          id_status:
            newStatus === "alive"
              ? "bf6fcfe3-fbe9-4467-98be-a935efcf67a3"
              : "b2fbee37-c6a6-4431-9c70-f361ade578b2",
        },
        onCompleted: async () => {
          newStatus === "empty" && router.push(`/location/${locationId}`);
          newStatus === "alive" &&
            (await createTreeMutation({
              variables: {
                serial_number: formatSerialNumber(undefined),
                location_id: locationId,
                data_entry_user_id: currentUserId,
                plantation_date: interventionDate,
              },
              onCompleted: async (response) => {
                await router.push(
                  `/tree/${response?.insert_tree_one?.id}?edit=active`
                );
              },
            }));
        },
      }));
  };

  const createIntervention = async (data) => {
    createInterventionMutation({
      variables: {
        tree_id: data.location?.treeId !== null ? data.location?.treeId : null,
        location_id:
          data.location?.treeId === null ? data.location?.locationId : null,
        realization_date: Boolean(data?.realization_date)
          ? data?.realization_date
          : null,
        scheduled_date: data?.scheduled_date,
        intervention_partner_id: data?.intervention_partner
          ? data?.intervention_partner.id
          : null,
        intervention_type_id: data?.intervention_type.id,
        cost: data?.cost || null,
        note: data?.note,
        data_entry_user_id: currentUserId,
        request_origin: data?.request_origin,
        is_parking_banned: data?.is_parking_banned,
      },
      onCompleted: async (response) => {
        const interventionType =
          response?.insert_intervention_one?.intervention_type.slug;
        const interventionIsDone = Boolean(data?.realization_date);
        // if location is empty, intervention is planting so update status location to alive and create tree with default serial_number XXX
        if (interventionIsDone) {
          switch (interventionType) {
            case "planting":
              await handleUpdateLocation(
                data.location.locationId,
                null,
                "alive",
                response?.insert_intervention_one?.realization_date
              );
              break;
            case "felling":
              await handleUpdateLocation(
                data.location.locationId,
                data.location.treeId,
                "stump"
              );
              break;
            case "grubbing":
              await handleUpdateLocation(
                data.location.locationId,
                null,
                "empty"
              );
              break;
            case "visual_diagnosis":
            case "detailed_diagnosis":
              router.replace(
                `/tree/${response?.insert_intervention_one?.tree?.id}?diagnosisModal=open&type=${interventionType}`
              );
              break;
            default:
              onSuccess("create");
              break;
          }
        } else {
          app.setAppState({
            snackbar: {
              ...app.snackbar,
              alerts: [
                {
                  message: t(`components.InterventionForm.success.create`),
                  severity: "success",
                },
              ],
            },
          });
          let result;
          result = await refetchDataAllInterventions();
          setAllInterventions(result?.data?.intervention);
          router.replace("/intervention?object=all");
          setOpenConfirmModal(false);
        }
      },
    });
  };

  const columns = useMemo<MRT_ColumnDef<Intervention>[]>(
    () => [
      {
        accessorFn: (originalRow) => {
          if (Boolean(originalRow.realization_date)) {
            return t("common.realized");
          } else if (originalRow.scheduled_date < transformDateFormat(now)) {
            return t("common.late");
          } else {
            return t("common.toCome");
          }
        },
        id: "state",
        header: t("Intervention.interventionsTable.state"),
        filterVariant: "select",
        filterSelectOptions: [
          t("common.realized"),
          t("common.late"),
          t("common.toCome"),
        ],
        Cell: ({ cell }) => {
          const status = cell.getValue() as string;

          const stateInfo = stateColors[status];

          if (!stateInfo) {
            // Handle the case where status doesn't match any keys in stateColors
            return <span>{status}</span>;
          }

          const { color, icon } = stateInfo;

          return (
            <Box
              display="flex"
              alignItems="center"
              justifyContent="center"
              sx={{
                borderColor: color,
                border: "solid 1px",
                borderRadius: "4px",
                padding: "2px",
                color: color,
                width: "90px",
              }}
            >
              {icon}
              <span>{status}</span>
            </Box>
          );
        },
      },
      {
        accessorFn: (originalRow) =>
          Boolean(originalRow.scheduled_date) &&
          new Date(originalRow.scheduled_date),
        id: "scheduled_date",
        header: t("Intervention.properties.scheduled"),
        filterVariant: "date-range",
        filterFn: "dateRangeBetweenInclusive",
        size: 350,
        Cell: ({ cell }) => {
          const dateValue = cell.getValue<Date>();

          return dateValue.toLocaleDateString(); // Format date for display
        },
      },
      {
        accessorFn: (originalRow) =>
          originalRow.tree
            ? originalRow.tree.location.address
            : originalRow.location?.address,
        id: "tree_location_address",
        header: t("common.emplacement"),
        Cell: ({ cell }) => {
          const value = cell.getValue() as string; // Assuming the value is a string
          return (
            <Box display="flex" alignItems="center">
              <PlaceIcon />
              <span>{value}</span>
            </Box>
          );
        },
      },
      {
        accessorFn: (originalRow) => {
          if (originalRow.tree?.scientific_name) {
            return originalRow.tree.scientific_name;
          } else if (originalRow.location?.location_status?.status) {
            const statusKey = originalRow.location.location_status.status;
            return t(`LocationForm.properties.status.${statusKey}`);
          } else {
            return t(`LocationForm.properties.status.alive`);
          }
        },

        id: "tree_location_status",
        header: "Genre/Espèce",
      },
      {
        accessorFn: (originalRow) => {
          return t(
            `Interventions.interventionsList.${originalRow.intervention_type.slug}`
          );
        },
        header: t("Template.menuItems.intervention.intervention"),
        id: "interventionType",
        filterVariant: "select",
        filterSelectOptions: dataGrid?.family_intervention_type.flatMap((f) =>
          f.intervention_types.map((type) =>
            t(`Interventions.interventionsList.${type.slug}`)
          )
        ),
        Cell: ({ cell }) => {
          const hasWorksiteInterventions = cell.row.original.worksite?.name;
          const type = cell.getValue() as string;

          return (
            <Box display="flex" alignItems="center">
              {hasWorksiteInterventions && (
                <SvgIcon>
                  <TreeToolbarIcon />
                </SvgIcon>
              )}
              {type}
            </Box>
          );
        },
      },
      {
        accessorFn: (originalRow) =>
          t(
            `Dashboard.interventions.family.${originalRow.intervention_type.family_intervention_type?.slug}`
          ),
        header: t("Dashboard.interventions.family.name"),
        id: "familyInterventionType",
        filterVariant: "select",
        filterSelectOptions: dataGrid?.family_intervention_type.map((i) =>
          t(`Dashboard.interventions.family.${i.slug}`)
        ),
      },
      {
        accessorFn: (originalRow) =>
          originalRow.intervention_partner?.name || "",
        header: t("Intervention.properties.interventionPartner"),
        id: "interventionPartner",
        filterVariant: "select",
        filterSelectOptions: dataGrid?.intervention_partner.map((i) => i.name),
      },
      {
        id: "cost",
        header: t("Intervention.cost"),
        filterFn: "between",
        accessorFn: (originalRow) => originalRow.cost,
        Cell: ({ cell }) => (
          <Box>
            {" "}
            {cell.getValue<number>()?.toLocaleString?.("fr-FR", {
              style: "currency",
              currency: "EUR",
              minimumFractionDigits: 0,
              maximumFractionDigits: 2,
            })}
          </Box>
        ),
      },
    ],
    [dataGrid]
  );

  const fetchInterventionsTree = async () => {
    try {
      const result = await fetchAllInterventionsOfATree({
        variables: { _eq: objectId },
      });
      setAllInterventions(result?.data?.intervention);
    } catch (error) {
      console.log(error);
    }
  };

  const fetchInterventionsLocation = async () => {
    try {
      const result = await fetchAllInterventionsOfALocation({
        variables: { _eq: objectId },
      });
      setAllInterventions(result?.data?.intervention);
    } catch (error) {
      console.log(error);
    }
  };

  const fetchDataInterventions = async () => {
    try {
      const result = await fetchDataAllInterventions();
      setAllInterventions(result?.data?.intervention);
    } catch (error) {
      console.log(error);
    }
  };

  const fetchOneInterventionData = async () => {
    try {
      await fetchOneIntervention({
        variables: {
          _eq: activeId,
        },
      });
    } catch (error) {
      console.log(error);
    }
  };

  const handleAbordValidationProcess = () => {
    setAction("");
    setValue("realization_date", null);
    setOpenConfirmModal(false);
  };

  useEffect(() => {
    if (object === "tree" && objectId) {
      fetchInterventionsTree();
    } else if (object === "location" && objectId) {
      fetchInterventionsLocation();
    } else if (object === "all") {
      fetchDataInterventions();
    }
    fetchDataGrid();
  }, [object, objectId, activeId]);

  useEffect(() => {
    if (activeId) {
      fetchOneInterventionData();
    }
  }, [activeId]);

  useEffect(() => {
    reset();
  }, [modal]);

  return (
    <>
      <AuthCan role={["reader"]}>
        <Custom403 />
      </AuthCan>
      <AuthCan role={["admin", "editor"]}>
        <Container maxWidth="xl" sx={{ mt: 1 }}>
          <DetailsHeader
            title={t("Template.menuItems.intervention.title")}
            withAddMode
            type="intervention"
            addMessage={t(
              "components.Template.menuItems.intervention.scheduleAnIntervention"
            )}
            onValidateIntervention={validateIntervention}
            interventionPartners={dataGrid?.intervention_partner}
            onDeleteObjects={deleteIntervention}
          />
          {allInterventions && (
            <InterventionsGrid
              allInterventions={allInterventions}
              columns={columns}
              initialFilters={initialFilters}
            />
          )}
          <>
            <ModalComponent
              open={modal === "open"}
              handleClose={() => router.replace("/intervention?object=all")}
            >
              <>
                <Typography
                  variant="h6"
                  component="div"
                  sx={{
                    padding: "8px 22px",
                    textAlign: "center",
                  }}
                >
                  {activeId
                    ? t(
                        "components.Template.menuItems.intervention.intervention"
                      )
                    : t(
                        "components.Template.menuItems.intervention.scheduleAnIntervention"
                      )}
                </Typography>
                {activeId && activeIntervention?.intervention[0] ? (
                  <InterventionFields
                    customControl={control}
                    customSetValue={setValue}
                    errors={errors}
                    interventionTypesGroupedByFamily={
                      dataGrid ? dataGrid?.family_intervention_type : []
                    }
                    interventionPartners={
                      dataGrid ? dataGrid?.intervention_partner : []
                    }
                    activeIntervention={activeIntervention.intervention[0]}
                    reset={reset}
                    userRole={userRole}
                  />
                ) : (
                  <InterventionFields
                    customControl={control}
                    customSetValue={setValue}
                    errors={errors}
                    interventionTypesGroupedByFamily={
                      dataGrid ? dataGrid?.family_intervention_type : []
                    }
                    interventionPartners={
                      dataGrid ? dataGrid?.intervention_partner : []
                    }
                    reset={reset}
                    userRole={userRole}
                  />
                )}

                <DialogActions
                  className="dialog-actions-dense"
                  sx={{ padding: "16px", justifyContent: "space-between" }}
                >
                  <Button
                    onClick={() => {
                      setAction("");
                      setOpenConfirmModal(true);
                    }}
                    variant="contained"
                  >
                    {activeId
                      ? t("common.buttons.save")
                      : t("common.buttons.schedule")}
                  </Button>

                  <Box display={"flex"} gap={1}>
                    <Button
                      variant="outlined"
                      color="error"
                      onClick={() => {
                        setAction("delete");
                        setOpenConfirmModal(true);
                      }}
                    >
                      {t("common.buttons.delete")}
                    </Button>
                    {activeId &&
                      !activeIntervention?.intervention[0].realization_date && (
                        <Button
                          onClick={() => {
                            setAction("validate");
                            setValue(
                              "realization_date",
                              now.toISOString().split("T")[0]
                            );
                            setOpenConfirmModal(true);
                          }}
                        >
                          {t(
                            "components.Template.menuItems.intervention.declareRealized"
                          )}
                        </Button>
                      )}
                  </Box>
                </DialogActions>
              </>
            </ModalComponent>

            <ModalComponent
              open={openConfirmModal}
              handleClose={() => {
                action === "validate"
                  ? handleAbordValidationProcess()
                  : setOpenConfirmModal(false);
              }}
            >
              {action === "validate" ? (
                <ConfirmDialog
                  title={t(`components.InterventionForm.message.validate`)}
                  onConfirm={handleSubmit(updateIntervention)}
                  onAbort={handleAbordValidationProcess}
                >
                  <>
                    {action === "validate" && (
                      <>
                        <Controller
                          name={`realization_date`}
                          control={control}
                          render={({ field: { onChange, value } }) => {
                            return (
                              <TextField
                                label={
                                  t(
                                    `components.Intervention.properties.realizationDate`
                                  ) as string
                                }
                                variant="filled"
                                type="date"
                                sx={{ width: "100%", m: 1 }}
                                InputLabelProps={{
                                  shrink: true,
                                }}
                                inputProps={{
                                  max: now.toISOString().split("T")[0],
                                }}
                                value={value}
                                onChange={(e) => onChange(e.target.value)}
                              />
                            );
                          }}
                        />
                        <Controller
                          name={`intervention_partner`}
                          control={control}
                          render={() => {
                            return (
                              <Box sx={{ width: "100%", m: 1 }}>
                                <FreeSolo
                                  defaultValue={""}
                                  control={control}
                                  setValue={setValue}
                                  name={
                                    "intervention_partner" as keyof Intervention
                                  }
                                  listOptions={
                                    dataGrid?.intervention_partner ??
                                    ([] as IFreeSoloOption[])
                                  }
                                  required={false}
                                  label={
                                    t(
                                      `components.Intervention.properties.interventionPartner`
                                    ) as string
                                  }
                                  object={"intervention"}
                                />
                              </Box>
                            );
                          }}
                        />

                        <Controller
                          name={`validation_note`}
                          control={control}
                          defaultValue={""}
                          render={({ field: { onChange, ref, value } }) => (
                            <TextField
                              value={value}
                              inputRef={ref}
                              fullWidth
                              sx={{
                                display: "flex",
                                m: 1,
                              }}
                              multiline={true}
                              label={
                                t(
                                  `components.Intervention.properties.note`
                                ) as string
                              }
                              variant="filled"
                              placeholder={t(
                                "components.Intervention.placeholder.validation_note"
                              )}
                              onChange={onChange}
                            />
                          )}
                        />
                      </>
                    )}
                  </>
                </ConfirmDialog>
              ) : (
                <ConfirmDialog
                  message={`${t("common.buttons.confirm")} ?`}
                  onConfirm={() => {
                    if (activeId && action !== "delete") {
                      handleSubmit(updateIntervention)();
                    } else if (activeId && action === "delete") {
                      deleteIntervention([activeId] as string[]);
                    } else {
                      handleSubmit(createIntervention)();
                    }
                  }}
                  onAbort={() => setOpenConfirmModal(false)}
                />
              )}
            </ModalComponent>
          </>
        </Container>
      </AuthCan>
    </>
  );
};

export default InterventionsPage;
