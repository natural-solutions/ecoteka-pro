import { NextPage } from "next";

import WorkInProgress from "@components/_core/WorkInProgress/WorkInProgress";

const WorkInProgressIndexPage: NextPage = () => {
  return <WorkInProgress type="page" />;
};

export default WorkInProgressIndexPage;
