import {
  Button,
  Container,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Typography,
  Box,
} from "@mui/material";
import { NextPage } from "next";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import * as yup from "yup";
import dynamic from "next/dynamic";

import {
  UpdateTreeMutationVariables,
  useCreateInterventionMutation,
  useDeleteLocationBoundariesMutation,
  useDeleteTreeMutation,
  useFetchAllBoundariesLazyQuery,
  useFetchInterventionTypesAndPartnersLazyQuery,
  useFetchOneLocationWithTreeIdLazyQuery,
  useFetchOneTreeLazyQuery,
  useFetchTreeEventsLazyQuery,
  useInsertLocationBoundariesMutation,
  useUpdateLocationStatusWithTreeIdMutation,
  useUpdateTreeAndLocationMutation,
  useGetUrbasenseSubjectMonitoringComboLazyQuery,
  useUpdateTreeWithFellingDateMutation,
  useFetchDiagnosisFormDataLazyQuery,
  Pathogen,
  Analysis_Tool,
  useCreateDiagnosisMutation,
  useInsertDiagnosisPathogenMutation,
  useInsertDiagnosisAnalysisToolMutation,
  useFetchLocationFormDataLazyQuery,
  Urban_Constraint,
  useDeleteLocationUrbanConstraintsMutation,
  useInsertLocationUrbanConstraintMutation,
  useFetchOneInterventionLazyQuery,
  Intervention,
  useUpdateInterventionMutation,
  useDeleteInterventionMutation,
} from "../../generated/graphql";
import useStore from "../../lib/store/useStore";

import DiagnosisEvents from "../../components/panel/DiagnosisEvents";
import TreeCard from "../../components/_blocks/layout/TreeCard/TreeCard";
import LocationCard from "../../components/_blocks/layout/LocationCard/LocationCard";
import { DetailsHeader } from "@components/_core/headers";
import { Controller, useForm } from "react-hook-form";

import { yupResolver } from "@hookform/resolvers/yup";
import HistoricCard from "@components/_blocks/layout/HistoricCard";
import { ModalComponent } from "@components/forms/modal/ModalComponent";
import InterventionFields from "@components/forms/intervention/InterventionFields";
import ConfirmDialog from "@components/forms/dialog/ConfirmDialog";
import Loading from "@components/layout/Loading";
import { useDeviceSize } from "@lib/utils";
import MobileButtonsCard from "@components/_core/buttons/MobileButtonsCard";
import DiagnosisFields from "@components/forms/diagnosis/DiagnosisFields";
import {
  FreeSolo,
  IFreeSoloOption,
} from "@components/forms/autocomplete/FreeSolo";
import { interventionSchema } from "@pages/intervention";
import { diagnosisSchema } from "@pages/diagnosis";
import AuthCan from "@components/auth/Can";
import Custom403 from "@pages/403";

const MonitoringCard = dynamic(
  () => import("@components/_blocks/layout/MonitoringCard/MonitoringCard"),
  {
    loading: () => <Loading />,
    ssr: false,
  }
);

const transformNumber = (value) => (isNaN(value) ? undefined : Number(value));
const combinedSchema = yup.object().shape({
  tree: yup.object().shape({
    id: yup.string().nullable(),
    picture: yup.string(),
    variety: yup.string().nullable(),
    scientific_name: yup.string().nullable(),
    vernacular_name: yup.string().nullable(),
    habit: yup.string().nullable(),
    serial_number: yup.string(),
    height: yup.number().transform(transformNumber).nullable(),
    circumference: yup.number().transform(transformNumber).nullable(),
    plantation_date: yup.string().nullable(),
    watering: yup.boolean().nullable(),
    is_tree_of_interest: yup.boolean().nullable(),
    estimated_date: yup.boolean().nullable(),
    note: yup.string().nullable(),
    taxon_id: yup.number().nullable(),
    urbasense_subject_id: yup.string().nullable(),
  }),
  location: yup.object().shape({
    latitude: yup.number().nullable(),
    longitude: yup.number().nullable(),
    address: yup.string().nullable(),
  }),
});

export interface TreeWithLocation {
  tree: {
    id: string | null;
    picture: string;
    variety: string | null;
    scientific_name: string | null;
    vernacular_name: string | null;
    habit: string | null;
    serial_number: string;
    height: string | null;
    circumference: number | null;
    plantation_date: string | null;
    watering: boolean | null;
    is_tree_of_interest: boolean | null;
    estimated_date: boolean | null;
    note: string | null;
    taxon_id: number | null;
  };
  location: {
    latitude: number;
    longitude: number;
    address: string;
  };
  realization_date: string | null;
  intervention_partner: string | null;
  validation_note: string | null;
}

const TreeIdPage: NextPage = () => {
  const router = useRouter();
  const { treeId } = router.query;
  const interventionModal = router.query.interventionModal;
  const diagnosisModal = router.query.diagnosisModal;
  const activeInterventionId = router.query.activeId;
  const { t } = useTranslation(["components", "common"]);
  const { isMobile } = useDeviceSize();
  const [openConfirmModal, setOpenConfirmModal] = useState(false);
  const [action, setAction] = useState<string>("");
  const [fetchData, { data: activeTree, refetch: refetchTree, loading }] =
    useFetchOneTreeLazyQuery();
  const [fetchDataLocation, { data: location }] =
    useFetchOneLocationWithTreeIdLazyQuery();
  const [fetchBoundaries, { data: boundaries }] =
    useFetchAllBoundariesLazyQuery();
  const [fetchTreeEvents, { data: treeEvents, refetch }] =
    useFetchTreeEventsLazyQuery();
  const [
    fetchInterventionTypesAndPartners,
    { data: interventionTypesAndPartners },
  ] = useFetchInterventionTypesAndPartnersLazyQuery();
  const [fetchDiagnosisFormData, { data: diagnosisFormData }] =
    useFetchDiagnosisFormDataLazyQuery();
  const [getUrbasenseSubjectMonitoringCombo, { data: monitoringData }] =
    useGetUrbasenseSubjectMonitoringComboLazyQuery();
  const [fetchLocationFormData, { data: locationFormData }] =
    useFetchLocationFormDataLazyQuery();
  const [fetchOneIntervention, { data: activeIntervention }] =
    useFetchOneInterventionLazyQuery();

  const [createDiagnosisMutation] = useCreateDiagnosisMutation();
  const [insertDiagnosisPathogen] = useInsertDiagnosisPathogenMutation();
  const [insertDiagnosisAnalysisTool] =
    useInsertDiagnosisAnalysisToolMutation();
  const [deleteLocationUrbanConstraints] =
    useDeleteLocationUrbanConstraintsMutation();
  const [insertLocationUrbanConstraint] =
    useInsertLocationUrbanConstraintMutation();
  const [deleteInterventionMutation] = useDeleteInterventionMutation();
  const [deleteLocationBoundaries] = useDeleteLocationBoundariesMutation();
  const [insertlocationBoundaries] = useInsertLocationBoundariesMutation();
  const [updateTreeAndLocationMutation] = useUpdateTreeAndLocationMutation();
  const [updateTreeWithFellingDateMutation] =
    useUpdateTreeWithFellingDateMutation();
  const [updateLocationStatus] = useUpdateLocationStatusWithTreeIdMutation();
  const [deleteTreeMutation] = useDeleteTreeMutation();
  const [createInterventionMutation] = useCreateInterventionMutation();
  const [updateInterventionMutation] = useUpdateInterventionMutation();
  const isIntervention = Boolean(interventionModal && !activeInterventionId);
  const isDiagnosis = Boolean(diagnosisModal);
  const isReadOnly = !router.query.hasOwnProperty("edit");
  const disableAddButtons = !!activeTree?.tree[0]?.felling_date;

  const createSchema = (isIntervention, isDiagnosis) => {
    let schema = combinedSchema;

    if (isIntervention) {
      schema = schema.concat(interventionSchema);
    }

    if (isDiagnosis) {
      schema = schema.concat(diagnosisSchema);
    }

    return schema;
  };

  const session = useSession();
  const { app } = useStore((store) => store);
  const now = new Date();

  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";

  const {
    handleSubmit,
    setValue,
    watch,
    reset,
    control,
    formState: { dirtyFields, errors },
  } = useForm<TreeWithLocation>({
    resolver: yupResolver(createSchema(isIntervention, isDiagnosis)),
  });

  const onSuccess = async (action: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`components.TreeForm.success.${action}`),
            severity: "success",
          },
        ],
      },
    });
  };

  const updateTree = async (data) => {
    const tree = data.tree as UpdateTreeMutationVariables;

    const coordinates = [data.location.longitude, data.location.latitude];
    const {
      urban_constraint,
      latitude,
      longitude,
      boundaries,
      urban_site_id,
      ...formDataLocationWithoutObjects
    } = data.location;
    const setLocationInput = {
      coords: {
        type: "Point",
        coordinates,
      },
      urban_site_id: data?.location?.urban_site_id || null,
      ...formDataLocationWithoutObjects,
    };

    updateTreeAndLocationMutation({
      variables: {
        ...tree,
        taxon_id:
          tree.taxon_id !== undefined
            ? tree.taxon_id
            : activeTree?.tree[0].taxon_id,
        treeId: treeId,
        data_entry_user_id: currentUserId,
        _setLocation: setLocationInput,
      },
      onCompleted: (response) => {
        const hasBoundaries = Object.values(data.location.boundaries).some(
          (value) => value !== ""
        );
        if (hasBoundaries) {
          deleteLocationBoundaries({
            variables: { location_id: activeTree?.tree[0].location.id },
          });

          const boundaryIds = Object.values(data.location.boundaries).filter(
            (id) => id !== ""
          );

          Promise.all(
            boundaryIds.map(async (boundaryId) => {
              await insertlocationBoundaries({
                variables: {
                  location_id: activeTree?.tree[0].location.id,
                  boundary_id: boundaryId,
                },
              });
            })
          );
        }
        const hasUrbanConstraints =
          data &&
          data?.location?.urban_constraint &&
          Object.keys(data?.location?.urban_constraint).length > 0;
        if (
          hasUrbanConstraints ||
          (data.location && !data.location.urban_constraint)
        ) {
          deleteLocationUrbanConstraints({
            variables: { location_id: activeTree?.tree[0].location.id },
          });
          const urbanConstraints = Object.values(
            data?.location?.urban_constraint || {}
          ).filter((option: Urban_Constraint) => option?.id !== "");

          Promise.all(
            urbanConstraints?.map(async (urbanConstraint: Urban_Constraint) => {
              await insertLocationUrbanConstraint({
                variables: {
                  location_id: activeTree?.tree[0].location.id,
                  urban_constraint_id: urbanConstraint.id,
                },
              });
            })
          );
        }
        onSuccess("update");
        refetch();
      },
    });
  };

  const deleteTree = async (id: string) => {
    const response = await updateLocationStatus({
      variables: {
        _eq: id,
        id_status: "b2fbee37-c6a6-4431-9c70-f361ade578b2",
      },
    });
    const locationId = response?.data?.update_location?.returning[0].id;
    await deleteTreeMutation({
      variables: {
        id: id,
      },
      onCompleted: () => {
        onSuccess("delete");
      },
    });
    await router.push(`/location/${locationId}`);
  };

  const handleUpdateLocation = async (newStatus: string) => {
    const now = new Date();
    newStatus === "stump" &&
      (await updateTreeWithFellingDateMutation({
        variables: {
          treeId: treeId,
          felling_date: now.toISOString().split("T")[0],
        },
        onCompleted: async (response) => {
          onSuccess("updateAndDeleteTree");
          await updateLocationStatus({
            variables: {
              _eq: response.update_tree_by_pk?.id,
              id_status: "d223091c-4931-47a0-8ebc-44c99a1663ed",
            },
            onCompleted: () => {
              router.push(
                `/location/${response.update_tree_by_pk?.location_id}`
              );
            },
          });
        },
      }));
  };

  const createIntervention = async (data) => {
    createInterventionMutation({
      variables: {
        tree_id: data.tree?.id ? data.tree?.id : data.location.treeId,
        location_id: null,
        realization_date: Boolean(data?.realization_date)
          ? data?.realization_date
          : null,
        scheduled_date: data?.scheduled_date,
        intervention_partner_id: data?.intervention_partner
          ? data?.intervention_partner.id
          : null,
        intervention_type_id: data?.intervention_type?.id,
        cost: data?.cost || null,
        note: data?.note,
        data_entry_user_id: currentUserId,
        request_origin: data?.request_origin,
        is_parking_banned: data?.is_parking_banned,
      },
      onCompleted: async (response) => {
        const interventionType =
          response?.insert_intervention_one?.intervention_type.slug;
        const interventionIsDone = Boolean(data?.realization_date);
        // if location is empty, intervention is planting so update status location to alive and create tree with default serial_number XXX
        if (interventionIsDone) {
          switch (interventionType) {
            case "felling":
              await handleUpdateLocation("stump");
              break;
            case "visual_diagnosis":
            case "detailed_diagnosis":
              router.replace(
                `/tree/${response?.insert_intervention_one?.tree?.id}?diagnosisModal=open&type=${interventionType}`
              );
              setOpenConfirmModal(false);
              break;
            default:
              onSuccess("createInterventionAndValidate");
              router.replace(`/tree/${treeId}`);
              setOpenConfirmModal(false);
              refetch();
              break;
          }
        } else {
          app.setAppState({
            snackbar: {
              ...app.snackbar,
              alerts: [
                {
                  message: t(`components.InterventionForm.success.create`),
                  severity: "success",
                },
              ],
            },
          });
          router.replace(`/tree/${treeId}`);
          setOpenConfirmModal(false);
          refetch();
        }
      },
    });
  };

  const updateIntervention = async (data) => {
    updateInterventionMutation({
      variables: {
        id: activeInterventionId,
        realization_date: Boolean(data?.realization_date)
          ? data?.realization_date
          : null,
        scheduled_date: data?.scheduled_date,
        intervention_partner_id: data?.intervention_partner?.id || null,
        intervention_type_id: data?.intervention_type.id,
        cost: data?.cost,
        note: data?.note,
        data_entry_user_id: currentUserId,
        request_origin: data?.request_origin,
        is_parking_banned: data?.is_parking_banned,
      },
      onCompleted: async (response) => {
        const interventionType =
          response?.update_intervention_by_pk?.intervention_type.slug;
        const interventionIsDone = Boolean(data?.realization_date);
        // if location is empty, intervention is planting so update status location to alive and create tree with default serial_number XXX
        if (interventionIsDone) {
          switch (interventionType) {
            case "felling":
              await handleUpdateLocation("stump");
              break;
            case "visual_diagnosis":
            case "detailed_diagnosis":
              router.replace(
                `/tree/${response?.update_intervention_by_pk?.tree?.id}?diagnosisModal=open&type=${interventionType}`
              );
              setOpenConfirmModal(false);
              break;
            default:
              onSuccess("updateInterventionAndValidate");
              router.replace(`/tree/${treeId}`);
              setOpenConfirmModal(false);
              refetch();
              break;
          }
        } else {
          onSuccess("updateIntervention");
          router.replace(`/tree/${treeId}`);
          setOpenConfirmModal(false);
          refetch();
        }
      },
    });
  };

  const deleteIntervention = async () => {
    deleteInterventionMutation({
      variables: {
        id: activeInterventionId,
      },
      onCompleted: async () => {
        onSuccess("deleteIntervention");
        router.replace(`/tree/${treeId}`);
        setOpenConfirmModal(false);
        refetch();
      },
    });
  };

  const createDiagnosis = async (data) => {
    const {
      diagnosis_type,
      pathogens,
      analysis_tools,
      tree,
      location,
      scheduled_date,
      intervention_partner,
      intervention_type,
      cost,
      validation_note,
      realization_date,
      id,
      __typename,
      user_entity,
      intervention_worksite_interventions,
      ...formDataWithoutObjects
    } = data;
    createDiagnosisMutation({
      variables: {
        tree_id: treeId,
        diagnosis_type_id: data?.diagnosis_type.id,
        ...formDataWithoutObjects,
        data_entry_user_id: currentUserId,
      },
      onCompleted: (response) => {
        const hasPathogens = data && data.pathogens ? true : false;
        const hasAnalysisTools = data.analysis_tools ? true : false;

        if (hasPathogens) {
          const pathogens = Object.values(data.pathogens).filter(
            (option: Pathogen) => option?.id !== ""
          );

          Promise.all(
            pathogens.map(async (pathogen: Pathogen) => {
              await insertDiagnosisPathogen({
                variables: {
                  diagnosis_id: response?.insert_diagnosis_one?.id,
                  pathogen_id: pathogen?.id,
                },
              });
            })
          );
        }
        if (hasAnalysisTools) {
          const analysisTools = Object.values(data.analysis_tools).filter(
            (option: Analysis_Tool) => option?.id !== ""
          );
          Promise.all(
            analysisTools.map(async (analysisTool: Analysis_Tool) => {
              await insertDiagnosisAnalysisTool({
                variables: {
                  diagnosis_id: response?.insert_diagnosis_one?.id,
                  analysis_tool_id: analysisTool?.id,
                },
              });
            })
          );
        }
        app.setAppState({
          snackbar: {
            ...app.snackbar,
            alerts: [
              {
                message: t(`components.DiagnosisForm.success.create`),
                severity: "success",
              },
            ],
          },
        });
        response?.insert_diagnosis_one?.recommendation !== "none"
          ? router.replace(
              `/tree/${activeTree?.tree[0].id}?interventionModal=open&type=${response?.insert_diagnosis_one?.recommendation}`
            )
          : router.push(`/tree/${treeId}`);
        refetchTree();
        setOpenConfirmModal(false);
      },
    });
  };

  const fetchOneInterventionData = async () => {
    try {
      await fetchOneIntervention({
        variables: {
          _eq: activeInterventionId,
        },
      });
    } catch (error) {
      console.log(error);
    }
  };

  const handleAbordValidationProcess = () => {
    setAction("");
    setValue("realization_date", null);
    setOpenConfirmModal(false);
  };

  useEffect(() => {
    if (!activeTree) {
      fetchData({ variables: { _eq: treeId } });
      fetchDataLocation({ variables: { _eq: treeId } });
      fetchTreeEvents({ variables: { _eq: treeId } });
      fetchDiagnosisFormData();
      fetchBoundaries();
      fetchLocationFormData();
      fetchInterventionTypesAndPartners();
    } else {
      if (activeTree.tree[0].urbasense_subject_id) {
        getUrbasenseSubjectMonitoringCombo({
          variables: {
            subjectId: Number(activeTree.tree[0].urbasense_subject_id),
          },
        });
      }
    }
  }, [treeId, activeTree]);

  useEffect(() => {
    if (activeInterventionId) {
      fetchOneInterventionData();
    }
  }, [activeInterventionId]);

  useEffect(() => {
    reset();
  }, [isIntervention, isDiagnosis, reset]);

  if (loading) {
    return <Loading />;
  }

  return (
    <>
      <AuthCan role={["reader"]}>
        <Custom403 />
      </AuthCan>
      <AuthCan role={["admin", "editor"]}>
        <Container maxWidth="xl" sx={isMobile ? styles.container : {}}>
          {!isMobile && (
            <DetailsHeader
              title={t("Sidebar.location")}
              locationStatus={"alive"}
              withEditMode={!activeTree?.tree[0]?.felling_date}
              onSave={handleSubmit(updateTree)}
              onDelete={deleteTree}
              reset={reset}
              dirtyFields={dirtyFields}
              deleteMessage={t(`TreeForm.message.delete`)}
            />
          )}

          {activeTree && location?.location[0] && (
            <>
              <Grid container spacing={2} mt={1} mb={1}>
                <Grid item md={6}>
                  <LocationCard
                    activeLocation={location?.location[0]}
                    customControl={control}
                    customSetValue={setValue}
                    errors={errors}
                    watch={watch}
                    boundaries={boundaries?.boundary}
                    locationFormData={locationFormData}
                    isReadOnly={isReadOnly}
                  />
                </Grid>
                <Grid item md={6}>
                  <TreeCard
                    activeTree={activeTree?.tree[0]}
                    customControl={control}
                    customSetValue={setValue}
                    errors={errors}
                    isReadOnly={isReadOnly}
                  />
                </Grid>
              </Grid>

              {!router.query.hasOwnProperty("edit") && (
                <>
                  <Grid container spacing={2} mt={1} mb={1}>
                    <Grid item md={6} sm={12} flexGrow={1}>
                      <DiagnosisEvents
                        activeTree={activeTree?.tree[0]}
                        disableAddButtons={disableAddButtons}
                      />
                    </Grid>
                    <Grid item md={6} sm={12} flexGrow={1}>
                      <HistoricCard
                        events={treeEvents?.intervention}
                        locationId={location?.location[0].id}
                        disableAddButtons={disableAddButtons}
                      />
                    </Grid>
                    {monitoringData &&
                      activeTree?.tree[0].urbasense_subject_id && (
                        <Grid item md={12}>
                          <MonitoringCard
                            meteo={
                              monitoringData.urbasenseGetSubjectMonitoringCombo
                            }
                          />
                        </Grid>
                      )}
                  </Grid>
                </>
              )}

              <ModalComponent
                open={interventionModal || diagnosisModal ? true : false}
                handleClose={() =>
                  router.replace(`/tree/${activeTree?.tree[0].id}`)
                }
              >
                <>
                  <DialogTitle>
                    <Typography
                      variant="h6"
                      component="div"
                      sx={{
                        padding: "0px 0px 16px 0px",
                        textAlign: "center",
                      }}
                    >
                      {!diagnosisModal &&
                        (activeInterventionId
                          ? t(
                              `components.Template.menuItems.intervention.intervention`
                            )
                          : t(
                              `components.Template.menuItems.intervention.scheduleAnIntervention`
                            ))}

                      {diagnosisModal &&
                        t("components.Diagnosis.labels.add_diagnosis")}
                    </Typography>
                  </DialogTitle>
                  <DialogContent>
                    {diagnosisModal && diagnosisFormData ? (
                      <DiagnosisFields
                        customControl={control}
                        customSetValue={setValue}
                        customWatch={watch}
                        errors={errors}
                        diagnosisFormData={diagnosisFormData}
                        activeTree={activeTree?.tree[0]}
                      />
                    ) : activeInterventionId &&
                      activeIntervention?.intervention[0] ? (
                      <InterventionFields
                        customControl={control}
                        customSetValue={setValue}
                        errors={errors}
                        interventionTypesGroupedByFamily={
                          interventionTypesAndPartners
                            ? interventionTypesAndPartners?.family_intervention_type
                            : []
                        }
                        interventionPartners={
                          interventionTypesAndPartners
                            ? interventionTypesAndPartners?.intervention_partner
                            : []
                        }
                        activeIntervention={activeIntervention?.intervention[0]}
                        reset={reset}
                      />
                    ) : (
                      <InterventionFields
                        customControl={control}
                        customSetValue={setValue}
                        errors={errors}
                        interventionTypesGroupedByFamily={
                          interventionTypesAndPartners
                            ? interventionTypesAndPartners?.family_intervention_type
                            : []
                        }
                        interventionPartners={
                          interventionTypesAndPartners
                            ? interventionTypesAndPartners?.intervention_partner
                            : []
                        }
                        location={location?.location[0]}
                        reset={reset}
                      />
                    )}
                  </DialogContent>
                  <DialogActions
                    className="dialog-actions-dense"
                    sx={{ padding: "16px", justifyContent: "space-between" }}
                  >
                    <Button
                      onClick={() => {
                        setAction("");
                        setOpenConfirmModal(true);
                      }}
                      variant="contained"
                    >
                      {t(
                        `common.buttons.${
                          diagnosisModal
                            ? "validate"
                            : activeInterventionId
                              ? "save"
                              : "schedule"
                        }`
                      )}
                    </Button>
                    {interventionModal && activeInterventionId && (
                      <>
                        <Button
                          variant="outlined"
                          color="error"
                          onClick={() => {
                            setAction("delete");
                            setOpenConfirmModal(true);
                          }}
                        >
                          {t("common.buttons.delete")}
                        </Button>
                        {!activeIntervention?.intervention[0]
                          .realization_date && (
                          <Button
                            onClick={() => {
                              setAction("validate");
                              setValue(
                                "realization_date",
                                now.toISOString().split("T")[0]
                              );
                              setOpenConfirmModal(true);
                            }}
                          >
                            {t(
                              "components.Template.menuItems.intervention.declareRealized"
                            )}
                          </Button>
                        )}
                      </>
                    )}
                  </DialogActions>
                </>
              </ModalComponent>
              <ModalComponent
                open={openConfirmModal}
                handleClose={() => {
                  action === "validate"
                    ? handleAbordValidationProcess()
                    : setOpenConfirmModal(false);
                }}
              >
                {action === "validate" ? (
                  <ConfirmDialog
                    title={t(`components.InterventionForm.message.validate`)}
                    onConfirm={handleSubmit(updateIntervention)}
                    onAbort={handleAbordValidationProcess}
                  >
                    <>
                      {action === "validate" && (
                        <>
                          <Controller
                            name={`realization_date`}
                            control={control}
                            render={({ field: { onChange, value } }) => {
                              return (
                                <TextField
                                  label={
                                    t(
                                      `components.Intervention.properties.realizationDate`
                                    ) as string
                                  }
                                  variant="filled"
                                  type="date"
                                  sx={{ width: "100%", m: 1 }}
                                  InputLabelProps={{
                                    shrink: true,
                                  }}
                                  inputProps={{
                                    max: now.toISOString().split("T")[0],
                                  }}
                                  value={value}
                                  onChange={(e) => onChange(e.target.value)}
                                />
                              );
                            }}
                          />
                          <Controller
                            name={`intervention_partner`}
                            control={control}
                            render={() => {
                              return (
                                <Box sx={{ width: "100%", m: 1 }}>
                                  <FreeSolo
                                    defaultValue={""}
                                    control={control}
                                    setValue={setValue}
                                    name={
                                      "intervention_partner" as keyof Intervention
                                    }
                                    listOptions={
                                      interventionTypesAndPartners?.intervention_partner ??
                                      ([] as IFreeSoloOption[])
                                    }
                                    required={false}
                                    label={
                                      t(
                                        `components.Intervention.properties.interventionPartner`
                                      ) as string
                                    }
                                    object={"intervention"}
                                  />
                                </Box>
                              );
                            }}
                          />

                          <Controller
                            name={`validation_note`}
                            control={control}
                            defaultValue={""}
                            render={({ field: { onChange, ref, value } }) => (
                              <TextField
                                value={value}
                                inputRef={ref}
                                fullWidth
                                sx={{
                                  display: "flex",
                                  m: 1,
                                }}
                                multiline={true}
                                label={
                                  t(
                                    `components.Intervention.properties.note`
                                  ) as string
                                }
                                variant="filled"
                                placeholder={t(
                                  "components.Intervention.placeholder.validation_note"
                                )}
                                onChange={onChange}
                              />
                            )}
                          />
                        </>
                      )}
                    </>
                  </ConfirmDialog>
                ) : (
                  <ConfirmDialog
                    message={`${t("common.buttons.confirm")} ?`}
                    onConfirm={() => {
                      if (activeInterventionId && action !== "delete") {
                        handleSubmit(updateIntervention)();
                      } else if (activeInterventionId && action === "delete") {
                        deleteIntervention();
                      } else if (interventionModal && !activeInterventionId) {
                        handleSubmit(createIntervention)();
                      } else {
                        handleSubmit(createDiagnosis)();
                      }
                    }}
                    onAbort={() => setOpenConfirmModal(false)}
                  />
                )}
              </ModalComponent>
            </>
          )}
          {isMobile && (
            <MobileButtonsCard
              withEditMode
              onSave={handleSubmit(updateTree)}
              onDelete={deleteTree}
              reset={reset}
              dirtyFields={dirtyFields}
              deleteMessage={t(`TreeForm.message.delete`)}
            />
          )}
        </Container>
      </AuthCan>
    </>
  );
};

export default TreeIdPage;

const styles = {
  container: {
    position: "relative",
    height: "calc(100% - 70px) !important",
    overflowY: "auto",
  },
};
