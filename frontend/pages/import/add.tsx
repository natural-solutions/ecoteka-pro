import { NextPage } from "next";
import { useTranslation } from "react-i18next";
import { Container } from "@mui/system";
import ImportStepper from "../../components/panel/ImportStepper";
import { DetailsHeader } from "@components/_core/headers";
import AuthCan from "@components/auth/Can";
import Custom403 from "@pages/403";

const InterventionAddPage: NextPage = () => {
  const { t } = useTranslation(["common"]);

  return (
    <>
      <AuthCan role={["reader", "editor"]}>
        <Custom403 />
      </AuthCan>
      <AuthCan role={["admin"]}>
        <Container maxWidth="xl" sx={{ mt: 1 }}>
          <DetailsHeader title={t("common.buttons.import")} />
          <ImportStepper />
        </Container>
      </AuthCan>
    </>
  );
};

export default InterventionAddPage;
