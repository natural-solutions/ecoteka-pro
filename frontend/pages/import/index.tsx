import { Stack } from "@mui/material";
import { NextPage } from "next";
import { useTranslation } from "react-i18next";
import { Container } from "@mui/system";
import {
  Import,
  useDeleteImportsMutation,
  useFetchAllImportsLazyQuery,
} from "@generated/graphql";
import { useEffect, useMemo, useState } from "react";
import ImportsGrid from "@components/grid/ImportsGrid";
import { MRT_ColumnDef } from "material-react-table";
import { formatDate } from "@lib/utils";
import AuthCan from "@components/auth/Can";
import Custom403 from "@pages/403";

const InterventionIndexPage: NextPage = () => {
  const { t } = useTranslation(["components"]);
  const [fetchAllImports, { refetch: refetchImports }] =
    useFetchAllImportsLazyQuery();
  const [deleteImports] = useDeleteImportsMutation();
  const [allImports, setAllImports] = useState<any>([]);

  const columns = useMemo<MRT_ColumnDef<Import>[]>(
    () => [
      {
        accessorFn: (originalRow) => originalRow.id,
        header: "Id",
      },
      {
        accessorFn: (originalRow) => originalRow.source_file_path,
        header: t("ImportHistoryTable.headers.file"),
      },
      {
        accessorFn: (originalRow) => originalRow.user_entity?.username,
        header: t("ImportHistoryTable.headers.username"),
      },
      {
        accessorFn: (originalRow) =>
          formatDate(
            originalRow.creation_date.substring(
              0,
              originalRow.creation_date.indexOf("T")
            )
          ),
        header: t("ImportHistoryTable.headers.importDate"),
      },
    ],
    []
  );

  const fetchImports = async () => {
    try {
      const result = await fetchAllImports();
      setAllImports(result?.data?.import);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteSelectedImports = async (ids: string[]) => {
    try {
      await deleteImports({
        variables: {
          _in: ids,
        },
      });
      const result = await refetchImports();
      setAllImports(result?.data?.import);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchImports();
  }, []);
  return (
    <>
      <AuthCan role={["reader", "editor"]}>
        <Custom403 />
      </AuthCan>
      <AuthCan role={["admin"]}>
        <Container maxWidth="xl" sx={{ mt: 1 }}>
          <Stack direction="row" alignItems="center" sx={{ mb: 2 }}></Stack>
          {allImports && (
            <ImportsGrid
              deleteImports={deleteSelectedImports}
              allImports={allImports}
              title={t("ImportHistoryTable.title")}
              columns={columns}
            />
          )}
        </Container>
      </AuthCan>
    </>
  );
};

export default InterventionIndexPage;
