import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Container, Grid } from "@mui/material";
import { useDeviceSize } from "@lib/utils";
import { DetailsHeader } from "@components/_core/headers";
import MobileButtonsCard from "@components/_core/buttons/MobileButtonsCard";
import {
  CompositionCard,
  GeoContextCard,
  StationCard,
} from "@components/_pages/VegetatedAreas/DetailPage";
import {
  useCountTreesFromBoundaryLazyQuery,
  useDeleteVegetatedAreaMutation,
  useDeleteVegetatedAreaRelationsMutation,
  useFetchOneVegetatedAreaLazyQuery,
  useFetchVegetatedAreaFormDataLazyQuery,
  useGetStationBoundaryVegetatedAreaLazyQuery,
  useGetVegetatedAreasFromStationBoundaryLazyQuery,
  useInsertVegetatedAreaRelationsMutation,
  useUpdateVegetatedAreaMutation,
} from "@generated/graphql";
import Loading from "@components/layout/Loading";
import {
  usePolygonFormActions,
  useVegetatedAreaFormData,
} from "@stores/forms/polygonForm";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import useStore from "@stores/useStore";
import AuthCan from "@components/auth/Can";
import Custom403 from "@pages/403";

type AreaIdType = string | string[] | undefined;

export const vegetatedAreaSchema = yup.object().shape({
  type: yup.string().required(),
  address: yup.string().nullable(),
  administrative_boundary: yup.string().nullable(),
  geographic_boundary: yup.string().nullable(),
  station_boundary: yup.string().nullable(),
  landscape_type: yup.string().nullable(),
  urban_site_id: yup.string().nullable(),
  urban_constraint: yup.array().of(yup.string()).nullable(),
  has_differentiated_mowing: yup.boolean().nullable(),
  is_accessible: yup.boolean().nullable(),
  residential_usage_type: yup.array().of(yup.string()).nullable(),
  inconvenience_risk: yup.string().nullable(),
  note: yup.string().nullable(),
  shrubs_data: yup.array().nullable(),
  herbaceous_data: yup.array().nullable(),
});

const VegetatedAreaPage: NextPage = () => {
  const router = useRouter();
  const { areaId } = router.query;
  const { isMobile } = useDeviceSize();
  const { t } = useTranslation(["pages", "components"]);
  const { app } = useStore((store) => store);
  const { setVegetatedAreaFormData } = usePolygonFormActions();
  const vegetatedAreaFormData = useVegetatedAreaFormData();

  const [fetchVegetatedArea, { data: activeVegetatedArea, loading, refetch }] =
    useFetchOneVegetatedAreaLazyQuery();
  const [fetchVegetatedAreaData, { data: formData }] =
    useFetchVegetatedAreaFormDataLazyQuery();
  const [fetchStationBoundary, { data: stationBoundary }] =
    useGetStationBoundaryVegetatedAreaLazyQuery();
  const [getVegetatedAreasFromStation, { data: vegetatedAreasFromStation }] =
    useGetVegetatedAreasFromStationBoundaryLazyQuery();
  const [countTreesFromStation, { data: treesFromStation }] =
    useCountTreesFromBoundaryLazyQuery();
  const [deleteVegetatedAreaById] = useDeleteVegetatedAreaMutation();
  const [updateVegetatedAreaById] = useUpdateVegetatedAreaMutation();
  const [deleteVegetatedAreaRelations] =
    useDeleteVegetatedAreaRelationsMutation();
  const [insertVegetatedAreaRelations] =
    useInsertVegetatedAreaRelationsMutation();

  const defaultValues =
    vegetatedAreaFormData || activeVegetatedArea?.vegetated_area || {};

  const {
    handleSubmit,
    setValue,
    watch,
    reset,
    control,
    getValues,
    formState: { dirtyFields, errors },
  } = useForm({
    resolver: yupResolver(vegetatedAreaSchema),
    defaultValues: defaultValues,
  });

  const onSuccess = async (action: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`components.VegetatedAreaForm.success.${action}`),
            severity: "success",
          },
        ],
      },
    });
  };

  const updateVegetatedArea = async () => {
    const formData = getValues();
    const {
      administrative_boundary,
      station_boundary,
      geographic_boundary,
      urban_constraint,
      residential_usage_type,
      locations,
      __typename,
      linear_meters,
      hedge_type,
      potential_area_state,
      afforestation_trees_data,
      type,
      ...formDataWithoutObjects
    } = formData as any;

    await updateVegetatedAreaById({
      variables: {
        id: areaId,
        _set: {
          ...formDataWithoutObjects,
          type: type,
          afforestation_trees_data:
            type === "afforestation" ? afforestation_trees_data : null,
          linear_meters: type === "hedge" ? linear_meters : null,
          hedge_type: type === "hedge" ? hedge_type : null,
          potential_area_state:
            type === "potential_area" ? potential_area_state : null,
        },
      },
      onCompleted: async () => {
        await deleteVegetatedAreaRelations({
          variables: {
            id: areaId,
          },
          onCompleted: async () => {
            // Prepare nested objects for relations
            const boundaryObjects = [
              ...(geographic_boundary
                ? [
                    {
                      boundary_id: geographic_boundary,
                      vegetated_area_id: areaId,
                    },
                  ]
                : []),
              ...(station_boundary
                ? [
                    {
                      boundary_id: station_boundary,
                      vegetated_area_id: areaId,
                    },
                  ]
                : []),
              ...(administrative_boundary
                ? [
                    {
                      boundary_id: administrative_boundary,
                      vegetated_area_id: areaId,
                    },
                  ]
                : []),
            ].filter((obj) => obj.boundary_id && obj.vegetated_area_id);
            const urbanConstraintObjects = urban_constraint?.map(
              (constraint) => ({
                urban_constraint_id: constraint.id,
                vegetated_area_id: areaId,
              })
            );
            const residentialUsageTypeObjects = residential_usage_type?.map(
              (usage) => ({
                residential_usage_type: usage.id,
                vegetated_area_id: areaId,
              })
            );
            await insertVegetatedAreaRelations({
              variables: {
                boundaryObjects,
                urbanConstraintObjects,
                residentialUsageTypeObjects,
              },
            });
          },
        });
        onSuccess("update");
        refetch();
      },
    });
  };

  const deleteVegetatedArea = async (id: string) => {
    deleteVegetatedAreaById({
      variables: {
        id: id,
      },
      onCompleted: () => {
        onSuccess("delete");
        router.push("/map");
      },
    });
  };
  const resetVegetatedAreaForm = () => {
    console.log("resetdeleteVegetatedAreaForm");
  };

  /*** Page logic utils functions ***/

  /**
   * Initialize the vegetated area data on page change
   * @param areaId - The id of the vegetated area
   */
  function initVegetatedArea(areaId: AreaIdType) {
    fetchVegetatedArea({
      variables: {
        id: areaId,
      },
      onCompleted: async () => {
        fetchStationBoundary({
          variables: {
            vegetated_area_id: areaId,
          },
          onCompleted: async (res) => {
            if (res?.boundaries_vegetated_areas[0]?.boundary) {
              const boundaryId =
                res?.boundaries_vegetated_areas[0]?.boundary?.id;
              getVegetatedAreasFromStation({
                variables: {
                  boundary_id: boundaryId,
                },
              });
              countTreesFromStation({
                variables: {
                  boundary_id: boundaryId,
                },
              });
            }
          },
        });
      },
    });
  }

  /**
   * Business logic reactivity
   */

  useEffect(() => {
    if (!activeVegetatedArea) {
      initVegetatedArea(areaId);
    } else {
      reset({
        ...activeVegetatedArea?.vegetated_area,
        locations: activeVegetatedArea?.locations,
        urban_constraint: activeVegetatedArea?.urban_constraint,
        residential_usage_type: activeVegetatedArea?.residential_usage_type,
        geographic_boundary:
          activeVegetatedArea?.geographic_boundary[0]?.boundary_id,
        administrative_boundary:
          activeVegetatedArea?.administrative_boundary[0]?.boundary_id,
        station_boundary: activeVegetatedArea?.station_boundary[0]?.boundary_id,
      });
    }
    fetchVegetatedAreaData();

    setVegetatedAreaFormData(undefined);
  }, [areaId, activeVegetatedArea]);

  if (loading) {
    return <Loading />;
  }

  return (
    <>
      <AuthCan role={["reader"]}>
        <Custom403 />
      </AuthCan>
      <AuthCan role={["admin", "editor"]}>
        <Container maxWidth="xl" sx={isMobile ? styles.container : {}}>
          {!isMobile && (
            <DetailsHeader
              title={t("VegetatedAreaDetailPage.Header.vegetatedArea.title")}
              withEditMode
              onSave={() => updateVegetatedArea()}
              onDelete={deleteVegetatedArea}
              reset={resetVegetatedAreaForm}
              dirtyFields={dirtyFields}
              deleteMessage={t(
                `VegetatedAreaDetailPage.Header.vegetatedArea.deleteMessage`
              )}
            />
          )}
          {isMobile && (
            <MobileButtonsCard
              withEditMode
              onSave={handleSubmit(updateVegetatedArea)}
              onDelete={deleteVegetatedArea}
              reset={resetVegetatedAreaForm}
              dirtyFields={dirtyFields}
              deleteMessage={t(
                `VegetatedAreaDetailPage.Header.vegetatedArea.deleteMessage`
              )}
            />
          )}
          {activeVegetatedArea && (
            <>
              <Grid container spacing={2} mt={1} mb={1}>
                <Grid item md={6}>
                  <GeoContextCard
                    formData={formData}
                    activeVegetatedArea={activeVegetatedArea}
                    control={control}
                    setValue={setValue}
                    watch={watch}
                    dirtyFields={dirtyFields}
                  />
                </Grid>
                {activeVegetatedArea?.vegetated_area?.type !==
                  "potential_area" && (
                  <Grid item md={6}>
                    <CompositionCard
                      activeVegetatedArea={activeVegetatedArea}
                      control={control}
                      setValue={setValue}
                      watch={watch}
                    />
                  </Grid>
                )}
                {stationBoundary?.boundaries_vegetated_areas[0]?.boundary && (
                  <Grid item md={6}>
                    <StationCard
                      name={
                        stationBoundary?.boundaries_vegetated_areas[0].boundary
                          .name
                      }
                      vegetatedAreasFromStation={
                        vegetatedAreasFromStation?.vegetated_area_aggregate
                      }
                      treesFromStation={
                        treesFromStation?.boundaries_locations_aggregate
                      }
                    />
                  </Grid>
                )}
              </Grid>
            </>
          )}
        </Container>
      </AuthCan>
    </>
  );
};

export default VegetatedAreaPage;

const styles = {
  container: {
    position: "relative",
    height: "calc(100% - 70px) !important",
    overflowY: "auto",
  },
};
