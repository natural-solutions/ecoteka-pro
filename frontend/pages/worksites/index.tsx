import { NextPage } from "next";
import Container from "@mui/material/Container";
import { WorksitesGrid } from "@ecosystems/Worksites";
import { useFetchAllWorksitesLazyQuery } from "generated/graphql";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { DetailsHeader as GridHeader } from "@components/_core/headers";
import AuthCan from "@components/auth/Can";
import Custom403 from "@pages/403";

const WorksitesIndexPage: NextPage = () => {
  const { t } = useTranslation(["components"]);
  const [fetchWorksites, { data: worksites }] = useFetchAllWorksitesLazyQuery();

  useEffect(() => {
    fetchWorksites();
  }, []);

  return (
    <>
      <AuthCan role={["reader"]}>
        <Custom403 />
      </AuthCan>
      <AuthCan role={["admin", "editor"]}>
        <Container maxWidth="xl" sx={{ mt: 1 }}>
          <GridHeader title={t("Template.menuItems.worksite.title")} />
          <WorksitesGrid worksites={worksites?.worksite ?? []} />
        </Container>
      </AuthCan>
    </>
  );
};

export default WorksitesIndexPage;
