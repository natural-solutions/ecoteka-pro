import { useEffect, useState } from "react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { useTranslation } from "react-i18next";
import {
  FetchOneWorksiteQuery,
  useCreateTreeMutation,
  useDeleteInterventionMutation,
  useDeleteWorksiteByPkMutation,
  useFetchOneWorksiteLazyQuery,
  useFetchOneWorksiteLocationsLazyQuery,
  useUpdateInterventionMutation,
  useUpdateLocationsStatusesWithLocationsIdsMutation,
  useUpdateTreesWithFellingDateMutation,
  useUpdateWorksiteMutation,
  useValidateInterventionsMutation,
} from "@generated/graphql";
import { Controller, useForm } from "react-hook-form";
import useStore from "@lib/store/useStore";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { format } from "date-fns";

import {
  Box,
  Button,
  Container,
  Grid,
  Paper,
  Stack,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import WorksiteCard from "@blocks/layout/WorksiteCard/WorksiteCard";
import LocationList from "@blocks/layout/LocationList";
import { DetailsHeader } from "@core/headers";
import WorkInProgress from "@core/WorkInProgress/WorkInProgress";
import ScheduledDateCard from "@components/_blocks/cards/ScheduledDateCard/ScheduledDateCard";
import _ from "lodash";
import Loading from "@components/layout/Loading";
import { exportToPDF, formatSerialNumber, useDeviceSize } from "@lib/utils";
import MobileButtonsCard from "@components/_core/buttons/MobileButtonsCard";
import CloudDownloadIcon from "@mui/icons-material/CloudDownload";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import InfoIcon from "@mui/icons-material/Info";
import AuthCan from "@components/auth/Can";
import Custom403 from "@pages/403";
import { useSession } from "next-auth/react";
import InterventionsInfosCard from "@components/_blocks/layout/InterventionsInfosCard";
import { DateRange } from "react-day-picker";

// TODO:
const toNumber = (value) => _.toNumber(value);
const transformNumber = (value) => (isNaN(value) ? undefined : Number(value));

export type ConditionalSchema<T> = T extends string
  ? Yup.StringSchema
  : T extends number
    ? Yup.NumberSchema
    : T extends boolean
      ? Yup.BooleanSchema
      : T extends Record<any, any>
        ? Yup.AnyObjectSchema
        : T extends Array<any>
          ? Yup.ArraySchema<any, any>
          : Yup.AnySchema;

export type Shape<Fields> = {
  [Key in keyof Fields]: ConditionalSchema<Fields[Key]>;
};

const worksiteSchema = Yup.object().shape<Shape<FetchOneWorksiteQuery>>({
  worksite: Yup.object().shape({
    id: Yup.string().nullable(),
    name: Yup.string().required("le champ est requis"),
    description: Yup.string().nullable(),
    cost: Yup.number().transform(transformNumber).nullable(),
  }),
});

export interface CheckedIntervention {
  interventionId: string;
  locationId: string;
}

const WorksiteMainPage: NextPage = () => {
  const router = useRouter();
  const { worksiteId } = router.query;
  const isEditable = router.query.hasOwnProperty("edit");
  const { t } = useTranslation(["components", "common"]);
  const { app } = useStore((store) => store);
  const session = useSession();
  const userRole = session?.data?.roles ?? [];
  const currentUserId = session?.data?.user?.id;
  const now = new Date().toISOString().split("T")[0];
  const { isMobile } = useDeviceSize();
  const [
    fetchWorksite,
    {
      data: worksiteData,
      loading: worksiteLoading,
      error: worksiteError,
      refetch,
    },
  ] = useFetchOneWorksiteLazyQuery();
  const [fetchInterventions, { data: interventionsData }] =
    useFetchOneWorksiteLocationsLazyQuery();
  const [checkedInterventions, setCheckedInterventions] = useState<
    CheckedIntervention[]
  >([]);
  const [validateInterventions] = useValidateInterventionsMutation();
  const [updateWorksiteMutation] = useUpdateWorksiteMutation();
  const [deleteWorksiteByPkMutation] = useDeleteWorksiteByPkMutation();
  const [deleteInterventionByPkMutation] = useDeleteInterventionMutation();
  const [updateInterventionByPkMutation] = useUpdateInterventionMutation();
  const [updateLocationsStatuses] =
    useUpdateLocationsStatusesWithLocationsIdsMutation();
  const [createTreeMutation] = useCreateTreeMutation();
  const [updateTreesMutation] = useUpdateTreesWithFellingDateMutation();

  const isWIP = false;

  const isWorksiteRealized = () => {
    if (!worksiteData?.worksite?.interventions?.length) return false;
    return worksiteData?.worksite.realization_date;
  };

  const allowDuplicate = worksiteData?.worksite?.interventions?.some(
    (intervention) =>
      !["grubbing", "felling", "planting"].includes(
        intervention.intervention_type.slug
      )
  );

  const setEditModeOff = () => {
    const newQuery = { ...router.query };
    delete newQuery.edit;
    router.replace({ query: { ...newQuery } }, undefined, {
      shallow: true,
    });
  };

  const {
    handleSubmit,
    getValues,
    reset,
    setValue,
    watch,
    control,
    formState: { dirtyFields, errors },
  } = useForm<FetchOneWorksiteQuery>({
    resolver: yupResolver(worksiteSchema),
    mode: "all",
    defaultValues: {
      worksite: {
        id: "",
        name: "",
        description: "",
        cost: 0,
        creation_date: "",
        interventions: [],
        realization_date: "",
        scheduled_start_date: "",
        scheduled_end_date: "",
        realization_report: "",
      },
    },
  });

  const onSuccess = async (action: string) => {
    app.setAppState({
      snackbar: {
        ...app.snackbar,
        alerts: [
          {
            message: t(`WorksiteForm.success.${action}`),
            severity: "success",
          },
        ],
      },
    });
  };

  const watchName = watch("worksite.name");

  useEffect(() => {
    fetchWorksite({
      variables: { id: worksiteId },
    });
    fetchInterventions({
      variables: { id: worksiteId },
    });
  }, []);

  useEffect(() => {
    worksiteData && reset(worksiteData);
  }, [worksiteData]);

  const deleteWorksite = async (id: string) => {
    const deleteInterventionsPromises =
      interventionsData?.worksite?.interventions.map((worksiteIntervention) => {
        return deleteInterventionByPkMutation({
          variables: {
            id: worksiteIntervention.id,
          },
        });
      });

    if (deleteInterventionsPromises) {
      await Promise.all(deleteInterventionsPromises);
      await deleteWorksiteByPkMutation({
        variables: {
          id: id,
        },
        onCompleted: () => {
          onSuccess("delete");
        },
      });
    }
    await router.push(`/worksites`);
  };

  const handleUpdateLocations = async (
    newStatus: string,
    selectedLocationsIds: string[],
    interventionDate?: string
  ) => {
    newStatus === "stump" &&
      (await updateTreesMutation({
        variables: {
          _in: selectedLocationsIds,
          felling_date: now,
        },
        onCompleted: async (response) => {
          await updateLocationsStatuses({
            variables: {
              _in: selectedLocationsIds,
              id_status: "d223091c-4931-47a0-8ebc-44c99a1663ed",
            },
            onCompleted: () => onSuccess("updateAndDeleteTrees"),
          });
        },
      }));

    ["empty", "alive"].includes(newStatus) &&
      (await updateLocationsStatuses({
        variables: {
          _in: selectedLocationsIds,
          id_status:
            newStatus === "alive"
              ? "bf6fcfe3-fbe9-4467-98be-a935efcf67a3"
              : "b2fbee37-c6a6-4431-9c70-f361ade578b2",
        },
        onCompleted: async () => {
          newStatus === "alive"
            ? selectedLocationsIds.map(async (id) => {
                await createTreeMutation({
                  variables: {
                    serial_number: formatSerialNumber(undefined),
                    location_id: id,
                    data_entry_user_id: currentUserId,
                    plantation_date: interventionDate,
                  },
                  onCompleted: async (response) => {
                    onSuccess("updateAndCreateTrees");
                  },
                });
              })
            : onSuccess("validateAndUpdateLocation");
        },
      }));
  };

  const updateWorksite = async () => {
    const formData = getValues();
    await updateWorksiteMutation({
      variables: {
        id: worksiteId,
        input_data: {
          scheduled_start_date: formData?.worksite?.scheduled_start_date,
          scheduled_end_date: formData?.worksite?.scheduled_end_date,
          name: formData?.worksite?.name,
          cost: formData?.worksite?.cost,
          description: formData?.worksite?.description,
          realization_report: formData?.worksite?.realization_report,
        },
      },
      onCompleted: () => {
        if (checkedInterventions.length) {
          handleValidateInterventions(false);
        } else {
          onSuccess("update");
        }
      },
    });
  };

  const handleValidateInterventions = async (
    validateAll: boolean = false,
    realizationReport?: string
  ) => {
    const interventionIds = validateAll
      ? interventionsData?.worksite?.interventions
          .filter((intervention) => intervention.realization_date === null)
          .map((intervention) => intervention.id) || []
      : checkedInterventions
          .filter((ci) => {
            const intervention =
              interventionsData?.worksite?.interventions.find(
                (intervention) => intervention.id === ci.interventionId
              );
            return intervention?.realization_date === null;
          })
          .map((ci) => ci.interventionId);

    await validateInterventions({
      variables: {
        _in: interventionIds,
        realization_date: now,
        data_entry_user_id: currentUserId,
      },
      onCompleted: async (response) => {
        const interventionType =
          response.update_intervention?.returning[0].intervention_type?.slug;
        const selectedLocationsIds = validateAll
          ? interventionsData?.worksite?.interventions.map(
              (intervention) =>
                intervention.location?.id || intervention.tree?.location.id
            )
          : checkedInterventions.map((ci) => ci.locationId);
        if (selectedLocationsIds) {
          switch (interventionType) {
            case "planting":
              await handleUpdateLocations(
                "alive",
                selectedLocationsIds,
                response.update_intervention?.returning[0].realization_date
              );
              break;
            case "grubbing":
              await handleUpdateLocations("empty", selectedLocationsIds);
              break;
            case "felling":
              await handleUpdateLocations("stump", selectedLocationsIds);
              break;
            default:
              break;
          }
        }

        // Refetch worksite data to get latest state
        const worksite = await refetch();

        // Check if all interventions are realized after refetch
        const allInterventionsRealized =
          worksite?.data.worksite?.interventions?.every(
            (intervention) => intervention.realization_date
          ) || validateAll;

        if (allInterventionsRealized) {
          await updateWorksiteMutation({
            variables: {
              id: worksiteId,
              input_data: {
                realization_date: now,
                realization_report: realizationReport,
                realization_user_id: currentUserId,
              },
            },
            onCompleted: () => {
              onSuccess("realized");
              refetch();
            },
          });
        }

        if (
          interventionType &&
          !["planting", "felling", "grubbing"].includes(interventionType)
        ) {
          onSuccess("validateInterventions");
          await refetch();
        }
        setEditModeOff();
      },
    });
  };

  const handleDuplicate = () => {
    router.push(`/map?activeWorksite=${worksiteId}&duplicate=true`);
  };

  const handleCheckboxChange = (interventionId: string, locationId: string) => {
    setCheckedInterventions((prev) => {
      const isChecked = prev.some((ci) => ci.interventionId === interventionId);
      if (isChecked) {
        return prev.filter((ci) => ci.interventionId !== interventionId);
      } else {
        return [...prev, { interventionId, locationId }];
      }
    });
  };

  const handleScheduledDateChange = (
    id: string,
    date: DateRange | undefined
  ) => {
    if (date && date.from && date.to) {
      const formattedStartDate = format(date.from, "yyyy-MM-dd");
      const formattedEndDate = format(date.to, "yyyy-MM-dd");

      updateWorksiteMutation({
        variables: {
          id: id,
          input_data: {
            scheduled_start_date: formattedStartDate,
            scheduled_end_date: formattedEndDate,
          },
        },
        onCompleted: (response) => {
          interventionsData?.worksite?.interventions.map(
            (worksiteIntervention) => {
              const {
                intervention_type,
                intervention_partner,
                tree,
                location,
                scheduled_date,
                __typename,
                ...formDataWithoutObjects
              } = worksiteIntervention;
              return updateInterventionByPkMutation({
                variables: {
                  scheduled_date: formattedEndDate,
                  intervention_type_id:
                    worksiteIntervention.intervention_type.id,
                  intervention_partner_id:
                    worksiteIntervention.intervention_partner?.id || null,
                  ...formDataWithoutObjects,
                },
              });
            }
          );
          app.setAppState({
            snackbar: {
              ...app.snackbar,
              alerts: [
                {
                  message: t(`WorksiteDetails.scheduledDateChange.success`),
                  severity: "success",
                },
              ],
            },
          });
          refetch();
        },
      });
    }
  };

  if (isWIP) {
    return <WorkInProgress type="page" />;
  }

  if (worksiteLoading) {
    return <Loading />;
  }

  return (
    <>
      <AuthCan role={["reader"]}>
        <Custom403 />
      </AuthCan>
      <AuthCan role={["admin", "editor"]}>
        <Container maxWidth="xl" sx={{ mt: 1 }}>
          {!isMobile && (
            <DetailsHeader
              title={watchName}
              withEditMode
              allowExport
              showDuplicate
              allowDuplicate={allowDuplicate}
              onDuplicate={handleDuplicate}
              onSave={handleSubmit(updateWorksite)}
              onDelete={deleteWorksite}
              deleteMessage={t(`WorksiteForm.message.delete`)}
              reset={reset}
              dirtyFields={dirtyFields}
            />
          )}
          {isMobile && (
            <Box
              sx={{
                textAlign: "center",
                mb: 1,
                display: "flex",
                justifyContent: "center",
                gap: 0.5,
              }}
            >
              <Button
                variant="contained"
                color="inherit"
                onClick={exportToPDF}
                startIcon={<CloudDownloadIcon />}
              >
                {t("common.buttons.exportPdf")}
              </Button>
              <AuthCan role={["admin"]}>
                <Tooltip
                  title={
                    !allowDuplicate
                      ? t("components.WorksiteForm.duplicateTooltipNotAllowed")
                      : t("components.WorksiteForm.duplicateTooltip")
                  }
                >
                  <Button
                    variant="contained"
                    color="inherit"
                    onClick={handleDuplicate}
                    startIcon={<ContentCopyIcon />}
                    endIcon={<InfoIcon />}
                    disabled={!allowDuplicate}
                  >
                    {t("common.buttons.duplicate")}
                  </Button>
                </Tooltip>
              </AuthCan>
            </Box>
          )}
          <Grid container columns={12} spacing={2}>
            <Grid item xs={12} sm={12} md={6}>
              {isWorksiteRealized() && (
                <Paper elevation={1} sx={{ p: 1, m: 1 }}>
                  <Typography variant="h6" mb={2}>
                    {t(`components.WorksiteForm.realizationReportTitle`)}
                  </Typography>
                  <Controller
                    name="worksite.realization_report"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        label={t(`components.WorksiteForm.realizationReport`)}
                        variant="outlined"
                        fullWidth
                        multiline
                        rows={4}
                        disabled={!isEditable}
                        sx={{ mb: 2 }}
                      />
                    )}
                  />
                  <TextField
                    label={t(`components.WorksiteForm.declareRealizedBy`)}
                    variant="outlined"
                    fullWidth
                    disabled
                    value={worksiteData?.worksite?.user_entity?.username}
                  />
                </Paper>
              )}

              <Stack direction="column" spacing={{ xs: 1, sm: 2 }}>
                {worksiteData && !isWorksiteRealized() && (
                  <ScheduledDateCard
                    itemId={worksiteData?.worksite?.id}
                    scheduledStartDate={
                      worksiteData?.worksite?.scheduled_start_date as Date
                    }
                    scheduledEndDate={
                      worksiteData?.worksite?.scheduled_end_date as Date
                    }
                    onConfirmScheduledDateChange={handleScheduledDateChange}
                  />
                )}
                {worksiteData && (
                  <>
                    <WorksiteCard
                      data={worksiteData}
                      control={control}
                      setValue={setValue}
                      userRole={userRole}
                    />
                    <InterventionsInfosCard data={worksiteData} />
                  </>
                )}
              </Stack>
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              {interventionsData && worksiteData && (
                <LocationList
                  data={interventionsData}
                  onCheckboxChange={handleCheckboxChange}
                  checkedInterventions={checkedInterventions}
                  validateAllInterventions={handleValidateInterventions}
                  isWorksiteRealized={isWorksiteRealized()}
                />
              )}
            </Grid>
          </Grid>
          {isMobile && (
            <MobileButtonsCard
              withEditMode
              onSave={handleSubmit(updateWorksite)}
              onDelete={deleteWorksite}
              reset={reset}
              dirtyFields={dirtyFields}
              deleteMessage={t(`WorksiteForm.message.delete`)}
            />
          )}
        </Container>
      </AuthCan>
    </>
  );
};

export default WorksiteMainPage;
