import { useEffect, useState, useCallback, useRef } from "react";
import { NextPage } from "next";
import { Stack } from "@mui/material";
import { FlyToInterpolator } from "deck.gl";
import { EditableGeoJsonLayer, SelectionLayer } from "@nebula.gl/layers";
import {
  createPolygonsLayer,
  createLocationsLayer,
  generateLayerDataUrl,
} from "@components/map/layers/Layers";
import { DrawPolygonMode, ModifyMode } from "@nebula.gl/edit-modes";
import _ from "lodash";
import MapBase, { InitialViewState } from "@components/map/Base";
import TempLayer from "@components/map/layers/Temp";
import MapActions from "@components/map/MapActions";
import Panel from "@components/map/Panel";
import "maplibre-gl/dist/maplibre-gl.css";
import useStore from "@lib/store/useStore";
import { useSession } from "next-auth/react";
import { useTranslation } from "react-i18next";
import {
  LocationType,
  MapBackground,
  MapActionsBarActionType,
  useViewState,
  initialMapEditorState,
  useEditionMode,
  useMapActiveAction,
  useMapEditorActions,
  useMapBackground,
  useMapTempPoint,
  useMapUserPosition,
  useMapPanelOpen,
  useMapLocationType,
  useMapLayers,
  useMapLocations,
  useMapStyle,
  useLocationStatuses,
  useFirstLoad,
  useMapFilteredLocations,
  useMapSelectedLocations,
  useSelectionLayerActive,
  useMapActiveLocationId,
  useForceReloadLayer,
} from "@lib/store/pages/mapEditor";
import {
  CreateTreeMutationVariables,
  Urban_Constraint,
  useCreateLocationMutation,
  useCreateTreeMutation,
  useFetchAllBoundariesLazyQuery,
  useFetchLocationFormDataLazyQuery,
  useFetchLocationStatusLazyQuery,
  useFetchOneBoundaryLazyQuery,
  useFetchOneLocationLazyQuery,
  useFetchOneVegetatedAreaLazyQuery,
  useFetchOneWorksiteInterventionsLazyQuery,
  useFetchOneWorksiteLazyQuery,
  useGetLocationsInsidePolygonLazyQuery,
  useInsertLocationBoundariesMutation,
  useInsertLocationUrbanConstraintMutation,
  useInsertLocationVegetatedAreaMutation,
} from "../generated/graphql";
import i18next from "../i18n";
import InitialModal from "@components/map/InitialModal";
import { useRouter } from "next/router";
import PanelLocation, { Location } from "@components/panel/Location";
import MapNavigation, { Direction } from "@components/map/buttons/Navigation";
import WorksitePanelContent from "@ecosystems/MapEditor/Panels/Worksite/WorksitePanelContent";
import CardInfos from "@components/_pages/MapEditor/CardInfos/CardInfos";
import {
  checkIfImageExists,
  getPolygonArea,
  useDeviceSize,
  isPresent,
} from "@lib/utils";
import { Feature } from "maplibre-gl";
import { useFiltersWithMartinContext } from "@context/FiltersWithMartinContext";
import PanelFiltersWithMartin from "@components/panel/FiltersWithMartin";
import Button from "@mui/material/Button";
import PolygonPanel from "@components/_pages/MapEditor/Panels/Polygon/PolygonPanelContent";
import {
  usePolygonFeatures,
  usePolygonFormActions,
  usePolygonCommonData,
} from "@stores/forms/polygonForm";
import { moveMinioFile } from "@services/MinioService";
import getConfig from "next/config";
import {
  useWorksiteActiveStep,
  useWorksiteFormActions,
  useWorksiteHoveredItemId,
  useWorksiteInterventionFormData,
} from "@stores/forms/worksiteForm";
import WorksiteLayer from "@components/map/layers/Worksite";

export type FeatureProperties = {
  id: string;
  layerName?: string;
  [key: string]: any;
};

const MapEditor: NextPage = () => {
  const { t } = useTranslation("components");
  const { app } = useStore((store) => store);
  const { query, push, replace } = useRouter();
  const session = useSession();
  const { publicRuntimeConfig } = getConfig();
  const currentUserId = session?.data?.user?.id ? session?.data?.user?.id : "";
  const { isDesktop } = useDeviceSize();
  const previousMapBackground = useRef(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [cardPreview, setCardPreview] = useState<boolean>(false);
  const [openCardModal, setOpenCardModal] = useState<boolean>(false);
  const [infos, setInfos] = useState();
  const [selectedLocationIds, setSelectedLocationIds] = useState<Set<string>>(
    new Set()
  );
  const [queryStringLocations, setQueryStringLocations] = useState<string>("");
  const [queryStringPolygons, setQueryStringPolygons] = useState<string>(
    "geographic_boundaries_active=true"
  );
  const { filtersWithMartin, setFiltersWithMartin, triggerFieldReset } =
    useFiltersWithMartinContext()!;

  const [fetchLocation, { data: activeLocationData }] =
    useFetchOneLocationLazyQuery();
  const [fetchLocationsStatuses] = useFetchLocationStatusLazyQuery();
  const [fetchBoundaries, { data: boundaries }] =
    useFetchAllBoundariesLazyQuery();
  const [fetchLocationFormData, { data: locationFormData }] =
    useFetchLocationFormDataLazyQuery();
  const [getLocationsInsidePolygon, { data: locationsInsidePolygon }] =
    useGetLocationsInsidePolygonLazyQuery();
  const [fetchBoundary, { data: boundary }] = useFetchOneBoundaryLazyQuery();
  const [fetchVegetatedArea, { data: vegetatedArea }] =
    useFetchOneVegetatedAreaLazyQuery();
  const [fetchWorksite, { data: worksite }] = useFetchOneWorksiteLazyQuery();
  const [fetchInterventionsWorksite, { data: interventionsDataWorksite }] =
    useFetchOneWorksiteInterventionsLazyQuery();
  const [insertlocationBoundaries] = useInsertLocationBoundariesMutation();
  const [insertlocationVegetatedArea] =
    useInsertLocationVegetatedAreaMutation();
  const [insertLocationUrbanConstraint] =
    useInsertLocationUrbanConstraintMutation();
  const [createLocationMutation] = useCreateLocationMutation();
  const [createTreeMutation] = useCreateTreeMutation();
  const [selectedFeatureIndexes] = useState([0]);
  const currentUserToken = session?.data?.accessToken
    ? session?.data?.accessToken
    : "";

  // Fix front_build
  // here use our getters from our MapEditorStore
  const firstLoad = useFirstLoad();
  const viewState = useViewState();
  const editionMode = useEditionMode();
  const mapStyle = useMapStyle();
  const mapActiveAction = useMapActiveAction();
  const mapBackground = useMapBackground();
  const mapTempPoint = useMapTempPoint();
  const mapUserPosition = useMapUserPosition();
  const mapPanelOpen = useMapPanelOpen();
  const mapLocationType = useMapLocationType();
  const mapActiveLocationId = useMapActiveLocationId();
  const mapLayers = useMapLayers();
  const isSelectionLayerActive = useSelectionLayerActive();
  const mapLocations = useMapLocations();
  const mapFilteredLocations = useMapFilteredLocations();
  const mapSelectedLocations = useMapSelectedLocations();
  const worksiteHoveredId = useWorksiteHoveredItemId();
  const mapStatuses = useLocationStatuses();
  const worksiteActiveStep = useWorksiteActiveStep();
  const worksiteInterventions = useWorksiteInterventionFormData();
  const {
    setPolygonCommonData,
    setPolygonFeatures,
    setBoundaryFormData,
    setPolygonObjectType,
  } = usePolygonFormActions();
  const polygonFeatures = usePolygonFeatures();
  const polygon = usePolygonCommonData();
  // selector for import all of our actions for the map
  const actions = useMapEditorActions();
  const {
    addLayer,
    removeLayer,
    setMapSelectedLocations,
    setForceReloadLayer,
    setHoveredSelectedLocationId,
  } = actions;
  const {
    setIsSelectionValidated,
    setWorksite,
    setInterventions,
    setLocations,
    setAssociatedLocations,
  } = useWorksiteFormActions();
  const forceReloadLayer = useForceReloadLayer();

  useEffect(() => {
    const { object } = query;
    if (query?.activeWorksite) {
      actions.setMapActiveAction("addWorksiteGroup");
      actions.setMapPanelOpen(true);
    }
    if (object) {
      const objectAsString = Array.isArray(object) ? object[0] : object;
      const decodedObject = JSON.parse(decodeURIComponent(objectAsString));
      setFiltersWithMartin(decodedObject);
    }
  }, [query]);
  //@ts-ignore
  const selectionLayer = new SelectionLayer({
    id: "selection",
    selectionType: "rectangle",
    onSelect: ({ pickingInfos }) => {
      if (mapActiveAction === "addWorksiteGroup") {
        const featuresProperties: FeatureProperties[] = [];
        const uniquePolygons: { [key: string]: FeatureProperties } = {};

        pickingInfos.forEach((picked) => {
          if (picked.layer.id === "locations") {
            // Allow all locations
            featuresProperties.push(picked.object.properties);
          } else if (
            picked.layer.id === "polygons" &&
            picked.object.properties.layerName === "vegetated_areas"
          ) {
            // Deduplicate polygons based on unique properties
            const featureId = `${picked.object.properties.id}-${picked.object.properties.layerName}`;
            if (!uniquePolygons[featureId]) {
              uniquePolygons[featureId] = picked.object.properties;
              featuresProperties.push(picked.object.properties);
            }
          }
        });
        actions.setMapSelectedLocations(featuresProperties);
      }
    },
    layerIds: ["locations", "polygons"],
    getTentativeFillColor: () => [255, 0, 255, 100],
    getTentativeLineColor: () => [0, 0, 255, 255],
    getTentativeLineDashArray: () => [0, 0],
    lineWidthMinPixels: 3,
  });

  const polygonLayer =
    //@ts-ignore
    new EditableGeoJsonLayer({
      id: "polygon-layer",
      data: polygonFeatures,
      mode:
        query?.activePolygon && query.hasOwnProperty("edit")
          ? ModifyMode
          : DrawPolygonMode,
      selectedFeatureIndexes,
      onEdit: ({ updatedData }): any => {
        // Ensure that only one feature is present in updatedData
        if (polygonFeatures.features.length > 0 && !query?.activePolygon) {
          const newFeatures = {
            ...polygonFeatures,
            features: polygonFeatures.features.slice(1), // Slice from index 1 to keep only the last polygon
          };
          setPolygonFeatures(newFeatures);
        } else {
          setPolygonFeatures(updatedData);
        }
      },
    });

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleOnActionClick = (action: MapActionsBarActionType) => {
    actions.setMapActiveAction(action);
    if (mapActiveAction === action) {
      actions.setMapPanelOpen(!mapPanelOpen);
    } else {
      actions.setMapPanelOpen(true);
    }
  };

  const handleOnAddLocation = (locationType: LocationType) => {
    actions.setMapActiveLocationId(undefined);
    actions.renderSelectionLayer(false);
    setOpenCardModal(false);
    setCardPreview(false);
    actions.setEditionMode(true);
    actions.setMapFilteredLocations(undefined);
    actions.setMapSelectedLocations(undefined);
    actions.setMapActiveAction("addLocation", locationType);
    actions.setMapPanelOpen(false);
    actions.setMapBackground(MapBackground.Aerial);
  };

  const handleCancelProcess = () => {
    actions.setMapActiveAction(undefined);
    actions.renderSelectionLayer(false);
    actions.setMapFilteredLocations(undefined);
    actions.setMapSelectedLocations(undefined);
    actions.setMapPanelOpen(false);
    setWorksite({
      name: "",
      description: "",
      scheduled_start_date: "",
      scheduled_end_date: "",
    });
    setInterventions([
      {
        is_current: true,
        intervention_type: { id: "", slug: "", location_types: [] },
        intervention_partner: "",
        associated_locations: [],
      },
    ]);
    setLocations([]);
    triggerFieldReset();
    setIsSelectionValidated(false);
    replace(`/map`);
  };

  const exitEditMode = () => {
    actions.setEditionMode(false);
    actions.setMapActiveAction(undefined);
    actions.setMapPanelOpen(false);
    actions.setForceReloadLayer(true);
    actions.setMapBackground(
      previousMapBackground.current ?? MapBackground.Map
    );
    const newQuery = { ...query };
    delete newQuery.edit;
    push({ query: { ...newQuery } }, undefined, {
      shallow: true,
    });
  };

  const handleOnBaseMapChange = (map: any) => {
    previousMapBackground.current = map;
    actions.setMapBackground(map);
  };

  const handleOnGeolocate = (x: number, y: number) => {
    const newViewState = {
      longitude: x,
      latitude: y,
      zoom: viewState.zoom,
      pitch: 0,
      bearing: 0,
      transitionDuration: 3000,
      transitionInterpolator: new FlyToInterpolator(),
    };
    actions.setViewState(newViewState);
    actions.setMapUserPosition({ lon: x, lat: y });
  };

  const onSuccess = useCallback(
    (migrationType: string, element: any, newItem?: any) => {
      app.setAppState({
        snackbar: {
          ...app.snackbar,
          alerts: [
            {
              message:
                element === "location"
                  ? i18next.t(
                      `components.LocationForm.success.${migrationType}.${newItem?.status?.status}`
                    )
                  : i18next.t(
                      `components.InterventionForm.success.${migrationType}`
                    ),
              severity: "success",
            },
          ],
        },
      });
      if (element === "location") {
        fetchLocation({ variables: { _eq: newItem.id } });
        actions.setMapPanelOpen(false);
      }
    },
    [
      app.snackbar.alerts,
      fetchLocation,
      actions.fetchMapLocations,
      mapPanelOpen,
    ]
  );

  const createTree = async (
    data: CreateTreeMutationVariables,
    locationId: string
  ) => {
    createTreeMutation({
      variables: {
        ...data,
        location_id: locationId,
        data_entry_user_id: currentUserId,
      },
      onCompleted: (res) => {
        const treeId = res?.insert_tree_one?.id;
        const serialNumber = data?.serial_number;
        if (treeId && serialNumber) {
          const oldPath = `trees/${serialNumber}/latest.png`;
          const newPath = `trees/${treeId}/latest.png`;
          const bucketName = publicRuntimeConfig.DEFAULT_STORAGE_BUCKET_NAME;

          checkIfImageExists(
            `${publicRuntimeConfig.NEXT_PUBLIC_FRONT_URL}/${publicRuntimeConfig.DEFAULT_STORAGE_BUCKET_NAME}/${oldPath}`,
            async (exists) => {
              if (exists) {
                try {
                  await moveMinioFile(
                    oldPath,
                    newPath,
                    bucketName,
                    currentUserToken
                  );
                  console.log("File moved successfully");
                } catch (error) {
                  console.error("Error moving file: ", error);
                }
              } else {
                console.log("Old image does not exist in Minio");
              }
            }
          );
        }
      },
    });
  };

  const handleOnZoom = (direction: Direction) => {
    actions.setViewState({
      ...viewState,
      zoom:
        direction === Direction.Out ? viewState.zoom - 1 : viewState.zoom + 1,
    });
  };

  const handleCloseCardInfos = () => {
    setOpenCardModal(false);
    setCardPreview(false);
  };

  const handleCreateLocation = async (data: Location) => {
    const location = data.location;
    const coordinates = [location.longitude, location.latitude];

    createLocationMutation({
      variables: {
        coords: {
          type: "Point",
          coordinates,
        },
        address: location?.address || "",
        foot_type: location?.foot_type,
        urban_site_id: location?.urban_site_id || null,
        plantation_type: location?.plantation_type,
        sidewalk_type: location?.sidewalk_type,
        landscape_type: location?.landscape_type,
        is_protected: location?.is_protected,
        id_status: location?.status,
        organization_id: app.organization?.id,
        data_entry_user_id: currentUserId,
      },
      onCompleted: async (response) => {
        if (Object.values(location.boundaries).some((value) => value !== "")) {
          const hasBoundaries = Object.values(location.boundaries).some(
            (value) => value !== ""
          );
          if (hasBoundaries) {
            const boundaryIds = Object.values(location.boundaries).filter(
              (id) => id !== ""
            );

            Promise.all(
              boundaryIds.map(async (boundaryId) => {
                await insertlocationBoundaries({
                  variables: {
                    location_id: response?.insert_location_one?.id,
                    boundary_id: boundaryId,
                  },
                });
              })
            );
          }
        }
        if (isPresent(location.vegetated_area_id)) {
          await insertlocationVegetatedArea({
            variables: {
              location_id: response?.insert_location_one?.id,
              vegetated_area_id: location.vegetated_area_id,
            },
            onError: async (response) => {
              const error = await response.message;
              console.log(error);
            },
          });
        }
        const hasUrbanConstraints =
          location &&
          location.urban_constraint &&
          Object.keys(location?.urban_constraint).length > 0;
        if (hasUrbanConstraints) {
          const urbanConstraints = Object.values(
            location?.urban_constraint
          ).filter((option: Urban_Constraint) => option?.id !== "");

          Promise.all(
            urbanConstraints.map(async (urbanConstraint: Urban_Constraint) => {
              await insertLocationUrbanConstraint({
                variables: {
                  location_id: response?.insert_location_one?.id,
                  urban_constraint_id: urbanConstraint.id,
                },
              });
            })
          );
        }

        if (response?.insert_location_one?.status?.status === "alive") {
          // @ts-ignore
          createTree(data?.tree, response?.insert_location_one?.id);
        }
        setForceReloadLayer(true);
        onSuccess("create", "location", response?.insert_location_one);
      },
      onError: async (response) => {
        const error = await response.message;
        console.log(error);
      },
    });
  };

  const isFeatureSelected = (currentFeature: Feature): boolean => {
    const locationId =
      currentFeature.properties.location_id ||
      currentFeature.properties.vegetated_area_id;
    return selectedLocationIds.has(locationId);
  };

  const handleOnViewStateChange = (viewState: any) => {
    const nextViewState: InitialViewState = viewState.viewState;
    actions.setViewState({
      ...nextViewState,
      longitude: nextViewState.longitude,
      latitude: nextViewState.latitude,
      zoom: nextViewState.zoom,
    });
  };
  const handleOnMapClick = async (info) => {
    if (editionMode) {
      if (mapActiveAction === "addLocation") {
        const [x, y] = info.coordinate;
        actions.setMapTempPoint({ lat: y, lon: x });
        actions.setMapPanelOpen(true);
      }
      return;
    }

    if (
      info?.layer === null &&
      mapPanelOpen &&
      !["addWorksiteGroup", "addPolygon"].includes(mapActiveAction as string)
    ) {
      actions.setMapPanelOpen(false);
      return;
    }

    if (mapActiveAction === "addWorksiteGroup" && mapSelectedLocations) {
      if (worksiteActiveStep === 0) {
        if (info?.object) {
          const { properties } = info.object;
          const existingIndex = mapSelectedLocations.findIndex(
            (location) => location.location_id === properties.location_id
          );

          if (existingIndex !== -1) {
            const updatedLocations = mapSelectedLocations.filter(
              (location) => location.location_id !== properties.location_id
            );
            actions.setMapSelectedLocations(updatedLocations);
          } else {
            actions.setMapSelectedLocations([
              ...mapSelectedLocations,
              properties,
            ]);
          }
        }
      }
    }

    if (info?.object?.properties) {
      const locationInfo = info.object.properties;
      // Check if the clicked layer is a point or a polygon
      if (info.layer?.id === "locations" && locationInfo.location_id) {
        const id = locationInfo.location_id;
        actions.setMapActiveLocationId(id);
        setOpenCardModal(true);
        setCardPreview(true);
        setInfos({ ...locationInfo });
        return;
      } else if (info.layer?.id === "polygons") {
        if (locationInfo.vegetated_area_id) {
          const id = locationInfo.vegetated_area_id;
          actions.setMapActiveLocationId(id);
          setOpenCardModal(true);
          setCardPreview(true);
          setInfos({ ...locationInfo });
          return;
        } else if (
          locationInfo.boundary_id &&
          !query.hasOwnProperty("edit") &&
          mapActiveAction !== "addPolygon"
        ) {
          replace(
            `/map?activePolygon=${locationInfo.boundary_id}&type=boundary`
          );
          return;
        }
      }
    }
  };

  useEffect(() => {
    if (query.activePolygon) {
      query.type === "boundary"
        ? fetchBoundary({
            variables: {
              id: query.activePolygon,
            },
          })
        : fetchVegetatedArea({
            variables: {
              id: query.activePolygon,
            },
          });
    }
  }, [query.activePolygon]);
  useEffect(() => {
    if (!query.activeWorksite) return;

    fetchWorksite({
      variables: { id: query.activeWorksite },
      onCompleted: () => {
        fetchInterventionsWorksite({
          variables: { id: query.activeWorksite },
          onCompleted: (response) => {
            if (!response?.worksite?.interventions?.length) return;

            // Filter interventions based on query.duplicate
            const interventions = response.worksite.interventions;
            const filteredInterventions = query.duplicate
              ? interventions.filter(
                  (intervention) =>
                    !["grubbing", "planting", "felling"].includes(
                      intervention.intervention_type.slug
                    )
                ) // Filter out interventions with specific slugs in duplicate mode
              : interventions.filter(
                  (intervention) => intervention.realization_date === null
                ); // Filter interventions where realization_date is null

            const firstIntervention = filteredInterventions[0];
            if (firstIntervention) {
              const coordinates =
                firstIntervention.location?.coords.coordinates ||
                firstIntervention.tree?.location?.coords.coordinates;

              if (coordinates) {
                actions.setViewState({
                  ...viewState,
                  longitude: coordinates[0],
                  latitude: coordinates[1],
                });
              }
            }
          },
        });
      },
    });
  }, [query.activeWorksite]);

  /**
   * Method to set polygon common data
   */
  const setPolygonComData = (
    data,
    type,
    setPolygonCommonData,
    setPolygonFeatures,
    setPolygonObjectType,
    actions
  ) => {
    setPolygonCommonData({
      geometry: data.coords,
      locationsInside: data.locations?.map((b) => b.location_id),
      vegetatedAreasInside: data.vegetated_areas?.map(
        (v) => v.vegetated_area_id
      ),
      automaticAssociation: data.locations?.length > 0,
    });

    setPolygonObjectType(type);

    const polygonFeature = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: {
            type: data.coords.type,
            coordinates: data.coords.coordinates,
          },
          properties: {
            id: data.id,
            name: data.name,
          },
        },
      ],
    };
    setPolygonFeatures(polygonFeature);
    actions.setMapPanelOpen(true);
    actions.setMapActiveAction("addPolygon");
  };

  useEffect(() => {
    if (
      boundary?.boundary &&
      (boundary?.boundaries_locations || boundary.boundaries_vegetated_areas)
    ) {
      setPolygonComData(
        {
          coords: boundary.boundary.coords,
          locations: boundary.boundaries_locations,
          vegetated_areas: boundary.boundaries_vegetated_areas,
          id: boundary.boundary.id,
          name: boundary.boundary.name,
        },
        boundary.boundary.type,
        setPolygonCommonData,
        setPolygonFeatures,
        setPolygonObjectType,
        actions
      );

      setBoundaryFormData({
        name: boundary.boundary.name,
      });
    } else if (vegetatedArea?.vegetated_area && vegetatedArea?.locations) {
      setPolygonComData(
        {
          coords: vegetatedArea.vegetated_area.coords,
          locations: vegetatedArea.locations,
          id: vegetatedArea.vegetated_area.id,
        },
        "vegetated_area",
        setPolygonCommonData,
        setPolygonFeatures,
        setPolygonObjectType,
        actions
      );
    }
  }, [boundary, vegetatedArea]);

  const debouncedHandleOnMapClick = _.debounce(handleOnMapClick, 100);

  useEffect(() => {
    if (mapActiveAction === "addLocation") {
      fetchBoundaries();
      fetchLocationFormData();
    }
  }, [mapActiveAction]);

  useEffect(() => {
    actions.setMapStyle(
      mapBackground !== MapBackground.MapDark
        ? `/mapStyles/${mapBackground}_${app.appMode}.json`
        : `/mapStyles/${mapBackground}_dark.json`
    );
  }, [app.appMode, mapBackground]);

  useEffect(() => {
    if (mapLocations && mapLocations?.length > 0 && !firstLoad) {
      const initialCoords = app.organization?.coords?.coordinates;
      if (initialCoords) {
        const viewState = {
          latitude: initialCoords[1],
          longitude: initialCoords[0],
          zoom: 14,
          bearing: 0,
          pitch: 0,
        };
        actions.setViewState(viewState);
      }
    }
    if (mapLocations?.length === 0) {
      setOpenModal(true);
      actions.setMapEditorState(initialMapEditorState);
    }
    actions.fetchLocationStatuses(fetchLocationsStatuses);
  }, [mapLocations, firstLoad]);

  useEffect(() => {
    if (mapActiveAction !== "addWorksiteGroup") {
      setMapSelectedLocations([]);
    }
  }, [mapActiveAction, setMapSelectedLocations]);

  const createTempPointLayer = () => {
    if (mapTempPoint) {
      return TempLayer({
        mapTempPoint: mapTempPoint,
        position: mapUserPosition,
        setMapTempPoint: actions.setMapTempPoint,
      });
    }
    return null;
  };

  const createWorksiteLayer = () => {
    if (
      mapActiveAction === "addWorksiteGroup" &&
      mapSelectedLocations &&
      worksiteActiveStep === 1
    ) {
      return WorksiteLayer({
        mapSelectedLocations: mapSelectedLocations,
        interventions: worksiteInterventions,
        setAssociatedLocations: setAssociatedLocations,
      });
    }
    return null;
  };

  // Function to create layers
  const createLayers = (
    generateLayerDataUrl,
    forceReloadLayer,
    handleOnMapClick,
    actions,
    queryStringLocations,
    isFeatureSelected,
    mapBackground,
    selectedLocationIds,
    mapActiveLocationId,
    worksiteHoveredId,
    setHoveredSelectedLocationId,
    queryStringPolygons,
    t
  ) => {
    return {
      locationsLayer: createLocationsLayer(
        generateLayerDataUrl,
        forceReloadLayer,
        handleOnMapClick,
        actions,
        queryStringLocations,
        isFeatureSelected,
        mapBackground,
        selectedLocationIds,
        mapActiveLocationId,
        worksiteHoveredId,
        setHoveredSelectedLocationId
      ),
      boundariesLayer: createPolygonsLayer(
        generateLayerDataUrl,
        forceReloadLayer,
        handleOnMapClick,
        actions,
        queryStringPolygons,
        t,
        selectedLocationIds,
        isFeatureSelected,
        worksiteHoveredId,
        setHoveredSelectedLocationId
      ),
      tempPointLayer: createTempPointLayer(),
      worksiteTempLayer: createWorksiteLayer(),
    };
  };

  useEffect(() => {
    if (forceReloadLayer) {
      generateLayerDataUrl(
        "filter_locations",
        forceReloadLayer,
        queryStringLocations,
        ""
      );

      generateLayerDataUrl(
        "filter_polygons",
        forceReloadLayer,
        "",
        queryStringPolygons
      );
    }
  }, [forceReloadLayer]);

  const updateLayers = (
    layers,
    actions,
    isSelectionLayerActive,
    mapLayers,
    mapActiveAction,
    worksiteActiveStep,
    selectionLayer,
    polygonLayer
  ) => {
    const updatedLayers = [
      layers.boundariesLayer,
      layers.tempPointLayer,
      layers.locationsLayer,
      isSelectionLayerActive && polygonLayer,
      layers.worksiteTempLayer,
    ].filter(Boolean);

    actions.setMapLayers(updatedLayers);

    if (isSelectionLayerActive && mapLayers) {
      if (mapActiveAction === "addWorksiteGroup") {
        removeLayer("polygon-layer");

        if (worksiteActiveStep === 0) {
          addLayer(selectionLayer);
        } else {
          removeLayer("selection");
          addLayer(layers.worksiteTempLayer);
        }
      } else {
        addLayer(polygonLayer);
      }
    } else {
      removeLayer("selection");
    }
  };

  const fetchPolygonData = (
    polygonFeatures,
    setPolygonCommonData,
    getLocationsInsidePolygon,
    polygon,
    getPolygonArea
  ) => {
    if (
      polygonFeatures?.features[0]?.geometry?.coordinates &&
      polygonFeatures?.features[0]?.geometry
    ) {
      const area = getPolygonArea(polygonFeatures.features[0].geometry);

      getLocationsInsidePolygon({
        variables: {
          polygon: polygonFeatures.features[0].geometry,
        },
      })
        .then((response) => {
          const locationsInside =
            response?.data?.location.map((l) => l.id) || [];
          const vegetatedAreasInside =
            response?.data?.vegetated_area.map((v) => v.id) || [];

          setPolygonCommonData({
            ...polygon,
            geometry: polygonFeatures.features[0].geometry,
            area: area,
            locationsInside: locationsInside,
            vegetatedAreasInside: vegetatedAreasInside,
          });
        })
        .catch((error) => {
          console.error("Error getting locations:", error);
        });
    }
  };

  useEffect(() => {
    const layers = createLayers(
      generateLayerDataUrl,
      forceReloadLayer,
      handleOnMapClick,
      actions,
      queryStringLocations,
      isFeatureSelected,
      mapBackground,
      selectedLocationIds,
      mapActiveLocationId,
      worksiteHoveredId,
      setHoveredSelectedLocationId,
      queryStringPolygons,
      t
    );

    updateLayers(
      layers,
      actions,
      isSelectionLayerActive,
      mapLayers,
      mapActiveAction,
      worksiteActiveStep,
      selectionLayer,
      polygonLayer
    );

    !worksiteHoveredId &&
      fetchPolygonData(
        polygonFeatures,
        setPolygonCommonData,
        getLocationsInsidePolygon,
        polygon,
        getPolygonArea
      );
  }, [
    filtersWithMartin,
    queryStringLocations,
    queryStringPolygons,
    editionMode,
    actions.setMapLayers,
    mapTempPoint,
    mapUserPosition,
    mapLocations,
    mapActiveLocationId,
    selectedLocationIds,
    mapFilteredLocations,
    activeLocationData,
    actions.setMapActiveAction,
    mapPanelOpen,
    mapActiveAction,
    fetchLocation,
    mapBackground,
    isSelectionLayerActive,
    polygonFeatures,
    worksiteHoveredId,
    worksiteActiveStep,
    worksiteInterventions,
  ]);

  const convertToQueryString = (data) => {
    return Object.entries(data)
      .map(([key, value]) => {
        if (Array.isArray(value)) {
          return `${key}=${value.join(",")}`;
        }
        return `${key}=${value}`;
      })
      .join("&");
  };

  useEffect(() => {
    const {
      geographic_boundaries_active,
      station_boundaries_active,
      vegetated_areas_active,
      vegetated_areas_ids,
      ...locationFilters
    } = filtersWithMartin;
    setQueryStringLocations(convertToQueryString(locationFilters));

    let boundaryQueryString = "";
    if (filtersWithMartin.geographic_boundaries_active) {
      boundaryQueryString += "geographic_boundaries_active=true&";
    }
    if (filtersWithMartin.station_boundaries_active) {
      boundaryQueryString += "station_boundaries_active=true&";
    }
    if (filtersWithMartin.vegetated_areas_active) {
      boundaryQueryString += "vegetated_areas_active=true&";
    }
    if (filtersWithMartin.vegetated_areas_ids) {
      boundaryQueryString += `vegetated_areas_ids=${filtersWithMartin?.vegetated_areas_ids}&`;
    }
    if (boundaryQueryString !== "") {
      boundaryQueryString = boundaryQueryString.slice(0, -1);
      setQueryStringPolygons(boundaryQueryString);
    } else {
      setQueryStringPolygons("");
    }
  }, [filtersWithMartin]);

  useEffect(() => {
    if (mapSelectedLocations) {
      // Update the set of selected location IDs
      const newSelectedLocationIds = new Set(
        mapSelectedLocations.map(
          (item) => item.location_id || item.vegetated_area_id
        )
      );
      setSelectedLocationIds(newSelectedLocationIds);
    }
  }, [mapSelectedLocations]);

  return (
    <Stack sx={{ height: "100%", position: "relative" }}>
      <MapBase
        handleOnViewStateChange={handleOnViewStateChange}
        initialViewState={viewState}
        mapStyle={mapStyle}
        layers={mapLayers}
        editionMode={editionMode}
        onClick={debouncedHandleOnMapClick}
      >
        <>
          <MapActions
            handleOnActionClick={handleOnActionClick}
            mapBackground={mapBackground}
            handleOnBaseMapChange={handleOnBaseMapChange}
            handleOnGeolocate={handleOnGeolocate}
            cardPreview={cardPreview}
            editionMode={editionMode}
            exitEditMode={exitEditMode}
            handleOnAddLocation={handleOnAddLocation}
            locationStatusList={mapStatuses!}
          />
          {isDesktop && <MapNavigation onZoom={handleOnZoom} />}
        </>
      </MapBase>
      {openModal && (
        <InitialModal
          open={openModal}
          handleClose={handleCloseModal}
          handleOnAddLocation={handleOnAddLocation}
        />
      )}
      {openCardModal && infos && (
        <CardInfos infos={infos} handleCloseCardInfos={handleCloseCardInfos} />
      )}
      <Panel
        mapLocationType={mapLocationType!}
        mapActiveAction={mapActiveAction!}
        mapPanelOpen={mapPanelOpen}
        handleDrawerClose={() => actions.setMapPanelOpen(false)}
        handleCancelProcess={handleCancelProcess}
        mapActiveLocation={activeLocationData?.location[0]}
        handleOnOpen={() => actions.setMapPanelOpen(true)}
      >
        <>
          {mapActiveAction === "filter" && (
            <>
              {Object.keys(filtersWithMartin).length > 0 && (
                <Button onClick={triggerFieldReset}>
                  {t("common.clearFilters")}
                </Button>
              )}
              <PanelFiltersWithMartin />
            </>
          )}
          {mapActiveAction === "addLocation" &&
            boundaries &&
            locationFormData && (
              <PanelLocation
                // @ts-ignores
                activeLocation={activeLocationData?.location[0]}
                tempPoint={mapTempPoint}
                locationType={mapLocationType!}
                onCreateLocation={handleCreateLocation}
                mapActiveAction={mapActiveAction}
                locationStatusList={mapStatuses!}
                boundaries={boundaries.boundary}
                locationFormData={locationFormData}
              />
            )}
          {mapActiveAction === "addWorksiteGroup" && (
            <WorksitePanelContent
              activeWorksite={worksite?.worksite}
              interventionsWorksiteData={interventionsDataWorksite?.worksite}
            />
          )}
          {mapActiveAction === "addPolygon" && <PolygonPanel />}
        </>
      </Panel>
    </Stack>
  );
};

export default MapEditor;
