from diagrams import Cluster, Diagram
from diagrams.c4 import Container, Database, Relationship, System, SystemBoundary


def create_diagram():
    with Diagram("Ecoteka PRO", show=False):
        dns = System(name="DNS", external=True)
        email = System(
            name="E-mail System",
            description="Internal Microsoft Exchange system.",
            external=True,
        )

        # Define the main system boundary
        with SystemBoundary("Docker network"):
            # Exposed Services Cluster
            with Cluster("Exposed Services"):
                traefik = Container(
                    name="traefik",
                    technology="Traefik",
                    description="Load Balancer",
                )
                frontend = Container(
                    name="frontend", technology="Next.js", description="User Interface"
                )
                keycloak = Container(
                    name="keycloak", technology="Keycloak", description="Authentication"
                )
                minio = Container(
                    name="minio", technology="MinIO", description="Object Storage"
                )
                meilisearch = Database(
                    name="meilisearch",
                    technology="MeiliSearch",
                    description="Search Engine",
                )
                graphql_engine = Container(
                    name="graphql_engine",
                    technology="Hasura",
                    description="GraphQL API",
                )
                api = Container(
                    name="api", technology="FastAPI", description="Main API"
                )
                martin = Container(
                    name="martin", technology="Martin", description="Geospatial Data"
                )

            # Internal Services Cluster
            with Cluster("Internal Services"):
                db = Database(
                    name="db", technology="PostgreSQL 14", description="Database"
                )
                valkey = Database(
                    name="valkey", technology="Valkey", description="Validation Service"
                )
                api_internal = Container(
                    name="api_internal",
                    technology="FastAPI",
                    description="Internal API",
                )
                worker = Container(
                    name="worker", technology="Celery", description="Background Tasks"
                )

        # Define relationships
        define_relationships(
            dns,
            traefik,
            email,
            db,
            frontend,
            keycloak,
            minio,
            meilisearch,
            graphql_engine,
            api,
            api_internal,
            worker,
            valkey,
            martin,
        )


def define_relationships(
    dns,
    traefik,
    email,
    db,
    frontend,
    keycloak,
    minio,
    meilisearch,
    graphql_engine,
    api,
    api_internal,
    worker,
    valkey,
    martin,
):
    # DNS and Traefik
    dns >> Relationship("Routes traffic [HTTPS]") >> traefik

    # Load Balancing to Exposed Services
    traefik >> Relationship("LB") >> frontend
    traefik >> Relationship("LB") >> keycloak
    traefik >> Relationship("LB") >> minio

    # Keycloak interactions
    keycloak >> Relationship("Authentication") >> db
    keycloak >> Relationship("Send notifications") >> email

    # GraphQL interactions
    graphql_engine >> Relationship("Queries data") >> db

    # API interactions
    api >> Relationship("Authenticates with") >> keycloak
    api >> Relationship("Reads/Writes") >> db
    api >> Relationship("Delegates tasks") >> worker

    # Internal API interactions
    api_internal >> Relationship("Authenticates with") >> keycloak
    api_internal >> Relationship("Calls GraphQL") >> graphql_engine

    # Worker interactions
    worker >> Relationship("Validates with") >> valkey

    # Frontend interactions
    frontend >> Relationship("Fetches data from") >> martin
    frontend >> Relationship("Authenticates with") >> keycloak
    frontend >> Relationship("Interacts with") >> minio
    frontend >> Relationship("Uses API") >> graphql_engine
    frontend >> Relationship("") >> meilisearch

    # Martin interactions
    martin >> Relationship("Stores geospatial data in") >> db


# Call the function to create the diagram
create_diagram()
