# Getting started with Python notebooks

## Managing Python versions and environments with pyenv on WSL

1. Install `pyenv` on WSL (Ubuntu 18.04 LTS)

```bash
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile
exec "$SHELL"
```

2. Install required librairies to work with Python

```bash
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev
```

3. Install Python

```bash
# Ecoteka uses Python 3.10 for its REST microservices
pyenv install 3.10.12
pyenv global 3.10.12
```

4. Create an environment to install Ecoteka depedencies (see `requirements.txt`)

```bash
pyenv virtualenv 3.10.12 ecoteka
pyenv activate ecoteka
```

> NB: Read more about managing python versions and environments with pyenv on this [nice guide on Real Python](https://realpython.com/intro-to-pyenv/)

## Working with Python Jupyter notebook locally with VS Code

1. Install Ecoteka Python dependencies

```bash
# if needed update pip
# python3.10 -m pip install --upgrade pip
pip install boto3 gql[all] wikidata psycopg2-binary meilisearch SQLAlchemy pandas geopandas fiona openpyxl httpx requests ibis-framework[postgres]
```

2. Open Python Jupyter notebook in VS Code

3. Select a Python environment to execute code cells included in your notebook (it should be ecoteka or any Python env suited for the task)

```bash
# You probably will be asked to install jupyter kernel in your environement, follow the instructions given by VS Code
pip install ipykernel

# this should be done with the proper Python environment activated in your terminal.
```

> Read more about [using Python in VS Code](https://code.visualstudio.com/docs/datascience/jupyter-notebooks)

4. Happy data science coding !! 👨‍💻 👨‍🔬
