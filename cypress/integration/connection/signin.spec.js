/// <reference types="cypress" />

describe("Sign In", () => {
  beforeEach(function () {
    cy.fixture("users").then(function (data) {
      cy.visit("/");
      cy.get("#username").as("username");
      cy.get("#password").as("password");
      cy.get("#kc-login").as("connection");
      this.data = data;
    });
  });

  it("need to be authenticated", function () {
    cy.get("@connection").should("exist");
    cy.login(this.data.admin.username, this.data.admin.password);
    cy.visit("/");
    cy.get("@connection").should("not.exist");
    cy.get("[data-test=index-page]").should("exist");
  });

  it("success login", function () {
    cy.get("@username").invoke("attr", "type").should("eq", "text");
    cy.get("@password").invoke("attr", "type").should("eq", "password");
    cy.login(this.data.admin.username, this.data.admin.password);
    cy.visit("/");
    cy.get("@connection").should("not.exist");
    cy.get("[data-test=index-page]").should("exist");
  });

  it("wrong password", function () {
    cy.get("@username").type(this.data.admin.username);
    cy.get("@password").type("wrong password").type("{enter}");
    cy.get("@password").should("have.attr", "aria-invalid", "true");
    cy.get(".kc-feedback-text").should("be.visible");
    cy.visit("/");
    cy.get("@connection").should("exist");
  });

  it("wrong username", function () {
    cy.get("@username").type("wrong username");
    cy.get("@password").type(this.data.admin.password).type("{enter}");
    cy.get("@username").should("have.attr", "aria-invalid", "true");
    cy.get(".kc-feedback-text").should("be.visible");
    cy.visit("/");
    cy.get("@connection").should("exist");
  });

  it("empty password", function () {
    cy.get("@username").type(this.data.admin.username);
    cy.get("@password").type("{enter}");
    cy.get("@password").should("have.attr", "aria-invalid", "true");
    cy.get(".kc-feedback-text").should("be.visible");
    cy.visit("/");
    cy.get("@connection").should("exist");
  });

  it("empty username", function () {
    cy.get("@password").type(this.data.admin.password).type("{enter}");
    cy.get("@username").should("have.attr", "aria-invalid", "true");
    cy.get(".kc-feedback-text").should("be.visible");
    cy.visit("/");
    cy.get("@connection").should("exist");
  });

  it("empty fields", function () {
    cy.get("@password").type("{enter}");
    cy.get("@username").should("have.attr", "aria-invalid", "true");
    cy.get("@password").should("have.attr", "aria-invalid", "true");
    cy.get(".kc-feedback-text").should("be.visible");
    cy.visit("/");
    cy.get("@connection").should("exist");
  });
});
