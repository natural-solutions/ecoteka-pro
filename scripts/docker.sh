#!/usr/bin/env bash

set -e

SCRIPT_PATH="$(dirname -- "${BASH_SOURCE[0]}")"
LOG_FILE="$SCRIPT_PATH/docker_run.log"

log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1" 
}

# Load environment variables
source "$SCRIPT_PATH/../docker/.env"

# Pre-pre-flight? 🤷
if [[ -n "$MSYSTEM" ]]; then
    log "Seems like you are using an MSYS2-based system (such as Git Bash) which is not supported. Please use WSL instead."
    exit 1
fi

# Ensure required environment variables are set
if [ -z "$PROJECT" ] || [ -z "$ENV" ]; then
    log "Required environment variables PROJECT and ENV are not set. Exiting."
    exit 1
fi

DOCKER_VERSION=$(docker version --format '{{.Server.Version}}')
VERSION_PARTS=(${DOCKER_VERSION//./ })

if ((${VERSION_PARTS[0]} < 24 || (${VERSION_PARTS[0]} == 0 && ${VERSION_PARTS[1]} < 0))); then
    COMPOSE_COMMAND='docker-compose'
else
    COMPOSE_COMMAND='docker compose'
fi

log "Using Docker Compose command: $COMPOSE_COMMAND"

if [ "$ENV" == "production" ]; then
    log "Starting Docker Compose in production mode..."
    $COMPOSE_COMMAND --project-name=${PROJECT} -f "$SCRIPT_PATH/../docker/docker-compose.yml" -f "$SCRIPT_PATH/../docker/docker-compose.prod.yml" -f "$SCRIPT_PATH/../docker/docker-compose.backup.yml" "$@"
elif [ "$ENV" == "proxy" ]; then
    log "Starting Docker Compose in proxy mode..."
    $COMPOSE_COMMAND --project-name=${PROJECT} -f "$SCRIPT_PATH/../docker/docker-compose.yml" -f "$SCRIPT_PATH/../docker/docker-compose.proxy.yml" -f "$SCRIPT_PATH/../docker/docker-compose.backup.yml" "$@"
else
    log "Starting Docker Compose in development mode..."
    $COMPOSE_COMMAND --project-name=${PROJECT} --project-directory="$SCRIPT_PATH/../docker" -f "$SCRIPT_PATH/../docker/docker-compose.yml" -f "$SCRIPT_PATH/../docker/docker-compose.override.yml" "$@"
fi

log "Docker Compose command completed."
