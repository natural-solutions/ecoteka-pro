#!/bin/bash

set -e

source ./docker/.env

./scripts/clean.sh

npm i -g yarn

yarn
(cd frontend && yarn install)

./scripts/docker.sh up --build -d

sudo ./scripts/hostctl add domains "${PROJECT}" "${DOMAIN}"

docker login registry.gitlab.com
