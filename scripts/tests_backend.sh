set -e

wait_for_it() {
  echo 'Waiting for backend to become available...'
  while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' localhost:8000/docs)" != "200" ]]; do   
    sleep 2; 
  done
  echo 'Backend is available'
}

#./docker.sh build --no-cache
./scripts/docker.sh up -d 
wait_for_it
./scripts/docker.sh exec api python -m pytest tests --cov-report html:/app/app/htmlcov --cov=app
./scripts/docker.sh down
