#!/usr/bin/env bash

set -e

source ./docker/.env

find . -name "node_modules" -type d -prune -print -exec rm -rf '{}' \;
find . -name ".next" -type d -prune -print -exec rm -rf '{}' \;
find . -name ".swc" -type d -prune -print -exec rm -rf '{}' \;

./scripts/docker.sh down -v

sudo ./scripts/hostctl -q remove domains "${PROJECT}" "${DOMAIN}" || true
