# Architecture Overview

The application is fully dockerized, with each service encapsulated in a separate container.

## Technical Stack

### Frontend

- **Next.js**: React framework for managing the user interface.
- **Zustand**: Lightweight and performant state management library used to manage the application's store.
- **Deck.gl & React Map GL**: Used for displaying and interacting with maps in the application, allowing manipulation of geospatial data and displaying complex maps.

### Backend

- **Python** + **FastAPI**

### Database

- **PostgreSQL/PostGIS**
- **Hasura**: GraphQL engine that automatically generates APIs from a PostgreSQL database, simplifying data access via GraphQL.

### Complementary Services

- **Worker** (Celery: execution of background tasks/asynchronous tasks, e.g., data export, import, etc.)
- **Valkey**: Broker for storing and transmitting tasks to Celery.
- **MeiliSearch** (Fast search engine).
- **Martin** (Vector tiling for geospatial data).
- **Keycloak** (Identity and access management).

### Orchestration and Management

- **Docker**: Containerization of services.
- **Traefik**: Reverse proxy and load balancer to manage traffic routing to the services.

## Details of Each Docker Container

### 1. Frontend

- **Technologies**: Next.js (React framework)
- **Role**: The frontend manages the user interface and communicates with various APIs to retrieve data and interact with backend services.

### 2. GraphQL Engine (Hasura)

- **Description**: Hasura is an open-source GraphQL engine that automatically generates GraphQL APIs from a PostgreSQL database.
- **Role**: It simplifies data access by generating GraphQL queries with minimal configuration, facilitating interaction with the database.

### 3. Internal API

- **Role**: This internal API centralizes server-side processing, particularly for features that do not go through GraphQL or require specific processing.

### 4. External API

- **Role**: The external API accessible via /api/rest/docs, allowing requests to be made through the FastAPI interface.

  (This will likely evolve to have only one API container in the future.)

### 5. MeiliSearch

- **Description**: MeiliSearch is a fast and easy-to-integrate open-source search engine.
- **Role**: Used for searching taxons.

### 6. Worker

- **Role**: The worker handles background and asynchronous tasks (data import/export, etc.). This architecture offloads heavy tasks from the backend, improving the application's responsiveness.

### 7. Martin

- **Description**: Martin is an open-source vector tile server designed to work with PostgreSQL/PostGIS. It transforms data into vector tiles. Martin creates MVT vector tiles from any PostGIS table or view.
- **Role**: A vector tiling solution for managing and displaying geospatial data. Martin slices geographical data into small vector tiles, optimizing map rendering and managing large data volumes.

### 8. Keycloak

- **Description**: Keycloak is an open-source solution for identity and access management.
- **Role**: It centralizes authentication and role management within the application.

### 9. MinIO

- **Description**: MinIO is an object storage solution compatible with the AWS S3 API.
- **Role**: It is used to store application files (photos).

### 10. Database (PostgreSQL/PostGIS)

- **Description**: Relational database enriched with PostGIS to manipulate spatial data (points, polygons, and their geographical relationships).
- **Role**: It stores the application's data.

### 11. Traefik

- **Description**: Traefik is an open-source reverse proxy and load balancer.
- **Role**: It manages traffic routing and load balancing between the various services in the application.

### 12. Valkey

- **Description**: Valkey is an in-memory database used as a cache.
- **Role**: It speeds up data access by temporarily storing frequently accessed information (used here to manage task queues executed by Celery).

---

## Architecture Summary

| **Component**                                     | **Description**                                                           |
| ------------------------------------------------- | ------------------------------------------------------------------------- |
| **Frontend (Next.js)**                            | User interface, server-side rendering management for SEO and performance. |
| **Backend (Python) with Internal & External API** | Handles complex requests and specific tasks.                              |
| **GraphQL (Hasura)**                              | GraphQL API for data management.                                          |
| **MeiliSearch**                                   | Fast and intuitive search engine.                                         |
| **Worker**                                        | Executes asynchronous background tasks.                                   |
| **Martin**                                        | Manages geospatial data.                                                  |
| **Keycloak**                                      | Identity management and access security.                                  |
| **MinIO**                                         | Object storage (images, documents, etc.).                                 |
| **Database (PostgreSQL/PostGIS)**                 | Main storage for structured data.                                         |
| **Traefik**                                       | Routing and load balancing between services.                              |
| **Valkey**                                        | Cache to speed up data access and improve performance.                    |

---
