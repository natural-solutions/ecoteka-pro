<div id="readme-top"></div>
<img src="frontend/public/logo.svg" alt="logo" />

# Ecoteka is a web and mobile application dedicated to the management of green heritage and the renaturation of cities.

## This project is partly Open Source and includes advanced modules under commercial license. 🌳

## Developed by [Natural Solutions](https://www.natural-solutions.eu/)

🔍 More infos [here](https://www.natural-solutions.eu/ecoteka)

English website version: https://www.natural-solutions.world/

<!-- PROJECT SHIELDS -->

[![Contributors][contributors-shield]][contributors-url]
[![Issues][issues-shield]][issues-url]
[![license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT -->
<div>
  <p align="center">
    <a href="https://www.natural-solutions.eu/ecoteka">View Demo</a>
    ·
    <a href="https://gitlab.com/natural-solutions/ecoteka-pro/-/issues/new?issuable_template=incident&issue%5Bissue_type%5D=incident">Report Bug</a>
    ·
    <a href="https://gitlab.com/natural-solutions/ecoteka-pro/-/issues/new">Request Feature</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">Description</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <ul>
        <li><a href="#roles-and-permissions">Roles and permissions</a></li>
      </ul>
    <li><a href="#license">License</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>

  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## Description

Ecoteka offers an innovative platform for ecological management. It is designed to be extensible with additional modules that provide advanced features.

<div align="right">(<a href="#readme-top">back to top</a>)</div>

### Built With

- [![Next][Next.js]][Next-url]
- [![React][React.js]][React-url]
- [![Python][Python]][Python-url]
- [![PostgreSQL][PostgreSQL]][PostgreSQL-url]

<div align="right">(<a href="#readme-top">back to top</a>)</div>

<!-- GETTING STARTED -->

## Getting Started

### Prerequisites

- Docker 19.03.6+
- Compose 1.28.0+
- 2 CPU Cores
- 4 GB RAM
- 20 GB Free Disk Space

### Installation

#### Install environment for development

You need to login to the gitlab registry the first time in order to download the images. For that, you have to execute the following command:

```console
docker login registry.gitlab.com
```

#### Dev environmemt with docker

```bash
./scripts/docker.sh pull
./scripts/docker.sh build
./scripts/docker.sh up -d (or npm run d:up)
```

Then, go to api/rest/docs, authenticate and call admin endpoints:

```
/setup : create your organization with a wikidata code (for page value)
/setup_storage : create a default bucket on minio for file uploads
/seed_taxa : seed 'taxon' table on database
/index_taxa : create taxa index on meilisearch service
```

If you need to remove all your containers and volumes :

```bash
npm run d:down:all
```

#### Run integration tests with cypress

- You need to install modules:

        yarn
        or
        npm install

- In WSL2, you need to have dependencies and a x-server installed. Check [the installation docs](https://docs.cypress.io/guides/getting-started/installing-cypress)
- Once x-server is launched run
  npx cypress open
  or
  yarn run cypress open

The front page should be visible at

    http://localhost:8888/

#### Running Docker and accessing the worker container

To run the Docker container and access the worker container, execute the following command:

```bash
./scripts/docker.sh exec worker bash
```

This command will start the worker container and open up a Bash terminal session inside it. From here, you can execute commands inside the container.

#### Running a Celery task

To run a Celery task, execute the following command:

```bash
celery -A workers.run call workers.imports.tasks.toto
```

This command will call the toto task defined in the workers.imports.tasks module using the Celery application defined in the workers.run module. The task will be executed by the Celery worker running inside the container.

<div align="right">(<a href="#readme-top">back to top</a>)</div>

<!-- USAGE -->

## Usage

Once the application is running, open your browser and go to http://localhost:3000 to use Ecoteka.

To use the private modules, please follow the instructions provided with each module after purchasing a commercial license.

### Roles and permissions

The application is designed with a role-based access control system, defining three primary roles, each with specific responsibilities and access levels:

1. **Admin (Administrator Manager)**  
   Has full access to all features including user management.

2. **Editor (Field Agent)**  
   Can perform almost all actions, except for user management, creating or deleting worksites and geographic boundaries.

3. **Reader (Dashboard & Map Viewer)**  
   Has read-only access to the dashboard and map for data consultation and analysis.

However, all users (including readers) can edit their personal account information, such as name, email, and password.

## Permission Table

| Permission                                   | Admin |             Editor              | Reader |
| -------------------------------------------- | :---: | :-----------------------------: | :----: |
| **User Management**                          |       |                                 |        |
| Manage users                                 |  ✅   |               ❌                |   ❌   |
| Edit user account (name, email, password)    |  ✅   |               ✅                |   ✅   |
| **Dashboard and Map Access**                 |       |                                 |        |
| Access dashboard                             |  ✅   |               ✅                |   ✅   |
| Access map and filters                       |  ✅   |               ✅                |   ✅   |
| **Geospatial Objects**                       |       |                                 |        |
| Create/edit/delete geospatial objects        |  ✅   | ✅ (except geographic boundary) |   ❌   |
| **Worksite Management**                      |       |                                 |        |
| Create/delete worksites                      |  ✅   |               ❌                |   ❌   |
| Edit worksites                               |  ✅   |      ✅ (only description)      |   ❌   |
| Validate worksites                           |  ✅   |               ✅                |   ❌   |
| **Intervention Management**                  |       |                                 |        |
| Create/edit/delete intervention              |  ✅   |        ✅ (except cost)         |   ❌   |
| Validate interventions (individual/worksite) |  ✅   |               ✅                |   ❌   |
| **Diagnosis Management**                     |       |                                 |        |
| Create/edit/delete diagnosis                 |  ✅   |               ✅                |   ❌   |
| **Intervention Partner Management**          |       |                                 |        |
| Create intervention partner                  |  ✅   |               ❌                |   ❌   |
| **Data Management**                          |       |                                 |        |
| Export data                                  |  ✅   |               ✅                |   ❌   |
| **Geographic Boundary Management**           |       |                                 |        |
| Manage geographic boundary                   |  ✅   |               ❌                |   ❌   |

<div align="right">(<a href="#readme-top">back to top</a>)</div>

<!-- LICENSE -->

## License

Ecoteka is distributed under a dual license:

Base application: GPL v3

Private modules: Commercial license

For more details, refer to the LICENSE_EN.txt file.

### Commercial Use

If you wish to use Ecoteka to provide paid services to clients, you must obtain prior approval from us and remit 40% of the revenue generated by this commercial use. Contact us for more information.

<div align="right">(<a href="#readme-top">back to top</a>)</div>

<!-- DIAGRAM -->

## Diagram

<img src="./doc/ecoteka_pro.png" width="700px;" alt="diagram" />

<div align="right">(<a href="#readme-top">back to top</a>)</div>

<!-- CONTRIBUTING -->

## Contributing

Contributions to the open source part of Ecoteka are welcome. To contribute:

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## Top Contributors

<div style="display: flex; justify-content: center; align-items: center; gap: 30px;">
    <a href="https://gitlab.com/Jordane_NS">
      <img src="https://gitlab.com/uploads/-/system/user/avatar/10824146/avatar.png?width=800" width="70px;" alt="contributor"/>
    </a>
    <a href="https://gitlab.com/gaetan_ns">
      <img src="https://gitlab.com/uploads/-/system/user/avatar/7541553/avatar.png?width=800" width="70px;" alt="contributor" />
    </a>
<a href="https://gitlab.com/ktalbi">
  <img src="https://gitlab.com/uploads/-/system/user/avatar/3928850/avatar.png?width=800" width="70px;" alt="contributor" style="border-radius: 50%;" alt="KTALBI"/>
</a>

<a href="https://gitlab.com/IssamH">
  <img src="https://ui-avatars.com/api/?name=Issam-Hadjal&background=cedb20&color=fff&rounded=true" alt="IssamH"/>
</a>
</div>

<div align="right">(<a href="#readme-top">back to top</a>)</div>

<!-- CONTACT -->

## Contact

For any questions or inquiries about a commercial license, please contact us at:
commercial@natural-solutions.eu

<div align="right">(<a href="#readme-top">back to top</a>)</div>

Done in Marseille, on August 1st, 2024.

Natural Solutions, SAS <br/>
68 rue Sainte, 13001 Marseille, France <br/>
www.natural-solutions.eu <br/>
commercial@natural-solutions.eu <br/>
04 91 72 00 26

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/badge/contributors%20-4-44CC11?style=for-the-badge
[contributors-url]: https://gitlab.com/natural-solutions/ecoteka-pro/-/graphs/main?ref_type=heads
[issues-shield]: https://img.shields.io/badge/issues%20-open-DFB317?style=for-the-badge
[issues-url]: https://gitlab.com/natural-solutions/ecoteka-pro/-/issues
[license-shield]: https://img.shields.io/badge/Dual%20License%20-20B2AA?style=for-the-badge
[license-url]: https://gitlab.com/natural-solutions/ecoteka-pro/-/blob/main/LICENSE_EN.txt?ref_type=heads
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/company/natural-solutions?originalSubdomain=fr
[Next.js]: https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white
[Next-url]: https://nextjs.org/
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Python]: https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54
[Python-url]: https://www.python.org/
[PostgreSQL]: https://img.shields.io/badge/postgresql-4169e1?style=for-the-badge&logo=postgresql&logoColor=white
[PostgreSQL-url]: https://www.postgresql.org/
