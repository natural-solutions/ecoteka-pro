from fastapi import FastAPI, Depends
from core.keycloak import idp
from api.internal.keycloak import users
from api.internal import metrics
from api.internal import grids

from api.internal.map import filters
from api.internal.providers import urbasense
from api.internal.interventions import widget
from api.v1 import events

app = FastAPI()

app.include_router(events.router)
app.include_router(urbasense.router, prefix="/providers/urbasense", dependencies=[Depends(idp.user_auth_scheme)])
app.include_router(metrics.router, prefix="/metrics", dependencies=[Depends(idp.user_auth_scheme)])
app.include_router(filters.router, prefix="/map", dependencies=[Depends(idp.user_auth_scheme)])
app.include_router(widget.router, prefix="/widget", dependencies=[Depends(idp.user_auth_scheme)])
app.include_router(users.router, prefix="/users", dependencies=[Depends(idp.user_auth_scheme)])
