import pytest
from core.config import settings
from core.keycloak import idp
from faker import Faker
from fastapi.testclient import TestClient
from main import app
from workers.run import celery_app


def test_celery_app_config():
    assert celery_app.conf.broker_url.startswith("redis://")
    assert "workers.data_transfer" in celery_app.autodiscover_tasks(["workers.data_transfer"])


@pytest.fixture(scope="session")
def faker():
    return Faker()


@pytest.fixture(scope="session")
def client():
    with TestClient(app) as client:
        yield client


@pytest.fixture(scope="session")
def access_token():
    username = "admin"
    password = "password"
    response = idp.user_login(username, password)
    return response.access_token


@pytest.fixture(scope="session")
def celery_config():
    return {"broker_url": settings.worker_broker, "result_backend": settings.worker_backend}


@pytest.fixture(scope="session")
def celery_worker_parameters():
    return {
        "queues": ("ecoteka",),
        "concurrency": 1,
        "perform_ping_check": False,
    }
