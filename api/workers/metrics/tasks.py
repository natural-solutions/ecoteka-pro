import httpx
import json
import valkey
from celery import shared_task
from core.config import settings
from fastapi import HTTPException
from typing import List


valkey_client = valkey.Valkey(host='valkey', port=6379, db=0, decode_responses=True)

@shared_task
def get_urbasense_site_metrics():
    cache_key = "urbasense:site_metrics"
    cached_result = valkey_client.get(cache_key)
    if cached_result:
        return json.loads(cached_result)

    with httpx.Client() as client:
        headers = {'token': settings.urbasense_api_access_token}
        response = client.get(f"{settings.urbasense_api_base_url}/sujets", headers=headers)

        if response.status_code != 200:
            print(f"Erreur: {response.status_code}")
            return None

        subject_ids = [subject['id_suj'] for subject in response.json()['data']]
        metrics = get_urbasense_subjects_metrics(subject_ids)
        valkey_client.set(cache_key, json.dumps(metrics), ex=86400)

        return metrics

def get_urbasense_subjects_metrics(subject_ids: List[int]):
    with httpx.Client() as client:
        headers = {'token': settings.urbasense_api_access_token}
        payload = {"sujets": subject_ids}
        base_url = settings.urbasense_api_base_url

        responses = {}

        url_dendro = f"{base_url}/tableaudebord/dendro"
        r_dendro = client.post(url_dendro, headers=headers, json=payload, timeout=30)
        if r_dendro.status_code == 200:
            responses["dendro"] = r_dendro.json()
        else:
            return HTTPException(status_code=r_dendro.status_code, detail="Oupsy!")

        url_meteo = f"{base_url}/tableaudebord/meteo"
        r_meteo = client.post(url_meteo, headers=headers, json=payload, timeout=30)
        if r_meteo.status_code == 200:
            responses["meteo"] = r_meteo.json()
        else:
            return HTTPException(status_code=r_meteo.status_code, detail="Oupsy!")

        url_tensio = f"{base_url}/tableaudebord/tensio"
        r_tensio = client.post(url_tensio, headers=headers, json=payload, timeout=30)
        if r_tensio.status_code == 200:
            responses["tensio"] = r_tensio.json()
        else:
            return HTTPException(status_code=r_tensio.status_code, detail="Oupsy!")

        return responses
