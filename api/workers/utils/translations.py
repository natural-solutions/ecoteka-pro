STATUS = {
    "stump": "souche ou arbre mort",
    "empty": "libre",
    "alive": "arbre vivant",
}

HABIT = {
    "structured": "Structuré",
    "clump": "Cépée",
    "pollard": "Têtard",
    "free": "Libre",
    "semi_free": "Semi-libre",
    "curtain": "En rideau",
    "cat_head": "Tête de chat (tous les 1 an)"
}

SURFACE_TYPE = {
    "potential_area": "surface potentielle",
    "green_area": "espace vert",
    "hedge": "haie",
    "lawn": "pelouse",
    "flower_bed": "massif fleuri",
    "afforestation": "boisement",
}

FOOT_TYPE = {
    "vegetal": "Végétalisé",
    "mineral": "Minéralisé",
    "coated": "Enrobé",
    "border": "Bordure",
    "geotextile": "Géotextile",
    "grid": "Grille",
    "mineral_mulching": "Paillage minéral",
    "natural_mulching": "Paillage naturel",
    "sand": "Sablé",
    "bare_ground": "Terre nue",
    "pit": "Fosse"
}

URBAN_SITE = {
    "playground": "Aire de jeux",
    "boulevard": "Boulevard",
    "canal": "Canal",
    "recreation_center": "Centre de loisirs",
    "cemetery": "Cimetière",
    "school": "École",
    "sport_area": "Espace sportif",
    "esplanade": "Esplanade",
    "road_island": "Îlot routier",
    "garden": "Jardin",
    "mountain": "Massif",
    "media_library": "Médiathèque",
    "park": "Parc",
    "car_park": "Parking",
    "cycle_path": "Piste cyclable",
    "river_bank": "Rive de cours d'eau",
    "street": "Rue",
    "sidewalk": "Trottoir",
    "afforestation_area": "Zone de boisement",
    "parking_area": "Zone de stationnement"
}

INVENTORY_SOURCE = {
    "agent": "Agent de terrain",
    "aerial_lidar": "Algorithme LiDAR aérien",
    "terrestrial_lidar": "IA LiDAR terrestre",
    "ia_satellite": "IA Satellite",
    "ia_aerial_photos": "IA Photos aériennes",
    "other_technology": "Autre technologie de détection"
}

SIDEWALK_TYPE = {
    "mineral": "Minéralisé",
    "coated": "Enrobé",
    "sand": "Sablé",
    "vegetal": "Végétalisé"
}

PLANTATION_TYPE = {
    "pit": "Fosse",
    "open_ground": "Pleine terre",
    "container": "Pot"
}

LANDSCAPE_TYPE = {
    "alignment": "Alignement",
    "afforestation": "Boisement",
    "grouped_2_5": "Groupé (2 à 5)",
    "grouped_6_more": "Groupé (6 ou plus)",
    "isolated": "Isolé"
}

URBAN_CONSTRAINT = {
    "playground": "Aire de jeux",
    "other": "Autre contrainte",
    "camera": "Caméra de surveillance",
    "path": "Chemin",
    "fence": "Clôture",
    "facade": "Façade",
    "bus_line": "Ligne de bus",
    "furniture": "Mobilier",
    "wall": "Mur",
    "parking": "Parking",
    "common_part": "Partie mitoyenne",
    "private_part": "Partie privative",
    "aerial_network": "Réseaux aériens",
    "lamp_post": "Réverbère",
    "road": "Route",
    "traffic_sign": "Signalisation"
}

RESIDENTIAL_USAGE = {
    "picnic": "Pique-nique",
    "playground": "Aire de jeux pour les enfants",
    "rest": "Repos",
    "not_used": "Non utilisé"
}

POTENTIAL_AREA_STATE = {
    "parking": "Parking",
    "non_common_facade": "Façade non mitoyenne",
    "vegetalizable_facade": "Façade végétalisable",
    "other_surface": "Autre surface perméable non-utilisée"
}