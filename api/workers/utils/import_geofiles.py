import os
import zipfile
from datetime import datetime
from io import BytesIO

import fiona
import geopandas as gpd
import pandas as pd
from celery.utils.log import get_task_logger
from core.s3 import s3_client
from workers.utils.gql_mutations import insert_location, insert_tree, insert_location_urban_constraint, insert_location_boundary, insert_diagnosis

logger = get_task_logger(__name__)

def set_epsg_4326_geodataframe(df, crs):
    required_columns = ["longitude", "latitude"] # Mercator WGS84 (degdec ) => epsg:4326 / Lambert RGF93 => epsg:2154 

    logger.info(df.head())
    logger.info(f"============= {df.columns} ==================")
    for col in required_columns:
        if col not in df.columns:
            logger.error(f"longitude and latitude are missing in file")
            raise Exception(f"longitude and latitude are missing in file")
    gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.longitude, df.latitude))
    gdf = gdf.set_crs(crs)  
    if gdf.crs != 4326:
        gdf = gdf.to_crs(4326)
    return gdf

def format_serial_number(tree_data):
    if "scientific_name" in tree_data.keys():
        serial_number_prefix = tree_data["scientific_name"][:4]
    else:
        serial_number_prefix ="XXXX"
    # use microseconds to ensure uniqueness
    serial_number_time_suffix = datetime.now().strftime("%Y%m%d-%f")
    return '_'.join([serial_number_prefix, serial_number_time_suffix])


def process_geoseries(gdf, user_id, organization_id):
    # Existing locations set to keep track of latitude and longitude combinations
    existing_locations = set()
    mapping_dictionnary = {
        'circumference': ['circonference', 'av_diam'],
        'variety': ['variete'],
        'height': ['hauteur'],
        'is_tree_of_interest': ['remarquable'],
        'note': ['remarque', 'notes'],
        'habit': ['port_arbre', 'av_typ_por', 'habit'],
        'serial_number': ['matricule'],
        'estimated_date': ['date_estimee'],
        'watering': ['arrosage'],
        'plantation_date': ['date_plantation', 'date_plant'],
        'vernacular_name': ['nom_vernaculaire', 'nom_vernac'],
        'scientific_name': ['nom_scientifique', 'genre'],
        'urbasense_subject_id': ['urbasense_subject_id'],
    }
    for i, row in gdf.iterrows():
        try:
            geom = {
                "type": "Point",
                "coordinates": [row['geometry'].x, row['geometry'].y]
            }
            tree_data = {"data_entry_user_id": user_id}
            location_data = {"coords": geom, "data_entry_user_id": user_id, "organization_id": organization_id}
            
            # Check if 'adresse' column exists in the row
            if 'adresse' in row and isinstance(row['adresse'], str):
                location_data["address"] = row['adresse']
            location_data["plantation_type"] = str(row.get("plantation_type", None))  
            location_data["foot_type"] = str(row.get("foot_type", None)) 
            location_data["sidewalk_type"] = str(row.get("sidewalk_type", None))
            location_data["landscape_type"] = str(row.get("landscape_type", None))
            inventory_source_str = row.get("inventory_source", None)
            if inventory_source_str:    
                inventory_source_array = [inventory_source_str]
            else:
                inventory_source_array = []
            location_data["inventory_source"] = inventory_source_array
            protection1 = row.get("protection1", None)
            protection2 = row.get("protection2", None)
            if protection1 != '' or protection2 != '':
                location_data["is_protected"] = True
            else:
                location_data["is_protected"] = None


            if 'urban_site_id' in row and isinstance(row['urban_site_id'], str):
                location_data["urban_site_id"] = None if row['urban_site_id'] ==  "" else row["urban_site_id"]
            
            for key in mapping_dictionnary.keys():
                for col_name in mapping_dictionnary[key]:
                    if col_name in row:
                        if isinstance(row[col_name], str):
                            if key == 'is_tree_of_interest':
                                tree_data[key] = True if row[col_name].lower() == 'oui' else False
                            elif key == 'height':
                                tree_data[key] = float(row[col_name]) if row[col_name] != "" else None
                            elif key == 'circumference':
                                tree_data[key] = float(row[col_name]) if row[col_name] != "" else None
                            elif key == 'estimated_date':
                                tree_data[key] = True if row[col_name].lower() == 'oui' else False
                            elif key == 'watering':
                                tree_data[key] = True if row[col_name].lower() == 'oui' else False
                            elif key == 'urbasense_subject_id':
                                tree_data[key] = str(row[col_name])
                            elif key == 'scientific_name':
                                if 'nom_scientifique' in row:
                                    tree_data[key] = row['nom_scientifique']
                                else:
                                    genre = row['genre'] if 'genre' in row else ''
                                    espece = row['espece'] if 'espece' in row else ''
                                    tree_data[key] = f"{genre} {espece}" if genre and espece else ''                            
                            elif key == 'serial_number':
                                tree_data[key] = row[col_name] if row[col_name] != "" else format_serial_number(tree_data)
                            else:
                                    tree_data[key] = row[col_name]
                            break
            # Check if the location already exists in the set
            if tuple(geom["coordinates"]) in existing_locations:
                logger.warning(f"Skipping duplicate location: {geom['coordinates']}")
                continue
            # Add the location to the set
            existing_locations.add(tuple(geom["coordinates"]))

            try:
                result = insert_location(location_data)
                if result['insert_location_one'] is None:
                # Handle the case when insert_location_one result is None
                    print("Insert location result is None. Skipping further processing.")
                    continue
                else:
                    location_id = result['insert_location_one']['id']
                    urban_constraint_ids_col = ["urban_constraint_id_1", "urban_constraint_id_2"]
                    for urban_constraint_id_col in urban_constraint_ids_col:
                        if row[urban_constraint_id_col] != "":
                            urban_constraint_id = row[urban_constraint_id_col]
                            variables = {
                                "location_id": location_id,
                                "urban_constraint_id": urban_constraint_id
                            }
                            insert_location_urban_constraint(variables)
                    boundary_types = ["geographic_boundary_id", "station_boundary_id", "administrative_boundary_id"]
                    for boundary_type in boundary_types:
                        if row[boundary_type] != "":
                            boundary_id = row[boundary_type]
                            variables = {
                                "location_id": location_id,
                                "boundary_id": boundary_id
                            }
                            insert_location_boundary(variables)
                    tree_data["location_id"] = result['insert_location_one']['id']  
                    tree_result = insert_tree(tree_data)
                    tree_id = tree_result['insert_tree_one']['id']

                    if row["diagnosis_tree_condition"] != "":
                        diagnosis_variables = {
                            "tree_condition": "bad" if row['diagnosis_tree_condition'] ==  "dangerous" else row["diagnosis_tree_condition"],
                            "collar_condition": row["diagnosis_collar_condition"],
                            "trunk_condition": row["diagnosis_trunk_condition"],
                            "crown_condition": row["diagnosis_crown_condition"],
                            "tree_vigor": row["diagnosis_vigor_condition"],
                            "note": row["diagnosis_notes"],
                            "tree_id": tree_id,
                            "data_entry_user_id": user_id,
                            "tree_is_dangerous": None if row['diagnosis_tree_condition'] !=  "dangerous" else True
                        }
                        insert_diagnosis(diagnosis_variables)
                # TODO update import row with processing_code if success 200 else 500
            except KeyError as e:
                print(f"Column {e.args[0]} not found in data frame row. Skipping row...")
            except Exception as e:
                print(f"An error occurred while processing data frame row. Error message: {str(e)}")
        except Exception as e:
            print(f"An error occurred while processing data frame row. Error message: {str(e)}")
    return True



def import_csv(file, crs, user_id, organization_id):
    dtype = {"latitude": float,
            "longitude": float,
            "nom_scientifique": str,
            "nom_vernaculaire": str,
            "variete": str,
            "matricule": str,
            "hauteur": str,
            "habit": str,
            "date_plantation": str,
            "arrosage": str,
            "remarque": str,
            "foot_type": str,
            "sidewalk_type": str,
            "plantation_type": str,
            "landscape_type": str,
            "boundary_id": str}
    
    df = pd.read_csv(file, sep=";", dtype= dtype, keep_default_na=False)
    geoseries = set_epsg_4326_geodataframe(df, crs)
    return process_geoseries(geoseries, user_id, organization_id)

def import_excel(file, crs, user_id, organization_id):
    df = pd.read_excel(BytesIO(file.read()))
    geoseries = set_epsg_4326_geodataframe(df, crs)
    return process_geoseries(geoseries, user_id, organization_id)

def import_geopackage(file, user_id, organization_id):
    # logger.info(f'====={fiona.listlayers(BytesIO(file.read()))}=======')
    # TODO : find a way to list geopackage layers, default layer seems to be filename...
    # ... but it could be different
    geoseries = gpd.read_file(BytesIO(file.read()), layer="squelette_import_arbre_v1")
    return process_geoseries(geoseries, user_id, organization_id)

def import_geofile(file, user_id, organization_id):
    geoseries = gpd.read_file(BytesIO(file.read()))
    return process_geoseries(geoseries, user_id, organization_id)

def import_archive(file, user_id, organization_id):
    # Extract the ZIP archive to a temporary directory
    file_content = BytesIO(file.read())

    with zipfile.ZipFile(file_content, 'r') as archive:
        archive.extractall('/tmp')

    # Find the shapefile within the archive
    filenames = archive.namelist()
    shp_filenames = [f for f in filenames if f.endswith('.shp')]
    if not shp_filenames:
        raise ValueError('Shapefile not found in archive')
    shp_filename = shp_filenames[0]
    shp_path = os.path.join('/tmp', shp_filename)

    # Read the shapefile and create a GeoDataFrame
    geoseries = gpd.read_file(shp_path)
    # Convert the projection to WGS84 (latitude/longitude)
    geoseries = geoseries.to_crs(epsg=4326)
    return process_geoseries(geoseries, user_id, organization_id)

# TODO: refact use fiona or geopandas to read zip stream directly
# def import_archive_v2(file, user_id, organization_id):
#     logger.info(f"============= Reading file with Fiona ==================")
#     logger.info(f"============= {file} ==================")
#     fiona_collection = fiona.open(BytesIO(file.read()), "r")
#     logger.info(f"============= {fiona_collection} ==================")
#     logger.info(f"============= Reading file with Geopandas ==================")
#     geodataframe = gpd.read_file(f"zip+s3://{file}")
#     logger.info(f"============= {geodataframe} ==================")
#     logger.info(f"============= Converting Fiona collection to Geopandas series ==================")
#     geoseries = gpd.GeoDataFrame.from_features([feature for feature in fiona_collection], crs=4326)
#     columns = list(fiona_collection.meta["schema"]["properties"]) + ["geometry"]
#     geoseries = geoseries[columns]
#     logger.info(f"============= {geoseries} ==================")
#     return process_geoseries(geoseries, user_id, organization_id)