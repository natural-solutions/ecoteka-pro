from sqlalchemy import text
from io import StringIO, BytesIO
from openpyxl import Workbook
import csv
import json
from celery.utils.log import get_task_logger
from api.internal.grids.services import get_all_locations_data_except_tree, get_all_trees, get_vegetated_areas
from api.internal.helpers import execute_sql_query
from workers.utils.translations import STATUS, HABIT, SURFACE_TYPE, FOOT_TYPE, URBAN_SITE, PLANTATION_TYPE, SIDEWALK_TYPE, INVENTORY_SOURCE, URBAN_CONSTRAINT, LANDSCAPE_TYPE, RESIDENTIAL_USAGE, POTENTIAL_AREA_STATE

logger = get_task_logger(__name__)

def get_total_count(query):
    """Helper function to get total count from a SQL query."""
    result = execute_sql_query(query)
    return result[0][0]

def fetch_all_counts():
    """Fetch total counts for all datasets."""
    queries = {
        "locations": "SELECT COUNT(*) FROM public.location l LEFT JOIN location_status ls ON l.id_status = ls.id WHERE ls.status != 'alive'",
        "trees": "SELECT COUNT(*) FROM public.tree",
        "vegetated_areas": "SELECT COUNT(*) FROM public.vegetated_area",
    }
    return {key: get_total_count(text(query)) for key, query in queries.items()}

def fetch_data(locations_count, trees_count, vegetated_areas_count):
    """Fetch all data based on counts."""
    locations_data, _ = get_all_locations_data_except_tree(1, locations_count)
    trees_data, _ = get_all_trees(1, trees_count)
    vegetated_areas_data, _ = get_vegetated_areas(1, vegetated_areas_count)
    return locations_data, trees_data, vegetated_areas_data

def export_all_data_to_excel():
    # Fetch counts and data
    counts = fetch_all_counts()
    locations_data, trees_data, vegetated_areas_data = fetch_data(
        counts["locations"], counts["trees"], counts["vegetated_areas"]
    )

    wb = Workbook()
    ws1 = wb.active
    ws1.title = "Emplacements et Arbres"
    ws2 = wb.create_sheet(title="Surfaces de pleine terre")

    location_tree_columns = [
        ("id", "ID"),
        ("address", "Adresse"),
        ("location_status", "Statut"),
        ("scientific_name", "Nom scientifique"),
        ("vernacular_name", "Nom vernaculaire"),
        ("serial_number", "Matricule"),
        ("plantation_date", "Date de plantation"),
        ("variety", "Variété"),
        ("height", "Hauteur"),
        ("circumference", "Circonférence"),
        ("habit", "Port de l'arbre"),
        ("is_tree_of_interest", "Arbre remarquable"),
        ("watering", "Arrosage"),
        ("boundaries_administrative", "Domanialité / Compétence administrative"),
        ("boundaries_geographic", "Secteur géographique"),
        ("boundaries_station", "Station / Unité de gestion"),
        ("latitude", "Latitude"),
        ("longitude", "Longitude"),
        ("foot_type", "Type de sol"), 
        ("landscape_type", "Type de station"), 
        ("sidewalk_type", "Type de trottoir"),
        ("plantation_type", "Contexte de plantation"),
        ("is_protected", "Protection du pied"),
        ("urban_site", "Environnement urbain"), 
        ("urban_constraint", "Contraintes urbaines"),
        ("inventory_source", "Source de l'inventaire"),
        ("note", "Remarques"),
        ("creation_date", "Date de création")
    ]

    vegetated_area_columns = [
        ("id", "ID"),
        ("address", "Adresse"),
        ("surface", "Superficie en m²"),
        ("type", "Type de surface"),
        ("afforestation_trees_data", "Arbres de boisement"),
        ("shrubs_data", "Arbustes"),
        ("herbaceous_data", "Herbacées"),
        ("vegetated_areas_locations", "Arbres sur emplacement"),
        ("boundaries_administrative", "Domanialité / Compétence administrative"),
        ("boundaries_geographic", "Secteur géographique"),
        ("boundaries_station", "Station / Unité de gestion"),
        ("urban_site", "Environnement urbain"), 
        ("urban_constraint", "Contraintes urbaines"),
        ("landscape_type", "Type de station"), 
        ("has_differentiated_mowing", "Tonte différenciée"),
        ("is_accessible", "Accessibilité"),
        ("residential_usage", "Usage par les habitants"),
        ("inconvenience_risk", "Risque de gêne"),
        ("hedge_type", "Type de haie (si haie)"),
        ("linear_meters", "Mètres linéaires (si haie)"),
        ("potential_area_state", "Etat actuelle de la surface (potentielle)"),
        ("note", "Remarques"),
        ("creation_date", "Date de création")
    ]

    ws1.append([custom_name for _, custom_name in location_tree_columns])
    ws2.append([custom_name for _, custom_name in vegetated_area_columns])

    status_translations = {
        "stump": "souche ou arbre mort",
        "empty": "libre",
        "alive": "arbre vivant",
    }

    surface_types_translations = {
        "potential_area": "surface potentielle",
        "green_area": "espace vert",
        "hedge": "haie",
        "lawn": "pelouse",
        "flower_bed": "massif fleuri",
        "afforestation": "boisement"
    }

    location_tree_round_fields = ["height", "circumference"]
    vegetated_area_round_fields = ["surface"]

    def write_data(sheet, data, columns, round_fields):
        for item in data:
            row = []
            for key, _ in columns:
                value = item.get(key, "")
                if key == "location_status":
                    value = status_translations.get(value, value)
                elif key == "habit":
                    value = HABIT.get(value, value)
                elif key == "foot_type":
                    value = FOOT_TYPE.get(value, value)
                elif key == "landscape_type":
                    value = LANDSCAPE_TYPE.get(value, value)
                elif key == "sidewalk_type":
                    value = SIDEWALK_TYPE.get(value, value)
                elif key == "plantation_type":
                    value = PLANTATION_TYPE.get(value, value)
                elif key == "urban_site":
                    value = URBAN_SITE.get(value, value)
                elif key == "potential_area_state":
                    value = POTENTIAL_AREA_STATE.get(value, value)
                elif key == "urban_constraint":
                    urban_constraints = item.get("urban_constraint", [])
                    translated_constraints = [
                        URBAN_CONSTRAINT.get(constraint.strip(), constraint.strip())
                        for constraint in urban_constraints
                    ]
                    value = ", ".join(translated_constraints) 
                elif key == "residential_usage":
                    residential_usages = item.get("residential_usage", [])
                    translated_usages = [
                        RESIDENTIAL_USAGE.get(usage.strip(), usage.strip())
                        for usage in residential_usages
                    ]
                    value = ", ".join(translated_usages) 
                elif key == "inventory_source":
                    inventory_sources = item.get("inventory_source", [])
                    translated_inventories = [
                        INVENTORY_SOURCE.get(source.strip(), source.strip())
                        for source in inventory_sources
                    ]
                    value = ", ".join(translated_inventories) 
                elif key in ["boundaries_administrative", "boundaries_geographic", "boundaries_station"]:
                    boundaries = item.get("boundaries", [])
                    filtered_boundaries = [
                        boundary["name"]
                        for boundary in boundaries
                        if boundary["type"] == key.split("_")[-1]
                    ]
                elif key == "type":
                    value = surface_types_translations.get(value, value)
                elif key in round_fields and value is not None and value != "":
                    try:
                        float_value = float(value)
                        if float_value.is_integer():
                            value = int(float_value)
                        else:
                            value = round(float_value, 2)
                    except ValueError:
                        pass
                elif key in ["latitude", "longitude"]:
                    coords = item.get("coords", None)
                    if coords and "coordinates" in coords:
                        longitude, latitude = coords["coordinates"]
                        value = latitude if key == "latitude" else longitude
                    else:
                        print(f"WARNING: Missing or malformed coords for item: {item}")
                        value = None
                elif isinstance(value, list):
                    value = "; ".join(str(v) for v in value if v is not None)
                row.append("" if value is None else str(value))
            sheet.append(row)

    write_data(ws1, locations_data + trees_data, location_tree_columns, location_tree_round_fields)
    write_data(ws2, vegetated_areas_data, vegetated_area_columns, vegetated_area_round_fields)

    excel_buffer = BytesIO()
    wb.save(excel_buffer)
    excel_buffer.seek(0)
    #logger.info(f"Type of excel_buffer: {type(excel_buffer)}")
    return excel_buffer
