import logging
import os

from pydantic import BaseSettings

log = logging.getLogger("uvicorn")


class Settings(BaseSettings):
    canonical_url: str = os.getenv("CANONICAL_URL")
    db_url: str = os.getenv("DB_URL")
    db_postgresql_url: str = "postgresql://ecoteka:password@db:5432/ecoteka"

    graphql_endpoint: str = "http://graphql_engine:8080/v1/graphql"

    keycloak_admin_secret: str = "Chk2aziqwPNsszciMMlWw3w9K4NpOKlc"
    keycloak_callback_uri: str = "http://api/callback"
    keycloak_client_id: str = "ecoteka"
    keycloak_client_secret: str = "Rm414Jw6krdbjhjpmHDBxCkof7RjguS0"
    keycloak_realm: str = "ecoteka"
    keycloak_server_url: str = "http://keycloak:8080/auth"
    keycloak_token_uri: str = f"""{os.getenv("KEYCLOAK_ISSUER")}/protocol/openid-connect/token"""
    keycloack_grant_type: str = "client_credentials"
    keycloak_timeout: int = 10

    meili_master_key: str = os.getenv("MEILI_MASTER_KEY")
    meili_endpoint: str = os.getenv("MEILI_ENDPOINT")
    meili_port: str = os.getenv("MEILI_PORT")
    meili_master_key: str = "yzyzeuyee4567UYUYYIY"

    minio_entrypoint_url: str = "http://minio:8888"
    minio_root_password: str = "password"
    minio_root_user: str = "ecoteka"
    minio_default_bucket: str = os.getenv("MINIO_BUCKET_NAME")

    worker_broker: str = "redis://valkey:6379/0"
    worker_backend: str = "db+postgresql://ecoteka:password@db/ecoteka"

    urbasense_api_access_token: str = os.getenv("URBASENSE_TOKEN")
    urbasense_monitored_site_id: int = 243
    urbasense_api_base_url: str = "https://arbre.urbasense.eu/api/v1/datahub"

    urbasense_api_preprod_access_token: str = ""
    urbasense_api_preprod_base_url: str = "https://preprodarbre.urbasense.eu/api/v1/datahub"

    root_path: str = "/api"

    class Config:
        env_prefix = "api_"


settings = Settings()


def get_settings() -> BaseSettings:
    log.info("Loading config settings from the environment...")
    return Settings()
