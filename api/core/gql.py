from core.config import settings
from core.keycloak import idp
from fastapi import Security
from gql import Client
from gql.transport.aiohttp import AIOHTTPTransport


async def get_gql_client(token=Security(idp.user_auth_scheme)):
    headers = {"Authorization": f"Bearer {token}"}
    transport = AIOHTTPTransport(url=settings.graphql_endpoint, headers=headers)
    client = Client(transport=transport)

    yield client
