from core.config import settings
from core.keycloak import init_keyclaok
from fastapi import FastAPI

from api.v1.api import api_v1_router

app = FastAPI(root_path=settings.root_path)
app.include_router(api_v1_router)


@app.on_event("startup")
async def on_startup():
    init_keyclaok(app)
