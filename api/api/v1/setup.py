import gql
from core.config import settings
from core.gql import get_gql_client
from core.s3 import create_default_public_bucket
from fastapi import APIRouter, Depends
from wikidata.client import Client
from wikidata.globecoordinate import GlobeCoordinate
from wikidata.quantity import Quantity

router = APIRouter()


@router.post("/setup")
async def setup(
    page: str, domain: str, default: bool = True, gql_client: gql.Client = Depends(get_gql_client)
):
    wikidata_client = Client()
    entity = wikidata_client.get(page)
    image_prop = wikidata_client.get("P18")
    image = entity[image_prop]
    name_prop = wikidata_client.get("P1448")
    name = entity[name_prop]
    coordinates_prop = wikidata_client.get("P625")
    coordinates: GlobeCoordinate = entity[coordinates_prop]
    population_prop = wikidata_client.get("P1082")
    population: Quantity = entity[population_prop]
    area_prop = wikidata_client.get("P2046")
    surface: Quantity = entity[area_prop]
    longitude = coordinates.longitude
    latitude = coordinates.latitude
    coords = {
        "type": "Point",
        "coordinates": [longitude, latitude]
    }

    document = gql.gql(
        """
        mutation (
            $image: String = ""
            $name: String = ""
            $population: numeric = ""
            $surface: numeric = ""
            $domain: String = ""
            $default: Boolean = true
            $coords: geometry = null
            ) {
            insert_organization_one(
                object: {
                    image: $image
                    name: $name
                    population: $population
                    surface: $surface
                    default: $default,
                    domain: $domain
                    coords: $coords
                },
                on_conflict: {
                    constraint: organization_domain_key,
                    update_columns: [image, name, population, surface, coords]
                }
            ) { 
                id,
                name,
                image,
                population,
                surface,
                domain,
                default,
                coords
            }
        }
    """
    )
    variables = {
        "image": image.image_url,
        "name": name,
        "population": population.amount,
        "surface": surface.amount,
        "domain": domain,
        "default": default,
        "coords": coords
    }
    result = await gql_client.execute_async(
        document,
        variables,
    )

    return result


@router.post("/setup_storage")
def setup_storage(
):
    result = create_default_public_bucket()
    return result
