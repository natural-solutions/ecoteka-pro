import logging

from fastapi import APIRouter
from workers.run import celery_app

router = APIRouter()


@router.get("")
async def test_worker():
    task = celery_app.send_task("workers.data_transfer.tasks.toto")

    return task.id
