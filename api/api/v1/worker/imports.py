import logging

from core.keycloak import idp
from fastapi import APIRouter, Security
from workers.run import celery_app

router = APIRouter()


@router.get("")
async def test_worker(token=Security(idp.user_auth_scheme)):
   print(token)
   # task = celery_app.send_task("workers.imports.tasks.imports", args=('token', 2154 ))
   return token
