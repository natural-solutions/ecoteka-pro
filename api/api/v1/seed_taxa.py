import logging
import os

import psycopg2
from core.config import Settings, get_settings, settings
from fastapi import APIRouter, Depends

import meilisearch

router = APIRouter()
logger = logging.getLogger("uvicorn")

@router.post("/seed-taxa")
async def seed_taxa(settings: Settings = Depends(get_settings)):
    # Open Taxa Source File
    csvfile = open('./data/ecoteka_tree_taxa_with_metrics.csv', 'r')

    # Connect to DB
    conn = psycopg2.connect(settings.db_url)
    cursor = conn.cursor()

    # Create a temporary table
    cursor.execute("""
        CREATE TEMP TABLE temp_taxon (
            LIKE taxon INCLUDING ALL
        )
    """)

    # Copy CSV data into the temporary table
    copy_sql = """
        COPY temp_taxon FROM stdin WITH CSV HEADER
        DELIMITER as ','
    """
    cursor.copy_expert(sql=copy_sql, file=csvfile)

    # Insert new records from temporary table into the main table, skipping existing ones
    insert_sql = """
        INSERT INTO taxon
        SELECT *
        FROM temp_taxon
        WHERE id NOT IN (SELECT id FROM taxon)
    """
    cursor.execute(insert_sql)

    # Commit and close
    conn.commit()
    cursor.close()
    conn.close()
    csvfile.close()

    return {
        "message": "Taxa successfully imported to DB",
    }

@router.post("/update-taxa-metrics")
async def update_taxa_metrics(settings: Settings = Depends(get_settings)):
    # current work dir
    cwd = os.getcwd()
    # Open Core Taxa Source File
    csvfile = open('./data/update_taxon_metrics.csv', 'r')

    # Connect to DB and COPY csvfile
    conn = psycopg2.connect(settings.db_url)
    cursor = conn.cursor()

    # drop table tmp_taxon if exists 
    drop_tmp = """DROP TABLE IF EXISTS tmp_taxon;"""
    cursor.execute(drop_tmp)

    conn.commit()

    create_tmp_table = f"""
            CREATE TABLE tmp_taxon (id int, carbon numeric NULL DEFAULT NULL, icu numeric NULL DEFAULT NULL, air numeric NULL DEFAULT NULL, biodiversity numeric NULL DEFAULT NULL, allergenic_score numeric NULL DEFAULT NULL, vulnerability numeric NULL DEFAULT NULL, growth_category text NULL DEFAULT NULL);
    """
    copy_sql = """
            COPY tmp_taxon FROM stdin WITH CSV HEADER DELIMITER AS ',';
           """
    update_sql = """
            UPDATE taxon
            SET carbon = tmp_taxon.carbon, air = tmp_taxon.air, icu = tmp_taxon.icu, biodiversity = tmp_taxon.biodiversity, allergenic_score = tmp_taxon.allergenic_score, vulnerability = tmp_taxon.vulnerability, growth_category = tmp_taxon.growth_category
            FROM tmp_taxon
            WHERE taxon.id = tmp_taxon.id;
            DROP TABLE tmp_taxon;
    """
    cursor.execute(create_tmp_table)
    conn.commit()
    cursor.copy_expert(sql=copy_sql, file=csvfile)
    conn.commit()
    cursor.execute(update_sql)
    conn.commit()
    cursor.close()

    return {
        "message": "Taxa successfully updated with metrics in DB",
    }

@router.post("/index-taxa")
async def index_taxa(settings: Settings = Depends(get_settings)):
    # Open Core Taxa Source File
    csvfile = open('./data/ecoteka_tree_taxa_with_metrics.csv', 'r')
    # meili_url = f'{settings.canonical_url}/meilisearch'
    client = meilisearch.Client(settings.meili_endpoint, settings.meili_master_key)
    client.delete_index('taxa')
    client.create_index('taxa')
    index = client.index('taxa')
    data = csvfile.read()
    logger.info(data)
    index_csv_task = index.add_documents_csv(data.encode('utf-8'), primary_key = 'id')
    logger.info(index_csv_task)

    return {
        "index_csv_task": index_csv_task,
        "key": settings.meili_master_key,
        "another_key": settings.meili_master_key,
        "endpoint": settings.meili_endpoint,
        "canonical_url": settings.canonical_url
    }


@router.post("/associate-tree-to-taxon")
async def associate_tree_to_taxon(settings: Settings = Depends(get_settings)):
    conn = psycopg2.connect(settings.db_url)
    cursor = conn.cursor()

    # Step 1: Retrieve all trees from the DB where taxon_id is NULL
    cursor.execute("SELECT id, scientific_name, vernacular_name FROM tree WHERE taxon_id IS NULL AND (scientific_name <> '' OR vernacular_name <> '')")
    trees = cursor.fetchall()

    # Associate it with a taxon if match
    for tree in trees:
        id, scientific_name, vernacular_name = tree

        # Step 2: Query the taxon table to find a match based on scientific_name or vernacular_name
        cursor.execute("SELECT id FROM taxon WHERE canonical_name ILIKE %s OR vernacular_name ILIKE %s", (scientific_name, vernacular_name))

        taxon_id = cursor.fetchone()
        if taxon_id:
            # Taxon already exists, use the existing taxon_id
            taxon_id = taxon_id[0]
        else:
            # Taxon does not exist, insert a new taxon 
            try:
                cursor.execute("INSERT INTO taxon (canonical_name, vernacular_name) VALUES (%s, %s) RETURNING id", (scientific_name, vernacular_name))
                taxon_id = cursor.fetchone()  # Fetch the generated id of the new taxon
                print(f"New taxon inserted with id: {taxon_id}")

            except psycopg2.Error as e:
                conn.rollback()
                print(f"Error inserting new taxon: {e}")
                continue  # Skip to next tree if insertion fails

        # Step 3: Update the tree with the found or newly created taxon_id
        try:
            cursor.execute("UPDATE tree SET taxon_id = %s WHERE id = %s", (taxon_id, id))
        except psycopg2.Error as e:
            conn.rollback()
            print(f"Error updating tree with taxon_id: {e}")
            continue  # Skip to next tree if update fails

    conn.commit()
    cursor.close()
    conn.close()

    return {
        "message": "Trees successfully associated to taxon in DB",
    }

