from core.s3 import create_bucket, remove_bucket
from fastapi import APIRouter, Request
from workers.run import celery_app


router = APIRouter()


@router.post("")
async def tree_location_import(request: Request):
    json = await request.json()

    if json["event"]["op"] == "INSERT":
        print(f'====== {json["event"]["data"]["new"]["id"]} =========')
        print(f'====== {json["event"]["data"]["new"]["creation_date"]} =========')
        new_import = json["event"]["data"]["new"]
        task = celery_app.send_task("workers.data_transfer.tasks.imports", args=(new_import["source_file_path"], new_import["source_crs"], new_import["data_entry_user_id"], new_import["organization_id"] ))
        return task.id
    else:
        return json
