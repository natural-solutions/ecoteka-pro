from fastapi import APIRouter, Depends

from api.v1.providers.urbasense import router as urbasense_router
from core.keycloak import idp

router = APIRouter(prefix="/providers", dependencies=[Depends(idp.user_auth_scheme)])
router.include_router(urbasense_router, prefix="/urbasense")
