
from json import dumps, loads

from fastapi import APIRouter, Request
from pydantic import BaseModel

from api.internal.providers.urbasense.models import (
    ComboPlotModel,
    SubjectMonitoring,
    UrbasenseSubjectMonitoringRaw,
)
from api.internal.providers.urbasense.services import (
    get_urbasense_subject_monitoring_antv_comboplot_format,
    get_urbasense_subject_monitoring_antv_series_format,
    get_urbasense_subject_monitoring_raw,
)


class Event(BaseModel):
    data: dict


router = APIRouter()


@router.get("/subject/{subject_id}/monitor")
async def subject_monitor(subject_id: int,) -> SubjectMonitoring:
    return get_urbasense_subject_monitoring_antv_series_format(subject_id)


@router.get("/subject/{subject_id}/monitor/raw")
async def subject_monitor_raw(subject_id: int) -> UrbasenseSubjectMonitoringRaw:
    response = get_urbasense_subject_monitoring_raw(subject_id)
    return response.json()

@router.get("/subject/{subject_id}/monitor/comboplot")
async def subject_monitor_comboplot(subject_id: int):
    return get_urbasense_subject_monitoring_antv_comboplot_format(subject_id)