from fastapi import APIRouter, Depends
from core.keycloak import idp

from api.internal.map import filters
from api.internal.interventions import widget
from api.internal.keycloak import users
from api.internal.keycloak import roles
from api.internal import organization
from api.internal import boundary
from api.internal import diagnosis
from api.internal import metrics
from api.internal import minio
from api.internal import grids
from api.internal.providers import urbasense
from api.v1 import events, providers, seed_taxa, setup, worker
from api.v1.keycloak import event

api_v1_router = APIRouter( dependencies=[Depends(idp.user_auth_scheme)])
api_v1_router.include_router(setup.router, prefix="/admin", tags=["admin"])
api_v1_router.include_router(seed_taxa.router, prefix="/admin", tags=["admin"])
api_v1_router.include_router(providers.router, prefix="", tags=["providers"])
api_v1_router.include_router(filters.router, prefix="/filters", tags=["filters"])
api_v1_router.include_router(widget.router, prefix="/widget", tags=["widget"])
api_v1_router.include_router(users.router, prefix="/keycloack", tags=["keycloack"])
api_v1_router.include_router(roles.router, prefix="/keycloack", tags=["keycloack"])
api_v1_router.include_router(organization.router, prefix="/organization", tags=["organization"])
api_v1_router.include_router(boundary.router, prefix="/boundary", tags=["boundary"])
api_v1_router.include_router(diagnosis.router, prefix="/diagnosis", tags=["diagnosis"])
api_v1_router.include_router(metrics.router, prefix="/metrics", tags=["metrics"])
api_v1_router.include_router(urbasense.router, prefix="/urbasense", tags=["urbasense"])
api_v1_router.include_router(minio.router, prefix="/minio", tags=["minio"])
api_v1_router.include_router(grids.router, prefix="/grids", tags=["grids"])
