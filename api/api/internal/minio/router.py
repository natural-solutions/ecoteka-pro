from fastapi import APIRouter, Body
from pydantic import BaseModel
from core.config import settings
from core.s3 import (
    move_file_in_minio
)
from core.keycloak import idp


router = APIRouter()

class Params(BaseModel):
    old_path: str
    new_path: str
    bucket_name: str

@router.post("/move-file-in-minio")
async def move_minio_file(
    params: Params = Body(...),
):  
    try:
        response = move_file_in_minio(params.old_path, params.new_path, params.bucket_name)
        return {"message": "File moved successfully", "response": response}
    except Exception as e:
        print(f"Error moving file: {e}")
        return {"error": "Failed to move file", "details": str(e)}