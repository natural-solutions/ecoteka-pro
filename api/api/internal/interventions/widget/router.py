from fastapi import APIRouter, Body

from .services import (
    get_late_interventions_by_family_intervention_type,
    get_family_intervention_types
)
from api.internal.metrics.models import Params

router = APIRouter()


# GET http://api_internal/metrics/get_late_interventions_by_family
@router.post("/get_late_interventions_by_family")
async def get_late_interventions_by_family(params: Params = Body(...)):
    return get_late_interventions_by_family_intervention_type(params)

@router.get("/get_all_families_intervention_type")
async def get_all_families_intervention_type():
    return get_family_intervention_types()
