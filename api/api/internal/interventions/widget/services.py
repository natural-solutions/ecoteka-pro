from api.internal.helpers import (
    execute_sql_query
)
from sqlalchemy import text


def get_late_interventions_by_family_intervention_type(params):
    boundary_ids = getattr(params, 'boundary_ids', None)

    # Define the joins clause if boundary_ids are provided
    if boundary_ids and boundary_ids != []:
        boundary_ids_str = ', '.join(f"'{x}'" for x in boundary_ids)
        joins_clause = """
            LEFT JOIN boundaries_locations bl_location ON i.location_id = bl_location.location_id
            LEFT JOIN tree tr ON i.tree_id = tr.id
            LEFT JOIN boundaries_locations bl_tree ON tr.location_id = bl_tree.location_id
        """
        where_clause = text(f"""AND (bl_location.boundary_id IN ({boundary_ids_str}) AND i.location_id IS NOT NULL) OR (bl_tree.boundary_id IN ({boundary_ids_str}) AND i.tree_id IS NOT NULL)""")
    else:
        joins_clause = ""  
        where_clause = ""
    sql_query = text(f"""
        SELECT
            f.id as FamilyID,
            f.slug as FamilyType,
            f.color as FamilyColor,
            t.slug as InterventionType,
            i.id,
            i.scheduled_date,
            i.tree_id,
            i.location_id,
            i.intervention_type_id
        FROM
            intervention i
        {joins_clause}
        INNER JOIN
            intervention_type t ON i.intervention_type_id = t.id
        INNER JOIN
            family_intervention_type f ON t.family_intervention_type_id = f.id
        WHERE
            i.scheduled_date < CURRENT_DATE
            AND i.realization_date IS NULL
            {where_clause}
        ORDER BY
            f.id, t.slug;
    """)

    result = execute_sql_query(sql_query)

    parsed_result = [{'family_id': row[0], 'family_name': row[1], 'family_color': row[2], 'intervention_type': row[3], 'intervention_id': row[4], 'scheduled_date': row[5], 'tree_id': row[6], 'intervention_type_id': row[7]} for row in result]

    grouped_interventions = {}
    for intervention in parsed_result:
        family_id = intervention.pop('family_id')
        if family_id not in grouped_interventions:
            grouped_interventions[family_id] = []
        grouped_interventions[family_id].append(intervention)
    return grouped_interventions

def get_family_intervention_types():
    sql_query = text("""
        SELECT
            id as FamilyID,
            slug as FamilyName,
            color as FamilyColor
        FROM
            family_intervention_type;
    """)

    result = execute_sql_query(sql_query)

    families = [{'family_id': row[0], 'family_slug': row[1], 'family_color': row[2]} for row in result]
    return families
