from sqlalchemy import text
from api.internal.helpers import (
    execute_sql_query,
    calculate_score,
    inclusive_filters,
)
from fastapi.encoders import jsonable_encoder
from bigtree import (
    tree_to_nested_dict,
    Node
)
import psycopg2
from core.config import settings
from typing import List, Optional
import pandas as pd

def calculate_health_score():
    sql_query = text("""
        SELECT
            o.population AS population,
            COUNT(t.id) AS num_trees_no_felling_date
        FROM
            organization o
        LEFT JOIN
            location l ON l.organization_id = o.id
        LEFT JOIN
            tree t ON t.location_id = l.id AND t.felling_date IS NULL
        GROUP BY
            o.population
    """)

    result = execute_sql_query(sql_query)
    health_scores = [{"population": row[0], "num_trees_no_felling_date": row[1]} for row in result]
    return health_scores

def calculate_non_allergenic_score(params):
    return calculate_score("allergenic_score", params)

def calculate_vulnerability_score(params):
    return calculate_score("vulnerability", params)

def calculate_mean_air_quality_score(params):
    return calculate_score("air", params)

def calculate_biodiversity_score(params):
    return calculate_score("biodiversity", params)

def calculate_icu_score(params):
    return calculate_score("icu", params)

def get_notable_trees(params):
    response = inclusive_filters("tree", {"is_tree_of_interest": True}, params)
    data = [{"id": str(t[0]), "__typename": "tree"} for t in response]
    return jsonable_encoder(data)


def set_taxonomic_tree(params: Optional[List[str]] = []):
    def lighten_color(color, amount=0.5):
        """
        Lightens the given color by multiplying (1-luminosity) by the given amount.
        Input can be matplotlib color string, hex string, or RGB tuple.

        Examples:
        >> lighten_color('g', 0.3)
        >> lighten_color('#F034A3', 0.6)
        >> lighten_color((.3,.55,.1), 0.5)
        """
        import colorsys

        import matplotlib.colors as mc
        import numpy as np
        try:
            c = mc.cnames[color]
        except:
            c = color
        c = np.array(colorsys.rgb_to_hls(*mc.to_rgb(c)))
        return mc.to_hex(colorsys.hls_to_rgb(c[0],1-amount * (1-c[1]),c[2]))

    with psycopg2.connect(settings.db_url) as conn:
        join_sql = ""
        and_sql = ""
        if params and params.boundary_ids:
            boundary_ids_str = ", ".join([f"'{id_}'" for id_ in params.boundary_ids])
            join_sql = """LEFT JOIN boundaries_locations ON tree.location_id = boundaries_locations.location_id"""
            and_sql = f"""AND tree.location_id IN (SELECT location_id FROM boundaries_locations WHERE boundary_id IN ({boundary_ids_str}))"""
        sql_parents = f"""SELECT
            taxon.family AS family,
            COUNT(*) AS count
            FROM taxon
            LEFT JOIN tree ON taxon.id = tree.taxon_id AND taxon.genus IS NOT NULL
            {join_sql}
            WHERE taxon.id = tree.taxon_id
            {and_sql}
            GROUP BY taxon.family
            ORDER BY count
            LIMIT 20;
        """

        sql_children = f"""SELECT
                taxon.family AS family,
                taxon.species AS species,
                taxon.genus as genus,
                COUNT(*) AS count
                FROM taxon
                LEFT JOIN tree ON taxon.id = tree.taxon_id  AND taxon.genus IS NOT NULL
                {join_sql}
                WHERE taxon.id = tree.taxon_id
                {and_sql}
                GROUP BY taxon.family, taxon.species, taxon.genus
                ORDER BY taxon.family
                LIMIT 20;"""

        try:
            data_parents = pd.read_sql_query(sql_parents, conn)
            data_children = pd.read_sql_query(sql_children, conn)
        except Exception as e:
            print(f"Error occurred: {e}")
            return None

        if data_parents.empty or data_children.empty:
            return None

        def set_family_color(row):
            import matplotlib.colors as mc
            from matplotlib.cm import viridis
            colors = viridis(row.name / len(data_parents))
            return mc.to_hex(colors)

        def compute_size(row, max_in):
            import numpy as np
            count = row['count']
            return round(np.interp(count, [1, max_in], [5, 200]))

        data_parents["color"] = data_parents.apply(set_family_color, axis=1)
        total_count = data_parents['count'].sum()
        data_parents['percentage'] = round((data_parents['count'] / float(total_count)) * 100, 1)
        data_parents['size'] = data_parents.apply(lambda row: compute_size(row, total_count), axis=1)

        joined = data_children.merge(data_parents, on='family', suffixes=('', '_parent'))

        genus_trees = {}

        for index, row in joined.iterrows():
            genus = row['genus']
            size = row['size']
            color = row['color']
            percentage = row['percentage']

            if genus not in genus_trees:
                genus_trees[genus] = Node(name=genus, size=size, color=color, percentage=percentage)

            species_name = row['species'] if row['species'] is not None else ""
            count = row['count']
            color = lighten_color(row['color'], 0.7)
    
            if species_name.strip():  
                species_node = Node(name=species_name, parent=genus_trees[genus], count=count, color=color)

        # Convert each genus tree to a dictionary
        trees_list = []
        for genus, tree_root in genus_trees.items():
            genus_info = {
                'name': genus,
                'size': tree_root.size,
                'percentage': tree_root.percentage,
                'color': tree_root.color,
                'count': count
            }
            
            children_dict = tree_to_nested_dict(tree_root, all_attrs=True)
            if 'children' in children_dict:
                genus_info['children'] = children_dict['children']
            
            trees_list.append(genus_info)

        return trees_list


def calculate_carbon_storage_score(params):
    # Définition de la requête SQL de base
    base_sql = """
    SELECT
        t.id,
        tx.growth_category,
        tx.carbon
    FROM
        tree t
    JOIN taxon tx ON t.taxon_id = tx.id
    """

    boundary_ids = getattr(params, 'boundary_ids', None)
    if boundary_ids:
        boundary_ids_str = ', '.join(f"'{x}'" for x in boundary_ids)
        boundary_sql = f"""
        JOIN boundaries_locations bl ON t.location_id = bl.location_id
        WHERE
            tx.growth_category IS NOT NULL AND
            bl.boundary_id IN ({boundary_ids_str})
        """
        base_sql += boundary_sql
    else:
        base_sql += " WHERE tx.growth_category IS NOT NULL"

    # Récupération des données de base en utilisant la fonction helper execute_sql_query
    base_data = execute_sql_query(text(base_sql))

    # Traitement des données de carbone
    carbon_data = [row.carbon for row in base_data if row.carbon is not None]
    carbon_average = round(pd.Series(carbon_data).mean(), 1)

    # Configuration des intervalles pour les scores de carbone
    bins = [0, 1, 3, 5, float('inf')]
    labels = ["0-1", "1-3", "3-5", "unknown"]

    bin_series = pd.cut([carbon_average], bins=bins, labels=labels, right=False)
    data = []

    for bin_label in bin_series.categories:
        if bin_label == "unknown":
            tree_count = len([x for x in carbon_data if x is None])
        else:
            bin_start, bin_end = map(float, bin_label.split('-'))
            tree_count = len([x for x in carbon_data if bin_start <= x < bin_end])

        data.append({
            "carbon_range": bin_label,
            "tree_count": tree_count
        })

    total_tree_count = len(base_data)
    metadata = {
        "tree_count": sum(d["tree_count"] for d in data),
        "total_tree_count": total_tree_count
    }

    return {"data": data, "metadata": metadata}
