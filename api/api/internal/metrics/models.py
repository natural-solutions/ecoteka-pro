from typing import List, Optional

from pydantic import BaseModel


class AirQualityScore(BaseModel):
    air_score: Optional[float]

class CarbonData(BaseModel):
    carbon_range: str
    tree_count: int

class CarbonStorageData(BaseModel):
    data: List[CarbonData]
    metadata: dict  # You may want to replace 'dict' with a more specific model if you have defined one

class BiodiversityScore(BaseModel):
    biodiversity_score: Optional[float]

class ICUScore(BaseModel):
    icu_score: Optional[float]

class NonAllergenicScore(BaseModel):
    non_allergenic_score: Optional[float]

class HealthScore(BaseModel):
    population: int
    num_trees_no_felling_date: int

class VulnerabilityScore(BaseModel):
    vulnerability_score: Optional[float]

class EcosystemScoresResponse(BaseModel):
    air_quality_score: List[AirQualityScore] 
    carbon_storage_score: CarbonStorageData
    biodiversity_score: List[BiodiversityScore]
    icu_score: List[ICUScore]

class HealthScoresResponse(BaseModel):
    non_allergenic_score: List[NonAllergenicScore]
    health_score: List[HealthScore]

class ResilienceScoresResponse(BaseModel):
    vulnerability_score: List[VulnerabilityScore]

class TaxonomyAggregatesResponse(BaseModel):
    family: str
    genus: str
    count: int

class Params(BaseModel):
    boundary_ids: Optional[List[str]]
