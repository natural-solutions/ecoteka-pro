from fastapi import APIRouter, Body
from api.internal.metrics.services import (
    calculate_health_score,
    calculate_non_allergenic_score,
    calculate_vulnerability_score,
    get_notable_trees,
    set_taxonomic_tree,
    calculate_mean_air_quality_score,
    calculate_carbon_storage_score,
    calculate_biodiversity_score,
    calculate_icu_score
)
from .models import Params


router = APIRouter()

@router.post("/health")
async def getHealthMetrics(params: Params = Body(...)):
    non_allergenic_score_data = calculate_non_allergenic_score(params)
    health_score_data = calculate_health_score()
    non_allergenic_score = non_allergenic_score_data[0]["non_allergenic_score"] if non_allergenic_score_data else None
    health_score = health_score_data[0] if health_score_data else {"population": 0, "num_trees_no_felling_date": 0}

    return {
        "non_allergenic_score": non_allergenic_score,
        "health_score": health_score
    }


@router.post("/resilience")
async def get_resilience_scores(params: Params = Body(...)):
    vulnerability_score = calculate_vulnerability_score(params)
    resilience_score = vulnerability_score[0] if vulnerability_score else {}
    return resilience_score

@router.post("/tree/notable")
async def fetch_notable_trees(params: Params = Body(...)):
    return get_notable_trees(params)

@router.post("/get_taxonomy_aggregates")
async def get_taxonomy_aggregates(params: Params):
    return set_taxonomic_tree(params)

@router.post("/get_all_ecosystem_scores")
async def get_all_ecosystem_scores(params: Params = Body(...)):
    air_quality_score = calculate_mean_air_quality_score(params)
    carbon_storage_score = calculate_carbon_storage_score(params)
    biodiversity_score = calculate_biodiversity_score(params)
    icu_score = calculate_icu_score(params)

    return {
        "air_quality_score": air_quality_score,
        "carbon_storage_score": carbon_storage_score,
        "biodiversity_score": biodiversity_score,
        "icu_score": icu_score,
    }
