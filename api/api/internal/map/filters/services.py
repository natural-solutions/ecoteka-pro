from api.internal.helpers import (
    get_unique_column_values,
    get_oldest_date,
    execute_sql_query
)
from sqlalchemy import text

def get_trees_data():
    unique_scientific_names = get_unique_column_values('tree', 'scientific_name')
    unique_vernacular_names = get_unique_column_values('tree', 'vernacular_name')
    oldest_plantation_date = get_oldest_date('tree', 'plantation_date')

    return {
        'scientific_names': unique_scientific_names,
        'vernacular_names': unique_vernacular_names,
        'plantation_date': oldest_plantation_date,
    }

def get_locations_data():
    return get_unique_column_values('location', 'address')

def get_location_status_data():
    sql_query = text("""
        SELECT id, status, color
        FROM location_status
    """)

    rows = execute_sql_query(sql_query)
    return [{'id': row.id, 'status': row.status, 'color': row.color} for row in rows]

def get_boundary_types_data():
    unique_types = get_unique_column_values('boundary', 'type')
    filtered_types = [boundary_type for boundary_type in unique_types if boundary_type != 'administrative']
    return filtered_types

def get_diagnosis_data():
    unique_tree_condition = get_unique_column_values('diagnosis', 'tree_condition')
    unique_recommendation = get_unique_column_values('diagnosis', 'recommendation')
    return {
        'tree_condition': unique_tree_condition,
        'recommendation': unique_recommendation,
    }

def get_green_area_data():
    sql_query = text("""
        SELECT id, name
        FROM green_area
    """)
    rows = execute_sql_query(sql_query)
    return [{'id': row.id, 'name': row.name} for row in rows]

def get_interventions_data():
    oldest_scheduled_dates = get_oldest_date('intervention', 'scheduled_date')
    oldest_realization_date = get_oldest_date('intervention', 'realization_date')

    return {
            'scheduled_date': oldest_scheduled_dates,
            'realization_date': oldest_realization_date,
        }

def get_interventions_partner_data():
    sql_query = text("""
        SELECT id, name
        FROM intervention_partner
    """)
    rows = execute_sql_query(sql_query)
    return [{'id': row.id, 'name': row.name} for row in rows]

def get_adminisatrive_boundary_data():
    sql_query = text("""
        SELECT id, name
        FROM boundary
        WHERE type = 'administrative'
    """)
    rows = execute_sql_query(sql_query)
    return [{'id': row.id, 'name': row.name} for row in rows]

def get_interventions_type_data():
    sql_query = text("""
        SELECT id, slug
        FROM intervention_type
    """)
    rows = execute_sql_query(sql_query)
    return [{'id': row.id, 'slug': row.slug} for row in rows]

def get_diagnosis_type_data():
    return get_unique_column_values('diagnosis_type', 'slug')

def get_filter_with_martin_data():
    diagnosis_info = get_diagnosis_data()
    tree_data = get_trees_data()
    boundary_data =  {
        "type": get_boundary_types_data(),
    }
    location_data = {
        'status': get_location_status_data(),
        'address': get_locations_data()
    }
    diagnosis_data = {
        'tree_condition': diagnosis_info["tree_condition"],
        'recommendation': diagnosis_info["recommendation"],
        'type': get_diagnosis_type_data()
    }
    intervention_data = get_interventions_data()
    intervention_partner_data = get_interventions_partner_data()
    intervention_type_data = get_interventions_type_data()
    administrative_boundary_data = get_adminisatrive_boundary_data()

    intervention_data['partner'] = intervention_partner_data
    intervention_data['type'] = intervention_type_data

    return {
        'tree': tree_data,
        'location': location_data,
        'diagnosis': diagnosis_data,
        'intervention': intervention_data,
        'boundary': boundary_data,
        'administrative_boundary': administrative_boundary_data
    }
