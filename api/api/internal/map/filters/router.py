from fastapi import APIRouter

from .services import (
    get_trees_data,
    get_locations_data,
    get_diagnosis_data,
    get_interventions_data,
    get_interventions_partner_data,
    get_interventions_type_data,
    get_diagnosis_type_data,
    get_filter_with_martin_data,
    get_location_status_data
)

router = APIRouter()


# GET http://api_internal/map/filters/tree
@router.get("/tree")
async def get_all_trees():
    return get_trees_data()

# GET http://api_internal/map/filters/address
@router.get("/address")
async def get_all_locations():
    return get_locations_data()

# GET http://api_internal/map/filters/location_status
@router.get("/location_status")
async def get_all_location_status():
    return get_location_status_data()

# GET http://api_internal/map/filters/diagnosis
@router.get("/diagnosis")
async def get_all_diagnosis():
    return get_diagnosis_data()

# GET http://api_internal/map/filters/intervention
@router.get("/intervention")
async def get_all_interventions():
    return get_interventions_data()

# GET http://api_internal/map/filters/intervention_partner
@router.get("/intervention_partner")
async def get_all_interventions_partner():
    return get_interventions_partner_data()

# GET http://api_internal/map/filters/intervention_type
@router.get("/intervention_type")
async def get_all_interventions_type():
    return get_interventions_type_data()

# GET http://api_internal/map/filters/diagnosis_type
@router.get("/diagnosis_type")
async def get_all_diagnosis_type():
    return get_diagnosis_type_data()

# GET http://api_internal/map/filters/filter_with_martin
@router.get("/filter_with_martin")
async def get_all_filter_with_martin():
    return get_filter_with_martin_data()
