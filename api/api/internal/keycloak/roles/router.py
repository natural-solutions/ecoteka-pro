from fastapi import APIRouter, Body, Header, Security
from core.keycloak import idp

from .services import (
    get_realm_roles
)

router = APIRouter()

@router.get("/roles")
async def list_roles(token=Security(idp.user_auth_scheme)):
    roles = await get_realm_roles()
    return roles
