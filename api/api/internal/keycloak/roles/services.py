import httpx
from core.config import settings
from ..helpers import get_admin_token

async def get_realm_roles():
    token = await get_admin_token()
    url = f"{settings.keycloak_server_url}/admin/realms/{settings.keycloak_realm}/roles"
    headers = {
        'Authorization': f"Bearer {token}"
    }
    async with httpx.AsyncClient() as client:
        response = await client.get(url, headers=headers)
    response.raise_for_status()
    filtered_data = [item for item in response.json() if not any(
        "${" in str(value) for key, value in item.items() if key == "description"
    )]
    return filtered_data
