from fastapi import APIRouter, Body, Header, Security
from core.keycloak import idp

from .services import (
    create_keycloak_user,
    delete_keycloak_user,
    get_keycloak_users,
    get_keycloak_user,
    set_password_keycloak_user,
    update_keycloak_user,
    reset_password_user,
    disable_keycloak_user
)

router = APIRouter()

@router.get("/users")
async def list_users(token=Security(idp.user_auth_scheme)):
    users = await get_keycloak_users()
    return users

@router.get("/user")
async def get_user(user_id: str = Header(...),token=Security(idp.user_auth_scheme)):
    user = await get_keycloak_user(user_id)
    return user

@router.put("/users/reset-password-email")
async def reset_password(user_id: str = Header(...),token=Security(idp.user_auth_scheme)):
    data = await reset_password_user(user_id, token)
    return data

@router.delete("/users/delete")
async def delete_user(user_id: str = Header(...),token=Security(idp.user_auth_scheme)):
    deleted_user = await delete_keycloak_user(user_id)
    return deleted_user

@router.put("/users/disable")
async def disable_user(user_id: str = Header(...),token=Security(idp.user_auth_scheme)):
    disabled_user = await disable_keycloak_user(user_id)
    return disabled_user

@router.put("/users/update")
async def update_user(user_id: str = Header(...), data: dict = Body(...),token=Security(idp.user_auth_scheme)):
    updated_user = await update_keycloak_user(user_id, data)
    return updated_user

@router.post("/users/create")
async def create_user(data: dict = Body(...),token=Security(idp.user_auth_scheme)):
    created_user = await create_keycloak_user(data)
    return created_user

@router.put("/users/password")
async def password_user(user_id: str = Header(...), data = Body(...),token=Security(idp.user_auth_scheme)):
    users = await set_password_keycloak_user(user_id, data)
    return users
