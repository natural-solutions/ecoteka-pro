import httpx
from httpx import Response
from core.config import settings
from fastapi import HTTPException
from ..roles.services import get_realm_roles
from ..helpers import get_admin_token

async def get_keycloak_users():
    user_token = await get_admin_token()
    url = f"{settings.keycloak_server_url}/admin/realms/{settings.keycloak_realm}/users"
    headers = {
        'Authorization': f"Bearer {user_token}"
    }
    async with httpx.AsyncClient() as client:
        response = await client.get(url, headers=headers)
    response.raise_for_status()
    return response.json()

async def get_keycloak_user(user_id):
    user_token = await get_admin_token()
    url = f"{settings.keycloak_server_url}/admin/realms/{settings.keycloak_realm}/users/{user_id}"
    headers = {
        'Authorization': f"Bearer {user_token}",
    }
    async with httpx.AsyncClient() as client:
        response = await client.get(url, headers=headers)
    response.raise_for_status()
    return response.json()

async def delete_keycloak_user(user_id):
    user_token = await get_admin_token()
    url = f"{settings.keycloak_server_url}/admin/realms/{settings.keycloak_realm}/users/{user_id}"
    headers = {
        'Authorization': f"Bearer {user_token}",
        'Content-Type': 'application/json'
    }
    async with httpx.AsyncClient() as client:
        # First try to delete the user
        response = await client.delete(url, headers=headers)
        
        if 200 <= response.status_code < 300:
            return "User deleted successfully"

        # If delete failed with 409 (Conflict), return 401 to trigger disable flow
        if response.status_code == 409:
            raise HTTPException(
                status_code=409,
                detail="This user has data he cannot be deleted only disabled"
            )
        try:
            error_message = response.json().get("error", "")
        except ValueError:
            error_message = "Failed to decode the error message from the response."
        raise HTTPException(status_code=500, detail=error_message)

async def disable_keycloak_user(user_id):
    user_token = await get_admin_token()
    url = f"{settings.keycloak_server_url}/admin/realms/{settings.keycloak_realm}/users/{user_id}"
    headers = {
        'Authorization': f"Bearer {user_token}",
        'Content-Type': 'application/json'
    }
    data = {"enabled": False}

    async with httpx.AsyncClient() as client:
        response = await client.put(url, headers=headers, json=data)

    if 200 <= response.status_code < 300:
        return "User disabled successfully"
    try:
        error_data = response.json()
        error_message = error_data.get("errorMessage", "An unknown error occurred.")
    except ValueError:
        error_message = "Failed to decode the error message from the response."
    raise HTTPException(status_code=500, detail=error_message)

async def update_keycloak_user(user_id, data):
    user_token = await get_admin_token()
    url = f"{settings.keycloak_server_url}/admin/realms/{settings.keycloak_realm}/users/{user_id}"
    headers = {
        'Authorization': f"Bearer {user_token}",
        'Content-Type': 'application/json'
    }
    async with httpx.AsyncClient() as client:
        response = await client.put(url, headers=headers, json=data)
    if 200 <= response.status_code < 300:
        # Update roles after successful user update if attributes exist
        if "attributes" in data and "role" in data["attributes"]:
            roles = await get_realm_roles()
            filtered_roles = [role for role in roles if role['name'] in data["attributes"]["role"]]
            await set_roles_to_keycloak_user(user_id, filtered_roles)
        return "User updated successfully"
    else:
        try:
            error_message = response.json().get("error", "")
        except ValueError:
            error_message = "Failed to decode the error message from the response."
        raise HTTPException(status_code=500, detail=error_message)

async def create_keycloak_user(data) -> str:
    user_token = await get_admin_token()
    url = f"{settings.keycloak_server_url}/admin/realms/{settings.keycloak_realm}/users"
    headers = {
        'Authorization': f"Bearer {user_token}",
        'Content-Type': 'application/json'
    }
    data["username"] = f"{data.get('firstName', '')} {data.get('lastName', '')}"
    data["emailVerified"] = True
    data["enabled"] = True
    data["attributes"]["passwordLength"] = 7

    async with httpx.AsyncClient() as client:
        response: Response = await client.post(url, headers=headers, json=data)

    if 200 <= response.status_code < 300:
        new_user = await get_keycloak_users()
        for u in new_user:
            if u["email"] == data["email"]:
                user_id = u["id"]
                await set_password_keycloak_user(user_id, "password")
                roles = await get_realm_roles()
                filtered_roles = [role for role in roles if role['name'] in u["attributes"]["role"]]
                await set_roles_to_keycloak_user(user_id, filtered_roles)
        return "User created successfully"
    else:
        try:
            error_message = response.json().get("errorMessage", "")
        except ValueError:
            error_message = "Failed to decode the error message from the response."
        raise HTTPException(status_code=500, detail=error_message)
    

## waiting for SMTP protocole (ticket glpi), after we can use the reset_password_user directly and we can also send a link to update profile for more security
async def reset_password_user(user_id, user_token):
    url = f"{settings.keycloak_server_url}/admin/realms/{settings.keycloak_realm}/users/{user_id}/execute-actions-email"
    headers = {
        'Authorization': f"Bearer {user_token}",
        'Content-Type': 'application/json'
    }
    data = ["UPDATE_PASSWORD"]
    async with httpx.AsyncClient() as client:
        response = await client.put(url, headers=headers, json=data)
    if 200 <= response.status_code < 300:
        return "Password update action initiated successfully. User will receive an email to update the password."
    else:
        try:
            error_message = response.json().get("error", "")
        except ValueError:
            error_message = "Failed to decode the error message from the response."
        raise HTTPException(status_code=500, detail=error_message)

async def set_password_keycloak_user(user_id, data):
    user_token = await get_admin_token()
    url = f"{settings.keycloak_server_url}/admin/realms/{settings.keycloak_realm}/users/{user_id}/reset-password"
    headers = {
        'Authorization': f"Bearer {user_token}",
        'Content-Type': 'application/json'
    }
    new_password = {
          "type": "password",
          "value": data,
          "temporary": False
        }
    async with httpx.AsyncClient() as client:
        response = await client.put(url, headers=headers, json=new_password)
    if 200 <= response.status_code < 300:
        return "User password successfully changed"
    else:
        try:
            error_message = response.json().get("error", "")
        except ValueError:
            error_message = "Failed to decode the error message from the response."
        raise HTTPException(status_code=500, detail=error_message)

async def set_roles_to_keycloak_user(user_id, data):
    user_token = await get_admin_token()
    url = f"{settings.keycloak_server_url}/admin/realms/{settings.keycloak_realm}/users/{user_id}/role-mappings/realm"
    headers = {
        'Authorization': f"Bearer {user_token}",
        'Content-Type': 'application/json'
    }
    async with httpx.AsyncClient() as client:
        response = await client.post(url, headers=headers, json=data)
    if 200 <= response.status_code < 300:
        return "User role successfully changed"
    else:
        try:
            error_message = response.json().get("error", "")
        except ValueError:
            error_message = "Failed to decode the error message from the response."
        raise HTTPException(status_code=500, detail=error_message)
