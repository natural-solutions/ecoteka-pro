import httpx
from core.config import settings

async def get_admin_token():
    url = f"{settings.keycloak_server_url}/realms/{settings.keycloak_realm}/protocol/openid-connect/token"
    payload = {
        "client_id": settings.keycloak_client_id,
        "client_secret": settings.keycloak_client_secret,
        "grant_type": settings.keycloack_grant_type,
    }
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    async with httpx.AsyncClient() as client:
        response = await client.post(url, headers=headers, data=payload)
    response.raise_for_status()
    return response.json()["access_token"]
