from api.internal.helpers import (
    inclusive_filters,
    convert_coords_to_geojson
)

def get_default_organization():
    response = inclusive_filters("organization", {"default": True})

    if not response:
        return {}

    data = response[0]._asdict()

    data["coords"] = convert_coords_to_geojson(data["coords"])
    data["__typename"] = "organization"
    return data
