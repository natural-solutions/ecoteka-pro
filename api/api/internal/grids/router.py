from fastapi import APIRouter, Query, Response, HTTPException
from fastapi.responses import JSONResponse, StreamingResponse
from celery.result import AsyncResult
from io import BytesIO
from api.internal.grids.services import (
    get_all_locations_data_except_tree,
    get_all_trees,
    get_vegetated_areas
)

router = APIRouter()

@router.get("/get_combined_grid_data")
async def fetch_combined_grid_data(page: int = Query(1, ge=1), size: int = Query(10, ge=1)):
    locations_data, locations_total_count  = get_all_locations_data_except_tree(page, size)
    trees_data, trees_data_total_count = get_all_trees(page, size)
    vegetated_areas_data, vegetated_areas_total_count = get_vegetated_areas(page, size)

    combined_grid_data = {
        "locations": locations_data,
        "trees": trees_data,
        "vegetated_areas": vegetated_areas_data,
        "totalCount": locations_total_count + trees_data_total_count + vegetated_areas_total_count
    }

    return combined_grid_data

@router.get("/export_all_data_excel")
async def export_all_data_excel():
    # Import task here to avoid circular import
    from workers.data_transfer.tasks import export_all_data_to_excel_task
    task = export_all_data_to_excel_task.delay()  # Déclenche la tâche Celery
    return {"task_id": task.id, "status": "Export started"}


@router.get("/download_excel/{task_id}")
async def download_excel(task_id: str):
    # Fetch the Celery task result using the task_id
    task_result = AsyncResult(task_id)

    # Check if the task is completed successfully
    if task_result.state == 'SUCCESS':
        # Task is completed, retrieve the Excel data
        excel_buffer = task_result.result
        
        # Send the Excel data as a downloadable file
        return StreamingResponse(
            excel_buffer, 
            media_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            headers={"Content-Disposition": "attachment; filename=export.xlsx"}
        )
    elif task_result.state == 'FAILURE':
        return JSONResponse({"error": "Task failed"}, status_code=500)
    else:
        # Task is still processing or hasn't started yet
        return JSONResponse({"status": "Task is still in progress"}, status_code=202)
