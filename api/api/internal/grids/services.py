from sqlalchemy import text
from api.internal.helpers import (
    execute_sql_query,
)
from io import StringIO, BytesIO
from openpyxl import Workbook
import csv
import json
import logging
from collections import defaultdict

def get_all_locations_data_except_tree(page=1, size=10):
    sql_query = text("""
        WITH location_data AS (
            SELECT DISTINCT
                l.id AS location_id,
                l.address AS location_address,
                ls.status AS location_status,
                l.foot_type AS location_foot_type,
                l.landscape_type AS location_landscape_type,
                l.sidewalk_type AS location_sidewalk_type,
                l.plantation_type AS location_plantation_type,
                l.is_protected AS location_is_protected,
                us.slug AS location_urban_site,
                l.note AS location_note,
                l.inventory_source AS location_inventory_source,
                l.creation_date AS location_creation_date,
                ST_AsGeoJSON(l.coords) AS location_coords
            FROM 
                location l
            LEFT JOIN
                location_status ls ON l.id_status = ls.id
            LEFT JOIN
                urban_site us ON l.urban_site_id = us.id
            WHERE 
                ls.status != 'alive'
        ),
        boundaries AS (
            SELECT 
                bl.location_id,
                MAX(CASE WHEN b.type = 'administrative' THEN b.name END) AS administrative,
                MAX(CASE WHEN b.type = 'geographic' THEN b.name END) AS geographic,
                MAX(CASE WHEN b.type = 'station' THEN b.name END) AS station
            FROM 
                boundaries_locations bl
            JOIN 
                boundary b ON bl.boundary_id = b.id
            GROUP BY 
                bl.location_id
        ),
        urban_constraints AS (
            SELECT 
                luc.location_id,
                STRING_AGG(DISTINCT uc.slug, ',') AS constraints
            FROM 
                location_urban_constraint luc 
            JOIN 
                urban_constraint uc ON luc.urban_constraint_id = uc.id 
            GROUP BY 
                luc.location_id
        )
        SELECT 
            ld.*,
            b.administrative AS boundaries_administrative,
            b.geographic AS boundaries_geographic,
            b.station AS boundaries_station,
            uc.constraints AS urban_constraints
        FROM 
            location_data ld
        LEFT JOIN 
            boundaries b ON ld.location_id = b.location_id
        LEFT JOIN 
            urban_constraints uc ON ld.location_id = uc.location_id
        LIMIT :size OFFSET :offset
    """)

    result = execute_sql_query(sql_query, {"size": size, "offset": (page - 1) * size})

    total_count_query = text("""
        SELECT COUNT(*)
        FROM location l
        LEFT JOIN location_status ls ON l.id_status = ls.id
        WHERE ls.status != 'alive'
    """)
    total_count = execute_sql_query(total_count_query)[0][0]

    locations_data = []
    for row in result:
        if isinstance(row[10], str):
            inventory_source = row[10].split(',') if row[10] else []
        elif isinstance(row[10], list):
            inventory_source = row[10]  
        else:
            inventory_source = []
        locations_data.append({
            "id": row[0],
            "address": row[1],
            "location_status": row[2],
            "boundaries_administrative": row[13],
            "boundaries_geographic": row[14],
            "boundaries_station": row[15],
            "coords": json.loads(row[12]) if row[12] else None,
            "foot_type": row[3],
            "landscape_type": row[4],
            "sidewalk_type": row[5],
            "plantation_type": row[6],
            "is_protected": row[7],
            "urban_site": row[8],
            "urban_constraint": row[16].split(',') if row[16] else [],
            "note": row[9],
            "inventory_source": inventory_source,
            "creation_date": row[11],
            "__typename": "location"
        })


    return locations_data, total_count
    
def get_all_trees(page=1, size=10):
    sql_query = text("""
        WITH tree_data AS (
            SELECT DISTINCT
                t.id AS tree_id,
                t.scientific_name AS tree_scientific_name,
                t.serial_number AS tree_serial_number,
                t.vernacular_name AS tree_vernacular_name,
                t.plantation_date AS tree_plantation_date,
                t.variety AS tree_variety,
                t.height AS tree_height,
                t.circumference AS tree_circumference,
                t.habit AS tree_habit,
                t.is_tree_of_interest AS tree_of_interest,
                t.watering AS tree_watering,
                ST_AsGeoJSON(l.coords) AS location_tree_coords,
                l.id AS location_id,
                l.address as location_address,
                l.foot_type AS location_foot_type,
                l.landscape_type AS location_landscape_type,
                l.sidewalk_type AS location_sidewalk_type,
                l.plantation_type AS location_plantation_type,
                l.is_protected AS location_is_protected,
                l.inventory_source as location_inventory_source,
                us.slug AS location_urban_site
            FROM 
                tree t
            LEFT JOIN
                location l ON t.location_id = l.id
            LEFT JOIN
                urban_site us ON l.urban_site_id = us.id
        ),
        boundaries AS (
            SELECT 
                bl.location_id,
                MAX(CASE WHEN b.type = 'administrative' THEN b.name END) AS administrative,
                MAX(CASE WHEN b.type = 'geographic' THEN b.name END) AS geographic,
                MAX(CASE WHEN b.type = 'station' THEN b.name END) AS station
            FROM 
                boundaries_locations bl
            JOIN 
                boundary b ON bl.boundary_id = b.id
            GROUP BY 
                bl.location_id
        ),
        urban_constraints AS (
            SELECT 
                luc.location_id,
                STRING_AGG(DISTINCT uc.slug, ',') AS constraints
            FROM 
                location_urban_constraint luc 
            JOIN 
                urban_constraint uc ON luc.urban_constraint_id = uc.id 
            GROUP BY 
                luc.location_id
        )
        SELECT 
            td.*,
            b.administrative AS boundaries_administrative,
            b.geographic AS boundaries_geographic,
            b.station AS boundaries_station,
            uc.constraints AS urban_constraints
        FROM 
            tree_data td
        LEFT JOIN 
            boundaries b ON td.location_id = b.location_id
        LEFT JOIN 
            urban_constraints uc ON td.location_id = uc.location_id
        LIMIT :size OFFSET :offset
    """)

    result = execute_sql_query(sql_query, {"size": size, "offset": (page - 1) * size})

    total_count_query = text("SELECT COUNT(*) FROM tree")
    total_count = execute_sql_query(total_count_query)[0][0]

    trees_data = []
    for row in result:
        if isinstance(row[19], str):
            inventory_source = row[19].split(',') if row[19] else []
        elif isinstance(row[19], list):
            inventory_source = row[19]  
        else:
            inventory_source = []  
        trees_data.append({
            "id": row[0],
            "scientific_name": row[1],
            "serial_number": row[2],
            "vernacular_name": row[3],
            "plantation_date": row[4],
            "variety": row[5],
            "height": row[6],
            "circumference": row[7],
            "habit": row[8],
            "is_tree_of_interest": row[9],
            "watering": row[10],
            "coords": json.loads(row[11]) if row[11] else None,
            "address": row[13],
            "foot_type": row[14],
            "landscape_type": row[15],
            "sidewalk_type": row[16],
            "plantation_type": row[17],
            "is_protected": row[18],
            "inventory_source": inventory_source,
            "urban_site": row[20],
            "urban_constraint": row[24].split(',') if row[24] else [],
            "boundaries_administrative": row[21],
            "boundaries_geographic": row[22],
            "boundaries_station": row[23],
            "__typename": "tree"
        })


    return trees_data, total_count


def get_vegetated_areas(page=1, size=10):
    main_query = text("""
        WITH urban_constraints AS (
            SELECT 
                vuc.vegetated_area_id,
                STRING_AGG(DISTINCT uc.slug, ',') AS constraints
            FROM 
                vegetated_areas_urban_constraints vuc
            JOIN 
                urban_constraint uc ON vuc.urban_constraint_id = uc.id
            GROUP BY 
                vuc.vegetated_area_id
        ),
        residential_usage AS (
            SELECT 
                vr.vegetated_area_id,
                STRING_AGG(DISTINCT r.slug, ',') AS usage_types
            FROM 
                vegetated_areas_residential_usage_types vr
            JOIN 
                residential_usage_type r ON vr.residential_usage_type = r.id
            GROUP BY 
                vr.vegetated_area_id
        ),
        location_counts AS (
            SELECT 
                val.vegetated_area_id,
                COUNT(DISTINCT val.location_id) AS locations_count
            FROM 
                vegetated_areas_locations val
            GROUP BY 
                val.vegetated_area_id
        )
        SELECT 
            va.id AS va_id,
            va.address AS va_address,
            va.surface AS va_surface,
            va.type AS va_type,
            COALESCE(lc.locations_count, 0) AS locations_count,
            us.slug AS vegetated_area_urban_site,
            va.afforestation_trees_data,
            va.herbaceous_data,
            va.shrubs_data,
            va.landscape_type,
            va.has_differentiated_mowing,
            va.is_accessible,
            va.inconvenience_risk,
            va.hedge_type,
            va.linear_meters,
            va.potential_area_state,
            va.note,
            va.creation_date,
            uc.constraints AS vegetated_area_urban_constraints,
            ru.usage_types AS vegetated_area_residential_usage
        FROM 
            vegetated_area va
        LEFT JOIN 
            urban_site us ON va.urban_site_id = us.id
        LEFT JOIN 
            urban_constraints uc ON va.id = uc.vegetated_area_id
        LEFT JOIN 
            residential_usage ru ON va.id = ru.vegetated_area_id
        LEFT JOIN 
            location_counts lc ON va.id = lc.vegetated_area_id
        ORDER BY 
            va.id
        LIMIT :size OFFSET :offset
    """)

    result = execute_sql_query(main_query, {"size": size, "offset": (page - 1) * size})
    
    boundaries_query = text("""
        SELECT 
            bva.vegetated_area_id,
            b.name,
            b.type
        FROM 
            boundaries_vegetated_areas bva
        JOIN
            boundary b ON bva.boundary_id = b.id
        WHERE
            bva.vegetated_area_id IN :va_ids
    """)

    result = execute_sql_query(main_query, {"size": size, "offset": (page - 1) * size})
    
    va_ids = [row[0] for row in result]

    boundaries_result = execute_sql_query(boundaries_query, {"va_ids": tuple(va_ids)}) if va_ids else []
    
    boundaries_dict = defaultdict(lambda: {"administrative": None, "geographic": None, "station": None})
    for va_id, name, b_type in boundaries_result:
        boundaries_dict[va_id][b_type] = name

    vegetated_areas_data = []
    for row in result:
        vegetated_areas_data.append({
            "id": row[0],
            "address": row[1],
            "surface": row[2],
            "type": row[3],
            "vegetated_areas_locations": row[4],
            "urban_site": row[5],
            "afforestation_trees_data": row[6],
            "herbaceous_data": row[7],
            "shrubs_data": row[8],
            "landscape_type": row[9],
            "has_differentiated_mowing": row[10],
            "is_accessible": row[11],
            "inconvenience_risk": row[12],
            "hedge_type": row[13],
            "linear_meters": row[14],
            "potential_area_state": row[15],
            "note": row[16],
            "creation_date": row[17],
            "boundaries_administrative": boundaries_dict[row[0]]["administrative"],
            "boundaries_geographic": boundaries_dict[row[0]]["geographic"],
            "boundaries_station": boundaries_dict[row[0]]["station"],
            "urban_constraint": row[18].split(',') if row[18] else [],
            "residential_usage": row[19].split(',') if row[19] else [],
            "__typename": "vegetated_area"
        })

    total_count_query = text("SELECT COUNT(*) FROM vegetated_area")
    total_count = execute_sql_query(total_count_query)[0][0]

    return vegetated_areas_data, total_count
