import re
from json import loads
from typing import List
import numpy as np
import pandas as pd
from core.config import settings
from fastapi import HTTPException

from .models import (
    MetricsByGroupResponseModel,
    SubjectMonitoring,
    UrbasenseSubjectMonitoringRaw,
)
from sqlalchemy.sql import text
from api.internal.helpers import (
    execute_sql_query
    )
from sqlalchemy.dialects.postgresql import ARRAY, UUID
from ...metrics.models import Params
import valkey
import httpx
import json

valkey_client = valkey.Valkey(host='valkey', port=6379, db=0, decode_responses=True)

def get_client():
    return "client"

client = get_client()

def urbasense_api_base_url():
    return "https://arbre.urbasense.eu/api/v1/datahub"

def get_urbasense_subjects(site_id = settings.urbasense_monitored_site_id):
    with httpx.Client() as client:
        headers = {'token': settings.urbasense_api_access_token}
        r = client.get(f"{settings.urbasense_api_base_url}/sites/{site_id}/subjects", headers=headers)
        if r.status_code != 200:
            return HTTPException(status_code=r.status_code, detail="Oupsy!")
        return r.json()['data']

def get_urbasense_groups(site_id = settings.urbasense_monitored_site_id):
    with httpx.Client() as client:
        headers = {'token': settings.urbasense_api_access_token}
        r = client.get(f"{settings.urbasense_api_base_url}/sites/{site_id}/groupes", headers=headers)
        if r.status_code != 200:
            return HTTPException(status_code=r.status_code, detail="Oupsy!")
        return r.json()['data']

def get_urbasense_group_metrics(group_id: int) -> MetricsByGroupResponseModel:
    with httpx.Client() as client:
        headers = {'token': settings.urbasense_api_access_token}
        r = client.get(f"{settings.urbasense_api_base_url}/groupes/{group_id}/sujets", headers=headers)
        if r.status_code != 200:
            return HTTPException(status_code=r.status_code, detail="Oupsy!")
        subject_ids = [subject['id_suj'] for subject in r.json()['data']]
        return get_urbasense_subjects_metrics(subject_ids)

def get_urbasense_site_metrics():
    cache_key = "urbasense:site_metrics"
    cached_result = valkey_client.get(cache_key)
    if cached_result:
        return json.loads(cached_result)

    with httpx.Client() as client:
        headers = {'token': settings.urbasense_api_access_token}
        response = client.get(f"{settings.urbasense_api_base_url}/sujets", headers=headers)

        if response.status_code != 200:
            return HTTPException(status_code=response.status_code, detail="Oupsy!")

        subject_ids = [subject['id_suj'] for subject in response.json()['data']]
        metrics = get_urbasense_subjects_metrics(subject_ids)
        valkey_client.set(cache_key, json.dumps(metrics), ex=86400)

        return metrics

def get_urbasense_subjects_metrics(subject_ids: List[int]):
    with httpx.Client() as client:
        headers = {'token': settings.urbasense_api_access_token}
        payload = {"sujets": subject_ids}
        base_url = settings.urbasense_api_base_url

        responses = {}

        url_dendro = f"{base_url}/tableaudebord/dendro"
        r_dendro = client.post(url_dendro, headers=headers, json=payload, timeout=30)
        if r_dendro.status_code == 200:
            responses["dendro"] = r_dendro.json()
        else:
            return HTTPException(status_code=r_dendro.status_code, detail="Oupsy!")

        url_meteo = f"{base_url}/tableaudebord/meteo"
        r_meteo = client.post(url_meteo, headers=headers, json=payload, timeout=30)
        if r_meteo.status_code == 200:
            responses["meteo"] = r_meteo.json()
        else:
            return HTTPException(status_code=r_meteo.status_code, detail="Oupsy!")

        url_tensio = f"{base_url}/tableaudebord/tensio"
        r_tensio = client.post(url_tensio, headers=headers, json=payload, timeout=30)
        if r_tensio.status_code == 200:
            responses["tensio"] = r_tensio.json()
        else:
            return HTTPException(status_code=r_tensio.status_code, detail="Oupsy!")

        return responses

def get_urbasense_subject_monitoring_raw(subject_id: int) -> UrbasenseSubjectMonitoringRaw:
    with httpx.Client() as client:
        headers = {"token": settings.urbasense_api_preprod_access_token}
        r = client.get(f"{settings.urbasense_api_preprod_base_url}/sujet/{subject_id}/courbe", headers=headers)
        if r.status_code != 200:
            return HTTPException(status_code=r.status_code, detail="Oupsy!")
        return r


def get_urbasense_subject_monitoring_antv_series_format(subject_id: int) -> SubjectMonitoring:
    r = get_urbasense_subject_monitoring_raw(subject_id)
    # Preparing dataframes
    df_data = pd.json_normalize(r.json(), record_path=["data"])
    df_legend = pd.json_normalize(r.json(), record_path=["legende"])
    df_meteo = pd.json_normalize(r.json(), record_path=["meteo"])
    # df_data["date_mes"] = pd.to_datetime(df_data["date_mes"])
    df_data = df_data.rename(columns={"date_mes": "Date"})
    df_meteo = df_meteo.rename(columns={"date_mes": "Date"})
    # print(df_data)
    # Reshaping tensiometry data
    p_tensio = re.compile("S_\d")
    tensio_series = [s for s in df_data if p_tensio.match(s)]
    df_data_melted = pd.melt(df_data.reset_index(), id_vars="Date", value_vars=tensio_series, var_name="series")
    df_data_melted["sensor_number"] = df_data_melted["series"].apply(lambda x: x.replace("S_", ""))
    df_data_melted_with_legend = df_data_melted.merge(df_legend, left_on="sensor_number", right_on="ref_sond")
    df_data_melted_with_legend["series_name"] = df_data_melted_with_legend["propriete"] + ": " + df_data_melted_with_legend["nom_sonde"]
    df_data_final = df_data_melted_with_legend.filter(["Date", "series", "value", "series_name", "unite"], axis=1)
    df_data_final = df_data_final.sort_values(by=["Date", "series"])

    # Reshaping temperature data
    p_temp = re.compile("S_t")
    df_temp_melted = pd.melt(df_data.reset_index(), id_vars="Date", value_vars=['S_t'], var_name="series")
    df_temp_melted["unite"] = "degrés Celsius (°C)"
    df_temp_melted["series_name"] = "température"

    # Response formating
    response = {}
    response["tensio"] = loads(df_data_final.to_json(orient="records"))
    response["temperature"] = loads(df_temp_melted.to_json(orient="records"))
    response["meteo"] = loads(df_meteo.to_json(orient="records"))
    return response

def get_urbasense_subject_monitoring_antv_comboplot_format(subject_id: int):
    r = get_urbasense_subject_monitoring_raw(subject_id)
    df_data = pd.json_normalize(r.json(), record_path=["data"])
    df_legend = pd.json_normalize(r.json(), record_path=["legende"])
    df_meteo = pd.json_normalize(r.json(), record_path=["meteo"])
    df_data.set_index('date_mes', inplace=True )
    df_meteo.set_index('date_mes', inplace=True)
    concat = pd.concat([df_data, df_meteo], axis=1)
    # DATA Objects
    df_data_out = concat.reset_index()
    df_data_out.drop(columns=['date_originale'], inplace=True)
    df_data_out['date_mes']= pd.to_datetime(df_data_out['date_mes'])
    df_data_out.set_index('date_mes', inplace=True )
    df_data_out['tair_max']= pd.to_numeric(df_data_out['tair_max'])
    df_data_out['pp_mmjour']= pd.to_numeric(df_data_out['pp_mmjour'])
    df_data_out.sort_values(by=['date_mes'], inplace = True)
    df_data_out.interpolate(limit=5, limit_direction="forward", inplace=True)
    df_data_out.interpolate(limit=5, limit_direction="backward", inplace=True)
    df_data_out.reset_index(inplace=True)
    df_data_out_no_date = df_data_out.copy()
    df_data_out_no_date.drop(columns=['date_mes'], inplace= True)
    ids = tuple(i for i in df_data_out_no_date.columns)
    intermediate = pd.DataFrame(data={'id': np.asarray(ids)})
    intermediate["sensor_number"] = intermediate["id"].apply(lambda x: x.replace("S_", ""))
    df_meta_out = intermediate.set_index('sensor_number').join(df_legend.set_index('ref_sond'))
    df_meta_out.at["t",'unite']='degré Celcius (°C)'
    df_meta_out.at["tair_max",'unite']='degré Celcius (°C)'
    df_meta_out.at["pp_mmjour",'unite']='millimètre (mm)'
    df_meta_out["alias"] = (
    df_meta_out["propriete"] + ": " + df_meta_out["nom_sonde"]
    )
    df_meta_out.at["t",'alias']='température de surface'
    df_meta_out.at["tair_max",'alias']='température de l\'air'
    df_meta_out.at["pp_mmjour",'alias']='précipations journalières'
    # META Objects
    df_meta_out_final = pd.DataFrame(data=df_meta_out.reset_index(), columns=['id', 'alias', 'unite'])

    # Response formating
    response = {}
    response["data"] = loads(df_data_out.to_json(orient="records", date_format='iso'))
    response["metadata"] = loads(df_meta_out_final.to_json(orient="records"))
    return response

def find_urbasense_by_subject_id(params: Params):
    boundary_ids = tuple(params.boundary_ids)
    sql_query = text(f"""
    SELECT DISTINCT tree.urbasense_subject_id
    FROM tree
    JOIN location ON tree.location_id = location.id
    JOIN boundaries_locations ON location.id = boundaries_locations.location_id
    JOIN boundary ON boundaries_locations.boundary_id = boundary.id
    WHERE boundary.id IN :boundary_ids
    AND tree.urbasense_subject_id IS NOT NULL
    AND tree.urbasense_subject_id != '';
    """)
    params = {"boundary_ids": boundary_ids}
    result = execute_sql_query(sql_query, params)
    return {"urbasense_subject_ids": [row[0] for row in result]}
