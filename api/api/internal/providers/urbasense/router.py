from enum import Enum
from typing import Annotated, List, Optional

from core.config import settings
from fastapi import APIRouter, Depends, HTTPException, Query, Body
from pydantic import BaseModel
from sqlalchemy import text
from .models import (
    ComboPlotModel,
    MetricsByGroupResponseModel,
    SubjectMonitoring,
    UrbasenseSubjectMonitoringRaw,
    GroupParams,
    GroupType,
    Provider
)
from .services import (
    get_urbasense_group_metrics,
    get_urbasense_groups,
    get_urbasense_site_metrics,
    get_urbasense_subject_monitoring_antv_comboplot_format,
    get_urbasense_subject_monitoring_antv_series_format,
    get_urbasense_subject_monitoring_raw,
    get_urbasense_subjects,
    get_urbasense_subjects_metrics,
    find_urbasense_by_subject_id
)
from ...metrics.models import Params
from api.internal.helpers import (
    execute_sql_query
)

router = APIRouter()

async def group_params(params: GroupParams):
    return params
GroupParamsDependency = Annotated[GroupParams, Depends(group_params)]

# Authorzation quick and dirty fastAPI dep
async def check_urbasense_access():
    if settings.urbasense_api_access_token is None:
        raise HTTPException(status_code=401, detail="Unauthorized access! No Urbasense token provided")


# GET http://api_internal/urbasense/metrics/by_group
@router.post("/metrics/by_group", dependencies=[Depends(check_urbasense_access)], response_model=MetricsByGroupResponseModel)
async def metrics_by_group(params: GroupParamsDependency) -> MetricsByGroupResponseModel:
    if params.provider == "urbasense":
        if params.type == "group":
            return get_urbasense_group_metrics(params.id)
        if params.type == "site":
            return get_urbasense_site_metrics()
        if params.type == "subjects":
            return get_urbasense_subjects_metrics(params.ids)

    if params.provider == "ecoteka":
        raise HTTPException(status_code=404, detail="Ressource not found! Ecoteka Urbasense metrics not yet wired.")
    return HTTPException(status_code=422, detail="Unprocessable entity.")

# GET http://api_internal/urbasense/groups
@router.get("/groups", dependencies=[Depends(check_urbasense_access)])
async def urbasense_groups(site_id: int | None = None ):
    if site_id:
        return get_urbasense_groups(site_id)
    return get_urbasense_groups()

# GET http://api_internal/urbasense/subjects
@router.get("/subjects", dependencies=[Depends(check_urbasense_access)])
async def urbasense_subjects(group_id: int):
    if group_id:
        return get_urbasense_subjects(group_id)
    return HTTPException(status_code=400, detail="Bad request")

# GET http://api_internal/urbasense/subject/{subject_id}/monitor
@router.get("/subject/{subject_id}/monitor")
async def subject_monitor(subject_id: int,) -> SubjectMonitoring:
    return get_urbasense_subject_monitoring_antv_series_format(subject_id)

# GET http://api_internal/urbasense/subject/{subject_id}/monitor/comboplot
@router.get("/subject/{subject_id}/monitor/comboplot")
async def subject_monitor_comboplot(subject_id: int):
    return get_urbasense_subject_monitoring_antv_comboplot_format(subject_id)

# GET http://api_internal/urbasense/subject/{subject_id}/monitor/raw
@router.get("/subject/{subject_id}/monitor/raw")
async def subject_monitor_raw(subject_id: int) -> UrbasenseSubjectMonitoringRaw:
    response = get_urbasense_subject_monitoring_raw(subject_id)
    return response.json()


@router.post("/tree/subject")
async def get_urbasense_by_subject_id(params: Params = Body(...)):
    return find_urbasense_by_subject_id(params)
