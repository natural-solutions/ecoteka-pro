package com.ns.ecoteka;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.keycloak.crypto.AsymmetricSignatureSignerContext;
import org.keycloak.crypto.KeyUse;
import org.keycloak.crypto.KeyWrapper;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.events.admin.ResourceType;
import org.keycloak.jose.jws.JWSBuilder;
import org.keycloak.models.KeycloakContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.representations.AccessToken;
import org.keycloak.services.Urls;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HTTPEventListenerProvider implements EventListenerProvider {
    private KeycloakSession session;

    public HTTPEventListenerProvider(KeycloakSession session) {
        this.session = session;
    }

    @Override
    public void close() {
        return;
    }

    @Override
    public void onEvent(Event event) {
        return;
    }

    @Override
    public void onEvent(AdminEvent event, boolean includeRepresentation) {
        if (event.getResourceType() != ResourceType.USER) {
            return;
        }

        String userId = event.getResourcePath().split("/")[1];
        String token = getAccessToken(event, session);

        switch (event.getOperationType()) {
            case DELETE:
                try {
                    WebHookUserEvent webHookUserEventCreateDelete = new WebHookUserEvent(
                            event.getOperationType().toString(),
                            userId);
                    send(token, webHookUserEventCreateDelete);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                break;
            case CREATE:
            case UPDATE:
                try {
                    RealmModel realm = session.realms().getRealm(event.getRealmId());
                    UserModel user = session.users().getUserById(realm, userId);
                    WebHookUserEvent webHookUserEventCreateUpdate = new WebHookUserEvent(
                            event.getOperationType().toString(),
                            user.getId(),
                            user.getUsername());
                    send(token, webHookUserEventCreateUpdate);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            default:
                break;
        }
    }

    public String getAccessToken(AdminEvent event, KeycloakSession keycloakSession) {
        String adminRole = System.getenv().getOrDefault("API_WEBHOOK_ADMIN_ROLE", "admin");
        String userId = event.getAuthDetails().getUserId();
        KeycloakContext keycloakContext = keycloakSession.getContext();
        String[] allowedRoles = new String[] { adminRole, "default-roles-" + event.getRealmId() };

        AccessToken token = new AccessToken();

        token.subject(userId);
        token.issuer(Urls.realmIssuer(keycloakContext.getUri().getBaseUri(), keycloakContext.getRealm().getName()));
        token.issuedNow();
        token.audience(event.getRealmId());
        token.type("Bearer");
        token.setOtherClaims("x-hasura-default-role", adminRole);
        token.setOtherClaims("x-hasura-allowed-roles", allowedRoles);
        token.setOtherClaims("x-hasura-user-id", userId);
        token.expiration((int) (token.getIat() + 60 * 60L));

        KeyWrapper key = keycloakSession.keys().getActiveKey(keycloakContext.getRealm(), KeyUse.SIG, "RS256");

        return new JWSBuilder().kid(key.getKid()).type("JWT").jsonContent(token)
                .sign(new AsymmetricSignatureSignerContext(key));
    }

    public HttpResponse send(String token, WebHookUserEvent type) throws ClientProtocolException, IOException {
        String apiWebhookUri = System.getenv().getOrDefault("API_WEBHOOK_URI",
                "http://ecoteka.localdomain:8888/api/rest/keyclaok/event");
        ObjectMapper objectMapper = new ObjectMapper();
        String payload = objectMapper.writeValueAsString(type);
        StringEntity entity = new StringEntity(payload,
                ContentType.APPLICATION_JSON);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(apiWebhookUri);

        request.setHeader("Authorization", "Bearer " + token);
        request.setEntity(entity);

        HttpResponse response = httpClient.execute(request);

        return response;
    }
}