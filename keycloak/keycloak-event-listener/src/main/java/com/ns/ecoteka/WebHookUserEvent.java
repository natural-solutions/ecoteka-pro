package com.ns.ecoteka;

public class WebHookUserEvent {
    private String type;
    private String userId;
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public WebHookUserEvent(String type, String userId) {
        this.type = type;
        this.userId = userId;
    }

    public WebHookUserEvent(String type, String userId, String username) {
        this.type = type;
        this.userId = userId;
        this.username = username;
    }
}
