# Changelog

All notable changes to this project will be documented in this file.

This project does not adhere to [Semantic Versioning](http://semver.org/) yet.
Until 1.0 relese candidate, we use versionning based on date and deploy count.

Here is the current format: 0.{YY}{MM}.{DEPLOY_NUMBER_IN_CURRENT_MONTH_BASE100}

## [1.6.1] - 2025-02-10

### Fixed

- disable add buttons if tree has felling_date

## [1.6.0] - 2025-02-10

### Added

- User permissions management in the application, including interface and Hasura permissions
- Worksite editing functionality
- Enabled worksite realization as a global action, along with granular actions on interventions
- Two new fields for interventions and included all columns in the export feature
- Cadastral basemap
- Switch from Redis to Valkey
- Doc for diagrams and architecture to aid in understanding the system design
- Remove nivo.js library and use echarts-for-react for late interventions chart

[PR-365](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/365)
[PR-366](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/366)
[PR-367](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/367)
[PR-368](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/368)
[PR-369](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/369)
[PR-370](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/370)
[PR-371](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/371)
[PR-372](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/372)
[PR-373](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/373)
[PR-374](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/374)
[PR-375](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/375)

## [1.5.13] - 2025-01-09

### Added

- On /account page, user with admin role can create and delete users
- if user has data on app, he cannot be deleted only disabled

## [1.5.12] - 2024-12-16

### Fixed

- circumference default value 10 to null

## [1.5.11] - 2024-12-16

### Added

- Pass token on all api routes
- New intervention types and families

[PR-356](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/356)
[PR-357](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/357)
[PR-355](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/355)

## [1.5.10] - 2024-11-25

### Fixed

- Export all data method

## [1.5.9] - 2024-11-21

### Added

- PYTHONPATH env variable on api Dockerfile

## [1.5.8] - 2024-11-20

### Added

- Map filter on administrative boundary
- Export combined data grid with worker backend logic

### Fixed

- Grid filters and export filtered on combined data grid

[PR-344](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/344)
[PR-345](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/345)

## [1.5.7] - 2024-11-08

### Added

- Column inventory_source on location table
- Implement map filter on this new field
- Some design on dashboard

### Fixed

- click on dashboard button "dangerous trees"

[PR-339](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/339)
[PR-340](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/340)

## [1.5.6] - 2024-10-25

### Added

- Page combined_data grid with 3 types of objects: trees, locations and vegetated_areas
- New api route for data of combined grid and server side pagination

### Fixed

- Worksite validations

[PR-336](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/336)

## [1.5.5] - 2024-10-15

### Added

- New worksite creation process (user can select different types of location and assign different interventions)
- Even when user refuse geolocalisation on browser, show dialog for geoloc
- Map filters are persistent now
- Use helpertext for suggested address

### Fixed

- Drop unecessary tables worksite_location and worksite_intervention
- Serial number is incremented when creating multiple trees

[PR-331](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/331)
[PR-327](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/327)
[PR-326](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/326)
[PR-325](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/325)
[PR-324](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/324)
[PR-323](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/323)

## [1.5.4] - 2024-07-17

### Added

- Add dark map background
- On tree creation process, user can add a photo
  (method 'move-file-in-minio' => first path trees/{serialnumber}/latest.png and after creation successful replace by path trees/{id}/latest.png)
- On diagnosis/id page, user can add/edit a photo
- Method for insert new taxa on database (304 new taxa)
- Add tree height on filter_locations function
- Update seed file for le-gosier instance (add administrative boundaries)

### Fixed

- GDAL version => 3.8.5 (not latest cause build errors)
- Remove intrusive loader on map and fix persistent tooltip when hovering polygons
- Form validations for diagnosis
- Refactoring of DiagnosisForm component

[PR-320](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/320)
[PR-319](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/319)
[PR-318](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/318)
[PR-317](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/317)
[PR-316](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/316)
[PR-315](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/315)
[PR-314](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/314)
[PR-313](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/313)
[PR-312](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/312)
[PR-311](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/311)
[PR-310](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/310)
[PR-309](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/309)
[PR-308](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/308)
[PR-302](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/302)

## [1.5.3] - 2024-07-05

### Fixed

- Fix unexpected variable vegetatedAreaIds on createBoundary

[Commit](https://gitlab.com/natural-solutions/ecoteka-pro/-/commit/981dfa48212c21f39bc7e08ea5cef86a09fe8218)

## [1.5.2] - 2024-07-05

### Fixed

- Fix createLocation method

[PR-303](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/303)

## [1.5.1] - 2024-07-04

### Added

- Add new vegetated areas enhancements:
  - Afforestation trees: an autocomplete with meilisearch index replaces initial text field for specie name
  - Name of the station on station card of vegetated area page
  - Add tooltips informations on title for afforestation trees and trees with locations

### Fixed

- Default value set to null for height on PlantInputRow
- Fill color polygon on map only for vegetated area, remove for boundaries

[PR-299](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/299)

## [1.5.0] - 2024-07-03

### Added

- Add new vegetated areas management features:
  - User can now create vegetated areas polygons of different that can be associated to geographic or administrative boundaries, or to managment zones
  - Tree locations can be associated to a vegetated areas
  - User can describe shrub or grasslans compostion for a given vegetated area
  - Each vegetated areas are readable and editable on its ownn page

### Fixed

- Fix locations circles size on map
- Fix Hasura migrations errors
- Fix confirmation modal behaviour for location creation
- Use backup in proxy mode

[PR-287](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/287)
[PR-289](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/289)
[PR-288](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/288)
[PR-290](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/290)
[PR-291](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/291)
[PR-292](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/292)
[PR-293](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/293)
[PR-294](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/294)
[PR-295](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/295)

## [1.4.0] - 2024-06-04

### Added

- New taxons with hybrid species
- Add polygon creation process on map for geographic and station boundary (a boundary of this type still cannot have coords, it's not a required field)
- Polygon can be edited/deleted
- Add boundary filter on map filters (station and geographic boundary)
- Add worksite filter on map filters (for see all active worksites)
- Pre filled location boundary if location point inside a boundary

### Fixed

- Secure api routes for keycloak by token
- Diagnosis creation form error
- Ux enhancements mobile/tablet

[PR-284](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/284)
[PR-281](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/281)
[PR-276](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/276)
[PR-272](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/272)
[PR-273](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/273)
[PR-280](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/280)
[PR-282](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/282)
[PR-283](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/283)
[PR-277](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/277)

## [1.3.6] - 2024-05-13

### Added

- On interventions grid, i can export all data or filtered data to CSV
- Use IGN services for satellite layer and update url for ign layer
- Worksite creation: on click on map i can select or remove a location from selection
- Date range filters on interventions grid
- Use last version of material-react-table (version 1 to 2)

### Fixed

- Versions of storybook packages
- SelectionLayer for worksite creation
- Filters on all grids specially date filters
- Diagnosis filters on map filters

[PR-269](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/269)
[PR-268](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/268)
[PR-267](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/267)

## [1.3.5] - 2024-04-18

### Added

- As an admin, on account page, i can see all of users, and create/edit/delete if need
- Enhancements dashboard performances and sector filtering
- Enhancements interventions grid filters
- Intervention modal for edit/delete/declare realized intervention (remove intervention/{id} page)

### Fixed

- Map loader and worksite mode

[PR-230](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/230)
[PR-260](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/260)
[PR-263](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/263)

## [1.3.4] - 2024-04-10

### Added

- Green area table but button for adding one temporally disabled because wip

### Fixed

- Data fetching for dashboard without graphql actions (remove it), use directly api calls
- Worksite form: intervention partner id on creation
- Dashboard filter dangerous trees
- Import process for dijon data

[PR-250](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/250)
[PR-244](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/244)
[PR-243](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/243)
[PR-252](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/252)

## [1.3.3] - 2024-03-27

### Added

- Backend method to associate tree to taxon
- Modal for add diagnosis
- Remove pages intervention/add diagnosis/add => only use modal for add
- New fields on diagnosis form, location form and tree form
- Seed files for new fields
- Seed file for "Le Gosier" sectors
- Replace label by slug on database for internationalization

### Fixed

- Refactor: remove some duplicate components & code for Location and Tree form (use only LocationFields & TreeFields)
- Use satellite-v2 maptiler

[PR-219](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/219)
[PR-241](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/241)
[PR-245](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/245)
[PR-240](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/240)

## [1.3.2] - 2024-03-11

### Added

- Seed file for Alès sectors

### Fixed

- Get late interventions method
- Scheduled date on worksite form (replace datepicker)
- Style
- Retroactions on interventions validations (felling, planting and grubbing)

[PR-235](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/235)
[PR-236](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/236)
[PR-234](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/234)
[PR-233](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/233)

## [1.3.1] - 2024-02-20

### Added

- New graph on dashboard for species distribution
- New page /diagnosis to see all diagnosis
- New page /account
- Add button on tree card to see all diagnosis of this tree
- Use martin service for intervention and diagnosis modals
- Add indexes on dashboard data
- Keep dashboard filters persistent

### Fixed

- Urbasense pydantic models

[PR-223](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/223)
[PR-222](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/222)
[PR-221](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/221)
[PR-208](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/208)
[PR-207](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/207)
[PR-218](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/218)
[PR-217](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/217)
[PR-216](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/216)
[PR-215](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/215)

## [1.3.0] - 2024-01-31

### Added

- Harmonizing the visuals on interventions list page
- Only one typography "Poppins" and native typo if this one not supported
- New analytics on dashboard: well-being indice, number of trees of interest and trees to cut down
- New module on dashboard to see only late interventions
- Actualize analytics data with sector filter
- New map filter: trees equiped with urbasense captor
- Update database schema (new tables and join tables)

### Fixed

- Intervention partner field is not required anymore on intervention form
- Version of SQLAlchemy

[PR-204](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/204)
[PR-212](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/212)
[PR-211](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/211)
[PR-200](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/200)
[PR-209](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/209)
[PR-206](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/206)

## [1.2.0] - 2024-01-11

### Added

- Use map page implemented with martin service on principal map page
- Reload martin map layer when add location
- Filters logic backend and frontend
- New design pictures for locations card infos
- Add GIST index on coords table location
- Add Btree indexes on filtered data for map

### Fixed

- Import process, if coords already exists in database skip insertion but continue the process for other rows

[PR-189](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/189)
[PR-190](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/190)
[PR-194](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/194)
[PR-195](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/195)
[PR-196](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/196)

## [1.1.2] - 2023-12-21

### Added

- On location map card, add zoom controls and geolocalisation
- Update pydantic models for urbasense new response api model
- When quit edition mode without saving, open modal. If confirm, reset fields

### Fixed

- On each deploy, Meilisearch data is persistent now
- Fetch suggested address when lat/lng change
- Yup schema validation for height (type numeric now)
- When delete a location, if this is the only location concerned by a worksite, delete the worksite

[PR-181](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/181)
[PR-183](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/183)
[PR-184](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/184)
[PR-185](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/185)

## 2023-12-13

Frontend and design :

- Design on desktop and mobile headers
- Responsive design
- Add Cypress IDs to components
- Worksite page: map centered on locations

### Added

- Header design on desktop and mobile
- New mobile design for tree/location cards
- Toolbar on map page for tablet and mobile
- Map active filters information
- Cypress attributes on map page

[PR-177](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/177)
[PR-179](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/179)
[PR-175](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/175)
[PR-172](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/172)
[PR-173](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/173)

## [1.1.1] - 2023-11-30

- Integrate new backend dashboard metrics (non_allergenic, vulnerability)
- Integrate frontend dashboard metrics (non_allergenic, vulnerability)

### Added

Backend dashboard metrics:

- Non allergenic calculation
- Vulnerability calculation

  [PR-159]https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/159

  [PR-168]https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/168

### Fixed

- Fix minio image url: add DEFAULT_STORAGE_BUCKET_NAME on publicRuntimeConfig
- Fix import process error on address column

## [1.1.0] - 2023-11-24

- Definie: integrate backend dashboard metrics (air, carbon, icu, biodiversity)
- New feature: integrate frontend dashboard metrics (air, carbon, icu, biodiversity)

### Added

Backend dashboard metrics:

- Carbon metric calculation
- Air quality metric calculation
- Biodiversity metric calculation
- ICU metric calculation
- Python microservices metrics API added to GraphQL endpoint
- Add Cypress IDs to components

[PR-153](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/153)

### Changed

- Resolve react-i18next build errors, need to upgrade typescript version for working
- Upgrade typescript version from 4 to 5 - [PR-152](https://gitlab.com/natural-solutions/ecoteka-pro/-/merge_requests/152)

### Fixed

- Set a default release version for MinIO setup.
- Mobile drawer display for worksite creationn from map
- Cardinfo hover effects

## [0.2311.02] - 2023-11-03

Mainly trying to resolve conflict on `main` branch due to missing hotfixes on `dev` branch

### Added

### Changed

### Fixed

- Syncing `main` and `dev`

## [0.2311.01] - 2023-11-03

This is the initial version release after months of active development. List of additions, changes, fixes are not fully detailled and exhaustive. Following versions will be.

### Added

#### Stack

- PostGIS **database** with Hasura **GraphQL engine**
- **Python microservices** with FastAPI
- **Search engine** with Meilisearch
- **Vector tiles service** with Martin
- **Authentification service** with Keycloak
- **Networking** with Traefik
- **Webapp** with NextJS and map visualisation GL engine

#### Features

- Add and import locations and trees
- Manage interventions on tree heritage with callback actions
- Manage tree health diagnosis
- Dashboarding
- Map visualisation and filtering
- External real-time tree monitoring (Urbasense integration)
- Manage worksites (multiple locations and interventions management)
- Geographical and administrative boundaries support
- Tree species search engine

### Changed

### Fixed
